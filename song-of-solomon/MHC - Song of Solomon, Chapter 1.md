Song of Solomon, Chapter 1
==========================

Commentary
----------

In this chapter, after the title of the book (v. 1), we have Christ and
his church, Christ and a believer, expressing their esteem for each
other. `I.` The bride, the church, speaks to the bridegroom (v. 2-4), to
the daughters of Jerusalem (v. 5, 6), and then to the bridegroom (v. 7).
`II.` Christ, the bridegroom, speaks in answer to the complaints and
requests of his spouse (v. 8-11). `III.` The church expresses the great
value she has for Christ, and the delights she takes in communion with
him (v. 12-14). `IV.` Christ commends the church\'s beauty (v. 15). `V.` The
church returns the commendation (v. 16, 17). Where there is a fire of
true love to Christ in the heart this will be of use to blow it up into
a flame.

### Verse 1

We have here the title of this book, showing, 1. The nature of it; it is
a song, that it might the better answer the intention, which is to stir
up the affections and to heat them, which poetry will be very
instrumental to do. The subject is pleasing, and therefore fit to be
treated of in a song, in singing which we may make melody with our
hearts unto the Lord. It is evangelical; and gospel-times should be
times of joy, for gospel-grace puts a new song into our mouths, Ps.
98:1. 2. The dignity of it; it is the song of songs, a most excellent
song, not only above any human composition, or above all other songs
which Solomon penned, but even above any other of the scripture-songs,
as having more of Christ in it. 3. The penman of it; it is Solomon\'s.
It is not the song of fools, as many of the songs of love are, but the
song of the wisest of men; nor can any man give a better proof of his
wisdom than to celebrate the love of God to mankind and to excite his
own love to God and that of others with it. Solomon\'s songs were a
thousand and five (1 Ki. 4:32); those that were of other subjects are
lost, but this of seraphic love remains, and will to the end of time.
Solomon, like his father, was addicted to poetry, and, which way soever
a man\'s genius lies, he should endeavor to honour God and edify the
church with it. One of Solomon\'s names was Jedidiah-beloved of the Lord
(2 Sa. 12:25); and none so fit to write of the Lord\'s love as he that
had himself so great an interest in it; none of all the apostles wrote
so much of love as he that was himself the beloved disciple and lay in
Christ\'s bosom. Solomon, as a king, had great affairs to mind and
manage, which took up much of his thoughts and time, yet he found heart
and leisure for this and other religious exercises. Men of business
ought to be devout men, and not to think that business will excuse them
from that which is every man\'s great business-to keep up communion with
God. It is not certain when Solomon penned this sacred song. Some think
that he penned it after he recovered himself by the grace of God from
his backslidings, as a further proof of his repentance, and as if by
doing good to many with this song he would atone for the hurt he had
perhaps done with loose, vain, amorous songs, when he loved many strange
wives; now he turned his wit the right way. It is more probable that he
penned it in the beginning of his time, while he kept close to God and
kept up his communion with him; and perhaps he put this song, with his
father\'s psalms, into the hands of the chief musician, for the service
of the temple, not without a key to it, for the right understanding of
it. Some think that it was penned upon occasion of his marriage with
Pharaoh\'s daughter, but that is uncertain; the tower of Lebanon, which
is mentioned in this book (ch. 7:4), was not built, as is supposed, till
long after the marriage. We may reasonably think that when in the height
of his prosperity he loved the Lord (1 Ki. 3:3) he thus served him with
joyfulness and gladness of heart in the abundance of all things. It may
be rendered, The song of songs, which is concerning Solomon, who as the
son and successor of David, on whom the covenant of royalty was
entailed, as the founder of the temple, and as one that excelled in
wisdom and wealth, was a type of Christ, in whom are hidden all the
treasures of wisdom and knowledge, and yet is a greater than Solomon;
this is therefore a song concerning him. It is here fitly placed after
Ecclesiastes; for when by the book we are thoroughly convinced of the
vanity of the creature, and its insufficiency to satisfy us and make a
happiness for us, we shall be quickened to seek for happiness in the
love of Christ, and that true transcendent pleasure which is to be found
only in communion with God through him. The voice in the wilderness,
that was to prepare Christ\'s way, cried, All flesh is grass.

### Verses 2-6

The spouse, in this dramatic poem, is here first introduced addressing
herself to the bridegroom and then to the daughters of Jerusalem.

`I.` To the bridegroom, not giving him any name or title, but beginning
abruptly: Let him kiss me; like Mary Magdalen to the supposed gardener
(Jn. 20:15), If thou have borne him hence, meaning Christ, but not
naming him. The heart has been before taken up with the thoughts of him,
and to this relative those thoughts were the antecedent, that good
matter which the heart was inditing, Ps. 45:1. Those that are full of
Christ themselves are ready to think that others should be so too. Two
things the spouse desires, and pleases herself with the thoughts of:-

`1.` The bridegroom\'s friendship (v. 2): \"Let him kiss me with the
kisses of his mouth, that is, be reconciled to me, and let me know that
he is so; let me have the token of his favour.\" Thus the Old-Testament
church desired Christ\'s manifesting himself in the flesh, to be no
longer under the law as a schoolmaster, under a dispensation of bondage
and terror, but to receive the communications of divine grace in the
gospel, in which God is reconciling the world unto himself, binding up
and healing what by the law was torn and smitten; as the mother kisses
the child that she has chidden. \"Let him no longer send to me, but come
himself, no longer speak by angels and prophets, but let me have the
word of his own mouth, those gracious words (Lu. 4:22), which will be to
me as the kisses of the mouth, sure tokens of reconciliation, as Esau\'s
kissing Jacob was.\" All gospel duty is summed up in our kissing the Son
(Ps. 2:12); so all gospel-grace is summed up in his kissing us, as the
father of the prodigal kissed him when he returned a penitent. It is a
kiss of peace. Kisses are opposed to wounds (Prov. 27:6), so are the
kisses of grace to the wounds of the law. Thus all true believers
earnestly desire the manifestations of Christ\'s love to their souls;
they desire no more to make them happy than the assurance of his favour,
the lifting up of the light of his countenance upon them (Ps. 4:6, 7),
and the knowledge of that love of his which surpasses knowledge; this is
the one thing they desire, Ps. 27:4. They are ready to welcome the
manifestation of Christ\'s love to their souls by his Spirit, and to
return them in the humble professions of love to him and complacency in
him, above all. The fruit of his lips is peace, Isa. 57:19. \"Let him
give me ten thousand kisses whose very fruition makes me desire him
more, and, whereas all other pleasures sour and wither by using, those
of the Spirit become more delightful.\" So bishop Reynolds. She gives
several reasons for this desire. `(1.)` Because of the great esteem she
has for his love: Thy love is better than wine. Wine makes glad the
heart, revives the drooping spirits, and exhilarates them, but gracious
souls take more pleasure in loving Christ and being beloved of him, in
the fruits and gifts of his love and in the pledges and assurances of
it, than any man ever took in the most exquisite delights of sense, and
it is more reviving to them than ever the richest cordial was to one
ready to faint. Note, `[1.]` Christ\'s love is in itself, and in the
account of all the saints, more valuable and desirable than the best
entertainments this world can give. `[2.]` Those only may expect the
kisses of Christ\'s mouth, and the comfortable tokens of his favour, who
prefer his love before all delights of the children of men, who would
rather forego those delights than forfeit his favour, and take more
pleasure in spiritual joys than in any bodily refreshments whatsoever.
Observe here the change of the person: Let him kiss me; there she speaks
of him as absent, or as if she were afraid to speak to him; but, in the
next words, she sees him near at hand, and therefore directs her speech
to him: \"Thy love, thy loves\" (so the word is), \"I so earnestly
desire, because I highly esteem it.\" `(2.)` Because of the diffuse
fragrancy of his love and the fruits of it (v. 3): \"Because of the
savour of thy good ointment (the agreeableness and acceptableness of thy
graces and comforts to all that rightly understand both them and
themselves), thy name is as ointment poured forth, thou art so, and all
that whereby thou hast made thyself known; thy very name is precious to
all the saints; it is an ointment and perfume which rejoice the heart.\"
The unfolding of Christ\'s name is as the opening of a box of precious
ointment, which the room is filled with the odour of. The preaching of
his gospel was the manifesting the savour of his knowledge in every
place, 2 Co. 2:14. The Spirit was the oil of gladness wherewith Christ
was anointed (Heb. 1:9), and all true believers have that unction (1 Jn.
2:27), so that he is precious to them, and they to him and to one
another. A good name is as precious ointment, but Christ\'s name is more
fragrant than any other. Wisdom, like oil, makes the face to shine; but
the Redeemer outshines, in beauty, all others. The name of Christ is not
now like ointment sealed up, as it had been long (Ask not after my name,
for it is secret), but like ointment poured forth, which denotes both
the freeness and fulness of the communications of his grace by the
gospel. `(3.)` Because of the general affection that all holy souls have
to him: Therefore do the virgins love thee. It is Christ\'s love shed
abroad in our hearts that draws them out in love to him; all that are
pure from the corruptions of sin, that preserve the chastity of their
own spirits, and are true to the vows by which they have devoted
themselves to God, that not only suffer not their affections to be
violated but cannot bear so much as to be solicited by the world and the
flesh, those are the virgins that love Jesus Christ and follow him
whithersoever he goes, Rev. 14:4. And, because Christ is the darling of
all the pure in heart, let him be ours, and let our desires be towards
him and towards the kisses of his mouth.

`2.` The bridegroom\'s fellowship, v. 4. Observe here,

`(1.)` Her petition for divine grace: Draw me. This implies sense of
distance from him, desire of union with him. \"Draw me to thyself, draw
me nearer, draw me home to thee.\" She had prayed that he would draw
nigh to her (v. 2); in order to that, she prays that he would draw her
nigh to him. \"Draw me, not only with the moral suasion which there is
in the fragrancy of the good ointments, not only with the attractives of
that name which is as ointment poured forth, but with supernatural
grace, with the cords of a man and the bands of love,\" Hos. 11:4.
Christ has told us that none come to him but such as the Father draws,
Jn. 6:44. We are not only weak, and cannot come of ourselves any further
than we are helped, but we are naturally backward and averse to come,
and therefore must pray for those influences and operations of the
Spirit, by the power of which we are unwilling made willing, Ps. 110:3.
\"Draw me, else I move not; overpower the world and the flesh that would
draw me from thee.\" We are not driven to Christ, but drawn in such a
way as is agreeable to rational creatures.

`(2.)` Her promise to improve that grace: Draw me, and then we will run
after thee. See how the doctrine of special and effectual grace consists
with our duty, and is a powerful engagement and encouragement to it, and
yet reserves all the glory of all the good that is in us to God only.
Observe, `[1.]` The flowing forth of the soul after Christ, and its
ready compliance with him, are the effect of his grace; we could not run
after him if he did not draw us, 2 Co. 3:5; Phil. 4:13. `[2.]` The grace
which God gives us we must diligently improve. When Christ by his Spirit
draws us we must with our spirits run after him. As God says, I will,
and you shall (Eze. 36:27), so we must say, \"Thou shalt and we will;
thou shalt work in us both to will and to do, and therefore we will work
out our own salvation\" (Phil. 2:12, 13); not only we will walk, but we
will run after thee, which denotes eagerness of desire, readiness of
affection, vigour of pursuit, and swiftness of motion. When thou shalt
enlarge my heart then I will run the way of thy commandments (Ps.
119:32); when thy right hand upholds me then my soul follows hard after
thee (Ps. 63:8); when with lovingkindness to us he draws us (Jer. 31:3)
we with lovingkindness to him must run after him, Isa. 40:31. Observe
the difference between the petition and the promise: \"Draw me, and then
we will run.\" When Christ pours out his Spirit upon the church in
general, which is his bride, all the members of it do thence receive
enlivening quickening influences, and are made to run to him with the
more cheerfulness, Isa. 55:5. Or, \"Draw me\" (says the believing soul)
\"and then I will not only follow thee myself as fast as I can, but will
bring all mine along with me: We will run after thee, I and the virgins
that love thee (v. 3), I and all that I have any interest in or
influence upon, I and my house (Jos. 24:15), I and the transgressors
whom I will teach thy ways,\" Ps. 51:13. Those that put themselves
forth, in compliance with divine grace, shall find that their zeal will
provoke many, 2 Co. 9:2. Those that are lively will be active; when
Philip was drawn to Christ he drew Nathanael; and they will be
exemplary, and so will win those that would not be won by the word.

`(3.)` The immediate answer that was given to this prayer: The King has
drawn me, has brought me into his chambers. It is not so much an answer
fetched by faith from the world of Christ\'s grace as an answer fetched
by experience from the workings of his grace. If we observe, as we
ought, the returns of prayer, we may find that sometimes, while we are
yet speaking, Christ hears, Isa. 65:24. The bridegroom is a king; so
much the more wonderful is his condescension in the invitations and
entertainments that he gives us, and so much the greater reason have we
to accept of them and to run after him. God is the King that has made
the marriage-supper for his Son (Mt. 22:2) and brings in even the poor
and the maimed, and even the most shy and bashful are compelled to come
in. Those that are drawn to Christ are brought, not only into his
courts, into his palaces (Ps. 45:15), but into his presence-chamber,
where his secret is with them (Jn. 14:21), and where they are safe in
his pavilion, Ps. 27:5; Isa. 26:20. Those that wait at wisdom\'s gates
shall be made to come (so the word is) into her chambers; they shall be
led into truth and comfort.

`(4.)` The wonderful complacency which the spouse takes in the honour
which the king put upon her. Being brought into the chamber, `[1.]` \"We
have what we would have. Our desires are crowned with unspeakable
delights; all our griefs vanish, and we will be glad and rejoice. If a
day in the courts, much more an hour in the chambers, is better than a
thousand, than ten thousand, elsewhere.\" Those that are, through grace,
brought into covenant and communion with God, have reason to go on their
way rejoicing, as the eunuch (Acts 8:39), and that joy will enlarge our
hearts and be our strength, Neh. 8:10. `[2.]` All our joy shall centre
in God: \"We will rejoice, not in the ointments, or the chambers, but in
thee. It is God only that is our exceeding joy, Ps. 43:4. We have no joy
but in Christ, and which we are indebted to him for.\" Gaudium in
Domino-Joy in the Lord, was the ancient salutation, and Salus in Domino
sempiterna-Eternal salvation in the Lord. `[3.]` \"We will retain the
relish and savour of this kindness of thine and never forget it: We will
remember thy loves more than wine; no only thy love itself (v. 2), but
the very remembrance of it shall be more grateful to us than the
strongest cordial to the spirits, or the most palatable liquor to the
taste. We will remember to give thanks for thy love, and it shall make
more durable impressions upon us than any thing in this world.\"

`(5.)` The communion which a gracious soul has with all the saints in this
communion with Christ. In the chambers to which we are brought we not
only meet with him, but meet with one another (1 Jn. 1:7); for the
upright love thee; the congregation, the generation, of the upright love
thee. Whatever others do, all that are Israelites indeed, and faithful
to God, will love Jesus Christ. Whatever differences of apprehension and
affection there may be among Christians in other things, this they are
all agreed in, Jesus Christ is precious to them. The upright here are
the same with the virgins, v. 3. All that remember his love more than
wine will love him with a superlative love. Nor is any love acceptable
to Christ but the love of the upright, love in sincerity, Eph. 6:24.

`II.` To the daughters of Jerusalem, v. 5, 6. The church in general,
being in distress, speaks to particular churches to guard them against
the danger they were in of being offended at the church\'s sufferings, 1
Th. 3:3. Or the believer speaks to those that were professors at large
in the church, but not of it, or to weak Christians, babes in Christ,
that labour under much ignorance, infirmity, and mistake, not perfectly
instructed, and yet willing to be taught in the things of God. She
observed these by-standers look disdainfully upon her because of her
blackness, in respect both of sins and sufferings, upon the account of
which they though she had little reason to expect the kisses she wished
for (v. 2) or to expect that they should join with her in her joys, v.
4. She therefore endeavors to remove this offence; she owns she is
black. Guilt blackens; the heresies, scandals, and offences, that happen
in the church, make her black; and the best saints have their failings.
Sorrow blackens; that seems to be especially meant; the church is often
in a low condition, mean, and poor, and in appearance despicable, her
beauty sullied and her face foul with weeping; she is in mourning weeds,
clothed with sackcloth, as the Nazarites that had become blacker than a
coal, Lam. 4:8. Now, to take off this offence,

`1.` She asserts her own comeliness notwithstanding (v. 5): I am black,
but comely, black as the tents of Kedar, in which the shepherds lived,
which were very coarse, and never whitened, weather-beaten and
discoloured by long use, but comely as the curtains of Solomon, the
furniture of whose rooms, no doubt, was sumptuous and rich, in
proportion to the stateliness of his houses. The church is sometimes
black with persecution, but comely in patience, constancy, and
consolation, and never the less amiable in the eyes of Christ, black in
the account of men, but comely in God\'s esteem, black in some that are
a scandal to her, but comely in others that are sincere and are an
honour to her. True believers are black in themselves, but comely in
Christ, with the comeliness that he puts upon them, black outwardly, for
the world knows them not, but all glorious within, Ps. 45:13. St. Paul
was weak, and yet strong, 2 Co. 12:10. And so the church is black and
yet comely; a believer is a sinner and yet a saint; his own
righteousnesses are as filthy rags, but he is clothed with the robe of
Christ\'s righteousness. The Chaldee Paraphrase applies it to the people
of Israel\'s blackness when they made the golden calf and their
comeliness when they repented of it.

`2.` She gives an account how she came to be so black. The blackness was
not natural, but contracted, and was owing to the hard usage that had
been given her: Look not upon me so scornfully because I am black. We
must take heed with what eye we look upon the church, especially when
she is in black. Thou shouldst not have looked upon the day of thy
brother, the day of his affliction, Obad. 12. Be not offended; for,

`(1.)` I am black by reason of my sufferings: The sun has looked upon me.
She was fair and comely; whiteness was her proper colour; but she got
this blackness by the burden and heat of the day, which she was forced
to bear. She was sun-burnt, scorched with tribulation and persecution
(Mt. 13:6, 21); and the greatest beauties, if exposed to the weather,
are soonest tanned. Observe how she mitigates her troubles; she does not
say, as Jacob (Gen. 31:40), In the day the drought consumed me, but, The
sun has looked upon me; for it becomes not God\'s suffering people to
make the worst of their sufferings. But what was the matter? `[1.]` She
fell under the displeasure of those of her own house: My mother\'s
children were angry with me. She was in perils by false brethren; her
foes were those of her own house (Mt. 10:36), brethren by nature as men,
by profession as members of the same sacred corporation, the children of
the church her mother, but not of God her Father; they were angry with
her. The Samaritans, who claimed kindred to the Jews, were vexed at any
thing that tended to the prosperity of Jerusalem, Neh. 2:10. Note, It is
no new thing for the people of God to fall under the anger of their own
mother\'s children. It was thou, a man, my equal, Ps. 55:12, 13. This
makes the trouble the more irksome and grievous; from such it is taken
unkindly, and the anger of such is implacable. A brother offended is
hard to be won. `[2.]` They dealt very hardly with her: They made me the
keeper of the vineyards, that is, First, \"They seduced me to sin, drew
me into false worships, to serve their gods, which was like dressing the
vineyards, keeping the vine of Sodom; and they would not let me keep my
own vineyard, serve my own God, and observe those pure worships which he
gave me in charge, and which I do and ever will own for mine.\" These
are grievances which good people complain most of in a time of
persecution, that their consciences are forced, and that those who rule
them with rigour say to their souls, Bow down, that we may go over, Isa.
51:23. Or, Secondly, \"They brought me into trouble, imposed that upon
me which was toilsome, and burdensome, and very disgraceful.\" Keeping
the vineyards was base servile work, and very laborious, Isa. 61:5. Her
mother\'s children made her the drudge of the family. Cursed be their
anger, for it was fierce, and their wrath, for it was cruel. The spouse
of Christ has met with a great deal of hard usage.

`(2.)` \"My sufferings are such as I have deserved; for my own vineyard
have I not kept. How unrighteous soever my brethren are in persecuting
me, God is righteous in permitting them to do so. I am justly made a
slavish keeper of men\'s vineyards, because I have been a careless
keeper of the vineyards God has entrusted me with.\" Slothful servants
of God are justly made to serve their enemies, that they may know his
service, and the service of the kings of the countries, 2 Chr. 12:8;
Deu. 28:47, 48; Eze. 20:23, 24. \"Think not the worse of the ways of God
for my sufferings, for I smart for my own folly.\" Note, When God\'s
people are oppressed and persecuted it becomes them to acknowledge their
own sin to be the procuring cause of their troubles, especially their
carelessness in keeping their vineyards, so that it has been like the
field of the slothful.

### Verses 7-11

Here is, `I.` The humble petition which the spouse presents to her
beloved, the shepherdess to the shepherd, the church and every believer
to Christ, for a more free and intimate communion with him. She turns
from the daughters of Jerusalem, to whom she had complained both of her
sins and of her troubles, and looks up to heaven for relief and succour
against both, v. 7. Here observe, 1. The title she gives to Christ: O
thou whom my soul loveth. Note, It is the undoubted character of all
true believers that their souls love Jesus Christ, which intimates both
the sincerity and the strength of their love; they love him with all
their hearts; and those that do so may come to him boldly and may humbly
plead it with him. 2. The opinion she has of him as the good shepherd of
the sheep; she doubts not but he feeds his flock and makes them rest at
noon. Jesus Christ graciously provides both repast and repose for his
sheep; they are not starved, but well fed, not scattered upon the
mountains, but fed together, fed in green pastures and in the hot time
of the day led by the still waters and made to lie down under a cool
refreshing shade. Is it with God\'s people a noon-time of outward
troubles, inward conflicts? Christ has rest for them; he carries them in
his arms, Isa. 40:11. 3. Her request to him that she might be admitted
into his society: Tell me where thou feedest. Those that would be told,
that would be taught, what they are concerned to know and do, must apply
to Jesus Christ, and beg of him to teach them, to tell them. \"Tell me
where to find thee, where I may have conversation with thee, where thou
feedest and tendest thy flock, that there I may have some of my
company.\" Observe, by the way, We should not, in love to our friends
and their company, tempt them or urge them to neglect their business,
but desire such an enjoyment of them as will consist with it, and
rather, if we can, to join with them in their business and help to
forward it. \"Tell me where thou feedest, and there I will sit with
thee, walk with thee, feed my flocks with thine, and not hinder thee nor
myself, but bring my work with me.\" Note, Those whose souls love Jesus
Christ earnestly desire to have communion with him, by his word in which
he speaks to us and by prayer in which we speak to him, and to share in
the privileges of his flock; and we may learn from the care he takes of
his church, to provide convenient food and rest for it, how to take care
of our own souls, which are our charge. 4. The plea she uses for the
enforcing of this request: \"For why should I be as one that turns aside
by (or after) the flocks of thy companions, that pretend to be so, but
are really thy competitors, and rivals with thee.\" Note, Turning aside
from Christ after other lovers is that which gracious souls dread, and
deprecate, more than any thing else. \"Thou wouldst not have me to turn
aside, no, nor to be as one that turns aside; tell me then, O tell me,
where I may be near thee, and I will never leave thee.\" `(1.)` \"Why
should I lie under suspicion, and look as if I belonged to some other
and not to thee? Why should I be thought by the flocks of our companions
to be a deserter from thee, and a retainer to some other shepherd?\"
Good Christians will be afraid of giving any occasion to those about
them to question their faith in Christ and their love to him; they would
not do any thing that looks like unconcernedness about their souls; or
uncharitableness towards their brethren, or that savours of indifference
and disaffection to holy ordinances; and we should pray to God to direct
us into and keep us in the way of our duty, that we may not so much as
seem to come short, Heb. 4:1. `(2.)` \"Why should I lie in temptation to
turn aside, as I do while I am absent from thee?\" We should be earnest
with God for a settled peace in communion with God through Christ, that
we may not be as waifs and strays, ready to be picked up by him that
next passes by.

`II.` The gracious answer which the bridegroom gives to this request, v.
8. See how ready God is to answer prayer, especially prayers for
instruction; even while she is yet speaking, he hears. Observe, 1. How
affectionately he speaks to her: O thou fairest among women! Note,
Believing souls are fair, in the eyes of the Lord Jesus, above any
other. Christ sees a beauty in holiness, whether we do or no. The spouse
has called herself black, but Christ calls her fair. Those that are low
in their own eyes are so much the more amiable in the eyes of Jesus
Christ. Blushing at their own deformity (says Mr. Durham) is a chief
part of their beauty. 2. How mildly he checks her for her ignorance, in
these words, If thou know not, intimating that she might have known it
if it had not been her own fault. What! dost thou not know where to find
me and my flock? Compare Christ\'s answer to a like address of Philip\'s
(Jn. 14:9), Have I been so long time with you, and yet hast thou not
known me, Philip? But, 3. With what tenderness he acquaints her where
she might find him. If men say, Lo, here is Christ, or, Lo, he is there,
believe them not, go not after them, Mt. 24:23, 26. But, `(1.)` Walk in
the way of good men (Prov. 2:20), follow the track, ask for the good old
way, observe the footsteps of the flock, and go forth by them. It will
not serve to sit still and cry, \"Lord, show me the way,\" but we must
bestir ourselves to enquire out the way; and we may find it by looking
which way the footsteps of the flock lead, what has been the practice of
godly people all along; let that practice be ours, Heb. 6:12; 1 Co.
11:1. `(2.)` Sit under the direction of good ministers: \"Feed thyself and
thy kids besides the tents of the under-shepherds. Bring thy charge with
thee\" (it is probable that the custom was to commit the lambs and kids
to the custody of the women, the shepherdesses); \"they shall all be
welcome; the shepherds will be no hindrance to thee, as they were to
Reuel\'s daughters (Ex. 2:17), but helpers rather, and therefore abide
by their tents.\" Note, Those that would have acquaintance and communion
with Christ must closely and conscientiously adhere to holy ordinances,
must join themselves to his people and attend his ministers. Those that
have the charge of families must bring them with them to religious
assemblies; let their kids, their children, their servants, have the
benefit of the shepherds\' tents.

`III.` The high encomiums which the bridegroom gives of his spouse. To be
given in marriage, in the Hebrew dialect, is to be praised (Ps. 78:63,
margin), so this spouse is here; her husband praises this virtuous woman
(Prov. 31:28); he praises her, as is usual in poems, by similitudes. 1.
He calls her his love (v. 9); it is an endearing compellation often used
in this book: \"My friend, my companion, my familiar.\" 2. He compares
her to a set of strong and stately horses in Pharaoh\'s chariots. Egypt
was famous for the best horses. Solomon had his thence; and Pharaoh, no
doubt, had the choicest the country afforded for his own chariots. The
church had complained of her own weakness, and the danger she was in of
being made a prey of by her enemies: \"Fear not,\" says Christ; \"I have
made thee like a company of horses; I have put strength into thee as I
have done into the horse (Job 39:19), so that thou shalt with a gracious
boldness mock at fear, and not be affrighted, like the lion, Prov. 28:1.
The Lord has made thee as his goodly horse in the day of battle, Zec.
10:3. I have compared thee to my company of horses which triumphed over
Pharaoh\'s chariots, the holy angels, horses of fire.\" Hab. 3:15, Thou
didst walk through the sea with thy horses; and see Isa. 63:13. We are
weak in ourselves, but if Christ make us as horses, strong and bold, we
need not fear what all the powers of darkness can do against us. 3. He
admires the beauty and ornaments of her countenance (v. 10): Thy cheeks
are comely with rows of jewels, the attire of the head, curls of hair,
or favourites (so some), or knots of ribbons; thy neck also with chains,
such as persons of the first rank wear, chains of gold. The ordinances
of Christ are the ornaments of the church. The graces, gifts, and
comforts of the Spirit, are the adorning of every believing soul, and
beautify it; these render it, in the sight of God, of great price. The
ornaments of the saints are many, but all orderly disposed in rows and
chains, in which there is a mutual connexion with and dependence upon
each other. The beauty is not from any thing in themselves, from the
neck or from the cheeks, but from ornaments with which they are set off.
It was comeliness which I put upon thee, said the Lord God; for we were
born not only naked, but polluted, Eze. 16:14.

`IV.` His gracious purpose to add to her ornaments; for where God has
given true grace he will give more grace; to him that has shall be
given. Is the church courageous in her resistance of sin, as the horses
in Pharaoh\'s chariots? Is she comely in the exercise of grace, as with
rows of jewels and chains of gold? She shall be yet further beautified
(v. 11): We will make thee borders of gold, inlaid, or enamelled, with
studs of silver. Whatever is wanting shall be made up, till the church
and every true believer come to be perfect in beauty; see Eze. 16:14.
This is here undertaken to be done by the concurring power of the three
persons in the Godhead: We will do it; like that (Gen. 1:26), \"Let us
make man; so let us new-make him, and perfect his beauty.\" The same
that is the author will be the finisher of the good work; and it cannot
miscarry.

### Verses 12-17

Here the conference is carried on between Christ and his spouse, and
endearments are mutually exchanged.

`I.` Believers take a great complacency in Christ, and in communion with
him. To you that believe he is precious, above any thing in this world,
1 Pt. 2:7. Observe,

`1.` The humble reverence believers have for Christ as their Sovereign,
v. 12. He is a King in respect both of dignity and dominion; he wears
the crown of honour, he bears the sceptre of power, both which are the
unspeakable satisfaction of all his people. This King has his royal
table spread in the gospel, in which is made for all nations a feast of
fat things, Isa. 25:6. Wisdom has furnished her table, Prov. 9:1. He
sits at this table to see his guests (Mt. 22:11), to see that nothing be
wanting that is fit for them; he sups with them and they with him (Rev.
3:20); he has fellowship with them and rejoices in them; he sits at his
table to bid them welcome, and to carve for them, as Christ broke the
five loaves and gave to his disciples, that they might distribute to the
multitude. He sits there to receive petitions, as Ahasuerus admitted
Esther\'s petition at the banquet of wine. He has promised to be present
with his people in his ordinances always. Then believers do him all the
honour they can, and study how to express their esteem of him and
gratitude to him, as Mary did when she anointed his head with the
ointment of spikenard that was very costly, one pound of it worth three
hundred pence, and so fragrant that the house was filled with the
pleasing odour of it (Jn. 12:3), which story seems as if it were
designed to refer to this passage, for Christ was then sitting at table.
When good Christians, in any religious duty, especially in the ordinance
of the Lord\'s supper, where the King is pleased, as it were, to sit
with us at his own table, have their graces exercised, their hearts
broken by repentance, healed by faith, and inflamed with holy love and
desires toward Christ, with joyful expectations of the glory to be
revealed, then the spikenard sends forth the smell thereof. Christ is
pleased to reckon himself honoured by it, and to accept of it as an
instance of respect to him, as it was in the wise men of the east, who
paid their homage to the new-born King of the Jews by presenting to him
frankincense and myrrh. The graces of God\'s Spirit in the hearts of
believers are exceedingly precious in themselves and pleasing to Christ,
and his presence in ordinances draws them out into act and exercise. If
he withdraw, graces wither and languish, as plants in the absence of the
sun; if he approach, the face of the soul is renewed, as of the earth in
the spring; and then it is time to bestir ourselves, that we may not
lose the gleam, not lose the gale; for nothing is done acceptably but
what grace does, Heb. 12:28.

`2.` The strong affection they have for Christ as their beloved, their
well-beloved, v. 13. Christ is not only beloved by all believing souls,
but is their well-beloved, their best-beloved, their only beloved; he
has that place in their hearts which no rival can be admitted to, the
innermost and uppermost place. Observe, `(1.)` How Christ is accounted of
by all believers: He is a bundle of myrrh and a cluster of camphire,
something, we may be sure, nay, every thing, that is pleasant and
delightful. The doctrine of his gospel, and the comforts of his Spirit,
are very refreshing to them, and they rest in his love; none of all the
delights of sense are comparable to the spiritual pleasure they have in
meditating on Christ and enjoying him. There is a complicated sweetness
in Christ and an abundance of it; there is a bundle of myrrh and a
cluster of camphire. We are not straitened in him whom there is all
fulness. The word translated camphire is copher, the same word that
signifies atonement or propitiation. Christ is a cluster of merit and
righteousness to all believers; therefore he is dear to them because he
is the propitiation for their sins. Observe what stress the spouse lays
upon the application: He is unto me, and again unto me, all that is
sweet; whatever he is to others, he is so to me. He loved me, and gave
himself for me. He is my Lord, and my God. `(2.)` How he is accepted: He
shall lie all night between my breasts, near my heart. Christ lays the
beloved disciples in his bosom; why then should not they lay their
beloved Saviour in their bosoms? Why should not they embrace him with
both arms, and hold him fast, with a resolution never to let him go?
Christ must dwell in the heart (Eph. 3:17), and, in order to that, the
adulteries must be put from between the breasts (Hos. 2:2), no pretender
must have his place in the soul. He shall be as a bundle of myrrh, or
perfume bag, between my breasts, always sweet to me; or his effigies in
miniature, his love-tokens, shall be hung between my breasts, according
to the custom of those that are dear to each other. He shall not only be
laid their for a while, but shall lie there, shall abide there.

`II.` Jesus Christ has a great complacency in his church and in every
true believer; they are amiable in his eyes (v. 15): Behold, thou art
fair, my love; and again, Behold, thou art fair. He says this, not to
make her proud (humility is one principal ingredient in spiritual
beauty), but, 1. To show that there is a real beauty in holiness, that
all who are sanctified are thereby beautified; they are truly fair. 2.
That he takes great delight in that good work which his grace has
wrought on the souls of believers; so that though they have their
infirmities, whatever they think of themselves, and the world thinks of
them, he thinks them fair. He calls them friends. The hidden man of the
heart, in that which is not corruptible, is in the sight of God of great
price, 1 Pt. 3:4. 3. To comfort weak believers, who are discouraged by
their own blackness; let them be told again and again that they are
fair. 4. To engage all who are sanctified to be very thankful for that
grace which has made them fair, who by nature were deformed, and changed
the Ethiopian\'s skin. One instance of the beauty of the spouse is here
mentioned, that she has doves\' eyes, as ch. 4:1. Those are fair, in
Christ\'s account, who have, not the piercing eye of the eagle, but the
pure and chaste eye of the dove, not like the hawk, who, when he soars
upwards, still has his eye upon the prey on earth, but a humble modest
eye, such an eye as discovers a simplicity and godly sincerity and a
dove-like innocency, eyes enlightened and guided by the Holy Spirit,
that blessed Dove, weeping eyes. I did mourn as a dove, Eze. 7:16.

`III.` The church expresses her value for Christ, and returns esteem (v.
16): Behold, thou art fair. See how Christ and believers praise one
another. Israel saith of God, Who is like thee? Ex. 15:11. And God saith
of Israel, Who is like thee? Deu. 33:29. Lord, saith the church, \"Dost
thou call me fair? No; if we speak of strength, thou art strong (Job
9:19), so, if of beauty, thou art fair. I am fair no otherwise than as I
have thy image stamped upon me. Thou art the great Original; I am but a
faint and imperfect copy, I am but thy umbra-the shadow of thee, Jn.
1:16; 3:34. Thou art fair in thyself and (which is more) pleasant to all
that are thine. Many are fair enough to look at, and yet the sourness of
their temper renders them unpleasant; but thou art fair, yea,
pleasant.\" Christ is pleasant, as he is ours, in covenant with us, in
relation to us. \"Thou art pleasant now, when the King sits at his
table.\" Christ is always precious to believers, but in a special manner
pleasant when they are admitted into communion with him, when they hear
his voice, and see his face, and taste his love. It is good to be here.
Having expressed her esteem of her husband\'s person, she next, like a
loving spouse, that is transported with joy for having disposed of
herself so well, applauds the accommodations he had for her
entertainment, his bed, his house, his rafters or galleries (v. 16),
which may be fitly applied to those holy ordinances in which believers
have fellowship with Jesus Christ, receive the tokens of his love and
return their pious and devout affections to him, increase their
acquaintance with him and improve their advantages by him. Now, 1. These
she calls ours, Christ and believers having a joint-interest in them. As
husband and wife are heirs together (1 Pt. 3:7), so believers are
joint-heirs with Christ, Rom. 8:17. They are his institutions and their
privileges; in them Christ and believers meet. She does not call them
mine, for a believer will own nothing as his but what Christ shall have
an interest in, nor thine, for Christ has said, All that I have is
thine, Lu. 15:31. All is ours if we are Christ\'s. Those that can by
faith lay claim to Christ may lay claim to all that is his. 2. These are
the best of the kind. Does the colour of the bed, and the furniture
belonging to it, help to set it off? Our bed is green, a colour which,
in a pastoral, is preferred before any other, because it is the colour
of the fields and groves where the shepherd\'s business and delight are.
It is a refreshing colour, good for the eyes; and it denotes
fruitfulness. I am like a green olive-tree, Ps. 52:8. We are married to
Christ, that we should bring forth unto God, Rom. 7:4. The beams of our
house are cedar (v. 17), which probably refers to the temple Solomon had
lately built for communion between God and Israel, which was of cedar, a
strong sort of wood, sweet, durable, and which will never rot, typifying
the firmness and continuance of the church, the gospel-temple. The
galleries for walking are of fir, or cypress, some sort of wood that was
pleasing both to the sight and to the smell, intimating the delight
which the saints take in walking with Christ and conversing with him.
Every thing in the covenant of grace (on which foot all their treaties
are carried on) is very firm, very fine, and very fragrant.
