Song of Solomon, Chapter 6
==========================

Commentary
----------

In this chapter, `I.` The daughters of Jerusalem, moved with the
description which the church had given of Christ, enquire after him (v.
1). `II.` The church directs them where they may meet with him (v. 2, 3).
III. Christ is now found of those that sought him, and very highly
applauds the beauty of his spouse, as one extremely smitten with it (v.
4-7), preferring her before all others (v. 8, 9), recommending her to
the love and esteem of all her neighbours (v. 10), and, lastly,
acknowledging the impressions which her beauty had made upon him and the
great delight he took in it (v. 11-13).

### Verses 1-3

Here is, `I.` The enquiry which the daughters of Jerusalem made concerning
Christ, v. 1. They still continue their high thoughts of the church, and
call her, as before, the fairest among women; for true sanctity is true
beauty. And now they raise their thoughts higher concerning Christ:
Whither has thy beloved gone, that we may seek him with thee? This would
be but an indecent, unacceptable, compliment, if the song were not to be
understood spiritually; for love is jealous of a rival, would monopolize
the beloved, and cares not that others should join in seeking him; but
those that truly love Christ are desirous that others should love him
too, and be joined to him; nay, the greatest instance of duty and
respect that the church\'s children can show to their mother is to join
with her in seeking Christ. The daughters of Jerusalem, who had asked
(ch. 5:9), What is thy beloved more than another beloved? wondering that
the spouse should be so passionately in love with him, are now of
another mind, and are themselves in love with him; for, 1. The spouse
had described him, and shown them his excellencies and perfections; and
therefore, though they have not seen him, yet, believing, they love him.
Those that undervalue Christ do so because they do not know him; when
God, by his word and Spirit, discovers him to the soul, with that ray of
light the fire of love to him will be kindled. 2. The spouse had
expressed her own love to him, her rest in that love, and triumphed in
it: This is my beloved; and that flame in her breast scattered sparks
into theirs. As sinful lusts, when they break out, defile many, so the
pious zeal of some may provoke many, 2 Co. 9:2. 3. The spouse had
bespoken their help in seeking her beloved (ch. 5:8); but now they beg
hers, for they perceive that now the cloud she had been under began to
scatter, and the sky to clear up, and, while she was describing her
beloved to them, she herself retrieved her comfort in him. Drooping
Christians would find benefit themselves by talking of Christ, as well
as do good to others. Now here, `(1.)` They enquire concerning him,
\"Wither has thy beloved gone? which may must we steer our course in
pursuit of him?\" Note, Those that are made acquainted with the
excellencies of Christ, and the comfort of an interest in him, cannot
but be inquisitive after him and desirous to know where they may meet
with him. `(2.)` They offer their service to the spouse to accompany her
in quest of him: We will seek him with thee. Those that would find
Christ must seek him, seek him early, seek him diligently; and it is
best seeking Christ in concert, to join with those that are seeking him.
We must seek for communion with Christ in communion with saints. We know
whither our beloved has gone; he has gone to heaven, to his Father, and
our Father. He took care to send us notice of it, that we might know how
to direct to him, Jn. 20:17. We must by faith see him there, and by
prayer seek him there, with boldness enter into the holiest, and herein
must join with the generation of those that seek him (Ps. 24:6), even
with all that in every place call upon him, 1 Co. 1:2. We must pray with
and for others.

`II.` The answer which the spouse gave to this enquiry, v. 2, 3. Now she
complains not any more, as she had done (ch. 5:6), \"He is gone, he is
gone,\" that she knew not where to find him, or doubted she had lost him
for ever; no,

`1.` Now she knows very well where he is (v. 2): \"My beloved is not to
be found in the streets of the city, and the crowd and noise that are
there; there I have in vain looked for him\" (as his parents sought him
among their kindred and acquaintance, and found him not); \"but he has
gone down to his garden, a place of privacy and retirement.\" The more
we withdraw from the hurry of the world the more likely we are to have
acquaintance with Christ, who took his disciples into a garden, there to
be witnesses of the agonies of his love. Christ\'s church is a garden
enclosed, and separated from the open common of the world; it is his
garden, which he has planted as he did the garden of Eden, which he
takes care of, and delights in. Though he had gone up to the paradise
above, yet he comes down to his garden on earth; it lies low, but he
condescends to visit it, and wonderful condescension it is. Will God in
very deed dwell with man upon the earth? Those that would find Christ
may expect to meet with him in his garden the church, for there he
records his name (Ex. 20:24); they must attend upon him in the
ordinances which he has instituted, the word, sacraments, and prayer,
wherein he will be with us always, even to the end of the world. The
spouse here refers to what Christ had said (ch. 5:1), I have come into
my garden. It is as if she had said, \"What a fool was I to fret and
fatigue myself in seeking him where he was not, when he himself had told
me where he was!\" Words of direction and comfort are often out of the
way when we have occasion to use them, till the blessed Spirit brings
them to our remembrance, and then we wonder how we overlooked them.
Christ has told us that he would come into his garden; thither therefore
we must go to seek him. The beds, and smaller gardens, in this greater,
are the particular churches, the synagogues of God in the land (Ps.
84:8); the spices and lilies are particular believers, the planting of
the Lord, and pleasant in his eyes. When Christ comes down to his church
it is, `(1.)` To feed among the gardens, to feed his flock, which he feeds
not, as other shepherds, in the open fields, but in his garden, so well
are they provided for, Ps. 23:2. He comes to feed his friends, and
entertain them; there you may not only find him, but find his table
richly furnished, and a hearty welcome to it. He comes to feed himself,
that is, to please himself with the products of his own grace in his
people; for the Lord takes pleasure in those that fear him. He has many
gardens, many particular churches of different sizes and shapes; but,
while they are his, he feeds in them all, manifests himself among them,
and is well pleased with them. `(2.)` To gather lilies, wherewith he is
pleased to entertain and adorn himself. He picks the lilies one by one,
and gathers them to himself; and there will be a general harvest of them
at the great day, when he will send forth his angels, to gather all his
lilies, that he may be for ever glorified and admired in them.

`2.` She is very confident of her own interest in him (v. 3): \"I am my
beloved\'s, and my beloved is mine; the relation is mutual, and the knot
is tied, which cannot be loosed; for he feeds among the lilies, and my
communion with him is a certain token of my interest in him.\" She had
said this before (ch. 2:16); but, `(1.)` Here she repeats it as that which
she resolved to abide by, and which she took an unspeakable pleasure and
satisfaction in; she liked her choice too well to change. Our communion
with God is very much maintained and kept up by the frequent renewing of
our covenant with him and rejoicing in it. `(2.)` She had occasion to
repeat it, for she had acted unkindly to her beloved, and, for her so
doing, he had justly withdrawn himself from her, and therefore there was
occasion to take fresh hold of the covenant, which continues firm
between Christ and believes, notwithstanding their failings and his
frowns, Ps. 89:30-35. \"I have been careless and wanting in my duty, and
yet I am my beloved\'s;\" for every transgression in the covenant does
not throw us out of covenant. \"He has justly hidden his face from me
and denied me his comforts, and yet my beloved is mine;\" for rebukes
and chastenings are not only consistent with, but they flow from
covenant-love. `(3.)` When we have not a full assurance of Christ\'s love
we must live by a faithful adherence to him. \"Though I have not the
sensible consolation I used to have, yet I will cleave to this, Christ
is mine and I am his.\" `(4.)` Though she had said the same before, yet
now she inverts the order, and asserts her interest in her first: I am
my beloved\'s, entirely devoted and dedicated to him; and then her
interest in him and in his grace: \"My beloved is mine, and I am happy,
truly happy in him.\" If our own hearts can but witness for us that we
are his, there is no room left to question his being ours; for the
covenant never breaks on his side. `(5.)` It is now her comfort, as it was
then, that he feeds among the lilies, that he takes delight in his
people and converses freely with them, as we do with those with whom we
feed; and therefore, though at present he be withdrawn, \"I shall meet
with him again. I shall yet praise him who is the health of my
countenance, and my God.\"

### Verses 4-10

Now we must suppose Christ graciously returned to his spouse, from whom
he had withdrawn himself, returned to converse with her (for he speaks
to her and makes her to hear joy and gladness), returned to favour her,
having forgiven and forgotten all her unkindness, for he speaks very
tenderly and respectfully to her.

`I.` He pronounces her truly amiable (v. 4): Thou art beautiful, O my
love! as Tirzah, a city in the tribe of Manasseh, whose name signifies
pleasant, or acceptable, the situation, no doubt, being very happy and
the building fine and uniform. Thou art comely as Jerusalem, a city
compact together (Ps. 122:3), and which Solomon had built and
beautified, the joy of the whole earth; it was an honour to the world
(whether they thought so or no) that there was such a city in it. It was
the holy city, and that was the greatest beauty of it; and fitly is the
church compared to it, for it was figured and typified by it. The
gospel-church is the Jerusalem that is above (Gal. 4:26), the heavenly
Jerusalem (Heb. 12:22); in it God has his sanctuary, and is, in a
special manner, present; thence he has the tribute of praise issuing; it
is his rest for ever, and therefore it is comely as Jerusalem, and,
being so, is terrible as an army with banners. Church-censures, duly
administered, strike an awe upon men\'s consciences; the word (the
weapons of her warfare) casts down imaginations (2 Co. 10:5), and even
an unbeliever is convinced and judged by the solemnity of holy
ordinances, 1 Co. 14:24, 25. The saints by faith overcome the world (1
Jn. 5:4); nay, like Jacob, they have power with God and prevail, Gen.
32:28.

`II.` He owns himself in love with her, v. 5. Though, for a small moment,
and in a little wrath, he had hid his face from her, yet now he gathers
her with very surprising instances of everlasting lovingkindness, Isa.
54:8. Turn thy eyes towards me (so some read it), \"turn the eyes of
faith and love towards me, for they have lifted me up; look unto me, and
be comforted.\" When we are calling to God to turn the eye of his favour
towards us he is calling to us to turn the eye of our obedience towards
him. We read it as a strange expression of love, \"Turn away thy eyes
from me, for I cannot bear the brightness of them; they have quite
overcome me, and I am prevailed with to overlook all that is past;\" as
God said to Moses, when he interceded for Israel, \"Let me alone, or I
must yield,\" Ex. 32:10. Christ is pleased to borrow these expressions
of a passionate lover only to express the tenderness of a compassionate
Redeemer, and the delight he takes in his redeemed and in the workings
of his own grace in them.

`III.` He repeats, almost word for word, part of the description he had
given of her beauty (ch. 4:1-3), her hair, her teeth, her temples (v.
5-7), not because he could not have described it in other words, and by
other similitudes, but to show that he had still the same esteem for her
since her unkindness to him, and his withdrawings from her, that he had
before. Lest she should think that, though he would not quite cast her
off, yet he would think the worse of her while he knew her, he says the
same of her now that he had done; for those to whom much is forgiven
will love the more, and, consequently, will be the more loved, for
Christ has said, I love those that love me. He is pleased with his
people, notwithstanding their weaknesses, when they sincerely repent of
them and return to their duty, and commends them as if they had already
arrived at perfection.

`IV.` He prefers her before all competitors, and sees all the beauties
and perfections of others meeting and centering in her (v. 8, 9):
\"There are, it may be, threescore queens, who, like Esther, have by
their beauty attained to the royal state and dignity, and fourscore
concubines, whom kings have preferred before their own queens, as more
charming, and these attended by their maids of honour, virgins without
number, who, when there is a ball at court, appear in great splendour,
with beauty that dazzles the eyes of the spectators; but my dove, my
undefiled, is but one, a holy one.\" 1. She excels them all. Go through
all the world, and view the societies of men that reckon themselves wise
and happy, kingdoms, courts, senates, councils, or whatever
incorporations you may think valuable, they are none of them to be
compared with the church of Christ; their honours and beauties are
nothing to hers. Who is like unto thee, O Israel! Deu. 33:29; 4:6, 7.
There are particular persons, as virgins without number, who are famed
for their accomplishments, the beauties of their address, language, and
performances, but the beauty of holiness is beyond all other beauty:
\"My dove, my undefiled, is one, has that one beauty that she is a dove,
an undefiled dove, and mine, and that makes her excel the queens and
virgins, though they were ever so many.\" 2. She included them all.
\"Other kings have many queens, and concubines, and virgins, with whose
conversation they entertain themselves, but my dove, my undefiled, is to
me instead of all; in that one I have more than they have in all
theirs.\" Or, \"Though there are many particular churches, some of
greater dignity, others of less, some of longer, others of shorter,
standing, and many particular believers, of different gifts and
attainments, some more eminent, others less so, yet they all constitute
but one catholic church, are all but parts of that whole, and that is my
dove, my undefiled.\" Christ is the centre of the church\'s unity; all
the children of God that are scattered abroad are gathered by him (Jn.
11:52), and meet in him (Eph. 1:10), and are all his doves.

`V.` He shows how much she was esteemed, not by him only, but by all that
had acquaintance with her and stood in relation to her. It would add to
her praise to say, 1. That she was her mother\'s darling; she had that
in her, from a child, which recommended her to the particular affection
of her parents. As Solomon himself is said to have been tender and an
only one in the sight of his mother (Prov. 4:3), so was she the only one
of her mother, as dear as if she had been an only one, and, if there
were many more, yet she was the choice one of her that bore her, more
excellent than all the societies of men this world ever produced. All
the kingdoms of the world, and the glory of them, are nothing, in
Christ\'s account, compared with the church, which is made up of the
excellent ones of the earth, the precious sons of Zion, comparable to
fine gold, and more excellent than their neighbours. 2. That she was
admired by all her acquaintance, not only the daughters, who were her
juniors, but even the queens and the concubines, who might have reason
to be jealous of her as a rival; they all blessed her, and wished well
to her, praised her, and spoke well of her. The daughters of Jerusalem
called her the fairest among women; all agreed to give her the
pre-eminence for beauty, and every sheaf bowed to hers. Note, `(1.)` Those
that have any correct sense of things cannot but be convinced in their
consciences (whatever they say) that godly people are excellent people;
many will give them their good word, and more their good-will. `(2.)`
Jesus Christ takes notice what people think and speak of his church, and
is well pleased with those that honour such as fear the Lord, and takes
it ill of those that despise them, particularly when they are under a
cloud, that offend any of his little ones.

`VI.` He produces the encomium that was given of her, and makes it his
own (v. 10): Who is she that looks forth as the morning? This is
applicable both to the church in the world and to grace in the heart.

`1.` They are amiable as the light, the most beautiful of all visible
things. Christians are, or should be, the lights of the world. The
patriarchal church looked forth as the morning when the promise of the
Messiah was first made known, and the day-spring from on high visited
this dark world. The Jewish church was fair as the moon; the ceremonial
law was an imperfect light; it shone by reflection; it was changing as
the moon, did not make day, nor had the sun of righteousness yet risen.
But the Christian church is clear as the sun, exhibits a great light to
those that sat in darkness. Or we may apply it to the kingdom of grace,
the gospel-kingdom. `(1.)` In its rise, it looks forth as the morning
after a dark night; it is discovering (Job 38:12, 13), and very
acceptable, looks forth pleasantly as a clear morning; but it is small
in its beginnings, and scarcely perceptible at first. `(2.)` It is, at the
best, in this world, but fair as the moon, which shines with a borrowed
light, which has her changes and eclipses, and her spots too, and, when
at the full, does but rule by night. But, `(3.)` When it is perfected in
the kingdom of glory then it will be clear as the sun, the church
clothed with the sun, with Christ the sun of righteousness, Rev. 12:1.
Those that love God will then be as the sun when he goes forth in his
strength (Jdg. 5:31; Mt. 13:43); they shall shine in inexpressible
glory, and that which is perfect will then come; there shall be no
darkness, no spots, Isa. 30:26.

`2.` The beauty of the church and of believers is not only amiable, but
awful as an army with banners. The church, in this world, is as an army,
as the camp of Israel in the wilderness; its state is militant; it is in
the midst of enemies, and is engaged in a constant conflict with them.
Believers are soldiers in this army. It has its banners; the gospel of
Christ is an ensign (Isa. 11:12), the love of Christ, ch. 2:4. It is
marshalled, and kept in order and under discipline. It is terrible to
its enemies as Israel in the wilderness was, Ex. 15:14. When Balaam saw
Israel encamped according to their tribes, by their standards, with
colours displayed, he said, How goodly are thy tents, O Jacob! Num.
24:5. When the church preserves her purity she secures her honour and
victory; when she is fair as the moon, and clear as the sun, she is
truly great and formidable.

### Verses 11-13

Christ having now returned to his spouse, and the breach being entirely
made up, and the falling out of these lovers being the renewing of love,
Christ here gives an account both of the distance and of the
reconciliation.

`I.` That when he had withdrawn from his church as his spouse, and did not
comfort her, yet even then he had his eye upon it as his garden, which
he took care of (v. 11): \"I went down into the garden of nuts, or
nutmegs, to see the fruits of the valley, with complacency and concern,
to see them as my own.\" When he was out of sight he was no further off
than the garden, hid among the trees of the garden, in a low and dark
valley; but then he was observing how the vine flourished, that he might
do all that to it which was necessary to promote its flourishing, and
might delight himself in it as a man does in a fruitful garden. He went
to see whether the pomegranates budded. Christ observes the first
beginnings of the good work of grace in the soul and the early buddings
of devout affections and inclinations there, and is well pleased with
them, as we are with the blossoms of the spring.

`II.` That yet he could not long content himself with this, but suddenly
felt a powerful, irresistible, inclination in his own bosom to return to
his church, as his spouse, being moved with her lamentations after him,
and her languishing desire towards him (v. 12): \"Or ever I was aware,
my soul made me like the chariots of Ammi-nadib; I could not any longer
keep at a distance; my repentings were kindled together, and I presently
resolved to fly back to the arms of my love, my dove.\" Thus Joseph made
himself strange to his brethren, for a while, to chastise them for their
former unkindnesses, and make trial of their present temper, till he
could no longer refrain himself, but, or ever he was aware, burst out
into tears, and said, I am Joseph, Gen. 45:1, 3. And now the spouse
perceives, as David did (Ps. 31:22), that though she said in her haste,
I am cut off from before thy eyes, yet, at the same time, he heard the
voice of her supplications, and became like the chariots of Ammi-nadib,
which were noted for their beauty and swiftness. My soul put me into the
chariots of my willing people (so some read it), \"the chariots of their
faith, and hope, and love, their desires, and prayers, and expectations,
which they sent after me, to fetch me back, as chariots of fire with
horses of fire.\" Note, 1. Christ\'s people are, and ought to be, a
willing people. 2. If they continue seeking Christ and longing after
him, even when he seems to withdraw from them, he will graciously return
to them in due time, perhaps sooner than they think and with a pleasing
surprise. No chariots sent for Christ shall return empty. 3. All
Christ\'s gracious returns to his people take rise from himself. It is
not they, it is his own soul, that puts him into the chariots of his
people; for he is gracious because he will be gracious, and loves his
Israel because he would love them; not for their sakes, be it known to
them.

`III.` That he, having returned to her, kindly courted her return to him,
notwithstanding the discouragements she laboured under. Let her not
despair of obtaining as much comfort as ever she had before this
distance happened, but take the comfort of the return of her beloved, v.
13. Here, 1. The church is called Shulamite, referring either to
Solomon, the bridegroom in type, by whose name she is called, in token
of her relation to him and union with him (thus believers are called
Christians from Christ), or referring to Salem, the place of her birth
and residence, as the woman of Shunem is called the Shunamite. Heaven is
the Salem whence the saints have their birth, and where they have their
citizenship; those that belong to Christ, and are bound for heaven,
shall be called Shulamites. 2. She is invited to return, and the
invitation most earnestly pressed: Return, return; and again, \"Return,
return; recover the peace thou hast lost and forfeited; come back to thy
former composedness and cheerfulness of spirit.\" Note, Good Christians,
after they have had their comfort disturbed, are sometimes hard to be
pacified, and need to be earnestly persuaded to return again to their
rest. As revolting sinners have need to be called to again and again
(Turn you, turn you, why will you die?) so disquieted saints have need
to be called to again and again, Turn you, turn you, why will you droop;
Why art thou cast down, O my soul? 3. Having returned, she is desired to
show her face: That we may look upon thee. Go no longer with they face
covered like a mourner. Let those that have made their peace with God
lift up their faces without spot (Job 22:26); let them come boldly to
his throne of grace. Christ is pleased with the cheerfulness and humble
confidence of his people, and would have them look pleasant. \"Let us
look upon thee, not I only, but the holy angels, who rejoice in the
consolation of saints as well as in the conversion of sinners; not I
only, but all the daughters.\" Christ and believers are pleased with the
beauty of the church. 4. A short account is given of what is to be seen
in her. The question is asked, What will you see in the Shulamite? And
it is answered, As it were the company of two armies. `(1.)` Some think
she gives this account of herself; she is shy of appearing, unwilling to
be looked upon, having, in her own account, no form or comeliness. Alas!
says she, What will you see in the Shulamite? nothing that is worth your
looking upon, nothing but as it were the company of two armies actually
engaged, where nothing is to be seen but blood and slaughter. The
watchmen had smitten her, and wounded her, and she carried in her face
the marks of those wounds, looked as if she had been fighting. She had
said (ch. 1:6), Look not upon me because I am black; here she says,
\"Look not upon me because I am bloody.\" Or it may denote the constant
struggle that is between grace and corruption in the souls of believers;
they are in them as two armies continually skirmishing, which makes her
ashamed to show her face. `(2.)` Others think her beloved gives the
account of her. \"I will tell you what you shall see in the Shulamite;
you shall see as noble a sight as that of two armies, or two parts of
the same army, drawn out in rank and file; not only as an army with
banners, but as two armies, with a majesty double to what was before
spoken; she is as Mahanaim, as the two hosts which Jacob saw (Gen. 32:1,
2), a host of saints and a host of angels ministering to them; the
church militant, the church triumphant.\" Behold two armies; in both the
church appears beautiful.
