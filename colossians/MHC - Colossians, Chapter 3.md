Colossians, Chapter 3
=====================

Commentary
----------

`I.` The apostle exhorts us to set our hearts upon heaven and take them
off from this world (v. 1-4). `II.` He exhorts to the mortification of
sin, in the various instances of it (v. 5-11). `III.` He earnestly presses
to mutual love and compassion (v. 12-17). And concludes with
exhortations to relative duties, of wives and husbands, parents and
children, masters and servants (v. 18-25).

### Verses 1-4

The apostle, having described our privileges by Christ in the former
part of the epistle, and our discharge from the yoke of the ceremonial
law, comes here to press upon us our duty as inferred thence. Though we
are made free from the obligation of the ceremonial law, it does not
therefore follow that we may live as we list. We must walk the more
closely with God in all the instances of evangelical obedience. He
begins with exhorting them to set their hearts on heaven, and take them
off from this world: If you then have risen with Christ. It is our
privilege that we have risen with Christ; that is, have benefit by the
resurrection of Christ, and by virtue of our union and communion with
him are justified and sanctified, and shall be glorified. Hence he
infers that we must seek those things which are above. We must mind the
concerns of another world more than the concerns of this. We must make
heaven our scope and aim, seek the favour of God above, keep up our
communion with the upper world by faith, and hope, and holy love, and
make it our constant care and business to secure our title to and
qualifications for the heavenly bliss. And the reason is because Christ
sits at the right hand of God. He who is our best friend and our head is
advanced to the highest dignity and honour in heaven, and has gone
before to secure to us the heavenly happiness; and therefore we should
seek and secure what he has purchased at so vast an expense, and is
taking so much care about. We must live such a life as Christ lived here
on earth and lives now in heaven, according to our capacities.

`I.` He explains this duty (v. 2): Set your affections on things above,
not on things on the earth. Observe, To seek heavenly things is to set
our affections upon them, to love them and let our desires be towards
them. Upon the wings of affection the heart soars upwards, and is
carried forth towards spiritual and divine objects. We must acquaint
ourselves with them, esteem them above all other things, and lay out
ourselves in preparation for the enjoyment of them. David gave this
proof of his loving the house of God, that he diligently sought after
it, and prepared for it, Ps. 27:4. This is to be spiritually minded
(Rom. 8:6), and to seek and desire a better country, that is, a
heavenly, Heb. 11:14, 16. Things on earth are here set in opposition to
things above. We must not dote upon them, nor expect too much from them,
that we may set our affections on heaven; for heaven and earth are
contrary one to the other, and a supreme regard to both is inconsistent;
and the prevalence of our affection to one will proportionably weaken
and abate our affection to the other.

`II.` He assigns three reasons for this, v. 3, 4.

`1.` That we are dead; that is, to present things, and as our portion. We
are so in profession and obligation; for we are buried with Christ, and
planted into the likeness of his death. Every Christian is crucified
unto the world, and the world is crucified unto him, Gal. 6:14. And if
we are dead to the earth, and have renounced it as our happiness, it is
absurd for us to set our affections upon it, and seek it. We should be
like a dead thing to it, unmoved and unaffected towards it.

`2.` Our true life lies in the other world: You are dead, and your life
is hid with Christ in God, v. 3. The new man has its livelihood thence.
It is born and nourished from above; and the perfection of its life is
reserved for that state. It is hid with Christ; not hid from us only, in
point of secrecy, but hid for us, denoting security. The life of a
Christian is hid with Christ. Because I live you shall live also, Jn.
14:19. Christ is at present a hidden Christ, or one whom we have not
seen; but this is our comfort, that our life is hid with him, and laid
up safely with him. As we have reason to love him whom we have not seen
(1 Pt. 1:8), so we may take the comfort of a happiness out of sight, and
reserved in heaven for us.

`3.` Because at the second coming of Christ we hope for the perfection of
our happiness. If we live a life of Christian purity and devotion now,
when Christ, who is our life, shall appear, we shall also appear with
him in glory, v. 4. Observe, `(1.)` Christ is a believer\'s life. I live,
yet not I, but Christ lives in me, Gal. 2:20. He is the principle and
end of the Christian\'s life. He lives in us by his Spirit, and we live
to him in all we do. To me to live is Christ, Phil. 1:21. `(2.)` Christ
will appear again. He is now hid; and the heavens must contain him; but
he will appear in all the pomp of the upper world, with his holy angels,
and in his own glory and his Father\'s glory, Mk. 8:38; Lu. 9:26. `(3.)`
We shall then appear with him in glory. It will be his glory to have his
redeemed with him; he will come to be glorified in his saints (2 Th.
1:10); and it will be their glory to come with him, and be with him for
ever. At the second coming of Christ there will be a general meeting of
all the saints; and those whose life is now hid with Christ shall then
appear with Christ in that glory which he himself enjoys, Jn. 17:24. Do
we look for such a happiness, and should we not set our affections upon
that world, and live above this? What is there here to make us fond of
it? What is there not there to draw our hearts to it? Our head is there,
our home is there, our treasure is there, and we hope to be there for
ever.

### Verses 5-7

The apostle exhorts the Colossians to the mortification of sin, the
great hindrance to seeking the things which are above. Since it is our
duty to set our affections upon heavenly things, it is our duty to
mortify our members which are upon the earth, and which naturally
incline us to the things of the world: \"Mortify them, that is, subdue
the vicious habits of mind which prevailed in your Gentile state. Kill
them, suppress them, as you do weeds or vermin which spread and destroy
all about them, or as you kill an enemy who fights against you and
wounds you.\"-Your members which are upon the earth; either the members
of the body, which are the earthly part of us, and were curiously
wrought in the lower parts of the earth (Ps. 139:15), or the corrupt
affections of the mind, which lead us to earthly things, the members of
the body of death, Rom. 7:24. He specifies,

`I.` The lusts of the flesh, for which they were before so very
remarkable: Fornication, uncleanness, inordinate affection, evil
concupiscence-the various workings of the carnal appetites and fleshly
impurities, which they indulged in their former course of life, and
which were so contrary to the Christian state and the heavenly hope.

`II.` The love of the world: And covetousness, which is idolatry; that
is, an inordinate love of present good and outward enjoyments, which
proceeds from too high a value in the mind, puts upon too eager a
pursuit, hinders the proper use and enjoyment of them, and creates
anxious fear and immoderate sorrow for the loss of them. Observe,
Covetousness is spiritual idolatry: it is the giving of that love and
regard to worldly wealth which are due to God only, and carries a
greater degree of malignity in it, and is more highly provoking to God,
than is commonly thought. And it is very observable that among all the
instances of sin which good men are recorded in the scripture to have
fallen into (and there is scarcely any but some or other, in one or
other part of their life, have fallen into) there is no instance in all
the scripture of any good man charged with covetousness. He proceeds to
show how necessary it is to mortify sins, v. 6, 7. 1. Because, if we do
not kill them, they will kill us: For which things\' sake the wrath of
God cometh on the children of disobedience, v. 6. See what we are all by
nature more or less: we are children of disobedience: not only
disobedient children, but under the power of sin and naturally prone to
disobey. The wicked are estranged from the womb; they go astray as soon
as they are born, speaking lies, Ps. 58:3. And, being children of
disobedience, we are children of wrath, Eph. 2:3. The wrath of God comes
upon all the children of disobedience. Those who do not obey the
precepts of the law incur the penalties of it. The sins he mentions were
their sins in their heathen and idolatrous state, and they were then
especially the children of disobedience; and yet these sins brought
judgments upon them, and exposed them to the wrath of God. 2. We should
mortify these sins because they have lived in us: In which you also
walked some time, when you lived in them, v. 7. Observe, The
consideration that we have formerly lived in sin is a good argument why
we should now forsake it. We have walked in by-paths, therefore let us
walk in them no more. If I have done iniquity, I will do no more, Job
34:32. The time past our lives may suffice us to have wrought the will
of the Gentiles, when we walked in lasciviousness, 1 Pt. 4:3.-When you
lived among those who did such things (so some understand it), then you
walked in those evil practices. It is a hard thing to live among those
who do the works of darkness and not have fellowship with them, as it is
to walk in the mire and contract no soil. Let us keep out of the way of
evil-doers.

### Verses 8-11

As we are to mortify inordinate appetites, so we are to mortify
inordinate passions (v. 8): But now you also put off all these, anger
wrath, malice; for these are contrary to the design of the gospel, as
well as grosser impurities; and, though they are more spiritual
wickedness, have not less malignity in them. The gospel religion
introduces a change of the higher as well as the lower powers of the
soul, and supports the dominion of right reason and conscience over
appetite and passion. Anger and wrath are bad, but malice is worse,
because it is more rooted and deliberate; it is anger heightened and
settled. And, as the corrupt principles in the heart must be cut off, so
the product of them in the tongue; as blasphemy, which seems there to
mean, not so much speaking ill of God as speaking ill of men, giving ill
language to them, or raising ill reports of them, and injuring their
good name by any evil arts,-filthy communication, that is, all lewd and
wanton discourse, which comes from a polluted mind in the speaker and
propagates the same defilements in the hearers,-and lying: Lie not one
to another (v. 9), for it is contrary both to the law of truth and the
law of love, it is both unjust and unkind, and naturally tends to
destroy all faith and friendship among mankind. Lying makes us like the
devil (who is the father of lies), and is a prime part of the devil\'s
image upon our souls; and therefore we are cautioned against this sin by
this general reason: Seeing you have put off the old man with his deeds,
and have put on the new man, v. 10. The consideration that we have by
profession put away sin and espoused the cause and interest of Christ,
that we have renounced all sin and stand engaged to Christ, should
fortify us against this sin of lying. Those who have put off the old man
have put it off with its deeds; and those who have put on the new man
must put on all its deeds-not only espouse good principles but act them
in a good conversation. The new man is said to be renewed in knowledge,
because an ignorant soul cannot be a good soul. Without knowledge the
heart cannot be good, Prov. 19:2. The grace of God works upon the will
and affections by renewing the understanding. Light is the first thing
in the new creation, as it was in the first: after the image of him who
created him. It was the honour of man in innocence that he was made
after the image of God; but that image was defaced and lost by sin, and
is renewed by sanctifying grace: so that a renewed soul is something
like what Adam was in the day he was created. In the privilege and duty
of sanctification there is neither Greek nor Jew, circumcision nor
uncircumcision, Barbarian, Scythian, bond nor free, v. 11. There is now
no difference arising from different country or different condition and
circumstance of life: it is as much the duty of the one as of the other
to be holy, and as much the privilege of the one as of the other to
receive from God the grace to be so. Christ came to take down all
partition-walls, that all might stand on the same level before God, both
in duty and privilege. And for this reason, because Christ is all in
all. Christ is a Christian\'s all, his only Lord and Saviour, and all
his hope and happiness. And to those who are sanctified, one as well as
another and whatever they are in other respects, he is all in all, the
Alpha and Omega, the beginning and the end: he is all in all things to
them.

### Verses 12-17

The apostle proceeds to exhort to mutual love and compassion: Put on
therefore bowels of mercy, v. 12. We must not only put off anger and
wrath (as v. 8), but we must put on compassion and kindness; not only
cease to do evil, but learn to do well; not only not do hurt to any, but
do what good we can to all.

`I.` The argument here used to enforce the exhortation is very affecting:
Put on, as the elect of God, holy and beloved. Observe, 1. Those who are
holy are the elect of God; and those who are the elect of God, and holy,
are beloved-beloved of God, and ought to be so of all men. 2. Those who
are the elect of God, holy and beloved, ought to conduct themselves in
every thing as becomes them, and so as not to lose the credit of their
holiness, nor the comfort of their being chosen and beloved. It becomes
those who are holy towards God to be lowly and loving towards all men.
Observe, What we must put on in particular. `(1.)` Compassion towards the
miserable: Bowels of mercy, the tenderest mercies. Those who owe so much
to mercy ought to be merciful to all who are proper objects of mercy. Be
you merciful, as your Father is merciful, Lu. 6:36. `(2.)` Kindness
towards our friends, and those who love us. A courteous disposition
becomes the elect of God; for the design of the gospel is not only to
soften the minds of men, but to sweeten them, and to promote friendship
among men as well as reconciliation with God. `(3.)` Humbleness of mind,
in submission to those above us, and condescension to those below us.
There must not only be a humble demeanour, but a humble mind. Learn of
me, for I am meek and lowly in heart, Mt. 11:29. `(4.)` Meekness towards
those who have provoked us, or been any way injurious to us. We must not
be transported into any indecency by our resentment of indignities and
neglects: but must prudently bridle our own anger, and patiently bear
the anger of others. `(5.)` Long-suffering towards those who continue to
provoke us. Charity suffereth long, as well as is kind, 1 Co. 13:4. Many
can bear a short provocation who are weary of bearing when it grows
long. But we must suffer long both the injuries of men and the rebukes
of divine Providence. If God is long-suffering to us, under all our
provocations of him, we should exercise long-suffering to others in like
cases. `(6.)` Mutual forbearance, in consideration of the infirmities and
deficiencies under which we all labour: Forbearing one another. We have
all of us something which needs to be borne with, and this is a good
reason why we should bear with others in what is disagreeable to us. We
need the same good turn from others which we are bound to show them.
`(7.)` A readiness to forgive injuries: Forgiving one another, if any man
have a quarrel against any. While we are in this world, where there is
so much corruption in our hearts, and so much occasion of difference and
contention, quarrels will sometimes happen, even among the elect of God,
who are holy and beloved, as Paul and Barnabas had a sharp contention,
which parted them asunder one from the other (Acts 15:39), and Paul and
Peter, Gal. 2:14. But it is our duty to forgive one another in such
cases; not to bear any grudge, but put up with the affront and pass it
by. And the reason is: Even as Christ forgave you, so also do you. The
consideration that we are forgiven by Christ so many offences is a good
reason why we should forgive others. It is an argument of the divinity
of Christ that he had power on earth to forgive sins; and it is a branch
of his example which we are obliged to follow, if we ourselves would be
forgiven. Forgive us our trespasses, as we forgive those who trespass
against us, Mt. 6:12.

`II.` In order to all this, we are exhorted here to several things:-1. To
clothe ourselves with love (v. 14): Above all things put on charity: epi
pasi de toutois-over all things. Let this be the upper garment, the
robe, the livery, the mark of our dignity and distinction. Or, Let this
be principal and chief, as the whole sum and abstract of the second
table. Add to faith virtue, and to brotherly-kindness charity, 2 Pt.
1:5-7. He lays the foundation in faith, and the top-stone in charity,
which is the bond of perfectness, the cement and centre of all happy
society. Christian unity consists of unanimity and mutual love. 2. To
submit ourselves to the government of the peace of God (v. 15): Let the
peace of God rule in your hearts, that is, God\'s being at peace with
you, and the comfortable sense of his acceptance and favour: or, a
disposition to peace among yourselves, a peaceable spirit, that keeps
the peace, and makes peace. This is called the peace of God, because it
is of his working in all who are his. The kingdom of God is
righteousness and peace, Rom. 14:17. \"Let this peace rule in your
heart-prevail and govern there, or as an umpire decide all matters of
difference among you.\"-To which you are called in one body. We are
called to this peace, to peace with God as our privilege and peace with
our brethren as our duty. Being united in one body, we are called to be
at peace one with another, as the members of the natural body; for we
are the body of Christ, and members in particular, 1 Co. 12:27. To
preserve in us this peaceable disposition, we must be thankful. The work
of thanksgiving to God is such a sweet and pleasant work that it will
help to make us sweet and pleasant towards all men. \"Instead of envying
one another upon account of any particular favours and excellence, be
thankful for his mercies, which are common to all of you.\" 3. To let
the word of Christ dwell in us richly, v. 16. The gospel is the word of
Christ, which has come to us; but that is not enough, it must dwell in
us, or keep house-enoikeitoµ, not as a servant in a family, who is under
another\'s control, but as a master, who has a right to prescribe to and
direct all under his roof. We must take our instructions and directions
from it, and our portion of meat and strength, of grace and comfort, in
due season, as from the master of the household. It must dwell in us;
that is, be always ready and at hand to us in every thing, and have its
due influence and use. We must be familiarly acquainted with it, and
know it for our good, Job 5:27. It must dwell in us richly: not only
keep house in our hearts, but keep a good house. Many have the word of
Christ dwelling in them, but it dwells in them but poorly; it has no
mighty force and influence upon them. Then the soul prospers when the
word of God dwells in us richly, when we have abundance of it in us, and
are full of the scriptures and of the grace of Christ. And this in all
wisdom. The proper office of wisdom is to apply what we know to
ourselves, for our own direction. The word of Christ must dwell in us,
not in all notion and speculation, to make us doctors, but in all
wisdom, to make us good Christians, and enable us to conduct ourselves
in every thing as becomes Wisdom\'s children. 4. To teach and admonish
one another. This would contribute very much to our furtherance in all
grace; for we sharpen ourselves by quickening others, and improve our
knowledge by communicating it for their edification. We must admonish
one another in psalms and hymns. Observe, Singing of psalms is a gospel
ordinance: psalmois kai hymnois kai oµdais-the Psalms of David, and
spiritual hymns and odes, collected out of the scripture, and suited to
special occasions, instead of their lewd and profane songs in their
idolatrous worship. Religious poesy seems countenanced by these
expressions and is capable of great edification. But, when we sing
psalms, we make no melody unless we sing with grace in our hearts,
unless we are suitably affected with what we sing and go along in it
with true devotion and understanding. Singing of psalms is a teaching
ordinance as well as a praising ordinance; and we are not only to
quicken and encourage ourselves, but to teach and admonish one another,
mutually excite our affections, and convey instructions. 5. All must be
done in the name of Christ (v. 17): And whatsoever you do in word or
deed, do all in the name of the Lord Jesus, according to his command and
in compliance with his authority, by strength derived from him, with an
eye to his glory, and depending upon his merit for the acceptance of
what is good and the pardon of what is amiss, Giving thanks to God and
the Father by him. Observe, `(1.)` We must give thanks in all things;
whatsoever we do, we must still give thanks, Eph. 5:20, Giving thanks
always for all things. `(2.)` The Lord Jesus must be the Mediator of our
praises as well as of our prayers. We give thanks to God and the Father
in the name of the Lord Jesus Christ, Eph. 5:20. Those who do all things
in Christ\'s name will never want matter of thanksgiving to God, even
the Father.

### Verses 18-25

The apostle concludes the chapter with exhortations to relative duties,
as before in the epistle to the Ephesians. The epistles which are most
taken up in displaying the glory of divine grace, and magnifying the
Lord Jesus, are the most particular and distinct in pressing the duties
of the several relations. We must never separate the privileges and
duties of the gospel religion.

`I.` He begins with the duties of wives and husbands (v. 18): Wives,
submit yourselves unto your own husbands, as it is fit in the Lord.
Submission is the duty of wives, hypotassesthe. It is the same word
which is used to express our duty to magistrates (Rom. 13:1, Let every
soul be subject to the higher powers), and is expressed by subjection
and reverence, Eph. 5:24, 33. The reason is that Adam was first formed,
then Eve: and Adam was not deceived, but the woman, being deceived, was
in the transgression, 1 Tim. 2:13, 14. He was first in the creation and
last in the transgression. The head of the woman is the man; and the man
is not of the woman, but the woman of the man; neither was the man
created for the woman, but the woman for the man, 1 Co. 11:3, 8, 9. It
is agreeable to the order of nature and the reason of things, as well as
the appointment and will of God. But then it is submission, not to a
rigorous lord or absolute tyrant, who may do his will and is without
restraints, but to a husband, and to her own husband, who stands in the
nearest relation, and is under strict engagements to proper duty too.
And this is fit in the Lord, it is becoming the relation, and what they
are bound in duty to do, as an instance of obedience to the authority
and law of Christ. On the other hand, husbands must love their wives,
and not be bitter against them, v. 19. They must love them with tender
and faithful affection, as Christ loved the church, and as their own
bodies, and even as themselves (Eph. 5:25, 28, 33), with a love peculiar
to the nearest relation and the greatest comfort and blessing of life.
And they must not be bitter against them, not use them unkindly, with
harsh language or severe treatment, but be kind and obliging to them in
all things; for the woman was made for the man, neither is the man
without the woman, and the man also is by the woman, 1 Co. 11:9, 11, 12.

`II.` The duties of children and parents: Children, obey your parents in
all things, for this is well-pleasing unto the Lord, v. 20. They must be
willing to do all their lawful commands, and be at their direction and
disposal; as those who have a natural right and are fitter to direct
them than themselves. The apostle (Eph. 6:2) requires them to honour as
well as obey their parents; they must esteem them and think honourably
of them, as the obedience of their lives must proceed from the esteem
and opinion of their minds. And this is well-pleasing to God, or
acceptable to him; for it is the first commandment with promise (Eph.
6:2), with an explicit promise annexed to it, namely, That it shall be
well with them, and they shall live long on the earth. Dutiful children
are the most likely to prosper in the world and enjoy long life. And
parents must be tender, as well as children obedient (v. 21): \"Fathers,
provoke not your children to anger, lest they be discouraged. Let not
your authority over them be exercised with rigour and severity, but with
kindness and gentleness, lest you raise their passions and discourage
them in their duty, and by holding the reins too tight make them fly out
with greater fierceness.\" The bad temper and example of imprudent
parents often prove a great hindrance to their children and a
stumbling-block in their way; see Eph. 6:4. And it is by the tenderness
of parents, and dutifulness of children, that God ordinarily furnishes
his church with a seed to serve him, and propagates religion from age to
age.

`III.` Servants and masters: Servants, obey your masters in all things
according to the flesh, v. 22. Servants must do the duty of the relation
in which they stand, and obey their master\'s commands in all things
which are consistent with their duty to God their heavenly Master. Not
with eye-service, as men-pleasers-not only when their master\'s eye is
upon them, but when they are from under their master\'s eye. They must
be both just and diligent. In singleness of heart, fearing God-without
selfish designs, or hypocrisy and disguise, as those who fear God and
stand in awe of him. Observe, The fear of God ruling in the heart will
make people good in every relation. Servants who fear God will be just
and faithful when they are from under their master\'s eye, because they
know they are under the eye of God. See Gen. 20:11, Because I thought,
Surely the fear of God is not in this place. Neh. 5:15, But so did not
I, because of the fear of God. \"And whatsoever you do, do it heartily
(v. 23), with diligence, not idly and slothfully:\" or, \"Do it
cheerfully, not discontented at the providence of God which put you in
that relation.\"-As to the Lord, and not as to men. It sanctifies a
servant\'s work when it is done as unto God-with an eye to his glory and
in obedience to his command, and not merely as unto men, or with regard
to them only. Observe, We are really doing our duty to God when we are
faithful in our duty to men. And, for servants\' encouragement, let them
know that a good and faithful servant is never the further from heaven
for his being a servant: \"Knowing that of the Lord you shall receive
the reward of the inheritance, for you serve the Lord Christ, v. 24.
Serving your masters according to the command of Christ, you serve
Christ, and he will be your paymaster: you will have a glorious reward
at last. Though you are now servants, you will receive the inheritance
of sons. But, on the other hand, He who does wrong will receive for the
wrong which he has done,\" v. 25. There is a righteous God, who, if
servants wrong their masters, will reckon with them for it, though they
may conceal it from their master\'s notice. And he will be sure to
punish the unjust as well as reward the faithful servant: and so if
masters wrong their servants.-And there is no respect of persons with
him. The righteous Judge of the earth will be impartial, and carry it
with an equal hand towards the master and servant; not swayed by any
regard to men\'s outward circumstances and condition of life. The one
and the other will stand upon a level at his tribunal.

It is probable that the apostle has a particular respect, in all these
instances of duty, to the case mentioned 1 Co. 7 of relations of a
different religion, as a Christian and heathen, a Jewish convert and an
uncircumcised Gentile, where there was room to doubt whether they were
bound to fulfil the proper duties of their several relations to such
persons. And, if it hold in such cases, it is much stronger upon
Christians one towards another, and where both are of the same religion.
And how happy would the gospel religion make the world, if it every
where prevailed; and how much would it influence every state of things
and every relation of life!
