2nd Chronicles, Chapter 34
==========================

Commentary
----------

Before we see Judah and Jerusalem ruined we shall yet see some glorious
years, while good Josiah sits at the helm. By his pious endeavours for
reformation God tried them yet once more; if they had known in this
their day, the day of their visitation, the things that belonged to
their peace and improved them, their ruin might have been prevented. But
after this reign they were hidden from their eyes, and the next reigns
brought an utter desolation upon them. In this chapter we have, `I.` A
general account of Josiah\'s character (v. 1, 2). `II.` His zeal to root
out idolatry (v. 3-7). `III.` His care to repair the temple (v. 8-13). `IV.`
The finding of the book of the law and the good use made of it (v.
14-28). `V.` The public reading of the law to the people and their
renewing their covenant with God thereupon (v. 29-33). Much of this we
had 2 Ki. 22.

### Verses 1-7

Concerning Josiah we are here told, 1. That he came to the crown when he
was very young, only eight years old (yet his infancy did not debar him
from his right), and he reigned thirty-one years (v. 1), a considerable
time. I fear, however, that in the beginning of his reign things went
much as they had done in his father\'s time, because, being a child, he
must have left the management of them to others; so that it was not till
his twelfth year, which goes far in the number of his years, that the
reformation began, v. 3. He could not, as Hezekiah did, fall about it
immediately. 2. That he reigned very well (v. 2), approved himself to
God, trod in the steps of David, and did not decline either to the right
hand of to the left: for there are errors on both hands. 3. That while
he was young, about sixteen years old, he began to seek after God, v. 3.
We have reason to think he had not so good an education as Manasseh had
(it is well if those about him did not endeavour to corrupt and debauch
him); yet he thus sought God when he was young. It is the duty and
interest of young people, and will particularly be the honour of young
gentlemen, as soon as they come to years of understanding, to begin to
seek God; for those that seek him early shall find him. 4. That in the
twelfth year of his reign, when it is probable he took the
administration of the government entirely into his own hands, he began
to purge his kingdom from the remains of idolatry; he destroyed the high
places, groves, images, altars, all the utensils of idolatry, v. 3, 4.
He not only cast them out as Manasseh did, but broke them to pieces, and
made dust of them. This destruction of idolatry is here said to be in
his twelfth year, but it was said (2 Ki. 23:23) to be in his eighteenth
year. Something was probably done towards it in his twelfth year; then
he began to purge out idolatry, but that good work met with opposition,
so that it was not thoroughly done till they had found the book of the
law six years afterwards. But here the whole work is laid together
briefly which was much more largely and particularly related in the
Kings. His zeal carried him out to do this, not only in Judah and
Jerusalem, but in the cities of Israel too, as far as he had any
influence upon them.

### Verses 8-13

Here, 1. Orders are given by the king for the repair of the temple, v.
8. When he had purged the house of the corruptions of it he began to fit
it up for the services that were to be performed in it. Thus we must do
by the spiritual temple of the heart, get it cleansed from the
pollutions of sin, and then renewed, so as to be transformed into the
image of God. Josiah, in this order, calls God the Lord his God. Those
that truly love God will love the habitation of his house. 2. Care is
taken about it, effectual care. The Levites went about the country and
gathered money towards it, which was returned to the three trustees
mentioned, v. 8. They brought it to Hilkiah the high priest (v. 9), and
he and they put it into the hands of workmen, both overseers and
labourers, who undertook to do it by the great, as we say, or in the
gross, v. 10, 11. It is observed that the workmen were industrious and
honest: They did the work faithfully (v. 12); and workmen are not
completely faithful if they are not both careful and diligent, for a
confidence is reposed in them that they will be so. It is also intimated
that the overseers were ingenious; for it is said that all those were
employed to inspect this work who were skilful in instruments of music;
not that their skill in music could be of any use in architecture, but
it was an evidence that they were men of sense and ingenuity, and
particularly that their genius lay towards the mathematics, which
qualified them very much for this trust. Witty men are then wise men
when they employ their wit in doing good, in helping their friends, and,
as they have opportunity, in serving the public. Observe, in this work,
how God dispenses his gifts variously; here were some that were bearers
of burdens, cut out for bodily labour and fit to work. Here were others
(made meliori luto-of finer materials) that had skill in music, and they
were overseers of those that laboured, and scribes and officers. The
former were the hands: these were the heads. They had need of one
another, and the work needed both. Let not the overseers of the work
despise the bearers of burdens, nor let those that work in the service
grudge at those whose office it is to direct; but let each esteem and
serve the other in love, and let God have the glory and the church the
benefit of the different gifts and dispositions of both.

### Verses 14-28

This whole paragraph we had, just as it is here related, 2 Ki. 22:8-20,
and have nothing to add here to what was there observed. But, 1. We may
hence take occasion to bless God that we have plenty of Bibles, and that
they are, or may be, in all hands,-that the book of the law and gospel
is not lost, is not scarce,-that, in this sense, the word of the Lord is
not precious. Bibles are jewels, but, thanks be to God, they are not
rarities. The fountain of the waters of life is not a spring shut up or
a fountain sealed, but the streams of it, in all places, make glad the
city of our God. Usus communis aquarum-These waters flow for general
use. What a great deal shall we have to answer for if the great things
of God\'s law, being thus made common, should be accounted by us as
strange things! 2. We may hence learn, whenever we read or hear the word
of God, to affect our hearts with it, and to get them possessed with a
holy fear of that wrath of God which is there revealed against all
ungodliness and unrighteousness of men, as Josiah\'s tender heart was.
When he heard the words of the law he rent his clothes (v. 19), and God
was well pleased with his doing so, v. 27. Were the things contained in
the scripture new to us, as they were here to Josiah, surely they would
make deeper impressions upon us than commonly they do; but they are not
the less weighty, and therefore should not be the less considered by us,
for their being well known. Rend the heart therefore, not the garments.
3. We are here directed when we are under convictions of sin, and
apprehensions of divine wrath, to enquire of the Lord; so Josiah did, v.
21. It concerns us to ask (as they did, Acts 2:37), Men and brethren,
what shall we do? and more particularly (as the jailor), What must I do
to be saved? Acts 16:30. If you will thus enquire, enquire (Isa. 21:12);
and, blessed be God, we have the lively oracles to which to apply with
these enquiries. 4. We are here warned of the ruin that sin brings upon
nations and kingdoms. Those that forsake God bring evil upon themselves
(v. 24, 25), and kindle a fire which shall not be quenched. Such will
the fire of God\'s wrath be when the decree has gone forth against those
that obstinately and impenitently persist in their wicked ways. 5. We
are here encouraged to humble ourselves before God and seek unto him, as
Josiah did. If we cannot prevail thereby to turn away God\'s wrath from
our land, yet we shall deliver our own souls, v. 27, 28. And good people
are here taught to be so far from fearing death as to welcome it rather
when it takes them away from the evil to come. See how the property of
it is altered by making it the matter of a promise: Thou shalt be
gathered to thy grave in peace, housed in that ark, as Noah, when a
deluge is coming.

### Verses 29-33

We have here an account of the further advances which Josiah made
towards the reformation of his kingdom upon the hearing of the law read
and the receipt of the message God sent him by the prophetess. Happy the
people that had such a king; for here we find that, 1. They were well
taught. He did not go about to force them to do their duty, till he had
first instructed them in it. He called all the people together, great
and small, young and old, rich and poor, high and low. He that hath ears
to hear, let him hear the words of the book of the covenant; for they
are all concerned in those words. To put an honour upon the service, and
to engage attention the more, though there were priests and Levites
present, the king himself read the book to the people (v. 30), and he
read it, no doubt, in such a manner as to show that he was himself
affected with it, which would be a means of affecting the hearers. 2.
They were well fixed. The articles of agreement between God and Israel
being read, that they might intelligently covenant with God, both king
and people with great solemnity did as it were subscribe the articles.
The king in his place covenanted to keep God\'s commandments with all
his heart and soul, according to what was written in the book (v. 31),
and urged the people to declare their consent likewise to this covenant,
and solemnly to promise that they would faithfully perform, fulfil, and
keep, all and every thing that was on their part to be done, according
to this covenant: this they did; they could not for shame do otherwise.
He caused all that were present to stand to it (v. 32), and made them
all to serve, even to serve the Lord their God (v. 33), to do it and to
make a business of it. he did all he could to bring them to it-to serve,
even to serve; the repetition denotes that this was the only thing his
heart was set on; he aimed at nothing else in what he did but to engage
them to God and their duty. 3. They were well tended, were honest with
good looking to. All his days they departed not from following the Lord;
he kept them, with much ado, from running into idolatry again. All his
days were days of restraint upon them; but this intimated that there was
in them a bent to backslide, a strong inclination to idolatry. Many of
them wanted nothing but to have him out of the way, and then they would
have their high places and their images up again. And therefore we find
that in the days of Josiah (Jer. 3:6) God charged it upon treacherous
Judah that she had not returned to him with all her heart, but feignedly
(v. 10), nay, had played the harlot (v. 8) and thereby had even
justified backsliding Israel, v. 11. In the twenty-third year of this
reign, four or five years after this, they had gone on to provoke God to
anger with the works of their hands (Jer. 25:3-7); and, which is very
observable, it is from the beginning of Josiah\'s reformation, his
twelfth or thirteenth year, that the iniquity of the house of Judah,
which brought ruin upon them, and which the prophet was to bear lying on
his right side, was dated (Eze. 4:6), for thence to the destruction of
Jerusalem was just forty years. Josiah was sincere in what he did, but
the generality of the people were averse to it and hankered after their
idols still; so that the reformation, though well designed and well
prosecuted by the prince, had little or no effect upon the people. It
was with reluctancy that they parted with their idols; still they were
in heart joined to them, and wished for them again. This God saw, and
therefore from that time, when one would have thought the foundations
had been laid for a perpetual security and peace, from that very time
did the decree go forth for their destruction. Nothing hastens the ruin
of a people nor ripens them for it more than the baffling of hopeful
attempts for reformation and a hypocritical return to God. Be not
deceived, God is not mocked.
