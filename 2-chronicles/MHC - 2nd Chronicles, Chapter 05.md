2nd Chronicles, Chapter 5
=========================

Commentary
----------

The temple being built and furnished for God, we have here, `I.`
Possession given to him, by bringing in the dedicated things (v. 1), but
especially the ark, the token of his presence (v. 2-10). `II.` Possession
taken by him, in a cloud (v. 11-14). For if any man open the door of his
heart to God he will come in, Rev. 3:20.

### Verses 1-10

This agrees with what we had 1 Ki. 8:2, etc., where an account was given
of the solemn introduction of the ark into the new-erected temple. 1.
There needed no great solemnity for the bringing in of the dedicated
things, v. 1. They added to the wealth, and perhaps were so disposed as
to add to the beauty of it; but they could not add to the holiness, for
it was the temple that sanctified the gold, Mt. 23:17. See how just
Solomon was both to God and to his father. Whatever David had dedicated
to God, however much he might have liked it himself, he would by no
means alienate it, but put it among the treasures of the temple. Those
children that would inherit their godly parents\' blessing must
religiously pursue their pious intentions and not defeat them. When
Solomon had made all the vessels of the temple in abundance (ch. 4:18),
many of the materials were left, which he would not convert to any other
use, but laid up in the treasury for a time of need. Dedicated things
must not be alienated. It is sacrilege to do it. 2. But it was fit that
the ark should be brought in with great solemnity; and so it was. All
the other vessels were made new, and larger, in proportion to the house,
than they had been in the tabernacle. But the ark, with the mercy-seat
and the cherubim, was the same; for the presence and the grace of God
are the same in little assemblies that they are in large ones, in the
poor condition of the church that they are in its prosperous estate.
Wherever two or three are gathered together in Christ\'s name there is
he as truly present with them as if there were 2000 or 3000. The ark was
brought in attended by a very great assembly of the elders of Israel,
who came to grace and solemnity; and a very sumptuous appearance no
doubt they made, v. 2-4. It was carried by the priests (v. 7), brought
into the most holy place, and put under the wings of the great cherubim
which Solomon had set up there, v. 7, 8. There they are unto this day
(v. 9), not the day when this book was written after the captivity, but
when that was written out of which this story was transcribed. Or they
were there (so it might be read) unto this day, the day of Jerusalem\'s
desolations, that fatal day, Ps. 137:7. The ark was a type of Christ,
and, as such, a token of the presence of God. That gracious promise, Lo,
I am with you always, even unto the end of the world, does in effect
bring the ark into our religious assemblies if we by faith and prayer
put that promise in suit; and this we should be most solicitous and
earnest for. Lord, if thy presence go not up with us, wherefore should
we go up? The temple itself, if Christ leave it, is a desolate place,
Mt. 23:38. 3. With the ark they brought up the tabernacle and all the
holy vessels that were in the tabernacle, v. 5. Those were not
alienated, because they had been dedicated to God, were not altered or
melted down for the new work, though there was no need of them; but they
were carefully laid up as monuments of antiquity, and probably as many
of the vessels as were fit for use were still used. 4. This was done
with great joy. They kept a holy feast upon the occasion (v. 3), and
sacrificed sheep and oxen without number, v. 6. Note, `(1.)` The
establishment of the public worship of God according to his institution,
and with the tokens of his presence, is, and ought to be, matter of
great joy to any people. `(2.)` When Christ is formed in a soul, the law
written in the heart, the ark of the covenant settled there, so that it
becomes the temple of the Holy Ghost, there is true satisfaction in that
soul. `(3.)` Whatever we have the comfort of we must, by the sacrifice of
praise, give God the glory of, and not be straitened therein; for with
such sacrifices God is well pleased. If God favour us with his presence,
we must honour him with our services, the best we have.

### Verses 11-14

Solomon, and the elders of Israel, had done what they could to grace the
solemnity of the introduction of the ark; but God, by testifying his
acceptance of what they did, put the greatest honour upon it. The cloud
of glory that filled the house beautified it more than all the gold with
which it was overlaid or the precious stones with which it was
garnished; and yet that was no glory in comparison with the glory of the
gospel dispensation, 2 Co. 3:8-10. Observe,

`I.` How God took possession of the temple: He filled it with a cloud, v.
13. 1. Thus he signified his acceptance of this temple to be the same to
him that the tabernacle of Moses was, and assured them that he would be
the same in it; for it was by a cloud that he made his public entry into
that, Ex. 40:34. 2. Thus he considered the weakness and infirmity of
those to whom he manifested himself, who could not bear the dazzling
lustre of the divine light: it would have overpowered them; he therefore
spread his cloud upon it, Job 26:9. Christ revealed things unto his
disciples as they were able to bear them, and in parables, which wrapped
up divine things as in a cloud. 3. Thus he would affect all that
worshipped in his courts with holy reverence and fear. Christ\'s
disciples were afraid when they entered into a cloud, Lu. 9:34. 4. Thus
he would intimate the darkness of that dispensation, by reason of which
they could not stedfastly look to the end of those things which were now
abolished, 2 Co. 3:13.

`II.` When he took possession of it. 1. When the priests had come out of
the holy place, v. 11. This is the way of giving possession. All must
come out, that the rightful owner may come in. Would we have God dwell
in our hearts? We must leave room for him; let every thing else give
way. We are here told that upon this occasion the whole family of the
priests attended, and not any one particular course: All the priests
that were present were sanctified (v. 11), because there was work enough
for them all, when such a multitude of sacrifices were to be offered,
and because it was fit that they should all be eye-witnesses of this
solemnity and receive the impressions of it. 2. When the singers and
musicians praised God, then the house was filled with a cloud. This is
very observable; it was not when they offered sacrifices, but when they
sang the praises of God, that God gave them this token of his favour;
for the sacrifice of praise pleaseth the Lord better than that of an ox
or bullock, Ps. 69:31. All the singers and musicians were employed,
those of all the three families; and, to complete the concert, 120
priests, with their trumpets, joined with them, all standing at the east
end of the altar, on that side of the court which lay outmost towards
the people, v. 12. And, when this part of the service began, the glory
of God appeared. Observe, `(1.)` It was when they were unanimous, when
they were as one, to make one sound. The Holy God descended on the
apostles when they met with one accord, Acts 2:1-4. Where unity is the
Lord commands the blessing. `(2.)` It was when they were lively and
hearty, and lifted up their voice to praise the Lord. Then we serve God
acceptably when we are fervent in spirit serving him. `(3.)` It was when
they were, in their praises, celebrating the everlasting mercy and
goodness of God. As there is one saying oftener repeated in scripture
than this, his mercy endureth for ever (twenty-six times in one psalm,
Ps. 136, and often elsewhere), so there is none more signally owned from
heaven; for it was not the expression of some rapturous flights that the
priests were singing when the glory of God appeared, but this plain
song, He is good, and his mercy endureth for ever. God\'s goodness is
his glory, and he is pleased when we give him the glory of it.

`III.` What was the effect of it. The priests themselves could not stand
to minister, by reason of the cloud (v. 14), which, as it was an
evidence that the law made men priests that had infirmity, so (as bishop
Patrick observes) it was a plain intimation that the Levitical
priesthood should cease, and stand no longer to minister, when the
Messiah should come, in whom the fulness of the godhead should dwell
bodily. In him the glory of God dwelt among us, but covered with a
cloud. The Word was made flesh; and when he comes to his temple, like a
refiner\'s fire, who may abide the day of his coming? And who shall
stand when he appeareth? Mal. 3:1, 2.
