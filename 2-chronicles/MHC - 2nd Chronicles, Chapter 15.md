2nd Chronicles, Chapter 15
==========================

Commentary
----------

Asa and his army were now returning in triumph from the battle, laden
with spoils and adorned with the trophies of victory, the pious prince,
we may now suppose, studying what he should render to God for this great
favour. He knew that the work of reformation, which he had begun in his
kingdom, was not perfected; his enemies abroad were subdued, but there
were more dangerous enemies at home that were yet unconquered-idols in
Judah and Benjamin: his victory over the former emboldened him
vigorously to renew his attack upon the latter. Now here we have, `I.` The
message which God sent to him, by a prophet, to engage him to, and
encourage him in, the prosecution of his reformation (v. 1-7). `II.` The
life which this message put into that good cause, and their proceedings
in pursuance of it. Idols removed (v. 8). The spoil dedicated to God (v.
9-11). A covenant made with God, and a law for the punishing of
idolaters (v. 12-15). A reformation at court (v. 16). Dedicated things
brought into the house of God (v. 18). All well, but that the high
places were permitted (v. 17). And the effect of this was great peace
(v. 19).

### Verses 1-7

It was a great happiness to Israel that they had prophets among them;
yet, while they were thus blessed, they were strangely addicted to
idolatry, whereas, when the spirit of prophecy had ceased under the
second temple, and the canon of the Old Temple was completed (which was
constantly read in their synagogues), they were pure from idolatry; for
the scriptures are of all other the most sure word of prophecy, and most
effectual, and the church could not be so easily imposed upon by a
counterfeit Bible as by a counterfeit prophet. Here was a prophet sent
to Asa and his army, when they returned victorious from the war with the
Ethiopians, not to compliment them and congratulate them on their
success, but to quicken them to their duty; this is the proper business
of God\'s ministers, even with princes and the greatest men. The Spirit
of God came upon the prophet (v. 1), both to instruct him what he should
say and to enable him to say it with clearness and boldness.

`I.` He told them plainly upon what terms they stood with God. Let them
not think that, having obtained this victory, all was their own for
ever; no, he must let them know they were upon their good behaviour. Let
them do well, and it will be well with them, otherwise not. 1. The Lord
is with you while you are with him. This is both a word of comfort, that
those who keep close to God shall always have his presence with them,
and also a word of caution: \"He is with you, while you are with him,
but no longer; you have now a signal token of his favourable presence
with you, but the continuance of it depends upon your perseverance in
the way of your duty.\" 2. \"If you seek him, he will be found of you.
Sincerely desire his favour, and aim at it, and you shall obtain it.
Pray, and you shall prevail. He never said, nor ever will, Seek you me
in vain.\" See Heb. 11:6. But, 3. \"If you forsake him and his
ordinances, he is not tied to you, but will certainly forsake you, and
then you are undone, your present triumphs will be no security to you;
woe to you when God departs.\"

`II.` He set before them the dangerous consequence of forsaking God and
his ordinances, and that there was no way of having grievances
redressed, but by repenting, and returning unto God. When Israel forsook
their duty they were over-run with a deluge of atheism, impiety,
irreligion, and all irregularity (v. 3), and were continually
embarrassed with vexatious and destroying wars, foreign and domestic, v.
5, 6. But when their troubles drove them to God they found it not in
vain to seek him, v. 4. But the question is, What time does this refer
to? 1. Some think it looks as far back as the days of the Judges. A long
season ago Israel was without the true God, for they worshipped false
gods; it was a time of ignorance, for, though they had priests, they had
no teaching priests, though they had elders, yet no law to any purpose,
v. 3. These were sad times, when they were frequently oppressed by one
enemy or other and grievously harassed by Moabites, Midianites,
Ammonites, and other nations. They were vexed with all adversity (v. 6),
yet when, in their perplexity, they turned to God by repentance, prayer,
and reformation, he raised up deliverers for them. Then was that maxim
often verified, that God is with us while we are with him. Whatsoever
things of this kind were written aforetime were written for our
admonition. 2. Others think it describes the state of the ten tribes
(who were now properly called Israel) in the days of Asa. \"Now, since
Jeroboam set up the calves, though he pretended to honour the God that
brought them out of Egypt, yet his idolatry has brought them to
downright infidelity; they are without the true God,\" and no marvel
when they were without teaching priests. Jeroboam\'s priests were not
teachers, and thus they came to be without law. It is next to impossible
that any thing of religion should be kept up without a preaching
ministry. In those times there was no peace, v. 5. Their war with Judah
gave them frequent alarms; so did the late insurrection of Baasha and
other occasions not mentioned. They provoked God with all iniquity, and
then he vexed them with all adversity; yet, when they turned to God, he
was entreated for them. Let Judah take notice of this; let their
neighbours\' harms be their warnings. Give no countenance to graven
images for you see what mischiefs they produce. 3. Others think the
whole passage may be read in the future tense, and that it looks
forward: Hereafter Israel will be without the true God and a teaching
priest, and they will be destroyed by one judgment after another till
they return to God and seek him. See Hos. 3:4.

`III.` Upon this he grounded his exhortation to prosecute the work of
reformation with vigour (v. 7): Be strong, for your work shall be
rewarded. Note, 1. God\'s work should be done with diligence and
cheerfulness, but will not be done without resolution. 2. This should
quicken us to the work of religion, that we shall be sure not to lose by
it ultimately. It will not go unrewarded. How should it, when the work
is its own reward?

### Verses 8-19

We are here told what good effect the foregoing sermon had upon Asa.

`I.` He grew more bold for God than he had been. His victory would inspire
him with some new degrees of resolution, but this message from God with
much more. Now he took courage. he saw how necessary a further
reformation was, and what assurance he had of God\'s presence with him
in it; and this made him daring, and helped him over the difficulties
which had before deterred him and driven him off from the undertaking.
Now he ventured to destroy all the abominable idols (and all idolatries
are abominable, 1 Pt. 4:3) as far as ever his power went. Away with them
all. He also renewed the altar of the Lord, which, it seems, had gone
out of repair, though it was not above thirty-five years since
Solomon\'s head was laid, who erected it. So soon did these ceremonial
institutions begin to wax old, as things which, in the fulness of time,
must vanish away, Heb. 8:13.

`II.` He extended his influence further than before, v. 9. He summoned a
solemn assembly, and particularly brought the strangers to it, who had
come over to him from the ten tribes. 1. Their coming was a great
encouragement to him; for the reason of their coming was because they
saw that the Lord his God was with him. It is good to be with those that
have God with them, to come into relation to, and contract acquaintance
and friendship with, those that live in the fear and favour of God. We
will go with you, for we have heard that God is with you, Zec. 8:23. 2.
The cognizance he took of them, and the invitation he gave them to the
general assembly, were a great encouragement to them. All strangers are
to be helped, but those that cast themselves upon God\'s good
providence, purely to keep a good conscience, are worthy of double
honour. Asa gave orders for the gathering of them together (v. 9), yet
it is said (v. 10) that they gathered themselves together, made it their
own act, so forward were they to obey the king\'s orders. This meeting
was held in the third month, probably at the feast of Pentecost, which
was in that month.

`III.` He and his people offered sacrifices to God, as his share of the
spoil they had got, v. 11. Their offering here was nothing to Solomon\'s
(ch. 7:5), which was owing to the diminution either of their zeal or of
their wealth, or of both. These sacrifices were intended by way of
thanksgiving for the favours they had received, and supplication for
further favours. Prayers and praises are now our spiritual sacrifices.
And, as he took care that the altar should have its gift, so he took
care that the temple should have its gold: He brought into the house of
God all the dedicated things, v. 18. It is honesty to render to God the
things that are his. What has been long designed for him, and long laid
by for him, as it should seem these dedicated things had been, should at
length be laid out for him. Will a man rob God, or make slow payment to
him, who is always ready to do us good?

`IV.` They entered into covenant with God, repenting that they had
violated their engagements to him and resolving to do better for the
future. It is proper for penitents, for converts, to renew their
covenants. It should seem, the motion came not from Asa, but from the
people themselves. Let every man be a volunteer that covenants with God.
Thy people shall be willing, Ps. 110:3. Observe,

`1.` What was the matter of this covenant. Nothing but what they were
before obliged to; and, though no vow or promise of theirs could lay any
higher obligation upon them than they were already under from the divine
precept, yet it would help to increase their sense of the obligation, to
arm them against temptations, and would be a testimony to the equity and
goodness of the precept. And, by joining all together in this covenant,
they strengthened the hands one of another. Two things they engaged
themselves to:-`(1.)` That they would diligently seek God themselves, seek
his precepts, seek his favour. What is religion but seeking God,
enquiring after him, applying to him, upon all occasions? We shall not
enjoy him till we come to heaven; while we are here we must continue
seeking. They would seek God as the God of their fathers, in the way
that their fathers sought him and in dependence upon the promise made to
their fathers; and they would do it with all their heart and with all
their soul, for those only seek God acceptably and successfully that are
inward with him, intent upon him, and entire for him, in their seeking
him. We make nothing of our religion if we do not make heart-work of it.
God will have all the heart or none; and, when a jewel of such
inestimable value as the divine favour is to be found, it is worth while
to seek it with all our soul. `(2.)` That they would, to the utmost of
their power, oblige others to seek him, v. 13. They agreed that
whosoever would not seek the Lord God of Israel (that is, would either
worship other gods or refuse to join with them in the worship of the
true God, that was either an obstinate idolater or an obstinate atheist)
he should be put to death. This was no new law of their own making, but
an order to put in execution that law of God to this purport, Deu. 17:2,
etc. If this law had been duly executed, there would not have been so
many abominable idols found in Judah and Benjamin, v. 8. Whether men may
now, under the gospel, be compelled by such methods as these to seek the
Lord is justly questioned; for the weapons of our warfare are not
carnal, and yet mighty.

`2.` In what manner they made this covenant. `(1.)` With great
cheerfulness, and all possible expressions of joy: The swore unto the
Lord; not secretly, as if they were either ashamed of what they did or
afraid of binding themselves too fast to him, but with a loud voice, to
express their own zeal and to animate one another; and they all rejoiced
at the oath, v. 14, 15. They did not swear to God with reluctancy (as
the poor debtor confesses a judgment to his creditor), but with all the
pleasure and satisfaction imaginable, as the bridegroom plights his
troth to the bride in the marriage covenant. Every honest Israelite was
pleased with his own engagements to God, and they were all pleased with
one another\'s. They rejoiced in it as a hopeful expedient to prevent
their apostasy from God and a happy indication of God\'s presence with
them. Note, The times of renewing our covenant with God should be times
of rejoicing, and national reformation cannot but give general
satisfaction to all that are good. It is an honour and happiness to be
in bonds to God. `(2.)` They did it with great sincerity, zeal and
resolution: They swore to God with all their hearts, and sought him with
their whole desire. The Israelites were now in an extraordinarily good
frame. O that there had always been such a heart in them! This comes in
as the reason why they rejoiced so much in what they did: it was because
they were hearty in it. Note, Those only experience the pleasure and
comfort of religion that are sincere and upright in it. What is done in
hypocrisy is a mere drudgery. But, if God has the heart, we have the
joy.

`V.` We are told what was the effect of this their solemn covenanting with
God. 1. God did well for them: He was found of them, and gave them rest
round about (v. 15), so that there was no war for a long time after (v.
19), no open general war, though there were constant bickerings between
Judah and Israel upon the frontiers, 1 Ki. 15:16. National piety
procures national blessings. 2. They did, on the whole, well for him.
They carried on the reformation so far that Maachah the queen-mother was
deposed for idolatry and her idol destroyed, v. 16. This was bravely
done of Asa, that he would not connive at idolatry in those that were
nearest to him, like Levi, that said to his father and mother, I have
not seen him, Deu. 33:9. Asa knows he must honour God more than his
grandmother, and dares not leave an idol in an apartment of his palace
while he is destroying idols in the cities of his kingdom. We may
suppose this Maachah was so far convinced of her sin that she was
willing to subscribe the association mentioned (v. 12, 13), binding
herself to seek the Lord, and therefore was not put to death as those
were that refused to sign it, great as well as small, women as well as
men: probably it was with an eye to her that women were specified. But
because she had been an idolater Asa thought fit to divest her of the
dignity and authority she had, and probably he banished her the court
and confined her to privacy, lest she should influence and infect
others. But the reformation was not complete; the high places were not
all taken away, though many of them were, ch. 14:3, 5. Those in the
cities were removed, but not those in the cities of Judah, but not those
in the cities of Israel which were reduced to the house of David; or
those that were used in the service of false gods, but not those that
were used in the service of the God of Israel. These he connived at, and
yet his heart was perfect. There may be defects in some particular
duties where yet the heart, in the man, is upright with God. Sincerity
is something less than sinless perfection.
