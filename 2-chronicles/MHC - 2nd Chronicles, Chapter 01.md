2nd Chronicles, Chapter 1
=========================

Commentary
----------

In the close of the foregoing book we read how God magnified Solomon and
Israel obeyed him; God and Israel concurred to honour him. Now here we
have an account, `I.` How he honoured God by sacrifice (v. 1-6) and by
prayer (v. 7-12). `II.` How he honoured Israel by increasing their
strength, wealth, and trade (v. 13-17).

### Verses 1-12

Here is, `I.` Solomon\'s great prosperity, v. 1. Though he had a contested
title, yet, God being with him, he was strengthened in his kingdom; his
heart and hands were strengthened, and his interest in the people.
God\'s presence will be our strength.

`II.` His great piety and devotion. His father was a prophet, a psalmist,
and kept mostly to the ark; but Solomon, having read much in his Bible
concerning the tabernacle which Moses built and the altars there, paid
more respect to them than, it should seem, David had done. Both did
well, and let neither be censured. If the zeal of one be carried out
most to one instance of religion, and of another to some other instance,
let them not judge nor despise each other.

`1.` All his great men must thus far be good men that they must join with
him in worshipping God. He spoke to the captains and judges, the
governors and chief of the fathers, to go with him to Gibeon, v. 2, 3.
Authority and interest are well bestowed on those that will thus use
them for the glory of God, and the promoting of religion. It is our duty
to engage all with whom we have influence in the solemnities of
religion, and it is very desirable to have many join with us in those
solemnities-the more the better; it is the more like heaven. Solomon
began his reign with this public pious visit to God\'s altar, and it was
a very good omen. Magistrates are then likely to do well for themselves
and their people when they thus take God along with them at their
setting out.

`2.` He offered abundance of sacrifices to God there (v. 6): 1000
burnt-offerings, and perhaps a greater number of peace-offerings, on
which he and his company feasted before the Lord. Where God sows
plentifully he expects to reap accordingly. His father David had left
him flocks and herds in abundance (1 Chr. 27:29, 31), and thus he gave
God his dues out of them. The ark was at Jerusalem (v. 4), but the altar
was at Gibeon (v. 5), and thither he brought his sacrifices; for it is
the altar that sanctifieth every gift.

`3.` He prayed a good prayer to God: this, with the answer to it, we had
before, 1 Ki. 3:5, etc. `(1.)` God bade him ask what he would; not only
that he might put him in the right way of obtaining the favours that
were intended him (Ask, and you shall receive, that your joy may be
full), but that he might try him, how he stood affected, and might
discover what was in his heart. Men\'s characters appear in their
choices and desires. What wouldst thou have? tries a man as much as,
What wouldst thou do? Thus God tried whether Solomon was one of the
children of this world, that say, Who will show us any good, or of the
children of light, that say, Lord, lift up the light of thy countenance
upon us. As we choose we shall have, and that is likely to be our
portion to which we give the preference, whether the wealth and pleasure
of this world or spiritual riches or delights. `(2.)` Like a genuine son
of David, he chose spiritual blessings rather than temporal. His
petition here is, Give me wisdom and knowledge. He owns those to be
desirable gifts, and God to be the giver of them, Prov. 2:6. God gave
the faculty of understanding, and to him we must apply for the furniture
of it. Two things are here pleaded which we had not in Kings:-`[1.]`
Thou hast made me reign in my father\'s stead, v. 8. \"Lord, thou hast
put me into this place, and therefore I can in faith ask of thee grace
to enable me to do the duty of it.\" What service we have reason to
believe God calls us to we have reason to hope he will qualify us for.
But that is not all. \"Lord, thou hast put me into this place in the
stead of David, the great and good man that filled it up so well;
therefore give me wisdom, that Israel may not suffer damage by the
change. Must I reign in my father\'s stead? Lord, give me my father\'s
spirit.\" Note, The eminency of those that went before us, and the
obligation that lies upon us to keep up and carry on the good work they
were engaged in, should provoke us to a gracious emulation, and quicken
our prayers to God for wisdom and grace, that we may do the work of God
in our day as faithfully and well as they did in theirs. `[2.]` Let thy
promise to David my father be established, v. 9. He means the promise of
concerning his successor. \"In performance of that promise, Lord, give
me wisdom.\" We do not find that wisdom was any of the things promised,
but it was necessary in order to the accomplishment of what was
promised, 2 Sa. 7:13-15. The promise was, He shall build a house for my
name, I will establish his throne, he shall be my son, and my mercy
shall not depart from him. \"Now, Lord, unless thou give me wisdom, thy
house will not be built, nor my throne established; I shall behave in a
manner unbecoming my relation to thee as a Father, shall forfeit thy
mercy, and fool it away; therefore, Lord, give me wisdom.\" Note, First,
God\'s promises are our best pleas in prayer. Remember thy word unto thy
servant. Secondly, Children may take the comfort of the promises of that
covenant which their parents, in their baptism, laid claim to, and took
hold of, for them. Thirdly, The best way to obtain the benefit of the
promises and privileges of the covenant is to be earnest in prayer with
God for wisdom and grace to do the duties of it.

`4.` He received a gracious answer to this prayer, v. 11, 12. `(1.)` God
gave him the wisdom that he asked for because he asked for it. Wisdom is
a gift that God gives as freely and liberally as any gift to those that
value it, and wrestle for it; and will resolve to make use of it; and he
upbraids not the poor petitioners with their folly, James 1:5. God\'s
grace shall never be wanting to those who sincerely desire to know and
do their duty. `(2.)` God gave him the wealth and honour which he did not
ask for because he asked not for them. Those that pursue present things
most earnestly are most likely to miss of them; while those that refer
themselves to the providence of God, if they have not the most of those
things, have the most comfort in them. Those that make this world their
end come short of the other and are disappointed in this too; but those
that make the other world their end shall not only obtain that, and full
satisfaction in it, but shall enjoy as much as is convenient of this
world in their way.

### Verses 13-17

Here is, 1. Solomon\'s entrance upon the government (v. 13): He came
from before the tabernacle, and reigned over Israel. He would not do any
acts of government till he had done his acts of devotion, would not take
honour to himself till he had given honour to God-first the tabernacle,
and then the throne. But, when he had obtained wisdom from God, he did
not bury his talent, but as he received the gift ministered the same,
did not give up himself to ease and pleasure, but minded business: he
reigned over Israel. 2. The magnificence of his court (v. 14): He
gathered chariots and horsemen. Shall we praise him for this? We praise
him not; for the king was forbidden to multiply horses, Deu. 17:16. I do
not remember that ever we find his good father in a chariot or on
horseback; a mule was the highest he mounted. We should endeavor to
excel those that went before us in goodness rather than in grandeur. 3.
The wealth and trade of his kingdom. He made silver and gold very cheap
and common, v. 15. The increase of gold lowers the value of it; but the
increase of grace advances its price; the more men have of that the more
they value it. How much better therefore is it to get wisdom than gold!
He opened also a trade with Egypt, whence he imported horses and
linen-yarn, which he exported again to the kings of Syria, with great
advantage no doubt, v. 16, 17. This we had before, 1 Ki. 10:28, 29. It
is the wisdom of princes to promote industry and encourage trade in
their dominions. Perhaps Solomon took the hint of setting up the
linen-manufacture, bringing linen-yarn out of Egypt, working it into
cloth, and then sending that to other nations, from what his mother
taught when she specified this as one of the characteristics of the
virtuous woman, She maketh fine linen, and selleth it, and delivereth
girdles of it to the merchant, Prov. 31:24. In all labour there is
profit.
