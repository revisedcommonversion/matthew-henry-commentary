2nd Chronicles, Chapter 17
==========================

Commentary
----------

Here begin the life and reign of Jehoshaphat, who was one of the first
three among the royal worthies, one of the best that ever swayed the
sceptre of Judah since David\'s head was laid. He was the good son of a
good father, so that, as this time, grace ran in the blood, even in the
blood-royal. Happy the son that had such a father, to lay a good
foundation in him and for him. Happy the father that had such a son, to
build so wall upon the foundation he had laid! Happy the kingdom that
was blessed with two such kings, two such reigns, together! In this
chapter we have, `I.` His accession to and establishment in the throne (v.
1, 2, 5). `II.` His persona piety (v. 3, 4, 6). `III.` The course he took to
promote religion in his kingdom (v. 7-9). `IV.` The mighty sway he bore
among the neighbours (v. 10, 11). `V.` The great strength of his kingdom,
both in garrisons and standing forces (v. 12-19). Thus was his
prosperity the reward of his piety and his piety the brightest grace and
ornament of his prosperity.

### Verses 1-9

Here we find concerning Jehoshaphat,

`I.` What a wise man he was. As soon as he came to the crown he
strengthened himself against Israel, v. 1. Ahab, an active warlike
prince, had now been three years upon the throne of Israel, the vigour
of his beginning falling in with the decay of Asa\'s conclusion. It is
probable that the kingdom of Israel had, of late, got ground of the
kingdom of Judah and began to grow formidable to it; so that the first
thing Jehoshaphat had to do was to make his part good on that side, and
to check the growing greatness of the king of Israel, which he did so
effectually, and without bloodshed, that Ahab soon courted his alliance,
so far was he from giving him any disturbance, and proved more dangerous
as a friend than he could have been as an enemy. Jehoshaphat
strengthened himself not to act offensively against Israel or invade
them, but only to maintain his own, which he did by fortifying the
cities that were on his frontiers, and putting garrisons, stronger than
had been, in the cities of Ephraim, which he was master of, v. 2. He did
not strengthen himself, as his father did, by a league with the king of
Syria, but by fair and regular methods, on which he might expect the
blessing of God and in which he trusted God.

`II.` What a good man he was. It is an excellent character that is here
given him. 1. He walked in the ways of his father David. In the
characters of the kings, David\'s ways are often made the standard, as 1
Ki. 15:3, 11; 2 Ki. 14:3; 16:2; 18:3. But the distinction is nowhere so
strongly marked as here between his first ways and his last ways; for
the last were not so good as the first. his ways, before he fell so
foully in the matter of Uriah (which is mentioned long afterwards as the
bar in his escutcheon, 1 Ki. 15:5), were good ways, and, though he
happily recovered from that fall, yet perhaps he never, while he lived,
fully retrieved the spiritual strength and comfort he lost by it.
Jehoshaphat followed David as far as he followed God and no further.
Paul himself thus limits our imitation of him (1 Co. 11:1): Follow me,
as I follow Christ, and not otherwise. Many good people have had their
first ways, which were their best ways, their first love, which was
their strongest love; and in every copy we propose to write after, as we
must single out that only which is good, so that chiefly which is best.
The words here will admit another reading; they run thus: He walked in
the ways of David his father (Hareshonim), those first ways, or those
ancient ways. He proposed to himself, for his example, the primitive
times of the royal family, those purest times, before the corruptions of
the late reigns came in. See Jer. 6:16. The Septuagint leaves out David,
and so refers it to Asa: He walked in the first ways of his father, and
did not imitate him in what was amiss in him, towards the latter end of
his time. It is good to be cautious in following the best men, lest we
step aside after them. 2. He sought not to Baalim, but sought to the
Lord God of his father, v. 3, 4. The neighbouring nations had their
Baalim, one had one Baal and another had another; but he abhorred them
all, had nothing to do with them. he worshipped the Lord God of his
father and him only, prayed to him only and enquired of him only; both
are included in seeking him. 3. That he walked in God\'s commandments,
not only worshipped the true God, but worshipped him according to his
own institution, and not after the doings of Israel, v. 4. Though the
king of Israel was his neighbour and ally, yet he did not learn his way.
Whatever dealings he had with him in civil matters, he would not have
communion with him, nor comply with him in his religion. In this he kept
close to the rule. 4. His heart was lifted up in the ways of the Lord
(v. 6), or he lifted up his heart. He brought his heart to his work, and
lifted up his heart in it; that is, he had a sincere regard to God in
it. Unto thee, O Lord! do I lift up my soul. His heart was enlarged in
that which is good, Ps. 119:32. He never thought he could do enough for
God. He was lively and affectionate in his religion, fervent in spirit,
serving the Lord, cheerful and pleasant in it; he went on in his work
with alacrity, as Jacob, who, after his vision of God at Bethel, lifted
up his feet, Gen. 29:1, margin. He was bold and resolute in the ways of
God and went on with courage. His heart was lifted up above the
consideration of the difficulties that were in the way of his duty; he
easily got over them all, and was not frightened with winds and clouds
from sowing and reaping, Eccl. 11:4. Let us walk in the same spirit.

`III.` What a useful man he was, not only a good man, but a good king. He
not only was good himself, but did good in his generation, did a great
deal of good. 1. He took away the teachers of lies, so images are called
(Hab. 2:18), the high places and the groves, v. 6. It is meant of those
in which idols were worshipped; for those that were dedicated to the
true God only were not taken away, ch. 20:33. It was only idolatry that
he abolished. Nothing debauched the nation more than those idolatrous
groves or images which he took away. 2. He sent forth teachers of truth.
When he enquired into the state of religion in his kingdom he found his
people generally very ignorant: they knew not that they did evil. Even
in the last good reign there had been little care taken to instruct them
in their duty; and therefore Jehoshaphat resolves to begin his work at
the right end, deals with them as reasonable creatures, will not lead
them blindfold, no, not into a reformation, but endeavours to have them
well taught, knowing that that was the way to have them well cured. In
this good work he employed, `(1.)` His princes. Those about him he sent
forth; those in the country he sent to teach in the cities of Judah, v.
7. He ordered them, in the administration of justice, not only to
correct the people when they did ill, but to teach them how to do
better, and to give a reason for what they did, that the people might be
informed of the difference between good and evil. The princes or judges
upon the bench have a great opportunity of teaching people their duty to
God and man, and it is not out of their province, for the laws of God
are to be looked upon as laws of the land. `(2.)` The Levites and priests
went with the princes, and taught in Judah, having the book of the law
with them, v. 8,9. They were teachers by office, Deu. 33:10. Teaching
was part of the work for which they had their maintenance. The priests
and the Levites had little else to do. But, it seems, they had neglected
it, pretending perhaps that they could not get the people to hear them.
\"Well,\" says Jehoshaphat, \"you shall go along with the princes, and
they with their authority shall oblige the people to come and hear you;
and then, if they be not well instructed, it is your fault.\" What an
abundance of good may be done when Moses and Aaron thus go hand in hand
in the doing of it, when princes with their power, and priests and
Levites with their scripture learning, agree to teach the people the
good knowledge of God and their duty! These itinerant judges and
itinerant preachers together were instrumental to diffuse a blessed
light throughout the cities of Judah. But it is said, They had the book
of the law of the Lord with them. `[1.]` For their own direction, that
thence they might fetch all the instructions they gave to the people,
and not teach for doctrines the commandments of men. `[2.]` For the
conviction of the people, that they might see that they had a divine
warrant for what they said and delivered to them that only which they
received from the Lord. Note, Ministers, when they go to teach the
people, should have their Bibles with them.

`IV.` What a happy man he was. 1. How happy he was in the favour of his
God, who signally owned and blessed him: The Lord was with him (v. 3);
the word of the Lord was his helper (so the Chaldee paraphrase); the
Lord established the kingdom in his hand, v. 5. Those stand firmly that
have the presence of God with them. If the beauty of the Lord our God be
upon us, that will establish the work of our hands and establish us in
our integrity. 2. How happy he was in the affections of his people (v.
5): All Judah brought him presents, in acknowledgment of his kindness in
sending preachers among them. The more there is of true religion among a
people the more there will be of conscientious loyalty. A government
that answers the end of government will be supported. The effect of the
favour both of God and his kingdom was that he had riches and honour in
abundance. It is undoubtedly true, though few will believe it, that
religion and piety are the best friends to outward prosperity. And,
observe, it follows immediately, His heart was lifted up in the ways of
the Lord. Riches and honour in abundance prove to many a clog and a
hindrance in the ways of the Lord, an occasion of pride, security, and
sensuality; but they had a quite contrary effect upon Jehoshaphat: his
abundance was oil to the wheels of his obedience, and the more he had of
the wealth of this world the more was his heart lifted up in the ways of
the Lord.

### Verses 10-19

We have here a further account of Jehoshaphat\'s great prosperity and
the flourishing state of his kingdom.

`I.` He had good interest in the neighbouring princes and nations. Though
he was not perhaps so great a soldier as David (which might have made
him their terror), nor so great a scholar as Solomon (which might have
made him their oracle), yet the fear of the Lord fell so upon them (that
is, God so influenced and governed their spirits) that they had all a
reverence for him, v. 10. And, 1. None of them made war against him.
God\'s good providence so ordered it that, while the princes and priests
were instructing and reforming the country, none of his neighbours gave
him any molestations, to take him off from that good work. Thus when
Jacob and his sons were going to worship at Bethel the terror of God was
upon the neighbouring cities, that they did not pursue after them, Gen.
35:5, and see Ex. 34:24. 2. Many of them brought presents to him (v.
11), to secure his friendship. Perhaps these were a tribute imposed upon
them by Asa, who made himself master of the cities of the Philistines,
and the tents of the Arabians, ch. 14:14, 15. With the 7700 rams, and
the same number of he-goats, which the Arabians brought, there was
probably a proportionable number of ewes and lambs, she-goats and kids.

`II.` He had a very considerable stores laid up in the cities of Judah.
He pulled down his barns, and built larger (v. 12), castles and cities
of store, for arms and victuals. He was a man of business, and aimed at
the public good in all his undertakings, either to preserve the peace or
prepare for war.

`III.` He had the militia in good order. It was never in better since
David modelled it. Five lord-lieutenants (if I may so call them) are
here named, with the numbers of those under their command (the
serviceable men, that were fit for war in their respective districts),
three in Judah, and two in Benjamin. It is said of one of these great
commanders, Amasiah, that he willingly offered himself unto the Lord (v.
16), not only to the king, to serve him in this post, but to the Lord,
to glorify him in it. He was the most eminent among them for religion,
he accepted the place, not for the honour, or power, or profit of it,
but for conscience\' sake towards God, that he might serve his country,.
It was usual for great generals then to offer of their spoils to the
Lord, 1 Chr. 26:26. But this good man offered himself first to the Lord,
and then his dedicated things. The number of the soldiers under these
five generals amounts to 1,160,000 men, a vast number for so small a
compass of ground as Judah\'s and Benjamin\'s lot to furnish out and
maintain. Abijah could bring into the field but 400,000 (ch. 13:3), Asa
not 600,000 (ch. 14:8), yet Jehoshaphat has at command almost 1,200,000.
But it must be considered, 1. That God had promised to make the seed of
Abraham like the sand of the sea for number. 2. There had now been a
long peace. 3. We may suppose that the city of Jerusalem was very much
enlarged. 4. Many had come over to them from the kingdom of Israel (ch.
15:19), which would increase the numbers of the people. 5. Jehoshaphat
was under a special blessing of God, which made his affairs to prosper
greatly. The armies, we may suppose, were dispersed all the country
over, and each man resided for the most part on his own estate; but they
appeared often, to be mustered and trained, and were ready at call
whenever there was occasion. The commanders waited on the king (v. 19)
as officers of his court, privy-counsellors, and ministers of state.

But, lastly, observe, It was not this formidable army that struck a
terror upon the neighbouring nations, that restrained them from
attempting any thing against Israel, or obliged them to pay tribute, but
the fear of God which fell upon them when Jehoshaphat reformed his
country and set up a preaching ministry in it, v. 10. The ordinances of
God are more the strength and safety of a kingdom than its military
force-its men of God more than its men of war.
