2nd Chronicles, Chapter 4
=========================

Commentary
----------

We have here a further account of the furniture of God\'s house. `I.`
Those things that were of brass. The altar for burnt-offerings (v. 1),
the sea and lavers to hold water (v. 2-6), the plates with which the
doors of the court were overlaid (v. 9), the vessels of the altar, and
other things (v. 10-18). `II.` Those that were of gold. The candlesticks
and tables (v. 7, 8), the altar of incense (v. 19), and the
appurtenances of each of these (v. 20-22). All these, except the brazen
altar (v. 1), were accounted for more largely, 1 Ki. 7:23, etc.

### Verses 1-10

David often speaks with much affection both of the house of the Lord and
of the courts of our God. Both without doors and within there was that
which typified the grace of the gospel and shadowed out good things to
come, of which the substance is Christ.

`I.` There were those things in the open court, in the view of all the
people, which were very significant.

`1.` There was the brazen altar, v. 1. The making of this was not
mentioned in the Kings. On this all the sacrifices were offered, and it
sanctified the gift. This altar was much larger than that which Moses
made in the tabernacle; that was five cubits square, this was twenty
cubits square. Now that Israel had become both numerous and more rich,
and it was to be hoped more devout (for every age should aim to be wiser
and better than that which went before it), it was expected that there
would be a greater abundance of offerings brought to God\'s altar than
had been. It was therefore made such a capacious scaffold that it might
hold them all, and none might excuse themselves from bringing those
temptations of their devotion by alleging that there was not room to
receive them. God had greatly enlarged their borders; it was therefore
fit that they should enlarge his altars. Our returns should bear some
proportion to our receivings. It was ten cubits high, so that the people
who worshipped in the courts might see the sacrifice burnt, and their
eye might affect their heart with sorrow for sin: \"It is of the Lord\'s
mercies that I am not thus consumed, and that this is accepted as an
expiation of my guilt.\" They might thus be led to consider the great
sacrifice which should be offered in the fulness of time to take away
sin and abolish death, which the blood of bulls and goats could not
possibly do. And with the smoke of the sacrifices their hearts might
ascend to heaven in holy desires towards God and his favour. In all our
devotions we must keep the eye of faith fixed upon Christ, the great
propitiation. How they went up to this altar, and carried the sacrifices
up to it, we are not told; some think by a plain ascent like a hill: if
by steps, doubtless they were so contrived as that the end of the law
(mentioned Ex. 20:26) might be answered.

`2.` There was the molten sea, a very large brass pan, in which they put
water for the priests to wash in, v. 2, 6. It was put just at the
entrance into the court of the priests, like the font at the church
door. If it were filled to the brim, it would hold 3000 baths (as here,
v. 5), but ordinarily there were only 2000 baths in it, 1 Ki. 7:26. The
Holy Ghost by this signified, `(1.)` Our great gospel privilege, that the
blood of Christ cleanseth from all sin, 1 Jn. 1:7. To us there is a
fountain opened for all believers (who are spiritual priests, Rev. 1:5,
6), nay, for all the inhabitants of Jerusalem to wash in, from sin,
which is uncleanness. There is a fulness of merit in Jesus Christ for
all those that by faith apply to him for the purifying of their
consciences, that they might serve the living God, Heb. 9:14. `(2.)` Our
great gospel duty, which is to cleanse ourselves by true repentance from
all the pollutions of the flesh and the corruption that is in the world.
Our hearts must be sanctified, or we cannot sanctify the name of God.
Those that draw nigh to God must cleanse their hands, and purify their
hearts, Jam. 4:8. If I was thee not, thou hast no part with me; and he
that is washed still needs to wash his feet, to renew his repentance,
whenever he goes in to minister, Jn. 13:10.

`3.` There were ten lavers of brass, in which they washed such things as
they offered for the burnt-offerings, v. 6. As the priests must be
washed, so must the sacrifices. We must not only purify ourselves in
preparation for our religious performances, but carefully put away all
those vain thoughts and corrupt aims which cleave to our performances
themselves and pollute them.

`4.` The doors of the court were overlaid with brass (v. 9), both for
strength and beauty, and that they might not be rotted with the weather,
to which they were exposed. Gates of brass we read of, Ps. 107:16.

`II.` There were those things in the house of the Lord (into which the
priests alone went to minister) that were very significant. All was gold
there. The nearer we come to God the purer we must be, the purer we
shall be. 1. There were ten golden candlesticks, according to the form
of that one which was in the tabernacle, v. 7. The written word is a
lamp and a light, shining in a dark place. In Moses\'s time they had but
one candlestick, the Pentateuch; but the additions which, in process of
time, were to be made of other books of scripture might be signified by
this increase of the number of the candlesticks. Light was growing. The
candlesticks are the churches, Rev. 1:20. Moses set up but one, the
church of the Jews; but, in the gospel temple, not only believers, but
churches, are multiplied. 2. There were ten golden tables (v. 8), tables
whereon the show-bread was set, v. 19. Perhaps every one of the tables
had twelve loaves of show-bread on it. As the house was enlarged, the
house-keeping was. In my father\'s house there is bread enough for the
whole family. To those tables belonged 100 golden basins, or dishes; for
God\'s table is well furnished. 3. There was a golden altar (v. 19), on
which they burnt incense. It is probable that this was enlarged in
proportion to the brazen altar. Christ, who once for all made atonement
for sin, ever lives, making intercession, in virtue of that atonement.

### Verses 11-22

We have here such a summary both of the brass-work and the gold-work of
the temple as we had before (1 Ki. 7:13, etc.), in which we have nothing
more to observe than, 1. That Huram the workman was very punctual: He
finished all that he was to make (v. 11), and left no part of his work
undone. Huram, his father, he is called, v. 16. Probably it was a sort
of nickname by which he was commonly known, Father Huram; for the king
of Tyre called him Huram Abi, my father, in compliance with whom Solomon
called him his, he being a great artist and father of the artificers in
brass and iron. He acquitted himself well both for ingenuity and
industry. 2. Solomon was very generous. He made all the vessels in great
abundance (v. 18), many of a sort, that many hands might be employed,
and so the work might go on with expedition, or that some might be laid
up for use when others were worn out. Freely he has received, and he
will freely give. When he had made vessels enough for the present he
could not convert the remainder of the brass to his own use; it is
devoted to God, and it shall be used for him.
