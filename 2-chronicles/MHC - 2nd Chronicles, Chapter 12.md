2nd Chronicles, Chapter 12
==========================

Commentary
----------

This chapter gives us a more full account of the reign of Rehoboam than
we had before in Kings and it is a very melancholy account. Methinks we
are in the book of Judges again; for, `I.` Rehoboam and his people did
evil in the sight of the Lord (v. 1). `II.` God thereupon sold them into
the hands of Shishak, king of Egypt, who greatly oppressed them (v. 2-4)
III. God sent a prophet to them, to expound to them the judgment and to
call them to repentance (v. 5). `IV.` They thereupon humbled themselves
(v. 6). `V.` God, upon their repentance, turned from his anger (v. 7, 12)
and yet left them under the marks of his displeasure (v. 8-11). Lastly,
Here is a general character of Rehoboam and his reign, with the
conclusion of it (v. 13-16).

### Verses 1-12

Israel was very much disgraced and weakened by being divided into two
kingdoms; yet the kingdom of Judah, having both the temple and the royal
city, both the house of David and the house of Aaron, might have done
very well if they had continued in the way of their duty; but here we
have all out of order there.

`I.` Rehoboam and his people left God: He forsook the law of the Lord, and
so in effect forsook God, and all Israel with him, v. 1. He had his
happy triennium, when he walked in the way of David and Solomon (ch.
11:17), but it expired, and he grew remiss in the worship of God; in
what instances we are not told, but he fell off, and Judah with him,
here called Israel, because they walked in the evil ways into which
Jeroboam had drawn the kingdom of Israel. Thus he did when he had
established the kingdom and strengthened himself. As long as he thought
his throne tottered he kept to his duty, that he might make God his
friend; but, when he found it stood pretty firmly, he thought he had no
more occasion for religion; he was safe enough without it. Thus the
prosperity of fools destroys them. Jeshurun waxed fat and kicked. When
men prosper, and are in no apprehension of troubles, they are ready to
say to God, Depart from us.

`II.` God quickly brought troubles upon them, to awaken them, and recover
them to repentance, before their hearts were hardened. It was but in the
fourth year of Rehoboam that they began to corrupt themselves, and in
the fifth year the king of Egypt came up against them with a vast army,
took the fenced cities of Judah, and came against Jerusalem, v. 2, 3, 4.
This great calamity coming upon them so soon after they began to desert
the worship of God, by a hand they had little reason to suspect (having
had a great deal of friendly correspondence with Egypt in the last
reign), and coming with so much violence that all the fenced cities of
Judah, which Rehoboam had lately fortified and garrisoned and on which
he relied much for the safety of his kingdom, fell immediately into the
hands of the enemy, without making any resistance, plainly showed that
it was from the Lord, because they had transgressed against him.

`III.` Lest they should not readily or not rightly understand the meaning
of this providence, God by the word explains the rod, v. 5. When the
princes of Judah had all met at Jerusalem, probably in a great council
of war, to concert measures for their own safety in this critical
juncture, he sent a prophet to them, the same that had brought them an
injunction from God not to fight against the ten tribes (ch. 11:2),
Shemaiah by name; he told them plainly that the reason why Shishak
prevailed against them was not because they had been impolitic in the
management of their affairs (which perhaps the princes in this congress
were at this time scrutinizing), but because they had forsaken God. God
never leaves any till they first leave him.

`IV.` The rebukes both of the word and of the rod being thus joined, the
king and princes humbled themselves before God for their iniquity,
penitently acknowledged the sin, and patiently accepted the punishment
of it, saying, The Lord is righteous, v. 6. \"We have none to blame but
ourselves; let God be clear when he judgeth.\" Thus it becomes us, when
we are under the rebukes of Providence, to justify God and judge
ourselves. Even kings and princes must either bend or break before God,
either be humbled or be ruined.

`V.` Upon the profession they made of repentance God showed them some
favour, saved them from ruin, and yet left them under some remaining
fears of the judgment, to prevent their revolt again.

`1.` God, in mercy, prevented the destruction they were now upon the
brink of. Such a vast and now victorious army as Shishak had, having
made themselves masters of all the fenced cities, what could be expected
but that the whole country, and even Jerusalem itself, would in a little
time be theirs? But when God saith, Here shall the proud waves be
stayed, the most threatening force strangely dwindles and becomes
impotent. Here again the destroying angel, when he comes to Jerusalem,
is forbidden to destroy it: \"My wrath shall not be poured out upon
Jerusalem; not at this time, not by this hand, not utterly to destroy
it,\" v. 7, 12. Note, Those that acknowledge God righteous in afflicting
them shall find him gracious. Those that humble themselves before him
shall find favour with him. So ready is the God of mercy to take the
first occasion to show mercy. If we have humbled hearts under humbling
providences, the affliction has done its work, and it shall either be
removed or the property of it altered.

`2.` He granted them some deliverance, not complete, but in part; he gave
them some advantages against the enemy, so that they recruited a little;
he gave them deliverance for a little while, so some. They reformed but
partially, and for a little while, soon relapsing again; and, as their
reformation was, so was their deliverance. Yet it is said (v. 12), in
Judah things went well, and began to look with a better face. `(1.)` In
respect of piety. There were good things in Judah (so it is in the
margin), good ministers, good people, good families, who were made
better by the calamities of their country. Note, In times of great
corruption and degeneracy it is some comfort if there be a remnant among
whom good things ar found; this is a ground of hope in Israel. `(2.)` In
respect of prosperity. In Judah things went ill when all the fenced
cities were taken (v. 4), but when they repented the posture of their
affairs altered, and things went well. Note, If things do not go so well
as we could wish, yet we have reason to take notice of it with
thankfulness if they go better than was to have been expected, better
than formerly, and better than we deserved. We should own God\'s
goodness if he do but grant us some deliverance.

`3.` Yet he left them to smart sorely by the hand of Shishak, both in
their liberty and in their wealth.

`(1.)` In their liberty (v. 8): They shall be his servants (that is, they
shall lie much at his mercy and be put under contribution by him, and
some of them perhaps be taken prisoners and held in captivity by him),
that they may know my service, and the service of the kingdoms of the
countries. They complained, it may be, of the strictness of their
religion, and forsook the law of the Lord (v. 1) because they thought it
a yoke to hard, too heavy, upon them. \"Well,\" saith God, \"let them
better themselves if they can; let the neighbouring princes rule them
awhile, since they are not willing that I should rule them, and let them
try how they like that. They might have served God with joyfulness and
gladness of heart, and would not; let them serve their enemies then in
hunger and thirst (Deu. 28:47, 48), till they think of returning to
their first Master, for then it was better with them,\" Hos. 2:7. This,
some think, is the meaning of Eze. 20:24, 25. Because they despised my
statutes, I gave them statutes that were not good. Note, `[1.]` The more
God\'s service is compared with other services the more reasonable and
easy it will appear. `[2.]` Whatever difficulties or hardships we may
imagine there are in the way of obedience, it is better a thousand times
to go through them than to expose ourselves to the punishment of
disobedience. Are the laws of temperance thought hard? The effects of
intemperance will be much harder. The service of virtue is perfect
liberty; the service of lust is perfect slavery.

`(2.)` In their wealth. The king of Egypt plundered both the temple and
the exchequer, the treasuries of both which Solomon left very full; but
he took them away; yea, he took all, all he could lay his hands on, v.
9. This was what he came for. David and Solomon, who walked in the way
of God, filled the treasuries, one by war and the other by merchandise;
but Rehoboam, who forsook the law of God, emptied them. The taking away
of the golden shields, and the substituting of brazen ones in their
place (v. 9-11), we had an account of before, 1 Ki. 14:25-28.

### Verses 13-16

The story of Rehoboam\'s reign is here concluded, much as the story of
the other reigns concludes. Two things especially are observable
here:-1. That he was at length pretty well fixed in his kingdom, v. 13.
His fenced cities in Judah did not answer his expectation, so he now
strengthened himself in Jerusalem, which he made it his business to
fortify, and there he reigned seventeen years, in the city which the
Lord had chosen to put his name there. This intimates his honour and
privilege, that he had his royal seat in the holy city, which yet was
but an aggravation of his impiety-near the temple, but far from God.
Frequent skirmishes there were between his subjects and Jeroboam\'s,
such as amounted to continual wars, (v. 15), but he held his own, and
reigned, and, as it should seem, did not so grossly forsake the law of
God as he had done (v. 1) in his fourth year. 2. That he was never
rightly fixed in his religion, v. 14. He never quite cast off God; and
yet in this he did evil, that he prepared not, he engaged not, his heart
to seek the Lord. See what the fault is laid upon. `(1.)` He did not serve
the Lord because he did not seek the Lord. He did not pray, as Solomon
did, for wisdom and grace. If we prayed better, we should be every way
better. Or he did not consult the word of God, did not seek to that as
his oracle, nor take directions from it. `(2.)` He made nothing of his
religion because he did not set his heart to it, never minded it with
any closeness of application, and never any hearty disposition to it,
nor ever came up to a steady resolution in it. What little goodness he
had was transient and passed away like the morning cloud. He did evil
because he was never determined for that which is good. Those are easily
drawn by Satan to any evil who are wavering and inconstant in that which
is good and are never persuaded to make religion their business.
