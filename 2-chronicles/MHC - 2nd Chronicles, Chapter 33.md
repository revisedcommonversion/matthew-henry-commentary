2nd Chronicles, Chapter 33
==========================

Commentary
----------

In this chapter we have the history of the reign, `I.` Of Manasseh, who
reigned long. 1. His wretched apostasy from God, and revolt to idolatry
and all wickedness (v. 1-10). 2. His happy return to God in his
affliction; his repentance (v. 11-13), his reformation (v. 15-17), and
prosperity (v. 14), with the conclusion of his reign (v. 18-20). `II.` Of
Amon, who reigned very wickedly (v. 21-23), and soon ended his days
unhappily (v. 24, 25).

### Verses 1-10

We have here an account of the great wickedness of Manasseh. It is the
same almost word for word with that which we had 2 Ki. 21:1-9, and took
a melancholy view of. It is no such pleasing subject that we should
delight to dwell upon it again. This foolish young prince, in
contradiction to the good example and good education his father gave
him, abandoned himself to all impiety, transcribed the abominations of
the heathen (v. 2), ruined the established religion, unravelled his
father\'s glorious reformation (v. 3), profaned the house of God with
his idolatry (v. 4, 5), dedicated his children to Moloch, and made the
devil\'s lying oracles his guides and his counsellors, v. 6. In contempt
of the choice God had made of Sion to be his rest for ever and Israel to
be his covenant-people (v. 8), and the fair terms he stood upon with
God, he embraced other gods, profaned God\'s chosen temple, and
debauched his chosen people. He made them to err, and do worse than the
heathen (v. 9); for, if the unclean spirit returns, he brings with him
seven other spirits more wicked than himself. That which aggravated the
sin of Manasseh was that God spoke to him and his people by the
prophets, but they would not hearken, v. 10. We may here admire the
grace of God in speaking to them, and their obstinacy in turning a deaf
ear to him, that either their badness did not quite turn away his
goodness, but still he waited to be gracious, or that his goodness did
not turn them from their badness, but still they hated to be reformed.
Now from this let us learn, 1. That it is no new thing, but a very sad
thing, for the children of godly parents to turn aside from that good
way of God in which they have been trained. Parents may give many good
things to their children, but they cannot give them grace. 2.
Corruptions in worship are such diseases of the church as it is very apt
to relapse into again even when they seem to be cured. 3. The god of
this world has strangely blinded men\'s minds, and has a wonderful power
over those that are led captive by him; else he could not draw them from
God, their best friend, to depend upon their sworn enemy.

### Verses 11-20

We have seen Manasseh by his wickedness undoing the good that his father
had done; here we have him by repentance undoing the evil that he
himself had done. It is strange that this was not so much as mentioned
in the book of Kings, nor does any thing appear there to the contrary
but that he persisted and perished in his son. But perhaps the reason
was because the design of that history was to show the wickedness of the
nation which brought destruction upon them; and this repentance of
Manasseh and the benefit of it, being personal only and not national, is
overlooked there; yet here it is fully related, and a memorable instance
it is of the riches of God\'s pardoning mercy and the power of his
renewing grace. Here is,

`I.` The occasion of Manasseh\'s repentance, and that was his affliction.
In his distress he did not (like king Ahaz) trespass yet more against
God, but humbled himself and returned to God. Sanctified afflictions
often prove happy means of conversion. What his distress was we are
told, v. 11. God brought a foreign enemy upon him; the king of Babylon,
that courted his father who faithfully served God, invaded him now that
he had treacherously departed from God. He is here called king of
Assyria, because he had made himself master of Assyria, which he would
the more easily do for the defeat of Sennacherib\'s army, and its
destruction before Jerusalem. He aimed at the treasures which the
ambassadors had seen, and all those precious things; but God sent him to
chastise a sinful people, and subdue a straying prince. The captain took
Manasseh among the thorns, in some bush or other, perhaps in his garden,
where he had hid himself. Or it is spoken figuratively: he was perplexed
in his counsels and embarrassed in his affairs. He was, as we say, in
the briers, and knew not which way to extricate himself, and so became
an easy prey to the Assyrian captains, who no doubt plundered his house
and took away what they pleased, as Isaiah had foretold, 2 Ki. 20:17,
18. What was Hezekiah\'s pride was their prey. They bound Manasseh, who
had been held before with the cords of his own iniquity, and carried him
prisoner to Babylon. About what time of his reign this was we are not
told; the Jews say it was in his twenty-second year.

`II.` The expressions of his repentance (v. 12, 13): When he was in
affliction he had time to bethink himself and reason enough too. He saw
what he had brought himself to by his sin. He found the gods he had
served unable to help him. He knew that repentance was the only way of
restoring his affairs; and therefore to him he returned from whom he had
revolted. 1. He was convinced the Jehovah is the only living and true
God: Then he knew (that is, he believed and considered) that the Lord he
was God. He might have known it at a less expense if he would have given
due attention and credit to the word written and preached: but it was
better to pay thus dearly for the knowledge of God than to perish in
ignorance and unbelief. Had he been a prince in the palace of Babylon,
it is probable he would have been confirmed in his idolatry; but, being
a captive in the prisons of Babylon, he was convinced of it and
reclaimed from it. 2. He applied to him as his God now, renouncing all
others, and resolving to cleave to him only, the God of his fathers, and
a God on covenant with him. 3. He humbled himself greatly before him,
was truly sorry for his sins, ashamed of them, and afraid of the wrath
of God. It becomes sinners to humble themselves before the face of that
God whom they have offended. It becomes sufferers to humble themselves
under the hand of that God who corrects them, and to accept the
punishment of their iniquity. Our hearts should be humbled under
humbling providences; then we accommodate ourselves to them, and answer
God\'s end in them. 4. He prayed to him for the pardon of sin and the
return of his favour. Prayer is the relief of penitents, the relief of
the afflicted. That is a good prayer, and very pertinent in this case,
which we find among the apocryphal books, entitled, The prayer of
Manasses, king of Judah, when he was holden captive in Babylon. Whether
it was his or no is uncertain; if it was, in it he gives glory to God as
the God of their fathers and their righteous seed, as the Creator of the
world, a God whose anger is insupportable, and yet his merciful promise
unmeasurable. He pleads that God has promised repentance and forgiveness
to those that have sinned, and has appointed repentance unto sinners,
that they may be saved, not unto the just, as to Abraham, Isaac, and
Jacob, but to me (says he) that am a sinner; for I have sinned above the
number of the sands of the sea: so he confesses his sin largely, and
aggravates it. He prays, Forgive me, O Lord! forgive me, and destroy me
not; he pleads, Thou art the God of those that repent, etc., and
concludes, Therefore I will praise thee for ever, etc.

`III.` God\'s gracious acceptance of his repentance: God was entreated of
him, and heard his supplication. Though affliction drive us to God, he
will not therefore reject us if in sincerity we seek him, for
afflictions are sent on purpose to bring us to him. As a token of God\'s
favour to him, he made a way for his escape. Afflictions are continued
no longer than till they have done their work. When Manasseh is brought
back to his God and to his duty he shall soon be brought back to his
kingdom. See how ready God is to accept and welcome returning sinners,
and how swift to show mercy. Let not great sinners despair, when
Manasseh himself, upon his repentance, found favour with God; in him God
showed forth a pattern of long-suffering, as 1 Tim. 1:16; Isa. 1:18.

`IV.` The fruits meet for repentance which he brought forth after his
return to his own land, v. 15, 16. 1. He turned from his sins. He took
away the strange gods, the images of them, and that idol (whatever it
was) which he had set up with so much solemnity in the house of the
Lord, as if it had been master of that house. He cast out all the
idolatrous altars that were in the mount of the house and in Jerusalem,
as detestable things. Now (we hope) he loathed them as much as ever he
had loved them, and said to them, Get you hence, Isa. 30:22. \"What have
I to do any more with idols? I have had enough of them.\" 2. He returned
to his duty; for he repaired the altar of the Lord, which had either
been abused and broken down by some of the idolatrous priests, or, at
least, neglected and gone out of repair. He sacrificed thereon
peace-offerings to implore God\'s favour, and thank-offerings to praise
him for his deliverance. Nay, he now used his power to reform his
people, as before he had abused it to corrupt them: He commanded Judah
to serve the Lord God of Israel. Note, Those that truly repent of their
sins will not only return to God themselves, but will do all they can to
recover those that have by their example been seduced and drawn away
from God; else they do not thoroughly (as they ought) undo what they
have done amiss, nor make the plaster as wide as the wound. We find that
he prevailed to bring them off from their false gods, but not from their
high places, v. 17. They still sacrificed in them, yet to the Lord their
God only; Manasseh could not carry the reformation so far as he had
carried the corruption. It is an easy thing to debauch men\'s manners,
but not so easy to reform them again.

`V.` His prosperity, in some measure, after his repentance. He might
plainly see it was sin that ruined him; for, when he returned to God in
a way of duty, God returned to him in a way of mercy: and then he built
a wall about the city of David (v. 14), for by sin he had unwalled it
and exposed it to the enemy. He also put captains of war in the fenced
cities for the security of his country. Josephus says that all the rest
of his time he was so changed for the better that he was looked upon as
a very happy man.

Lastly, Here is the conclusion of his history. The heads of those things
for a full narrative of which we are referred to the other writings that
were then extant are more than of any of the kings, v. 18, 19. A
particular account, it seems, was kept, 1. Of all his sin, and his
trespass, the high places he built, the groves and images he set up,
before he was humbled. Probably this was taken from his own confession
which he made of his sin when God gave him repentance, and which he left
upon record, in a book entitled, The words of the seers. To those seers
that spoke to him (v. 18) to reprove him for his sin he sent his
confession when he repented, to be inserted in their memoirs, as a token
of his gratitude to them for their kindness in reproving him. Thus it
becomes penitents to take shame to themselves, to give thanks to their
reprovers, and warning to others. 2. Of the words of the seers that
spoke to him in the name of the Lord (v. 10, 18), the reproofs they gave
him for his sin and their exhortations to repentance. Note, Sinners
ought to consider, that, how little notice soever they take of them, an
account is kept of the words of the seers that speak to them from God to
admonish them of their sins, warn them of their danger, and call them to
their duty, which will be produced against them in the great day. 3. Of
his prayer to God (this is twice mentioned as a remarkable thing) and
how God was entreated of him. This was written for the generations to
come, that the people that should be created might praise the Lord for
his readiness to receive returning prodigals. Notice is taken of the
place of his burial, not in the sepulchres of the kings, but in his own
house; he was buried privately, and nothing of that honour was done him
at his death that was done to his father. Penitents may recover their
comfort sooner than their credit.

### Verses 21-25

We have little recorded concerning Amon, but enough unless it were
better. Here is,

`I.` His great wickedness. He did as Manasseh had done in the days of his
apostasy, v. 22. Those who think this an evidence that Manasseh did not
truly repent forget how many good kings had wicked sons. Only it should
seem that Manasseh was in this defective, that, when he cast out the
images, he did not utterly deface and destroy them, according to the law
which required Israel to burn the images with fire, Deu. 7:2. How
necessary that law was this instance shows; for the carved images being
only thrown by, and not burnt, Amon knew where to find them, soon set
them up, and sacrificed to them. It is added, to represent him
exceedingly sinful and to justify God in cutting him off so soon, 1.
That he out-did his father in sinning: He trespassed more and more, v.
23. His father did ill, but he did worse. Those that were joined to
idols grew more and more mad upon them. 2. That he came short of his
father in repenting: He humbled not himself before the Lord, as his
father had humbled himself. He fell like him, but did not get up again
like him. It is not so much sin as impenitence in sin that ruins men,
not so much that they offend as that they do not humble themselves for
their offences, not the disease, but the neglect of the remedy.

`II.` His speedy destruction. He reigned but two years and then his
servants conspired against him and slew him, v. 24. Perhaps when Amon
sinned as his father did in the beginning of his days he promised
himself that he should repent as his father did in the latter end of his
days. But his case shows what a madness it is to presume upon that. If
he hoped to repent when he was old, he was wretchedly disappointed; for
he was cut off when he was young. He rebelled against God, and his own
servants rebelled against him. Herein God was righteous, but they were
wicked, and justly did the people of the land put them to death as
traitors. The lives of kings are particularly under the protection of
Providence and the laws both of God and man.
