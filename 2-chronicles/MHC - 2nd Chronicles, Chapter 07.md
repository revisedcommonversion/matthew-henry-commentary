2nd Chronicles, Chapter 7
=========================

Commentary
----------

In this chapter we have God\'s answer to Solomon\'s prayer. `I.` His
public answer by fire from heaven, which consumed the sacrifices (v. 1),
with which the priests and people were much affected (v. 2, 3). By that
token of God\'s acceptance they were encouraged to continue the
solemnities of the feast for fourteen days, and Solomon was encouraged
to pursue all his designs for the honour of God (v. 4-11). `II.` His
private answer by word of mouth, in a dream or vision of the night (v.
12-22). Most of these things we had before, 1 Ki. 8 and 9.

### Verses 1-11

Here is, `I.` The gracious answer which God immediately made to Solomon\'s
prayer: The fire came down from heaven and consumed the sacrifice, v. 1.
In this way God testified his acceptance of Moses (Lev. 9:24), of Gideon
(Jdg. 6:21), of David (1 Chr. 21:26), of Elijah (1 Ki. 18:38); and, in
general, to accept the burnt-sacrifice is, in the Hebrew phrase, to turn
it to ashes, Ps. 20:3. The fire came down here, not upon the killing of
the sacrifices, but the praying of the prayer.

`1.` This fire intimated that God was, `(1.)` Glorious in himself; for our
God is a consuming fire, terrible even in his holy places. This fire,
breaking forth (as it is probable) out of the thick darkness, made it
the more terrible, as on Mount Sinai, Ex. 24:16, 17. The sinners in Sion
had reason to be afraid at that sight, and to say, Who among us shall
dwell near this devouring fire? Isa. 33:14. And yet, `(2.)` Gracious to
Israel; for this fire, which might justly have consumed them, fastened
upon the sacrifice which was offered in their stead, and consumed that,
by which God signified to them that he accepted their offerings and that
his anger was turned away from them.

`2.` Let us apply this, `(1.)` To the suffering of Christ. When it pleased
the Lord to bruise him, and put him to grief, in that he showed his
good-will to men, having laid on him the iniquity of us all. His death
was our life, and he was made sin and a curse that we might inherit
righteousness and a blessing. That sacrifice was consumed that we might
escape. Here am I, let these go their way. `(2.)` To the sanctification of
the Spirit, who descends like fire, burning up our lusts and
corruptions, those beasts that must be sacrificed or we are undone, and
kindling in our souls a holy fire of pious and devout affections, always
to be kept burning on the altar of the heart. The surest evidence of
God\'s acceptance of our prayers is the descent of the holy fire upon
us. Did not our hearts burn within us? Lu. 24:32. As a further evidence
that God accepted Solomon\'s prayer, still the glory of the Lord filled
the house. The heart that is thus filled with a holy awe and reverence
of the divine glory, the heart to which God manifests himself in his
greatness, and (which is no less his glory) in his goodness, is thereby
owned as a living temple.

`II.` The grateful return made to God for this gracious token of his
favour.

`1.` The people worshipped and praised God, v. 3. When they saw the fire
of God come down from heaven thus they did not run away affrighted, but
kept their ground in the courts of the Lord, and took occasion from it,
`(1.)` With reverence to adore the glory of God: They bowed their faces to
the ground and worshipped, thus expressing their awful dread of the
divine majesty, their cheerful submission to the divine authority, and
the sense they had of their unworthiness to come into God\'s presence
and their inability to stand before the power of his wrath. `(2.)` With
thankfulness to acknowledge the goodness of God; even when the fire of
the Lord came down they praised him, saying, He is good, for his mercy
endureth for ever. This is a song never out of season, and for which our
hearts and tongues should be never out of tune. However it be, yet God
is good. When he manifests himself as a consuming fire to sinners, his
people can rejoice in him as their light. Nay, they had reason to say
that in this God was good. \"It is of the Lord\'s mercies that we are
not consumed, but the sacrifice in our stead, for which we are bound to
be very thankful.\"

`2.` The king and all the people offered sacrifices in abundance, v. 4,
5. With these they feasted this holy fire, and bade it welcome to the
altar. They had offered sacrifices before, but now they increased them.
Note, The tokens of God\'s favour to us should enlarge our hearts in his
service, and make us to abound therein more and more. The king\'s
example stirred up the people. Good work is then likely to go on when
the leaders of a people lead in it. The sacrifices were so numerous that
the altar could not contain them all; but, rather than any of them
should be turned back (though we may suppose the blood of them all was
sprinkled upon the altar), the flesh of the burnt-offerings and the fat
of the peace-offerings were burnt in the midst of the court (v. 7),
which Solomon either hallowed for that service or hallowed by it. In
case of necessity the pavement might be an altar.

`3.` The priests did their part; they waited on their offices, and the
singers and musicians on theirs (v. 6), with the instruments that David
made, and the hymn that David had put into their hand, as some think it
may be read (meaning that 1 Chr. 16:7), or, as we read it, when David
praised by their ministry. He employed, directed, and encouraged them in
this work of praising God; and therefore their performances were
accepted as his act, and he is said to praise by their ministry.

`4.` The whole congregation expressed the greatest joy and satisfaction
imaginable. They kept the feast of the dedication of the altar seven
days, from the second to the ninth; the tenth day was the day of
atonement, when they were to afflict their souls for sin, and that was
not unseasonable in the midst of their rejoicings; on the fifteenth day
began the feast of tabernacles, which continued to the twenty-second,
and they did not separate till the twenty-third. We must never grudge
the time that we spend in the worship of God and communion with him, nor
think it long, or grow weary of it.

`5.` Solomon went on in his work, and prosperously effected all he
designed for the adorning both of God\'s house and his own, v. 11. Those
that begin with the service of God are likely to go on successfully in
their own affairs. It was Solomon\'s praise that what he undertook he
went through with, and it was by the grace of God that he prospered in
it.

### Verses 12-22

That God accepted Solomon\'s prayer appeared by the fire from heaven.
But a prayer may be accepted and yet not answered in the letter of it;
and therefore God appeared to him in the night, as he did once before
(ch. 1:7), and after a day of sacrifice too, as then, and gave him a
peculiar answer to his prayer. We had the substance of it before, 1 Ki.
9:2-9.

`I.` He promised to own this house for a house of sacrifice to Israel and
a house of prayer for all people (Isa. 56:7): My name shall be there for
ever (v. 12, 16), that is, \"There will I make myself known, and there
will I be called upon.\"

`II.` He promised to answer the prayers of his people that should at any
time be made in that place, v. 13-15. National judgments are here
supposed (v. 13), famine, and pestilence, and perhaps war, for by the
locusts devouring the land meant enemies as greedy as locusts, and
laying all waste. 2. National repentance, prayer, and reformation, are
required, v. 14. God expects that his people who are called by his name,
if they have dishonoured his name by their iniquity, should honour it by
accepting the punishment of their iniquity. They must be humble
themselves under his hand, must pray for the removal of the judgment,
must seek the face and favour of God; and yet all this will not do
unless they turn from their wicked ways, and return to the God from whom
they have revolted. 3. National mercy is then promised, that God will
forgive their sin, which brought the judgment upon them, and then heal
their land, redress all their grievances. Pardoning mercy makes ways for
healing mercy, Ps. 103:3; Mt. 9:2.

`III.` He promised to perpetuate Solomon\'s kingdom, upon condition that
he persevered in his duty, v. 17, 18. If he hoped for the benefit of
God\'s covenant with David, he must imitate the example of David. But he
set before him death as well as life, the curse as well as the blessing.
`1.` He supposed it possible that though they had this temple built to the
honour of God, yet they might be drawn aside to worship other gods, v.
19. He knew their proneness to backslide into that sin. 2. He threatened
it as certain that, if they did so, it would certainly be the ruin of
both church and state. `(1.)` It would be the ruin of their state, v. 20.
\"Though they have taken deep root, and taken root long, in this good
land, yet I will pluck them up by the roots, extirpate the whole nation,
pluck them up as men pluck up weeds out of their garden, which are
thrown to the dunghill.\" `(2.)` It would be the ruin of their church.
This sanctuary would be no sanctuary to them, to protect them from the
judgment of God, as they imagined, saying, The temple of the Lord are
we, Jer. 7:4. \"This house which is high, not only for the magnificence
of its structure, but for the designed ends and uses of it, shall be an
astonishment, it shall come down wonderfully (Lam. 1:9), to the
amazement of all the neighbours.\"
