2nd Chronicles, Chapter 36
==========================

Commentary
----------

We have here, `I.` A short but sad account of the utter ruin of Judah and
Jerusalem within a few years after Josiah\'s death. 1. The history of it
in the unhappy reigns of Jehoahaz for three months (v. 1-4), Jehoiakim
(v. 5-8) for eleven years, Jehoiachin three months (v. 9, 10), and
Zedekiah eleven years (v. 11). Additions were made to the national
guilt, and advances towards the national destruction, in each of those
reigns. The destruction was, at length, completed in the slaughter of
multitudes (v. 17), the plundering and burning of the temple and all the
palaces, the desolation of the city (v. 18, 19), and the captivity of
the people that remained (v. 20). 2. Some remarks upon it-that herein
sin was punished, Zedekiah\'s wickedness (v. 12, 13), the idolatry the
people were guilty of (v. 14), and their abuse of God\'s prophets (v.
15, 16). The word of God was herein fulfilled (v. 21). `II.` The dawning
of the day of their deliverance in Cyrus\'s proclamation (v. 22, 23).

### Verses 1-10

The destruction of Judah and Jerusalem is here coming on by degrees. God
so ordered it to show that he has no pleasure in the ruin of sinners,
but had rather they would turn and live, and therefore gives them both
time and inducement to repent and waits to be gracious. The history of
these reigns was more largely recorded in the last three chapters of the
second of Kings. 1. Jehoahaz was set up by the people (v. 1), but in one
quarter of a year was deposed by Pharaoh-necho, and carried a prisoner
to Egypt, and the land fined for setting him up, v. 2-4. Of this young
prince we hear no more. Had he trodden in the steps of his father\'s
piety he might have reigned long and prospered; but we are told in the
Kings that he did evil in the sight of the Lord, and therefore his
triumphing was short and his joy but for a moment. 2. Jehoiakim was set
up by the king of Egypt, an old enemy to their land, gave what king he
pleased to the kingdom and what name he pleased to the king! v. 4. He
made Eliakim king, and called him Jehoiakim, in token of his authority
over him. Jehoiakim did that which was evil (v. 5), nay, we read of the
abominations which he did (v. 8); he was very wild and wicked.
Idolatries generally go under the name of abominations. We hear no more
of the king of Egypt, but the king of Babylon came up against him (v.
6), seized him, and bound him with a design to carry him to Babylon;
but, it seems, he either changed his mind, and suffered him to reign as
his vassal, or death released the prisoner before he was carried away.
However the best and most valuable vessels of the temple were now
carried away and made use of in Nebuchadnezzar\'s temple in Babylon (v.
7); for, we may suppose, no temple in the world was so richly furnished
as that of Jerusalem. The sin of Judah was that they had brought the
idols of the heathen into God\'s temple; and now their punishment was
that the vessels of the temple were carried away to the service of the
gods of the nations. If men will profane God\'s institutions by their
sins, it is just with God to suffer them to be profaned by their
enemies. These were the vessels which the false prophets flattered the
people with hopes of the return of, Jer. 27:16. But Jeremiah told them
that the rest should go after them (Jer. 27:21, 22), and they did so.
But, as the carrying away of these vessels to Babylon began the calamity
of Jerusalem, so Belshazzar\'s daring profanation of them there filled
the measure of the iniquity of Babylon; for, when he drank wine in them
to the honour of his gods, the handwriting on the wall presented him
with his doom, Dan. 5:3, etc. In the reference to the book of the Kings
concerning this Jehoiakim mention is made of that which was found in him
(v. 8), which seems to be meant of the treachery that was found in him
towards the king of Babylon; but some of the Jewish writers understand
it of certain private marks or signatures found in his dead body, in
honour of his idol, such cuttings as God had forbidden, Lev. 19:28. 3.
Jehoiachin, or Jeconiah, the son of Jehoiakim, attempted to reign in his
stead, and reigned long enough to show his evil inclination; but, after
three months and ten days, the king of Babylon sent and fetched him away
captive, with more of the goodly vessels of the temple. He is here said
to be eight years old, but in Kings he is said to be eighteen when he
began to reign, so that this seems to be a mistake of the transcriber,
unless we suppose that his father took him at eight years old to join
with him in the government, as some think.

### Verses 11-21

We have here an account of the destruction of the kingdom of Judah and
the city of Jerusalem by the Chaldeans. Abraham, God\'s friend, was
called out of that country, from Ur of the Chaldees, when God took him
into covenant and communion with himself; and now his degenerate seed
were carried into that country again, to signify that they had forfeited
all that kindness wherewith they had been regarded for the father\'s
sake, and the benefit of that covenant into which he was called; all was
now undone again. Here we have,

`I.` The sins that brought this desolation.

`1.` Zedekiah, the king in whose days it came, brought it upon himself by
his own folly; for he conducted himself very ill both towards God and
towards the king of Babylon. `(1.)` If he had but made God his friend,
that would have prevented the ruin. Jeremiah brought him messages from
God, which, if he had given due regard to them, might have secured a
lengthening of his tranquillity; but it is here charged upon him that he
humbled not himself before Jeremiah, v. 12. It was expected that this
mighty prince, high as he was, should humble himself before a poor
prophet, when he spoke from the mouth of the Lord, should submit to his
admonitions and be amended by them, to his counsels and be ruled by
them, should lay himself under the commanding power of the word of God
in his mouth; and, because he would not thus make himself a servant to
God, he was made a slave to his enemies. God will find some way or other
to humble those that will not humble themselves. Jeremiah, as a prophet,
was set over the nations and kingdoms (Jer. 1:10), and, as mean a figure
as he made, whoever would not humble themselves before him found that it
was at their peril. `(2.)` If he had but been true to his covenant with
the king of Babylon, that would have prevented his ruin; but he rebelled
against him, though he had sworn to be his faithful tributary, and
perfidiously violated his engagements to him, v. 13. It was this that
provoked the king of Babylon to deal so severely with him as he did. All
nations looked upon an oath as a sacred thing, and on those that durst
break through the obligations of it as the worst of men, abandoned of
God and to be abhorred by all mankind. If therefore Zedekiah falsify his
oath, when, lo, he has given his hand, he shall not escape, Eze. 17:18.
Though Nebuchadnezzar was a heathen, an enemy, yet if, having sworn to
him, he be false to him, he shall know there is a God to whom vengeance
belongs. The thing that ruined Zedekiah was not only that he turned not
to the Lord God of Israel, but that he stiffened his neck and hardened
his heart from turning to him, that is, he as obstinately resolved not
to return to him, would not lay his neck under God\'s yoke nor his heart
under the impressions of his word, and so, in effect, he would not be
healed, he would not live.

`2.` The great sin that brought this destruction was idolatry. The
priests and people went after the abominations of the heathen, forsook
the pure worship of God for the lewd and filthy rites of the Pagan
superstition, and so polluted the house of the Lord, v. 14. The priests,
the chief of the priests, who should have opposed idolatry, were
ring-leaders in it. That place is not far from ruin in which religion is
already ruined.

`3.` The great aggravation of their sin, and that which filled the
measure of it, was the abuse they gave to God\'s prophets, who were sent
to call them to repentance, v. 15, 16. Here we have, `(1.)` God\'s tender
compassion towards them in sending prophets to them. Because he was the
God of their fathers, in covenant with them, and whom they worshipped
(though this degenerate race forsook him), therefore he sent to them by
his messengers, to convince them of their sin and warn them of the ruin
they would bring upon themselves by it, rising up betimes and sending,
which denotes not only that he did it with the greatest care and concern
imaginable, as men rise betimes to set their servants to work when their
heart is upon their business, but that, upon their first deviation from
God to idols, if they took but one step that way, God immediately sent
to them by his messengers to reprove them for it. He gave them early
timely notice both of their duty and danger. Let this quicken us to seek
God early, that he rises betimes to send to us. The prophets that were
sent rose betimes to speak to them, were diligent and faithful in their
office, lost no time, slipped no opportunity of dealing with them; and
therefore God is said to rise betimes. The more pains ministers take in
their work the more will the people have to answer for if it be all in
vain. The reason given why God by his prophets did thus strive with them
is because he had compassion on his people and on his dwelling-lace, and
would by these means have prevented their ruin. Note, The methods God
takes to reclaim sinners by his word, by ministers, by conscience, by
providences, are all instances of his compassion towards them and his
unwillingness that any should perish. `(2.)` Their base and disingenuous
carriage towards God (v. 16): They mocked the messengers of God (which
was a high affront to him that sent them), despised his word in their
mouths, and not only so, but misused the prophets, treating them as
their enemies. The ill usage they gave Jeremiah who lived at this time,
and which we read much of in the book of his prophecy, is an instance of
this. This was an evidence of an implacable enmity to God, and an
invincible resolution to go on in their sins. This brought wrath upon
them without remedy, for it was sinning against the remedy. Nothing is
more provoking to God than abuses given to his faithful ministers; for
what is done against them he takes as done against himself. Saul, Saul,
why persecutest thou me? Persecution was the sin that brought upon
Jerusalem its final destruction by the Romans. See Mt. 23:34-37. Those
that mock at God\'s faithful ministers, and do all they can to render
them despicable or odious, that vex and misuse them, to discourage them
and to keep others from hearkening to them, should be reminded that a
wrong done to an ambassador is construed as done to the prince that
sends him, and that the day is coming when they will find it would have
been better for them if they had been thrown into the sea with a
mill-stone about their necks; for hell is deeper and more dreadful.

`II.` The desolation itself, and some few of the particular so fit, which
we had more largely 2 Ki. 25:1. Multitudes were put to the sword, even
in the house of their sanctuary (v. 17), whither they fled for refuge,
hoping that the holiness of the place would be their protection. But how
could they expect to find it so when they themselves had polluted it
with their abominations? v. 14. Those that cast off the dominion of
their religion forfeit all the benefit and comfort of it. The Chaldeans
not only paid no reverence to the sanctuary, but showed no natural pity
either to the tender sex or to venerable age. They forsook God, who had
compassion on them (v. 15), and would have none of him; justly therefore
are they given up into the hands of cruel men, for they had no
compassion on young man or maiden. 2. All the remaining vessels of the
temple, great and small, and all the treasures, sacred and secular, the
treasures of God\'s house and of the king and his princes, were seized,
and brought to Babylon, v. 18. 3. The temple was burnt, the walls of
Jerusalem were demolished, the houses (called here the palaces, as Ps.
48:3, so stately, rich, and sumptuous were they) laid in ashes, and all
the furniture, called here the goodly vessels thereof, destroyed, v. 19.
Let us see where what woeful havock sin makes, and, as we value the
comfort and continuance of our estates, keep that worm from the root of
them. 4. The remainder of the people that escaped the sword were carried
captives to Babylon (v. 20), impoverished, enslaved, insulted, and
exposed to all the miseries, not only of a strange and barbarous land,
but of an enemy\'s land, where those that hated them bore rule over
them. They were servants to those monarchs, and no doubt were ruled with
rigour so long as that monarchy lasted. Now they sat down by the rivers
of Babylon, with the streams of which they mingled their tears, Ps.
137:1. And though there, it should seem, they were cured of idolatry,
yet, as appears by the prophet Ezekiel, they were not cured of mocking
the prophets. 5. The land lay desolate while they were captives in
Babylon, v. 21. That fruitful land, the glory of all lands, was now
turned into a desert, not tilled, nor husbanded. The pastures were not
clothed as they used to be with flocks, nor the valleys with corn, but
all lay neglected. Now this may be considered, `(1.)` As the just
punishment of their former abuse of it. They had served Baal with its
fruits; cursed therefore is the ground for their sakes. Now the land
enjoyed her sabbaths; (v. 21), as God had threatened by Moses, Lev.
26:34, and the reason there given (v. 35) is, \"Because it did not rest
on your sabbaths; you profaned the sabbath-day, did not observe the
sabbatical year.\" They many a time ploughed and sowed their land in the
seventh year, when it should have rested, and now it lay unploughed and
unsown for ten times seven years. Note, God will be no loser in his
glory at last by the disobedience of men: if the tribute be not paid, he
will distrain and recover it, as he speaks, Hos. 2:9. If they would not
let the land rest, God would make it rest whether they would or no. Some
think they had neglected the observance of seventy sabbatical years in
all, and just so many, by way of reprisal, the land now enjoyed; or, if
those that had been neglected were fewer, it was fit that the law should
be satisfied with interest. We find that one of the quarrels God had
with them at this time was for not observing another law which related
to the seventh year, and that was the release of servants; see Jer.
34:13, etc. `(2.)` Yet we may consider it as giving some encouragement to
their hopes that they should, in due time, return to it again. Had
others come and taken possession of it, they might have despaired of
ever recovering it; but, while it lay desolate, it did, as it were, lie
waiting for them again, and refuse to acknowledge any other owners.

### Verses 22-23

These last two verses of this book have a double aspect. 1. They look
back to the prophecy of Jeremiah, and show how that was accomplished, v.
22. God had, by him, promised the restoring of the captives and the
rebuilding of Jerusalem, at the end of seventy years; and that time to
favour Sion, that set time, came at last. After a long and dark night
the day-spring from on high visited them. God will be found true to
every word he has spoken. 2. They look forward to the history of Ezra,
which begins with the repetition of these last two verses. They are
there the introduction to a pleasant story; here they are the conclusion
of a very melancholy one; and so we learn from them that, though God\'s
church be cast down, it is not cast off, though his people be corrected,
they are not abandoned, though thrown into the furnace, yet not lost
there, nor left there any longer than till the dross be separated.
Though God contend long, he will not contend always. The Israel of God
shall be fetched out of Babylon in due time, and even the dry bones made
to live. It may be long first; but the vision is for an appointed time,
and at the end it shall speak and not lie; therefore, though it tarry,
wait for it.
