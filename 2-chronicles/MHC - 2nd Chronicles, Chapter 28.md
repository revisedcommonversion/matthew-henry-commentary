2nd Chronicles, Chapter 28
==========================

Commentary
----------

This chapter is the history of the reign of Ahaz the son of Jotham; a
bad reign it was, and which helped to augment the fierce anger of the
Lord. We have here, `I.` His great wickedness (v. 1-4). `II.` The trouble he
brought himself into by it (v. 5-8). `III.` The reproof which God sent by
a prophet to the army of Israel for trampling upon their brethren of
Judah, and the obedient ear they gave to that reproof (v. 9-15). `IV.` The
many calamities that followed to Ahaz and his people (v. 16-21). `V.` The
continuance of his idolatry notwithstanding (v. 22-25), and so his story
ends (v. 26, 27).

### Verses 1-5

Never surely had a man greater opportunity of doing well than Ahaz had,
finding things in a good posture, the kingdom rich and strong and
religion established; and yet here we have him in these few verses, 1.
Wretchedly corrupted and debauched. He had had a good education given
him and a good example set him: but parents cannot give grace to their
children. All the instructions he had were lost upon him: He did not
that which was right in the sight of the Lord (v. 1), nay, he did a
great deal that was wrong, a wrong to God, to his own soul, and to his
people; he walked in the way of the revolted Israelites and the devoted
Canaanites, made molten images and worshipped them, contrary to the
second commandment; nay, he made them for Baalim, contrary to the first
commandment. he forsook the temple of the Lord and sacrificed and burnt
incense on the hills, as if they would place him nearer heaven, and
under every green tree, as if they would signify the protection and
influence of heaven by their shade and dropping. To complete his
wickedness, as one perfectly divested of all natural affection as well
as religion and perfectly devoted to the service and interest of the
great enemy of mankind, he burnt his children in the fire to Moloch (v.
3), not thinking it enough to dedicate them to that infernal fiend by
causing them to pass through the fire. See what an absolute sway the
prince of the power of the air bears among the children of disobedience.
2. Wretchedly spoiled and made a prey of. When he forsook God, and at a
vast expense put himself under the protection of false gods, God, who of
right was his God, delivered him into the hands of his enemies, v. 5.
`(1.)` The Syrians insulted him and triumphed over him, beat him in the
field and carried away a great many of his people into captivity. `(2.)`
The king of Israel, though an idolater too, was made a scourge to him,
and smote him with a great slaughter. The people suffered by these
judgments: their blood was shed, their country wasted, their families
ruined; for when they had a good king, though they did corruptly (ch.
27:2), yet then his goodness sheltered them; but now that they had a bad
one all the defence had departed from them and an inundation of
judgments broke in upon them. Those that knew not their happiness in the
foregoing reign were taught to value it by the miseries of this reign.

### Verses 6-15

We have here,

`I.` Treacherous Judah under the rebukes of God\'s providence, and they
are very severe. Never was such bloody work made among them since they
were a kingdom, and by Israelites too. Ahaz walked in the ways of the
kings of Israel, and the king of Israel was the instrument God made use
of for his punishment. It is just with God to make those our plagues
whom we make our patterns or make ourselves partners with in sin. A war
broke out between Judah and Israel, in which Judah was worsted. For, 1.
There was a great slaughter of men in the field of battle. Vast numbers
(120,000 men, and valiant men too at other times) were slain (v. 6) and
some of the first rank, the king\'s son for one. He had sacrificed some
of this sons to Moloch; justly therefore is this sacrificed to the
divine vengeance. Here is another that was next the king, his friend,
the prime-minister of state, or perhaps next him in the battle, so that
the king himself had a narrow escape, v. 7. The kingdom of Israel was
not strong at this time, and yet strong enough to bring this great
destruction upon Judah. But certainly so many men, great men, stout men,
could not have been cut off in one day if they had not been strangely
dispirited both by the consciousness of their own guilt and by the
righteous hand of God upon them. Even valiant men were numbered as sheep
for the slaughter, and became an easy prey to the enemy because they had
forsaken the Lord God of their fathers, and he had therefore forsaken
them. 2. There was a great captivity of women and children, v. 8. When
the army in the field was routed, the cities, and towns, and country
villages, were all easily stripped, the inhabitants taken for slaves,
and their wealth for a prey.

`II.` Even victorious Israel under the rebuke of God\'s word for the bad
principle they had gone upon in making war with Judah and the bad use
they had made of their success, and the good effect of this rebuke. Here
is,

`1.` The message which God sent them by a prophet, who went out to meet
them, not to applaud their valour or congratulate them on their victory,
though they returned laden with spoils and triumphs, but in God\'s name
to tell them of their faults and warn them of the judgments of God.

`(1.)` He told them how they came by this victory of which they were so
proud. It was not because God favoured them, or that they had merited it
at his hand, but because he was wroth with Judah, and made them the rod
of his indignation. Not for your righteousness, be it known to you, but
for their wickedness (Deu. 9:5) they are broken off; therefore be not
you high-minded, but fear lest God also spare not you, Rom. 11:20, 21.

`(2.)` He charged them with the abuse of the power God had given them over
their brethren. Those understand not what victory is who think it gives
them authority to do what they will, and that the longest sword is the
clearest claim to lives and estates (Jusque datum sceleri-might is
right); no, as it is impolitic not to use a victory, so it is impious to
abuse it. The conquerors are here reproved, `[1.]` For the cruelty of
the slaughter they had made in the field. They had indeed shed the blood
of war in war; we suppose that to be lawful, but it turned into sin to
them, because they did it from a bad principle of enmity to their
brethren and after a bad manner, with a barbarous fury, a rage reaching
up to heaven, that is, that cried to God for vengeance against such
bloody men, that delighted in military execution. Those that serve
God\'s justice, if they do it with rage and a spirit of revenge, make
themselves obnoxious to it, and forfeit the honour of acting for him;
for the wrath of man worketh not the righteousness of God. `[2.]` For
the imperious treatment they gave their prisoners. \"You now purpose to
keep them under, to use them or sell them as slaves, though they are
your brethren and free-born Israelites.\" God takes notice of what men
purpose, as well as of what they say and do.

`(3.)` He reminded them of their own sins, by which they also were
obnoxious to the wrath of God: Are there not with you, even with you,
sins against the Lord your God? v. 10. He appeals to their own
consciences, and to the notorious evidence of the thing. \"Though you
are now made the instruments of correcting Judah for sin, yet do not
think that you are therefore innocent yourselves; no, you also are
guilty before God.\" This is intended as a check, `[1.]` To their
triumph in their success. \"You are sinners, and it ill becomes sinners
to be proud; you have carried the day now, but be not secure, the wheel
may ere long return upon yourselves, for, if judgment begin thus with
those that have the house of God among them, what shall be the end of
such as worship the calves?\" `[2.]` To their severity towards their
brethren. \"You have now got them under, but you ought to show mercy to
them, for you yourselves are undone if you do not find mercy with God.
It ill becomes sinners to be cruel. You have transgressions enough to
answer for already, and need not add this to the rest.\"

`(4.)` He commanded them to release the prisoners, and to send them home
again carefully (v. 11); \"for you having sinned, the fierce wrath of
God is upon you, and there is no other way of escaping it than by
showing mercy.\"

`2.` The resolution of the princes thereupon not to detain the prisoners.
They stood up against those that came from the war, though flushed with
victory, and told them plainly that they should not bring their captives
into Samaria, v. 12, 13. They had sin enough already to answer for, and
would have nothing done to add to their trespass. In this they
discovered an obedient regard to the word of God by his prophet and a
tender compassion towards their brethren, which was wrought in them by
the tender mercy of God; for he regarded the affliction of this poor
people, and hears their cry, and made them to be pitied of all those
that carried them captive, Ps. 106:44, 46.

`3.` The compliance of the soldiers with the resolutions of the princes
in this matter, and the dismission of the captives thereupon. `(1.)` The
armed men, though being armed they might be force have maintained their
title to what they got by the sword, acquiesced, and left their captives
and the spoil to the disposal of the princes (v. 14), and herein they
showed more truly heroic bravery than they did in taking them. It is a
great honour for any man to yield to the authority of reason and
religion against his interest. `(2.)` The princes very generously sent
home the poor captives well accommodated, v. 15. Those that hope to find
mercy with God must learn hence with what tenderness to carry themselves
towards those that lie at their mercy. It is strange that these princes,
who in this instance discovered such a deference to the word of God, and
such an influence upon the people, had not so much grace as, in
obedience to the calls of God by so many prophets, to root idolatry out
of their kingdom, which, soon after this, was the ruin of it.

### Verses 16-27

Here is, `I.` The great distress which the kingdom of Ahaz was reduced to
for his sin. In general, 1. The Lord brought Judah low, v. 19. They had
lately been very high in wealth and power; but God found means to bring
them down, and make them as despicable as they had been formidable.
Those that will not humble themselves under the word of God will justly
be humbled by his judgments. Iniquity brings men low, Ps. 106:43. 2.
Ahaz made Judah naked. As his sin debased them, so it exposed them. It
made them naked to their shame; for it exposed them to contempt, as a
man unclothed. It made them naked to their danger; for it exposed them
to assaults, as a man unarmed, Ex. 32:25. Sin strips men. In particular,
the Edomites, to be revenged for Amaziah\'s cruel treatment of them (ch.
25:12), smote Judah, and carried off many captives, v. 17. The
Philistines also insulted them, took and kept possession of several
cities and villages that lay near them (v. 18), and so they were
revenged for the incursions which Uzziah had made upon them, ch. 26:6.
And, to show that it was purely the sin of Ahaz that brought the
Philistines upon his country, in the very year that he died the prophet
Isaiah foretold the destruction of the Philistines by his son, Isa.
14:28, 29.

`II.` The addition which Ahaz made both to the national distress and the
national guilt.

`1.` He added to the distress, by making court to strange kings, in hopes
they would relieve him. When the Edomites and Philistines were vexatious
to him, he sent to the kings of Assyria to help him (v. 16); for he
found his own kingdom weakened and made naked, and he could not put any
confidence in God, and therefore was at a vast expense to get an
interest in the king of Assyria. He pillaged the house of God, and the
king\'s house, and squeezed the princes for money to hire these foreign
forces into his service, v. 21. Though he had conformed to the idolatry
of the heathen nations, his neighbours, they did not value him for that,
nor love him the better, nor did his compliance, by which he lost God,
gain them, nor could he make any interest in them, but with his money.
It is often found that wicked men themselves have no real affection for
those that revolt to them, nor do they care to do them a kindness. A
degenerate branch is looked upon, on all sides, as an abominable branch,
Isa. 14:19. But what did Ahaz get by the king of Assyria? Why, he came
to him, but he distressed him, and strengthened him not (v. 20), helped
him not, v. 21. The forces of the Assyrian quartered upon his country,
and so impoverished and weakened it; they grew insolent and imperious,
and created him a great deal of vexation, like a broken reed, which not
only fails, but pierces the hand.

`2.` He added to the guilt, by making court to strange gods, in hopes
they would relieve him. In his distress, instead of repenting of his
idolatry, which he had reason enough to see the folly of, he trespassed
yet more (v. 22), was more mad than ever upon his idols. A brand of
infamy is here set upon him for it: This is that king Ahaz, that
wretched man, who was the scandal of the house of David and the curse
and plague of his generation. Note, Those are wicked and vile indeed
that are made worse by their afflictions, instead of being made better
by them, who in their distress trespass yet more, have their corruptions
exasperated by that which should mollify them, and their hearts more
fully set in them to do evil. Let us see what his trespass was. `(1.)` He
abused the house of God; for he cut in pieces the vessels of it, that
the priests might not perform the service of the temple, or not as it
should be performed, for want of vessels; and, at length, he shut up the
doors, that the people might not attend it, v. 24. This was worse than
the worst of the kings before him had done. `(2.)` He confronted the altar
of God, for he made himself altars in every corner of Jerusalem; so
that, as the prophet speaks, they were like heaps in the furrows of the
fields, Hos. 12:11. And in the cities of Judah, either by his power or
by his purse, perhaps by both, he erected high places for the people to
burn incense to what idols they pleased, as if on purpose to provoke the
God of his fathers, v. 25. `(3.)` He cast off God himself; for he
sacrificed to the gods of Damascus (v. 23), not because he loved them,
for he thought they smote him; but because he feared them, thinking that
they helped his enemies, and that, if he could bring them into his
interest, they would help him. Foolish man! It was his own God that
smote him and strengthened the Syrians against him, not the gods of
Damascus; had he sacrificed to him, and to him only, he would have
helped him. But no marvel that men\'s affections and devotions are
misplaced when they mistake the author of their trouble and their help.
And what comes of it? The gods of Syria befriend Ahaz no more than the
kings of Assyria did; they were the ruin of him and of all Israel. This
sin provoked God to bring judgments upon them, to cut him off in the
midst of his days, when he was but thirty-six years old; and it
debauched the people so that the reformation of the next reign could not
prevail to cure them of their inclination to idolatry, but they retained
that root of bitterness till the captivity in Babylon plucked it up.

The chapter concludes with the conclusion of the reign of Ahaz, v. 26,
27. For aught that appears, he died impenitent, and therefore died
inglorious; for he was not buried in the sepulchres of the kings. Justly
was he thought unworthy to be laid among them who was so unlike them-to
be buried with kings who had used his kingly power for the destruction
of the church and not for its protection or edification.
