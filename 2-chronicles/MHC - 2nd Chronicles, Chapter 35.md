2nd Chronicles, Chapter 35
==========================

Commentary
----------

We are here to attend Josiah, `I.` To the temple, where we see his
religious care for the due observance of the ordinance of the passover,
according to the law (v. 1-19). `II.` To the field of battle, where we see
his rashness in engaging with the king of Egypt, and how dearly it cost
him (v. 20-23). `III.` To the grave, where we see him bitterly lamented
(v. 24-27). And so we must take our leave of Josiah.

### Verses 1-19

The destruction which Josiah made of idols and idolatry was more largely
related in the Kings, but just mentioned here in the foregoing chapter
(v. 33); but his solemnizing the passover, which was touched upon there
(2 Ki. 23:21), is very particularly related here. Many were the feasts
of the Lord, appointed by the ceremonial law, but the passover was the
chief. It began them all in the night wherein Israel came out of Egypt;
it concluded them all in the night wherein Christ was betrayed; and in
the celebration of it Hezekiah and Josiah, those two great reformers,
revived religion in their day. The ordinance of the Lord\'s supper
resembles the passover more than it does any of the Jewish festivals;
and the due observance of that ordinance, according to the rule, is an
instance and means both of the growing purity and beauty of churches and
of the growing piety and devotion of particular Christians. Religion
cannot flourish where that passover is either wholly neglected or not
duly observed; return to that, revive that, make a solemn business of
that affecting binding ordinance, and then, it is to be hoped, there
will be a reformation in other instances also.

In the account we had of Hezekiah\'s passover the great zeal of the
people was observable, and the transport of devout affection that they
were in; but little of the same spirit appears here. It was more in
compliance with the king that they all kept the passover (v. 17, 18)
than from any great inclination they had to it themselves. Some pride
they took in this form of godliness, but little pleasure in the power of
it. But, whatever defect there was among the people in the spirit of the
duty, both the magistrates and the ministers did their part and took
care that the external part of the service should be performed with due
solemnity.

`I.` The king exhorted and directed, quickened and encouraged, the priests
and Levites to do their office in this solemnity. Perhaps he saw them
remiss and indifferent, unwilling to go out of their road or mend their
pace. If ministers are so, it is not amiss for any, but most proper for
magistrates, to stir them up to their business. Say to Archippus, Take
heed to thy ministry, Col. 4:17. Let us see how this good king managed
his clergy upon this occasion. 1. He reduced them to the office they
were appointed to by the law of Moses (v. 6) and the order they were put
into by David and Solomon, v. 4. He set them in their charge, v. 2. He
did not cut them out new work, nor put them into any new method, but
called them back to their institution. Their courses were settled in
writing; let them have recourse to that writing, and marshal themselves
according to the divisions of their families, v. 5. Our rule is settled
in the written word; let magistrates take care that ministers walk
according to that rule and they do their duty. 2. He ordered the ark to
be put in its place. It should seem, it had of late been displaced,
either by the wicked kings, to make room for their idols in the most
holy place, or by Hezekiah, to make room for the workmen that repaired
the temple. However it was, Josiah bids the Levites put the ark in the
house (v. 3), and not carry it about from place to place, as perhaps of
late they had done, justifying themselves therein by the practice before
the temple was built. Now that the priests were discharged from this
burden of the ark they must be careful in other services about it. 3. He
charged them to serve God and his people Israel, v. 3. Ministers must
look upon themselves as servants both to Christ and to his church for
his sake, 2 Co. 4:5. They must take care, and take pains, and lay out
themselves to the utmost, `(1.)` For the glory and honour of God, and to
advance the interests of his kingdom among men. Paul, a servant of God,
Tit. 1:1. `(2.)` For the welfare and benefit of his people, not as having
dominion over their faith, but as helpers of their holiness and joy; and
there will be no difficulty, in the strength of God, in honestly serving
these two masters. 4. He charged them to sanctify themselves, and
prepare their brethren, v. 6. Ministers\' work must begin at home, and
they must sanctify themselves in the first place, purify themselves from
sin, sequester themselves from the world, and devote themselves to God.
But it must not end there; they must do what they can to prepare their
brethren by admonishing, instructing, exhorting, quickening, and
comforting, them. The preparation of the heart is indeed from the Lord;
but ministers must be instruments in his hand. 5. He encouraged them to
the service, v. 2. He spoke comfortably to them, as Hezekiah did, ch.
30:22. He promised them his countenance. Note, Those whom we charge we
should encourage. Most people love to be commended, and will be wrought
upon by encouragements more than by threats.

`II.` The king and the princes, influenced by his example, gave liberally
for the bearing of the charges of this passover. The ceremonial services
were expensive, which perhaps was one reason why they had been
neglected. People had not zeal enough to be at the charge of them; nor
were they now very fond of them, for that reason, and therefore, 1.
Josiah, at his own proper cost, furnished the congregation with paschal
lambs, and other sacrifices, to be offered during the seven days of the
feast. He allowed out of his own estate 30,000 lambs for passover
offerings, which the offerers were to feast upon, and 3000 bullocks (v.
7) to be offered during the following seven days. Note, Those who are
serious in religion should, when they persuade others to do that which
is good, make it as cheap and easy to them as may be. And where God sows
plentifully he expects to reap accordingly. It is to be feared that the
congregation generally had not come provided; so that, if Josiah had not
furnished them, the work of God must have stood still. 2. The chief of
the priests, who were men of great estates, contributed towards the
priests\' charges, as Josiah did towards the people\'s. The princes (v.
8), that is, the chief of the priests, the princes of the holy tribe,
rulers of the house of God, bore the priests\' charges. And some of the
rich and great men of the Levites furnished them also with cattle, both
great and small, for offerings, v. 9. For, as to those that sincerely
desire to be found in the way of their duty, Providence sometimes raises
up friends to bear them out in it, beyond what they could have expected.

`III.` The priests and Levites performed their office very readily, v.
10. They killed the paschal lambs in the court of the temple, the
priests sprinkled the blood upon the altar, the Levites flayed them, and
then gave the flesh to the people according to their families (v. 11,
12), not fewer than ten, nor more than twenty, to a lamb. They took it
to their several apartments, roasted it, and ate it according to the
ordinance, v. 13. As for the other sacrifices that were eucharistical,
the flesh of them was boiled according to the law of the peace-offerings
and was divided speedily among the people, that they might feast upon it
as a token of their joy in the atonement made and their reconciliation
to God thereby. And, lastly, The priests and Levites took care to honour
God by eating of the passover themselves, v. 14. Let not ministers think
that the care they take for the souls of others will excuse their
neglect of their own, or that being employed so much in public worship
will supersede the religious exercises of their closets and families.
The Levites here mace ready for themselves and for the priests, because
the priests were wholly taken up all day in the service of the altar;
therefore, that they might not have their lamb to dress when they should
eat it, the Levites got it ready for them against supper time. Let
ministers learn hence to help one another, and to forward one another\'s
work, as brethren, and fellow-servants of the same Master.

`IV.` The singers and porters attended in their places, and did their
office, v. 15. The singers with their sacred songs and music expressed
and excited the joy of the congregation, and made the service very
pleasant to them; and the porters at the gates took care that there
should be no breaking in of any thing to defile or disquiet the
assembly, nor going out of any from it, that none should steal away till
the service was done. While they were thus employed their brethren the
Levites prepared paschal lambs for them.

`V.` The whole solemnity was performed with great exactness, according to
the law (v. 16, 17), and, upon that account, there was none like it
since Samuel\'s time (v. 18), for in Hezekiah\'s passover there were
several irregularities. And bishop Patrick observes that in this also it
exceeded the other passovers which the preceding kings had kept, that
though Josiah was by no means so rich as David, and Solomon, and
Jehoshaphat, yet he furnished the whole congregation with beasts for
sacrifice, both paschal and eucharistical, at his own proper cost and
charge, which was more than any king ever did before him.

### Verses 20-27

It was thirteen years from Josiah\'s famous passover to his death.
During this time, we may hope, thing went well in his kingdom, that he
prospered, and religion flourished; yet we are not entertained with the
pleasing account of those years, but they are passed over in silence,
because the people, for all this, were not turned from the love of their
sins nor God from the fierceness of his anger. The next news therefore
we hear of Josiah is that he is cut off in the midst of his days and
usefulness, before he is full forty years old. We had this sad story, 2
Ki. 23:29, 30. Here it is somewhat more largely related. That appears
here, more than did there, which reflects such blame on Josiah and such
praise on the people as one would not have expected.

`I.` Josiah was a very good prince, yet he was much to be blamed for his
rashness and presumption in going out to war against the king of Egypt
without cause or call. It was bad enough, as it appeared in the Kings,
that he meddled with strife which belonged not to him. But here it looks
worse; for, it seems, the king of Egypt sent ambassadors to him, to warn
him against this enterprise, v. 21.

`1.` The king of Egypt argued with Josiah, `(1.)` From principles of
justice. He professed that he had no desire to do him any hurt, and
therefore it was unfair, against common equity and the law of nations,
for Josiah to take up arms against him. If even a righteous man engage
in an unrighteous cause, let him not expect to prosper. God is no
respecter of persons. See Prov. 3:30; 25:8. `(2.)` From principles of
religion: \"God is with me; nay, He commanded me to make haste, and
therefore, if thou retard my motions, thou meddlest with God.\" It
cannot be that the king of Egypt only pretended this (as Sennacherib did
in a like case, 2 Ki. 18:25), hoping thereby to make Josiah desist,
because he knew he had a veneration for the word of God; for it is said
here (v. 22) that the words of Necho were from the mouth of God. We must
therefore suppose that either by a dream, or by a strong impulse upon
his spirit which he had reason to think was from God, or by Jeremiah or
some other prophet, he had ordered him to make war upon the king of
Assyria. `(3.)` From principles of policy: \"That he destroy thee not; it
is at thy peril if thou engage against one that has not only a better
army and a better cause, but God on his side.\"

`2.` It was not in wrath to Josiah, whose heart was upright with the Lord
his God, but in wrath to a hypocritical nation, who were unworthy of so
good a king, that he was so far infatuated as not to hearken to these
fair reasonings and desist from his enterprise. He would not turn his
face from him, but went in person and fought the Egyptian army in the
valley of Megiddo, v. 22. If perhaps he could not believe that the king
of Egypt had a command from God to do what he did, yet, upon his
pleading such a command, he ought to have consulted the oracles of God
before he went out against him. His not doing that was his great fault,
and of fatal consequence. In this matter he walked not in the ways of
David his father; for, had it been his case, he would have enquired of
the Lord, Shall I go up? Wilt thou deliver them into my hands? How can
we think to prosper in our ways if we do not acknowledge God in them?

`II.` The people were a very wicked people, yet they were much to be
commended for lamenting the death of Josiah as they did. That Jeremiah
lamented him I do not wonder; he was the weeping prophet, and plainly
foresaw the utter ruin of his country following upon the death of this
good king. But it is strange to find that all Judah and Jerusalem, that
stupid senseless people, mourned for him (v. 24), contrived how to have
their mourning excited by singing men and singing women, how to have it
spread through the kingdom (they made an ordinance in Israel that the
mournful ditties penned on this sad occasion should be learned and sung
by all sorts of people), and also how to have the remembrance of it
perpetuated: these elegies were inserted in the collections of state
poems; they are written in the Lamentations. Hereby it appeared, 1. That
they had some respect to their good prince, and that, though they did
not cordially comply with him in all his good designs, they could not
but greatly honour him. Pious useful men will be manifested in the
consciences even of those that will not be influenced by their example;
and many that will not submit to the rules of serious godliness
themselves yet cannot but give it their good word and esteem it in
others. Perhaps those lamented Josiah when he was dead that were not
thankful to God for him while he lived. The Israelites murmured at Moses
and Aaron while they were with them and spoke sometimes of stoning them,
and yet, when they died, they mourned for them many days. We are often
taught to value mercies by the loss of them which, when we enjoyed them,
we did not prize as we ought. 2. That they had some sense of their own
danger now that he was gone. Jeremiah told them, it is likely, of the
evil they might now expect to come upon them, from which he was taken
away; and so far they credited what he said that they lamented the death
of him that was their defence. Note, Many will more easily be persuaded
to lament the miseries that are coming upon them than to take the proper
way by universal reformation to prevent them, will shed tears for their
troubles, but will not be prevailed upon to part with their sins. But
godly sorrow worketh repentance and that repentance will be to
salvation.
