2nd Chronicles, Chapter 25
==========================

Commentary
----------

Amaziah\'s reign, recorded in this chapter, was not one of the worse and
yet for from good. Most of the passages in this chapter we had before
more briefly related, 2 Ki. 14. Here we find Amaziah, `I.` A just revenger
of his father\'s death (v. 1-4). `II.` An obedient observer of the command
of God (v. 5-10). `III.` A cruel conqueror of the Edomites (v. 11-13). `IV.`
a foolish worshipper of the gods of Edom and impatient of reproof for it
(v. 14-16). `V.` Rashly challenging the king of Israel, and smarting for
his rashness (v. 17-24). And, lastly, ending his days ingloriously (v.
25-28).

### Verses 1-13

Here is, `I.` The general character of Amaziah: He did that which was
right in the eyes of the Lord, worshipped the true God, kept the temple
service a going, and countenanced religion in his kingdom; but he did
not do it with a perfect heart (v. 2), that is, he was not a man of
serious piety or devotion himself, nor had he any zeal for the exercises
of religion. He was no enemy to it, but a cool and indifferent friend.
Such is the character of too many in this Laodicean age: they do that
which is good, but not with the heart, not with a perfect heart.

`II.` A necessary piece of justice which he did upon the traitors that
murdered his father: he put them to death, v. 3. Though we should
suppose they intended to avenge on their king the death of the prophet
(as was intimated, ch. 24:25), yet this would by no means justify their
wickedness; for they were not the avengers, but presumptuously took
God\'s work out of his hands: and therefore Amaziah did what became him
in calling them to an account for it, but forbade the putting of the
children to death for the parents\' sin, v. 4.

`III.` An expedition of his against the Edomites, who, some time ago, had
revolted from under the dominion of Judah, to which he attempted to
reduce them. Observe,

`1.` The great preparation he made for this expedition. `(1.)` He mustered
his own forces, and marshalled them (v. 5), and found Judah and Benjamin
in all but 300,000 men that were fit for war, whereas, in Jehoshaphat\'s
time, fifty or sixty years before, they were four times as many. Sin
weakens a people, diminishes them, dispirits them, and lessens their
number and figure. `(2.)` He hired auxiliary troops out of the kingdom of
Israel, v. 6. Finding his own kingdom defective in men, he thought to
make up the deficiency with his money, and therefore took into his pay
100,000 Israelites. If he had advised with any of his prophets before he
did this, or had but considered how little any of his ancestors got by
their alliances with Israel, he would not have had this to undo again.
But rashness makes work for repentance.

`2.` The command which God sent him by a prophet to dismiss out of his
service the forces of Israel, v. 7, 8. He would not have him call in any
assistance at all: it looked like distrust of God. If he made sure of
God\'s presence, the army he had of his own was sufficient. But
particularly he must not take in their assistance: For the Lord is not
with the children of Ephraim, because they are not with him, but worship
the calves. This was a good reason why he should not make use of them,
because he could not depend upon them to do him any service. What good
could be expected from those that had not God with them, nor his
blessings upon their undertakings? It is comfortable to employ those
who, we have reason to hope, have an interest in heaven, and dangerous
to associate with those from whom the Lord has departed. The prophet
assured him that if he persisted in his resolution to take these
idolatrous apostate Israelites with him, in hopes thereby to make
himself strong for the battle, it was at his peril; they would prove a
dead weight to his army, would sink and betray it: \"God shall make thee
fall before the enemy, and these Israelites will be the ruin of thy
cause; for God has power to help thee without them, and to cast thee
down though thou hast them with thee.\"

`3.` The objection which Amaziah made against this command, and the
satisfactory answer which the prophet gave to that objection, v. 9. The
king had remitted 100 talents to the men of Israel for advance-money.
\"Now,\" says he, \"if I send them back, I shall lose that: But what
shall we do for the 100 talents?\" This is an objection men often make
against their duty: they are afraid of losing by it. \"Regard not
that,\" says the prophet: \"The Lord is able to give thee much more than
this; and, thou mayest depend upon it, he will not see thee lose by him.
What are 100 talents between thee and him? He has ways enough to make up
the loss to thee; it is below thee to speak of it.\" Note, A firm belief
of God\'s all-sufficiency to bear us out in our duty, and to make up all
the loss and damage we sustain in his service abundantly to our
advantage, will make his yoke very easy and his burden very light. What
is it to trust in God, but to be willing to venture the loss of any
thing for him, in confidence of the goodness of the security he gives us
that we shall not lose by him, but that whatever we part with for his
sake shall be made up to us in kind or kindness. When we grudge to part
with any thing for God and our religion, this should satisfy us, that
God is able to give us much more than this. He is just, and he is good,
and he is solvent. The king lost 100 talents by his obedience; and we
find just that sum given to his grandson Jotham as a present (ch. 27:5);
then the principal was repaid, and, for interest, 10,000 measures of
wheat and as many of barley.

`4.` His obedience to the command of God, which is upon record to his
honour. He would rather lose his money, disoblige his allies, and
dismiss a fourth part of his army just as they were going to take the
field, than offend God: He separated the army of Ephraim, to go home
again, v. 10. And they went home in great anger, taking it as a great
affront thus to be made fools of, and to be cashiered as men not fit to
be employed, and being perhaps disappointed of the advantages they
promised themselves in spoil and plunder by joining with Judah against
Edom. Men are apt to resent that which touches them in their profit or
reputation, though it frees them from trouble.

`5.` His triumphs over the Edomites, v. 11, 12. He left dead upon the
spot, in the field of battle, 10,000 men; 10,000 more he took prisoners,
and barbarously killed them all by throwing them down some steep and
craggy precipice. What provocation he had to exercise this cruelty
towards them we are not told; but it was certainly very severe.

`6.` The mischief which the disbanded soldiers of Israel did to the
cities of Judah, either in their return or soon after, v. 13. They were
so enraged at being sent home that, if they might not go to share with
Judah in the spoil of Edom, they would make a prey of Judah. Several
cities that lay upon the borders they plundered, killing 3000 men that
made resistance. But why should God suffer this to be done? Was it not
in obedience to him that they were sent home, and yet shall the country
thus suffer by it? Surely God\'s way is in the sea! Did not the prophet
say that God was not with the children of Ephraim, and yet they are
suffered to prevail against Judah? Doubtless God intended hereby to
chastise those cities of Judah for their idolatries, which were found
most in those parts that lay next to Israel. The men of Israel had
corrupted them, and now they were made a plague to them. Satan both
tempts and torments.

### Verses 14-16

Here is, `I.` The revolt of Amaziah from the God of Israel to the gods of
the Edomites. Egregious folly! Ahaz worshipped the gods of those that
had conquered him, for which he had some little colour, ch. 28:23. But
to worship the gods of those whom he had conquered, who could not
protect their own worshippers, was the greatest absurdity that could be.
What did he see in the gods of the children of Seir that could tempt him
to set them up for his gods and bow himself down before them? v. 14. If
he had cast the idols down from the rock and broken them to pieces,
instead of the prisoners, he would have manifested more of the piety as
well as more of the pity of an Israelite; but perhaps for that barbarous
inhumanity he was given up to this ridiculous idolatry.

`II.` The reproof which God sent to him, by a prophet, for this sin. The
anger of the Lord was kindled against him, and justly; yet, before he
sent to destroy him, he sent to convince and reclaim him, and so to
prevent his destruction. The prophet reasoned with him very fairly and
very mildly: Why hast thou sought the favour of those gods which could
not deliver their own people? v. 15. If men would but duly consider the
inability of all those things to help them to which they have recourse
when they forsake God, they would not be such enemies to themselves.

`III.` The check he gave to the reprover, v. 16. He could say nothing in
excuse of his own folly; the reproof was too just to be answered. But he
fell into a passion with the reprover. 1. He taunted him as saucy and
impertinent, and meddling with that which did not belong to him: Art
thou made of the king\'s counsel? Could not a man speak reasonably to
him, but he must be upbraided as usurping the place of a
privy-counsellor? But, as a prophet, he really was made of the king\'s
counsel by the King of kings, in duty to whom the king was bound not
only to hear, but to ask and take his counsel. 2. He silenced him, bade
him forbear and say not a word more to him. He said to the seer, See
not, Isa. 30:10. Men would gladly have their prophets thus under their
girdles, as we say, to speak just when and what they would have them
speak, and not otherwise. 3. He threatened him: \"Why shouldst thou be
smitten? It is at thy peril if thou sayest a word more of this matter.\"
He seems to remind him of Zechariah\'s fate in the last reign, who was
put to death for making bold with the king; and bids him take warning by
him. Thus he justifies the killing of that prophet by menacing this, and
so, in effect, makes himself guilty of the blood of both. He had
hearkened to the prophet who ordered him to send back the army of
Israel, and was ruled by him, though he contradicted his politics and
lost him 100 talents, v. 10. But this prophet, who dissuaded him from
worshipping the gods of the Edomites, he ran upon with an unaccountable
rage, which must be attributed to the witchcraft of idolatry. He was
easily persuaded to part with his talents of silver, but by no means
with his gods of silver.

`IV.` The doom which the prophet passed upon him for this. He had more to
say to him by way of instruction and advice; but, finding him obstinate
in his iniquity, he forbore. He is joined to idols; let him alone, Hos.
4:17. Miserable is the condition of that man with whom the blessed
Spirit, by ministers and conscience, forbears to strive, Gen. 6:3. And
both the reprovers in the gate and that in the bosom, if long
brow-beaten and baffled, will at length forbear. So I gave them up to
their own hearts\' lusts. The secure sinner perhaps values himself upon
it as a noble and happy achievement to have silenced his reprovers and
monitors, and to get clear of them; but what comes of it? \"I know that
God has determined to destroy thee; it is a plain indication that thou
art marked for ruin that thou hast done this, and hast not hearkened to
my counsel.\" Those that are deaf to reproof are ripening apace for
destruction, Prov. 29:1.

### Verses 17-28

We have here this degenerate prince mortified by his neighbour and
murdered by his own subjects.

`I.` Never was proud prince more thoroughly mortified than Amaziah was by
Joash king of Israel.

`1.` This part of the story (which was as fully related 2 Ki. 14:8, etc.,
as it is here)-embracing the foolish challenge which Amaziah sent to
Joash (v. 17), his haughty scornful answer to it (v. 18), with the
friendly advice he gave him to sit still and know when he was well off,
(v. 19),-his wilfully persisting in his challenge (v. 20, 21), the
defeat that was given him (v. 22), and the calamity he brought upon
himself and his city thereby (v. 23, 24),-verifies two of Solomon\'s
proverbs:-`(1.)` That a man\'s pride will bring him low, Prov. 29:23. It
goes before his destruction; not only procures it meritoriously, but is
often the immediate occasion of it. He that exalteth himself shall be
abased. `(2.)` That he that goes forth hastily to strive will probably not
know what to do in the end thereof, when his neighbour has put him to
shame, Prov. 25:8. He that is fond of contention may have enough of it
sooner than he thinks of.

`2.` But there are two passages in this story which we had not before in
the Kings. `(1.)` That Amaziah took advice before he challenged the king
of Israel, v. 17. But of whom? Not of the prophet-he was not made of the
king\'s counsel; but of his statesmen that would flatter him and bid him
go up and prosper. It is good to take advice, but then it must be of
those that are fit to advise us. Those that will not take advice from
the word of God, which would guide them aright, will justly be left to
the bad advice of those that will counsel them to their destruction. Let
those be made fools that will not be made wise. `(2.)` Amaziah\'s
imprudence is here made the punishment of his impiety (v. 20): It was of
the Lord; he left him to himself to act thus foolishly, that he and his
people might be delivered into the hands of their enemies, because they
had forsaken God and sought after the gods of Edom. Those that will not
persuaded to do well for their souls will justly be given up to their
own counsels to do ill for themselves even in their outward affairs.

`II.` Never was poor prince more violently pursued by his own subjects.
From the time that he departed from the Lord (so it may be read, v. 27)
the hearts of his subjects departed from him, and they began to form a
design against him in Jerusalem. It is probable they were exasperated
against him more for his rashly engaging in a war against Israel than
for his worshipping the gods of Edom. But at length the ferment grew so
high, and he perceived the plot to be laid so deeply, that he thought
fit to quit his royal city and flee to Lachish, either as a private
place where he might be hid or as a strong place where he might be
guarded; but they sent after him thither, and slew him there. By this
the putting of him to death seems to have been done deliberately, and to
have been the act, not of a disgusted servant or two, but of a
considerable body that durst avow it. How unrighteous soever they were
herein, God was righteous.
