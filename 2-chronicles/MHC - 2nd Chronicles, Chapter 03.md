2nd Chronicles, Chapter 3
=========================

Commentary
----------

It was a much larger and more particular account of the building of the
temple which we had in the book of Kings than is here in this book of
Chronicles. In this chapter we have, `I.` The place and time of building
the temple (v. 1, 2). `II.` The dimensions and rich ornaments of it (v.
3-9). `III.` The cherubim in the most holy place (v. 10-13). `IV.` The veil
(v. 14). `V.` The two pillars (v. 15-17). Of all this we have already and
an account, 1 Ki. 6, 7.

### Verses 1-9

Here is, `I.` The place where the temple was built. Solomon was neither at
liberty to choose nor at a loss to fix the place. It was before
determined (1 Chr. 22:1), which was an ease to his mind. 1. It must be
at Jerusalem; for that was the place where God had chosen to put his
name there. The royal city must be the holy city. There must be the
testimony of Israel; for there are set the thrones of judgment, Ps.
122:4, 5. 2. It must be on Mount Moriah, which, some think, was that
very place in the land of Moriah where Abraham offered Isaac, Gen. 22:2.
So the Targum says expressly, adding, But he was delivered by the word
of the Lord, and a ram provided in his place. That was typical of
Christ\'s sacrifice of himself; therefore fitly was the temple, which
was likewise a type of him, built there. 3. It must be where the Lord
appeared to David, and answered him by fire, 1 Chr. 21:18, 26. There
atonement was made once; and therefore, in remembrance of that, there
atonement was made once; and therefore, in remembrance of that, there
atonement must still be made. Where God has met with me it is to be
hoped that he will still manifest himself. 4. It must be in the place
which David has prepared, not only which he had purchased with his
money, but which he had purchased with his money, but which he had
pitched upon divine direction. It was Solomon\'s wisdom not to enquire
out a more convenient place, but to acquiesce in the appointment of God,
whatever might be objected against it. 5. It must be in the threshold
floor of Ornan, which, if (as a Jebusite) it gives encouragement to the
Gentiles, obliges us to look upon temple-work as that which requires the
labour of the mind, no less than threshing-work dos that of the body.

`II.` The time when it was begun; not till the fourth year of Solomon\'s
reign, v. 2. Not that the first three years were trifled away, or spent
in deliberating whether they should build the temple or no; but they
were employed in the necessary preparations for it, wherein three years
would be soon gone, considering how many hands were to be got together
and set to work. Some conjecture that this was a sabbatical year, or
year of release and rest to the land, when the people, being discharged
from their husbandry, might more easily lend a hand to the beginning of
this work; and then the year in which it was finished would fall out to
be another sabbatical year, when they would likewise have leisure to
attend the solemnity of the dedication of it.

`III.` The dimensions of it, in which Solomon was instructed (v. 3), as
he was in other things, by his father. This was the foundation (so it
may be read) which Solomon laid for the building of the house. This was
the rule he went by, so many cubits the length and breadth, after the
first measure, that is, according to the measure first fixed, which
there was no reason to make any alteration of when the work came to be
done; for the dimensions were given by divine wisdom, and what God does
shall be for ever; nothing can be put to it, or taken from it, Eccl.
3:14. His first measure will be the last.

`IV.` The ornaments of the temple. The timber-work was very fine, and
yet, within, it was overlaid with pure gold (v. 4), with fine gold (v.
5). and that embossed with palm-trees and chains. It was gold of Parvaim
(v. 6), the best gold. The beams and posts, the walls and doors, were
overlaid with gold, v. 7. The most holy place, which was ten yards
square, was all overlaid with fine gold (v. 8), even the upper chambers,
or rather the upper floor or roof-top, bottom, and sides, were all
overlaid with gold. Every nail, or screw, or pin, with which the golden
plates were fastened to the walls that were overlaid with them, weighed
fifty shekels, or was worth so much, workmanship and all. A great many
precious stones were dedicated to God (1 Chr. 29:2, 8), and these were
set here and there, where they would show to the best advantage. The
finest houses now pretend to no better garnishing than good paint on the
roof and walls; but the ornaments of the temple were most substantially
rich. It was set with precious stones, because it was a type of the new
Jerusalem, which has no temple in it because it is all temple, and the
walls, gates, and foundations of which are said to be of precious stones
and pearls, Rev. 21:18, 19, 21.

### Verses 10-17

Here is an account of 1. The two cherubim, which were set up in the holy
of holies. There were two already over the ark, which covered the
mercy-seat with their wings; these were small ones. Now that the most
holy place was enlarged, though these were continued (being
appurtenances to the ark, which was not to be made new, as all the other
utensils of the tabernacle were), yet those two large ones were added,
doubtless by divine appointment, to fill up the holy place, which
otherwise would have looked bare, like a room unfurnished. These
cherubim are said to be of image-work (v. 10), designed, it is likely,
to represent the angels who attend the divine Majesty. Each wing
extended five cubits, so that the whole was twenty cubits (v. 12, 13),
which was just the breadth of the most holy place, v. 8. They stood on
their feet, as servants, their faces inward toward the ark (v. 13), that
it might appear they were not set there to be adored (for then they
would have been made sitting, as on a throne, and their faces towards
their worshippers), but rather as themselves attendants on the invisible
God. We must not worship angels, but we must worship with angels; for we
have come into communion with them (Heb. 12:22), and must do the will of
God as the angels do it. The thought that we are worshipping him before
whom the angels cover their faces will help to inspire us with reverence
in all our approaches to God. Compare 1 Co. 11:10 with Isa. 6:2. 2. The
veil that parted between the temple and the most holy place, v. 14. This
denoted the darkness of that dispensation, and the distance which the
worshippers were kept at; but, at the death of Christ, this veil was
rent; for through him we are made nigh, and have boldness not only to
look, but to enter, into the holiest. On this he was wrought cherubim.
Heb. he caused them to ascend, that is, they were made in raised work,
embossed. Or he made them on the wing in an ascending posture, as the
other two that stood on their feet in an attending posture, to remind
the worshippers to lift up their hearts, and to soar upwards in their
devotions. 3. The two pillars which were set up before the temple. Both
together were somewhat above thirty-five cubits in length (v. 15), about
eighteen cubits high a-piece. See 1 Ki. 7:15, etc., where we took a view
of those pillars, Jachin and Boaz, establishment and strength in
temple-work and by it.
