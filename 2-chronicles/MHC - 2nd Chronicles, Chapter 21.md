2nd Chronicles, Chapter 21
==========================

Commentary
----------

Never surely did any kingdom change its king so much for the worse as
Judah did, when Jehoram, one of the vilest, succeeded Jehoshaphat, one
of the best. Thus were they punished for not making a better use of
Jehoshaphat\'s good government, and their disaffectedness (or coldness
at least) to his reformation, 20:33. Those that knew not now to value a
good king are justly plagued with a bad one. Here is, `I.` Jehoram\'s
elevation to the throne (v. 1-3). `II.` The wicked course he took to
establish himself in it, by the murder of his brethren (v. 4). `III.` The
idolatries and other wickedness he was guilty of (v. 5, 6, 11). `IV.` The
prophecy of Elijah against him (v. 12-15). `V.` The judgments of God upon
him, in the revolt of his subjects from him (v. 8-10) and the success of
his enemies against him (v. 16, 17). `VI.` His miserable sickness and
inglorious exit (v. 18-20). `VII.` The preservation of the house of David
notwithstanding (v. 7).

### Verses 1-11

We find here,

`I.` That Jehoshaphat was a very careful indulgent father to Jehoram. He
had many sons, who are here named (v. 2), and it is said (v. 13) that
they were better than Jehoram, had a great deal more wisdom and virtue,
and lived up to their education, which he went counter to. They were
very hopeful, and any of them more fit for the crown than he; and yet,
because he was the first-born (v. 3), his father secured the kingdom to
him, and portioned his brethren and disposed of them so as that they
would be easy and give him no disturbance; as Abraham, when he made
Isaac his heir, dismissed his other children with gifts. Herein
Jehoshaphat was very kind and fair to his son, which might have obliged
him to be respectful to him, and tread in the steps of so good a father.
But it is no new thing for the children that have been most indulged by
their parents to be least dutiful to them. Whether in doing this he
acted wisely and well for his people, and was just to them, I cannot
say. His birthright entitled him to a double portion of his father\'s
estate, Deu. 21:17. But if he appeared utterly unfit for government (the
end of which is the good of the people), and likely to undo all that his
father had done, it would have been better perhaps to have set him
aside, and taken the next that was hopeful, and not inclined as he was
to idolatry. Power is a sacred thing, with which men may either do much
good or much hurt; and therefore Detur digniori-Let him that deserves it
have it. Salus populi suprema lex-The security of the people is the
first consideration.

`II.` That Jehoram was a most barbarous brother to his father\'s sons. As
soon as he had settled himself in the throne he slew all his brethren
with the sword, either by false accusation, under colour of law, or
rather by assassination. By some wicked hand or other he got them all
murdered, pretending (it is likely) that he could not think himself safe
in the government till they were taken out of the way. Those that mean
ill themselves are commonly, without cause, jealous of those about them.
The wicked fear where no fear is, or pretend to do so, in order to
conceal their malice. Jehoram, it is likely, hated his brethren and slew
them for the same reason that Cain hated Abel and slew him, because
their piety condemned his impiety and won them that esteem with the
people which he had lost. With them he slew divers of the princes of
Israel, who adhered to them, or were likely to avenge their death. The
princes of Judah, those who had taught the good knowledge of the Lord
(ch. 17:7), are here called princes of Israel, as before fathers of
Israel (ch. 19:8), because they were Israelites indeed, men of
integrity. The sword which the good father had put into their hands this
wicked son sheathed in their bowels. Woe unto him that thus foundeth a
kingdom in blood (Hab. 2:12); it will prove a foundation that will sink
the superstructure.

`III.` That Jehoram was a most wicked king, who corrupted and debauched
his kingdom, and ruined the reformation that his good father and
grandfather had carried on: He walked in the way of the house of Ahab
(v. 6), made high places, which the people were of themselves too
forward to make, and did his utmost to set up idolatry again, v. 11. 1.
As for the inhabitants of Jerusalem, where he kept his court, he easily
drew them into his spiritual whoredom: He caused them to commit
fornication, seducing them to eat things sacrificed to idols, Rev. 2:20.
2. The country people seem to have been brought to it with more
difficulty; but those that would not be corrupted by flatteries were
driven by force to partake in his abominable idolatries: He compelled
Judah thereto. He used that power for the destruction of the church
which was given him for the edification of it.

`IV.` That when he forsook God and his worship his subjects withdrew from
their allegiance to him. 1. Some of the provinces abroad that were
tributaries to him did so. The Edomites revolted (v. 8), and, though he
chastised them (v. 9), yet he could not reduce them, v. 10. 2. One of
the cities of his own kingdom did so. Libnah revolted (v. 10) and set up
for a free state, as of old it had a king of its own, Jos. 12:15. And
the reason is here given, not only why God permitted it, but why they
did it; they shook off his government because he had forsaken the Lord
God of his fathers, had become an idolater and a worshipper of false
gods, and they could not continue subject to him without some danger of
being themselves also drawn away from God and their duty. While he
adhered to God they adhered to him; but, when he cast God off, they cast
him off. Whether this reason will justify them in their revolt of no, it
will justify God\'s providence which ordered it so.

`V.` That yet God was tender of his covenant with the house of David, and
therefore would not destroy the royal family, though it was so
wretchedly corrupted and degenerated, v. 7. These things we had before,
2 Ki. 8:19-22. The tenour of the covenant was that David\'s seed should
be visited for their transgressions, but the covenant should never be
broken, Ps. 89:30, etc.

### Verses 12-20

Here we have, `I.` A warning from God sent to Jehoram by a writing from
Elijah the prophet. By this it appears that Jehoram came to the throne,
and showed himself what he was before Elijah\'s translation. It is true
we find Elisha attending Jehoshaphat, and described as pouring water on
the hands of Elijah, after the story of Elijah\'s translation (2 Ki.
3:11); but that might be, and that description might be given of him,
while Elijah was yet on earth: and it is certain that that history is
put out of its proper place, for we read of Jehoshaphat\'s death, and
Jehoram\'s coming to the crown, before we read of Elijah\'s translation,
1 Ki. 22:50. We will suppose that the time of his departure was at hand,
so that he could not go in person to Jehoram; but that, hearing of his
great wickedness in murdering his brethren, he left this writing it is
probable with Elisha, to be sent him by the first opportunity, that it
might either be a means to reclaim him or a witness against him that he
was fairly told what would be in the end hereof. The message is sent him
in the name of the Lord God of David his father (v. 12), upbraiding him
with his relation to David as that which, though it was his honour, was
an aggravation of his degeneracy. 1. His crimes are plainly charged upon
him-his departure from the good ways of God, in which he had been
educated, and which he had been directed and encouraged to walk in by
the example of his good father and grandfather, who lived and died in
peace and honour (v. 12)-his conformity to the ways of the house of
Ahab, that impious scandalous family-his setting up and enforcing
idolatry in his kingdom-and his murdering his brethren because they were
better than himself, v. 13. These are the heads of the indictment
against him. 2. Judgment is given against him for these crimes; he is
plainly told that his sin should certainly be the ruin, `(1.)` Of his
kingdom and family (v. 14): \"With a heavy stroke, even that of war and
captivity, will the Lord smite thy people and thy children,\" etc. Bad
men bring God\'s judgments upon all about them. His people justly suffer
because they had complied with his idolatry, and his wives because they
had drawn him to it. `(2.)` Of his health and life: \"Thou shalt have
great sickness, very painful and tedious, and at last mortal,\" v. 15.
This he is warned of before, that his blood might be upon his own head,
the watchman having delivered his soul; and that when these things so
particularly foretold, came to pass, it might appear that they did not
come by chance, but as the punishment of his sins, and were so intended.
And now if, as he had learned of Ahab to do wickedly, he had but learned
even of Ahab to humble himself upon the receipt of this threatening
message from Elijah-if, like (1 Ki. 21:27), he had rent his clothes, put
on sackcloth, and fasted-who knows but, like him, he might have obtained
at least a reprieve? But it does not appear that he took any notice of
it; he threw it by as waste-paper; Elijah seemed to him as one that
mocked. But those that will not believe shall feel.

`II.` The threatened judgments brought upon him because he slighted the
warning. No marvel that hardened sinners are not frightened from sin and
to repentance by the threatenings of misery in another world, which is
future and out of sight, when the certain prospect of misery in this
world, the sinking of their estates and the ruin of their healths, will
not restrain them from vicious courses.

`1.` See Jehoram here stripped of all his comforts. God stirred up the
spirit of his neighbours against him, who had loved and feared
Jehoshaphat, but hated and despised him, looking upon it as a scandalous
thing for a nation to change their gods. Some occasion or other they
took to quarrel with him, invaded his country, but, as it should seem,
fought neither against small nor great, but the king\'s house only; they
made directly to that, and carried away all the substance that was found
in it. No mention is made of their carrying any away captive but the
king\'s wives and his sons, v. 17. Thus God made it evident that the
controversy was with him and his house. Here it is only said, They
carried away his sons; but we find (ch. 22:1) that they slew them all.
Blood for blood. He had slain all his brethren, to strengthen himself;
and now all his sons are slain but one, and so he is weakened. If he had
not been of the house of David, that one would not have escaped. When
Jeroboam\'s house, and Baasha\'s, and Ahab\'s, were destroyed, there was
none left; but David\'s house must not be wholly extirpated, though
sometimes wretchedly degenerated, because a blessing was in it, no less
a blessing than that of the Messiah.

`2.` See him tormented with sore diseases and of long continuance, such
as were threatened in the law against those that would not fear the Lord
their God, Deu. 28:58, 59. His disease was very grievous. It lay in his
bowels, producing a continual griping, and with this there was a
complication of other sore diseases. The affliction was moreover very
tedious. Two years he continued ill, and could get no relief; for the
disease was incurable, though he was in the prime of life, not forty
years old. Asa, whose heart was perfect with God though in some
instances he stepped aside, was diseased only in his feet; but Jehoram,
whose heart was wicked, was struck in his inwards, and he that had no
bowels of compassion towards his brethren was so plagued in his bowels
that they fell out. Even good men, and those who are very dear to God,
may be afflicted with diseases of this kind; but to them they are
fatherly chastisements, and by the support of divine consolations the
soul may dwell at ease even then when the body lies in pain. These sore
diseases seized him just after his house was plundered and his wives and
children were carried away. `(1.)` Perhaps his grief and anguish of mind
for that calamity might occasion his sickness, or at least contribute to
the heightening of it. `(2.)` By this sickness he was disabled to do any
thing for the recovery of them or the revenge of the injury done him.
`(3.)` It added, no doubt, very much to his grief, in his sickness, that
he was deprived of the society of his wives and children and that all
the substance of his house was carried away. To be sick and poor, sick
and solitary, but especially to be sick and in sin, sick and under the
curse of God, sick and destitute of grace to bear the affliction, and of
comfort to counter-balance it-is a most deplorable case.

`3.` See him buried in disgrace. He reigned but eight years, and then
departed without being desired, v. 20. Nobody valued him while he lived,
none lamented him when he died, but all wished that no greater loss
might ever come to Jerusalem. To show what little affection or respect
they had for him, they would not bury him in the sepulchres of the
kings, as thinking him unworthy to be numbered among them who had
governed so ill. The excluding of his body from the sepulchres of his
fathers might be ordered by Providence as an intimation of the
everlasting separation of the souls of the wicked after death, from the
spirits of just men. This further disgrace they put upon him, that they
made no burning for him, like the burning of his fathers, v. 19. His
memory was far from being sweet and precious to them, and therefore they
did not honour it with any sweet odours or precious spices, though we
may suppose that his dead body, after so long and loathsome a disease,
needed something to perfume it. The generality of the people, though
prone to idolatry, yet had no true kindness for their idolatrous kings.
Wickedness and profaneness make men despicable even in the eyes of those
who have but little religion themselves, while natural conscience itself
often gives honour to those who are truly pious. Those that despise God
shall be lightly esteemed, as Jehoram was.
