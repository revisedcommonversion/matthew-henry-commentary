2nd Chronicles, Chapter 23
==========================

Commentary
----------

Six years bloody Athaliah had tyrannised; in this chapter we have her
deposed and slain, and Joash, the rightful heir, enthroned. We had the
story before nearly as it is here related, 2 Ki. 11:4, etc. `I.` Jehoiada
prepared the people for the king, acquainted them with his design, armed
them, and appointed them their posts (v. 1-10). `II.` He produced the king
to the people, crowned him, and anointed him (v. 11). `III.` He slew the
usurper (v. 12-15). `IV.` He reformed the kingdom, re-established
religion, and restored the civil government (v. 16-21).

### Verses 1-11

We may well imagine the bad posture of affairs in Jerusalem during
Athaliah\'s six years\' usurpation, and may wonder that God permitted it
and his people bore it so long; but after such a dark and tedious night
the returning day in this revolution was the brighter and the more
welcome. The continuance of David\'s seed and throne was what God had
sworn by his holiness (Ps. 89:35), and an interruption was no
defeasance; the stream of government here runs again in the right
channel. The instrument and chief manager of the restoration is
Jehoiada, who appears to have been, 1. A man of great prudence, who
reserved the young prince for so many years till he was fit to appear in
public, and till the nation had grown weary of the usurper, who prepared
his work beforehand, and then effected it with admirable secresy and
expedition. When God has work to do he will qualify and animate men for
it. 2. A man of great interest. The captains joined with him, v. 1. The
Levites and the chief of the fathers of Israel came at his call to
Jerusalem (v. 2) and were there ready to receive his orders. See what a
command wisdom and virtue will give men. The Levites and all Judah did
as Jehoiada commanded (v. 8), and, which is strange, all that were
entrusted with the secret kept their own counsel till it was executed.
Thus the words of the wise are heard in quiet, Eccl. 9:17. 3. A man of
great faith. It was not only common equity (much less his wife\'s
relation to the royal family) that put him upon this undertaking, but a
regard to the word of God, and the divine entail of the crown (v. 3):
The king\'s son shall reign, must reign, as the Lord hath said. His eye
to the promise, and dependence upon that, added a great deal of glory to
this undertaking. 4. A man of great religion. This matter was to be done
in the temple, which might occasion some breach of rule, and the
necessity of the case might be thought to excuse it; but he gave special
order that none of the people should come into the house of the Lord,
but the priests and Levites only, who were holy, upon pain of death, v.
6, 7. Never let sacred things be profaned, no, not for the support of
civil rights. 5. A man of great resolution. When he had undertaken this
business he went through with it, brought out the king, crowned him, and
gave him the testimony, v. 11. He ventured his head, but it was in a
good cause, and therefore he went on boldly. It is here said that his
sons joined with him in anointing the young king. One of them, it is
likely, was that Zechariah whom Joash afterwards put to death for
reproving him (ch. 24:20), which was so much the more ungrateful because
he bore a willing part in anointing him.

### Verses 12-21

Here we have, `I.` The people pleased, v. 12, 13. When the king stood at
his pillar, whose right it was to stand there, all the people of the
land rejoiced to see a rod sprung out of the stem of Jesse, Isa. 11:1.
When it seemed a withered root in a dry ground, to see what they
despaired of ever seeing-a king of the house of David, what a pleasing
surprise was it to them! They ran in transports of joy to see this
sight, praised the king, and praised God, for they had with them such as
taught to sing praise.

`II.` Athaliah slain. She ran upon the point of the sword of justice;
for, imagining her interest much better than it was, she ventured into
the house of the Lord at that time, and cried, Treason, treason! But
nobody seconded her, or sided with her. The pride of her heart deceived
her. She thought all her own, whereas none were cordially so. Jehoiada,
as protector in the king\'s minority, ordered her to be slain (v. 14),
which was done immediately (v. 15), only care was taken that she should
not be slain in the house of the Lord, that sacred place must not be so
far disgraced, nor that wicked woman so far honoured.

`III.` The original contract agreed to, v. 16. In the Kings it is said
that Jehoiada made a covenant between the Lord, the people, and the
king, 2 Ki. 11:17. Here it is said to be between himself, the people,
and the king; for he, as God\'s priest, was his representative in this
transaction, or a sort of mediator, as Moses was. The indenture was
tripartite, but the true intent and meaning of the whole was that they
should be the Lord\'s people. God covenanted by Jehoiada to take them
for his people; the king and people covenanted with him to be his; and
then the king covenanted with the people to govern them as the people of
God, and the people with the king to be subject to him as the Lord\'s
people, in his fear and for his sake. Let us look upon ourselves and one
another as the Lord\'s people, and this will have a powerful influence
upon us in the discharge of all our duty both to God and man.

`IV.` Baal destroyed, v. 17. They would not have done half their work if
they had only destroyed the usurper of the king\'s right, and not the
usurper of God\'s right-if they had asserted the honour of the throne,
and not that of the altar. The greatest grievance of Athaliah\'s reign
was the bringing in of the worship of Baal, and supporting of that;
therefore that must be abolished in the first place. Down with Baal\'s
house, his altars, his images; down with them all, and let the blood of
his priests be mingled with his sacrifices; for God had commanded that
seducers to idolatry should be put to death, Deu. 13:5, 6.

`V.` The temple service revived, v. 18, 19. This had been neglected in the
last reigns, the priest and people wanting either power or zeal to keep
it up when they had princes that were disaffected to it. But Jehoiada
restored the offices of the house of the Lord, which in the late times
had been disturbed and invaded, to the proper course and proper hands.
`1.` He appointed the priests to their courses, for the due offering of
sacrifices, according to the law of Moses. 2. The singers to theirs,
according to the appointment of David. The sacrifices (it should seem)
were offered with rejoicing and singing, and with good reason. We joy in
God when we receive the atonement, Rom. 5:11. 3. The porters were put in
their respective posts as David ordered (v. 19), and their office was to
take care that none who were upon any account ceremonially unclean
should be admitted into the courts of the temple.

`VI.` The civil government re-established, v. 20. They brought the king
in state to his own palace, and set him upon the throne of the kingdom,
to give law, and give judgment, either in his own person or by Jehoiada
his tutor. Thus was this happy revolution perfected. The generality of
the people rejoiced in it, and the rest were quiet and made no
opposition, v. 21. When the Son of David is enthroned in the soul all is
quiet and springs of joy are opened.
