Introduction to Ezra
====================

The Jewish church puts on quite another face in this book from what it
had appeared with; its state much better, and more pleasant, than it was
of late in Babylon, and yet far inferior to what it had been formerly.
The dry bones here live again, but in the form of a servant; the yoke of
their captivity is taken off, but the marks of it in their galled necks
remain. Kings we hear no more of; the crown has fallen from their heads.
Prophets they are blessed with, to direct them in their
re-establishment, but, after a while, prophecy ceases among them, till
the great prophet appears, and his fore-runner. The history of this book
is the accomplishment of Jeremiah\'s prophecy concerning the return of
the Jews out of Babylon at the end of seventy years, and a type of the
accomplishment of the prophecies of the Apocalypse concerning the
deliverance of the gospel church out of the New-Testament Babylon. Ezra
preserved the records of that great revolution and transmitted them to
the church in this book. His name signifies a helper; and so he was to
that people. A particular account concerning him we shall meet with, ch.
7, where he himself enters upon the stage of action. The book gives us
an account, `I.` Of the Jews\' return out of their captivity, ch. 1, 2.
`II.` Of the building of the temple, the opposition it met with, and yet
the perfecting of it at last, ch. 3-6. `III.` Of Ezra\'s coming to
Jerusalem, ch. 7, 8. `IV.` Of the good service he did there, in obliging
those that had married strange wives to put them away, ch. 9, 10. This
beginning again of the Jewish nation was small, yet its latter end
greatly increased.
