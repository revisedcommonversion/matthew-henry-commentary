Introduction to Nehemiah
========================

This book continues the history of the children of the captivity, the
poor Jews, that had lately returned out of Babylon to their own land. At
this time not only the Persian monarchy flourished in great pomp and
power, but Greece and Rome began to be very great and to make a figure.
Of the affairs of those high and mighty states we have authentic
accounts extant; but the sacred and inspired history takes cognizance
only of the state of the Jews, and makes no mention of other nations but
as the Israel of God had dealings with them: for the Lord\'s portion is
his people; they are his peculiar treasure, and, in comparison with
them, the rest of the world is but as lumber. In my esteem, Ezra the
scribe and Nehemiah the tirshatha, though neither of them ever wore a
crown, commanded an army, conquered any country, or was famed for
philosophy or oratory, yet both of them, being pious praying men, and
very serviceable in their day to the church of God and the interests of
religion, were really greater men and more honourable, not only than any
of the Roman consuls or dictators, but than Xenophon, or Demosthenes, or
Plato himself, who lived at the same time, the bright ornaments of
Greece. Nehemiah\'s agency for the advancing of the settlement of Israel
we have a full account of in this book of his own commentaries or
memoirs, wherein he records not only the works of his hands, but the
workings of his heart, in the management of public affairs, inserting in
the story many devout reflections and ejaculations, which discover in
his mind a very deep tincture of serious piety and are peculiar to his
writing. Twelve years, from his twentieth year (ch. 1:1) to his
thirty-second year (ch. 13:6), he was governor of Judea, under
Artaxerxes king of Persia, whom Dr. Lightfoot supposes to be the same
Artaxerxes as Ezra has his commission from. This book relates, `I.`
Nehemiah\'s concern for Jerusalem and the commission he obtained from
the king to go thither, ch. 1, 2. `II.` His building the wall of Jerusalem
notwithstanding the opposition he met with, ch. 3, 4. `III.` His
redressing the grievances of the people, ch. 5. `IV.` His finishing the
wall, ch. 6. `V.` The account he took of the people, ch. 7. `VI.` The
religions solemnities of reading the law, fasting, and praying, and
renewing their covenants, to which he called the people (ch. 8-10). `VII.`
The care he took for the replenishing of the holy city and the settling
of the holy tribe, ch. 11, 12. `VIII.` His zeal in reforming various
abuses, ch. 13. Some call this the second book of Ezra, not because he
was the penman of it, but because it is a continuation of the history of
the foregoing book, with which it is connected (v. 1). This was the last
historical book that was written, as Malachi was the last prophetical
book, of the Old Testament.
