Nehemiah, Chapter 12
====================

Commentary
----------

In this chapter are preserved upon record, `I.` The names of the chief of
the priests and the Levites that came up with Zerubbabel (v. 1-9). `II.`
The succession of the high priests (v. 10, 11). `III.` The names of the
next generation of the other chief priests (v. 12-21). `IV.` The eminent
Levites that were in Nehemiah\'s time (v. 22-26). `V.` The solemnity of
dedicating the wall of Jerusalem (v. 27-43). `VI.` The settling of the
offices of the priests and Levites in the temple (v. 44-47).

### Verses 1-26

We have here the names, and little more than the names, of a great many
priests and Levites, that were eminent in their day among the returned
Jews. Why this register should be here inserted by Nehemiah does not
appear, perhaps to keep in remembrance those good men, that posterity
might know to whom they were beholden, under God, for the happy revival
and re-establishment of their religion among them. Thus must we
contribute towards the performance of that promise, Ps. 112:6, The
righteous shall be in everlasting remembrance. Let the memory of the
just be blessed, be perpetuated. It is a debt we still owe to faithful
ministers to remember our guides, who have spoken to us the word of God,
Heb. 13:7. Perhaps it is intended to stir up their posterity, who
succeeded them in the priest\'s office and inherited their dignities and
preferments, to imitate their courage and fidelity. It is good to know
what our godly ancestors and predecessors were, that we may learn
thereby what we should be. We have here, 1. The names of the priests and
Levites that came up with the first out of Babylon, when Jeshua was high
priest. Jeremiah and Ezra are mentioned with the first (v. 1), but, it
is supposed, not Jeremiah the prophet nor Ezra the scribe; the fame of
the one was long before and that of the other some time after, though
both of them were priests. Of one of the Levites it is said (v. 8) that
he was over the thanksgiving, that is, he was entrusted to see that the
psalms, the thanksgiving psalms, were constantly sung in the temple in
due time and manner. The Levites kept their turns in their watches,
reliving one another as becomes brethren, fellow-labourers, and
fellow-soldiers. 2. The succession of high priests during the Persian
monarchy, from Jeshua (or Jesus), who was high priest at the time of the
restoration, to Jaddua (or Jaddus), who was high priest when Alexander
the Great, after the conquest of Tyre, came to Jerusalem, and paid great
respect to this Jaddus, who met him in his pontifical habit, and showed
him the prophecy of Daniel, which foretold his conquests. 3. The next
generation of priests, who were chief men, and active in the days of
Joiakim, sons of the first set. Note, We have reason to acknowledge
God\'s favour to his church, and care of it, in that, as one generation
of ministers passes away, another comes. All those who are mentioned v.
1, etc., as eminent in their generation, are again mentioned, though
with some variation in several of the names, v. 12, etc., except two, as
having sons that were likewise eminent in their generation-a rare
instance, that twenty good fathers should leave behind them twenty good
sons (for so many here are) that filled up their places. 4. The next
generation of Levites, or rather a latter generation; for those priests
who are mentioned flourished in the days of Joiakim the high priest,
these Levites in the days of Eliashib, v. 22. Perhaps then the
forementioned families of the priests began to degenerate, and the third
generation of them came short of the first two; but the work of God
shall never fail for want of instruments. Then a generation of Levites
was raised up, who were recorded chief of the fathers (v. 22), and were
eminently serviceable to the interests of the church, and their service
not the less acceptable either to God or to his people for their being
Levites only, of the lower rank of ministers. Eliashib the high priest
being allied to Tobiah (ch. 13:4), the other priests grew remiss; but
then the Levites appeared the more zealous, as appears by this, that
those who were now employed in expounding (ch. 8:7) and in praying (ch.
9:4, 5) were all Levites, not priests, regard being had to their
personal qualifications more than to their order. These Levites were
some of them singers (v. 24), to praise and give thanks, others of them
porters (v. 25), keeping the ward at the thresholds of the gates, and
both according to the command of David.

### Verses 27-43

We have read of the building of the wall of Jerusalem with a great deal
of fear and trembling; we have here an account of the dedicating of it
with a great deal of joy and triumph. Those that sow in tears shall thus
reap.

`I.` We must enquire what was the meaning of this dedication of the wall;
we will suppose it to include the dedication of the city too (continens
pro contentothe thing containing for the thing contained), and therefore
it was not done till the city was pretty well replenished, ch. 11:1. It
was a solemn thanksgiving to God for his great mercy to them in the
perfecting of this undertaking, of which they were the more sensible
because of the difficulty and opposition they had met with in it. 2.
They hereby devoted the city in a peculiar manner to God and to his
honour, and took possession of it for him and in his name. All our
cities, all our houses, must have holiness to the Lord written upon
them; but this city was (so as never any other was) a holy city, the
city of the great King (Ps. 48:2 and Mt. 5:35): it had been so ever
since God chose it to put his name there, and as such, it being now
refitted, it was afresh dedicated to God by the builders and
inhabitants, in token of their acknowledgment that they were his
tenants, and their desire that it might still be is and that the
property of it might never be altered. Whatever is done for their
safety, ease, and comfort, must be designed for God\'s honour and glory.
3. They hereby put the city and its walls under the divine protection,
owning that unless the Lord kept the city the walls were built in vain.
When this city was in possession of the Jebusites, they committed the
guardianship of it to their gods, though they were blind and lame ones,
2 Sa. 5:6. With much more reason do the people of God commit it to his
keeping who is all-wise and almighty. The superstitious founders of
cities had an eye to the lucky position of the heavens (see Mr.
Gregory\'s works, p. 29, etc.); but these pious founders had an eye to
God only, to his providence, and not to fortune.

`II.` We must observe with what solemnity it was performed, under the
direction of Nehemiah. 1. The Levites from all parts of the country were
summoned to attend. The city must be dedicated to God, and therefore his
ministers must be employed in the dedicating of it, and the surrender
must pass through their hands. When those solemn feasts were over (ch. 8
and 9) they went home to their respective posts, to mind their cures in
the country; but now their presence and assistance were again called
for. 2. Pursuant to this summons, there was a general rendezvous of all
the Levites, v. 28, 29. Observe in what method they proceeded. `(1.)` They
purified themselves, v. 30. We are concerned to cleanse our hands, and
purify our hearts, when any work for God is to pass through them. They
purified themselves and then the people. Those that would be
instrumental to sanctify others must sanctify themselves, and set
themselves apart for God, with purity of mind and sincerity of
intention. Then they purified the gates and the wall. Then may we expect
comfort when we are prepared to receive it. To the pure all things are
pure (Tit. 1:15); and, to those who are sanctified, houses and tables,
and all their creature comforts and enjoyments, are sanctified, 1 Tim.
4:4, 5. This purification was performed, it is probable, by sprinkling
the water of purifying (or of separation, as it is called, Num. 19:9) on
themselves and the people, the walls and the gates-a type of the blood
of Christ, with which our consciences being purged from dead works, we
become fit to serve the living God (Heb. 9:14) and to be his care. `(2.)`
The princes, priests, and Levites, walked round upon the wall in two
companies, with musical instruments, to signify the dedication of it all
to God, the whole circuit of it (v. 36); so that it is likely they sung
psalms as they went along, to the praise and glory of God. This
procession is here largely described. They had a rendezvous at one
certain lace, where they divided themselves into two companies. Half of
the princes, with several priests and Levites, went on the right hand,
Ezra leading their van, v. 36. The other half of the princes and
priests, who gave thanks likewise, went to the left hand, Nehemiah
bringing up the rear, v. 38. At length both companies met in the temple,
where they joined their thanksgivings, v. 40. The crowd of people, it is
likely, walked on the ground, some within the wall and others without,
one end of this ceremony being to affect them with the mercy they were
giving thanks for, and to perpetuate the remembrance of it among them.
Processions, for such purposes, have their use. `(3.)` The people greatly
rejoiced, v. 43. While the princes, priests, and Levites, testified
their joy and thankfulness by great sacrifices, sound of trumpet,
musical instruments, and songs of praise, the common people testified
theirs by loud shouts, which were heard afar off, further than the more
harmonious sound of their songs and music: and these shouts, coming from
a sincere and hearty joy, are here taken notice of; for God overlooks
not, but graciously accepts, the honest zealous services of mean people,
though there is in them little of art and they are far from being fine.
It is observed that the women and children rejoiced; and their hosannas
were not despised, but recorded to their praise. All that share in
public mercies ought to join in public thanksgivings. The reason given
is that God had made them rejoice with great joy. He had given them both
matter for joy and hearts to rejoice; his providence had made them safe
and easy, and then his grace made them cheerful and thankful. The
baffled opposition of their enemies, no doubt, added to their joy and
mixed triumph with it. Great mercies call for the most solemn returns of
praise, in the courts of the Lord\'s house, in the midst of thee, O
Jerusalem!

### Verses 44-47

We have here an account of the remaining good effects of the universal
joy that was at the dedication of the wall. When the solemnities of a
thanksgiving day leave such impressions on ministers and people as that
both are more careful and cheerful in doing their duty afterwards, then
they are indeed acceptable to God and turn to a good account. So it was
here. 1. The ministers were more careful than they had been of their
work; the respect the people paid them upon this occasion encouraged
them to diligence and watchfulness, v. 45. The singers kept the ward of
their God, attending in due time to the duty of their office; the
porters, too, kept the ward of the purification, that is, they took care
to preserve the purity of the temple by denying admission to those that
were ceremonially unclean. When the joy of the Lord thus engages us to
our duty, and enlarges us in it, it is then an earnest of that joy
which, in concurrence with the perfection of holiness, will be our
everlasting bliss. 2. The people were more careful than they had been of
the maintenance of their ministers. The people, at the dedication of the
wall, among other things which they made matter of their joy, rejoiced
for the priests and for the Levites that waited, v. 44. They had a great
deal of comfort in their ministers, and were glad of them. When they
observed how diligently they waited, and what pains they took in their
work, they rejoiced in them. Note, The surest way for ministers to
recommend themselves to their people, and gain an interest in their
affections, is to wait on their ministry (Rom. 12:7), to be humble and
industrious, and to mind their business. When these did so the people
thought nothing too much to do for them, to encourage them. The law had
provided then their portions (v. 44), but what the better were they for
that provision if what the law appointed them either was not duly
collected or not justly paid to them? Now, `(1.)` Care is here taken for
the collecting of their dues. They were modest, and would rather lose
their right than call for it themselves. The people were many of them
careless and would not bring their dues unless they were called upon;
and therefore some were appointed whose office it should be to gather
into the treasuries, out of the fields of the cities, the portions of
the law for the priests and Levites (v. 44), that their portion might
not be lost for want of being demanded. This is a piece of good service
both to ministers and people, that the one may not come short of their
maintenance nor the other of their duty. `(2.)` Care is taken that, being
gathered in, they might be duly paid out, v. 47. They gave the singers
and porters their daily portion, over and above what was due to them as
Levites; for we may suppose that when David and Solomon appointed them
their work (v. 45, 46), above what was required from them as Levites,
they settled a fund for their further encouragement. Let those that
labour more abundantly in the word and doctrine be counted worthy of
this double honour. As for the other Levites, the tithes, here called
the holy things, were duly set apart for them, out of which they paid
the priests their tithe according to the law. Both are said to be
sanctified; when what is contributed, either voluntarily or by law, for
the support of religion and the maintenance of the ministry, is given
with an eye to God and his honour, it is sanctified, and shall be
accepted of him accordingly, and it will cause the blessing to rest on
the house and all that is in it, Eze. 44:30.
