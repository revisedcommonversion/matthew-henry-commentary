Nehemiah, Chapter 10
====================

Commentary
----------

We have in this chapter a particular account of the covenant which in
the close of the foregoing chapter was resolved upon; they struck while
the iron was not, and immediately put that good resolve in execution,
when they were in a good frame, lest, if it should be delayed, it might
be dropped. Here we have, `I.` The names of those that set their hands and
seals to it (v. 1-27). `II.` An account of those who signified their
consent and concurrence (v. 28, 29). `III.` The covenant itself, and the
articles of it in general, that they would \"keep God\'s commandments\"
(v. 29); in particular, that they would not marry with the heathen (v.
30), nor profane the sabbath, nor be rigorous with their debtors (v.
31), and that they would carefully pay their church-dues, for the
maintenance of the temple service, which they promise faithfully to
adhere to (v. 32-39).

### Verses 1-31

When Israel was first brought into covenant with God it was done by
sacrifice and the sprinkling of blood, Ex. 24. But here it was done by
the more natural and common way of sealing and subscribing the written
articles of the covenant, which bound them to no more than was already
their duty. Now here we have,

`I.` The names of those public persons who, as the representatives and
heads of the congregation, set their hands and seals to this covenant,
because it would have been an endless piece of work for every particular
person to do it; and, if these leading men did their part in pursuance
of this covenant, their example would have a good influence upon all the
people. Now observe, 1. Nehemiah, who was the governor, signed first, to
show his forwardness in this work and to set others a good example, v.
`1.` Those that are above others in dignity and power should go before
them in the way of God. 2. Next to him subscribed twenty-two priests,
among whom I wonder we do not find Ezra, who was an active man in the
solemnity (ch. 8:2) which was but the first day of the same month, and
therefore we cannot think he was absent; but he, having before done his
part as a scribe, now left it to others to do theirs. 3. Next to the
priests, seventeen Levites subscribed this covenant, among whom we find
all or most of those who were the mouth of the congregation in prayer,
ch. 9:4, 5. This showed that they themselves were affected with what
they had said, and would not bind those burdens on others which they
themselves declined to touch. Those that lead in prayer should lead in
every other good work. 4. Next to the Levites, forty-four of the chief
of the people gave it under their hands for themselves and all the rest,
chiefly those whom they had influence upon, that they would keep God\'s
commandments. Their names are left upon record here, to their honour, as
men that were forward and active in reviving and endeavouring to
perpetuate religion in their country. The memory of such shall be
blessed. It is observable that most of those who were mentioned, ch.
7:8, etc., as heads of houses or clans, are here mentioned among the
first of the chief of the people that subscribed, whoever was the
present head bearing the name of him that was head when they came out of
Babylon, and these were fittest to subscribe for all those of their
father\'s house. Here are Parosh, Pahathmoab, Elam, Zatthu, Bani (v.
14), Azgad, Bebai, Bigvai, Adin, Ater, Hashum, Bezai, Hariph, Anathoth,
and some others in the following verses, that are all found in that
catalogue. Those that have interest must use it for God.

`II.` The concurrence of the rest of the people with them, and the rest
of the priests and Levites, who signified their consent to what their
chiefs did. With them joined, 1. Their wives and children; for they had
transgressed, and they must reform. Every one that had knowledge and
understanding must covenant with God. As soon as young people grow up to
be capable of distinguishing between good and evil, and of acting
intelligently, they ought to make it their own act and deed to join
themselves to the Lord. 2. The proselytes of other nations, all that had
separated themselves from the people of the lands, their gods and their
worship, unto the law of God, and the observance of that law. See what
conversion it; it is separating ourselves from the course and custom of
this world, and devoting ourselves to the conduce of the word of God.
And, as there is one law, so there is one covenant, one baptism, for the
stranger and for him that is born in the land. Observe how the
concurrence of the people is expressed, v. 29. `(1.)` They clave to their
brethren one and all. Here those whom the court blessed the country
blessed too! The commonalty agreed with their nobles in this good work.
Great men never look so great as when they encourage religion, and are
examples of it; and they would by that, as much as any thing, secure an
interest in the most valuable of their inferiors. Let but the nobles
cordially espouse religious causes, and perhaps they will find people
cleave to them therein closer than they can imagine. Observe, Their
nobles are called their brethren; for, in the things of God, rich and
poor, high and low, meet together. `(2.)` They entered into a curse and an
oath. As the nobles confirmed the covenant with their hands and seals,
so the people with a curse and an oath, solemnly appealing to God
concerning their sincerity, and imprecating his just revenge if they
dealt deceitfully. Every oath has in it a conditional curse upon the
soul, which makes it a strong bond upon the soul; for our own tongues,
if false and lying tongues, will fall, and fall heavily, upon ourselves.

`III.` The general purport of this covenant. They laid upon themselves no
other burden than this necessary thing, which they were already obliged
to by all other engagements of duty, interest, and gratitude-to walk in
God\'s law, and to do all his commandments, v. 29. Thus David swore that
he would keep God\'s righteous judgments, Ps. 119:106. Our own covenant
binds us to this, if not more strongly, yet more sensibly, than we were
before bound, and therefore we must not think it needless thus to bind
ourselves. Observe, When we bind ourselves to do the commandments of God
we bind ourselves to do all his commandments, and therein to have an eye
to him as the Lord and our Lord.

`IV.` Some of the particular articles of this covenant, such as were
adapted to their present temptations. 1. That they would not intermarry
with the heathen, v. 30. Many of them had been guilty of this, Ezra 9:1.
In our covenants with God we should engage particularly against those
sins that we have been most frequently overtaken in and damaged by.
Those that resolve to keep the commandments of God must say to evil
doers, Depart, Ps. 119:115. 2. That they would keep no markets on the
sabbath day, or any other day of which the law had said, You shall do no
work therein. They would not only not sell goods themselves for gain on
that day, but they would not encourage the heathen to sell on that day
by buying of them, no not victuals, under pretence of necessity; but
would buy in their provisions for their families the day before, v. 31.
Note, Those that covenant to keep all God\'s commandments must
particularly covenant to keep sabbaths well; for the profanation of them
is an inlet to other instances of profaneness. The sabbath is a market
day for our souls, but not for our bodies. 3. That they would not be
severe in exacting their debts, but would observe the seventh year as a
year of release, according to the law, v. 31. In this matter they had
been faulty (ch. 5), and here therefore they promise to reform. This was
the acceptable fast, to undo the heavy burden, and to let the oppressed
go free, Isa. 58:6. It was in the close of the day of expiation that the
jubilee trumpet sounded. It was for the neglect of observing the seventh
year as a year of rest for the land that God had made it enjoy its
sabbaths seventy years (Lev. 26:35), and therefore they covenanted to
observe that law. Those are stubborn children indeed that will not amend
the fault for which they have been particularly corrected.

### Verses 32-39

Having covenanted against the sins they had been guilty of, they proceed
in obliging themselves to revive and observe the duties they had
neglected. We must not only cease to do evil, but learn to do well.

`I.` It was resolved, in general, that the temple service should be
carefully kept up, that the work of the house of their God should be
done in its season, according to the law, v. 33. Let not any people
expect the blessing of God unless they make conscience of observing his
ordinances and keeping up the public worship of him. Then it is likely
to go well with our houses when care is taken that the work of God\'s
house go on well. It was likewise resolved that they would never forsake
the house of their God (v. 39), as they and their fathers had done,
would not forsake it for the house of any other god, or for the high
places, as idolaters did, nor forsake it for their farms and
merchandises, as those did that were atheistical and profane. Those that
forsake the worship of God forsake God.

`II.` It was resolved, in pursuance of this, that they would liberally
maintain the temple service, and not starve it. The priests were ready
to do their part in all the work of God\'s house, if the people would do
theirs, which was to find them with materials to work upon. Now here it
was agreed and concluded, 1. That a stock should be raised for the
furnishing of God\'s table and altar plentifully. Formerly there were
treasures in the house of the Lord for this purpose, but these were
gone, and there was no settled fund to supply the want of them. It was a
constant charge to provide show-bread for the table, two lambs for the
daily offerings, four for the sabbaths, and more, and more costly,
sacrifices for other festivals, occasional sin-offerings, and
meat-offerings, and drink-offerings for them all. They had no rich king
to provide these, as Hezekiah did; the priests could not afford to
provide them, their maintenance was so small; the people therefore
agreed to contribute yearly, every one of them, the third part of a
shekel, about ten pence a-piece for the bearing of this expense. When
every one will act, and every one will give, though but little, towards
a good work, the whole amount will be considerable. The tirshatha did
not impose this tax, but the people made it an ordinance for themselves,
and charged themselves with it, v. 32, 33. 2. That particular care
should be taken to provide wood for the altar, to keep the fire always
burning upon it, and wherewith to boil the peace-offerings. All of them,
priests and Levites as well as people, agreed to bring in their quota,
and cast lots in what order they should bring it in, which family first
and which next, that there might be a constant supply, and not a
scarcity at one time and an overplus at another, v. 34. Thus they
provided the fire and the wood, as well as the lambs for the
burnt-offerings. 3. That all those things which the divine law had
appointed for the maintenance of the priests and Levites should be duly
paid in, for their encouragement to mind their business, and that they
might not be under any temptation to neglect it for the making of
necessary provision for their families. Then the work of the house of
God is likely to go on when those that serve at the altar live, and live
comfortably, upon the altar. First-fruits and tenths were then the
principal branches of the ministers\' revenues; and they here resolved,
`(1.)` To bring in the first-fruits justly, the first-fruits of their
ground and trees (Ex. 23:19; Lev. 19:23), the first-born of their
children (even the money wherewith they were to be redeemed) and of
their cattle, Ex. 13:2, 11, 12 (this was given to the priests, Num.
18:15, 16), also the first-fruits of their dough (Num. 15:21),
concerning which there is a particular order given in the prophecy
concerning the second temple, Eze. 44:30. `(2.)` To bring in their tenths
likewise, which were due to the Levites (v. 37), and a tenth out of
those tenths to the priest, v. 38. This was the law (Num. 18:21-28); but
these dues had been withheld, in consequence of which God, by the
prophet, charges them with robbing him (Mal. 3:8, 9), at the same time
encouraging them to be more just to him and his receivers, with a
promise that, if they brought the tithes into the store-house, he would
pour out blessings upon them, v. 10. This therefore they resolved to do,
that there might be meat in God\'s house, and plenty in the
store-chambers of the temple, where the vessels of the sanctuary were,
v. 39. \"We will do it (say they) in all the cities of our tillage,\" v.
37. In all the cities of our servitude, so the Septuagint, for they were
servants in their own land, ch. 9:36. But (as Mr. Poole well observes),
though they paid great taxes to the kings of Persia, and had much
hardship put upon them, they would not make that an excuse for not
paying their tithes, but would render to God the things that were his,
as well as to Caesar the things that were his. We must do what we can in
works of piety and charity notwithstanding the taxes we pay to the
government, and cheerfully perform our duty to God in our servitude,
which will be the surest way to ease and liberty in God\'s due time.
