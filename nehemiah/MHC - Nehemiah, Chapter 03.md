Nehemiah, Chapter 3
===================

Commentary
----------

Saying and doing are often two things: many are ready to say, \"Let us
rise up and build,\" who sit still and do nothing, like that fair-spoken
son who said,\"I go, Sir, but went not.\" The undertakers here were none
of those. As soon as they had resolved to build the wall about Jerusalem
they lost no time, but set about it presently, as we find in this
chapter. Let it never be said that we left that good work to be done
to-morrow which we might as well have done to-day. This chapter gives an
account of two things:-I. The names of the builders, which are recorded
here to their honour, for they were such as herein discovered a great
zeal for God and their country, both a pious and a public spirit, a
great degree both of industry and courage; and what they did was fit to
be thus largely registered, both for their praise and for the
encouragement of others to follow their example. `II.` The order of the
building; they took it before them, and ended where they began. They
repaired, 1. From the sheep-gate to the fish-gate (v. 1, 2). 2. Thence
to the old-gate (v. 3-5). 3. Thence to the valley-gate (v. 6-12). 4.
Thence to the dung-gate (v. 13, 14). 5. Thence to the gate of the
fountain (v. 15). 6. Thence to the water-gate (v. 16-26). 7. Thence by
the horse-gate to the sheep-gate again, where they began (v. 27-32), and
so they brought their work quite round the city.

### Verses 1-32

The best way to know how to divide this chapter is to observe how the
work was divided among the undertakers, that every one might know what
he had to do, and mind it accordingly with a holy emulation, and desire
to excel, yet without any contention, animosity, or separate interest.
No strife appears among them but which should do most for the public
good. Several things are observable in the account here given of the
building of the wall about Jerusalem:-

`I.` That Eliashib the high priest, with his brethren the priests, led the
van in this troop of builders, v. 1. Ministers should be foremost in
every good work; for their office obliges them to teach and quicken by
their example, as well as by their doctrine. If there be labour in it,
who so fit as they to work? if danger, who so fit as they to venture?
The dignity of the high priest was very great, and obliged him to
signalize himself in this service. The priests repaired the sheep-gate,
so called because through it were brought the sheep that were to be
sacrificed in the temple; and therefore the priests undertook the repair
of it because the offerings of the Lord made by fire were their
inheritance. And of this gate only it is said that they sanctified it
with the word and prayer, and perhaps with sacrifices perhaps, 1.
Because it led to the temple; or, 2. Because with this the building of
the wall began, and it is probable (though they were at work in all
parts of the wall at the same time) that this was first finished, and
therefore at this gate they solemnly committed their city and the walls
of it to the divine protection; or, 3. Because the priests were the
builders of it; and it becomes ministers above others, being themselves
in a peculiar manner sanctified to God, to sanctify to him all their
performances, and to do even their common actions after a godly sort.

`II.` That the undertakers were very many, who each took his share, some
more and some less, in this work, according as their ability was. Note,
What is to be done for the public good every one should assist in, and
further, to the utmost of his place and power. United force will conquer
that which no individual dares venture on. Many hands will make light
work.

`III.` That many were active in this work who were not themselves
inhabitants of Jerusalem, and therefore consulted purely the public
welfare and not any private interest or advantage of their own. Here are
the men of Jericho with the first (v. 2), the men of Gibeon and Mizpah
(v. 7), and Zanoah, v. 13. Every Israelite should lend a hand towards
the building up of Jerusalem.

`IV.` That several rulers, both of Jerusalem and of other cities, were
active in this work, thinking themselves bound in honour to do the
utmost that their wealth and power enabled them to do for the
furtherance of this good work. But it is observable that they are called
rulers of part, or the half part, of their respective cities. One was
ruler of the half part of Jerusalem (v. 12), another of part of
Beth-haccerem (v. 14), another of part of Mizpah (v. 15), another of the
half part of Beth-zur (v. 16), one was ruler of one half part, and
another of the other half part, of Keilah, v. 17, 18. Perhaps the
Persian government would not entrust any one with a strong city, but
appointed two to be a watch upon each other. Rome had two consuls.

`V.` Here is a just reproach fastened upon the nobles of Tekoa, that they
put not their necks to the work of their Lord (v. 5), that is, they
would not come under the yoke of an obligation to this service; as if
the dignity and liberty of their peerage were their discharge from
serving God and doing good, which are indeed the highest honour and the
truest freedom. Let not nobles think any thing below them by which they
may advance the interests of their country; for what else is their
nobility good for but that it puts them in a higher and larger sphere of
usefulness than that in which inferior persons move?

`VI.` Two persons joined in repairing the old gate (v. 6), and so were
co-founders, and shared the honour of it between them. The good work
which we cannot compass ourselves we must be thankful to those that will
go partners with us in. Some think that this is called the old gate
because it belonged to the ancient Salem, which was said to be first
built by Melchizedek.

`VII.` Several good honest tradesmen, as well as priests and rulers, were
active in this work-goldsmiths, apothecaries, merchants, v. 8, 32. They
did not think their callings excused them, nor plead that they could not
leave their shops to attend the public business, knowing that what they
lost would certainly be made up to them by the blessing of God upon
their callings.

`VIII.` Some ladies are spoken of as helping forward this work-Shallum
and his daughters (v. 12), who, though not capable of personal service,
yet having their portions in their own hands, or being rich widows,
contributed money for buying materials and paying workmen. St. Paul
speaks of some good women that laboured with him in the gospel, Phil.
4:3.

`IX.` Of some it is said that they repaired over against their houses (v.
10, 23, 28, 29), and of one (who, it is likely, was only a lodger) that
he repaired over against his chamber, v. 30. When a general good work is
to be done each should apply himself to that part of it that falls
nearest to him and is within his reach. If every one will sweep before
his own door, the street will be clean; if every one will mend one, we
shall be all mended. If he that has but a chamber will repair before
that, he does his part.

`X.` Of one it is said that he earnestly repaired that which fell to his
share (v. 20)-he did it with an inflamed zeal; not that others were cold
or indifferent, but he was the most vigorous of any of them and
consequently made himself remarkable. It is good to be thus zealously
affected in a good thin; and it is probable that this good man\'s zeal
provoked very many to take the more pains and make the more haste.

`XI.` Of one of these builders it is observed that he was the sixth son
of his father, v. 30. His five elder brethren, it seems, laid not their
hand to this work, but he did. In doing that which is good we need not
stay to see our elders go before us; if they decline it, it does not
therefore follow that we must. Thus the younger brother, if he be the
better man, and does God and his generation better service, is indeed
the better gentleman; those are most honourable that are most useful.

`XII.` Some of those that had first done helped their fellows, and
undertook another share where they saw there was most need. Meremoth
repaired, v. 4. and again, v. 21. And the Tekoites, besides the piece
they repaired (v. 5), undertook another piece (v. 27), which is the more
remarkable because their nobles set them a bad example by withdrawing
from the service, which, instead of serving them for an excuse to sit
still, perhaps made them the more forward to do double work, that by
their zeal they might either shame or atone for the covetousness and
carelessness of their nobles.

Lastly, Here is no mention of any particular share that Nehemiah himself
had in this work. A name-sake of his is mentioned, v. 16. But did he do
nothing? Yes, though he undertook not any particular piece of the wall,
yet he did more than any of them, for he had the oversight of them all;
half of his servants worked where there was most need, and the other
half stood sentinel, as we find afterwards (ch. 4:16), while he himself
in his own person walked the rounds, directed and encouraged the
builders, set his hand to the work where he saw occasion, and kept a
watchful eye upon the motions of the enemy, as we shall find in the next
chapter. The pilot needs not haul at a rope: it is enough for him to
steer.
