Introduction to 1st John
========================

Though the continued tradition of the church attests that this epistle
came from John the apostle, yet we may observe some other evidence that
will confirm (or with some perhaps even outweigh) the certainty of that
tradition. It should seem that the penman was one of the apostolical
college by the sensible palpable assurance he had of the truth of the
Mediator\'s person in his human nature: That which we have heard, which
we have seen with our eyes, which we have looked upon, and our hands
have handled, of the Word of life, v. 1. Here he takes notice of the
evidence the Lord gave to Thomas of his resurrection, by calling him to
feel the prints of the nails and of the spear, which is recorded by
John. And he must have been one of the disciples present when the Lord
came on the same day in which he arose from the dead, and showed them
his hands and his side, Jn. 20:20. But, that we may be assured which
apostle this was, there is scarcely a critic or competent judge of
diction, or style of argument and spirit, but will adjudge this epistle
to the writer of that gospel that bears the name of the apostle John.
They wonderfully agree in the titles and characters of the Redeemer: The
Word, the Life, the Light; his name was the Word of God. Compare 1:1 and
5:7 with Jn. 1:1 and Rev. 19:13. They agree in the commendation of
God\'s love to us (3:9; 4:7; and 5:1; Jn. 3:5, 6. Lastly (to add no more
instances, which may be easily seen in comparing this epistle with that
gospel), they agree in the allusion to, or application of, that passage
in that gospel which relates (and which alone relates) the issuing of
water and blood out of the Redeemer\'s opened side: This is he that came
by water and blood, 5:6. Thus the epistle plainly appears to flow from
the same pen as that gospel did. Now I know not that the text, or the
intrinsic history of any of the gospels, gives us such assurance of its
writer or penman as that ascribed to John plainly does. There (viz.
21:24) the sacred historian thus notifies himself: This is the disciple
that testifieth of these things and wrote these things; and we know that
his testimony is true. Now who is this disciple, but he concerning whom
Peter asked, What shall this man do? And concerning whom the Lord
answered, If I will that he tarry till I come, what is that to thee? (v.
22). And who (v. 20) is described by these three characters:- 1. That he
is the disciple whom Jesus loved, the Lord\'s peculiar friend. 2. That
he also leaned on his breast at supper. 3. That he said unto him, Lord,
who is he that betrayeth thee? As sure then as it is that that disciple
was John, so sure may the church be that that gospel and this epistle
came from the beloved John.

The epistle is styled general, as being not inscribed to any particular
church; it is, as a circular letter (or visitation charge), sent to
divers churches (some say of Parthia), in order to confirm them in their
stedfast adherence to the Lord Christ, and the sacred doctrines
concerning his person and office, against seducers; and to instigate
them to adorn that doctrine by love to God and man, and particularly to
each other, as being descended from God, united by the same head, and
travelling towards the same eternal life.
