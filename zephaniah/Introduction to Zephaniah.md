Introduction to Zephaniah
=========================

This prophet is placed last, as he was last in time, of all the minor
prophets before the captivity, and not long before Jeremiah, who lived
at the time of the captivity. He foretels the general destruction of
Judah and Jerusalem by the Chaldeans, and sets their sins in order
before them, which had provoked God to bring their ruin upon them, calls
them to repentance, threatens the neighbouring nations with the like
destructions, and gives encouraging promises of their joyful return out
of captivity in due time, which have a reference to the grace of the
gospel. We have, in the first verse, an account of the prophet and the
date of his prophecy, which supersedes our enquiry concerning them here.
