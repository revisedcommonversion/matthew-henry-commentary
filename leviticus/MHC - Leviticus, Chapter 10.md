Leviticus, Chapter 10
=====================

Commentary
----------

The story of this chapter is as sad an interruption to the institutions
of the levitical law as that of the golden calf was to the account of
the erecting of the tabernacle. Here is, `I.` The sin and death of Nadab
and Abihu, the sons of Aaron (v. 1, 2). `II.` The quieting of Aaron under
this sore affliction (v. 3). `III.` Orders given and observed about the
funeral and mourning (v. 4-7). `IV.` A command to the priests not to drink
wine when they went in to minister (v. 8-11). `V.` The care Moses took
that they should go on with their work, notwithstanding the agitation
produced by this event (v. 12, etc.).

### Verses 1-2

Here is, `I.` The great sin that Nadab and Abihu were guilty of: and a
great sin we must call it, how little soever it appears in our eye,
because it is evident by the punishment of it that it was highly
provoking to the God of heaven, whose judgment, we are sure, is
according to truth. But what was their sin? All the account here given
of it is that they offered strange fire before the Lord, which he
commanded them not (v. 1), and the same Num. 3:4. 1. It does not appear
the they had any orders to burn incense at all at this time. It is true
their consecration was completed the day before, and it was part of
their work, as priests, to serve at the altar of incense; but, it should
seem, the whole service of this solemn day of inauguration was to be
performed by Aaron himself, for he slew the sacrifices (ch. 9:8, 15,
18), and his sons were only to attend him (v. 9, 12, 18); therefore
Moses and Aaron only went into the tabernacle, v. 23. But Nadab and
Abihu were so proud of the honour they were newly advanced to, and so
ambitious of doing the highest and most honourable part of their work
immediately, that though the service of this day was extraordinary, and
done by particular direction from Moses, yet without receiving orders,
or so much as asking leave from him, they took their censers, and they
would enter into the tabernacle, at the door of which they thought they
had attended long enough, and would burn incense. And then their
offering strange fire is the same with offering strange incense, which
is expressly forbidden, Ex. 30:9. Moses, we may suppose, had the custody
of the incense which was prepared for this purpose (Ex. 39:38), and
they, doing this without his leave, had none of the incense which should
have been offered, but common incense, so that the smoke of their
incense came from a strange fire. God had indeed required the priests to
burn incense, but, at this time, it was what he commanded them not; and
so their crime was like that of Uzziah the king, 2 Chr. 26:16. The
priests were to burn incense only when it was their lot (Lu. 1:9), and,
at this time, it was not theirs. 2. Presuming thus to burn incense of
their own without order, no marvel that they made a further blunder, and
instead of taking of the fire from the altar, which was newly kindled
from before the Lord and which henceforward must be used in offering
both sacrifice and incense (Rev. 8:5), they took common fire, probably
from that with which the flesh of the peace-offerings was boiled, and
this they made use of in burning incense; not being holy fire, it is
called strange fire; and, though not expressly forbidden, it was crime
enough that God commanded it not. For (as bishop Hall well observes
here) \"It is a dangerous thing, in the service of God, to decline from
his own institutions; we have to do with a God who is wise to prescribe
his own worship, just to require what he has prescribed, and powerful to
revenge what he has not prescribed.\" 3. Incense was always to be burned
by only one priest at a time, but here they would both go in together to
do it. 4. They did it rashly, and with precipitation. They snatched
their censers, so some read it, in a light careless way, without due
reverence and seriousness: when all the people fell upon their faces,
before the glory of the Lord, they thought the dignity of their office
was such as to exempt them from such abasements. The familiarity they
were admitted to bred a contempt of the divine Majesty; and now that
they were priests they thought they might do what they pleased. 5. There
is reason to suspect that they were drunk when they did it, because of
the law which was given upon this occasion, v. 8. They had been feasting
upon the peace-offerings, and the drink-offerings that attended them,
and so their heads were light, or, at least, their hearts were merry
with wine; they drank and forgot the law (Prov. 31:5) and were guilty of
this fatal miscarriage. 6. No doubt it was done presumptuously; for, if
it had been done through ignorance, they would have been allowed the
benefit of the law lately made, even for the priests, that they should
bring a sin-offering, ch. 4:2, 3. But the soul that doth aught
presumptuously, and in contempt of God\'s majesty, authority, and
justice, that soul shall be cut of, Num. 15:30.

`II.` The dreadful punishment of this sin: There went out fire from the
Lord, and devoured them, v. 2. This fire which consumed the sacrifices
came the same way with that which had consumed the sacrifices (ch.
9:24), which showed what justice would have done to all the guilty
people if infinite mercy had not found and accepted a ransom; and, if
that fire struck such an awe upon the people, much more would this.

`1.` Observe the severity of their punishment. `(1.)` They died. Might it
not have sufficed if they had been only struck with a leprosy, as
Uzziah, or struck dumb, as Zechariah, and both by the altar of incense?
No; they were both struck dead. The wages of this sin was death. `(2.)`
They died suddenly, in the very act of their sin, and had not time so
much as to cry, \"Lord, have mercy upon us!\" Though God is
long-suffering to us-ward, yet sometimes he makes quick work with
sinners; sentence is executed speedily: presumptuous sinners bring upon
themselves a swift destruction, and are justly denied even space to
repent. `(3.)` They died before the Lord; that is, before the veil that
covered the mercy-seat; for even mercy itself will not suffer its own
glory to be affronted. Those that sinned before the Lord died before
him. Damned sinners are said to be tormented in the presence of the
Lamb, intimating that he does not interpose on their behalf, Rev. 14:10.
`(4.)` They died by fire, as by fire they sinned. They slighted the fire
that came from before the Lord to consume the sacrifices, and thought
other fire would do every jot as well; and now God justly made them feel
the power of that fire which they did not reverence. Thus those that
hate to be refined by the fire of divine grace will undoubtedly be
ruined by the fire of divine wrath. The fire did not burn them to ashes,
as it had done the sacrifices, nor so much as singe their coats (v. 5),
but, like lightning, struck them dead in an instant; by these different
effects of the same fire God would show that it was no common fire, but
kindled by the breath of the Almighty, Isa. 30:23. `(5.)` It is twice
taken notice of in scripture that they died childless, Num. 3:4, and 1
Chr. 24:2. By their presumption they had reproached God\'s name, and God
justly blotted out their names, and laid that honour in the dust which
they were proud of.

`2.` But why did the Lord deal thus severely with them? Were they not the
sons of Aaron, the saint of the Lord, nephews to Moses, the great
favourite of heaven? Was not the holy anointing oil sprinkled upon them,
as men whom God had set apart for himself? Had they not diligently
attended during the seven days of their consecration, and kept the
charge of the Lord, and might not that atone for this rashness? Would it
not excuse them that they were young men, as yet unexperienced in these
services, that it was the first offence, and done in a transport of joy
for their elevation? And besides, never could men be worse spared: a
great deal of work was now lately cut out for the priests to do, and the
priesthood was confined to Aaron and his seed; he has but four sons; if
two of them die, there will not be hands enough to do the service of the
tabernacle; if they die childless, the house of Aaron will become weak
and little, and the priesthood will be in danger of being lost for want
of heirs. But none of all these considerations shall serve either to
excuse the offence or bring off the offenders. For, `(1.)` The sin was
greatly aggravated. It was a manifest contempt of Moses, and the divine
law that was given by Moses. Hitherto it had been expressly observed
concerning every thing that was done that they did it as the Lord
commanded Moses, in opposition to which it is here said they did that
which the Lord commanded them not, but they did it of their own heads.
God was now teaching his people obedience, and to do every thing by
rule, as becomes servants; for priests therefore to break rules and
disobey was such a provocation as must by no means go unpunished. Their
character made their sin more exceedingly sinful. For the sons of Aaron,
his eldest sons, whom God had chosen to be immediate attendants upon
him, for them to be guilty of such a piece of presumption, it cannot be
suffered. There was in their sin a contempt of God\'s glory, which had
now newly appeared in fire, as if that fire were needless, they had as
good of their own before. `(2.)` Their punishment was a piece of necessary
justice, now at the first settling of the ceremonial institutions. It is
often threatened in the law that such and such offenders should be cut
off from the people; and here God explained the threatening with a
witness. Now that the laws concerning sacrifices were newly made, lest
any should be tempted to think lightly of them because they descended to
many circumstances which seemed very minute, these that were the first
transgressors were thus punished, for warning to others, and to show how
jealous God is in the matters of his worship. Thus he magnified the law
and made it honourable; and let his priests know that the caution which
so often occurs in the laws concerning them, that they must do so that
they die not, was not a mere bugbear, but fair warning of their danger,
if they did the work of the Lord negligently. And no doubt this
exemplary piece of justice at first prevented many irregularities
afterwards. Thus Ananias and Sapphira were punished, when they presumed
to lie to the Holy Ghost, that newly-descended fire. `(3.)` As the
people\'s falling into idolatry, presently after the moral law was
given, shows the weakness of the law and its insufficiency to take away
sin, so the sin and punishment of these priests show the imperfection of
that priesthood from the very beginning, and its inability to shelter
any from the fire of God\'s wrath otherwise than as it was typical of
Christ\'s priesthood, in the execution of which there never was, nor can
be, any irregularity, or false step taken.

### Verses 3-7

We may well think that when Nadab and Abihu were struck with death all
about them were struck with horror, and every face, as well as theirs,
gathered blackness. Great consternation, no doubt, seized them, and they
were all full of confusion; but, whatever the rest were, Moses was
composed, and knew what he said and did, not being displeased, as David
was in a like case, 2 Sa. 6:8. But though it touched him in a very
tender part, and was a dreadful damp to one of the greatest joys he ever
knew, yet he kept possession of his own soul, and took care to keep good
order and a due decorum in the sanctuary.

`I.` He endeavours to pacify Aaron, and to keep him in a good frame under
this sad dispensation, v. 3. Moses was a brother that was born for
adversity, and has taught us, by his example, with seasonable counsels
and comforts to support the weak, and strengthen the feeble-minded.
Observe here,

`1.` What it was that Moses suggested to his poor brother upon this
occasion: This is it that the Lord spoke. Note, The most quieting
considerations under affliction are those that are fetched from the word
of God. So and so the Lord hath said, and it is not for us to gainsay
it. Note, also, In all God\'s providences it is good to observe the
fulfilling of scripture, and to compare God\'s word and his works
together, which if we do we shall find an admirable harmony and
agreement between them, and that they mutually explain and illustrate
each other. But, `(1.)` Where did God speak this? We do not find the very
words; but to this purport he had said (Ex. 19:22), Let the priests who
come near to the Lord sanctify themselves, lest the Lord break forth
upon them. Indeed the whole scope and tenour of his law spoke this, that
being a holy God, and a sovereign Lord, he must always be worshipped
with holiness and reverence, and exactly according to his own
appointment; and, if any jest with him, it is at their peril. Much had
been said to this purport, as Ex. 29:43, 44; 34:14; ch. 8:35. `(2.)` What
was it that God spoke? It was this (the Lord by his grace speak it to
all our hearts!) I will be sanctified in those that come nigh me,
whoever they are, and before all the people I will be glorified. Note,
First, Whenever we worship God, we come nigh unto him, as spiritual
priests. This consideration ought to make us very reverent and serious
in all acts of devotion, that in them we approach to God, and present
ourselves before him. Secondly, It concerns us all, when we come nigh to
God, to sanctify him, that is, to give him the praise of his holiness,
to perform every religious exercise as those who believe that the God
with whom we have to do is a holy God, a God of spotless purity and
transcendent perfection, Isa. 8:13. Thirdly, When we sanctify God we
glorify him, for his holiness is his glory; and, when we sanctify him in
our solemn assemblies, we glorify him before all the people, confessing
our own belief of his glory and desiring that others also may be
affected with it. Fourthly, If God be not sanctified and glorified by
us, he will be sanctified and glorified upon us. He will take vengeance
on those that profane his sacred name by trifling with him. If his rent
be not paid, it shall be distrained for. `(3.)` But what was this to the
present case? What was there in this to quiet Aaron? Two things:-`[1.]`
This must silence him, that his sons deserved their death; for they were
thus cut off from their people because they did not sanctify and glorify
God. The acts of necessary justice, how hard soever they may seem to
bear upon the persons concerned, are not to be complained of, but
submitted to. `[2.]` This must satisfy him, that the death of his sons
redounded to the honour of God, and his impartial justice would for it
be adored throughout all ages.

`2.` What good effects this had upon him: Aaron held his peace, that is,
he patiently submitted to the holy will of God in this sad providence,
was dumb, and opened not his mouth, because God did it. Something he was
ready to say by way of complaint (as losers think they may have leave to
speak), but he wisely suppressed it, laid his hand upon his mouth, and
said nothing, for fear lest he should offend with his tongue, now that
his heart was hot within him. Note, `(1.)` When God corrects us or ours
for sin, it is our duty to be silent under the correction, not to
quarrel with God, arraign his justice, or charge him with folly, but to
acquiesce in all that God does; not only bearing, but accepting, the
punishment of iniquity, and saying, as Eli, in a case not much unlike
this, It is the Lord, let him do what seemeth him good, 1 Sa. 3:18. If
our children have sinned against God (as Bildad puts the case, Job 8:4),
and he have cast them away for their transgression, though it must needs
be grievous to think that the children of our love should be the
children of God\'s wrath, yet we must awfully adore the divine justice,
and make no exceptions against its processes. `(2.)` The most effectual
arguments to quiet a gracious spirit under afflictions are those that
are fetched from God\'s glory; this silenced Aaron. It is true he is a
loser in his comforts by this severe execution, but Moses has shown him
that God is a gainer in his glory, and therefore he has not a word to
say against it: if God be sanctified, Aaron is satisfied. Far be it form
him that he should honour his sons more than God, or wish that God\'s
name, or house, or law, should be exposed to reproach or contempt for
the preserving of the reputation of his family. No; now, as well as in
the matter of the golden calf, Levi does not acknowledge his brethren,
nor know his own children; and therefore they shall teach Jacob thy
judgments, and Israel thy law, Deu. 33:9, 10. Ministers and their
families are sometimes exercised with sore trials that they may be
examples to the believers of patience and resignation to God, and they
may comfort others with that with which they themselves have been
comforted.

`II.` Moses gives orders about the dead bodies. It was not fit that they
should be left to lie where they fell; yet their own father and
brethren, the amazed spectators of this dismal tragedy, durst not offer
to lift them up, no, not to see whether there was any life left in them;
they must neither be diverted from nor unfitted for the great work that
was now upon their hands. Let the dead bury their dead, but they must go
on with their service; that is, \"Rather let the dead be unburied, if
there be nobody else to do it, than that work for God should be left
undone by those whom he has called to it.\" But Moses takes care of this
matter, that though they died by the hand of justice in the act of sin,
yet they should be decently buried, and they were so, 5:4, 5. 1. Some of
their nearest relations were employed in it, who were cousins-german to
their father, and are here named, who would perform this office with
tenderness and respect. They were Levites only, and might not have come
into the sanctuary, no, not upon such an occasion as this, if they had
not had a special command for it. 2. They carried them out of the camp
to be burned, so far were they from burying them in the place of
worship, or the court of it, according to our modern usage, though they
died there, that they did not bury them, nor any of their dead, within
the lines of their camp; as afterwards their burying places were out of
their cities. The tabernacle was pitched in the midst of the camp, so
that they could not carry these dead priests to their graves without
carrying them through one of the squadrons of the camp; and doubtless it
was a very awful affecting sight to the people. The names of Nadab and
Abihu had become very great and honourable among them; none more talked
of, nor more expected to appear abroad after the days of their
consecration, to receive the honours and caresses of the crowd, whose
manner it is to adore the rising sun; and next to Moses and Aaron, who
were old and going off, Nadab and Abihu (who had been in the mount with
God, Ex. 24:1) were looked upon as the great favourites of heaven, and
the hopes of their people; and now on a sudden, when the tidings of the
event had scarcely reached their ears, to see them both carried out
dead, with the visible marks of divine vengeance upon them, as
sacrifices to the justice of God, they could not choose but cry out, Who
is able to stand before this holy Lord God? 1 Sa. 6:20. 3. They carried
them out (and probably buried them) in their coats, and the garments of
their priesthood, which they had lately put on, and perhaps were too
proud of. Thus the impartiality of God\'s justice was proclaimed, and
all the people were made to know that even the priests\' garments would
not protect an offender from the wrath of God. And it was easy to argue,
\"If they escape not when they transgress, can we expect to go
unpunished?\" And the priests\' clothes being so soon made grave-clothes
might intimate both that the law worketh death, and that in the process
of time that priesthood itself should be abolished and buried in the
grave of the Lord Jesus.

`III.` He gives directions about the mourning.

`1.` That the priests must not mourn. Aaron and his two surviving sons,
though sad in spirit, must not use any outward expressions of sorrow
upon this sad occasion, nor so much as follow the corpse one step from
the door of the tabernacle, v. 7. It was afterwards forbidden to the
high priest to use the ceremonies of mourning for the death of any
friend whatsoever, though it were a father or mother (ch. 21:11); yet it
was allowed at the same time to the inferior priests to mourn for their
near relations, v. 2, 3. But here it was forbidden both to Aaron and his
sons, because, `(1.)` They were now actually waiting, doing a great work,
which must by no means cease (Neh. 6:3); and it was very much for the
honour of God that their attendance on him should take place of their
respects to their nearest relations, and that all services should give
way to those of their ministry. By this they must make it to appear that
they had a greater value and affection for their God and their work than
for the best friend they had in the world; as Christ did, Mt. 12:47, 48.
And we are hereby taught, when we are serving God in holy duties, to
keep out minds, as much as may be, intent and engaged, and not to suffer
them to be diverted by any worldly thoughts, or cares, or passions. Let
us always attend upon the Lord without distraction. `(2.)` Their brethren
were cut off for their transgression by the immediate hand of God, and
therefore they must not mourn for them lest they should seem to
countenance the sin, or impeach the justice of God in the punishment.
Instead of lamenting their own loss, they must be wholly taken up in
applauding the sentence, and subscribing to the equity of it. Note, The
public concerns of God\'s glory ought to lie nearer our hearts than any
private affections of our own. Observe, How Moses frightens them into
this submission, and holds the rod over them to still their crying (v.
6): \"Lest you die likewise, and lest wrath come upon all the people,
who may be in danger of suffering for your irreverence, and
disobedience, and ungoverned passions;\" and again (v. 7), lest you die.
See here what use we are to make of the judgments of God upon others; we
must double our guard over ourselves, lest we likewise perish. The
death, especially the sudden death, of others, instead of moving our
passion, should compose us into a holy reverence of God, a cautious
separation from all sin, and a serious expectation of our own death. The
reason given them is because the anointing oil of your God is upon you,
the honour of which must be carefully preserved by your doing the duty
of your office with cheerfulness. Note, Those that through grace have
received the anointing ought not to disturb themselves with the sorrow
of the world, which worketh death. It was very hard, no doubt, for Aaron
and his sons to restrain themselves upon such an extraordinary occasion
from inordinate grief, but reason and grace mastered the passion, and
they bore the affliction with an obedient patience: They did according
to the word of Moses, because they knew it to be the word of God. Happy
those who thus are themselves under God\'s government, and have their
passions under their own government.

`2.` The people must mourn: Let the whole house of Israel bewail the
burning which the Lord has kindled. The congregation must lament, not
only the loss of their priests, but especially the displeasure of God
which appeared in it. They must bewail the burning that was kindled,
that it might not burn further. Aaron and his sons were in danger of
being too much affected with the providence, and therefore they are
forbidden to mourn: the house of Israel were in danger of being too
little affected with it, and therefore they are commanded to lament.
Thus nature must always be governed by grace, according as it needs to
be either constrained or restrained.

### Verses 8-11

Aaron having been very observant of what God said to him by Moses, now
God does him the honour to speak to him immediately (v. 8): The Lord
spoke unto Aaron, and the rather because what was now to be said Aaron
might perhaps have taken amiss from Moses, as if he had suspected him to
have been a gluttonous man and a wine-bibber, so apt are we to resent
cautions as accusations; therefore God saith it himself to him, Do not
drink wine, nor strong drink, when you go into the tabernacle, and this
at their peril, lest you die, v. 9. Probably they had seen the ill
effect of it in Nadab and Abihu, and therefore must take warning by
them. Observe here, 1. The prohibition itself: Do not drink wine nor
strong drink. At other times they were allowed it (it was not expected
that every priest should be a Nazarite), but during the time of their
ministration they were forbidden it. This was one of the laws in
Ezekiel\'s temple (Eze. 44:21), and so it is required of gospel
ministers that they be not given to wine, 1 Tim. 3:3. Note, Drunkenness
is bad in any, but it is especially scandalous and pernicious in
ministers, who of all men ought to have the clearest heads and the
cleanest hearts. 2. The penalty annexed to the prohibition: Lest you
die; lest you die when you are in drink, and so that day come upon you
unawares, Lu. 21:34. Or, \"Lest you do that which will make you liable
to be cut off by the hand of God.\" The danger of death we are
continually in should engage us to be sober, 1 Pt. 4:7. It is a pity
that it should ever be used for the support of licentiousness, as it is
by those who argue, Let us eat and drink, for to-morrow we die. 3. The
reasons assigned for this prohibition. They must needs to be sober, else
they could not duly discharge their office; they will be in danger of
erring through wine, Isa. 28:7. They must be sure to keep sober, `(1.)`
That they might themselves be able to distinguish, in their
ministrations, between that which was sacred and that which was common,
and might never confound them, v. 10. It concerns the Lord\'s ministers
to put a difference between holy and unholy, both things and persons,
that they may separate between the precious and the vile, Jer. 15:19.
`(2.)` That they might be able to teach the people (v. 11), for that was a
part of the priests\' work (Deu. 33:10); and those that are addicted to
drunkenness are very unfit to teach people God\'s statutes, both because
those that live after the flesh can have no experimental acquaintance
with the things of the Spirit, and because such teachers pull down with
one hand what they build up with the other.

### Verses 12-20

Moses is here directing Aaron to go on with his service after this
interruption. Afflictions should rather quicken us to our duty than take
us off from it. Observe (v. 12), He spoke unto Aaron and to his sons
that were left. The notice taken of their survivorship intimates, 1.
That Aaron should take comfort under the loss of two of his sons, from
this consideration, that God had graciously spared him the other two,
and that he had reason to be thankful for the remnant that was left, and
all his sons were not dead, and, in token of his thankfulness to God, to
go on cheerfully in his work. 2. That God\'s sparing them should be an
engagement upon them to proceed in his service, and not to fly off from
it. Here were four priests consecrated together, two were taken away,
and two left; therefore the two that were left should endeavour to fill
up the places of those that were gone, by double care and diligence in
the services of the priesthood. Now,

`I.` Moses repeats the directions he had formerly given them about eating
their share of the sacrifices, v. 12-14, 15. The priests must learn not
only to put a difference between the holy and the unholy, as they had
been taught (v. 10), but also to distinguish between that which was most
holy and that which was only holy of the things that were to eat. That
part of the meat-offering which remained to the priest was most holy,
and therefore must be eaten in the courts of the tabernacle, and by
Aaron sons only (v. 12, 13); but the breast and shoulder of the
peace-offerings might be eaten in any decent place out of the courts of
the tabernacle, and by the daughters of their families. The
meat-offerings, being annexed to the burnt-offerings, were intended only
and wholly for the glory of God; but the peace-offerings were ordained
for the furtherance of men\'s joy and comfort; the former therefore were
the more sacred, and to be had more in veneration. This distinction the
priests must carefully observe, and take heed of making any blunders.
Moses does not pretend to give any reasons for this difference, but
refers to his instructions: For so am I commanded, v. 13. This was
reason enough; he had received of the Lord all that he delivered unto
them, 1 Co. 11:23.

`II.` He enquires concerning one deviation from the appointment, which it
seems had happened upon this occasion, which was this:-There was a goat
to be sacrificed as a sin-offering or the people, ch. 9:15. Now the law
of the sin-offerings was that if the blood of them was brought into the
holy place, as that of the sin-offerings for the priest was, then the
flesh was to be burnt without the camp; otherwise it was to be eaten by
the priest in the holy place, ch. 6:30. The meaning of this is here
explained (v. 17), that the priests did hereby bear the iniquity of the
congregation, that is, they were types of him who was to be made sin for
us, and on whom God would lay the iniquity of us all. Now the blood of
this goat was not brought into the holy place, and yet, it seems, it was
burnt without the camp. Now observe here, 1. The gentle reproof Moses
gives to Aaron and his sons for this irregularity. Here again Aaron sons
are said to be those that were left alive (v. 16), who therefore ought
to have taken warning; and Moses was angry with them. Though he was the
meekest man in the world, it seems he could be angry; and when he
thought God was disobeyed and dishonoured, and the priesthood
endangered, he would be angry. Yet observe how very mildly he deals with
Aaron and his sons, considering their present affliction. He only tells
them they should indeed have eaten it in the holy place, but is willing
to hear what they have to say for themselves, being loth to speak to the
grief of those whom God had wounded.

`2.` The plausible excuse which Aaron makes for this mistake. Moses
charged the fault upon Eleazar and Ithamar (v. 16), but it is probable
that what they did was by Aaron direction, and therefore he apologized
for it. He might have pleaded that this was a sin-offering for the
congregation, and if it had been a bullock it must have been wholly
burnt (ch. 4:21), and therefore why not now that it was a goat? But it
seems it was otherwise ordered at this time, and therefore he makes his
affliction his excuse, v. 19. Observe, `(1.)` How he speaks of affliction:
Such things have befallen me, such sad things, which could not but go
near his heart, and make it very happy. He was a high priest taken from
among men, and could not put off natural affection when he put on the
holy garments. He held his peace (v. 3), yet his sorrow was stirred, as
David\'s, Ps. 39:2. Note, There may be a deep sense of affliction even
where there is a sincere resignation to the will of God in the
affliction. \"Such things as never befel me before, and as I little
expected now. My spirits cannot but sink, when I see my family sinking;
I must needs be heavy, when God is angry:\" thus it is easy to say a
great deal to aggravate an affliction, but it is better to say little.
`(2.)` How he makes this an excuse for his varying from the appointment
about the sin-offering. He could not have eaten it but in his mourning,
and with a sorrowful spirit; and would this have been accepted? He does
not plead that his heart was so full of grief that he had no appetite
for it, but that he feared it would not be accepted. Note, `[1.]`
Acceptance with God is the great thing we should desire and aim at in
all our religious services, particularly in the Lord\'s supper, which is
our eating of the sin-offering. `[2.]` The sorrow of the world is a very
great hindrance to our acceptable performance of holy duties, both as it
is discomposing to ourselves, takes off our chariot-wheels and makes us
drive heavily (1 Sa. 1:7, 8), and as it is displeasing to God, whose
will it is that we should serve him cheerfully, Deu. 12:7. Mourner\'s
bread was polluted, Hos. 9:4. See Mal. 3:14.

`3.` The acquiescence of Moses in this excuse: He was content, v. 20.
Perhaps he thought it justified what they had done. God had provided
that what could not be eaten might be burnt. Our unfitness for duty,
when it is natural and not sinful, will have great allowances made for
it; and God will have mercy and not sacrifice. At least he thought it
did very much extenuate the fault; the spirit indeed was willing, but
the flesh was weak. God by Moses showed that he considered his frame. It
appeared that Aaron sincerely aimed at God\'s acceptance; and those that
do so with an upright heart shall find he is not extreme to mark what
they do amiss. Nor must we be severe in our animadversions upon every
mistake, considering ourselves, lest we also be tempted.
