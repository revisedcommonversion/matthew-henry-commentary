Leviticus, Chapter 27
=====================

Commentary
----------

The last verse of the foregoing chapter seemed to close up the
statute-book; yet this chapter is added as an appendix. Having given
laws concerning instituted services, here he directs concerning vows and
voluntary services, the free-will offerings of their mouth. Perhaps some
devout serious people among them might be so affected with what Moses
had delivered to them in the foregoing chapter as in a pang of zeal to
consecrate themselves, or their children, or estates to him: this,
because honestly meant, God would accept; but, because men are apt to
repent of such vows, he leaves room for the redemption of what had been
so consecrated, at a certain rate. Here is, `I.` The law concerning what
was sanctified to God, persons (v. 2-8), cattle, clean or unclean (v.
9-13), houses and lands (v. 15-25), with an exception of firstlings, (v.
26, 27). `II.` Concerning what was devoted (v. 28, 29). `III.` Concerning
tithes (v. 30, etc.).

### Verses 1-13

This is part of the law concerning singular vows, extraordinary ones,
which though God did not expressly insist on, yet, if they were
consistent with and conformable to the general precepts, he would be
well pleased with. Note, We should not only ask, What must we do, but,
What may we do, for the glory and honour of God? As the liberal devises
liberal things (Isa. 32:8), so the pious devises pious things, and the
enlarged heart would willingly do something extraordinary in the service
of so good a Master as God is. When we receive or expect some singular
mercy it is good to honour God with some singular vow.

`I.` The case is here put of persons vowed to God by a singular vow, v. 2.
If a man consecrated himself, or a child, to the service of the
tabernacle, to be employed there in some inferior office, as sweeping
the floor, carrying out ashes, running of errands, or the like, the
person so consecrated shall be for the Lord, that is, \"God will
graciously accept the good-will.\" Thou didst well that it was in thy
heart, 2 Chr. 6:8. But forasmuch as he had no occasion to use their
service about the tabernacle, a whole tribe being appropriated to the
use of it, those that were thus vowed were to be redeemed, and the money
paid for their redemption was employed for the repair of the sanctuary,
or other uses of it, as appears by 2 Ki. 12:14, where it is called, in
the margin, the money of the souls of his estimation. A book of rates is
accordingly provided, by which the priests were to go in their
estimation. Here is, 1. The rate of the middle-aged, between twenty and
threescore, these were valued highest, because most serviceable; a male
fifty shekels, and a female thirty, v. 3, 4. The females were then less
esteemed, but not so in Christ; for in Christ Jesus there is neither
male nor female, Gal. 3:28. Note, Those that are in the prime of their
time must look upon themselves as obliged to do more in the service of
God and their generation than can be expected either from minors, that
have not yet arrived to their usefulness, or from the aged, that have
survived it. 2. The rate of the youth between five years old and twenty
was less, because they were then less capable of doing service, v. 5. 3.
Infants under five years old were capable of being vowed to God by their
parents, even before they were born, as Samuel was, but not to be
presented and redeemed till a month old, that, as one sabbath passed
over them before they were circumcised, so one new moon might pass over
them before they were estimated; and their valuation was but small, v.
6. Samuel, who was thus vowed to God, was not redeemed, because he was a
Levite, and a particular favourite, and therefore was employed in his
childhood in the service of the tabernacle. 4. The aged are valued less
than youth, but more than children, v. 7. And the Hebrews observe that
the rate of an aged woman is two parts of three to that of an aged man,
so that in that age the female came nearest to the value of the male,
which occasioned (as bishop Patrick quotes it here) this saying among
them, That an old woman in a house is a treasure in a house. Paul sets a
great value upon the aged women, when he makes them teachers of good
things, Tit. 2:3. 5. The poor shall be valued according to their
ability, v. 8. Something they must pay, that they might learn not to be
rash in vowing to God, for he hath no pleasure in fools, Eccl. 5:4. Yet
not more than their ability, but secundum tenementum-according to their
possessions, that they might not ruin themselves and their families by
their zeal. Note, God expects and requires from men according to what
they have, and not according to what they have not, Lu. 21:4.

`II.` The case is put of beasts vowed to God, 1. If it was a clean beast,
such as was offered in sacrifice, it must not be redeemed, nor any
equivalent given for it: It shall be holy, v. 9, 10. After it was vowed,
it was not to be put to any common use, nor changed upon second
thoughts; but it must be either offered upon the altar, or, if through
any blemish it was not meet to be offered, he that vowed it should not
take advantage of that, but the priests should have it for their own use
(for they were God\'s receivers), or it should be sold for the service
of the sanctuary. This teaches caution in making vows and constancy in
keeping them when they are made; for it is a snare to a man to devour
that which is holy, and after vows to make enquiry, Prov. 20:25. And to
this that rule of charity seems to allude (2 Co. 9:7), Every man,
according as he purposeth in his heart, so let him give. 2. If it was an
unclean beast, it should go to the use of the priest at such a value;
but he that vowed it, upon paying that value in money, and adding a
fifth part more to it, might redeem it if he pleased, v. 11-13. It was
fit that men should smart for their inconstancy. God has let us know his
mind concerning his service, and he is not pleased if we do not know our
own. God expects that those that deal with him should be at a point, and
way what they will stand to.

### Verses 14-25

Here is the law concerning real estates dedicated to the service of God
by a singular vow.

`I.` Suppose a man, in his zeal for the honour of God, should sanctify his
house to God (v. 14), the house must be valued by the priest, and the
money got by the sale of it was to be converted to the use of the
sanctuary, which by degrees came to be greatly enriched with dedicated
things, 1 Ki. 15:15. But, if the owner be inclined to redeem it himself,
he must not have it so cheap as another, but must add a fifth part to
the price, for he should have considered before he had vowed it, v. 15.
To him that was necessitous God would abate the estimation (v. 8); but
to him that was fickle and humoursome, and whose second thoughts
inclined more to the world and his secular interest than his first, God
would rise in the price. Blessed be God, there is a way of sanctifying
our houses to be holy unto the Lord, without either selling them or
buying them. If we and our houses serve the Lord, if religion rule in
them, and we put away iniquity far from them, and have a church in our
house, holiness to the Lord is written upon it, it is his, and he will
dwell with us in it.

`II.` Suppose a man should sanctify some part of his land to the Lord,
giving it to pious uses, then a difference must be made between land
that came to the donor by descent and that which came by purchase, and
accordingly the case altered.

`1.` If it was the inheritance of his fathers, here called the field of
his possession, which pertained to his family from the first division of
Canaan, he might not give it all, no, not to the sanctuary; God would
not admit such a degree of zeal as ruined a man\'s family. But he might
sanctify or dedicate only some part of it, v. 16. And in that case, `(1.)`
The land was to be valued (as our countrymen commonly compute land) by
so many measures\' sowing of barley. So much land as would take a homer,
or chomer, of barley, which contained ten ephahs, Eze. 45:11 (not, as
some have here mistaken it, an omer, which was but a tenth part of an
ephah, Ex. 16:36), was valued at fifty shekels, a moderate price (v.
16), and that if it were sanctified immediately from the year of
jubilee, v. 17. But, if some years after, there was to be a discount
accordingly, even of that price, v. 18. And, `(2.)` When the value was
fixed, the donor might, if he pleased, redeem it for sixty shekels the
homer\'s sowing, which was with the addition of a fifth part: the money
then went to the sanctuary, and the land reverted to him that had
sanctified it, v. 19. But if he would not redeem it, and the priest sold
it to another, then at the year of jubilee, beyond which the sale could
not go, the land came to the priests, and was theirs for ever, v. 20,
21. Note, What is given to the Lord ought not to be given with a power
of revocation; what is devoted to the Lord must be his for ever, by a
perpetual covenant.

`2.` If the land was his own purchase, and came not to him from his
ancestors, then not the land itself, but the value of it was to be given
to the priests for pious uses, v. 22, 24. It was supposed that those
who, by the blessing of God, had grown so rich as to become purchasers
would think themselves obliged in gratitude to sanctify some part of
their purchase, at least (and here they are not limited, but they might,
if they pleased, sanctify the whole), to the service of God. For we
ought to give as God prospers us, 1 Co. 16:2. Purchasers are in a
special manner bound to be charitable. Now, forasmuch as purchased lands
were by a former law to return at the year of jubilee to the family from
which they were purchased, God would not have that law and the
intentions of it defeated by making the lands corban, a gift, Mk. 7:11.
But it was to be computed how much the land was worth for so many years
as were from the vow to the jubilee; for only so long it was his own,
and God hates robbery for burnt-offerings. We can never acceptably serve
God with that of which we have wronged our neighbour. And so much money
he was to give for the present, and keep the land in his own hands till
the year of jubilee, when it was to return free of all encumbrances,
even that of its being dedicated to him of whom it was bought. The value
of the shekel by which all these estimations were to be made is here
ascertained (v. 25); it shall be twenty gerahs, and every gerah was
sixteen barley-corns. This was fixed before (Ex. 30:13); and, whereas
there had been some alterations, it is again fixed in the laws of
Ezekiel\'s visionary temple (Eze. 45:12), to denote that the gospel
should reduce things to their ancient standard.

### Verses 26-34

Here is, `I.` A caution given that no man should make such a jest of
sanctifying things to the Lord as to sanctify any firstling to him, for
that was his already by the law, v. 26. Though the matter of a general
vow be that which we were before obliged to, as of our sacramental
covenant, yet a singular vow should be of that which we were not, in
such circumstances and proportions, antecedently bound to. The law
concerning the firstlings of unclean beasts (v. 27) is the same with
that before, v. 11, 12.

`II.` Things or persons devoted are here distinguished from things or
persons that were only sanctified. 1. Devoted things were most holy to
the Lord, and could neither revert nor be alienated, v. 28. They were of
the same nature with those sacrifices which were called most holy, which
none might touch but only the priests themselves. The difference between
these and other sanctified things arose from the different expression of
the vow. If a man dedicated any thing to God, binding himself with a
solemn curse never to alienate it to any other purpose, then it was a
thing devoted. 2. Devoted persons were to be put to death, v. 29. Not
that it was in the power of any parent or master thus to devote a child
or a servant to death; but it must be meant of the public enemies of
Israel, who, either by the appointment of God or by the sentence of the
congregation, were devoted, as the seven nations with which they must
make no league. The city of Jericho in particular was thus devoted, Jos.
6:17. The inhabitants of Jabesh-Gilead were put to death for violating
the curse pronounced upon those who came not up to Mizpeh, Jdg. 21:9,
10. Some think it was for want of being rightly informed of the true
intent and meaning of this law that Jephtha sacrificed his daughter as
one devoted, who might not be redeemed.

`III.` A law concerning tithes, which were paid for the service of God
before the law, as appears by Abraham\'s payment of them, (Gen. 14:20),
and Jacob\'s promise of them, Gen. 28:22. It is here appointed, 1. That
they should pay tithe of all their increase, their corn, trees, and
cattle, v. 30, 32. Whatsoever productions they had the benefit of God
must be honoured with the tithe of, if it were titheable. Thus they
acknowledged God to be the owner of their land, the giver of its fruits,
and themselves to be his tenants, and dependents upon him. Thus they
gave him thanks for the plenty they enjoyed, and supplicated his favour
in the continuance of it. And we are taught in general to honour the
Lord with our substance (Prov. 3:9), and in particular to support and
maintain his ministers, and to be ready to communicate to them, Gal.
6:6; 1 Co. 9:11. And how this may be done in a fitter and more equal
proportion than that of the tenth, which God himself appointed of old, I
cannot see. 2. That which was once marked for tithe should not be
altered, no, not for a better (v. 33), for Providence directed the rod
that marked it. God would accept it though it were not the best, and
they must not grudge it though it were, for it was what passed under the
rod. 3. That it should not be redeemed, unless the owner would give a
fifth part more for its ransom, v. 31. If men had the curiosity to
prefer what was marked for tithe before any other part of their
increase, it was fit that they should pay for their curiosity.

`IV.` The last verse seems to have reference to this whole book of which
it is the conclusion: These are the commandments which the Lord
commanded Moses, for the children of Israel. Many of these commandments
are moral, and of perpetual obligation; others of them, which were
ceremonial and peculiar to the Jewish economy, have notwithstanding a
spiritual significancy, and are instructive to us who are furnished with
a key to let us into the mysteries contained in them; for unto us, by
those institutions, is the gospel preached as well as unto them, Heb.
4:2. Upon the whole matter, we may see cause to bless God that we have
not come to mount Sinai, Heb. 12:18. 1. That we are not under the dark
shadows of the law, but enjoy the clear light of the gospel, which shows
us Christ the end of the law for righteousness, Rom. 10:4. The doctrine
of our reconciliation to God by a Mediator is not clouded with the smoke
of burning sacrifices, but cleared by the knowledge of Christ and him
crucified. 2. That we are not under the heavy yoke of the law, and the
carnal ordinances of it (as the apostle calls them, Heb. 9:10), imposed
till the time of reformation, a yoke which neither they nor their
fathers were able to bear (Acts 15:10), but under the sweet and easy
institutions of the gospel, which pronounces those the true worshippers
that worship the Father in spirit and truth, by Christ only, and in his
name, who is our priest, temple, altar, sacrifice, purification, and
all. Let us not therefore think that because we are not tied to the
ceremonial cleansings, feasts, and oblations, a little care, time, and
expense, will serve to honour God with. No, but rather have our hearts
more enlarge with free-will offerings to his praise, more inflamed with
holy love and joy, and more engaged in seriousness of thought and
sincerity of intention. Having boldness to enter into the holiest by the
blood of Jesus, let us draw near with a true heart, and full assurance
of faith, worshipping God with so much the more cheerfulness and humble
confidence, still saying, Blessed be God for Jesus Christ!
