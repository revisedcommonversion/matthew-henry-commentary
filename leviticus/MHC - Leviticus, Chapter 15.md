Leviticus, Chapter 15
=====================

Commentary
----------

In this chapter we have laws concerning other ceremonial uncleannesses
contracted either by bodily disease like that of the leper, or some
natural incidents, and this either, `I.` In men (v. 1-18). Or, `II.` In
women (v. 19-33). We need not be at all curious in explaining these
antiquated laws, it is enough if we observe the general intention; but
we have need to be very cautious lest sin take occasion by the
commandment to become more exceedingly sinful; and exceedingly sinful it
is when lust is kindled by sparks of fire from God\'s altar. The case is
bad with the soul when it is putrefied by that which should purify it.

### Verses 1-18

We have here the law concerning the ceremonial uncleanness that was
contracted by running issues in men. It is called in the margin (v. 2)
the running of the reins: a very grievous and loathsome disease, which
was, usually the effect and consequent of wantonness and uncleanness,
and a dissolute course of life, filling men\'s bones with the sins of
their youth, and leaving them to mourn at the last, when all the
pleasures of their wickedness have vanished, and nothing remains but the
pain and anguish of a rotten carcase and a wounded conscience. And what
fruit has the sinner then of those things whereof he has so much reason
to be ashamed? Rom. 6:21. As modesty is an ornament of grace to the head
and chains about the neck, so chastity is health to the navel and marrow
to the bones; but uncleanness is a wound and dishonour, the consumption
of the flesh and the body, and a sin which is often its own punishment
more than any other. It was also sometimes inflicted by the righteous
hand of God for other sins, as appears by David\'s imprecation of a
curse upon the family of Joab, for the murder of Abner. 2 Sa. 3:29, Let
there not fail from the house of Joab one that hath an issue, or is a
leper. A vile disease for vile deserts. Now whoever had this disease
upon him, 1. He was himself unclean, v. 2. He must not dare to come near
the sanctuary, it was at his peril if he did, nor might he eat of the
holy things. This signified the filthiness of sin, and of all the
productions of our corrupt nature, which render us odious to God\'s
holiness, and utterly unfit for communion with him. Out of a pure heart
well kept are the issues of life (Prov. 4:23), but out of an unclean
heart comes that which is defiling, Mt. 12:34, 35. 2. He made every
person and thing unclean that he touched, or that touched him, v. 4-12.
His bed, and his chair, and his saddle, and every thing that belonged to
him, could not be touched without a ceremonial uncleanness contracted,
which a man must remain conscious to himself of till sunset, and from
which he could not be cleansed without washing his clothes, and bathing
his flesh in water. This signified the contagion of sin, the danger we
are in of being polluted by conversing with those that are polluted, and
the need we have with the utmost circumspection to save ourselves from
this untoward generation. 3. When he was cured of the disease, yet he
could not be cleansed from the pollution without a sacrifice, for which
he was to prepare himself by seven days\' expectation after he was
perfectly clear from his distemper, and by bathing in spring water, v.
13-15. This signified the great gospel duties of faith and repentance,
and the great gospel privileges of the application of Christ\'s blood to
our souls for our justification and his grace for our sanctification.
God has promised to sprinkle clean water upon us, and to cleanse us from
all our filthiness, and has appointed us by repentance to wash and make
ourselves clean: he has also provided a sacrifice of atonement, and
requires us by faith to interest ourselves in that sacrifice; for it is
the blood of Christ his Son that cleanses us from all sin, and by which
atonement is made for us, that we may have admission into God\'s
presence and may partake of his favour.

### Verses 19-33

This is concerning the ceremonial uncleanness which women lay under from
their issues, both those that were regular and healthful, and according
to the course of nature (v. 19-24), and those that were unseasonable,
excessive, and the disease of the body; such was the bloody issue of
that poor woman who was suddenly cured by touching the hem of Christ\'s
garment, after she had lain twelve years under her distemper, and had
spent her estate upon physicians and physic in vain. This made the woman
that was afflicted with it unclean (v. 25) and every thing she touched
unclean, v. 26, 27. And if she was cured, and found by seven days\'
trial that she was perfectly free from her issue of blood, she was to be
cleansed by the offering of two turtle-doves or two young pigeons, to
make an atonement for her, v. 28, 29. All wicked courses, particularly
idolatries, are compared to the uncleanness of a removed woman (Eze.
36:17), and, in allusion to this, it is said of Jerusalem (Lam. 1:9),
Her filthiness is in her skirts, so that (as it follows, v. 17) she was
shunned as a menstruous woman.

`I.` The reasons given for all these laws (which we are ready to think
might very well have been spared) we have, v. 31. 1. Thus shall you
separate the children of Israel (for to them only and their servants and
proselytes these laws pertained) from their uncleanness; that is, `(1.)`
By these laws they were taught their privilege and honour, that they
were purified unto God a peculiar people, and were intended by the holy
God for a kingdom of priests, a holy nation; for that was a defilement
to them which was not so to others. `(2.)` They were also taught their
duty, which was to preserve the honour of their purity, and to keep
themselves from all sinful pollutions. It was easy for them to argue
that if those pollutions which were natural, unavoidable, involuntary,
their affliction and not their sin, rendered them for the time so odious
that they were not fit for communion either with God or man, much more
abominable and filthy were they if they sinned against the light and law
of nature, by drunkenness, adultery, fraud, and the like sins, which
defile the very mind and conscience. And, if these ceremonial pollutions
could not be done away but by sacrifice and offering, something greater
and much more valuable must be expected and depended upon for the
purifying of the soul from the uncleanness of sin. 2. Thus their dying
in their uncleanness by the hand of God\'s justice, if while they were
under any of these defilements they should come near the sanctuary,
would be prevented. Note, It is a dangerous thing to die in our
uncleanness; and it is our own fault if we do, since we have not only
fair warning given us, by God\'s law, against those things that will
defile us, but also such gracious provision made by his gospel for our
cleansing if at any time we be defiled. 3. In all these laws there seems
to be a special regard had to the honour of the tabernacle, to which
none must approach in their uncleanness, that they defile not my
tabernacle. Infinite Wisdom took this course to preserve in the minds of
that careless people a continual dread of, and veneration for, the
manifestations of God\'s glory and presence among them in his sanctuary.
Now that the tabernacle of God was with men familiarity would be apt to
breed contempt, and therefore the law made so many things of frequent
incidence to be ceremonial pollutions, and to involve an incapacity of
drawing near to the sanctuary (making death the penalty), that so they
might not approach without great caution, and reverence, and serious
preparation, and fear of being found unfit. Thus they were taught never
to draw near to God but with an awful humble sense of their distance and
danger, and an exact observance of every thing that was required in
order to their safety and acceptance.

`II.` And what duty must we learn from all this? 1. Let us bless God that
we are not under the yoke of these carnal ordinances, that, as nothing
can destroy us, so nothing can defile us, but sin. Those may now partake
of the Lord\'s supper who durst not then eat of the peace-offerings. And
the defilement we contract by our sins of daily infirmity we may be
cleansed from in secret by the renewed acts of repentance and faith,
without bathing in water or bringing an offering to the door of the
tabernacle. 2. Let us carefully abstain from all sin, as defiling to the
conscience, and particularly from all fleshly lusts, possessing our
vessel in sanctification and honour, and not in the lusts of
uncleanness, which not only pollute the soul, but war against it, and
threaten its ruin. 3. Let us all see how indispensably necessary real
holiness is to our future happiness, and get our hearts purified by
faith, that we may see God. Perhaps it is in allusion to these laws
which forbade the unclean to approach the sanctuary that when it is
asked, Who shall stand in God\'s holy place? it is answered, He that
hath clean hands and a pure heart (Ps. 24:3, 4); for without holiness no
man shall see the Lord.
