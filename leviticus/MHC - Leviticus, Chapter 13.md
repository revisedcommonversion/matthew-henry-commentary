Leviticus, Chapter 13
=====================

Commentary
----------

The next ceremonial uncleanness is that of the leprosy, concerning which
the law was very large and particular; we have the discovery of it in
this chapter, and the cleansing of the leper in the next. Scarcely any
one thing in all the levitical law takes up so much room as this. `I.`
Rules are here given by which the priest must judge whether the man had
the leprosy or no, according as the symptom was that appeared. 1. If it
was a swelling, a scab, or a bright spot (v. 1-17). 2. If it was a bile
(v. 18-23). 3. If it was in inflammation (v. 24-28). 4. If it was in the
head or beard (v. 29-37). 5. If it was a bright spot (v. 38, 39). 6. If
it was in a bald head (v. 40-44). `II.` Direction is given how the leper
must be disposed of (v. 45, 46). `III.` Concerning the leprosy in garments
(v. 47, etc.).

### Verses 1-17

`I.` Concerning the plague of leprosy we may observe in general, 1. That
it was rather an uncleanness than a disease; or, at least, so the law
considered it, and therefore employed not the physicians but the priests
about it. Christ is said to cleanse lepers, not to cure them. We do not
read of any that died of the leprosy, but it rather buried them alive,
by rendering them unfit for conversation with any but such as were
infected like themselves. Yet there is a tradition that Pharaoh, who
sought to kill Moses, was the first that ever was struck with this
disease, and that he died of it. It is said to have begun first in
Egypt, whence it spread into Syria. It was very well known to Moses,
when he put his own hand into his bosom and took it out leprous. 2. That
it was a plague inflicted immediately by the hand of God, and came not
from natural causes, as other diseases; and therefore must be managed
according to a divine law. Miriam\'s leprosy, and Gehazi\'s, and king
Uzziah\'s, were all the punishments of particular sins: and, if
generally it was so, no marvel there was so much care taken to
distinguish it from a common distemper, that none might be looked upon
as lying under this extraordinary token of divine displeasure but those
that really were so. 3. That it is a plague not now known in the world;
what is commonly called the leprosy is of a quite different nature. This
seems to have been reserved as a particular scourge for the sinners of
those times and places. The Jews retained the idolatrous customs they
had learnt in Egypt, and therefore God justly caused this with some
others of the diseases of Egypt to follow them. Yet we read of Naaman
the Syrian, who was a leper, 2 Ki. 5:1. 4. That there were other
breakings-out in the body which did very much resemble the leprosy, but
were not it, which might make a man sore and loathsome and yet not
ceremonially unclean. Justly are our bodies called vile bodies, which
have in them the seeds of so many diseases, by which the lives of so
many are made bitter to them. 5. That the judgment of it was referred to
the priests. Lepers were looked upon as stigmatized by the justice of
God, and therefore it was left to his servants the priests, who might be
presumed to know his mark best, to pronounce who were lepers and who
were not. All the Jews say, \"Any priest, though disabled by a blemish
to attend the sanctuary, might be a judge of the leprosy, provided the
blemish were not in his eye. And he might\" (they say) \"take a common
person to assist him in the search, but the priest only must pronounce
the judgment.\" 6. That it was a figure of the moral pollution of men\'s
minds by sin, which is the leprosy of the soul, defiling to the
conscience, and from which Christ alone can cleanse us; for herein the
power of his grace infinitely transcends that of the legal priesthood,
that the priest could only convict the leper (for by the law is the
knowledge of sin), but Christ can cure the leper, he can take away sin.
Lord, if thou wilt, thou canst make me clean, which was more than the
priests could do, Mt. 8:2. Some think that the leprosy signified, not so
much sin in general as a state of sin, by which men are separated from
God (their spot not being the spot of God\'s children), and scandalous
sin, for which men are to be shut out from the communion of the
faithful. It is a work of great importance, but of great difficulty, to
judge of our spiritual state: we have all cause to suspect ourselves,
being conscious to ourselves of sores and spots, but whether clean or
unclean is the question. A man might have a scab (v. 6) and yet be
clean: the best have their infirmities; but, as there were certain marks
by which to know that it was a leprosy, so there are characters of such
as are in the gall of bitterness, and the work of ministers is to
declare the judgment of leprosy and to assist those that suspect
themselves in the trial of their spiritual state, remitting or retaining
sin. And hence the keys of the kingdom of heaven are said to be given to
them, because they are to separate between the precious and the vile,
and to judge who are fit as clean to partake of the holy things and who
as unclean must be debarred from them.

`II.` Several rules are here laid down by which the judgment of the
priest must be governed. 1. If the sore was but skin-deep, it was to be
hoped it was not the leprosy, v. 4. But, if it was deeper than the skin,
the man must be pronounced unclean, v. 3. The infirmities that consist
with grace do not sink deep into the soul, but the mind still serves the
law of God, and the inward man delights in it, Rom. 7:22, 25. But if the
matter be really worse than it shows, and the inwards be infected, the
case is dangerous. 2. If the sore be at a stay, and do not spread, it is
no leprosy, v. 4, 5. But if it spread much abroad, and continue to do so
after several inspections, the case is bad, v. 7, 8. If men do not grow
worse, but a stop be put to the course of their sins and their
corruptions be checked, it is to be hoped they will grow better; but if
sin get ground, and they become worse every day, they are going
downhill. 3. If there was proud raw flesh in the rising, the priest
needed not to wait any longer, it was certainly a leprosy, v. 10, 11.
Nor is there any surer indication of the badness of a man\'s spiritual
state than the heart\'s rising in self-conceit, confidence in the flesh,
and resistance of the reproofs of the word and strivings of the Spirit.
4. If the eruption, whatever it was, covered all the skin from head to
foot, it was no leprosy (v. 12, 13); for it was an evidence that the
vitals were sound and strong, and nature hereby helped itself, throwing
out what was burdensome and pernicious. There is hope in the small-pox
when they come out well: so if men freely confess their sins, and hide
them not, there is no danger comparable to theirs that cover their sins.
Some gather this from it, that there is more hope of the profane than of
hypocrites. The publicans and harlots went into the kingdom of heaven
before scribes and Pharisees. In one respect, the sudden breakings-out
of passion, though bad enough, are not so dangerous as malice concealed.
Others gather this, that, if we judge ourselves, we shall not be judged;
if we see and own that there is no health in us, no soundness in our
flesh, by reason of sin, we shall find grace in the eyes of the Lord. 5.
The priest must take time in making his judgment, and not give it
rashly. If the matter looked suspicious, he must shut up the patient
seven days, and then seven days more, that his judgment might be
according to truth. This teaches all, both ministers and people, not to
be hasty in their censures, nor to judge any thing before the time. If
some men\'s sins go before unto judgment, the sins of others follow
after, and so men\'s good works; therefore let nothing be done suddenly,
1 Tim. 5:22, 24, 25. 6. If the person suspected was found to be clean,
yet he must wash his clothes (v. 6), because he had been under the
suspicion, and there had been in him that which gave ground for the
suspicion. Even the prisoner that is acquitted must go down on his
knees. We have need to be washed in the blood of Christ from our spots,
though they be not leprosy-spots; for who can say, I am pure from sin?
though there are those who through grace are innocent from the great
transgression.

### Verses 18-37

The priest is here instructed what judgment to make if there was any
appearance of a leprosy, either, 1. In an old ulcer, or bile, that has
been healed, v. 18, etc. When old sores, that seemed to be cured, break
out again, it is to be feared there is a leprosy in them; such is the
danger of those who, having escaped the pollutions of the world, are
again entangled therein and overcome. Or, 2. In a burn by accident, for
this seems to be meant, v. 24, etc. The burning of strife and contention
often proves the occasion of the rising up and breaking out of that
corruption which witnesses to men\'s faces that they are unclean. 3. In
a scall-head. And in this commonly the judgment turned upon a very small
matter. If the hair in the scall was black, it was a sign of soundness;
if yellow, it was an indication of a leprosy, v. 30-37. The other rules
in these cases are the same with those mentioned before. In reading of
these several sorts of ailments, it will be good for us, 1. To lament
the calamitous state of human life, which lies exposed to so many
grievances. What troops of diseases are we beset with on every side! and
they all entered by sin. 2. To give thanks to God if he has never
afflicted us with any of these sores: if the constitution is healthful,
and the body lively and easy, we are bound to glorify God with our
bodies.

### Verses 38-46

We have here,

`I.` Provisos that neither a freckled skin nor a bald head should be
mistaken for a leprosy, v. 38-41. Every deformity must not forthwith be
made a ceremonial defilement. Elisha was jeered for his bald head (2 Ki.
2:23); but it was the children of Bethel, that knew not the judgments of
their God, who turned it to his reproach.

`II.` A particular brand set upon the leprosy if at any time it did
appear in a bald head: The plague is in his head, he is utterly unclean,
v. 44. If the leprosy of sin have seized the head, if the judgment be
corrupted, and wicked principles which countenance and support wicked
practices, be embraced, it is an utter uncleanness, from which few are
ever cleansed. Soundness in the faith keeps the leprosy from the head,
and saves conscience from being shipwrecked.

`III.` Directions what must be done with the convicted leper. When the
priest, upon mature deliberation, had solemnly pronounced him unclean,

`1.` He must pronounce himself so, v. 45. He must put himself into the
posture of a mourner and cry, Unclean, unclean. The leprosy was not
itself a sin, but it was a sad token of God\'s displeasure and a sore
affliction to him that was under it. It was a reproach to his name, put
a full stop to his business in the world, cut him off from conversation
with his friends and relations, condemned him to banishment till he was
cleansed, shut him out from the sanctuary, and was, in effect, the ruin
of all the comfort he could have in this world. Heman, it would seem,
either was a leper or alludes to the melancholy condition of a leper,
Ps. 88:8, etc. He must therefore, `(1.)` Humble himself under the mighty
hand of God, not insisting upon his cleanness when the priest had
pronounced him unclean, but justifying God and accepting the punishment
of his iniquity. He must signify this by rending his clothes, uncovering
his head, and covering his upper lip, all tokens of shame and confusion
of face, and very significant of that self-loathing and self-abasement
which should fill the hearts of penitents, the language of which is
self-judging. Thus must we take to ourselves the shame that belongs to
us, and with broken hearts call ourselves by our own name, Unclean,
unclean-heart unclean, life unclean, unclean by original corruption,
unclean by actual transgression-unclean, and therefore worthy to be for
ever excluded from communion with God, and all hope of happiness in him.
We are all as an unclean thing (Isa. 64:6)-unclean, and therefore
undone, if infinite mercy do not interpose. `(2.)` He must give warning to
others to take heed of coming near him. Wherever he went, he must cry to
those he saw at a distance, \"I am unclean, unclean, take heed of
touching me.\" Not that the leprosy was catching, but by the touch of a
leper ceremonial uncleanness was contracted. Every one therefore was
concerned to avoid it; and the leper himself must give notice of the
danger. And this was all that the law could do, in that it was weak
through the flesh; it taught the leper to cry, Unclean, unclean, but the
gospel has put another cry into the lepers\' mouths, Lu. 17:12, 13,
where we find ten lepers crying with a loud voice, Jesus, Master, have
mercy on us. The law only shows us our disease; the gospel shows us our
help in Christ.

`2.` He must then be shut out of the camp, and afterwards, when they came
to Canaan, out of the city, town, or village, where he lived, and dwell
alone (v. 46), associating with none but those that were lepers like
himself. When king Uzziah became a leper, he was banished from his
palace, and dwelt in a separate house, 2 Chr. 26:21. And see 2 Ki. 7:3.
This typified the purity which ought to be preserved in the gospel
church, by the solemn and authoritative exclusion of scandalous sinners,
that hate to be reformed, from the communion of the faithful. Put away
from among yourselves that wicked person, 1 Co. 5:13.

### Verses 47-59

This is the law concerning the plague of leprosy in a garment, whether
linen or woollen. A leprosy in a garment, with discernible indications
of it, the colour changed by it, the garment fretted, the nap worn off,
and this in some one particular part of the garment, and increasing when
it was shut up, and not to be got out by washing is a thing which to us
now is altogether unaccountable. The learned confess that it was a sign
and a miracle in Israel, an extraordinary punishment inflicted by the
divine power, as a token of great displeasure against a person or
family. 1. The process was much the same with that concerning a leprous
person. The garment suspected to be tainted was not to be burnt
immediately, though, it may be, there would have been no great loss of
it; for in no case must sentence be given merely upon a surmise, but it
must be shown to the priest. If, upon search, it was found that there
was a leprous spot (the Jews say no bigger than a bean), it must be
burnt, or at least that part of the garment in which the spot was, v.
52, 57. If the cause of the suspicion was gone, it must be washed, and
then might be used, v. 58. 2. The signification also was much the same,
to intimate the great malignity there is in sin: it not only defiles the
sinner\'s conscience, but it brings a stain upon all his employments and
enjoyments, all he has and all he does. To those that are defiled and
unbelieving is nothing pure, Tit. 1:15. And we are taught hereby to hate
even the garments spotted with the flesh, Jude 23. Those that make their
clothes servants to their pride and lust may see them thereby tainted
with a leprosy, and doomed to the fire, Isa. 3:18-24. But the ornament
of the hidden man of the heart is incorruptible, 1 Pt. 3:4. The robes of
righteousness never fret nor are moth-eaten.
