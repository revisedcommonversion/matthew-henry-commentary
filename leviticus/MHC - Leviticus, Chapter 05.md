Leviticus, Chapter 5
====================

Commentary
----------

This chapter, and part of the next, concern the trespass-offering. The
difference between this and the sin-offering lay not so much in the
sacrifices themselves, and the management of them, as in the occasions
of the offering of them. They were both intended to make atonement for
sin; but the former was more general, this applied to some particular
instances. Observe what is here said, `I.` Concerning the trespass. If a
man sin, 1. In concealing his knowledge, when he is adjured (v. 1). 2.
In touching an unclean thing (v. 2, 3). 3. In swearing (v. 4). 4. In
embezzling the holy things (v. 14-16). 5. In any sin of infirmity (v.
17-19). Some other cases there are, in which these offerings were to be
offered (ch. 6:2-4; 14:12; 19:21; Num. 6:12). `II.` Concerning the
trespass-offerings, 1. Of the flock (v. 5, 6). 2. Of fowls (v. 7-10). 3.
Of flour (v. 11-13; but chiefly a ram without blemish (v. 15, etc.).

### Verses 1-6

`I.` The offences here supposed are, 1. A man\'s concealing the truth when
he was sworn as a witness to speak the truth, the whole truth, and
nothing but the truth. Judges among the Jews had power to adjure not
only the witnesses, as with us, but the person suspected (contrary to a
rule of our law, that no man is bound to accuse himself), as appears by
the high priest adjuring our Saviour, who thereupon answered, though
before he stood silent, Mt. 26:63, 64. Now (v. 1), If a soul sin (that
is, a person, for the soul is the man), if he hear the voice of swearing
(that is, if he be adjured to testify what he knows, by an oath of the
Lord upon him, 1 Ki. 8:31), if in such a case, for fear of offending one
that either has been his friend or may be his enemy, he refuses to give
evidence, or gives it but in part, he shall bear his iniquity. And that
is a heavy burden, which, if some course be not taken to get it removed,
will sink a man to the lowest hell. He that heareth cursing (that is,
that is thus adjured) and betrayeth it not (that is, stifles his
evidence, and does not utter it), he is a partner with the sinner, and
hateth his own soul; see Prov. 29:24. Let all that are called out at any
time to bear testimony think of this law, and be free and open in their
evidence, and take heed of prevaricating. An oath of the Lord is a
sacred thing, and not to be dallied with. 2. A man\'s touching any thing
that was ceremonially unclean, v. 2, 3. If a man, polluted by such
touch, came into the sanctuary inconsiderately, or if he neglected to
wash himself according to the law, then he was to look upon himself as
under guilt, and must bring his offering. Though his touching the
unclean thing contracted only a ceremonial defilement, yet his neglect
to wash himself according to the law was such an instance either of
carelessness or contempt as contracted a moral guilt. If at first it be
hidden from him, yet when he knows it he shall be guilty. Note, As soon
as ever God by his Spirit convinces our consciences of any sin or duty
we must immediately set in with the conviction, and prosecute it, as
those that are not ashamed to own our former mistake. 3. Rash swearing.
If a man binds himself by an oath that he will do or not do such a
thing, and the performance of his oath afterwards proves either unlawful
or impracticable, by which he is discharged from the obligation, yet he
must bring an offering to atone for his fully in swearing so rashly, as
David that he would kill Nabal. And then it was that he must say before
the angel that it was an error, Eccl. 5:6. He shall be guilty in one of
these (ch. 5:4), guilty if he do not perform his oath, and yet, if the
matter of it were evil, guilty if he do. Such wretched dilemmas as these
do some men bring themselves into by their own rashness and folly; go
which way they will their consciences are wounded, sin stares them in
the face, so sadly are they snared in the words of their mouth. A more
sad dilemma this is than that of the lepers, \"If we sit still, we die;
if we stir, we die.\" Wisdom and watchfulness beforehand would prevent
these straits.

`II.` Now in these cases, 1. The offender must confess his sin and bring
his offering (v. 5, 6); and the offering was not accepted unless it was
accompanied with a penitential confession and a humble prayer for
pardon. Observe, The confession must be particular, that he hath sinned
in that thing; such was David\'s confession (Ps. 51:4), I have done this
evil; and Achan\'s (Jos. 7:20), Thus and thus have I done. Deceit lies
in generals; many will own in general they have sinned, for that all
must own, so that it is not any particular reproach to them; but that
they have sinned in this thing they stand too much upon their honour to
acknowledge: but the way to be well assured of pardon, and to be well
armed against sin for the future, is to be particular in our penitent
confessions. 2. The priest must make atonement for him. As the atonement
was not accepted without his repentance, so his repentance would not
justify him without the atonement. Thus, in our reconciliation to God,
Christ\'s part and ours are both needful.

### Verses 7-13

Provision is here made for the poor of God\'s people, and the pacifying
of their consciences under the sense of guilt. Those that were not able
to bring a lamb might bring for a sin-offering a pair of turtle-doves or
two young pigeons; nay, if any were so extremely poor that they were not
able to procure these so often as they would have occasion, they might
bring a pottle of fine flour, and this should be accepted. Thus the
expense of the sin-offering was brought lower than that of any other
offering, to teach us that no man\'s poverty shall ever be a bar in the
way of his pardon. The poorest of all may have atonement made for them,
if it be not their own fault. Thus the poor are evangelized; and no man
shall say that he had not wherewithal to bear the charges of a journey
to heaven. Now,

`I.` If the sinner brought two doves, one was to be offered for a
sin-offering and the other for a burnt-offering, v. 7. Observe, 1.
Before he offered the burnt-offering, which was for the honour and
praise of God, he must offer the sin-offering, to make atonement. We
must first see to it that our peace be made with God, and then we may
expect that our services for his glory will be accepted. The
sin-offering must make way for the burnt-offering. 2. After the
sin-offering, which made atonement, came the burnt-offering, as an
acknowledgment of the great mercy of God in appointing and accepting the
atonement.

`II.` If he brought fine flour, a handful of it was to be offered, but
without either oil or frankincense (v. 11), not only because this would
make it too costly for the poor, for whose comfort this sacrifice was
appointed, but because it was a sin-offering, and therefore, to show the
loathsomeness of the sin for which it was offered, it must not be made
grateful either to the taste by oil or to the smell by frankincense. The
unsavouriness of the offering was to intimate that the sinner must never
relish his sin again as he had done. God by these sacrifices did speak,
`1.` Comfort to those that had offended, that they might not despair, nor
pine away in their iniquity; but, peace being thus made for them with
God, they might have peace in him. 2. Caution likewise not to offend any
more, remembering what an expensive troublesome thing it was to make
atonement.

### Verses 14-19

Hitherto in this chapter orders were given concerning those sacrifices
that were both sin-offerings and trespass-offerings, for they go by both
names, v. 6. Here we have the law concerning those that were properly
and peculiarly trespass-offerings, which were offered to atone for
trespasses done against a neighbour, those sins we commonly call
trespasses. Now injuries done to another may be either in holy things or
in common things; of the former we have the law in these verses; of the
latter in the beginning of the next chapter. If a man did harm (as it is
v. 16) in the holy things of the Lord, he thereby committed a trespass
against the priests, the Lord\'s ministers, who were entrusted with the
care of these holy things, and had the benefit of them. Now if a man did
alienate or convert to his own use any thing that was dedicated to God,
unwittingly, he was to bring this sacrifice; as suppose he had
ignorantly made use of the tithes, or first-fruits, or first-born of his
cattle, or (which, it should seem by ch. 22:14-16, is principally meant
here) had eaten any of those parts of the sacrifices which were
appropriated to the priests; this was a trespass. It is supposed to be
done through mistake, or forgetfulness, for want either of care or zeal;
for if it was done presumptuously, and in contempt of the law, the
offender died without mercy, Heb. 10:28. But in case of negligence and
ignorance this sacrifice was appointed; and Moses is told, 1. What must
be done in case the trespass appeared to be certain. The trespasser must
bring an offering to the Lord, which, in all those that were purely
trespass-offerings, must be a ram without blemish, \"of the second
year,\" say the Jewish doctors. He must likewise make restitution to the
priest, according to a just estimation of the thing which he had so
alienated, adding a fifth part to it, that he might learn to take more
heed next time of embezzling what was sacred to God, finding to his cost
that there was nothing got by it, and that he paid dearly for his
oversights. 2. What must be done in case it were doubtful whether he had
trespassed or no; he had cause to suspect it, but he wist it not (v.
17), that is, he was not very certain; in this case, because it is good
to be sure, he must bring his trespass-offering, and the value of that
which he feared he had embezzled, only he was not to add the fifth part
to it. Now this was designed to show the very great evil there is in
sacrilege. Achan, that was guilty of it presumptuously, died for it; so
did Ananias and Sapphira. But this goes further to show the evil of it,
that if a man had, through mere ignorance, and unwittingly, alienated
the holy things, nay, if he did but suspect that he had done so, he must
be at the expense, not only of a full restitution with interest, but of
an offering, with the trouble of bringing it, and must take shame to
himself, by making confession of it; so bad a thing is it to invade
God\'s property, and so cautious should we be to abstain from all
appearances of this evil. We are also taught here to be jealous over
ourselves with a godly jealousy, to ask pardon for the sin, and make
satisfaction for the wrong, which we do but suspect ourselves guilty of.
In doubtful cases we should take and keep the safer side.
