Leviticus, Chapter 7
====================

Commentary
----------

Here is, `I.` The law of the trespass-offering (v. 1-7), with some further
directions concerning the burnt-offering and the meat-offering (v.
8-10). `II.` The law of the peace-offering. The eating of it (v. 11-21),
on which occasion the prohibition of eating fat or blood is repeated (v.
22-27), and the priests\' share of it (v. 28-34). `III.` The conclusion of
those institutions (v. 35, etc.).

### Verses 1-10

Observe here, 1. Concerning the trespass-offering, that, being much of
the same nature with the sin-offering, it was to be governed by the same
rules, v. 6. When the blood and fat were offered to God to make
atonement, the priests were to eat the flesh, as that of the
sin-offering, in the holy place. The Jews have a tradition (as we have
it from the learned bishop Patrick) concerning the sprinkling of the
blood of the trespass-offering round about upon the altar, \"That there
was a scarlet line which went round about the altar exactly in the
middle, and the blood of the burnt-offerings was sprinkled round about
above the line, but that of the trespass-offerings and peace-offerings
round about below the line.\" As to the flesh of the trespass-offering,
the right to it belonged to the priest that offered it, v. 7. He that
did the work must have the wages. This was an encouragement to the
priests to give diligent attendance on the altar; the more ready and
busy they were the more they got. Note, The more diligent we are in the
services of religion the more we shall reap of the advantages of it. But
any of the priests, and the males of their families, might be invited by
him to whom it belonged to partake with him: Every male among the
priests shall eat thereof, that is, may eat thereof, in the holy place,
v. 6. And, no doubt, it was the usage to treat one another with those
perquisites of their office, by which friendship and fellowship were
kept up among the priests. Freely they had received, and must freely
give. It seems the offerer was not himself to have any share of his
trespass-offering, as he was to have of his peace-offering; but it was
all divided between the altar and the priest. They offered
peace-offerings in thankfulness for mercy, and then it was proper to
feast; but they offered trespass-offerings in sorrow for sin, and then
fasting was more proper, in token of holy mourning, and a resolution to
abstain from sin. 2. Concerning the burnt-offering it is here appointed
that the priest that offered it should have the skin (v. 8), which no
doubt he might make money of. \"This\" (the Jews say) \"is meant only
for the burnt-offerings which were offered by particular persons; for
the profit of the skins of the daily burnt-offerings for the
congregation went to the repair of the sanctuary.\" Some suggest that
this appointment will help us to understand God\'s clothing our first
parents with coats of skins, Gen. 3:21. It is probable that the beasts
whose skins they were were offered in sacrifice as whole
burnt-offerings, and that Adam was the priest that offered them; and
then God gave him the skins, as his fee, to make clothes of for himself
and his wife, in remembrance of which the skins ever after pertained to
the priest; and see Gen. 27:16. 3. Concerning the meat-offering, if it
was dressed, it was fit to be eaten immediately; and therefore the
priest that offered it was to have it, v. 9. If it was dry, there was
not so much occasion for being in haste to use it; and therefore an
equal dividend of it must be made among all the priests that were then
in waiting, v. 10.

### Verses 11-34

All this relates to the peace-offerings: it is the repetition and
explication of what we had before, with various additions.

`I.` The nature and intention of the peace-offerings are here more
distinctly opened. They were offered either, 1. In thankfulness for some
special mercy received, such as recovery from sickness, preservation in
a journey, deliverance at sea, redemption out of captivity, all which
are specified in Ps. 107, and for them men are called upon to offer the
sacrifice of thanksgiving, v. 22. Or, 2. In performance of some vow
which a man made when he was in distress (v. 16), and this was less
honourable than the former, though the omission of it would have been
more culpable. Or, 3. In supplication for some special mercy which a man
was in the pursuit and expectation of, here called a voluntary offering.
This accompanied a man\'s prayers, as the former did his praises. We do
not find that men were bound by the law, unless they had bound
themselves by vow, to offer these peace-offerings upon such occasions,
as they were to bring their sacrifices of atonement in case of sin
committed. Not but that prayer and praise are as much our duty as
repentance is; but here, in the expressions of their sense of mercy, God
left them more to their liberty than in the expressions of their sense
of sin-to try the generosity of their devotion, and that their
sacrifices, being free-will offerings, might be the more laudable and
acceptable; and, by obliging them to bring the sacrifices of atonement,
God would show the necessity of the great propitiation.

`II.` The rites and ceremonies about the peace-offerings are enlarged
upon.

`1.` If the peace-offering was offered for a thanksgiving, a
meat-offering must be offered with it, cakes of several sorts, and
wafers (v. 12), and (which was peculiar to the peace-offerings) leavened
bread must be offered, not to be burnt upon the altar, that was
forbidden (ch. 2:11), but to be eaten with the flesh of the sacrifice,
that nothing might be wanting to make it a complete and pleasant feast;
for unleavened bread was less grateful to the taste, and therefore,
though enjoined in the passover for a particular reason, yet in other
festivals leavened bread, which was lighter and more pleasant, was
appointed, that men might feast at God\'s table as well as at their own.
And some think that a meat-offering is required to be brought with every
peace-offering, as well as with that of thanksgiving, by that law (v.
29) which requires an oblation with it, that the table might be as well
furnished as the altar.

`2.` The flesh of the peace-offerings, both that which was the priest\'s
share and that which was the offerer\'s must be eaten quickly, and not
kept long, either raw, or dressed, cold. If it was a peace-offering for
thanksgiving, it must be all eaten the same day (v. 16); if a vow, or
voluntary offering, it must be eaten either the same day or the day
after, v. 16. If any was left beyond the time limited, it was to be
burnt (v. 17); and, if any person ate of what was so left their conduct
should be animadverted upon as a very high misdemeanour, v. 18. Though
they were not obliged to eat it in the holy place, as those offerings
that are called most holy, but might take it to their own tents and
feast upon it there, yet God would by this law make them to know a
difference between that and other meat, and religiously to observe it,
that whereas they might keep other meat cold in the house as long as
they thought fit, and warm it again if they pleased, and eat it three or
four days after, they might not do so with the flesh of their
peace-offerings, but it must be eaten immediately. `(1.)` Because God
would not have that holy flesh to be in danger of putrefying, or being
fly-blown, to prevent which it must be salted with fire (as the
expression is, Mk. 9:49) if it were kept; as, if it was used, it must be
salted with salt. `(2.)` Because God would not have his people to be
niggardly and sparing, and distrustful of providence, but cheerfully to
enjoy what God gives them (Eccl. 8:15), and to do good with it, and not
to be anxiously solicitous for the morrow. `(3.)` The flesh of the
peace-offerings was God\'s treat, and therefore God would have the
disposal of it; and he orders it to be used generously for the
entertainment of their friends, and charitably for the relief of the
poor, to show that he is a bountiful benefactor, giving us all things
richly to enjoy, the bread of the day in its day. If the sacrifice was
thanksgiving, they were especially obliged thus to testify their holy
joy in God\'s goodness by their holy feasting. This law is made very
strict (v. 18), that if the offerer did not take care to have all his
offering eaten by himself or his family, his friends or the poor, within
the time limited by the law, or, in the event of any part being left, to
burn it (which was the most decent way of disposing of it, the
sacrifices upon the altar being consumed by fire), then his offering
should not be accepted, nor imputed to him. Note, All the benefit of our
religious services is lost if we do not improve them, and conduct
ourselves aright afterwards. They are not acceptable to God if they have
not a due influence upon ourselves. If a man seemed generous in bringing
a peace-offering, and yet afterwards proved sneaking and paltry in the
using of it, it was as if he had never brought it; nay, it shall be an
abomination. Note, There is no mean between God\'s acceptance and his
abhorrence. If our persons and performances are sincere and upright,
they are accepted; if not, they are an abomination, Prov. 15:8. He that
eats it after the time appointed shall bear his iniquity, that is, he
shall be cut off from his people, as it is explained (ch. 19:8), where
this law is repeated. This law of eating the peace-offerings before the
third day, that they might not putrefy, is applicable tot the
resurrection of Christ after two days, that, being God\'s holy one, he
might not see corruption, Ps. 16:10. And some think that it instructs us
speedily, and without delay, to partake of Christ and his grace, feeding
and feasting thereon by faith to-day, while it is called to-day (Heb.
3:13, 14), for it will be too late shortly.

`3.` But the flesh, and those that eat it, must be pure. `(1.)` The flesh
must touch no unclean thing; if it did, it must not be eaten, but burnt,
v. 19. If, in carrying it from the altar to the place where it was
eaten, a dog touched it, or it touched a dead body or any other unclean
thing, it was then unfit to be used in a religious feast. Every thing we
honour the holy God with must be pure and carefully kept from all
pollution. It is a case adjudged (Hag. 2:12) that the holy flesh could
not by its touch communicate holiness to what was common; but by this
law it is determined that by the touch of that which was unclean it
received pollution from it, which intimates that the infection of sin is
more easily and more frequently communicated than the savour of grace.
`(2.)` It must not be eaten by any unclean person. When a person was upon
any account ceremonially unclean it was at his peril if he presumed to
eat of the flesh of the peace-offerings, v. 20, 21. Holy things are only
for holy persons; the holiness of the food being ceremonial, those were
incapacitated to partake of it who lay under any ceremonial uncleanness;
but we are hereby taught to preserve ourselves pure from all the
pollutions of sin, that we may have the benefit and comfort of Christ\'s
sacrifice, 1 Pt. 2:1, 2. Our consciences must be purged from dead works,
that we may be fit to serve the living God, Heb. 9:14. But if any dare
to partake of the table of the Lord under the pollution of sin
unrepented of, and so profane sacred things, they eat and drink judgment
to themselves, as those did that ate of the peace-offerings (v. 20) and
again (v. 21), that they pertain unto the Lord: whatever pertains to the
Lord is sacred, and must be used with great reverence and not with
unhallowed hands. \"Be you holy, for God is holy, and you pertain to
him.\"

`4.` The eating of blood and the fat of the inwards is here again
prohibited; and the prohibition is annexed as before to the law of the
peace-offerings, ch. 3:17. `(1.)` The prohibition of the fat seems to be
confined to those beasts which were used for sacrifice, the bullocks,
sheep, and goats: but of the roe-buck, the hart, and other clean beasts,
they might eat the fat; for those only of which offerings were brought
are mentioned here, v. 23-25. This was to preserve in their minds a
reverence for God\'s altar, on which the fat of the inwards was burnt.
The Jews say, \"If a man eat so much as an olive of forbidden fat-if he
do it presumptuously, he is in danger of being cut off by the hand of
God-if ignorantly, he is to bring a sin-offering, and so to pay dearly
for his carelessness.\" To eat of the flesh of that which died of
itself, or was torn of beasts, was unlawful; but to eat of the fat of
such was doubly unlawful, v. 24. `(2.)` The prohibition of blood is more
general (v. 26, 27), because the fat was offered to God only by way of
acknowledgment, but the blood made atonement for the soul, and so
typified Christ\'s sacrifice much more than the burning of the fat did;
to this therefore a greater reverence must be paid, till these types had
their accomplishment in the offering up of the body of Christ once for
all. The Jews rightly expound this law as forbidding only the blood of
the life, as they express it, not that which we call the gravy, for of
that they supposed it was lawful to eat.

`5.` The priest\'s share of the peace-offerings is here prescribed. Out
of every beast that was offered for a peace-offering the priest that
offered it was to have to himself the breast and the right shoulder, v.
30-34. Observe here, `(1.)` That when the sacrifice was killed the offerer
himself must, with his own hands, present God\'s part of it, that he
might signify thereby his cheerfully giving it up to God, and his desire
that it might be accepted. He was with his own hands to lift it up, in
token of his regard to God as the God of heaven, and then to wave it to
and fro, in token of his regard to God as the Lord of the whole earth,
to whom thus, as far as he could reach, he offered it, showing his
readiness and wish to do him honour. Now that which was thus heaved and
waved was the fat, and the breast, and the right shoulder, it was all
offered to God; and then he ordered the fat to his altar, and the breast
and shoulder to his priest, both being his receivers. `(2.)` That when the
fat was burnt the priest took his part, on which he and his family were
to feast, as well as the offerer and his family. In holy joy and
thanksgiving, it is good to have our ministers to go before us, and to
be our mouth to God. The melody is sweet when he that sows and those
that reap rejoice together. Some observe a significancy in the parts
assigned to the priests: the breast and the shoulder intimate the
affections and the actions, which must be devoted to the honour of God
by all his people and to the service also of the church by all his
priests. Christ, our great peace-offering, feasts all his spiritual
priests with the breast and shoulder, with the dearest love and the
sweetest and strongest supports; for his is the wisdom of God and the
power of God. When Saul was designed for a king Samuel ordered the
shoulder of the peace-offering to be set before him (1 Sa. 9:24), which
gave him a hint of something great and sacred intended for him. Jesus
Christ is our great peace-offering; for he made himself a sacrifice, not
only to atone for sin, and so to save us from the curse, but to purchase
a blessing for us, and all good. By our joyfully partaking of the
benefits of redemption we feast upon the sacrifice, to signify which the
Lord\'s supper was instituted.

### Verses 35-38

Here is the conclusion of these laws concerning the sacrifices, though
some of them are afterwards repeated and explained. The are to be
considered, 1. As a grant to the priests, v. 35, 36. In the day they
were ordained to that work and office this provision was made for their
comfortable maintenance. Note, God will take care that those who are
employed for him be well paid and well provided for. Those that receive
the anointing of the Spirit to minister unto the Lord shall have their
portion, and it shall be a worthy portion, out of the offerings of the
Lord; for God\'s work is its own wages, and there is a present reward of
obedience in obedience. 2. As a statute for ever to the people, that
they should bring these offerings according to the rules prescribed, and
cheerfully give the priests their share out of them. God commanded the
children of Israel to offer their oblations, v. 38. Note, The solemn
acts religious worship are commanded. They are not things that we are
left to our liberty in, and which we may do or not do at our pleasure;
but we are under indispensable obligations to perform them in their
season, and it is at our peril if we omit them. The observance of the
laws of Christ cannot be less necessary than the observance of the laws
of Moses was.
