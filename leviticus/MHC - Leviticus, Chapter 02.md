Leviticus, Chapter 2
====================

Commentary
----------

In this chapter we have the law concerning the meat-offering. `I.` The
matter of it; whether of raw flour with oil and incense (v. 1), or baked
in the oven (v. 4), or upon a plate (v. 5, 6), or in a frying pan (v.
7). `II.` The management of it, of the flour (v. 2, 3), of the cakes (v.
8-10). `III.` Some particular rules concerning it, That leaven and honey
must never be admitted (v. 11, 12), and salt never omitted in the
meat-offering (v. 13). `IV.` The law concerning the offering of
firstfruits in the ear (v. 14, etc.).

### Verses 1-10

There were some meat-offerings that were only appendices to the
burnt-offerings, as that which was offered with the daily sacrifice (Ex.
29:38, 39) and with the peace-offerings; these had drink-offerings
joined with them (see Num. 15:4, 7, 9, 10), and in these the quantity
was appointed. But the law of this chapter concerns those meat-offerings
that were offered by themselves, whenever a man saw cause thus to
express his devotion. The first offering we read of in scripture was of
this kind (Gen. 4:3): Cain brought of the fruit of the ground an
offering.

`I.` This sort of offerings was appointed, 1. In condescension to the
poor, and their ability, that those who themselves lived only upon bread
and cakes might offer an acceptable offering to God out of that which
was their own coarse and homely fare, and by making for God\'s altar, as
the widow of Sarepta for his prophet, a little cake first, might procure
such a blessing upon the handful of meal in the barrel, and the oil in
the cruse, as that it should not fail. 2. As a proper acknowledgment of
the mercy of God to them in their food. This was like a quitrent, by
which they testified their dependence upon God, their thankfulness to
him, and their expectations from him as their owner and bountiful
benefactor, who giveth to all life, and breath, and food convenient.
Thus must they honour the Lord with their substance, and, in token of
their eating and drinking to his glory, must consecrate some of their
meat and drink to his immediate service. Those that now, with a grateful
charitable heart, deal out their bread to the hungry, and provide for
the necessities of those that are destitute of daily food, and when they
eat the fat and drink the sweet themselves send portions to those for
whom nothing is prepared, offer unto God an acceptable meat-offering.
The prophet laments it as one of the direful effects of famine that
thereby the meat-offering and drink-offering were cut off from the house
of the Lord (Joel 1:9), and reckoned it the greatest blessing of plenty
that it would be the revival of them, Joel 2:14.

`II.` The laws of the meat-offerings were these:-1. The ingredients must
always be fine flour and oil, two staple commodities of the land of
Canaan, Deu. 8:8. Oil was to them then in their food what butter is now
to us. If it was undressed, the oil must be poured upon the flour (v.
1); if cooked, it must be mingled with the flour, v. 4, etc. 2. If it
was flour unbaked, besides the oil it must have frankincense put upon
it, which was to be burnt with it (v. 1, 2), for the perfuming of the
altar; in allusion to this, gospel ministers are said to be a sweet
savour unto God, 2 Co. 2:15. 3. If it was prepared, this might be done
in various ways; the offerer might bake it, or fry it, or mix the flour
and oil upon a plate, for the doing of which conveniences were provided
about the tabernacle. The law was very exact even about those offerings
that were least costly, to intimate the cognizance God takes of the
religious services performed with a devout mind, even by the poor of his
people. 4. It was to be presented by the offerer to the priest, which is
called bringing it to the Lord (v. 8), for the priests were God\'s
receivers, and were ordained to offer gifts. 5. Part of it was to be
burnt upon the altar, for a memorial, that is, in token of their
mindfulness of God\'s bounty to them, in giving them all things richly
to enjoy. It was an offering made by fire, v. 2, 9. The consuming of it
by fire might remind them that they deserved to have all the fruits of
the earth thus burnt up, and that it was of the Lord\'s mercies that
they were not. They might also learn that as meats are for the belly,
and the belly for meats, so God shall destroy both it and them (1 Co.
6:13), and that man lives not by bread alone. This offering made by fire
is here said to be of a sweet savour unto the Lord; and so are our
spiritual offerings, which are made by the fire of holy love,
particularly that of almsgiving, which is said to be an odour of a sweet
smell, a sacrifice acceptable, well pleasing to God (Phil. 4:18), and
with such sacrifices God is well pleased, Heb. 13:16. 6. The remainder
of the meat-offering was to be given to the priests, v. 3, 10. It is a
thing most holy, not to be eaten by the offerers, as the peace-offerings
(which, though holy, were not most holy), but by the priests only, and
their families. Thus God provided that those who served at the altar
should live upon the altar, and live comfortably.

### Verses 11-16

Here, `I.` Leaven and honey are forbidden to be put in any of their
meat-offerings: No leaven, nor any honey, in any offering made by fire,
v. 11. 1. The leaven was forbidden in remembrance of the unleavened
bread they ate when they came out of Egypt. So much despatch was
required in the offerings they made that it was not convenient they
should stay for the leavening of them. The New Testament comparing pride
and hypocrisy to leaven because they swell like leaven, comparing also
malice and wickedness to leaven because they sour like leaven, we are to
understand and improve this as a caution to take heed of those sins
which will certainly spoil the acceptableness of our spiritual
sacrifices. Pure hands must be lifted up without wrath, and all our
gospel feasts kept with the unleavened bread of sincerity and truth. 2.
Honey was forbidden, though Canaan flowed with it, because to eat much
honey is not good (Prov. 25:16, 27); it turns to choler and bitterness
in the stomach, though luscious to the taste. Some think the chief
reason why those two things, leaven and honey, were forbidden, was
because the Gentiles used them very much in their sacrifices, and God\'s
people must not learn or use the way of the heathen, but his services
must be the reverse of their idolatrous services; see Deu. 12:30, 31.
Some make this application of this double prohibition: leaven signifies
grief and sadness of spirit (Ps. 73:21), My heart was leavened; honey
signifies sensual pleasure and mirth. In our service of God both these
must be avoided, and a mean observed between those extremes; for the
sorrow of the world worketh death, and a love to the delights of sense
is a great enemy to holy love.

`II.` Salt is required in all their offerings, v. 13. The altar was the
table of the Lord; and therefore, salt being always set on our tables,
God would have it always used at his. It is called the salt of the
covenant, because, as men confirmed their covenants with each other by
eating and drinking together, at all which collations salt was used, so
God, by accepting his people\'s gifts and feasting them upon his
sacrifices, supping with them and they with him (Rev. 3:20), did confirm
his covenant with them. Among the ancients salt was a symbol of
friendship. The salt for the sacrifice was not brought by the offerers,
but was provided at the public charge, as the wood was, Ezra 7:20-22.
And there was a chamber in the court of the temple called the chamber of
salt, in which they laid it up. Can that which is unsavoury be eaten
without salt? God would hereby intimate to them that their sacrifices in
themselves were unsavoury. The saints, who are living sacrifices to God,
must have salt in themselves, for every sacrifice must be salted with
salt (Mk. 9:49, 50), and our speech must be always with grace (Col.
4:6), so must all our religious performances be seasoned with that salt.
Christianity is the salt of the earth.

`III.` Directions are given about the first-fruits. 1. The oblation of
their first-fruits at harvest, of which we read, Deu. 26:2. These were
offered to the Lord, not to be burnt upon the altar, but to be given to
the priests as perquisites of their office, v. 12. And you shall offer
them (that is, leaven and honey) in the oblation of the first-fruits,
though they were forbidden in other meat-offerings; for they were proper
enough to be eaten by the priests, though not to be burnt upon the
altar. The loaves of the first-fruits are particularly ordered to be
baked with leaven, Lev. 23:17. And we read of the first-fruits of honey
brought to the house of God, 2 Chr. 31:5. 2. A meat-offering of their
first-fruits. The former was required by the law; this was a free-will
offering, v. 14-16. If a man, with a thankful sense of God\'s goodness
to him in giving him hopes of a plentiful crop, was disposed to bring an
offering in kind immediately out of his field, and present it to God,
owning thereby his dependence upon God and obligations to him, `(1.)` Let
him be sure to bring the first ripe and full ears, not such as were
small and half-withered. Whatever was brought for an offering to God
must be the best in its kind, though it were but green ears of corn. We
mock God, and deceive ourselves, if we think to put him off with a
corrupt thing while we have in our flock a male, Mal. 1:14. `(2.)` These
green ears must be dried by the fire, that the corn, such as it was,
might be beaten out of them. That is not expected from green ears which
one may justly look for from those that have been left to grow fully
ripe. If those that are young do God\'s work as well as they can, they
shall be accepted, though they cannot do it so well as those that are
aged and experienced. God makes the best of green ears of corn, and so
must we. `(3.)` Oil and frankincense must be put upon it. Thus (as some
allude to this) wisdom and humility must soften and sweeten the spirits
and services of young people, and then their green ears of corn shall be
acceptable. God takes a particular delight in the first ripe fruits of
the Spirit and the expressions of early piety and devotion. Those that
can but think and speak as children, yet, if they think and speak well,
God will be well pleased with their buds and blossoms, and will never
forget the kindness of their youth. `(4.)` It must be used as other
meat-offerings, v. 16, compare v. 9. He shall offer all the
frankincense; it is an offering made by fire. The fire and the
frankincense seem to have had a special significancy. `[1.]` The fire
denotes the fervency of spirit which ought to be in all our religious
services. In every good thing we must be zealously affected. Holy love
to God is the fire by which all our offerings must be made; else they
are not of a sweet savour to God. `[2.]` The frankincense denotes the
mediation and intercession of Christ, by which all our services are
perfumed and recommended to God\'s gracious acceptance. Blessed be God
that we have the substance of which all these observances were but
shadows, the fruit that was hid under these leaves.
