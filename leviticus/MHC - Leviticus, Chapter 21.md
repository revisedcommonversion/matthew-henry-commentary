Leviticus, Chapter 21
=====================

Commentary
----------

This chapter might borrow its title from Mal. 2:1, \"And now, O you
priests, this commandment is for you.\" It is a law obliging priests
with the utmost care and jealousy to preserve the dignity of their
priesthood. `I.` The inferior priests are here charged both concerning
their mourning and concerning their marriages and their children (v.
1-9). `II.` The high priest is restrained more than any of them (v.
10-15). `III.` Neither the one nor the other must have any blemish (v. 16,
etc.).

### Verses 1-9

It was before appointed that the priests should teach the people the
statutes God had given concerning the difference between clean and
unclean, ch. 10:10, 11. Now here it is provided that they should
themselves observe what they were to teach the people. Note, Those whose
office it is to instruct must do it by example as well as precept, 1
Tim. 4:12. The priests were to draw nearer to God than any of the
people, and to be more intimately conversant with sacred things, and
therefore it was required of them that they should keep at a greater
distance than others from every thing that was defiling and might
diminish the honour of their priesthood.

`I.` They must take care not to disparage themselves in their mourning for
the dead. All that mourned for the dead were supposed to come near the
body, if not to touch it: and the Jews say, \"It made a man ceremonially
unclean to come within six feet of a dead corpse;\" nay, it is declared
(Num. 19:14) that all who come into the tent where the dead body lies
shall be unclean seven days. Therefore all the mourners that attended
the funeral could not but defile themselves, so as not to be fit to come
into the sanctuary for seven days: for this reason it is ordered, 1.
That the priests should never put themselves under this incapacity of
coming into the sanctuary, unless it were for one of their nearest
relations, v. 1-3. A priest was permitted to do it for a parent or a
child, for a brother or an unmarried sister, and therefore, no doubt
(though this is not mentioned) for the wife of his bosom; for Ezekiel, a
priest, would have mourned for his wife if he had not been particularly
prohibited, Eze. 24:17. By this allowance God put an honour upon natural
affection, and favoured it so far as to dispense with the attendance of
his servants for seven days, while they indulged themselves in their
sorrow for the death of their dear relations; but, beyond this period,
weeping must not hinder sowing, nor their affection to their relations
take them off from the service of the sanctuary. Nor was it at all
allowed for the death of any other, no, not of a chief man among the
people, as some read it, v. 4. They must not defile themselves, no, nor
for the high priest himself, unless thus akin to them. Though there is a
friend that is nearer than a brother, yet the priests must not pay this
respect to the best friend they had, except he were a relation, lest, if
it were allowed for one, others should expect it, and so they should be
frequently taken off from their work: and it is hereby intimated that
there is a particular affection to be reserved for those that are thus
near akin to us; and, when any such are removed by death, we ought to be
affected with it, and lay it to heart, as the near approach of death to
ourselves, and an alarm to us to prepare to follow. 2. That they must
not be extravagant in the expressions of their mourning, no, not for
their dearest relations, v. 5. Their mourning must not be either, `(1.)`
Superstitious, according to the manner of the heathen, who cut off their
hair, and let out their blood, in honour of the imaginary deities which
presided (as they thought) in the congregation of the dead, that they
might engage them to be propitious to their departed friends. Even the
superstitious rites used of old at funerals are an indication of the
ancient belief of the immortality of the soul, and its existence in a
separate state: and though the rites themselves were forbidden by the
divine law, because they were performed to false gods, yet the decent
respect which nature teaches and which the law allows to be paid to the
remains of our deceased friends, shows that we are not to look upon them
as lost. Nor, `(2.)` Must it be passionate or immoderate. Note, God\'s
ministers must be examples to others of patience under affliction,
particularly that which touches in a very tender part, the death of
their near relations. They are supposed to know more than others of the
reasons why we must not sorrow as those that have no hope (1 Th. 4:13),
and therefore they ought to be eminently calm and composed, that they
may be able to comfort others with the same comforts wherewith they are
themselves comforted of God. The people were forbidden to mourn for the
dead with superstitious rites (ch. 19:27, 28), and what was unlawful to
them was much more unlawful to the priest. The reason given for their
peculiar care not to defile themselves we have (v. 6): Because they
offered the bread of their God, even the offerings of the Lord made by
fire, which were the provisions of God\'s house and table. They are
highly honoured, and therefore must not stain their honour by making
themselves slaves to their passions; they are continually employed in
sacred service, and therefore must not be either diverted from or
disfitted for the services they were called to. If they pollute
themselves, they profane the name of their God on whom they attend: if
the servants are rude and of ill behaviour, it is a reflection upon the
master, as if he kept a loose and disorderly house. Note, All that
either offer or eat the bread of our God must be holy in all manner of
conversation, or else they profane that name which they pretend to
sanctify.

`II.` They must take care not to degrade themselves in their marriage, v.
7. A priest must not marry a woman of ill fame, that either had been
guilty or was suspected to have been guilty of uncleanness. He must not
only not marry a harlot, though ever so great a penitent for her former
whoredoms, but he must not marry one that was profane, that is, of a
light carriage or indecent behaviour. Nay, he must not marry one that
was divorced, because there was reason to think it was for some fault
she was divorced. The priests were forbidden to undervalue themselves by
such marriages as these, which were allowed to others, 1. Lest it should
bring a present reproach upon their ministry, harden the profane in
their profaneness, and grieve the hearts of serious people: the New
Testament gives laws to ministers\' wives (1 Tim. 3:11), that they be
grave and sober, that the ministry be not blamed. 2. Lest it should
entail a reproach upon their families; for the work and honour of the
priesthood were to descend as an inheritance to their children after
them. Those do not consult the good of their posterity as they ought who
do not take care to marry such as are of good report and character. He
that would seek a godly seed (as the expression is, Mal. 2:15) must
first seek a godly wife, and take heed of a corruption of blood. It is
added here (v. 8), Thou shalt sanctify him, and he shall be holy unto
thee. \"Not only thou, O Moses, by taking care that these laws be
observed, but thou, O Israel, by all endeavours possible to keep up the
reputation of the priesthood, which the priests themselves must do
nothing to expose or forfeit. He is holy to his God (v. 7), therefore he
shall be holy unto thee.\" Note, We must honour those whom our God puts
honour upon. Gospel ministers by this rule are to be esteemed very
highly in love for their works\' sake (1 Th. 5:13), and every Christian
must look upon himself as concerned to be the guardian of their honour.

`III.` Their children must be afraid of doing any thing to disparage them
(v. 9): If the daughter of any priest play the whore, her crime is
great; she not only polluteth but profaneth herself: other women have
not that honour to lose that she has, who, as one of a priest\'s family,
has eaten of the holy things, and is supposed to have been better
educated than others. Nay, she profaneth her father; he is reflected
upon, and every body will be ready to ask, \"Why did not he teach her
better?\" And the sinners in Zion will insult and say, \"Here is your
priest\'s daughter.\" Her punishment there must be peculiar: She shall
be burnt with fire, for a terror to all priests\' daughters. Note, The
children of ministers ought, of all others, to take heed of doing any
thing that is scandalous, because in them it is doubly scandalous, and
will be punished accordingly by him whose name is Jealous.

### Verses 10-15

More was expected from a priest than from other people, but more from
the high priest than from other priests, because upon his head the
anointing oil was poured, and he was consecrated to put on the garments
(v. 10), both which were typical of the anointing and adorning of the
Lord Jesus, with all the gifts and graces of the Holy Spirit, which he
received without measure. It is called the crown of the anointing oil of
his God (v. 12); for the anointing of the Spirit is, to all that have
it, a crown of glory, and a diadem of beauty. The high priest being thus
dignified,

`I.` He must not defile himself at all for the dead, no, nor for his
nearest relations, his father or his mother, much less his child or
brother, v. 11. 1. He must not use the common expressions of sorrow on
those occasions, such as uncovering his head, and rending his clothes
(v. 10), so perfectly unconcerned must he show himself in all the
crosses and comforts of this life: even his natural affection must be
swallowed up in compassion to the ignorant, and a feeling of their
infirmities, and a tender concern for the household of God, which he was
made the ruler of. Thus being the holy one that was entrusted with the
thummim and the urim he must not know father or mother, Deu. 33:8, 9. 2.
He must not go in to any dead body, v. 11. If any of the inferior
priests were under a ceremonial pollution, there were other priests that
might supply their places; but, if the high priest were defiled, there
would be a greater want of him. And the forbidding of him to go to any
house of mourning, or attend any funeral, would be an indication to the
people of the greatness of that dignity to which he was advanced. Our
Lord Jesus, the great high priest of our profession, touched the dead
body of Jairus\'s daughter, the bier of the widow\'s son, and the grave
of Lazarus, to show that he came to altar the property of death, and to
take off the terror of it, by breaking the power of it. Now that it
cannot destroy it does not defile. 3. He must not go out of the
sanctuary (v. 12); that is, whenever he was attending or officiating in
the sanctuary, where usually he tarried in his own apartment all day, he
must not go out upon any occasion whatsoever, nor cut short his
attendance on the living God, no, not to pay his last respects to a
dying relation. It was a profanation of the sanctuary to leave it, while
his presence was requisite there, upon any such occasion; for thereby he
preferred some other business before the service of God and the business
of his profession, to which he ought to make every thing else give
place. Thus our Lord Jesus would not leave off preaching to speak with
his mother and brethren, Mt. 12:48.

`II.` He might not marry a widow (as other priests might), much less one
divorced, or a harlot, v. 13, 14. The reason of this was to put a
difference between him and other priests in this matter; and (as some
suggest) that he might be a type of Christ, to whom the church was to be
presented a chaste virgin, 2 Co. 11:2. See Eze. 44:22. Christ must have
our first love, our pure love, our entire love; thus the virgins love
thee (Cant. 1:3), and such only are fit to follow the Lamb, Rev. 14:4.

`III.` He might not profane his seed among his people, v. 15. Some
understand it as forbidding him to marry any of an inferior rank, which
would be a disparagement to his family. Jehoiada indeed married of his
own tribe, but then it was into the royal family, 2 Chr. 22:11. This was
not to teach him to be proud, but to teach him to be pure, and to do
nothing unbecoming his office and the worthy name by which he was
called. Or it may be a caution to him in disposing of his children; he
must not profane his seed by marrying them unsuitably. Ministers\'
children are profaned if they be unequally yoked with unbelievers.

### Verses 16-24

The priesthood being confined to one particular family, and entailed
upon all the male issue of that family throughout their generations, it
was very likely that some or other in after-ages that were born to the
priesthood would have natural blemishes and deformities: the honour of
the priesthood would not secure them from any of those calamities which
are common to men. Divers blemishes are here specified; some that were
ordinarily for life, as blindness; others that might be for a time, as a
scurf or scab, and, when they were gone, the disability ceased. Now,

`I.` The law concerning priests that had blemishes was, 1. That they might
live upon the altar (v. 22): He shall eat of the sacrifices with the
other priests, even the most holy things, such as the show-bread and the
sin-offerings, as well as the holy things, such as the tithes and
first-fruits, and the priests\' share of the peace-offerings. The
blemishes were such as they could not help, and therefore, though they
might not work, they must not starve. Note, None must be abused for
their natural infirmities. Even the deformed child in the family must
have its child\'s part. 2. Yet they must not serve at the altar, at
either of the altars, nor be admitted to attend or assist the other
priests in offering sacrifice or burning incense, v. 17, 21, 23. Great
men choose to have such servants about them as are sightly, and it was
fit that the great God should have such in his house then, when he was
pleased to manifest his glory in external indications of it. But it was
especially requisite that comely men should be chosen to minister about
holy things, for the sake of the people, who were apt to judge according
to outward appearance, and to think meanly of the service, how
honourable soever it was made by the divine institution, of those that
performed it looked despicably or went about it awkwardly. This
provision God made for the preserving of the reputation of his altar,
that it might not at any time fall under contempt. It was for the credit
of the sanctuary that none should appear there who were any way
disfigured, either by nature or accident.

`II.` Under the gospel, 1. Those that labour under any such blemishes as
these have reason to thank God that they are not thereby excluded from
offering spiritual sacrifices to God; nor, if otherwise qualified for
it, from the office of the ministry. There is many a healthful beautiful
soul lodged in a crazy deformed body. Yet, 2. We ought to infer hence
how incapable those are to serve God acceptably whose minds are
blemished and deformed by any reigning vice. Those are unworthy to be
called Christians, and unfit to be employed as minsters, that are
spiritually blind, and lame, and crooked, whose sins render them
scandalous and deformed, so as that the offerings of the Lord are
abhorred for their sakes. The deformities of Hophni and Phinehas were
worse than any of the blemishes here mentioned. Let such therefore as
are openly vicious be put out of the priesthood as polluted persons; and
let all that are made to our God spiritual priests be before him holy
and without blemish, and comfort themselves with this, that, though in
this imperfect state they have spots that are the spots of God\'s
children, yet they shall shortly appear before the throne of God without
spot, or wrinkle, or any such thing.
