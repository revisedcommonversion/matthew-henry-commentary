Leviticus, Chapter 19
=====================

Commentary
----------

Some ceremonial precepts there are in this chapter, but most of them are
moral. One would wonder that when some of the lighter matters of the law
are greatly enlarged upon (witness two long chapters concerning the
leprosy) many of the weightier matters are put into a little compass:
divers of the single verses of this chapter contain whole laws
concerning judgment and mercy; for these are things which are manifest
in every man\'s conscience; men\'s own thoughts are able to explain
these, and to comment upon them. `I.` The laws of this chapter, which were
peculiar to the Jews, are, 1. Concerning their peace-offerings (v. 5-8).
2. Concerning the gleanings of their fields (v. 9, 10). 3. Against
mixtures of their cattle, seed, and cloth (v. 19). 4. Concerning their
trees (v. 23-25). 5. Against some superstitious usages (v. 26-28). But,
`II.` Most of these precepts are binding on us, for they are expositions
of most of the ten commandments. 1. Here is the preface to the ten
commandments, \"I am the Lord,\" repeated fifteen times. 2. A sum of the
ten commandments. All the first table in this, \"Be you holy,\" (v. 2).
All the second table in this, \"Thou shalt love thy neighbour\" (v. 18),
and an answer to the question, \"Who is my neighbour?\" (v. 33, 34). 3.
Something of each commandment. `(1.)` The first commandment implied in
that which is often repeated here, \"I am your God.\" And here is a
prohibition of enchantment (v. 26) and witchcraft (v. 31), which make a
god of the devil. `(2.)` Idolatry, against the second commandment, is
forbidden, (v. 4). `(3.)` Profanation of God\'s name, against the third
(v. 12). `(4.)` Sabbath-sanctification is pressed (v. 3, 30). `(5.)`
Children are required to honour their parents (v. 3), and the aged (v.
32). `(6.)` Hatred and revenge are here forbidden, against the sixth
commandment (v. 17, 18). `(7.)` Adultery (v. 20-22), and whoredom (v. 29).
`(8.)` Justice is here required in judgment (v. 15), theft forbidden (v.
11), fraud and withholding dues (v. 13), and false weights (v. 35, 36).
`(9.)` Lying (v. 11). Slandering (v. 14). Tale-bearing, and false-witness
bearing (v. 16). `(10.)` The tenth commandment laying a restraint upon the
heart, so does that (v. 17), \"Thou shalt not hate thy brother in thy
heart.\" And here is a solemn charge to observe all these statutes (v.
37). Now these are things which need not much help for the understanding
of them, but require constant care and watchfulness for the observing of
them. \"A good understanding have all those that do these
commandments.\"

### Verses 1-10

Moses is ordered to deliver the summary of the laws to all the
congregation of the children of Israel (v. 2); not to Aaron and his sons
only, but to all the people, for they were all concerned to know their
duty. Even in the darker ages of the law, that religion could not be of
God which boasted of ignorance as its mother. Moses must make known
God\'s statutes to all the congregation, and proclaim them through the
camp. These laws, it is probable, he delivered himself to as many of the
people as could be within hearing at once, and so by degrees at several
times to them all. Many of the precepts here given they had received
before, but it was requisite that they should be repeated, that they
might be remembered. Precept must be upon precept, and line upon line,
and all little enough. In these verses,

`I.` It is required that Israel be a holy people, because the God of
Israel is a holy God, v. 2. Their being distinguished from all other
people by peculiar laws and customs was intended to teach them a real
separation from the world and the flesh, and an entire devotedness to
God. And this is now the law of Christ (the Lord bring every thought
within us into obedience to it!) You shall be holy, for I am holy, 1 Pt.
1:15, 16. We are the followers of the holy Jesus, and therefore must be,
according to our capacity, consecrated to God\'s honour, and conformed
to his nature and will. Israel was sanctified by the types and shadows
(ch. 20:8), but we are sanctified by the truth, or substance of all
those shadows, Jn. 17:17; Tit. 2:14.

`II.` That children be obedient to their parents: \"You shall fear every
man his mother and his father, v. 3. 1. The fear here required is the
same with the honour commanded by the fifth commandment; see Mal. 1:6.
It includes inward reverence and esteem, outward expressions of respect,
obedience to the lawful commands of parents, care and endeavour to
please them and make them easy, and to avoid every thing that may offend
and grieve them, and incur their displeasure. The Jewish doctors ask,
\"What is this fear that is owing to a father?\" And they answer, \"It
is not to stand in his way nor to sit in his place, not to contradict
what he says nor to carp at it, not to call him by his name, either
living or dead, but \'My Father,\' or \'Sir;\' it is to provide for him
if he be poor, and the like.\" 2. Children, when they grow up to be men,
must not think themselves discharged from this duty: every man, though
he be a wise man, and a great man, yet must reverence his parents,
because they are his parents. 3. The mother is put first, which is not
usual, to show that the duty is equally owing to both; if the mother
survive the father, still she must be reverenced and obeyed. 4. It is
added, and keep my sabbaths. If God provides by his law for the
preserving of the honour of parents, parents must use their authority
over their children for the preserving of the honour of God,
particularly the honour of his sabbaths, the custody of which is very
much committed to parents by the fourth commandment, Thou, and thy son,
and thy daughter. The ruin of young people has often been observed to
begin in the contempt of their parents and the profanation of the
sabbath day. Fitly therefore are these two precepts here put together in
the beginning of this abridgment of the statutes: \"You shall fear,
every man, his mother and his father, and keep my sabbaths. Those are
hopeful children, and likely to do well, that make conscience of
honouring their parents and keeping holy the sabbath day. 5. The reason
added to both these precepts is, \"I am the Lord your God; the Lord of
the sabbath and the God of your parents.\"

`III.` That God only be worshipped, and not by images (v. 4): \"Turn you
not to idols, to Elilim, to vanities, things of no power, no value, gods
that are no gods. Turn not from the true God to false ones, from the
mighty God to impotent ones, from the God that will make you holy and
happy to those that will deceive you, debauch you, ruin you, and make
you for ever miserable. Turn not your eye to them, much less your heart.
Make not to yourselves gods, the creatures of your own fancy, nor think
to worship the Creator by molten gods. You are the work of God\'s hands,
be not so absurd as to worship gods the work of your own hands.\" Molten
gods are specified for the sake of the molten calf.

`IV.` That the sacrifices of their peace-offerings should always be
offered, and eaten, according to the law, v. 5-8. There was some
particular reason, it is likely, for the repetition of this law rather
than any other relating to the sacrifices. The eating of the
peace-offerings was the people\'s part, and was done from under the eye
of the priests, and perhaps some of them had kept the cold meat of their
peace-offerings, as they had done the manna (Ex. 16:20), longer than was
appointed, which occasioned this caution; see the law itself before, ch.
7:16-18. God will have his own work done in his own time. Though the
sacrifice was offered according to the law, if it was not eaten
according to the law, it was not accepted. Though ministers do their
part, what the better if people do not theirs? There is work to be done
after our spiritual sacrifices, in a due improvement of them; and, if
this be neglected, all is in vain.

`V.` That they should leave the gleanings of their harvest and vintage for
the poor, v. 9, 10. Note, Works of piety must be always attended with
works of charity, according as our ability is. When they gathered in
their corn, they must leave some standing in the corner of the field;
the Jewish doctors say, \"It should be a sixtieth part of the field;\"
and they must also leave the gleanings and the small clusters of their
grapes, which at first were overlooked. This law, though not binding now
in the letter of it, yet teaches us, 1. That we must not be covetous and
griping, and greedy of every thing we can lay any claim to; nor insist
upon our right in things small and trivial. 2. That we must be well
pleased to see the poor supplied and refreshed with the fruit of our
labours. We must not think every thing lost that goes beside ourselves,
nor any thing wasted that goes to the poor. 3. That times of joy, such
as harvest-time is, are proper times for charity; that, when we rejoice,
the poor may rejoice with us, and when our hearts are blessing God their
loins may bless us.

### Verses 11-18

We are taught here,

`I.` To be honest and true in all our dealings, v. 11. God, who has
appointed every man\'s property by his providence, forbids by his law
the invading of that appointment, either by downright theft, You shall
not steal, or by fraudulent dealing, \"You shall not cheat, or deal
falsely.\" Whatever we have in the world, we must see to it that it be
honestly come by, for we cannot be truly rich, nor long rich, with that
which is not. The God of truth, who requires truth in the heart (Ps.
51:6), requires it also in the tongue: Neither lie one to another,
either in bargaining or common converse. This is one of the laws of
Christianity (Col. 3:9): Lie not one to another. Those that do not speak
truth do not deserve to be told truth; those that sin by lying justly
suffer by it; therefore we are forbidden to lie one to another; for, if
we lie to others, we teach them to lie to us.

`II.` To maintain a very reverent regard to the sacred name of God (v.
12), and not to call him to be witness either, 1. To a lie: You shall
not swear falsely. It is bad to tell a lie, but it is much worse to
swear it. Or, 2. To a trifle, and every impertinence: Neither shalt thou
profane the name of thy God, by alienating it to any other purpose than
that for which it is to be religiously used.

`III.` Neither to take nor keep any one\'s right from him, v. 13. We must
not take that which is none of our own, either by fraud or robbery; nor
detain that which belongs to another, particularly the wages of the
hireling, let it not abide with thee all night. Let the day-labourer
have his wages as soon as he has done his day\'s work, if he desire it.
It is a great sin to deny the payment of it, nay, to defer it, to his
damage, a sin that cries to heaven for vengeance, Jam. 5:4.

`IV.` To be particularly tender of the credit and safety of those that
cannot help themselves, v. 14. 1. The credit of the deaf: Thou shalt not
curse the deaf; that is, not only those that are naturally deaf, that
cannot hear at all, but also those that are absent, and at present out
of hearing of the curse, and so cannot show their resentment, return the
affront, nor right themselves, and those that are patient, that seem as
if they heard not, and are not willing to take notice of it, as David,
Ps. 38:13. Do not injure any because they are unwilling, or unable, to
avenge themselves, for God sees and hears, though they do not. 2. The
safety of the blind we must likewise be tender of, and not put a
stumbling-block before them; for this is to add affliction to the
afflicted, and to make God\'s providence a servant to our malice. This
prohibition implies a precept to help the blind, and remove
stumbling-blocks out of their way. The Jewish writers, thinking it
impossible that any should be so barbarous as to put a stumbling-block
in the way of the blind, understood it figuratively, that it forbids
giving bad counsel to those that are simple and easily imposed upon, by
which they may be led to do something to their own prejudice. We ought
to take heed of doing any thing which may occasion our weak brother to
fall, Rom. 14:13; 1 Co. 8:9. It is added, as a preservative from these
sins, but fear thou God. \"Thou dost not fear the deaf and blind, they
cannot right themselves; but remember it is the glory of God to help the
helpless, and he will plead their cause.\" Note, The fear of God will
restrain us from doing that which will not expose us to men\'s
resentments.

`V.` Judges and all in authority are here commanded to give verdict and
judgment without partiality (v. 15); whether they were constituted
judges by commission or made so in a particular case by the consent of
both parties, as referees or arbitrators, they must do no wrong to
either side, but, to the utmost of their skill, must go according to the
rules of equity, having respect purely to the merits of the cause, and
not to the characters of the person. Justice must never be perverted,
either, 1. In pity to the poor: Thou shalt not respect the person of the
poor, Ex. 23:3. Whatever may be given to a poor man as an alms, yet let
nothing be awarded him as his right but what he is legally entitled to,
nor let his poverty excuse him from any just punishment for a fault. Or,
2. In veneration or fear of the mighty, in whose favour judges would be
most frequently biased. The Jews say, \"Judges were obliged by this law
to be so impartial as not to let one of the contending parties sit while
the other stood, nor permit one to say what he pleased and bid the other
be short; see James 2:1-4.

`VI.` We are all forbidden to do any thing injurious to our neighbour\'s
good name (v. 16), either, 1. In common conversation: Thou shalt not go
up and down as a tale-bearer. It is as bad an office as a man can put
himself into to be the publisher of every man\'s faults, divulging what
was secret, aggravating crimes, and making the worst of every thing that
was amiss, with design to blast and ruin men\'s reputation, and to sow
discord among neighbours. The word used for a tale-bearer signifies a
pedlar, or petty chapman, the interlopers of trade; for tale-bearers
pick up ill-natured stories at one house and utter them at another, and
commonly barter slanders by way of exchange. See this sin condemned,
Prov. 11:13; 20:19; Jer. 9:4, 5; Eze. 22:9. Or, 2, In witness-bearing:
Neither shalt thou stand as a witness against the blood of thy
neighbour, if his blood be innocent, nor join in confederacy with such
bloody men as those described,\" Prov. 1:11, 12. The Jewish doctors put
this further sense upon it: \"Thou shalt not stand by and see thy
brother in danger, but thou shalt come in to his relief and succour,
though it be with the peril of thy own life or limb;\" they add, \"He
that can by his testimony clear one that is accused is obliged by this
law to do it;\" see Prov. 24:11, 12.

`VII.` We are commanded to rebuke our neighbour in love (v. 17): Thou
shalt in any wise rebuke thy neighbour. 1. Rather rebuke him than hate
him for an injury done to thyself. If we apprehend that our neighbour
has any way wronged us, we must not conceive a secret grudge against
him, and estrange ourselves from him, speaking to him neither bad nor
good, as the manner of some is, who have the art of concealing their
displeasure till they have an opportunity of a full revenge (2 Sa.
13:22); but we must rather give vent to our resentments with the
meekness of wisdom, endeavour to convince our brother of the injury,
reason the case fairly with him, and so put an end to the disgust
conceived: this is the rule our Saviour gives in this case, Lu. 17:3. 2.
Therefore rebuke him for his sin against God, because thou lovest him;
endeavour to bring him to repentance, that his sin may be pardoned, and
he may turn from it, and it may not be suffered to lie upon him. Note,
Friendly reproof is a duty we owe to one another, and we ought both to
give it and take it in love. Let the righteous smite me, and it shall be
a kindness, Ps. 141:5. Faithful and useful are those wounds of a friend,
Prov. 27:5, 6. It is here strictly commanded, \"Thou shalt in any wise
do it, and not omit it under any pretence.\" Consider, `(1.)` The guilt we
incur by not reproving: it is construed here into a hating of our
brother. We are ready to argue thus, \"Such a one is a friend I love,
therefore I will not make him uneasy by telling him of his faults;\" but
we should rather say, \"therefore I will do him the kindness to tell him
of them.\" Love covers sin from others, but not from the sinner himself.
`(2.)` The mischief we do by not reproving: we suffer sin upon him. Must
we help the ass of an enemy that has fallen under his burden, and shall
we not help the soul of a friend? Ex. 23:5. And by suffering sin upon
him we are in danger of bearing sin for him, as the margin reads it. If
we reprove not the unfruitful works of darkness, we have fellowship with
them, and become accessaries ex post facto-after the fact, Eph. 5:11. It
is thy brother, thy neighbour, that is concerned; and he was a Cain that
said, Am I my brother\'s keeper?

`VIII.` We are here required to put off all malice, and to put on
brotherly love, v. 18. 1. We must be ill-affected to none: Thou shalt
not avenge, nor bear any grudge; to the same purport with that v. 17,
Thou shalt not hate thy brother in thy heart; for malice is murder
begun. If our brother has done us an injury, we must not return it upon
him, that is avenging; we must not upon every occasion upbraid him with
it, that is bearing a grudge; but we must both forgive it and forget it,
for thus we are forgiven of God. It is a most ill-natured thing, and the
bane of friendship, to retain the resentment of affronts and injuries,
and to let that word devour for ever. 2. We must be well-affected to
all: Thou shalt love thy neighbour as thyself. We often wrong ourselves,
but we soon forgive ourselves those wrongs, and they do not at all
lessen our love to ourselves; and in like manner we should love our
neighbour. Our Saviour has made this the second great commandment of the
law (Mt. 22:39), and the apostle shows how it is the summary of all the
laws of the second table, Rom. 13:9, 10; Gal. 5:14. We must love our
neighbour as truly as we love ourselves, and without dissimulation; we
must evidence our love to our neighbour in the same way as that by which
we evidence our love to ourselves, preventing his hurt, and procuring
his good, to the utmost of our power. We must do to our neighbour as we
would be done to ourselves (Mt. 7:12), putting our souls into his
soul\'s stead, Job 16:4, 5. Nay, we must in many cases deny ourselves
for the good of our neighbour, as Paul, 1 Co. 9:19, etc. Herein the
gospel goes beyond even that excellent precept of the law; for Christ,
by laying down his life for us, has taught us even to lay down our lives
for the brethren, in some cases (1 Jn. 3:16), and so to love our
neighbour better than ourselves.

### Verses 19-29

Here is, `I.` A law against mixtures, v. 19. God in the beginning made the
cattle after their kind (Gen. 1:25), and we must acquiesce in the order
of nature God hath established, believing that is best and sufficient,
and not covet monsters. Add thou not unto his works, lest he reprove
thee; for it is the excellency of the work of God that nothing can,
without making it worse, be either put to it or taken from it, Eccl.
3:14. As what God has joined we must not separate, so what he has
separated we must not join. The sowing of mingled corn and the wearing
of linsey-woolsey garments are forbidden, either as superstitious
customs of the heathen or to intimate how careful they should be not to
mingle themselves with the heathen nor to weave any of the usages of the
Gentiles into God\'s ordinances. Ainsworth suggests that it was to lead
Israel to the simplicity and sincerity of religion, and to all the parts
and doctrines of the law and gospel in their distinct kinds. As faith is
necessary, good works are necessary, but to mingle these together in the
cause of our justification before God is forbidden, Gal. 2:16.

`II.` A law for punishing adultery committed with one that was a bondmaid
that was espoused, v. 20-22. If she had not been espoused, the law
appointed no punishment at all; being espoused, if she had not been a
bondmaid, the punishment had been no less than death: but, being as yet
a bondmaid (though before the completing of her espousals she must have
been made free), the capital punishment is remitted, and they shall both
be scourged; or, as some think, the woman only, and the man was to bring
a sacrifice. It was for the honour of marriage, though but begun by
betrothing, that the crime should be punished; but it was for the honour
of freedom that it should not be punished as the debauching of a free
woman was, so great was the difference then made between bond and free
(Gal. 4:30); but the gospel of Christ knows no such distinction, Col.
3:11.

`III.` A law concerning fruit-trees, that for the first three years after
they were planted, if they should happen to be so forward as to bear in
that time, yet no use should be made of the fruit, v. 23-25. It was
therefore the practice of the Jews to pluck off the fruit, as soon as
they perceived it knit, from their young trees, as gardeners do
sometimes, because their early bearing hinders their growing. If any did
come to perfection, it was not to be used in the service either of God
or man; but what they bore the fourth year was to be holy to the Lord,
either given to the priests, or eaten before the Lord with joy, as their
second tithe was, and thenceforward it was all their own. Now, 1. Some
think this taught them not to follow the custom of the heathen, who,
they say, consecrated the very first products of their fruit-trees to
their idols, saying that otherwise all the fruits would be blasted. 2.
This law in the case of fruit-trees seems to be parallel with that in
the case of animals, that no creature should be accepted as an offering
till it was past eight days old, nor till that day were children to be
circumcised; see ch. 22:27. God would have the first-fruits of their
trees, but, because for the first three years they were as
inconsiderable as a lamb or a calf under eight days old, therefore God
would not have them, for it is fit he should have every thing at its
best; and yet he would not allow them to be used, because his
first-fruits were not as yet offered: they must therefore be accounted
as uncircumcised, that is, as an animal under eight days\' old, not fit
for any use. 3. We are hereby taught not to be over-hasty in catching at
any comfort, but to be willing with patience to wait the time for the
enjoyment of it, and particularly to acknowledge ourselves unworthy of
the increase of the earth, our right to the fruits of which was
forfeited by our first parents eating forbidden fruit, and we are
restored to it only by the word of God and prayer, 1 Tim. 4:5.

`IV.` A law against the superstitious usages of the heathen, v. 26-28. 1.
Eating upon the blood, as the Gentiles did, who gathered the blood of
their sacrifices into a vessel for their demons (as they fancied) to
drink, and then sat about it, eating the flesh themselves, signifying
their communion with devils by their feasting with them. Let not this
custom be used, for the blood of God\'s sacrifices was to be sprinkled
on the altar, and then poured at the foot of it, and conveyed away. 2.
Enchantment and divination, and a superstitious observation of the
times, some days and hours lucky and others unlucky. Curious arts of
this kind, it is likely, had been of late invented by the Egyptian
priests, to amuse the people, and support their own credit. The
Israelites had seen them practised, but must by no means imitate them.
It would be unpardonable in those to whom were committed the oracles of
God to ask counsel of the devil, and yet worse in Christians, to whom
the Son of God is manifested, who has destroyed the works of the devil.
For Christians to have their nativities cast, and their fortunes told
them, to use spells and charms for the cure of diseases and the driving
away of evil spirits, to be affected with the falling of the salt, a
hare crossing the way, cross days, or the like, is an intolerable
affront to the Lord Jesus, a support of paganism and idolatry, and a
reproach both to themselves and to that worthy name by which they are
called: and those must be grossly ignorant, both of the law and the
gospel, that ask, \"What harm is there in these things?\" Is it no harm
for those that have fellowship with Christ to have fellowship with
devils, or to learn the ways of those that have? Surely we have not so
learned Christ. 3. There was a superstition even in trimming themselves
used by the heathen, which must not be imitated by the people of God:
You shall not round the corners of your heads. Those that worshipped the
hosts of heaven, in honour of them, cut their hair so as that their
heads might resemble the celestial globe; but, as the custom was foolish
itself, so, being done with respect to their false gods, it was
idolatrous. 4. The rites and ceremonies by which they expressed their
sorrow at their funerals must not be imitated, v. 28. They must not make
cuts or prints in their flesh for the dead; for the heathen did so to
pacify the infernal deities they dreamt of, and to render them
propitious to their deceased friends. Christ by his sufferings has
altered the property of death, and made it a true friend to every true
Israelite; and now, as there needs nothing to make death propitious to
us (for, if God be so, death is so of course), so we sorrow not as those
that have no hope. Those whom the God of Israel had set apart for
himself must not receive the image and superscription of these dunghill
deities. Lastly, The prostituting of their daughters to uncleanness,
which is here forbidden (v. 29), seems to have been practised by the
heathen in their idolatrous worships, for with such abominations those
unclean spirits which they worshipped were well pleased. And when
lewdness obtained as a religious rite, and was committed in their
temples, no marvel that the land became full of that wickedness, which,
when it entered at the temple-doors, overspread the land like a mighty
torrent, and bore down all the fences of virtue and modesty. The devil
himself could not have brought such abominations into their lives if he
had not first brought them into their worships. And justly were those
given up to vile affections who forsook the holy God, and gave divine
honours to impure spirits. Those that dishonour God are thus suffered to
dishonour themselves and their families.

### Verses 30-37

Here is, `I.` A law for the preserving of the honour of the time and place
appropriated to the service of God, v. 30. This would be a means to
secure them both from the idolatries and superstitions of the heathen
and from all immoralities in conversation. 1. Sabbaths must be
religiously observed, and not those times mentioned (v. 26) to which the
heathen had a superstitious regard. 2. The sanctuary must be reverenced:
great care must be taken to approach the tabernacle with that purity and
preparation which the law required, and to attend there with that
humility, decency, and closeness of application which became them in the
immediate presence of such an awful majesty. Though now there is no
place holy by divine institution, as the tabernacle and temple then
were, yet this law obliges us to respect the solemn assemblies of
Christians for religious worship, as being held under a promise of
Christ\'s special presence in them, and to carry ourselves with a due
decorum while in those assemblies we attend the administration of holy
ordinances, Eccl. 5:1.

`II.` A caution against all communion with witches, and those that were
in league with familiar spirits: \"Regard them not, seek not after them,
be not in fear of any evil from them nor in hopes of any good from them.
Regard not their threatenings, or promises, or predictions; seek not to
them for discovery or advice, for, if you do, you are defiled by it, and
rendered abominable both to God and your own consciences.\" This was the
sin that completed Saul\'s wickedness, for which he was rejected of God,
1 Chr. 10:13.

`III.` A charge to young people to show respect to the aged: Thou shall
rise up before the hoary head, v. 32. Age is honourable, and he that is
the Ancient of days requires that honour be paid to it. The hoary head
is a crown of glory. Those whom God has honoured with the common
blessing of long life we ought to honour with the distinguishing
expressions of civility; and those who in age are wise and good are
worthy of double honour: more respect is owing to such old men than
merely to rise up before them; their credit and comfort must be
carefully consulted, their experience and observations improved, and
their counsels asked and hearkened to, Job 32:6, 7. Some, by the old man
whose face or presence is to be honoured, understand the elder in
office, as by the hoary head the elder in age; both ought to be
respected as fathers, and in the fear of God, who has put some of his
honour upon both. Note, Religion teaches good manners, and obliges us to
give honour to those to whom honour is due. It is an instance of great
degeneracy and disorder in a land when the child behaves himself proudly
against the ancient, and the base against the honourable, Isa. 3:5; Job
30:1, 12. It becomes the aged to receive this honour, and the younger to
give it; for it is the ornament as well as duty of their youth to order
themselves lowly and reverently to all their betters.

`IV.` A charge to the Israelites to be very tender of strangers, v. 33,
34. Both the law of God and his providence had vastly dignified Israel
above any other people, yet they must not therefore think themselves
authorized to trample upon all mankind but those of their own nation,
and to insult them at their pleasure; no, \"Thou shall not vex a
stranger, but love him as thyself, and as one of thy own people.\" It is
supposed that this stranger was not an idolater, but a worshipper of the
God of Israel, though not circumcised, a proselyte of the gate at least,
though not a proselyte of righteousness: if such a one sojourned among
them, they must not vex him, nor oppress, nor over-reach him in a
bargain, taking advantage of his ignorance of their laws and customs;
they must reckon it as great a sin to cheat a stranger as to cheat an
Israelite; \"nay\" (say the Jewish doctors) \"they must not so much as
upbraid him with his being a stranger, and his having been formerly an
idolater.\" Strangers are God\'s particular care, as the widow and the
fatherless are, because it is his honour to help the helpless, Ps.
146:9. It is therefore at our peril if we do them any wrong, or put any
hardships upon them. Strangers shall be welcome to God\'s grace, and
therefore we should do what we can to invite them to it, and to
recommend religion to their good opinion. It argues a generous
disposition, and a pious regard to God, as a common Father, to be kind
to strangers; for those of different countries, customs, and languages,
are all made of one blood. But here is a reason added peculiar to the
Jews: \"For you were strangers in the land of Egypt. God then favoured
you, therefore do you now favour the strangers, and do to them as you
then wished to be done to. You were strangers, and yet are now thus
highly advanced; therefore you know not what these strangers may come
to, whom you are apt to despise.\"

`V.` Justice in weights and measures is here commanded. That there should
be no cheat in them, v. 35. That they should be very exact, v. 36. In
weighing and measuring, we pretend a design to give all those their own
whom we deal with; but, if the weights and measures be false, it is like
a corruption in judgment, it cheats under colour of justice; and thus to
deceive a man to his damage is worse than picking his pocket or robbing
him on the highway. He that sells is bound to give the full of the
commodity, and he that buys the full of the price agreed upon, which
cannot be done without just balances, weights, and measures. Let no man
go beyond or defraud his brother, for, though it be hidden from man, it
will be found that God is the avenger of all such.

`VI.` The chapter concludes with a general command (v. 37): You shall
observe all my statutes, and do them. Note, 1. We are not likely to do
God\'s statutes, unless we observe them with great care and
consideration. 2. Yet it is not enough barely to observe God\'s
precepts, but we must make conscience of obeying them. What will it
avail us to be critical in our notions, if we be not conscientious in
our conversations? 3. An upright heart has respect to all God\'s
commandments, Ps. 119:6. Though in many instances the hand fails in
doing what should be done, yet the eye observes all God\'s statutes. We
are not allowed to pick and choose our duty, but must aim at standing
complete in all the will of God.
