Leviticus, Chapter 18
=====================

Commentary
----------

Here is, `I.` A general law against all conformity to the corrupt usages
of the heathen (v. 1-5). `II.` Particular laws, 1. Against incest (v.
6-18). 2. Against beastly lusts, and barbarous idolatries (v. 19-23).
III. The enforcement of these laws from the ruin of the Canaanites (v.
24-30).

### Verses 1-5

After divers ceremonial institutions, God here returns to the
enforcement of moral precepts. The former are still of use to us as
types, the latter still binding as laws. We have here, 1. The sacred
authority by which these laws are enacted: I am the Lord your God (v. 1,
4, 30), and I am the Lord, v. 5, 6, 21. \"The Lord, who has a right to
rule all; your God, who has a peculiar right to rule you.\" Jehovah is
the fountain of being, and therefore the fountain of power, whose we
are, whom we are bound to serve, and who is able to punish all
disobedience. \"Your God to whom you have consented, in whom you are
happy, to whom you lie under the highest obligations imaginable, and to
whom you are accountable.\" 2. A strict caution to take heed of
retaining the relics of the idolatries of Egypt, where they had dwelt,
and of receiving the infection of the idolatries of Canaan, whither they
were now going, v. 3. Now that God was by Moses teaching them his
ordinances there was aliquid dediscendum-something to be unlearned,
which they had sucked in with their milk in Egypt, a country noted for
idolatry: You shall not do after the doings of the land of Egypt. It
would be the greatest absurdity in itself to retain such an affection
for their house of bondage as to be governed in their devotions by the
usages of it, and the greatest ingratitude to God, who had so
wonderfully and graciously delivered them. Nay, as if governed by a
spirit of contradiction, they would be in danger, even after they had
received these ordinances of God, of admitting the wicked usages of the
Canaanites and of inheriting their vices with their land. Of this danger
they are here warned, You shall not walk in their ordinances. Such a
tyrant is custom that their practices are called ordinances, and they
became rivals even with God\'s ordinances, and God\'s professing people
were in danger of receiving law from them. 3. A solemn charge to them to
keep God\'s judgments, statutes, and ordinances, v. 4, 5. To this
charge, and many similar ones, David seems to refer in the many prayers
and professions he makes relating to God\'s laws in the 119th Psalm.
Observe here, `(1.)` The great rule of our obedience-God\'s statutes and
judgments. These we must keep to walk therein. We must keep them in our
books, and keep them in our hands, that we may practise them in our
hearts and lives. Remember God\'s commandments to do them, Ps. 103:18.
We must keep in them as our way to travel in, keep to them as our rule
to work by, keep them as our treasure, as the apple of our eye, with the
utmost care and value. `(2.)` The great advantage of our obedience: Which
if a man do, he shall live in them, that is, \"he shall be happy here
and hereafter.\" We have reason to thank God, `[1.]` That this is still
in force as a promise, with a very favourable construction of the
condition. If we keep God\'s commandments in sincerity, though we come
short of sinless perfection, we shall find that the way of duty is the
way of comfort, and will be the way to happiness. Godliness has the
promise of life, 1 Tim. 4:8. Wisdom has said, Keep my commandments and
live: and if through the Spirit we mortify the deeds of the body (which
are to us as the usages of Egypt were to Israel) we shall live. `[2.]`
That it is not so in force in the nature of a covenant as that the least
transgression shall for ever exclude us from this life. The apostle
quotes this twice as opposite to the faith which the gospel reveals. It
is the description of the righteousness which is by the law, the man
that doeth them shall live ev autois-in them (Rom. 10:5), and is urged
to prove that the law is not of faith, Gal. 3:12. The alteration which
the gospel has made is in the last word: still the man that does them
shall live, but not live in them; for the law could not give life,
because we could not perfectly keep it; it was weak through the flesh,
not in itself; but now the man that does them shall live by the faith of
the Son of God. He shall owe his life to the grace of Christ, and not to
the merit of his own works; see Gal. 3:21, 22. The just shall live, but
they shall live by faith, by virtue of their union with Christ, who is
their life.

### Verses 6-18

These laws relate to the seventh commandment, and, no doubt, are
obligatory on us under the gospel, for they are consonant to the very
light and law of nature: one of the articles, that of a man\'s having
his father\'s wife, the apostle speaks of as a sin not so much as named
among the Gentiles, 1 Co. 5:1. Though some of the incests here forbidden
were practised by some particular persons among the heathen, yet they
were disallowed and detested, unless among those nations who had become
barbarous, and were quite given up to vile affections. Observe,

`I.` That which is forbidden as to the relations here specified is
approaching to them to uncover their nakedness, v. 6.

`1.` It is chiefly intended to forbid the marrying of any of these
relations. Marriage is a divine institution; this and the sabbath, the
eldest of all, of equal standing with man upon the earth: it is intended
for the comfort of human life, and the decent and honourable propagation
of the human race, such as became the dignity of man\'s nature above
that of the beasts. It is honourable in all, and these laws are for the
support of the honour of it. It was requisite that a divine ordinance
should be subject to divine rules and restraints, especially because it
concerns a thing wherein the corrupt nature of man is as apt as in any
thing to be wilful and impetuous in its desires, and impatient of check.
Yet these prohibitions, besides their being enacted by an incontestable
authority, are in themselves highly reasonable and equitable. `(1.)` By
marriage two were to become one flesh, therefore those that before were
in a sense one flesh by nature could not, without the greatest
absurdity, become one flesh by institution; for the institution was
designed to unite those who before were not united. `(2.)` Marriage puts
an equality between husband and wife. \"Is she not thy companion taken
out of thy side?\" Therefore, if those who before were superior and
inferior should intermarry (which is the case in most of the instances
here laid down), the order of nature would be taken away by a positive
institution, which must by no means be allowed. The inequality between
master and servant, noble and ignoble, is founded in consent and custom,
and there is no harm done if that be taken away by the equality of
marriage; but the inequality between parents and children, uncles and
nieces, aunts and nephews, either by blood or marriage, is founded in
nature, and is therefore perpetual, and cannot without confusion be
taken away by the equality of marriage, the institution of which, though
ancient, is subsequent to the order of nature. `(3.)` No relations that
are equals are forbidden, except brothers and sisters, by the whole
blood or half blood, or by marriage; and in this there is not the same
natural absurdity as in the former, for Adam\'s sons must of necessity
have married their own sisters; but it was requisite that it should be
made by a positive law unlawful and detestable, for the preventing of
sinful familiarities between those that in the days of their youth are
supposed to live in a house together, and yet cannot intermarry without
defeating one of the intentions of marriage, which is the enlargement of
friendship and interest. If every man married his own sister (as they
would be apt to do from generation to generation if it were lawful),
each family would be a world to itself, and it would be forgotten that
we are members one of another. It is certain that this has always been
looked upon by the more sober heathen as a most infamous and abominable
thing; and those who had not this law yet were herein a law to
themselves. The making use of the ordinance of marriage for the
patronizing of incestuous mixtures is so far from justifying them, or
extenuating their guilt, that it adds the guilt of profaning an
ordinance of God, and prostituting that to the vilest of purposes which
was instituted for the noblest ends. But,

`2.` Uncleanness, committed with any of these relations out of marriage,
is likewise, without doubt, forbidden here, and no less intended than
the former: as also all lascivious carriage, wanton dalliance, and every
thing that has the appearance of this evil. Relations must love one
another, and are to have free and familiar converse with each other, but
it must be with all purity; and the less it is suspected of evil by
others the more care ought the persons themselves to take that Satan do
not get advantage against them, for he is a very subtle enemy, and seeks
all occasions against us.

`II.` The relations forbidden are most of them plainly described; and it
is generally laid down as a rule that what relations of a man\'s own he
is bound up from marrying the same relations of his wife he is likewise
forbidden to marry, for they two are one. That law which forbids
marrying a brother\'s wife (v. 16) had an exception peculiar to the
Jewish state, that, if a man died without issue, his brother or next of
kin should marry the widow, and raise up seed to the deceased (Deu.
25:5), for reasons which held good only in that commonwealth; and
therefore now that those reasons have ceased the exception ceases, and
the law is in force, that a man must in no case marry his brother\'s
widow. That article (v. 18) which forbids a man to take a wife to her
sister supposes a connivance at polygamy, as some other laws then did
(Ex. 21:10; Deu. 21:15), but forbids a man\'s marrying two sisters, as
Jacob did, because between those who had before been equal there would
be apt to arise greater jealousies and animosities than between wives
that were not so nearly related. If the sister of the wife be taken for
the concubine, or secondary wife, nothing can be more vexing in her
life, or as long as she lives.

### Verses 19-30

Here is, `I.` A law to preserve the honour of the marriage-bed, that it
should not be unseasonably used (v. 19), nor invaded by an adulterer, v.
20.

`II.` A law against that which was the most unnatural idolatry, causing
their children to pass through the fire to Moloch, v. 21. Moloch (as
some think) was the idol in and by which they worshipped the sun, that
great fire of the world; and therefore in the worship of it they made
their own children either sacrifices to this idol, burning them to death
before it, or devotees to it, causing them to pass between two fires, as
some think, or to be thrown through one, to the honour of this pretended
deity, imagining that the consecrating of but one of their children in
this manner to Moloch would procure good fortune for all the rest of
their children. Did idolaters thus give their own children to false
gods, and shall we think any thing too dear to be dedicated to, or to be
parted with for, the true God? See how this sin of Israel (which they
were afterwards guilty of, notwithstanding this law) is aggravated by
the relation which they and their children stood in to God. Eze. 16:20,
Thou hast taken thy sons and thy daughters, whom thou hast borne unto
me, and these thou hast sacrificed. Therefore it is here called
profaning the name of their God; for it looked as if they thought they
were under greater obligations to Moloch than to Jehovah; for to him
they offered their cattle only, but to Moloch their children.

`III.` A law against unnatural lusts, sodomy and bestiality, sins not to
be named nor thought of without the utmost abhorrence imaginable, v. 22,
23. Other sins level men with the beasts, but these sink them much
lower. That ever there should have been occasion for the making of these
laws, and that since they are published they should ever have been
broken, is the perpetual reproach and scandal of human nature; and the
giving of men up to these vile affections was frequently the punishment
of their idolatries; so the apostle shows, Rom. 1:24.

`IV.` Arguments against these and the like abominable wickednesses. He
that has an indisputable right to command us, yet because he will deal
with us as men, and draw with the cords of a man, condescends to reason
with us. 1. Sinners defile themselves with these abominations: Defile
not yourselves in any of these things, v. 24. All sin is defiling to the
conscience, but these are sins that have a peculiar turpitude in them.
Our heavenly Father, in kindness to us, requires of us that we keep
ourselves clean, and do not wallow in the dirt. 2. The souls that commit
them shall be cut off, v. 29. And justly; for, if any man defile the
temple of God, him shall God destroy, 1 Co. 3:17. Fleshly lusts war
against the soul, and will certainly be the ruin of it if God\'s mercy
and grace prevent not. 3. The land is defiled, v. 25. If such
wickednesses as these be practised and connived at, the land is thereby
made unfit to have God\'s tabernacle in it, and the pure and holy God
will withdraw the tokens of his gracious presence from it. It is also
rendered unwholesome to the inhabitants, who are hereby infected with
sin and exposed to plagues and it is really nauseous and loathsome to
all good men in it, as the wickedness of Sodom was to the soul of
righteous Lot. 4. These have been the abominations of the former
inhabitants, v, 24, 27. Therefore it was necessary that these laws
should be made, as antidotes and preservatives from the plague are
necessary when we go into an infected place. And therefore they should
not practise any such things, because the nations that had practised
them now lay under the curse of God, and were shortly to fall by the
sword of Israel. They could not but be sensible how odious those people
had made themselves who wallowed in this mire, and how they stank in the
nostrils of all good men; and shall a people sanctified and dignified as
Israel was make themselves thus vile? When we observe how ill sin looks
in others we should use this as an argument with ourselves with the
utmost care and caution to preserve our purity. 5. For these and the
like sins the Canaanites were to be destroyed; these filled the measure
of the Amorites\' iniquity (Gen. 15:16), and brought down that
destruction of so many populous kingdoms which the Israelites were now
shortly to be not only the spectators, but the instruments of: Therefore
I do visit the iniquity thereof upon it, v. 25. Note, The tremendous
judgments of God, executed on those that are daringly profane and
atheistical, are intended as warnings to those who profess religion to
take heed of every thing that has the least appearance of, or tendency
towards, profaneness or atheism. Even the ruin of the Canaanites is an
admonition to the Israelites not to do like them. Nay, to show that not
only the Creator is provoked, but the creation burdened, by such
abominations as these, it is added (v. 25), The land itself vomiteth out
her inhabitants. The very ground they went upon did, as it were, groan
under them, and was sick of them, and not easy till it had discharged
itself of these enemies of the Lord, Isa. 1:24. This bespeaks the
extreme loathsomeness of sin; sinful man indeed drinks in iniquity like
water, but the harmless part of the creation even heaves at it, and
rises against it. Many a house and many a town have spued out the wicked
inhabitants, as it were, with abhorrence, Rev. 3:16. Therefore take
heed, saith God, that the land spue not you out also, v. 28. It was
secured to them, and entailed upon them, and yet they must expect that,
if they made the vices of the Canaanites their own, with their land
their fate would be the same. Note, Wicked Israelites are as abominable
to God as wicked Canaanites, and more so, and will be as soon spued out,
or sooner. Such a warning as was here given to the Israelites is given
by the apostle to the Gentile converts, with reference to the rejected
Jews, in whose room they were substituted (Rom. 11:19, etc.); they must
take heed of falling after the same example of unbelief, Heb. 4:11.
Apply it more generally; and let it deter us effectually from all sinful
courses to consider how many they have been the ruin of. Lay the ear of
faith to the gates of the bottomless pit, and hear the doleful shrieks
and outcries of damned sinners, whom earth has spued out and hell has
swallowed, that find themselves undone, for ever undone, by sin; and
tremble lest this be your portion at last. God\'s threatenings and
judgments should frighten us from sin.

`V.` The chapter concludes with a sovereign antidote against this
infection: Therefore you shall keep my ordinance that you commit not any
one of these abominable customs, v. 30. This is the remedy prescribed.
Note, 1. Sinful customs are abominable customs, and their being common
and fashionable does not make them at all the less abominable nor should
we the less abominate them, but the more; because the more customary
they are the more dangerous they are. 2. It is of pernicious consequence
to admit and allow of any one sinful custom, because one will make way
for many, Uno absurdo dato, mille sequuntur-Admit but a single
absurdity, you invite a thousand. The way of sin is downhill. 3. A close
and constant adherence to God\'s ordinances is the most effectual
preservative from the infection of gross sin. The more we taste of the
sweetness and feel of the power of holy ordinances the less inclination
we shall have to the forbidden pleasures of sinners\' abominable
customs. It is the grace of God only that will secure us, and that grace
is to be expected only in the use of the means of grace. Nor does God
ever leave any to their own hearts\' lusts till they have first left him
and his institutions.
