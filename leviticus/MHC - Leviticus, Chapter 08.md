Leviticus, Chapter 8
====================

Commentary
----------

This chapter gives us an account of the solemn consecration of Aaron and
his sons to the priest\'s office. `I.` It was done publicly, and the
congregation was called together to be witnesses of it (v. 1-4). `II.` It
was done exactly according to God\'s appointment (v. 5). 1. They were
washed and dressed (v. 6-9, 13). 2. The tabernacle and the utensils of
it were anointed, and then the priests (v. 10-12). 3. A sin-offering was
offered for them (v. 14-17). 4. A burnt-offering (v. 18-21). 5. The ram
of consecration (v. 22-30). 6. The continuance of this solemnity for
seven days (v. 31, etc.).

### Verses 1-13

God had given Moses orders to consecrate Aaron and his sons to the
priests\' office, when he was with him the first time upon mount Sinai,
Ex. 28 and 29, where we have also the particular instructions he had how
to do it. Now here we have,

`I.` The orders repeated. What was there commanded to be done is here
commanded to be done now, v. 2, 3. The tabernacle was newly set up,
which, without the priests, would be as a candlestick without a candle;
the law concerning sacrifices was newly given, but could not be observed
without priests; for, though Aaron and his sons had been nominated to
the office, they could not officiate, till they were consecrated, which
yet must not be done till the place of their ministration was prepared,
and the ordinances were instituted, that they might apply themselves to
work as soon as ever they were consecrated, and might know that they
were ordained, not only to the honour and profit, but to the business of
the priesthood. Aaron and his sons were near relations to Moses, and
therefore he would not consecrate them till he had further orders, lest
he should seem too forward to bring honour into his family.

`II.` The congregation called together, at the door, that is, in the
court of the tabernacle, v. 4. The elders and principal men of the
congregation, who represented the body of the people, were summoned to
attend; for the court would hold but a few of the many thousands of
Israel. It was done thus publicly, 1. Because it was a solemn
transaction between God and Israel; the priests were to be ordained for
men in things pertaining to God, for the maintaining of a settled
correspondence, and the negotiating of all affairs between the people
and God; and therefore it was fit that both sides should appear, to own
the appointment, at the door of the tabernacle of meeting. 2. The
spectators of the solemnity could not but be possessed, by the sight of
it, with a great veneration for the priests and their office, which was
necessary among a people so wretchedly prone as these were to envy and
discontent. It was strange that any of those who were witnesses of what
was here done should afterwards say, as some of them did, You take too
much upon you, you sons of Levi; but what would they have said if it had
been done clandestinely? Note, It is very fit, and of good use, that
ministers should be ordained publicly, plebe praesente-in the presence
of the common people, according to the usage of the primitive church.

`III.` The commission read, v. 5. Moses, who was God\'s representative in
this solemnity, produced his orders before the congregation: This is the
thing which the Lord commanded to be done. Though God had crowned him
king in Jeshurun, when he made his face to shine in the sight of all
Israel, yet he did not institute or appoint any thing in God\'s worship
but what God himself had commanded. The priesthood he delivered to them
was that which he had received from the Lord. Note, All that minister
about holy things must have an eye to God\'s command as their rule and
warrant; for it is only in the observance of this that they can expect
to be owned and accepted of God. Thus we must be able to say, in all
acts of religious worship, This is the thing which the Lord commanded to
be done.

`IV.` The ceremony performed according to the divine ritual. 1. Aaron and
his sons were washed with water (v. 6), to signify that they ought now
to purify themselves from all sinful dispositions and inclinations, and
ever after to keep themselves pure. Christ washes those from their sins
in his own blood whom he makes to our God kings and priests (Rev. 1:5,
6); and those that draw near to God must be washed in pure water, Heb.
10:22. Though they were ever so clean before and no filth was to be seen
upon them, yet they must be washed, to signify their purification from
sin, with which their souls were polluted, how clean soever their bodies
were. 2. They were clothed with the holy garments, Aaron with his (v.
7-9), which typified the dignity of Christ our great high priest, and
his sons with theirs (v. 13), which typified the decency of Christians,
who are spiritual priests. Christ wears the breast-plate of judgment and
the holy crown; for the church\'s high priest is her prophet and king.
All believers are clothed with the robe of righteousness, and girt with
the girdle of truth, resolution, and close application; and their heads
are bound, as the word here is, with the bonnet or diadem of beauty, the
beauty of holiness. 3. The high priest was anointed, and, it should
seem, the holy things were anointed at the same time; some think that
they were anointed before, but that the anointing of them is mentioned
here because Aaron was anointed with the same oil with which they were
anointed; but the manner of relating it here makes it more than probable
that it was done at the same time, and that the seven days employed in
consecrating the altar were coincident with the seven days of the
priests\' consecration. The tabernacle, and all its utensils, had some
of the anointing oil put upon them with Moses\'s finger (v. 10), so had
the altar (v. 11); these were to sanctify the gold and the gift (Mt.
23:17-19), and therefore must themselves be thus sanctified; but he
poured it out more plentifully upon the head of Aaron (v. 12), so that
it ran down to the skirts of his garments, because his unction was to
typify the anointing of Christ with the Spirit, which was not given by
measure to him. Yet all believers also have received the anointing,
which puts an indelible character upon them, 1 Jn. 2:27.

### Verses 14-30

The covenant of priesthood must be made by sacrifice, as well as other
covenants, Ps. 50:5. And thus Christ was consecrated by the sacrifice of
himself, once for all. Sacrifices of each kind must be offered for the
priests, that they might with the more tenderness and concern offer the
gifts and sacrifices of the people, with compassion on the ignorant, and
on those that were out of the way, not insulting over those for whom
sacrifices were offered, remembering that they themselves had had
sacrifices offered for them, being compassed with infirmity. 1. A
bullock, the largest sacrifice, was offered for a sin-offering (v. 14),
that hereby atonement might be made, and they might not bring any of the
guilt of the sins of their former state into the new character they were
now to put on. When Isaiah was sent to be a prophet, he was told to his
comfort, Thy iniquity is taken away, Isa. 6:7. Ministers, that are to
declare the remission of sins to others, should give diligence to get it
made sure to themselves in the first place that their own sins are
pardoned. Those to whom is committed the ministry of reconciliation must
first be reconciled to God themselves, that they may deal for the souls
of others as for their own. 2. A ram was offered for a burnt-offering,
v. 18-21. By this they gave to God the glory of this great honour which
was now put upon them, and returned him praise for it, as Paul thanked
Christ Jesus for putting him into the ministry, 1 Tim. 1:12. They also
signified the devoting of themselves and all their services to the
honour of God. 3. Another ram, called the ram of consecration, was
offered for a peace-offering, v. 22, etc. The blood of it was part put
on the priests, on their ears, thumbs, and toes, and part sprinkled upon
the altar; and thus he did (as it were) marry them to the altar, upon
which they must all their days give attendance. All the ceremonies about
this offering, as those before, were appointed by the express command of
God; and, if we compare this chapter with Ex. 29, we shall find that the
performance of the solemnity exactly agrees with the precept there, and
in nothing varies. Here, therefore, as in the account we had of the
tabernacle and its vessels, it is again and again repeated, As the Lord
commanded Moses. And thus Christ, when he sanctified himself with his
own blood, had an eye to his Father\'s will in it. As the Father gave me
commandment so I do, Jn. 14:31; 10:18; 6:38.

### Verses 31-36

Moses, having done his part of the ceremony, now leaves Aaron and his
sons to do theirs.

`I.` They must boil the flesh of their peace-offering, and eat it in the
court of the tabernacle, and what remained they must burn with fire, v.
31, 32. This signified their thankful consent to the consecration: when
God gave Ezekiel his commission, he told him to eat the roll, Eze. 3:1,
2.

`II.` They must not stir out of the court of the tabernacle for seven
days, v. 33. The priesthood being a good warfare, they must thus learn
to endure hardness, and to disentangle themselves from the affairs of
this life, 2 Tim. 2:3, 4. Being consecrated to their service, they must
give themselves wholly to it, and attend continually to this very thing.
Thus Christ\'s apostles were appointed to wait for the promise of the
Father, Acts 1:4. During this time appointed for their consecration,
they were daily to repeat the same sacrifices which were offered the
first day, v. 34. This shows the imperfection of the legal sacrifices,
which, because they could not take away sin, were often repeated (Heb.
10:1, 2), but were here repeated seven times (a number of perfection),
because they typified that one offering, which perfected for ever those
that were sanctified. The work lasted seven days; for it was a kind of
creation: and this time was appointed in honour of the sabbath, which,
probably, was the last day of the seven, for which they were to prepare
during the six days. Thus the time of our life, like the six days, must
be our preparation for the perfection of our consecration to God in the
everlasting sabbath: they attended day and night (v. 35), and so
constant should we be in our meditation on God\'s law, Ps. 1:2. They
attended to keep the charge of the Lord: we have every one of us a
charge to keep, an eternal God to glorify, an immortal soul to provide
for, needful duty to be done, our generation to serve; and it must be
our daily care to keep this charge, for it is the charge of the Lord our
Master, who will shortly call us to an account about it, and it is at
our utmost peril if we neglect it. Keep it that you die not; it is
death, eternal death, to betray the trust we are charged with; by the
consideration of this we must be kept in awe. Lastly, We are told (v.
36) that Aaron and his sons did all that was commanded. Thus their
consecration was completed; and thus they set an example before the
people of an exact obedience to the laws of sacrifices now newly given,
and then they could with the better grace teach them. Thus the covenant
of peace (Num. 25:12), of life and peace (Mal. 2:5), was made with Aaron
and his sons; but after all the ceremonies that were used in their
consecration there was one point of ratification which was reserved to
be the honour and establishment of Christ\'s priesthood, which was this,
that they were made priests without an oath, but Christ with an oath
(Hab. 7:21), for neither such priests nor their priesthood could
continue, but Christ\'s is a perpetual and unchangeable priesthood.

Gospel ministers are compared to those who served at the altar, for they
minister about holy things (1 Co. 9:13), they are God\'s mouth to the
people and the people\'s to God, the pastors and teachers Christ has
appointed to continue in the church to the end of the world: they seem
to be meant in that promise which points at gospel times (Isa. 66:21), I
will take of them for priests and for Levites. No man may take this
honour to himself, but he who upon trial is found to be clothed and
anointed by the Spirit of God with gifts and graces to qualify him for
it, and who with purpose of heart devotes himself entirely to the
service, and is then by the word and prayer (for so every thing is
sanctified), and the imposition of the hands of those that give
themselves to the word and prayer, set apart to the office, and
recommended to Christ as a servant and to the church as a steward and
guide. And those that are thus solemnly dedicated to God ought not to
depart from his service, but faithfully to abide in it all their days;
and those that do so, and continue labouring in the word and doctrine,
are to be accounted worthy of double honour, double to that of the
Old-Testament priests.
