Leviticus, Chapter 20
=====================

Commentary
----------

The laws which before were made are in this chapter repeated and
penalties annexed to them, that those who would not be deterred from sin
by the fear of God might be deterred from it by the fear of punishment.
If we will not avoid such and such practices because the law has made
them sin (and it is most acceptable when we go on that principle of
religion), surely we shall avoid them when the law has made them death,
from a principle of self-preservation. In this chapter we have, `I.` Many
particular crimes that are made capital. `I.` Giving their children to
Moloch (v. 1-5). 2. Consulting witches (v. 6, 27). 3. Cursing parents
(v. 9). 4. Adultery (v. 10). 5. Incest (v. 11, 12, 14, 17, 19-21). 6.
Unnatural lusts (v. 13, 15, 16, 18). `II.` General commands given to be
holy (v. 7, 8, 22-26).

### Verses 1-9

Moses is here directed to say that again to the children of Israel which
he had in effect said before, v. 2. We are sure it was no vain
repetition, but very necessary, that they might give the more earnest
heed to the things that were spoken, and might believe them to be of
great consequence, being so often inculcated. God speaketh once, yea,
twice, and what he orders to be said again we must be willing to hear
again, because for us it is safe, Phil. 3:1.

`I.` Three sins are in these verses threatened with death:-

`1.` Parents abusing their children, by sacrificing them to Moloch, v. 2,
3. There is the grossest absurdity that can be in all the rites of
idolatry, and they are all a great reproach to men\'s reason; but none
trampled upon all the honours of human nature as this did, the burning
of children in the fire to the honour of a dunghill-god. It was a plain
evidence that their gods were devils, who desired and delighted in the
misery and ruin of mankind, and that the worshippers were worse than the
beasts that perish, perfectly stripped, not only of reason, but of
natural affection. Abraham\'s offering Isaac could not give countenance,
much less could it give rise to this barbarous practice, since, though
that was commanded, it was immediately countermanded. Yet such was the
power of the god of this world over the children of disobedience that
this monstrous piece of inhumanity was generally practised; and even the
Israelites were in danger of being drawn into it, which made it
necessary that this severe law should be made against it. It was not
enough to tell them they might spare their children (the fruit of their
body should never be accepted for the sin of their soul), but they must
be told, `(1.)` That the criminal himself should be put to death as a
murderer: The people of the land shall stone him with stones (v. 2),
which was looked upon as the worst of capital punishments among the
Jews. If the children were sacrificed to the malice of the devil, the
parents must be sacrificed to the justice of God. And, if either the
fact could not be proved or the magistrates did not do their duty, God
would take the work into his own hands: I will cut him off, v. 3. Note,
Those that escape punishment from men, yet shall not escape the
righteous judgments of God; so wretchedly do those deceive themselves
that promise themselves impunity in sin. How can those escape against
whom God sets his face, that is, whom he frowns upon, meets as an enemy,
and fights against? The heinousness of the crime is here set forth to
justify the doom: it defiles the sanctuary, and profanes the holy name
of God, for the honour of both which he is jealous. Observe, The
malignity of the sin is laid upon that in it which was peculiar to
Israel. When the Gentiles sacrificed their children they were guilty of
murder and idolatry; but, if the Israelites did it, they incurred the
additional guilt of defiling the sanctuary (which they attended upon
even when they lay under this guilt, as if there might be an agreement
between the temple of God and idols), and of profaning the holy name of
God, by which they were called, as if he allowed his worshippers to do
such things, Rom. 2:23, 24. `(2.)` That all his aiders and abetters should
be cut off likewise by the righteous hand of God. If his neighbours
concealed him, and would not come in as witnesses against him,-if the
magistrates connived at him, and would not pass sentence upon him,
rather pitying his folly than hating his impiety,-God himself would
reckon with them, v. 4, 5. Misprision of idolatry is a crime cognizable
in the court of heaven, and which shall not go unpunished: I will set my
face against that man (that magistrate, Jer. 5:1) and against his
family. Note, `[1.]` The wickedness of the master of a family often
brings ruin upon a family; and he that should be the house-keeper proves
the house-breaker. `[2.]` If magistrates will not do justice upon
offenders, God will do justice upon them, because there is danger that
many will go a whoring after those who do but countenance sin by winking
at it. And, if the sins of leaders be leading sins, it is fit that their
punishments should be exemplary punishments.

`2.` Children\'s abusing their parents, by cursing them, v. 9. If
children should speak ill of their parents, or wish ill to them, or
carry it scornfully or spitefully towards them, it was an iniquity to be
punished by the judges, who were employed as conservators both of God\'s
honour and of the public peace, which were both attacked by this
unnatural insolence. See Prov. 30:17, The eye that mocks at his father
the ravens of the valley shall pick out, which intimates that such
wicked children were in a fair way to be not only hanged, but hanged in
chains. This law of Moses Christ quotes and confirms (Mt. 15:4), for it
is as direct a breach of the fifth commandment as wilful murder is of
the sixth. The same law which requires parents to be tender of their
children requires children to be respectful to their parents. He that
despitefully uses his parents, the instruments of his being, flies in
the face of God himself, the author of his being, who will not see the
paternal dignity and authority insulted and trampled upon.

`3.` Persons abusing themselves by consulting such as have familiar
spirits, v. 6. By this, as much as any thing, a man diminishes,
disparages, and deceives himself, and so abuses himself. What greater
madness can there be than for a man to go to a liar for information, and
to an enemy for advice? Those do so who turn after those that deal in
the black art, and know the depths of Satan. This is spiritual adultery
as much as idolatry is, giving that honour to the devil which is due to
God only; and the jealous God will give a bill of divorce to those that
thus go a whoring from him, and will cut them off, they having first cut
themselves off from him.

`II.` In the midst of these particular laws comes in that general charge,
v. 7, 8, where we have,

`1.` The duties required; and they are two:-`(1.)` That in our principles,
affections, and aims, we be holy: Sanctify yourselves and be you holy.
We must cleanse ourselves from all the pollutions of sin, consecrate
ourselves to the service and honour of God, and conform ourselves in
every thing to his holy will and image: this is to sanctify ourselves.
`(2.)` That in all our actions, and in the whole course of our
conversation, we be obedient to the laws of God: You shall keep my
statutes. By this only can we make it to appear that we have sanctified
ourselves and are holy, even by our keeping God\'s commandments; the
tree is known by its fruit. Nor can we keep God\'s statutes, as we
ought, unless we first sanctify ourselves, and be holy. Make the tree
good, and the fruit will be good.

`2.` The reasons to enforce these duties. `(1.)` \"I am the Lord your God;
therefore be holy, that you may resemble him whose people you are, and
may be pleasing to him. Holiness becomes his house and household.\" `(2.)`
I am the Lord who sanctifieth you. God sanctified them by peculiar
privileges, laws, and favours, which distinguished them from all other
nations, and dignified them as a people set apart for God. He gave them
his word and ordinances to be means of their sanctification, and his
good Spirit to instruct them; therefore they must be holy, else they
received the grace of God herein in vain. Note, `[1.]` God\'s people
are, and must be, persons of distinction. God has distinguished them by
his holy covenant, and therefore they ought to distinguish themselves by
their holy conversation. `[2.]` God\'s sanctifying us is a good reason
why we should sanctify ourselves, that we may comply with the designs of
his grace, and not walk contrary to them. If it be the Lord that
sanctifies us, we may hope the work shall be done, though it be
difficult: the manner of expression is like that, 2 Co. 5:5, He that
hath wrought us for the self-same thing is God. And his grace is so far
from superseding our care and endeavour that it most strongly engages
and encourages them. Work out your salvation, for it is God that worketh
in you.

### Verses 10-21

Sins against the seventh commandment are here ordered to be severely
punished. These are sins which, of all others, fools are most apt to
make a mock at; but God would teach those the heinousness of the guilt
by the extremity of the punishment that would not otherwise be taught
it.

`I.` Lying with another man\'s wife was made a capital crime. The
adulterer and the adulteress that had joined in the sin must fall alike
under the sentence: they shall both be put to death, v. 10. Long before
this, even in Job\'s time, this was reputed a heinous crime and an
iniquity to be punished by the judges, Job 31:11. It is a presumptuous
contempt of an ordinance of God, and a violation of his covenant, Prov.
2:17. It is an irreparable wrong to the injured husband, and debauches
the mind and conscience of both the offenders as much as any thing. It
is a sin which headstrong and unbridled lusts hurry men violently to,
and therefore it needs such a powerful restraint as this. It is a sin
which defiles a land and brings down God\'s judgments upon it, which
disquiets families, and tends to the ruin of all virtue and religion,
and therefore is fit to be animadverted upon by the conservators of the
public peace: but see Jn. 8:3-11.

`II.` Incestuous connections, whether by marriage or not. 1. Some of them
were to be punished with death, as a man\'s lying with his father\'s
wife, v. 11. Reuben would have been put to death for his crime (Gen.
35:22) if this law had been then made. It was the sin of the incestuous
Corinthian, for which he was to be delivered unto Satan, 1 Co. 5:1, 5. A
man\'s debauching his daughter-in-law, or his mother-in-law, or his
sister, was likewise to be punished with death, v. 12, 14, 17. 2. Others
of them God would punish with the curse of barrenness, as a man\'s
defiling his aunt, or his brother\'s wife (v. 19-21): They shall die
childless. Those that keep not within the divine rules of marriage
forfeit the blessings of marriage: They shall commit whoredom, and shall
not increase, Hos. 4:10. Nay it is said, They shall bear their iniquity,
that is, though they be not immediately cut off by the hand either of
God or man for this sin, yet the guilt of it shall lie upon them, to be
reckoned for another day, and not be purged with sacrifice or offering.

`III.` The unnatural lusts of sodomy and bestiality (sins not to be
mentioned without horror) were to be punished with death, as they are at
this day by our law, v. 13, 15, 16. Even the beast that was thus abused
was to be killed with the sinner, who was thereby openly put to the
greater shame: and the villany was thus represented as in the highest
degree execrable and abominable, all occasions of the remembrance or
mention of it being to be taken away. Even the unseasonable use of the
marriage, if presumptuous, and in contempt of the law, would expose the
offenders to the just judgment of God: they shall be cut off, v. 18. For
this is the will of God, that every man should possess his vessel (and
the wife is called the weaker vessel) in sanctification and honour, as
becomes saints.

### Verses 22-27

The last verse is a particular law, which comes in after the general
conclusion, as if omitted in its proper place: it is for the putting of
those to death that dealt with familiar spirits, v. 27. It would be an
affront to God and to his lively oracles, a scandal to the country, and
a temptation to ignorant bad people, to consult them, if such were known
and suffered to live among them. Those that are in league with the devil
have in effect made a covenant with death and an agreement with hell,
and so shall their doom be.

The rest of these verses repeat and inculcate what had been said before;
for to that unthinking forgetful people it was requisite that there
should be line upon line, and that general rules, with their reasons,
should be frequently insisted on, for the enforcement of particular
laws, and making them more effectual. Three things we are here reminded
of:-

`I.` Their dignity. 1. They had the Lord for their God, v. 24. They were
his, his care, his choice, his treasure, his jewels, his kingdom of
priests (v. 26): That you should be mine. Happy the people, and truly
great, that are in such a case. 2. Their God was a holy God (v. 26),
infinitely advanced above all others. His holiness is his glory, and it
was their honour to be related to him, while their neighbours were the
infamous worshippers of impure and filthy spirits. 3. The great God had
separated them from other people (v. 24), and again, v. 26. Other
nations were the common; they were the enclosure, beautified and
enriched with peculiar privileges, and designed for peculiar honours;
let them therefore value themselves accordingly, preserve their honour,
and not lay it in the dust, by walking in the way of the heathen.

`II.` Their duty; this is inferred from their dignity. God had done more
for them than for others, and therefore expected more from them than
from others. And what is it that the Lord their God requires, in
consideration of the great things done and designed? 1. You shall keep
all my statutes (v. 22); and there was all the reason in the world that
they should, for the statutes were their honour, and obedience to them
would be their lasting comfort. 2. You shall not walk in the manners of
nations, v. 23. Being separated from them, they must not associate with
them, nor learn their ways. The manners of the nations were bad enough
in them, but would be much worse in God\'s people. 3. You shall put a
difference between clean and unclean, v. 25. This is holiness, to
discern between things that differ, not to live at large, as if we might
say and do any thing, but to speak and act with caution. 4. You shall
not make your souls abominable, v. 25. Our constant care must be to
preserve the honour, by preserving the purity, of our own souls, and
never to do any thing to make them abominable to God and to our own
consciences.

`III.` Their danger. 1. They were going into an infected place (v. 24):
You shall inherit their land, a land flowing with milk and honey, which
they would have the comfort of if they kept their integrity; but,
withal, it was a land full of idols, idolatries, and superstitious
usages, which they would be apt to fall in love with, having brought
from Egypt with them a strange disposition to take that infection. 2. If
they took the infection, it would be of pernicious consequence to them.
The Canaanites were to be expelled for these very sins: They committed
all these things, therefore I abhorred them, v. 23. See what an evil
thing sin is; it provokes God to abhor his own creatures, whereas
otherwise he delights in the work of his hands. And, if the Israelites
trod in the steps of their impiety, they must expect that the land would
spue them out (v. 22), as he had told them before, ch. 18:28. If God
spared not the natural branches, but broke them off, neither would he
spare those who were grafted in, if they degenerated. Thus the rejection
of the Jews stands for a warning to all Christian churches to take heed
lest the kingdom of God be taken from them. Those that sin like others
must expect to smart like them; and their profession of relation to God
will be no security to them.
