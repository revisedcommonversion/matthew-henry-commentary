Introduction to Leviticus
=========================

There is nothing historical in all this book of Leviticus except the
account which it gives us of the consecration of the priesthood (ch.
8-9), of the punishment of Nadab and Abihu, by the hand of God, for
offering strange fire (ch. 10), and of Shelomith\'s son, by the hand of
the magistrate, for blasphemy (ch. 24). All the rest of the book is
taken up with the laws, chiefly the ecclesiastical laws, which God gave
to Israel by Moses, concerning their sacrifices and offerings, their
meats and drinks, and divers washings, and the other peculiarities by
which God set that people apart for himself, and distinguished them from
other nations, all which were shadows of good things to come, which are
realized and superseded by the gospel of Christ. We call the book
Leviticus, from the Septuagint, because it contains the laws and
ordinances of the levitical priesthood (as it is called, Heb. 7:11), and
the ministrations of it. The Levites were principally charged with these
institutions, both to do their part and to teach the people theirs. We
read, in the close of the foregoing book, of the setting up of the
tabernacle, which was to be the place of worship; and, as that was
framed according to the pattern, so must the ordinances of worship be,
which were there to be administered. In these the divine appointment was
as particular as in the former, and must be as punctually observed. The
remaining record of these abrogated laws is of use to us, for the
strengthening of our faith in Jesus Christ, as the Lamb slain from the
foundation of the world, and for the increase of our thankfulness to
God, that by him we are freed from the yoke of the ceremonial law, and
live in the times of reformation.
