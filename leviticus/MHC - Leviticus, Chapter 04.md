Leviticus, Chapter 4
====================

Commentary
----------

This chapter is concerning the sin-offering, which was properly intended
to make atonement for a sin committed through ignorance, `I.` By the
priest himself (v. 1-12). Or, `II.` By the whole congregation (v. 13-21).
Or, `III.` By a ruler (v. 22-26). Or, `IV.` By a private person (v. 27,
etc.).

### Verses 1-12

The laws contained in the first three chapters seem to have been
delivered to Moses at one time. Here begin the statutes of another
session, another day. From the throne of glory between the cherubim God
delivered these orders. And he enters now upon a subject more strictly
new than those before. Burnt-offerings, meat-offerings, and
peace-offerings, it should seem, had been offered before the giving of
the law upon mount Sinai; those sacrifices the patriarchs had not been
altogether unacquainted with (Gen. 8:20; Ex. 20:24), and in them they
had respect to sin, to make atonement for it, Job 1:5. But the law being
now added because of transgressions (Gal. 3:19), and having entered,
that eventually the offence might abound (Rom. 5:20), they were put into
a way of making atonement for sin more particularly by sacrifice, which
was (more than any of the ceremonial institutions) a shadow of good
things to come, but the substance is Christ, and that one offering of
himself by which he put away sin and perfected for ever those who are
sanctified.

`I.` The general case supposed we have, v. 2. Here observe, 1. Concerning
sin in general, that it is described to be against any of the
commandments of the Lord; for sin is the transgression of the law, the
divine law. The wits or wills of men, their inventions or their
injunctions, cannot make that to be sin which the law of God has not
made to be so. It is said likewise, if a soul sin, for it is not sin if
it be not some way or other the soul\'s act; hence it is called the sin
of the soul (Mic. 6:7), and it is the soul that is injured by it, Prov.
8:36. 2. Concerning the sins for which those offerings were appointed.
`(1.)` They are supposed to be overt acts; for, had they been required to
bring a sacrifice for every sinful thought or word, the task had been
endless. Atonement was made for those in the gross, on the day of
expiation, once a year; but these are said to be done against the
commandments. `(2.)` They are supposed to be sins of commission, things
which ought not to be done. Omissions are sins, and must come into
judgment; but what had been omitted at one time might be done at
another, and so to obey was better than sacrifice: but a commission was
past recall. `(3.)` They are supposed to be sins committed through
ignorance. If they were done presumptuously, and with an avowed contempt
of the law and the Law-maker, the offender was to be cut off, and there
remained no sacrifice for the sin, Heb. 10:26, 27; Num. 15:30. But if
the offender were either ignorant of the law, as in divers instances we
may suppose many were (so numerous and various were the prohibitions),
or were surprised into the sin unawares, the circumstances being such as
made it evident that his resolution against the sin was sincere, but
that he was overtaken in it, as the expression is (Gal. 6:1), in this
case relief was provided by the remedial law of the sin-offering. And
the Jews say, \"Those crimes only were to be expiated by sacrifice, if
committed ignorantly, for which the criminal was to have been cut off if
they had been committed presumptuously.\"

`II.` The law begins with the case of the anointed priest, that is, the
high priest, provided he should sin through ignorance; for the law made
men priests who had infirmity. Though his ignorance was of all others
least excusable, yet he was allowed to bring his offering. His office
did not so far excuse his offence as that it should be forgiven him
without a sacrifice; yet it did not so far aggravate it but that it
should be forgiven him when he did bring his sacrifice. If he sin
according to the sin of the people (so the case is put, v. 3), which
supposes him in this matter to stand upon the level with other
Israelites, and to have no benefit of his clergy at all. Now the law
concerning the sin-offering for the high priest is, 1. That he must
bring a bullock without blemish for a sin-offering (v. 3), as valuable
an offering as that for the whole congregation (v. 14); whereas for any
other ruler, or a common person, a kid of the goats should serve, v. 23,
28. This intimated the greatness of the guilt connected with the sin of
a high priest. The eminency of his station, and his relation both to God
and to the people, greatly aggravated his offences; see Rom. 2:21. 2.
The hand of the offerer must be laid upon the head of the offering (v.
4), with a solemn penitent confession of the sin he had committed,
putting it upon the head of the sin-offering, ch. 16:21. No remission
without confession, Ps. 32:5; Prov. 28:13. It signified also a
confidence in this instituted way of expiating guilt, as a figure of
something better yet to come, which they could not stedfastly discern.
He that laid his hand on the head of the beast thereby owned that he
deserved to die himself, and that it was God\'s great mercy that he
would please to accept the offering of this beast to die for him. The
Jewish writers themselves say that neither the sin-offering nor the
trespass-offering made atonement, except for those that repented and
believed in their atonement. 3. The bullock must be killed, and a great
deal of solemnity there must be in disposing of the blood; for it was
the blood that made atonement, and without shedding of blood there was
no remission, v. 5-7. Some of the blood of the high-priest\'s
sin-offering was to be sprinkled seven times before the veil, with an
eye towards the mercy-seat, though it was veiled: some of it was to be
put upon the horns of the golden altar, because at that altar the priest
himself ministered; and thus was signified the putting away of that
pollution which from his sins did cleave to his services. It likewise
serves to illustrate the influence which Christ\'s satisfaction has upon
the prevalency of his intercession. The blood of his sacrifice is put
upon the altar of his incense and sprinkled before the Lord. When this
was done the remainder of the blood was poured at the foot of the brazen
altar. By this rite, the sinner acknowledged that he deserved to have
his blood thus poured out like water. It likewise signified the pouring
out of the soul before God in true repentance, and typified our
Saviour\'s pouring out his soul unto death. 4. The fat of the inwards
was to be burnt upon the altar of burnt-offering, v. 8-10. By this the
intention of the offering and of the atonement made by it was directed
to the glory of God, who, having been dishonoured by the sin, was thus
honoured by the sacrifice. It signified the sharp sufferings of our Lord
Jesus, when he was made sin (that is, a sin-offering) for us, especially
the sorrows of his soul and his inward agonies. It likewise teaches us,
in conformity to the death of Christ, to crucify the flesh. 5. The head
and body of the beast, skin and all, were to be carried without the
camp, to a certain place appointed for that purpose, and there burnt to
ashes, v. 11, 12. This was very significant, `(1.)` Of the duty of
repentance, which is the putting away of sin as a detestable thing,
which our soul hates. True penitents say to their idols, \"Get you
hence; what have we to do any more with idols?\" The sin-offering is
called sin. What they did to that we must do to our sins; the body of
sin must be destroyed, Rom. 6:6. `(2.)` Of the privilege of remission.
When God pardons sin he quite abolishes it, casts it behind his back.
The iniquity of Judah shall be sought for and not found. The apostle
takes particular notice of this ceremony, and applies it to Christ (Heb.
13:11-13), who suffered without the gate, in the place of a skull, where
the ashes of dead men, as those of the altar, were poured out.

### Verses 13-21

This is the law for expiating the guilt of a national sin, by a sin
offering. If the leaders of the people, through mistake concerning the
law, caused them to err, when the mistake was discovered an offering
must be brought, that wrath might not come upon the whole congregation.
Observe, 1. It is possible that the church may err, and that her guides
may mislead her. It is here supposed that the whole congregation may
sin, and sin through ignorance. God will always have a church on earth;
but he never said it should be infallible, or perfectly pure from
corruption on this side heaven. 2. When a sacrifice was to be offered
for the whole congregation, the elders were to lay their hands upon the
head of it (three of them at least), as representatives of the people
and agents for them. The sin we suppose to have been some common custom,
taken up and used by the generality of the people, upon presumption of
its being lawful, which afterwards, upon search, appeared to be
otherwise. In this case the commonness of the usage received perhaps by
tradition from their fathers, and the vulgar opinion of its being
lawful, would not so far excuse them from sin but that they must bring a
sacrifice to make atonement for it. There are many bad customs and forms
of speech which are thought to have no harm in them, and yet may bring
guilt and wrath upon a land, which therefore it concerns the elders both
to reform and to intercede with God for the pardon of, Joel 2:16. 3. The
blood of this sin-offering, as of the former, was to be sprinkled seven
times before the Lord, v. 17. It was not to be poured out there, but
sprinkled only; for the cleansing virtue of the blood of Christ was then
and still is sufficiently signified and represented by sprinkling, Isa.
52:15. It was to be sprinkled seven times. Seven is a number of
perfection, because when God had made the world in six days he rested
the seventh; so this signified the perfect satisfaction Christ made, and
the complete cleansing of the souls of the faithful by it; see Heb.
10:14. The blood was likewise to be put upon the horns of the
incense-altar, to which there seems to be an allusion in Jer. 17:1,
where the sin of Judah is said to be graven upon the horns of their
altars. If they did not forsake their sins, the putting of the blood of
their sin-offerings upon the horns of their altars, instead of taking
away their guilt, did but bind it on the faster, perpetuated the
remembrance of it, and remained a witness against them. It is likewise
alluded to in Rev. 9:13, where a voice is heard from the four horns of
the golden altar; that is, an answer of peace is given to the prayers of
the saints, which are acceptable and prevalent only by virtue of the
blood of the sin-offering put upon the horns of that altar; compare Rev.
8:3. 4. When the offering is completed, it is said, atonement is made,
and the sin shall be forgiven, v. 20. The promise of remission is
founded upon the atonement. It is spoken here of the forgiveness of the
sin of the whole congregation, that is, the turning away of those
national judgments which the sin deserved. Note, The saving of churches
and kingdoms from ruin is owing to the satisfaction and mediation of
Christ.

### Verses 22-26

Observe here, 1. That God takes notice of and is displeased with the
sins of rulers. Those who have power to call others to account are
themselves accountable to the ruler of rulers; for, as high as they are,
there is a higher than they. This is intimated in that the commandment
transgressed is here said to be the commandment of the Lord his God, v.
22. He is a prince to others, but let him know the Lord is a God to him.
2. The sin of the ruler which he committed through ignorance is supposed
afterwards to come to his knowledge (v. 23), which must be either by the
check of his own conscience or by the reproof of his friends, both which
we should all, even the best and greatest, not only submit to, but be
thankful for. What we have done amiss we should be very desirous to come
to the knowledge of. That which I see not, teach thou me, and show me
wherein I have erred, are prayers we should put up to God every day,
that though through ignorance we fall into sin we may not through
ignorance lie still in it. 3. The sin-offering for a ruler was to be a
kid of the goats, not a bullock, as for the priest and the whole
congregation; nor was the blood of his sin-offering to be brought into
the tabernacle, as of the other two, but it was all bestowed upon the
brazen altar (v. 25); nor was the flesh of it to be burnt, as that of
the other two, without the camp, which intimated that the sin of a
ruler, though worse than that of a common person, yet was not so
heinous, nor of such pernicious consequence, as the sin of the high
priest, or of the whole congregation. A kid of the goats was sufficient
to be offered for a ruler, but a bullock for a tribe, to intimate that
the ruler, though major singulis-greater than each, was minor
universis-less than the whole. It is bad when great men give bad
examples, but worse when all men follow them. 4. It is promised that the
atonement shall be accepted and the sin forgiven (v. 26), that is, if he
repent and reform; for otherwise God swore concerning Eli, a judge in
Israel, that the iniquity of his house should not be purged with
sacrifice nor offering for ever, 1 Sa. 3:14.

### Verses 27-35

`I.` Here is the law of the sin-offering for a common person, which
differs from that for a ruler only in this, that a private person might
bring either a kid or a lamb, a ruler only a kid; and that for a ruler
must be a male, for the other a female: in all the circumstances of the
management of the offering they agreed. Observe, 1. The case supposed:
If any one of the common people sin through ignorance, v. 27. The
prophet supposes that they were not so likely as the great men to know
the way of the Lord, and the judgment of their God (Jer. 5:4), and yet,
if they sin through ignorance, they must bring a sin-offering. Note,
Even sins of ignorance need to be atoned for by sacrifice. To be able to
plead, when we are charged with sin, that we did it ignorantly, and
through the surprise of temptation, will not bring us off if we be not
interested in that great plea, Christ hath died, and entitled to the
benefit of that. We have all need to pray with David (and he was a
ruler) to be cleansed from secret faults, the errors which we ourselves
do not understand or are not aware of, Ps. 19:12. 2. That the sins of
ignorance committed by a single person, a common obscure person, did
require a sacrifice; for, as the greatest are not above the censure, so
the meanest are not below the cognizance of the divine justice. None of
the common people, if offenders, were overlooked in a crowd. 3. That a
sin-offering was not only admitted, but accepted, even from one of the
common people, and an atonement made by it, v. 31, 35. Here rich and
poor, prince and peasant, meet together; they are both alike welcome to
Christ, and to an interest in his sacrifice, upon the same terms. See
Job 34:19.

`II.` From all these laws concerning the sin-offerings we may learn, 1.
To hate sin, and to watch against it. That is certainly a very bad thing
to make atonement for which so many innocent and useful creatures must
be slain and mangled thus. 2. To value Christ, the great and true
sin-offering, whose blood cleanses from all sin, which it was not
possible that the blood of bulls and of goats should take away. Now, if
any man sin, Christ is the propitiation (1 Jn. 2:1, 2), not for Jews
only, but for Gentiles. And perhaps there was some allusion to this law
concerning sacrifices for sins of ignorance in that prayer of Christ\'s,
just when he was offering up himself a sacrifice, Father, forgive them,
for they know not what they do.
