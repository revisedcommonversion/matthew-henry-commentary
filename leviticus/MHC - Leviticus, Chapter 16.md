Leviticus, Chapter 16
=====================

Commentary
----------

In this chapter we have the institution of the annual solemnity of the
day of atonement, or expiation, which had as much gospel in it as
perhaps any of the appointments of the ceremonial law, as appears by the
reference the apostle makes to it, Heb. 9:7, etc. We had before divers
laws concerning sin-offerings for particular persons, and to be offered
upon particular occasions; but this is concerning the stated sacrifice,
in which the whole nation was interested. The whole service of the day
is committed to the high priest. `I.` He must never come into the most
holy place but upon this day (v. 1, 2). `II.` He must come dressed in
linen garments (v. 4). `III.` He must bring a sin-offering and a
burnt-offering for himself (v. 3), offer his sin-offering (v. 6-11),
then go within the veil with some of the blood of his sin-offering, burn
incense, and sprinkle the blood before the mercy-seat (v. 12-14). `IV.`
Two goats must be provided for the people, lots cast upon them, and, 1.
One of them must be a sin-offering for the people (v. 5, 7-9), and the
blood of it must be sprinkled before the mercy-seat (v. 15-17), and then
some of the blood of both the sin-offerings must be sprinkled upon the
altar (v. 18, 19). 2. The other must be a scape-goat (v. 10), the sins
of Israel must be confessed over him, and then he must be sent away into
the wilderness (v. 20-22), and he that brought him away must be
ceremonially unclean (v. 26). `V.` The burnt-offerings were then to be
offered, the fat of the sin-offerings burnt on the altar, and their
flesh burnt without the camp (v. 23-25, 27, 28). `VI.` The people were to
observe the day religiously by a holy rest and holy mourning for sin;
and this was to be a statute for ever (v. 29, etc.).

### Verses 1-4

Here is, `I.` The date of this law concerning the day of atonement: it was
after the death of the two sons of Aaron (v. 1), which we read, ch.
10:1. 1. Lest Aaron should fear that any remaining guilt of that sin
should cleave to his family, or (seeing the priests were so apt to
offend) that some after-sin of his other sons should be the ruin of his
family, he is directed how to make atonement for his house, that it
might keep in with God; for the atonement for it would be the
establishment of it, and preserve the entail of the blessing upon it. 2.
The priests being warned by the death of Nadab and Abihu to approach to
God with reverence and godly fear (without which they came at their
peril), directions are here given how the nearest approach might be
made, not only without peril, but to unspeakable advantage and comfort,
if the directions were observed. When they were cut off for an undue
approach, the rest must not say, \"Then we will not draw near at all,\"
but, \"Then we will do it by rule.\" They died for their sin, therefore
God graciously provides for the rest, that they die not. Thus God\'s
judgments on some should be instructions to others.

`II.` The design of this law. One intention of it was to preserve a
veneration for the most holy place, within the veil, where the
Shechinah, or divine glory, was pleased to dwell between the cherubim:
Speak unto Aaron, that he come not at all times into the holy place, v.
2. Before the veil some of the priests came every day to burn incense
upon the golden altar, but within the veil none must ever come but the
high priest only, and he but on one day in the year, and with great
ceremony and caution. That place where God manifested his special
presence must not be made common. If none must come into the
presence-chamber of an earthly king uncalled, no, not the queen herself,
upon pain of death (Esth. 4:11), was it not requisite that the same
sacred respect should be paid to the Kings of kings? But see what a
blessed change is made by the gospel of Christ; all good Christians have
now boldness to enter into the holiest, through the veil, every day
(Heb. 10:19, 20); and we come boldly (not as Aaron must, with fear and
trembling) to the throne of grace, or mercy-seat, Heb. 4:16. While the
manifestations of God\'s presence and grace were sensible, it was
requisite that they should thus be confined and upon reserve, because
the objects of sense the more familiar they are made the less awful or
delightful they become; but now that they are purely spiritual it is
otherwise, for the objects of faith the more they are conversed with the
more do they manifest of their greatness and goodness: now therefore we
are welcome to come at all times into the holy place not made with
hands, for we are made to sit together with Christ in heavenly places by
faith, Eph. 2:6. Then Aaron must not come near at all times, lest he
die; we now must come near at all times that we may live: it is distance
only that is our death. Then God appeared in the cloud upon the
mercy-seat, but now with open face we behold, not in a dark cloud, but
in a clear glass, the glory of the Lord, 2 Co. 3:18.

`III.` The person to whom the work of this day was committed, and that
was the high priest only: Thus shall Aaron come into the holy place, v.
3. He was to do all himself upon the day of atonement: only there was a
second provided to be his substitute or supporter, in case any thing
should befal him, either of sickness or ceremonial uncleanness, that he
could not perform the service of the day. All Christians are spiritual
priests, but Christ only is the high priest, and he alone it is that
makes atonement, nor needed he either assistant or substitute.

`IV.` The attire of the high priest in this service. He was not to be
dressed up in his rich garments that were peculiar to himself: he was
not to put on the ephod, with the precious stones in it, but only the
linen clothes which he wore in common with the inferior priests, v. 4.
That meaner dress did best become him on this day of humiliation; and,
being thinner and lighter, he would in it be more expedite for the work
or service of the day, which was all to go through his hands. Christ,
our high priest, made atonement for sin in our nature; not in the robes
of his own peculiar glory, but the linen garments of our mortality,
clean indeed, but mean.

### Verses 5-14

The Jewish writers say that for seven days before the day of expiation
the high priest was to retire from his own house, and to dwell in a
chamber of the temple, that he might prepare himself for the service of
this great day. During those seven days he himself did the work of the
inferior priests about the sacrifices, incense, etc., that he might have
his hand in for this day: he must have the institution read to him again
and again, that he might be fully apprised of the whole method. 1. He
was to begin the service of the day very early with the usual morning
sacrifice, after he had first washed his whole body before he dressed
himself, and his hands and feet again afterwards. He then burned the
daily incense, dressed the lamps, and offered the extraordinary
sacrifice appointed for this day (not here, but Num. 29:8), a bullock, a
ram, and seven lambs, all for burnt-offerings. This he is supposed to
have done in his high priest\'s garments. 2. He must now put off his
rich robes, bathe himself, put on the linen garments, and present unto
the Lord his own bullock, which was to be a sin-offering for himself and
his own house, v. 6. The bullock was set between the temple and the
altar, and the offering of him mentioned in this verse was the making of
a solemn confession of his sins and the sins of his house, earnestly
praying for the forgiveness of them, and this with his hands on the head
of the bullock. 3. He must then cast lots upon the two goats, which were
to make (both together) one sin-offering for the congregation. One of
these goats must be slain, in token of a satisfaction to be made to
God\'s justice for sin, the other must be sent away, in token of the
remission or dismission of sin by the mercy of God. Both must be
presented together to God (v. 7) before the lot was cast upon them, and
afterwards the scape-goat by itself, v. 10. Some think that goats were
chosen for the sin-offering because, by the disagreeableness of their
smell, the offensiveness of sin is represented: others think, because it
was said that the demons which the heathens then worshipped often
appeared to their worshippers in the form of goats, God therefore
obliged his people to sacrifice goats, that they might never be tempted
to sacrifice to goats. 4. The next thing to be done was to kill the
bullock for the sin-offering for himself and his house, v. 11. \"Now,\"
say the Jews, \"he must again put his hands on the head of the bullock,
and repeat the confession and supplication he had before made, and kill
the bullock with his own hands, to make atonement for himself first (for
how could he make reconciliation for the sins of the people till he was
himself first reconciled?) and for his house, not only his own family,
but all the priests, who are called the house of Aaron,\" Ps. 135:19.
This charity must begin at home, though it must not end there. The
bullock being killed, he left one of the priests to stir the blood, that
it might not thicken, and then, 5. He took a censer of burning coals
(that would not smoke) in one hand, and a dish full of the sweet incense
in the other, and then went into the holy of holies through the veil,
and went up towards the ark, set the coals down upon the floor, and
scattered the incense upon them, so that the room was immediately filled
with smoke. The Jews say that he was to go in side-ways, that he might
not look directly upon the ark where the divine glory was, till it was
covered with smoke; then he must come out backwards, out of reverence to
the divine majesty; and, after a short prayer, he was to hasten out of
the sanctuary, to show himself to the people, that they might not
suspect that he had misbehaved himself and died before the Lord. 6. He
then fetched the blood of the bullock from the priest whom he had left
stirring it, and took that in with him the second time into the holy of
holies, which was now filled with the smoke of the incense, and
sprinkled with his finger of that blood upon, or rather towards, the
mercy-seat, once over against the top of it and then seven times towards
the lower part of it, v. 14. But the drops of blood (as the Jews expound
it) all fell upon the ground, and none touched the mercy-seat. Having
done this, he came out of the most holy place, set the basin of blood
down in the sanctuary, and went out.

### Verses 15-19

When the priest had come out from the sprinkling the blood of the
bullock before the mercy-seat, 1. He must next kill the goat which was
the sin-offering for the people (v. 15) and go the third time into the
holy of holies, to sprinkle the blood of the goat, as he had done that
of the bullock; and thus he was to make atonement for the holy place (v.
16); that is, whereas the people by their sins had provoked God to take
away those tokens of his favourable presence with them, and rendered
even that holy place unfit to be the habitation of the holy God,
atonement was hereby made for sin, that God, being reconciled to them,
might continue with them. 2. He must then do the same for the outward
part of the tabernacle that he had done for the inner room, by
sprinkling the blood of the bullock first, and then that of the goat,
without the veil, where the table and incense-altar stood, eight times
each as before. The reason intimated is because the tabernacle remained
among them in the midst of their uncleanness, v. 16. God would hereby
show them how much their hearts needed to be purified, when even the
tabernacle, only by standing in the midst of such an impure and sinful
people, needed this expiation; and also that even their devotions and
religious performances had much amiss in them, for which it was
necessary that atonement should be made. During this solemnity, none of
the inferior priests must come into the tabernacle (v. 17), but, by
standing without, must own themselves unworthy and unfit to minister
there, because their follies, and defects, and manifold impurities in
their ministry, had made this expiation of the tabernacle necessary. 3.
He must then put some of the blood, both of the bullock and of the goat
mixed together, upon the horns of the altar that is before the Lord, v.
18, 19. It is certain that the altar of incense had this blood put upon
it, for so it is expressly ordered (Ex. 30:10); but some think that this
directs the high priest to the altar of burnt-offerings, for that also
is here called the altar before the Lord (v. 12), because he is said to
go out to it, and because it may be presumed that that also had need of
an expiation; for to that the gifts and offerings of the children of
Israel were all brought, from whose uncleanness the altar is here said
to be hallowed.

### Verses 20-28

The high priest having presented unto the Lord the expiatory sacrifices,
by the sprinkling of their blood, the remainder of which, it is
probable, he poured out at the foot of the brazen altar, 1. He is next
to confess the sins of Israel, with both his hands upon the head of the
scape-goat (v. 20, 21); and whenever hands were imposed upon the head of
any sacrifice it was always done with confession, according as the
nature of the sacrifice was; and, this being a sin-offering, it must be
a confession of sin. In the latter and more degenerate ages of the
Jewish church they had a set form of confession prepared for the high
priest, but God here prescribed none; for it might be supposed that the
high priest was so well acquainted with the state of the people, and had
such a tender concern for them, that he needed not any form. The
confession must be as particular as he could make it, not only of all
the iniquities of the children of Israel, but all their transgressions
in all their sins. In one sin there may be many transgressions, from the
several aggravating circumstances of it; and in our confessions we
should take notice of them, and not only say, I have sinned, but, with
Achan, \"Thus and thus have I done.\" By this confession he must put the
sins of Israel upon the head of the goat; that is, exercising faith upon
the divine appointment which constituted such a translation, he must
transfer the punishment incurred from the sinners to the sacrifice,
which would have been but a jest, nay, an affront to God, if he himself
had not ordained it. 2. The goat was then to be sent away immediately by
the hand of a fit person pitched upon for the purpose, into a
wilderness, a land not inhabited; and God allowed them to make this
construction of it, that the sending away of the goat was the sending
away of their sins, by a free and full remission: He shall bear upon him
all their iniquities, v. 22. The losing of the goat was a sign to them
that the sins of Israel should be sought for, and not found, Jer. 50:20.
The later Jews had a custom to tie one shred of scarlet cloth to the
horns of the goat and another to the gate of the temple, or to the top
of the rock where the goat was lost, and they concluded that if it
turned white, as they say it usually did, the sins of Israel were
forgiven, as it is written, Though your sins have been as scarlet, they
shall be as wool: and they add that for forty years before the
destruction of Jerusalem by the Romans the scarlet cloth never changed
colour at all, which is a fair confession that, having rejected the
substance, the shadow stood them in no stead. 3. The high priest must
then put off his linen garments in the tabernacle, and leave them there,
the Jews say never to be worn again by himself or any other, for they
made new ones every year; and he must bathe himself in water, put on his
rich clothes, and then offer both his own and the people\'s
burnt-offerings, v. 23, 24. When we have the comfort of our pardon God
must have the glory of it. If we have the benefit of the sacrifice of
atonement, we must not grudge the sacrifices of acknowledgment. And, it
should seem, the burning of the fat of the sin-offering was deferred
till now (v. 25), that it might be consumed with the burnt-offerings. 4.
The flesh of both those sin-offerings whose blood was taken within the
veil was to be all burnt, not upon the altar, but at a distance without
the camp, to signify both our putting away sin by true repentance, and
the spirit of burning, and God\'s putting it away by a full remission,
so that it shall never rise up in judgment against us. 5. He that took
the scape-goat into the wilderness, and those that burned the
sin-offering, were to be looked upon as ceremonially unclean, and must
not come into the camp till they had washed their clothes and bathed
their flesh in water, which signified the defiling nature of sin; even
the sacrifice which was but made sin was defiling: also the imperfection
of the legal sacrifices; they were so far from taking away sin that even
they left some stain upon those that touched them. 6. When all this was
done, the high priest went again into the most holy place to fetch his
censer, and so returned to his own house with joy, because he had done
his duty, and died not.

### Verses 29-34

`I.` We have here some additional directions in reference to this great
solemnity, particularly,

`1.` The day appointed for this solemnity. It must be observed yearly on
the tenth day of the seventh month, v. 29. The seventh had been reckoned
the first month, till God appointed that the month in which the children
of Israel came out of Egypt should thenceforward be accounted and called
the first month. Some have fancied that this tenth day of the seventh
month was the day of the year on which our first parents fell, and that
it was kept as a fast in remembrance of their fall. Dr. Lightfoot
computes that this was the day on which Moses came the last time down
from the mount, when he brought with him the renewed tables, and the
assurances of God\'s being reconciled to Israel, and his face shone:
that day must be a day of atonement throughout their generations; for
the remembrance of God\'s forgiving them their sin about the golden calf
might encourage them to hope that, upon their repentance, he would
forgive them all trespasses.

`2.` The duty of the people on this day. `(1.)` They must rest from all
their labours: It shall be a sabbath of rest, v. 31. The work of the day
was itself enough, and a good day\'s work if it was done well; therefore
they must do no other work at all. The work of humiliation for sin
requires such a close application of mind, and such a fixed engagement
of the whole man, as will not allow us to turn aside to any other work.
The day of atonement seems to be that sabbath spoken of by the prophet
(Isa. 58:13), for it is the same with the fast spoken of in the verses
before. `(2.)` They must afflict their souls. They must refrain from all
bodily refreshments and delights, in token of inward humiliation and
contrition of soul for their sins. They all fasted on this day from food
(except the sick and children), and laid aside their ornaments, and did
not anoint themselves, as Daniel, ch. 10:3, 12. David chastened his soul
with fasting, Ps. 35:13. And it signified the mortifying of sin and
turning from it, loosing the bands of wickedness, Isa. 58:6, 7. The
Jewish doctors advised that they should not on that day read those
portions of scripture which were proper to affect them with delight and
joy, because it was a day to afflict their souls.

`3.` The perpetuity of this institution: It shall be a statute for ever,
v. 29, 34. It must not be intermitted any year, nor ever let fall till
that constitution should be dissolved, and the type should be superseded
by the antitype. As long as we are continually sinning, we must be
continually repenting, and receiving the atonement. The law of
afflicting our souls for sin is a statute for ever, which will continue
in force till we arrive where all tears, even those of repentance, will
be wiped from our eyes. The apostle observes it as an evidence of the
insufficiency of the legal sacrifices to take away sin, and purge the
conscience from it, that in them there was a remembrance made of sin
every year, upon the day of atonement, Heb. 10:1-3. The annual
repetition of the sacrifices showed that there was in them only a faint
and feeble effort towards making atonement; it could be done effectually
only by the offering up of the body of Christ once for all, and that
once was sufficient; that sacrifice needed not to be repeated.

`II.` Let us see what there was of gospel in all this.

`1.` Here are typified the two great gospel privileges of the remission
of sin and access to God, both which we owe to the mediation of our Lord
Jesus. Here then let us see,

`(1.)` The expiation of guilt which Christ made for us. He is himself both
the maker and the matter of the atonement; for he is, `[1.]` The priest,
the high priest, that makes reconciliation for the sins of the people,
Heb. 2:17. He, and he only, is par negotio-fit for the work and worthy
of the honour: he is appointed by the Father to do it, who sanctified
him, and sent him into the world for this purpose, that God might in him
reconcile the world to himself. He undertook it, and for our sakes
sanctified himself, and set himself apart for it, Jn. 17:19. The high
priest\'s frequently bathing himself on this day, and performing the
service of it in fine linen clean and white, signified the holiness of
the Lord Jesus, his perfect freedom from all sin, and his being
beautified and adorned with all grace. No man was to be with the high
priest when he made atonement (v. 17); for our Lord Jesus was to tread
the wine-press alone, and of the people there must be none with him
(Isa. 63:3); therefore, when he entered upon his sufferings, all his
disciples forsook him and fled, for it any of them had been taken and
put to death with him it would have looked as if they had assisted in
making the atonement; none but thieves, concerning whom there could be
no such suspicion, must suffer with him. And observe what the extent of
the atonement was which the high priest made: it was for the holy
sanctuary, for the tabernacle, for the altar, for the priests, and for
all the people, v. 33. Christ\'s satisfaction is that which atones for
the sins both of ministers and people, the iniquities of our holy (and
our unholy) things; the title we have to the privileges of ordinances,
our comfort in them, and benefit by them, are all owing to the atonement
Christ made. But, whereas the atonement which the high priest made
pertained only to the congregation of Israel, Christ is the
propitiation, not for their sins only, that are Jews, but for the sins
of the whole Gentile world. And in this also Christ infinitely excelled
Aaron, that Aaron needed to offer sacrifice for his own sin first, of
which he was to make confession upon the head of his sin-offering; but
our Lord Jesus had no sin of his own to answer for. Such a high priest
became us, Heb. 7:26. And therefore, when he was baptized in Jordan,
whereas others stood in the water confessing their sins (Mt. 3:6), he
went up straightway out of the water (v. 16), having no sins to confess.
`[2.]` As he is the high priest, so he is the sacrifice with which
atonement is made; for he is all in all in our reconciliation to God.
Thus he was prefigured by the two goats, which both made one offering:
the slain goat was a type of Christ dying for our sins, the scape-goat a
type of Christ rising again for our justification. It was directed by
lot, the disposal whereof was of the Lord, which goat should be slain;
for Christ was delivered by the determinate counsel and foreknowledge of
God. First, The atonement is said to be completed by putting the sins of
Israel upon the head of the goat. They deserved to have been abandoned
and sent into a land of forgetfulness, but that punishment was here
transferred to the goat that bore their sins, with reference to which
God is said to have laid upon our Lord Jesus (the substance of all these
shadows) the iniquity of us all (Isa. 53:6), and he is said to have
borne our sins, even the punishment of them, in his own body upon the
tree, 1 Pt. 2:24. Thus was he made sin for us, that is, a sacrifice for
sin, 2 Co. 5:21. He suffered and died, not only for our good, but in our
stead, and was forsaken, and seemed to be forgotten for a time, that we
might not be forsaken and forgotten for ever. Some learned men have
computed that our Lord Jesus was baptized of John in Jordan upon the
tenth day of the seventh month, which was the very day of atonement.
Then he entered upon his office as Mediator, and was immediately driven
of the Spirit into the wilderness, a land not inhabited. Secondly, The
consequence of this was that all the iniquities of Israel were carried
into a land of forgetfulness. Thus Christ, the Lamb of God, takes away
the sin the of world, by taking it upon himself, Jn. 1:29. And, when God
forgives sin, he is said to remember it no more (Heb. 8:12), to cast it
behind his back (Isa. 38:17), into the depths of the sea (Mic. 7:19),
and to separate it as far as the east is from the west, Ps. 103:12.

`(2.)` The entrance into heaven which Christ made for us is here typified
by the high priest\'s entrance into the most holy place. This the
apostle has expounded (Heb. 9:7, etc.), and he shows, `[1.]` That heaven
is the holiest of all, but not of that building, and that the way into
it by faith, hope, and prayer, through a Mediator, was not then so
clearly manifested as it is to us now by the gospel. `[2.]` That Christ
our high priest entered into heaven at his ascension once for all, and
as a public person, in the name of all his spiritual Israel, and through
the veil of his flesh, which was rent for that purpose, Heb. 10:20.
`[3.]` That he entered by his own blood (Heb. 9:12), taking with him to
heaven the virtues of the sacrifice he offered on earth, and so
sprinkling his blood, as it were, before the mercy-seat, where it speaks
better things than the blood of bulls and goats could do. Hence he is
said to appear in the midst of the throne as a lamb that had been slain,
Rev. 5:6. And, though he had no sin of his own to expiate, yet it was by
his own merit that he obtained for himself a restoration to his own
ancient glory (Jn. 17:4, 5), as well as an eternal redemption for us,
Heb. 9:12. `[4.]` The high priest in the holy place burned incense,
which typified the intercession that Christ ever lives to make for us
within the veil, in virtue of his satisfaction. And we could not expect
to live, no, not before the mercy-seat, if it were not covered with the
cloud of this incense. Mere mercy itself will not save us, without the
interposition of a Mediator. The intercession of Christ is there set
forth before God as incense, as this incense. And as the high priest
interceded for himself first, then for his household, and then for all
Israel, so our Lord Jesus, in the 17th of St. John (which was a specimen
of the intercession he makes in heaven), recommended himself first to
his Father, then his disciples who were his household, and then all that
should believe on him through their word, as all Israel; and, having
thus adverted to the uses and intentions of his offering, he was
immediately seized and crucified, pursuant to these intentions. `[5.]`
Herein the entry Christ made far exceeded Aaron\'s, that Aaron could not
gain admission, no, not for his own sons, into the most holy place; but
our Lord Jesus has consecrated for us also a new and living way into the
holiest, so that we also have boldness to enter, Heb. 10:19, 20. `[6.]`
The high priest was to come out again, but our Lord Jesus ever lives,
making intercession, and always appears in the presence of God for us,
whither as the forerunner he has for us entered, and where as agent he
continues for us to reside.

`2.` Here are likewise typified the two great gospel duties of faith and
repentance, by which we are qualified for the atonement, and come to be
entitled to the benefit of it. `(1.)` By faith we must put our hands upon
the head of the offering, relying on Christ as the Lord our
Righteousness, pleading his satisfaction as that which was alone able to
atone for our sins and procure us a pardon. \"Thou shalt answer, Lord,
for me. This is all I have to say for myself, Christ has died, yea,
rather has risen again; to his grace and government I entirely submit
myself, and in him I receive the atonement,\" Rom. 5:11. `(2.)` By
repentance we must afflict our souls; not only fasting for a time from
the delights of the body, but inwardly sorrowing for our sins, and
living a life of self-denial and mortification. We must also make a
penitent confession of sin, and this with an eye to Christ, whom we have
pierced, and mourning because of him; and with a hand of faith upon the
atonement, assuring ourselves that, if we confess our sins, God is
faithful and just to forgive us our sins, and to cleanse us from all
unrighteousness.

Lastly, In the year of jubilee, the trumpet which proclaimed the liberty
was ordered to be sounded in the close of the day of atonement, ch.
25:9. For the remission of our debt, release from our bondage, and our
return to our inheritance, are all owing to the mediation and
intercession of Jesus Christ. By the atonement we obtain rest for our
souls, and all the glorious liberties of the children of God.
