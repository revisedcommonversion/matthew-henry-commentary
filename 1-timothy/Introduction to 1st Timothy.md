Introduction to 1st Timothy
===========================

Hitherto Paul\'s epistles were directed to churches; now follow some to
particular persons: two to Timothy, one to Titus, and another to
Philemon-all three ministers. Timothy and Titus were evangelists, an
inferior order to the apostles, as appears by Eph. 4:11, Some prophets,
some apostles, some evangelists. Their commission and work was much the
same with that of the apostles, to plant churches, and water the
churches that were planted; and accordingly they were itinerants, as we
find Timothy was. Timothy was first converted by Paul, and therefore he
calls him his own son in the faith: we read of his conversion, Acts
16:3.

The scope of these two epistles is to direct Timothy how to discharge
his duty as an evangelist at Ephesus, where he now was, and where Paul
ordered him for some time to reside, to perfect the good work which he
had begun there. As for the ordinary pastoral charge of that church, he
had very solemnly committed it to the presbytery, as appears from Acts
20:28, where he charges the presbyters to feed the flock of God, which
he had purchased with his own blood.
