1st Chronicles, Chapter 18
==========================

Commentary
----------

David\'s piety and his prayer we had an account of in the foregoing
chapter; here follows immediately that which one might reasonably
expect, an account of his prosperity; for those that seek first the
kingdom of God and the righteousness thereof, as David did, shall have
other things added to them as far as God sees good for them. Here is, `I.`
His prosperity abroad. He conquered the Philistines (v. 1), the Moabites
(v. 2), the king of Zobah (v. 3, 4), the Syrians (v. 5-8), made the king
of Hamath his tributary (v. 9-11), and the Edomites (v. 12, 13). `II.` His
prosperity at home. His court and kingdom flourished (v. 14-17). All
this we had an account of before, 2 Sa. 8.

### Verses 1-8

After this, it is said (v. 1), David did those great exploits. After the
sweet communion he had had with God by the word and prayer, as mentioned
in the foregoing chapter, he went on his work with extraordinary vigour
and courage, conquering and to conquer. Thus Jacob, after his vision,
lifted up his feet, Gen. 29:1.

We have taken a view of these victories before, and shall now only
observe, 1. Those that have been long enemies to the Israel of God will
be brought down at last. The Philistines had, for several generations,
been vexatious to Israel, but now David subdued them, v. 1. Thus shall
all opposing rule, principality, and power, be, at the end of time, put
down by the Son of David, and the most inveterate enemies shall fall
before him. 2. Such is the uncertainty of this world that frequently men
lose their wealth and power when they think to confirm it. Hadarezer was
smitten as he went to establish his dominion, v. 3. 3. A horse is a vain
thing for safety, so David said (Ps. 33:17), and it seems he believed
what he said, for he houghed the chariot-horses, v. 4. Being resolved
not to trust to them (Ps. 20:7), he would not use them. 4. The enemies
of God\'s church are often made to ruin themselves by helping one
another, v. 5. The Syrians of Damascus were smitten when they came to
help Hadarezer. When hand thus joins in hand they shall not only not go
unpunished, but thereby they shall be gathered as the sheaves into the
floor, Mic. 4:11, 12. 5. The wealth of the sinner sometimes proves to
have been laid up for the just. The Syrians brought gifts, v. 6. Their
shields of gold and their brass were brought to Jerusalem, v. 7, 8. As
the tabernacle was built of the spoils of the Egyptians, so the temple
of the spoils of other Gentile nations, a happy presage of the interest
the Gentiles should have in the gospel church.

### Verses 9-17

Here let us learn, 1. That it is our interest to make those our friends
who have the presence of God with them. The king of Hamath, hearing of
David\'s great success, sent to congratulate him and to court his favour
with a noble present, v. 9, 10. It is in vain to contend with the Son of
David. Kiss the Son, therefore, lest he be angry; let the kings and
judges of the earth, and all inferior people too, be thus wise, thus
instructed. The presents we are to bring him are not vessels of gold and
silver, as here (those shall be welcomed to him who have no such
presents to bring), but our hearts and sincere affections, our whole
selves, we must present to him as living sacrifices. 2. That what God
blesses us with we must honour him with. The presents of his friends, as
well as the spoils of his enemies, David dedicated unto the Lord (v.
11), that is, he laid them up towards the building and enriching of the
temple. That is most truly and most comfortably our own which we have
consecrated unto the Lord, and which we use for his glory. Let our
merchandise and our hire be holiness to the Lord, Isa. 23:18. 3. That
those who take God along with them whithersoever they go may expect to
prosper, and be preserved, whithersoever they go. It was said before (v.
6) and here it is repeated (v. 13) that the Lord preserved David
whithersoever he went. Those are always under the eye of God that have
God always in their eye. 4. God gives men power, not that they may look
great with it, but that they may do good with it. When David reigned
over all Israel he executed judgment and justice among all his people,
and so answered the end of his elevation. He was not so intent on his
conquests abroad as to neglect the administration of justice at home.
Herein he served the purposes of the kingdom of providence, and of that
God who sits in the throne judging right; and he was an eminent type of
the Messiah, the sceptre of whose kingdom is a right sceptre.
