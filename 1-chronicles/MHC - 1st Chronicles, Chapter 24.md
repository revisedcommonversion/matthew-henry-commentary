1st Chronicles, Chapter 24
==========================

Commentary
----------

This chapter gives us a more particular account of the distribution of
the priests and Levites into their respective classes, for the more
regular discharge of the duties of their offices, according to their
families. `I.` Of the priests (v. 1-19). `II.` Of the Levites (v. 20-31).

### Verses 1-19

The particular account of these establishments is of little use to us
now; but, when Ezra published it, it was of great use to direct their
church affairs after their return from captivity into the old channel
again. The title of this record we have v. 1-These are the divisions of
the sons of Aaron, not by which they divided one from another, or were
at variance one with another (it is a pity there should ever be any such
divisions among the sons of Israel, but especially among the sons of
Aaron), but the distribution of them in order to the dividing of their
work among themselves; it was a division which God made, and was made
for him. 1. This distribution was made for the more regular discharge of
the duties of their office. God was, and still is, the God of order, and
not of confusion, particularly in the things of his worship. Number
without order is but a clog and an occasion of tumult; but when every
one has, and knows, and keeps, his place and work, the more the better.
In the mystical body, every member has its use, for the good of the
whole, Rom. 12:4, 5; 1 Co. 12:12. 2. It was made by lot, that the
disposal thereof might be of the Lord, and so all quarrels and
contentions might be prevented, and no man could be charged with
partiality, nor could any say that they had wrong done them. As God is
the God or order, so he is the God of peace. Solomon says of the lot
that it causeth contention to cease. 3. The lot was cast publicly, and
with great solemnity, in the presence of the king, princes, and priests,
that there might be no room for any fraudulent practices or the
suspicion of them. The lot is an appeal to God, and ought to be managed
with corresponding reverence and sincerity. Matthias was chosen to the
apostleship by lot, with prayer (Acts 1:24, 26), and I know not but it
might be still used in faith in parallel cases, as an instituted
ordinance. We have here the name of the public notary that was employed
in writing the names, and drawing the lots, (v. 6): Shemaiah, one of the
Levites. 4. What those priests were chosen to was to preside in the
affairs of the sanctuary (v. 5), in their several courses and turns.
That which was to be determined by the lot was only the precedency, not
who should serve (for they chose all the chief men), but who should
serve first, and who next, that every one might know his course, and
attend in it. Of the twenty-four chief men of the priests sixteen were
of the house of Eleazar and eight of Ithamar; for the house of Ithamar
may well be supposed to have dwindled since the sentence passed on the
family of Eli, who was of that house. The method of drawing the lots is
intimated (v. 6), one chief household being taken for Eleazar, and one
for Ithamar. The sixteen chief names of Eleazar were put in one urn, the
eight for Ithamar in another, and they drew out of them alternately, as
long as those for Ithamar lasted, and then out of those only for
Eleazar, or two for Eleazar, and then one for Ithamar, throughout. 5.
Among these twenty-four courses the eighth is that of Abijah or Abia (v.
10), which is mentioned (Lu. 1:5) as the course which Zechariah was of,
the father of John the Baptist, by which it appears that these courses
which David now settled, though interrupted perhaps in the bad reigns
and long broken off by the captivity, yet continued in succession till
the destruction of the second temple by the Romans. And each course was
called by the name of him in whom it was first founded, as the high
priest is here called Aaron (v. 19), because succeeding in his dignity
and power, though we read not of any of them that bore that name.
Whoever was high priest must be reverenced and observed by the inferior
priests as their father, as Aaron their father. Christ is high priest
over the house of God, to whom all believers, being made priests, are to
be in subjection.

### Verses 20-31

Most of the Levites here named were mentioned before, ch. 23:16, etc.
They were of those who were to attend the priests in the service of the
house of God. But they are here mentioned again as heads of the
twenty-four courses of Levites (and about so many are here named), who
were to attend the twenty-four courses of the priests: they are
therefore said to cast lots over against their brethren (so they are
called, not their lords), the sons of Aaron, who were not to lord it
over God\'s clergy, as the original word is, 1 Pt. 5:3. And, that the
whole disposal of the affair might be of the Lord, the principal fathers
cast lots over against their younger brethren; that is, those that were
of the elder house came upon he level with those of the younger
families, and took their place, not by seniority, but as God by the lot
directed. Note, In Christ no difference is made between bond and free,
elder and younger. The younger brethren, if they be faithful and
sincere, shall be no less acceptable to Christ than the principal
fathers.
