1st Chronicles, Chapter 14
==========================

Commentary
----------

In this chapter we have, `I.` David\'s kingdom established (v. 1, 2). `II.`
His family built up (v. 3-7). `III.` His enemies, the Philistines, routed
in two campaigns (v. 8-17). This is repeated here from 2 Sa. 5:11, etc.

### Verses 1-7

We may observe here, 1. There is no man that has such a sufficiency in
himself but he has need of his neighbours and has reason to be thankful
for their help: David had a very large kingdom, Hiram a very little one;
yet David could not build himself a house to his mind unless Hiram
furnished him with both workmen and materials, v. 1. This is a reason
why we should despise none, but, as we have opportunity, be obliging to
all. 2. It is a great satisfaction to a wise man to be settled, and to a
good man to see the special providences of God in his settlement. The
people had made David king; but he could not be easy, nor think himself
happy, till he perceived that the Lord had confirmed him king over
Israel, v. 2. \"Who shall unfix me if God hath fixed me?\" 3. We must
look upon all our advancements as designed for our usefulness. David\'s
kingdom was lifted up on high, not for his own sake, that he might look
great, but because of his people Israel, that he might be a guide and
protector to them. We are blessed in order that we may be blessings. See
Gen. 12:2. We are not born, nor do we live, for ourselves. 4. It is
difficult to thrive without growing secure and indulgent to the flesh.
It was David\'s infirmity that when he settled in his kingdom he took
more wives (v. 3), yet the numerous issue he had added to his honour and
strength. Lo, children are a heritage of the Lord. We had an account of
David\'s children, not only in Samuel, but in this book (ch. 3:1, etc.)
and now here again; for it was their honour to have such a father.

### Verses 8-17

This narrative of David\'s triumph over the Philistines is much the same
with that, 2 Sa. 5:17, etc. 1. Let the attack which the Philistines made
upon David forbid us to be secure in any settlement or advancement, and
engage us to expect molestation in this world. When we are most easy
something or other may come to be a terror or vexation to us. Christ\'s
kingdom will thus be insulted by the serpent\'s seed, especially when it
makes any advances. 2. Let David\'s enquiry of God, once and again, upon
occasion of the Philistines\' invading him, direct us in all our ways to
acknowledge God-in distress to fly to him, when we are wronged to appeal
to him, and, when we know not what to do, to ask counsel at his oracles,
to put ourselves under his direction, and to beg of him to show us the
right way. 3. Let David\'s success encourage us to resist our spiritual
enemies, in observance of divine directions and dependence on divine
strength. Resist the devil, and he shall flee as the Philistines did
before David. 4. Let the sound of the going in the tops of the mulberry
trees direct us to attend God\'s motions both in his providence and in
the influences of his Spirit. When we perceive God to go before us let
us gird up our loins, gird on our armour, and follow him. 5. Let
David\'s burning the gods of the Philistines, when they fell into his
hands, teach us a holy indignation against idolatry and all the remains
of it. 6. Let David\'s thankful acknowledgment of the hand of God in his
successes direct us to bring all our sacrifices of praise to God\'s
altar. Not unto us, O Lord! not unto us, but to thy name give glory. 7.
Let the reputation which David obtained, not only in his kingdom, but
among his neighbours, be looked upon as a type and figure of the exalted
honour of the Son of David (v. 17): The fame of David went out into all
lands; he was generally talked of, and admired by all people, and the
Lord brought the fear of him upon all nations. All looked upon him as a
formidable enemy and a desirable ally. Thus has God highly exalted our
Redeemer, and given him a name above every name.
