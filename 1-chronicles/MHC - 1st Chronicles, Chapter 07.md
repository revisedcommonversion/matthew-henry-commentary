1st Chronicles, Chapter 7
=========================

Commentary
----------

In this chapter we have some account of the genealogies, `I.` Of Issachar
(v. 1-5). `II.` Of Benjamin (v. 6-12). `III.` Of Naphtali (v. 13). `IV.` Of
Manasseh (v. 14-19). `V.` Of Ephraim (v. 20-29). `VI.` Of Asher (v. 30-40).
Here is no account either of Zebulun or Dan. Why they only should be
omitted we can assign no reason; only it is the disgrace of the tribe of
Dan that idolatry began in that colony of the Danites which fixed in
Laish, and called Dan, and there one of the golden calves was set up by
Jeroboam. Dan is omitted, Rev. 7.

### Verses 1-19

We have here a short view given us,

`I.` Of the tribe of Issachar, whom Jacob had compared to a strong ass,
couching between two burdens (Gen. 49:14), an industrious tribe, that
minded their country business very closely and rejoiced in their tents,
Deu. 33:18. And here it appears, 1. That they were a numerous tribe; for
they had many wives. So fruitful their country was that they saw no
danger of over-stocking the pasture, and so ingenious the people were
that they could find work for all hands. Let no people complain of their
numbers, provided they suffer none to be idle. 2. That they were a
valiant tribe, men of might (v. 2, 5), chief men, v. 3. Those that were
inured to labour and business were of all men the fittest to serve their
country when there was occasion, The number of the respective families,
as taken in the days of David, is here set down, amounting in the whole
to above 145,000 men fit for war. The account, some think, was taken
when Joab numbered the people, 2 Sa. 24. But I rather think it refers to
some other computation that was made, perhaps among themselves, because
it is said (1 Chr. 27:24) that that account was not inserted in the
chronicles of king David, it having offended God.

`II.` Of the tribe of Benjamin. Some account is here given of this tribe,
but much larger in the next chapter. The militia of this tribe scarcely
reached to 60,000; but they are said to be mighty men of valour, v. 7,
9, 11. Benjamin shall ravin as a wolf, Gen. 49:27. It was the honour of
this tribe that it produced Saul the first king, and more its honour
that it adhered to the rightful kings of the house of David when the
other tribes revolted. Here is mention (v. 12) of Hushim the sons of
Aher. The sons of Dan are said to be Hushim (Gen. 46:23), and therefore
some read Aher appellatively, Hushim-the sons of another (that is,
another of Jacob\'s sons) or the sons of a stranger, which Israelites
should not be, but such the Danites were when they set up Micah\'s
graven and molten image among them.

`III.` Of the tribe of Naphtali, v. 13. The first fathers only of that
tribe are named, the very same that we shall find, Gen. 46:24, only that
Shillem there is Shallum here. None of their descendents are named,
perhaps because their genealogies were lost.

`IV.` Of the tribe of Manasseh, that part of it which was seated within
Jordan; for of the other part we had some account before, ch. 5:23, etc.
Of this tribe observe, 1. That one of them married an Aramitess, that
is, a Syrian, v. 14. This was during their bondage in Egypt, so early
did they begin to mingle with the nations. 2. That, though the father
married a Syrian, Machir, the son of that marriage, perhaps seeing the
inconvenience of it in his father\'s house, took to wife a daughter of
Benjamin, v. 15. It is good for the children to take warning by their
father\'s mistakes and not stumble at the same stone. 3. Here is mention
of Bedan (v, 17), who perhaps is the same with that Bedan who is
mentioned as one of Israel\'s deliverers, 1 Sa. 12:11. Jair perhaps, who
was of Manasseh (Jdg. 10:3), was the man.

### Verses 20-40

We have here an account,

`I.` Of the tribe of Ephraim. Great things we read of that tribe when it
came to maturity. Here we have an account of the disasters of its
infancy, while it was in Egypt as it should seem; for Ephraim himself
was alive when those things were done, which yet is hard to imagine if
it were, as is here computed, seven generations off. Therefore I am apt
to think that either it was another Ephraim or that those who were slain
were the immediate sons of that Ephraim that was the son of Joseph. In
this passage, which is related here only, we have, 1. The great breach
that was made upon the family of Ephraim. The men of Gath, Philistines,
giants, slew many of the sons of that family, because they came down to
take away their cattle, v. 21. It is uncertain who were the aggressors
here. Some make the men of Gath the aggressors, men born in the land of
Egypt, but now resident in Gath, supposing that they came down into the
land of Goshen, to drive away the Ephraimites\' cattle, and slew the
owners, because they stood up in the defence of them. Many a man\'s life
has been exposed and betrayed by his wealth; so far is it from being a
strong city. Others think that the Ephraimites made a descent upon the
men of Gath to plunder them, presuming that the time had come when they
should be put in possession of Canaan; but they paid dearly for their
rashness and precipitation. Those that will not wait God\'s time cannot
expect God\'s blessing. I rather think that the men of Gath came down
upon the Ephraimites, because the Israelites in Egypt were shepherds,
not soldiers, abounded in cattle of their own, and therefore were not
likely to venture their lives for their neighbours\' cattle: and the
words may be read, The men of Gath slew them, for they came down to take
away their cattle. Zabad the son of Ephraim, and Shuthelah, and Ezer,
and Elead (his grandchildren), were, as Dr. Lightfoot thinks, the men
that were slain. Jacob had foretold that the seed of Ephraim should
become a multitude of nations (Gen. 48:19), and yet that plant is thus
nipped in the bud. God\'s providences often seem to contradict his
promises; but, when they do so, they really magnify the promise, and
make the performance of it, notwithstanding, so much more illustrious.
The Ephraimites were the posterity of Joseph, and yet his power could
not protect them, though some think he was yet living. The sword devours
one as well as another. 2. The great grief which oppressed the father of
the family hereupon: Ephraim mourned many days. Nothing brings the aged
to the grave with more sorrow than their following the young that
descend from them to the grave first, especially if in blood. It is
often the burden of those that live to be old that they see those go
before them of whom they said, These same shall comfort us. It was a
brotherly friendly office which his brethren did, when they came to
comfort him under this great affliction, to express their sympathy with
him and concern for him, and to suggest that to him which would support
and quiet him under this sad providence. Probably they reminded him of
the promise of increase which Jacob had blessed him when he laid his
right hand upon his head. Although his house was not so with God as he
hoped, but a house of mourning, a shattered family, yet that promise was
sure, 2 Sa. 23:5. 3. The repair of this breach, in some measure, by
addition of another son to his family in his old age (v. 23), like Seth,
another seed instead of that of Abel whom Cain slew, Gen. 4:25. When God
thus restores comfort to his mourners, makes glad according to the days
wherein he afflicted, setting the mercies over against the crosses, we
ought therein to take notice of the kindness and tenderness of divine
Providence; it is as if it repented God concerning his servants, Ps.
90:13, 15. Yet joy that a man was born into his family could not make
him forget his grief; for he gives a melancholy name to his son,
Beriah-in trouble, for he was born when the family was in mourning, when
it went evil with his house. It is good to have in remembrance the
affliction and the misery, the wormwood and the gall, that our souls may
be humbled within us, Lam. 3:19, 20. What name more proper for man that
is born of a woman than Beriah, because born into a troublesome world?
It is added, as a further honour to the house of Ephraim, `(1.)` That a
daughter of that tribe, Sherah by name, at the time of Israel\'s setting
in Canaan, built some cities, either at her own charge or by her own
care; one of them bore her name, Uzzen-sherah, v. 24. A virtuous woman
may be as great an honour and blessing to a family as a mighty man. `(2.)`
That a son of that tribe was employed in the conquest of Canaan, Joshua
the son of Nun, v. 27. In this also the breach made on Ephraim\'s family
was further repaired; and perhaps the resentment of this injury formerly
done by the Canaanites to the Ephraimites might make him more vigorous
in the war.

`II.` Of the tribe of Asher. Some men of note of that tribe are here
named. Their militia was not numerous in comparison with some other
tribes, only 26,000 men in all; but their princes were choice and mighty
men of valour, chief of the princes (v. 40), and perhaps it was their
wisdom that they coveted not to make their trained bands numerous, but
rather to have a few, and those apt to the war and serviceable men.
