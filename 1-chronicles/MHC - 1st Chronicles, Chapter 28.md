1st Chronicles, Chapter 28
==========================

Commentary
----------

The account we have of David\'s exit, in the beginning of the first book
of Kings, does not make his sun nearly so bright as that given in this
and the following chapter, where we have his solemn farewell both to his
son and his subjects, and must own that he finished well. In this
chapter we have, `I.` A general convention of the states summoned to meet
(v. 1). `II.` A solemn declaration of the divine entail both of the crown
and of the honour of building the temple upon Solomon (v. 2-7). `III.` An
exhortation both to the people and to Solomon to make religion their
business (v. 8-10). `IV.` The model and materials delivered to Solomon for
the building of the temple (v. 11-19). `V.` Encouragement given him to
undertake it and proceed in it (v. 20, 21).

### Verses 1-10

A great deal of service David had done in his day, had served his
generation according to the will of God, Acts 13:36. But now the time
draws night that he must die, and, as a type of the Son of David, the
nearer he comes to his end the more busy he is, and does his work with
all his might. He is now a little recovered from the indisposition
mentioned 1 Ki. 1:1, when they covered him with clothes, and he got no
heat: but was cure is there for old age? He therefore improves his
recovery, as giving him an opportunity of doing God and his country a
little more service.

`I.` He summoned all the great men to attend him, that he might take leave
of them all together, v. 1. Thus Moses did (Deu. 31:28), and Joshua, ch.
23:2; 24:1. David would not declare the settlement of the crown but in
the presence, and to the satisfaction, of those that were the
representatives of the people.

`II.` He addressed them with a great deal of respect and tenderness. He
not only exerted himself to rise from his bed, to give them the meeting
(the occasion putting new spirits into him), but he rose out of his
chair, and stood up upon his feet (v. 2), in reverence to God whose will
he was to declare, and in reverence to this solemn assembly of the
Israel of God, as if he looked upon himself, though major
singulis-greater than any individual among them, yet minor
universis-less than the whole of them together. His age and infirmities,
as well as his dignity, might well have allowed him to keep his seat;
but he would show that he was indeed humbled for the pride of his heart
both in the numbers of his people and his dominion over them. It had
been too much his pleasure that they were all his servants (ch. 21:3),
but now he calls them his brethren, whom he loved, his people, whom he
took care of, not his servants, whom he had command of: Hear me, my
brethren, and my people. It becomes superiors thus to speak with
affection and condescension even to their inferiors; they will not be
the less honoured for it, but the more beloved. Thus he engages their
attention to what he was about to say.

`III.` He declared the purpose he had formed to build a temple for God,
and God\'s disallowing that purpose, v. 2, 3. This he had signified to
Solomon before, ch. 22:7, 8. A house of rest for the ark is here said to
be a house of rest for the footstool of our God; for heaven is his
throne of glory; the earth, and the most magnificent temples that can be
built upon it, are but his footstool: so much difference is there
between the manifestations of the divine glory in the upper and lower
world. Angels surround his throne, Isa. 6:1. We poor worms do but
worship at his footstool Ps. 99:5; 132:7. As an evidence of the
sincerity of his purpose to build the temple, he tells them that he had
made ready for it, but that God would not suffer him to proceed because
he had appointed other work for him to do, which was enough for one man,
namely, the managing of the wars of Israel. He must serve the public
with the sword; another must do it with the line and plummet. Times of
rest are building times, Acts 9:31.

`IV.` He produced his own title first, and then Solomon\'s, to the crown;
both were undoubtedly jure divino-divine. They could make out such a
title as no monarch on earth can; the Lord God of Israel chose them both
immediately, by prophecy, not providence, v. 4, 5. No right of
primogeniture is pretended. Detur digniori, non seniori-It went by
worth, not by age. 1. Judah was not the eldest son of Jacob, yet God
chose that tribe to be the ruling tribe; Jacob entailed the sceptre upon
it, Gen. 49:10. 2. It does not appear that the family of Jesse was the
senior house of that tribe; from Judah it is certain that it was not,
for Shelah was before Pharez; whether from Nahshon and Salmon is not
certain. Ram, the father of Nahshon, had a elder brother, 1 Chr. 2:9.
Perhaps so had Boaz, Obed, and Jesse. Yet \"God chose the house of my
father.\" 3. David was the youngest son of Jesse, yet God liked him to
make him king; so it seemed good unto him. God takes whom he likes, and
likes whom he makes like himself, as he did David, a man after his own
heart. 4. Solomon was one of the youngest sons of David, and yet God
chose him to sit upon the throne, because he was the likeliest of them
all to build the temple, the wisest and best inclined.

`V.` He opened to them God\'s gracious purposes concerning Solomon (v. 6,
7): I have chosen him to be my son. Thus he declares the decree, that
the Lord had said to Solomon, as a type of Christ, Thou art my son (Ps.
2:7), the son of my love; for he was called Jedidiah, because the Lord
loved him, and Christ is his beloved Son. Of him God said, as a figure
of him that was to come, 1. He shall build my house. Christ is both the
founder and the foundation of the gospel temple. 2. I will establish his
kingdom for ever. This must have its accomplishment in the kingdom of
the Messiah, which shall continue in his hands through all the ages of
time (Isa. 9:7; Lu. 1:33) and shall then be delivered up to God, even
the Father, yet perhaps to be delivered back to the Redeemer for ever.
As to Solomon, this promise of the establishment of his kingdom is here
made conditional: If he be constant to do my commandments, as at this
day. Solomon was now very towardly and good: \"If he continue so, his
kingdom shall continue, otherwise not.\" Note, If we be constant to our
duty, then, and not otherwise, we may expect the continuance of God\'s
favour. Let those that are well taught, and begin well, take notice of
this-if they be constant, they are happy; perseverance wears the crown,
though it wins it not.

`VI.` He charged them to adhere stedfastly to God and their duty, v. 8.
Observe, 1. The matter for this charge: Keep, and seek for all the
commandments of the Lord your God. The Lord was their God; his
commandments must be their rule; they must have respect to them all,
must make conscience of keeping them, and, in order thereunto, must seek
for them, that is, must be inquisitive concerning their duty, search the
scriptures, take advice, seek the law at the mouth of those whose lips
were to keep this knowledge, and pray to God to teach and direct them.
God\'s commandments will not be kept without great care. 2. The
solemnity of it. He charged them in the sight of all Israel, who would
all have notice of this public charge, and in the audience of their God.
\"God is witness, and this congregation is witness, that they have good
counsel given them, and fair warning; if they do not take it, it is
their fault, and God and man will be witnesses against them.\" See 1
Tim. 5:21; 2 Tim. 4:1. Those that profess religion, as they tender the
favour of God and their reputation with men, must be faithful to their
profession. 3. The motive to observe this charge. It was the way to be
happy, to have the peaceable possession of this good land themselves and
to preserve the entail of it upon their children.

`VII.` He concluded with a charge to Solomon himself, v. 9, 10. He was
much concerned that Solomon should be religious. He was to be a great
man, but he must not think religion below him-a wise man, and this would
be his wisdom. Observe,

`1.` The charge he gives him. He must look upon God and the God of his
father, his good father, who had devoted him to God and educated him for
God. He was born in God\'s house and therefore bound in duty to be his,
brought up in his house and therefore bound in gratitude. Thy own
friend, and thy father\'s friend, forsake not. He must know God and
serve him. We cannot serve God aright if we do not know him; and in vain
do we know him if we do not serve him, serve him with heart and mind. We
make nothing of religion if we do not mind it, and make heart-work of
it. Serve him with a perfect, that is, an upright heart (for sincerity
is our gospel perfection), and with a willing mind, from a principle of
love, and as a willing people, cheerfully and with pleasure.

`2.` The arguments to enforce this charge.

`(1.)` Two arguments of general inducement:-`[1.]` That the secrets of our
souls are open before God; he searches all hearts, even the hearts of
kings, which to men are unsearchable, Prov. 25:3. We must therefore be
sincere, because, if we deal deceitfully, God sees it, and cannot be
imposed upon; we must therefore employ our thoughts, and engage them in
God\'s service, because he fully understands all the imaginations of
them, both good and bad. `[2.]` That we are happy or miserable here, and
for ever, according as we do, or do not, serve God. If we seek him
diligently, he will be found of us, and that is enough to make us happy,
Heb. 11:6. If we forsake him, desert his service and turn from following
him, he will cast us off for ever, and that is enough to make us
miserable. Note, God never casts any off till they have first cast him
off. Here is,

`(2.)` One argument peculiar to Solomon (v. 10): \"Thou art to build a
house for the sanctuary; therefore seek and serve God, that that work
may be done from a good principle, in a right manner, and may be
accepted.\"

`3.` The means prescribed in order hereunto, and they are prescribed to
us all. `(1.)` Caution: Take heed; beware of every thing that looks like,
or leads to, that which is evil. `(2.)` Courage: Be strong, and do it. We
cannot do our work as we should unless we put on resolution, and fetch
in strength from divine grace.

### Verses 11-21

As for the general charge that David gave his son to seek God and serve
him, the book of the law was, in that, his only rule, and there needed
no other; but, in building the temple, David was now to give him three
things:-1. A model of the building, because it was to be such a building
as neither he nor his architects ever saw. Moses had a pattern of the
tabernacle shown him in the mount (Heb. 8:5), so had David of the
temple, by the immediate hand of God upon him, v. 19. It was given him
in writing, probably by the ministry of an angel, or as clearly and
exactly represented to his mind as if it had been in writing. But it is
said (v. 12), He had this pattern by the Spirit. The contrivance either
of David\'s devotion or of Solomon\'s wisdom must not be trusted to in
an affair of this nature. The temple must be a sacred thing and a type
of Christ; there must be in it not only convenience and decency, but
significancy: it was a kind of sacrament, and therefore it must not be
left to man\'s art or invention to contrive it, but must be framed by
divine institution. Christ the true temple, the church the gospel
temple, and heaven the everlasting temple, are all framed according to
the divine councils, and the plan laid in the divine wisdom, ordained
before the world for God\'s glory and ours. This pattern David gave to
Solomon, that he might know what to provide and might go by a certain
rule. When Christ left with his disciples a charge to build his gospel
church he gave them an exact model of it, ordering them to observe that,
and that only, which he commanded. The particular models are here
mentioned, of the porch, which was higher than the rest, like a
steeple,-then the houses, both the holy place and the most holy, with
the rooms adjoining, which were for treasuries, chambers, and
parlours,-especially the place of the mercy-seat (v. 11),-of the courts
likewise, and the chambers about them, in which the dedicated things
were laid up. Bishop Patrick supposes that, among other things, the
tabernacle which Moses reared and all the utensils of it, which there
was now no further occasion for, were laid up here, signifying that in
the fulness of time all the Mosaic economy, all the rites and ceremonies
of that dispensation, should be respectfully laid aside, and something
better come in their room. He gave him a table of the courses of the
priests, patterns of the vessels of service (v. 13), and a pattern of
the chariot of the cherubim, v. 18. Besides the two cherubim over the
mercy-seat, there were two much larger, whose wings reached from wall to
wall (1 Ki. 6:23, etc.), and of these David here gave Solomon the
pattern, called a chariot; for the angels are the chariots of God, Ps.
68:17. 2. Materials for the most costly of the utensils of the temple.
That they might not be made any less than the patterns, he weighed out
the exact quantity for each vessel both of gold and silver, v. 14. In
the tabernacle there was but one golden candlestick; in the temple there
were ten (1 Ki. 7:49), besides silver ones, which, it is supposed, were
hand-candlesticks, v. 15. In the tabernacle there was but one table; but
in the temple, besides that on which the show-bread was set, there were
ten others for other uses (2 Chr. 4:8), besides silver tables; for, this
house being much larger than that, it would look bare if it had not
furniture proportionable. The gold for the altar of incense is
particularly said to be refined gold (v. 18), purer than any of the
rest; for that was typical of the intercession of Christ, than which
nothing is more pure and perfect. 3. Directions which way to look for
help in this great undertaking. \"Fear not opposition; fear not the
charge, care, and trouble; fear not miscarrying in it, as in the case of
Uzza; fear not the reproach of the foolish builder, that began to build
and was not able to finish. Be not dismayed. `(1.)` God will help thee,
and thou must look up to him in the first place (v. 20): The Lord God,
even my God, whom I have chosen and served, who has all along been
present with me and prospered me, and to whom, from my own experience of
his power and goodness, I recommend thee, he will be with thee, to
direct, strengthen, and prosper thee; he will not fail thee nor forsake
thee.\" Note, We may be sure that God, who owned our fathers and carried
them through the services of their day, will, in like manner, if we be
faithful to him, go along with us in our day, and will never leave us,
while he has any work to do in us or by us. The same that was Joshua\'s
encouragement (Jos. 1:5), and Solomon\'s, is given to all believers,
Heb. 13:5. He will never leave thee, nor forsake thee. God never leaves
any unless they first leave him. `(2.)` \"Good men will help thee, v. 21.
The priests and Levites will advise thee, and thou mayest consult them.
Thou hast good workmen, who are both willing and skilful;\" and these
are two very good properties in a workman, especially in those that work
at the temple. And, lastly, \"The princes and the people will be so far
from opposing or retarding the work that they will be wholly at thy
command, every one in his place ready to further it.\" Then good work is
likely to go on when all parties concerned are hearty in it, and none
secretly clog it, but all drive on heartily in it.
