1st Chronicles, Chapter 8
=========================

Commentary
----------

We had some account given us of Benjamin in the foregoing chapter; here
we have a larger catalogue of the great men of that tribe. 1. Because of
that tribe Saul came, the first king of Israel, to the story of whom the
sacred writer is hastening, 10:1. 2. Because that tribe clave to Judah,
inhabited much of Jerusalem, was one of the two tribes that went into
captivity, and returned back; and that story also he has an eye to, 9:1.
Here is, `I.` Some of the heads of that tribe named (v. 1-32). `II.` A more
particular account of the family of Saul (v. 33-40).

### Verses 1-32

There is little or nothing of history in all these verses; we have not
therefore much to observe. 1. As to the difficulties that occur in this
and the foregoing genealogies we need not perplex ourselves. I presume
Ezra took them as he found them in the books of the kings of Israel and
Judah (ch. 9:1), according as they were given in by the several tribes,
each observing what method they thought fit. Hence some ascend, others
desecnd; some have numbers affixed, others places; some have historical
remarks intermixed, others have not; some are shorter, others longer;
some agree with other records, others differ; some, it is likely, were
torn, erased, and blotted, others more legible. Those of Dan and Reuben
were entirely lost. This holy man wrote as he was moved by the Holy
Ghost; but there was no necessity for the making up of the defects, no,
nor for the rectifying of the mistakes, of these genealogies by
inspiration. It was sufficient that he copied them out as they came into
his hand, or so much of them as was requisite to the present purpose,
which was the directing of the returned captives to settle as nearly as
they could with those of their own family, and in the places of their
former residence. We may suppose that many things in these genealogies
which to us seem intricate, abrupt, and perplexed, were plain and easy
to them then (who knew how to fill up the deficiencies) and abundantly
answered the intention of the publishing of them. 2. Many great and
mighty nations there were now in being upon earth, and many illustrious
men in them, whose names are buried in perpetual oblivion, while the
names of multitudes of the Israel of God are here carefully preserved in
everlasting remembrance. They are Jasher, Jeshurun-just ones, and the
memory of the just is blessed. Many of these we have reason to fear,
came short of everlasting honour (for even the wicked kings of Judah
come into the genealogy), yet the perpetuating of their names here was a
figure of the writing of the names of all God\'s spiritual Israel in the
Lamb\'s book of life. 3. This tribe of Benjamin was once brought to a
very low ebb, in the time of the judges, upon the occasion of the
iniquity of Gibeah, when only 600 men escaped the sword of justice; and
yet, in these genealogies, it makes as good a figure as almost any of
the tribes: for it is the honour of God to help the weakest and raise up
those that are most diminished and abased. 4. Here is mention of one
Ehud (v. 6), in the preceding verse of one Gera (v. 5) and (v. 8) of one
that descended from him, that begat children in the country of Moab,
which inclines me to think it was that Ehud who was the second of the
judges of Israel; for he is said to be the son of Gera and a Benjamite
(Jdg. 3:15), and he delivered Israel from the oppression of the Moabites
by killing the king of Moab, which might give him a greater sway in the
country of Moab than we find evidence of in his history and might
occasion some of his posterity to settle there. 5. Here is mention of
some of the Benjamites that drove away the inhabitants of Gath (v. 13),
perhaps those that had slain the Ephraimites (ch. 7:21) or their
posterity, by way of reprisal: and one of those that did this piece of
justice was named Beriah too, that name in which the memorial of that
injury was preserved. 6. Particular notice is taken of those that dwelt
in Jerusalem (v. 28 and again v. 32), that those whose ancestors had had
their residence there might thereby be induced, at their return from
captivity, to settle there too, which, for aught that appears, few were
willing to do, because it was the post of danger: and therefore we find
(Neh. 11:2) the people blessed those that willingly offered themselves
to dwell at Jerusalem, the greater part being inclined to prefer the
cities of Judah. Those whose godly parents had their conversation in the
new Jerusalem should thereby be engaged to set their faces thitherward
and pursue the way thither, whatever it cost them.

### Verses 33-40

It is observable that among all the genealogies of the tribes there is
no mention of any of the kings of Israel after the defection from the
house of David, much less of their families; not a word of Jeroboam\'s
house or Baasha\'s, of Umri\'s or Jehu\'s; for they were all idolaters.
But of the family of Saul, which was the royal family before the
elevation of David, we have here a particular account. 1. Before Saul,
Kish and Ner only are named, his father and grandfather, v. 33. His
pedigree is carried higher 1 Sa. 9:1, only there Kish is said to be the
son of Abiel, here of Ner. He was in truth the son of Ner but the
grandson of Abiel, as appears by 1 Sa. 14:51, where it is said that Ner
was the son of Abiel, and that Abner, who was the son of Ner, was
Saul\'s uncle (that is, his father\'s brother); therefore his father was
also the son of Ner. It is common in all languages to put sons for
grandsons and other descendents, much more in the scanty language of the
Hebrews. 2. After Saul, divers of his sons are named, but the posterity
of none of them, save Jonathan only, who was blessed with numerous issue
and those honoured with a place in the sacred genealogies for the sake
of his sincere kindness to David. The line of Jonathan is drawn down
here for about ten generations. Perhaps David was, in a particular
manner, careful to preserve that, and assigned it a page by itself,
because of the covenant made between his seed and Jonathan\'s seed
forever, 1 Sa. 20:15, 23, 42. This genealogy ends in Ulam, whose family
became famous in the tribe of Benjamin for the number of its valiant
men. Of that one man\'s posterity there were, as it should seem, at one
time, 150 archers brought into the field of battle, that were mighty men
of valour, v. 40. That is taken notice of concerning them which is more
a man\'s praise than his pomp or wealth is, that they were qualified to
serve their country.
