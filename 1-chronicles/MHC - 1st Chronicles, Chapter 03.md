1st Chronicles, Chapter 3
=========================

Commentary
----------

Of all the families of Israel none was so illustrious as the family of
David. That is the family which was mentioned in the foregoing chapter
(v. 15). Here we have a full account of it. `I.` David\'s sons (v. 1-9).
`II.` His successors in the throne as long as the kingdom continued (v.
10-16). `III.` The remains of his family in and after the captivity (v.
17-24). From this family, \"as concerning the flesh, Christ came.\"

### Verses 1-9

We had an account of David\'s sons, 2 Sa. 3:2, etc., and 5:14, etc. 1.
He had many sons; and no doubt wrote as he thought, Ps. 127:5. Happy is
the man that hath his quiver full of these arrows. 2. Some of them were
a grief to him, as Amnon, Absalom, and Adonijah; and we do not read of
any of them that imitated his piety or devotion except Solomon, and he
came far short of it. 3. One of them, which Bath-sheba bore to him, he
called Nathan, probably in honour of Nathan the prophet, who reproved
him for his sin in that matter and was instrumental to bring him to
repentance. It seems he loved him the better for it as long as he lived.
It is wisdom to esteem those our best friends that deal faithfully with
us. From this son of David our Lord Jesus descended, as appears Lu.
3:31. 4. Here are two Elishamas, and two Eliphelets, v. 6, 8. Probably
the two former were dead, and therefore David called two more by their
names, which he would not have done if there had been any ill omen in
this practice as some fancy. 5. David had many concubines; but their
children are not named, as not worthy of the honour (v. 9), the rather
because the concubines had dealt treacherously with David in the affair
of Absalom. 6. Of all David\'s sons Solomon was chosen to succeed him,
perhaps not for any personal merits (his wisdom was God\'s gift), but
so, Father, because it seemed good unto thee.

### Verses 10-24

David having nineteen sons, we may suppose them to have raised many
noble families in Israel whom we never hear of in the history. But the
scripture gives us an account only of the descendants of Solomon here,
and of Nathan, Lu. 3. The rest had the honour to be the sons of David;
but these only had the honour to be related to the Messiah. The sons of
Nathan were his fathers as man, the sons of Solomon his predecessors as
king. We have here, 1. The great and celebrated names by which the line
of David is drawn down to the captivity, the kings of Judah in a lineal
succession, the history of whom we have had at large in the two books of
Kings and shall meet with again in the second book of Chronicles. Seldom
has a crown gone in a direct line from father to son for seventeen
descents together, as here. This was the recompence of David\'s piety.
About the time of the captivity the lineal descent was interrupted, and
the crown went from one brother to another and from a nephew to an
uncle, which was a presage of the eclipsing of the glory of that house.
`2.` The less famous, and most of them very obscure, names, in which the
house of David subsisted after the captivity. The only famous man of
that house that we meet with at their return from captivity was
Zerubbabel, elsewhere called the son of Salathiel, but appearing here to
be his grandson (v. 17-19), which is usual in scripture. Belshazzar is
called Nebuchadnezzar\'s son, but was his grandson. Salathiel is said to
be the son of Jeconiah because adopted by him, and because, as some
think, he succeeded him in the dignity to which he was restored by
Evil-merodach. Otherwise Jeconiah was written childless: he was the
signet God plucked from his right hand (Jer. 22:24), and in his room
Zerubbabel was placed, and therefore God saith to him (Hag. 2:23), I
will make thee as a signet. The posterity of Zerubbabel here bear not
the same names that they do in the genealogies (Mt. 1, or Lu. 3), but
those no doubt were taken from the then herald\'s office, the public
registers which the priests kept of all the families of Judah,
especially that of David. The last person named in this chapter is
Anani, of whom bishop Patrick says that the Targum adds these words, He
is the king Messiah, who is to be revealed, and some of the Jewish
writers give this reason, because it is said (Dan. 7:13), the son of man
came gnim gnanani-with the clouds of heaven. The reason indeed is very
foreign and far-fetched; but that learned man thinks it may be made use
of as an evidence that their minds were always full of the thoughts of
the Messiah and that they expected it would not be very long after the
days of Zerubbabel before the set time of his approach would come.
