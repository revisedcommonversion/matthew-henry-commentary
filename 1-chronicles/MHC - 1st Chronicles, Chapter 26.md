1st Chronicles, Chapter 26
==========================

Commentary
----------

We have here an account of the business of the Levites. That tribe had
made but a very small figure all the time of the judges, till Eli and
Samuel appeared. But when David revived religion the Levites were, of
all men, in the greatest reputation. And happy it was that they had
Levites who were men of sense, fit to support the honour of their tribe.
We have here an account, `I.` Of the Levites that were appointed to be
porters (v. 1-19). `II.` Of those that were appointed to be treasurers and
storekeepers (v. 20-28). `III.` Of those that were officers and judges in
the country, and were entrusted with the administration of public
affairs (v. 29-32).

### Verses 1-19

Observe, `I.` There were porters appointed to attend the temple, who
guarded all the avenues that let to it, opened and shut all the outer
gates and attended at them, not only for the state, but for service, to
direct and instruct those who were going to worship in the courts of the
sanctuary in the decorum they were to observe, to encourage those that
were timorous, to send back the strangers and unclean, and to guard
against thieves and others that were enemies to the house of God. In
allusion to this office, ministers are said to have the keys to the
kingdom of heaven committed to them (Mt. 16:19), that they may admit,
and exclude, according to the law of Christ.

`II.` Of several of those that were called to this service, it is taken
notice of that they were mighty men of valour (v. 6), strong men (v. 7),
able men (v. 8), and one of them that he was a wise counsellor (v. 14),
who probably, when he had used this office of a deacon well and given
proofs of more than ordinary wisdom, purchased to himself a good degree,
and was preferred from the gate to the council-board, 1 Tim. 3:13. As
for those that excelled in strength of body, and courage and resolution
of mind, they were thereby qualified for the post assigned them; for
whatever service God calls men to he either finds them fit or makes them
so.

`III.` The sons of Obed-edom were employed in this office, sixty-two of
that family. This was he that entertained the ark with reverence and
cheerfulness; and see how he was rewarded for it. 1. He had eight sons
(v. 5), for God blessed him. The increase and building up of families
are owing to the divine blessing; and a great blessing it is to a family
to have many children, when like these they are able for, and eminent
in, the service of God. 2. His sons were preferred to places of trust in
the sanctuary. They had faithfully attended the ark in their own house,
and now were called to attend it in God\'s house. He that is trusty in
little shall be trusted with more. He that keeps God\'s ordinances in
his own tent is fit to have the custody of them in God\'s tabernacle, 1
Tim. 3:4, 5. I have kept thy law, says David, and this I had because I
kept thy precepts, Ps. 119:55, 56.

`IV.` It is said of one here that though he was not the first-born his
father made him the chief (v. 10), either because he was very excellent,
or because the elder son was very weak. He was made chief, perhaps not
in inheriting the estate (for that was forbidden by the law, Deu. 21:16,
17), but in this service, which required personal qualifications.

`V.` The porters, as the singers, had their post assigned them by lot, so
many at such a gate, and so many at such a one, that every one might
know his post and make it good, v. 13. It is not said that they were
cast into twenty-four courses, as before; but here are the names of
about twenty-four (v. 1-11), and the posts assigned are twenty-four, v.
17, 18. We have therefore reason to think they were distributed into as
many companies. Happy are those who dwell in God\'s house: for, as they
are well fed, well taught, and well employed, so they are well guarded.
Men attended at the gates of the temple, but angels attend at the gates
of the New Jerusalem, Rev. 21:12.

### Verses 20-28

Observe, 1. There were treasures of the house of God. A great house
cannot be well kept without stores of all manner of provisions. Much was
expended daily upon the altar-flour, wine, oil, salt, fuel, besides the
lamps; quantities of these were to be kept beforehand, besides the
sacred vestments and utensils. These were the treasures of the house of
God. And, because money answers all things, doubtless they had an
abundance of it, which was received from the people\'s offerings,
wherewith they bought in what they had occasion for. And perhaps much
was laid up for an exigence. These treasures typified the plenty there
is in our heavenly Father\'s house, enough and to spare. In Christ, the
true temple, are hid treasures of wisdom and knowledge, and unsearchable
riches. 2. There were treasures of dedicated things, dedicated mostly
out of the spoils won in battle (v. 27), as a grateful acknowledgment of
the divine protection. Abraham gave Melchisedec the tenth of the spoils
Heb. 7:4. In Moses\'s time the officers of the army, when they returned
victorious, brought of their spoils an oblation to the Lord, Num. 31:50.
Of late this pious custom had been revived; and not only Samuel and
David, but Saul, and Abner, and Joab, had dedicated of their spoils to
the honour and support of the house of God, v. 28. Note, The more God
bestows upon us the more he expects from us in works of piety and
charity. Great successes call for proportionable returns. When we look
over our estates we should consider, \"Here are convenient things, rich
things, it may be, and fine things; but where are the dedicated
things?\" Men of war must honour God with their spoils. 3. These
treasures had treasurers, those that were over them (v. 20, 26), whose
business it was to keep them, that neither moth nor rust might corrupt
them, nor thieves break through and steal, to give out as there was
occasion and to see that they were not wasted, embezzled, or alienated
to the common use; and it is probable that they kept accounts of all
that was brought in and how it was laid out.

### Verses 29-32

All the offices of the house of God being well provided with Levites, we
have here an account of those that were employed as officers and judges
in the outward business, which must not be neglected, no, not for the
temple itself. The magistracy is an ordinance of God for the good of the
church as truly as the ministry is. And here we are told, 1. That the
Levites were employed in the administration of justice in concurrence
with the princes and elders of the several tribes, who could not be
supposed to understand the law so well as the Levites, who made it their
business to study it. None of those Levites who were employed in the
service of the sanctuary, none of the singers or porters, were concerned
in this outward business; either one was enough to engage the whole man
or it was presumption to undertake both. 2. Their charge was both in all
business of the Lord, and in the service of the kings, v. 30 and again
v. 32. They managed the affairs of the country, as well ecclesiastical
as civil, took care both of God\'s tithes and the king\'s taxes,
punished offences committed immediately against God and his honour and
those against the government and the public peace, guarded both against
idolatry and against injustice, and took care to put the laws in
execution against both. Some, it is likely, applied themselves to the
affairs of religion, others to secular affairs; and so, between both,
God and the king were well served. It is happy with a kingdom when its
civil and sacred interests are thus interwoven and jointly minded and
advanced. 3. There were more Levites employed as judges with the two
tribes and a half on the other side of Jordan than with all the rest of
the tribes; there were 2700; whereas as the west side of Jordan there
were 1700, v. 30, 32. Either those remote tribes were not so well
furnished as the rest with judges of their own, or because they, lying
furthest from Jerusalem and on the borders of the neighbouring nations,
were most in danger of being infected with idolatry, and most needed the
help of Levites to prevent it. The frontiers must be well guarded. 4.
This is said to be done (as were all the foregoing settlements) in the
fortieth year of the reign of David (v. 31), that is, the last year of
his reign. We should be so much the more industrious to do good as we
can see the day approaching. If we live to enjoy the fruit of our
labours, grudge it not to those that shall come after us.
