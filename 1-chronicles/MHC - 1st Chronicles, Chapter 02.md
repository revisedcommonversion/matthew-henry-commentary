1st Chronicles, Chapter 2
=========================

Commentary
----------

We have now come to what was principally intended, the register of the
children of Israel, that distinguished people, that were to \"dwell
alone, and not be reckoned among the nations.\" Here we have, `I.` The
names of the twelve sons of Israel (v. 1, 2). `II.` An account of the
tribe of Judah, which has the precedency, not so much for the sake of
David as for the sake of the Son of David, our Lord, who sprang out of
Judah, Heb. 7:14. 1. The first descendants from Judah, down to Jesse (v.
3-12). 2. The children of Jesse (v. 13-17). 3. The posterity of Hezron,
not only through Ram, from whom David came, but through Caleb (v.
18-20), Segub (v. 21-24), Jerahmeel (v. 25-33, and so to v. 41), and
more by Caleb (v. 42-49), with the family of Caleb the son of Hur (v.
50-55). The best exposition we can have of this and the following
chapters, and which will give the clearest view of them, is found in
those genealogical tables which were published with some of the first
impressions of the last English Bible about 100 years ago, and continued
for some time; and it is a pity but they were revived in some of our
later editions, for they are of great use to those who diligently search
the scriptures. They are said to be drawn up by that great master in
scripture-learning, Mr. Hugh Broughton. We meet with them sometimes in
old Bibles.

### Verses 1-17

Here is, `I.` The family of Jacob. His twelve sons are here named, that
illustrious number so often celebrated almost throughout the whole
Bible, from the first to the last book of it. At every turn we meet with
the twelve tribes that descended from these twelve patriarchs. The
personal character of several of them was none of the best (the first
four were much blemished), and yet the covenant was entailed on their
seed; for it was of grace, free grace, that it was said, Jacob have I
loved-not of works, lest any man should boast.

`II.` The family of Judah. That tribe was most praised, most increased,
and most dignified, of any of the tribes, and therefore the genealogy of
it is the first and largest of them all. In the account here given of
the first branches of that illustrious tree, of which Christ was to be
the top branch, we meet, 1. With some that were very bad. Here is Er,
Judah\'s eldest son, that was evil in the sight of the Lord, and was cut
off, in the beginning of his days, by a stroke of divine vengeance: The
Lord slew him, v. 3. His next brother, Onan, was no better, and fared no
better. Here is Tamar, with whom Judah, her father-in-law, committed
incest, v. 4. And here is Achan, called Achar-a troubler, that troubled
Israel by taking of the accursed thing, v. 7. Note, The best and most
honourable families may have those belonging to them that are blemishes.
`2.` With some that were very wise and good, as Heman and Ethan, Calcol
and Dara, who were not perhaps the immediate sons of Zerah, but
descendants from him, and are named because they were the glory of their
father\'s house; for, when the Holy Ghost would magnify the wisdom of
Solomon, he declares him wiser than these four men, who, though the sons
of Mahol, are called Ezrahites, from Zerah, 1 Ki. 4:31. That four
brothers should be eminent for wisdom and grace was a rare thing. 3.
With some that were very great, as Nahshon, who was prince of the tribe
of Judah when the camp of Israel was formed in the wilderness, and so
led the van in that glorious march, and Salman, or Salmon, who was in
that post of honour when they entered into Canaan, v. 10, 11.

`III.` The family of Jesse, of which a particularly account is kept for
the sake of David, and the Son of David, who is a rod out of the stem of
Jesse, Isa. 11:1. Hence it appears that David was a seventh son, and
that his three great commanders, Joab, Abishai, and Asahel, were the
sons of one of his sisters, and Amasa of another. Three of the four went
down slain to the pit, though they were the terror of the mighty.

### Verses 18-55

The persons mentioned in the former paragraph are most of them such as
we read of, and most of them such as we read much of, in other
scriptures; but very few of those to whom this paragraph relates are
mentioned any where else. It should seem, the tribe of Judah were more
full and exact in their genealogies than any other of the tribes, in
which we must acknowledge a special providence, for the clearing of the
genealogy of Christ. 1. Here we find Bezaleel, who was head-workman in
building the tabernacle, Ex. 31:2. 2. Hezron, who was the son of Pharez
(v. 5), was the father of all this progeny, his sons, Caleb and
Jerahmeel, being very fruitful, and he himself likewise, even in his old
age, for he left his wife pregnant when he died, v. 24. This Hezron was
one of the seventy that went down with Jacob into Egypt, Gen. 46:12.
There his family thus increased, as other oppressed families there did.
We cannot but suppose that he died during the Israelites\' bondage in
Egypt; and yet it is here said he died in Caleb-Ephratah (that is,
Bethlehem), in the land of Canaan, v. 24. Perhaps, though the body of
the people continued in Egypt, yet some that were more active than the
rest, at least before their bondage came to be extreme, visited Canaan
sometimes and got footing there, though afterwards they lost it. The
achievements of Jair, here mentioned (v. 22, 23), we had an account of
in Num. 32:41; and, it is supposed, they were long after the conquest of
Canaan. The Jews say, Hezron married his third wife when he was sixty
years old (v. 21), and another afterwards (v. 24), because he had a
great desire of posterity in the family of Pharez, from whom the Messiah
was to descend. 3. Here is mention of one that died without children (v.
30), and another (v. 32), and of one that had no sons, but daughters, v.
`34.` Let those that are in any of these ways afflicted not think their
case new or singular. Providence orders these affairs of families by an
incontestable sovereignty, as pleaseth him, giving children, or
withholding them, or giving all of one sex. He is not bound to please
us, but we are bound to acquiesce in his good pleasure. To those that
love him he will himself be better than ten sons, and give them in his
house a place and a name better than of sons and daughters. Let not
those therefore that are written childless envy the families that are
built up and replenished. Shall our eye be evil because God\'s is good?
`4.` Here is mention of one who had an only daughter, and married her to
his servant an Egyptian, v. 34, 35. If it be mentioned to his praise, we
must suppose that this Egyptian was proselyted to the Jewish religion
and that he was very eminent for wisdom and virtue, otherwise it would
not have become a true-born Israelite to match a daughter to him,
especially an only daughter. If Egyptians become converts, and servants
do worthily, neither their parentage nor their servitude should be a bar
to their preferment. Such a one this Egyptian servant might be that she
who married him might live as happily with him as if she had married one
of the rulers of her tribe. 5. The pedigree of several of these
terminates, not in a person, but in a place or country, as one is said
to be the father of Kirjath-jearim (v. 50), another of Bethlehem (v.
51), which was afterwards David\'s city, because these places fell to
their lot in the division of the land. 6. here are some that are said to
be families of scribes (v. 55), such as kept up learning in their
family, especially scripture-learning, and taught the people the good
knowledge of God. Among all these great families we are glad to find
some that were families of scribes. Would to God that all the Lord\'s
people were prophets-all the families of Israel families of scribes,
well instructed to the kingdom of heaven, and able to bring out of their
treasury things new and old!
