1st Chronicles, Chapter 16
==========================

Commentary
----------

This chapter concludes that great affair of the settlement of the ark in
the royal city, and with it the settlement of the public worship of God
during the reign of David. Here is, `I.` The solemnity with which the ark
was fixed (v. 1-6). `II.` The psalm David gave to be sung on this occasion
(v. 7-36). `III.` The settling of the stated public worship of God in
order thenceforward (v. 37-43).

### Verses 1-6

It was a glorious day when the ark of God was safely lodged in the tent
David had pitched for it. That good man had his heart much upon it,
could not sleep contentedly till it was done, Ps. 132:4, 5.

`I.` The circumstances of the ark were now, 1. Better than what they had
been. It had been obscure in a country town, in the fields of the wood;
now it was removed to a public place, to the royal city, where all might
resort to it. It had been neglected, as a despised broken vessel; now it
was attended with veneration, and God was enquired of by it. It had
borrowed a room in a private house, which it enjoyed by courtesy; now it
had a habitation of its own entirely to itself, was set in the midst of
it, and not crowded into a corner. Note, Though God\'s word and
ordinances may be clouded and eclipsed for a time, they shall at length
shine out of obscurity. Yet, 2. They were much short of what was
intended in the next reign, when the temple was to be built. This was
but a tent, a poor mean dwelling; yet this was the tabernacle, the
temple which David in his psalms often speaks of with so much affection.
David, who pitched a tent for the ark and continued steadfast to it, did
far better than Solomon, who built a temple for it and yet in his latter
end turned his back upon it. The church\'s poorest times were its
purest.

`II.` Now David was easy in his mind, the ark was fixed, and fixed near
him. Now see how he takes care, 1. That God shall have the glory of it.
Two ways he gives him honour upon this occasion:-`(1.)` By sacrifices (v.
1), burnt-offerings in adoration of his perfections, peace-offerings in
acknowledgment of his favours. `(2.)` By songs: he appointed Levites to
record this story in a song for the benefit of others, or to celebrate
it themselves by thanking and praising the God of Israel, v. 4. All our
rejoicings must express themselves in thanksgivings to him from whom all
our comforts are received. 2. That the people shall have the joy of it.
They shall fare the better for this day\'s solemnity; for he gives them
all what is worth coming for, not only a royal treat in honour of the
day (v. 3), in which David showed himself generous to his subjects, as
he had found God gracious to him (those whose hearts are enlarged with
holy joy should show it by being open-handed); but (which is far better)
he gives them also a blessing in the name of the Lord, as a father, as a
prophet, v. 2. He prayed to God for them, and commended them to his
grace. In the name of the Word of the Lord (so the Targum), the
essential eternal Word, who is Jehovah, and through whom all blessings
come to us.

### Verses 7-36

We have here the thanksgiving psalm which David, by the Spirit,
composed, and delivered to the chief musician, to be sung upon occasion
of the public entry the ark made into the tent prepared for it. Some
think he appointed this hymn to be daily used in the temple service, as
duly as the day came; whatever other psalms they sung, they must not
omit this. David had penned many psalms before this, some in the time of
his trouble by Saul. This was composed before, but was now first
delivered into the hand of Asaph, for the use of the church. It is
gathered out of several psalms (from the beginning to v. 23 is taken
from Ps. 105:1, etc.; and then v. 23 to v. 34 is the whole 96th psalm,
with little variation; v. 34 is taken from Ps. 136:1 and divers others;
and then the last two verses are taken from the close of Ps. 106), which
some think warrants us to do likewise, and make up hymns out of David\'s
psalms, a part of one and a part of another put together so as may be
most proper to express and excite the devotion of Christians. These
psalms will be best expounded in their proper places (if the Lord will);
here we take them as they are put together, with a design to thank the
Lord (v. 7), a great duty, to which we need to be excited and in which
we need to be assisted. 1. Let God be glorified in our praises; let his
honour be the centre in which all the lines meet. Let us glorify him by
our thanksgivings (Give thanks to the Lord), by our prayers (Call on his
name, v. 8), by our songs (Sing psalms unto him), by our discourse-Talk
of all his wondrous works, v. 9. Let us glorify him as a great God, and
greatly to be praised (v. 25), as supreme God (above all gods), as sole
God, for all others are idols, v. 26. Let us glorify him as most bright
and blessed in himself (Glory and honour are in his presence, v. 27), as
creator (The Lord made the heavens), as the ruler of the whole creation
(His judgments are in all the earth, v. 14), and as ours-He is the Lord
our God. Thus must we give unto the Lord the glory due to his name (v.
28, 29), and own it, and much more, his due. 2. Let other be edified and
instructed: Make known his deeds among the people (v. 8), declare his
glory among the heathen (v. 24), that those who are strangers to him may
be led into acquaintance with him, allegiance to him, and the adoration
of him. Thus must we serve the interests of his kingdom among men, that
all the earth may fear before him, v. 30. 3. Let us be ourselves
encouraged to triumph and trust in God. Those that give glory to God\'s
name are allowed to glory in it (v. 10), to value themselves upon their
relation to God and venture themselves upon his promise to them. Let the
heart of those rejoice that seek the Lord, much more of those that have
found him. Seek him, and his strength, and his face: that is, seek him
by the ark of his strength, in which he manifests himself. 4. Let the
everlasting covenant be the great matter of our joy and praise (v. 15):
Be mindful of his covenant. In the parallel place it is, He will be ever
mindful of it, Ps. 105:8. Seeing God never will forget it, we never
must. The covenant is said to be commanded, because God has obliged us
to obey the conditions of it, and because he has both authority to make
the promise and ability to make it good. This covenant was ancient, yet
never to be forgotten. It was made with Abraham, Isaac, and Jacob, who
were long since dead (v. 16-18), yet still sure to the spiritual seed,
and the promises of it pleadable. 5. Let God\'s former mercies to his
people of old, to our ancestors and our predecessors in profession, be
commemorated by us now with thankfulness to his praise. Let it be
remembered how God protected the patriarchs in their unsettled
condition. When they came strangers to Canaan and were sojourners in it,
when they were few and might easily have been swallowed up, when they
were continually upon the remove and so exposed, when there were many
that bore them ill-will and sought to do them mischief, yet no man was
suffered to do them wrong-not the Canaanites, Philistines, Egyptians.
Kings were reproved and plagued for their sakes. Pharaoh was so, and
Abimelech. They were the anointed of the Lord, sanctified by his grace,
sanctified by his glory, and had received the unction of the Spirit.
They were his prophets, instructed in the things of God themselves and
commissioned to instruct others (and prophets are said to be anointed, 1
Ki. 19:16; Isa. 61:1); therefore, if any touch them, they touch the
apple of God\'s eye; if any harm them, it is at their peril, v. 19-22.
`6.` Let the great salvation of the Lord be especially the subject of our
praises (v. 23): Show forth from day to day his salvation, that is (says
bishop Patrick), his promised salvation by Christ. We have reason to
celebrate that from day to day; for we daily receive the benefits of it,
and it is a subject that can never be exhausted. 7. Let God be praised
by a due and constant attendance upon him in the ordinances he has
appointed: Bring an offering, then the fruit of the ground, now the
fruit of the lips, of the heart (Heb. 13:15), and worship him in the
beauty of holiness, in the holy places and in a holy manner, v. 29.
Holiness is the beauty of the Lord, the beauty of all sanctified souls
and all religious performances. 8. Let God\'s universal monarchy be the
fear and joy of all people. Let us reverence it: Fear before him, all
the earth. And let us rejoice in it: Let the heavens be glad and
rejoice, because the Lord reigns, and by his providence establishes the
world, so that, though it be moved, it cannot be removed, nor the
measures broken which Infinite Wisdom has taken in the government of it,
v. 30, 31. 9. Let the prospect of the judgment to come inspire us with
an awful pleasure, Let earth and sea, fields and woods, though in the
great day of the Lord they will all be consumed, yet rejoice that he
will come, doth come, to judge the earth, v. 32, 33. 10. In the midst of
our praises we must not forget to pray for the succour and relief of
those saints and servants of God that are in distress (v. 35): Save us,
gather us, deliver us from the heathen, those of us that are scattered
and oppressed. When we are rejoicing in God\'s favours to us we must
remember our afflicted brethren, and pray for their salvation and
deliverance as our own. We are members one of another; and therefore
when we mean, \"Lord, save them,\" it is not improper to say, \"Lord,
save us.\" Lastly, Let us make God the Alpha and Omega of our praises.
David begins with (v. 8), Give thanks to the Lord; he concludes (v. 36),
Blessed be the Lord. And whereas in the place whence this doxology is
taken (Ps. 106:48) it is added, Let all the people say, Amen,
Hallelujah, here we find they did according to that directory: All the
people said, Amen, and praised the Lord. When the Levites had finished
this psalm or prayer and praise, then, and not till then, the people
that attended signified their consent and concurrence by saying, Amen,
And so they praised the Lord, much affected no doubt with this newly
instituted way of devotion, which had been hitherto used in the schools
of the prophets only, 1 Sa. 10:5. And, if this way of praising God
please the Lord better than an ox or a bullock that has horns and hoofs,
the humble shall see it and be glad, Ps. 69:31, 32.

### Verses 37-43

The worship of God is not only to be the work of a solemn day now and
then, brought in to grace a triumph; but it ought to be the work of
every day. David therefore settles it here for a constancy, puts it into
a method, which he obliged those that officiated to observe in their
respective posts. In the tabernacle of Moses, and afterwards in the
temple of Solomon, the ark and the altar were together; but, ever since
Eli\'s time, they had been separated, and still continued so till the
temple was built. I cannot conceive what reason there was why David, who
knew the law and was zealous for it, did not either bring the ark to
Gibeon, where the tabernacle and the altar were, or bring them to Mount
Zion, where the ark was. Perhaps the curtains and hangings of Moses\'s
tabernacle were so worn with time and weather that they were not fit to
be removed, nor fit to be a shelter for the ark; and yet he would not
make all new, but only a tent for the ark, because the time was at hand
when the temple should be built. Whatever was the reason, all David\'s
time they were asunder, but he took care that neither of them should be
neglected. 1. At Jerusalem, where the ark was, Asaph and his brethren
were appointed to attend, to minister before the ark continually, with
songs of praise, as every day\'s work required, v. 37. No sacrifices
were offered there, nor incense burnt, because the altars were not
there: but David\'s prayers were directed as incense, and the lifting up
of his hands as the evening sacrifice (Ps. 141:2), so early did
spiritual worship take place of ceremonial. 2. Yet the ceremonial
worship, being of divine institution, must by no means be omitted; and
therefore at Gibeon were the altars where the priests attended, for
their work was to sacrifice and burn incense, which they did
continually, morning and evening, according to the law of Moses, v. 39,
`40.` These must be kept up because, however in their own nature they were
inferior to the moral services of prayer and praise, yet, as they were
types of the mediation of Christ, they had a great deal of honour put
upon them, and the observance of them was of great consequence. Here
Zadok attended, to preside in the service of the altar; as (it is
probable) Abiathar settled at Jerusalem, to attend the ark, because he
had the breast-plate of judgment, which must be consulted before the
ark: this is the reason why we read in David\'s time both Zadok and
Abiathar were the priests (2 Sa. 8:17; 20:25), one where the altar was
and the other where the ark was. At Gibeon, where the altars were, David
also appointed singers to give thanks to the Lord, and the burden of all
their songs must be, For his mercy endureth for ever, v. 41. They did it
with musical instruments of God, such instruments as were appointed and
appropriated to this service, not such as they used on other occasions.
Between common mirth and holy joy there is a vast difference, and the
limits and distances between them must be carefully observed and kept
up. Matters being thus settled, and the affairs of religion put into a
happy channel, `(1.)` The people were satisfied, and went home pleased.
`(2.)` David returned to bless his house, resolving to keep up family
worship still, which public worship must not supersede.
