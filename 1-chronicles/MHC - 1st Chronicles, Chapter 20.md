1st Chronicles, Chapter 20
==========================

Commentary
----------

Here is a repetition of the story of David\'s wars, `I.` With the
Ammonites, and the taking of Rabbah (v. 1-3). `II.` With the giants of the
Philistines (v. 4-8).

### Verses 1-3

How the army of the Ammonites and their allies was routed in the field
we read in the foregoing chapters. Here we have the destruction of
Rabbah, the metropolis of their kingdom (v. 1), the putting of their
king\'s crown upon David\'s head (v. 2), and the great severity that was
used towards the people, v. 3. Of this we had a more full account in 2
Sa. 11, 12, and cannot but remember it by this sad token, that while
Joab was besieging Rabbah David fell into that great sin in the matter
of Uriah. But it is observable that, though the rest of the story is
repeated, that is not: a hint only is given of it, in those words which
lie here in a parenthesis-But David tarried at Jerusalem. If he had been
abroad with his army, he would have been out of the way of that
temptation; but, indulging his ease, he fell into uncleanness. Now, as
the relating of the sin David fell into is an instance of the
impartiality and fidelity of the sacred writers, so the avoiding of the
repetition of it here, when there was a fair occasion given to speak of
it again, is designed to teach us that, though there may be a just
occasion to speak of the faults and miscarriages of others, yet we
should not take delight in the repetition of them. That should always be
looked upon as an unpleasing subject which, though sometimes one cannot
help falling upon, yet one would not choose to dwell upon, any more than
we should love to rake in a dunghill. The persons, or actions, we can
say no good of, we had best say nothing of.

### Verses 4-8

The Philistines were nearly subdued (ch. 18:1); but, as in the
destruction of the Canaanites by Joshua the sons of Anak were last
subdued (Jos. 11:21), so here in the conquest of the Philistines the
giants of Gath were last brought down. In the conflicts between grace
and corruption there are some sins which, like these giants, keep their
ground a great while and are not mastered without much difficulty and a
long struggle: but judgment will be brought forth unto victory at last.
Observe, 1. We never read of giants among the Israelites as we do of the
giants among the Philistines-giants of Gath, but not giants of
Jerusalem. The growth of God\'s plants is in usefulness, not in bulk.
Those who covet to have cubits added to their stature do not consider
that it will but make then more unwieldy. In the balance of the
sanctuary David far outweighs Goliath. 2. The servants of David, though
men of ordinary stature, were too hard for the giants of Gath in every
encounter, because they had God on their side, who takes pleasure in
abasing lofty looks, and mortifying the giants that are in the earth, as
he did of old by the deluge, though they were men of renown. Never let
the church\'s friends be disheartened by the power and pride of the
church\'s enemies. We need not fear great men against us while we have
the great God for us. What will a finger more on each hand do, or a toe
more on each foot, in contest with Omnipotence? 3. These giants defied
Israel (v. 7) and were thus made to pay for their insolence. None are
more visibly marked for ruin that those who reproach God and his Israel.
God will do great things rather than suffer the enemy to behave
themselves proudly, Deu. 32:27. The victories of the Son of David, like
those of David himself, are gradual. We see not yet all things put under
him; but it will be seen shortly: and death itself, the last enemy, like
these giants, will be triumphed over.
