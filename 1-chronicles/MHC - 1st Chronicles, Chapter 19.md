1st Chronicles, Chapter 19
==========================

Commentary
----------

The story is here repeated of David\'s war with the Ammonites and the
Syrians their allies, and the victories he obtained over them, which we
read just as it is here related, 2 Sa. 10. Here is, `I.` David\'s civility
to the king of Ammon, in sending an embassy of condolence to him on
occasion of his father\'s death (v. 1, 2). `II.` His great incivility to
David, in the base usage he gave to his ambassadors (v. 3, 4). `III.`
David\'s just resentment of it, and the war which broke out thereupon,
in which the Ammonites acted with policy in bringing the Syrians to
their assistance (v. 6, 7), Joab did bravely (v. 8-13), and Israel was
once and again victorious (v. 14-19).

### Verses 1-5

Let us here observe, 1. That is becomes good people to be neighbourly,
and especially to be grateful. David will pay respect to Hanun because
he is his neighbour; and religion teaches us to be civil and obliging to
all, to honour all men, and to be ready to do all offices of kindness to
those we live among; nor must difference in religion be any obstruction
to this. But, besides this, David remembered the kindness which his
father showed to him. Those that have received kindness must return it
as they have ability and opportunity: those that have received it from
the parents must return it to the children when they are gone. 2. That,
as saith the proverb of the ancients, Wickedness proceedeth from the
wicked, 1 Sa. 24:13. The vile person will speak villany, and the
instruments of the churl will be evil, to destroy those with lying words
that speak right, Isa. 32:6, 7. Those that are base, and design ill
themselves, are apt to be jealous and to suspect ill of others without
cause. Hanun\'s servant suggested that David\'s ambassadors came as
spies, as if so great and mighty a man as David needed to do so mean a
thing (if he had any design upon the Ammonites, he could effect it by
open force, and had no occasion for any fraudulent practices), or as if
a man of such virtue and honour would do so base a thing. Yet Hanun
hearkened to the suggestion, and, against the law of nations, treated
David\'s ambassadors villainously. 3. Masters ought to protect their
servants, and with the greatest tenderness to concern themselves for
them if they come by any loss or damage in their service. David did so
for his ambassadors, v. 5. Christ will do so for his ministers; and let
all masters thus give unto their servants that which is just and equal.

### Verses 6-19

We may see here, 1. How the hearts of sinners that are marked for ruin
are hardened to their destruction. The children of Ammon saw that they
had made themselves odious to David (v. 6), and then it would have been
their wisdom to desire conditions of peace, to humble themselves and
offer any satisfaction for the injury they had done him, the rather
because they had made themselves not only odious to David, but obnoxious
to the justice of God, who is King of nations, and will assert the
injured rights and maintain the violated laws of nations. But, instead
of this, they prepared for war, and so brought upon themselves, by
David\'s hand, those desolations which he never intended them. 2. How
the courage of brave men is heightened and invigorated by difficulties.
When Joab saw that the battle was set against him before and behind (v.
10), instead of meditating a retreat, he doubled his resolution; and,
though he could not double, he divided his army, and not only spoke, but
acted, like a gallant man, that had great presence of mind when he saw
himself surrounded. He engaged with his brother for mutual assistance
(v. 12), excited himself and the rest of the officers to act vigorously
in their respective posts, with an eye to God\'s glory and their
country\'s good, not to any honour and advantage of their own, and then
left the issue to God: Let the Lord do that which is right in his sight.
`3.` How vain the greatest art and strength are against justice and
equity. The Ammonites did their utmost to make the best of their
position: they brought as good a force into the field, and disposed it
with as much policy as possible; yet, having a bad cause, and acting in
defence of wrong, it would not do; they were put to the worst. Right
will prevail and triumph at last. 4. To how little purpose it is for
those to rally again, and reinforce themselves, that have not God on
their side. The Syrians, though in no way concerned in the merits of the
cause, but serving only as mercenaries to the Ammonites, when they were
beaten, thought themselves concerned to retrieve their honour, and
therefore called in the assistance of the Syrians on the other side
Euphrates; but to no purpose, for still they fled before Israel (v. 18);
they lost 7000 men, who are said to be the men of 700 chariots, 2 Sa.
10:18. For, as now in a man of war for sea-service they allot ten men to
a gun, so then, in land-service, ten men to a chariot. 5. those who have
meddled with strife that belongs not to them, and have found that they
meddled to their own heart, do well to learn wit at length and meddle no
further. The Syrians, finding that Israel was the conquering side, not
only broke off their alliance with the Ammonites and would help them no
more (v. 19), but made peace with David and became his servants. Let
those who have in vain stood it out against God be thus wise for
themselves, and agree with him quickly, while they are in the way. Let
them become his servants; for they cannot but see themselves undone if
they be his enemies.
