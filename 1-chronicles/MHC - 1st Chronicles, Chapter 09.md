1st Chronicles, Chapter 9
=========================

Commentary
----------

This chapter intimates to us that one end of recording all these
genealogies was to direct the Jews, now that they had returned out of
captivity, with whom to incorporate and where to reside; for here we
have an account of those who first took possession of Jerusalem after
their return from Babylon, and began the rebuilding of it upon the old
foundation. `I.` The Israelites (v. 2-9). `II.` The priests (v. 10-13). `III.`
The Levites and other Nethinim (v. 14-26). `IV.` Here is the particular
charge of some of the priests and Levites (v. 27-34). `V.` A repetition of
the genealogy of king Saul (v. 35-44).

### Verses 1-13

The first verse looks back upon the foregoing genealogies, and tells us
they were gathered out of the books of the kings of Israel and Judah,
not that which we have in the canon of scripture, but another civil
record, which was authentic, as the king\'s books with us. Mentioning
Israel and Judah, the historian takes notice of their being carried away
to Babylon for their transgression. Let that judgment never be
forgotten, but ever be remembered, for warning to posterity to take heed
of those sins that brought it upon them. Whenever we speak of any
calamity that has befallen us, it is good to add this, \"it was for my
transgression,\" that God may be justified and clear when he judges.
Then follows an account of the first inhabitants, after their return
from captivity, that dwelt in their cities, especially in Jerusalem. 1.
The Israelites. That general name is used (v. 2) because with those of
Judah and Benjamin there were many of Ephraim and Manasseh, and the
other ten tribes (v. 3), such as had escaped to Judah when the body of
the ten tribes were carried captive or returned to Judah upon the
revolutions in Assyria, and so went into captivity with them, or met
them when they were in Babylon, associated with them, and so shared in
the benefit of their enlargement. It was foretold that the children of
Judah and of Israel should be gathered together and come up out of the
land (Hos. 1:11), and that they should be one nation again, Eze. 37:22.
Trouble drives those together that have been at variance; and the pieces
of metal that had been separated will run together again when melted in
the same crucible. Many both of Judah and Israel staid behind in
captivity; but some of both, whose spirit God stirred up, enquired the
way to Zion again. Divers are here named, and many more numbered, who
were chief of the fathers (v. 9), who ought to be remembered with
honour, as Israelites indeed. 2. The priests, v. 10. It was their praise
that they came with the first. Who should lead in a good work if the
priests, the Lord\'s ministers, do not? It was the people\'s praise that
they would not come without them; for who but the priests should keep
knowledge? Who but the priests should bless them in the name of the
Lord? `(1.)` It is said of one of them that he was the ruler of the house
of God (v. 11) not the chief ruler, for Joshua was then the high priest,
but the sagan, and the next under him, his deputy, who perhaps applied
more diligently to the business than the high priest himself. In the
house of God it is requisite that there be rulers, not to make new laws,
but to take care that the laws of God be duly observed by priests as
well as people. `(2.)` It is said of many of them that they were very able
men for the service of the house of God, v. 13. In the house of God
there is service to be done, constant service; and it is well for the
church when those are employed in that service who are qualified for it,
able ministers of the New Testament, 2 Co. 3:6. The service of the
temple was such as required at all times, especially in this critical
juncture, when they had newly come out of Babylon, great courage and
vigour of mind, as well as strength of body; and therefore they are
praised as mighty men of valour.

### Verses 14-34

We have here a further account of the good posture which the affairs of
religion were put into immediately upon the return of the people out of
Babylon. They had smarted for their former neglect of ordinances and
under the late want of ordinances. Both these considerations made them
very zealous and forward in setting up the worship of God among them; so
they began their worship of God at the right end. Instances hereof we
have here.

`I.` Before the house of the Lord was built they had the house of the
tabernacle, a plain and movable tent, which they made use of in the mean
time. Those that cannot yet reach to have a temple must not be without a
tabernacle, but be thankful for that and make the best of it. Never let
God\'s work be left undone for want of a place to do it in.

`II.` In allotting to the priests and Levites their respective
employments, they had an eye to the model that was drawn up by David,
and Samuel the seer, v. 22. Samuel, in his time, had drawn the scheme of
it, and laid the foundation, though the ark was then in obscurity, and
David afterwards finished it, and both acted by immediate direction from
God. Or David, as soon as he was anointed had this matter in his mind
and consulted Samuel about it, though he was then in his troubles, and
the plan was formed in concert between them. This perhaps had been
little regarded for many ages; but now, after a long interruption, it
was revived. In dividing the work, they observed these ancient
land-marks.

`III.` The most of them dwelt at Jerusalem (v. 34), yet there were some
that dwelt in the villages (v. 16, 22), because, it may be, there was
not yet room for them in Jerusalem. However they were employed in the
service of the tabernacle (v. 25): They were to come after seven days
from time to time. They had their week\'s attendance in their turns.

`IV.` Many of the Levites were employed as porters at the gates of the
house of God, four chief porters (v. 26), and, under them, others, to
the number of 212, v. 22. They had the oversight of the gates (v. 23),
were keepers of the thresholds, as in the margin (v. 19), and keepers of
the entry. This seemed a mean office; and yet David would rather have it
than dwell in the tents of wickedness, Ps. 84:10. Their office was, 1.
To open the doors of God\'s house every morning (v. 27) and shut them at
night. 2. To keep off the unclean, and hinder those from thrusting in
that were forbidden by the law. 3. To direct and introduce into the
courts of the Lord those that came thither to worship, and to show them
where to go and what to do, that they might not incur punishment. This
required care, and diligence, and constant attendance. Ministers have
work to do of this kind.

`V.` Here is one Phinehas, a son of Eleazar, that is said to be a ruler
over them in time past (v. 20), not the famous high priest of that name,
but (as is supposed) an eminent Levite, of whom it is here said that the
Lord was with him, or (as the Chaldee reads it) the Word of the Lord was
his helper-the eternal Word, who is Jehovah, the mighty one on whom help
is laid.

`VI.` It is said of some of them that, because the charge was upon them,
they lodged round about the house of God, v. 27. It is good for
ministers to be near their work, that they may give themselves wholly to
it. The Levites pitched about the tabernacle when they marched through
the wilderness. Then they were porters in one sense, bearing the burdens
of the sanctuary, now porters in another sense, attending the gates and
the doors-in both instances keeping the charge of the sanctuary.

`VII.` Every one knew his charge. Some were entrusted with the plate, the
ministering vessels, to bring them in and out by tale, v. 28. Others
were appointed to prepare the fine flour, wine, oil, etc., v. 29.
Others, that were priests, made up the holy anointing oil, v. 30. Others
took care of the meat-offerings, v. 31. Others of the show-bread, v. 32.
As in other great houses, so in God\'s house, the work is likely to be
done well when every one knows the duty of his place and makes a
business of it. God is the God of order: but that which is every body\'s
work will be nobody\'s work.

`VIII.` The singers were employed in that work day and night, v. 33. They
were the chief fathers of the Levites that made a business of it, not
mean singing-men, that made a trade of it. They remained in the chambers
of the temple, that they might closely and constantly attend it, and
were therefore excused from all other services. It should seem, some
companies were continually singing, at least at stated hours, both day
and night. Thus was God continually praised, as it is fit he should be
who is continually doing good. Thus devout people might, at any hour,
have assistance in their devotion. Thus was the temple a figure of the
heavenly one, where they rest not day nor night from praising God, Rev.
4:8. Blessed are those that dwell in thy house; they will be still
praising thee.

### Verses 35-44

These verses are the very same with ch. 8:29-38, giving an account of
the ancestors of Saul and the posterity of Jonathan. There it is the
conclusion of the genealogy of Benjamin; here it is an introduction to
the story of Saul. We take the repetition as we find it; but if we admit
that there are in the originals, especially in these books, some errors
of the transcribers, I should be tempted to think this repetition arose
from a blunder. Some one, in copying out these genealogies, having
written those words, v. 34 (These dwelt in Jerusalem), cast his eye on
the same words, ch. 8:28 (These dwelt in Jerusalem), and so went on with
what followed there, instead of going on with what followed here; and,
when he perceived his mistake, was loth to make a blot in his book, and
so let it stand. We have a rule in our law, Redundans non
nocet-Redundancies do no harm.
