1st Chronicles, Chapter 25
==========================

Commentary
----------

David, having settled the courses of these Levites that were to attend
the priests in their ministrations, proceeds, in this chapter, to put
those into a method that were appointed to be singers and musicians in
the temple. Here is, `I.` The persons that were to be employed, Asaph,
Heman, and Jeduthun (v. 1), their sons (v. 2-6), and other skilful
persons (v. 7). `II.` The order in which they were to attend determined by
lot (v. 8-31).

### Verses 1-7

Observe, `I.` Singing the praises of God is here called prophesying (v.
1-3), not that all those who were employed in this service were honoured
with the visions of God, or could foretel things to come. Heman indeed
is said to be the king\'s seer in the words of God (v. 5); but the
psalms they sang were composed by the prophets, and many of them were
prophetical; and the edification of the church was intended in it, as
well as the glory of God. In Samuel\'s time singing the praises of God
went by the name of prophesying (1 Sa. 10:5; 19:20), and perhaps that is
intended in what St. Paul calls prophesying, 1 Co. 11:4; 14:24.

`II.` This is here called a service, and the persons employed in it
workmen, v. 1. Not but that it is the greatest liberty and pleasure to
be employed in praising God: what is heaven but that? But it intimates
that it is our duty to make a business of it, and stir up all that is
within us to it; and that, in our present state of corruption and
infirmity, it will not be done as it should be done without labour and
struggle. We must take pains with our hearts to bring them, and keep
them, to this work, and to engage all that is within us.

`III.` Here were, in compliance with the temper of that dispensation, a
great variety of musical instruments used, harps, psalteries, cymbals
(v. 1, 6), and here was one that lifted up the horn (v. 5), that is,
used wind-music. The bringing of such concerts of music into the worship
of God now is what none pretend to. But those who use such concerts for
their own entertainment should feel themselves obliged to preserve them
always free from any thing that savours of immorality or profaneness, by
this consideration, that time was when they were sacred; and then those
were justly condemned who brought them into common use, Amos 6:5. They
invented to themselves instruments of music like David.

`IV.` The glory and honour of God were principally intended in all this
temple-music, whether vocal or instrumental. It was to give thanks, and
praise the Lord, that the singers were employed, v. 3. It was in the
songs of the Lord that they were instructed (v. 7), that is, for songs
in the house of the Lord, v. 6. This agrees with the intention of the
perpetuating of psalmody in the gospel-church, which is to make melody
with the heart, in conjunction with the voice, unto the Lord, Eph. 5:19.

`V.` The order of the king is likewise taken notice of, v. 2 and again v.
`6.` In those matters indeed David acted as a prophet; but his taking care
for the due and regular observance of divine institutions, both ancient
and modern, is an example to all in authority to use their power for the
promoting of religion, and the enforcing of the laws of Christ. Let them
thus be ministers of God for good.

`VI.` The fathers presided in this service, Asaph, Heman, and Jeduthun
(v. 1), and the children were under the hands of their father, v. 2, 3,
`6.` This gives a good example to parents to train up their children, and
indeed to all seniors to instruct their juniors in the service of God,
and particularly in praising him, than which there is no part of our
work more necessary or more worthy to be transmitted to the succeeding
generations. It gives also an example to the younger to submit
themselves to the elder (whose experience and observation fit them for
direction), and, as far as may be, to do what they do under their hand.
It is probable that Heman, Asaph, and Jeduthun, were bred up under
Samuel, and had their education in the schools of the prophets which he
was the founder and president of; then they were pupils, now they came
to be masters. Those that would be eminent must begin early, and take
time to prepare themselves. This good work of singing God\'s praises
Samuel revived, and set on foot, but lived not to see it brought to the
perfection it appears in here. Solomon perfects what David began, so
David perfects what Samuel began. Let all, in their day, do what they
can for God and his church, though they cannot carry it so far as they
would; when they are gone God can out of stones raise up others who
shall build upon their foundation and bring forth the top-stone.

`VII.` There were others also, besides the sons of these three great men,
who are called their brethren (probably because they had been wont to
join with them in their private concerts), who were instructed in the
songs of the Lord, and were cunning or well skilled therein, v. 7. They
were all Levites and were in number 288. Now, 1. These were a good
number, and a competent number to keep up the service in the house of
God; for they were all skilful in the work to which they were called.
When David the king was so much addicted to divine poesy and music many
others, all that had a genius for it, applied their studies and
endeavours that way. Those do religion a great deal of good service that
bring the exercises of devotion into reputation. 2. Yet these were but a
small number in comparison with the 4000 whom David appointed thus to
praise the Lord, ch. 23:5. Where were all the rest when only 288, and
those but by twelve in a course, were separated to this service? It is
probable that all the rest were divided into as many courses, and were
to follow as these led. Or, perhaps, these were for songs in the house
of the Lord (v. 6), with whom any that worshipped in the courts of that
house might join; and the rest were disposed of, all the kingdom over,
to preside in the country congregations, in this good work: for, though
the sacrifices instituted by the hand of Moses might be offered but at
one place, the psalms penned by David might be sung every where, 1 Tim.
2:8.

### Verses 8-31

Twenty-four persons are named in the beginning of this chapter as sons
of those three great men, Asaph, Heman, and Jeduthun. Ethan was the
third (ch. 6:44), but probably he was dead before the establishment was
perfected and Jeduthun came in his room. \[Or perhaps Ethan and Jeduthun
were two names for the same person.\] Of these three Providence so
ordered it that Asaph had four sons, Jeduthun six \[only five are
mentioned v. 3; Shimei, mentioned v. 17, is supposed to have been the
sixth\], and Heman fourteen, in all twenty-four (who were named, v.
2-4), who were all qualified for the service and called to it. But the
question was, In what order must they serve? This was determined by lot,
to prevent strife for precedency, a sin which most easily besets many
that otherwise are good people.

`I.` The lot was thrown impartially. They were placed in twenty-four
companies, twelve in a company, in two rows, twelve companies in a row,
and so they cast lots, ward against ward, putting them all upon a level,
small and great, teacher and scholar. They did not go according to their
age, or according to their standing, or the degrees they had taken in
the music-schools; but it was referred to God, v. 8. Small and great,
teachers and scholars, stand alike before God, who goes not according to
our rules of distinction and precedency. See Mt. 20:23.

`II.` God determined it as he pleased, taking account, it is probable, of
the respective merits of the persons, which are of much more importance
than seniority of age or priority of birth. Let us compare them with the
preceding catalogue and we shall find that, 1. Josephus was the second
son of Asaph. 2. Gedaliah the eldest son of Jeduthun. 3. Zaccur the
eldest of Asaph. 4. Izri the second of Jeduthun. 5. Nethaniah the third
of Asaph. 6. Bukkiah the eldest of Heman. 7. Jesharelah the youngest of
Asaph. 8. Jeshaiah the third of Jeduthun. 9. Mattaniah the second of
Heman. 10. Shimei the youngest of Jeduthun. 11. Azareel the third of
Heman. 12. Hashabiah the fourth of Jeduthun. 13. Shubael the fourth of
Heman. 14. Mattithiah the fifth of Jeduthun. 15. Jeremoth the fifth of
Heman. 16. Hananiah the sixth of Heman. 17. Joshbekashah the eleventh of
Heman. 18. Hanani the seventh of Heman. 19. Mallothi the twelfth of
Heman. 20. Eliathah the eighth of Heman. 21. Hothir the thirteenth of
Heman. 22. Giddalti the ninth of Heman. 23. Mehazioth the fourteenth of
Heman. And, lastly, Romamti-ezer, the tenth of Heman. See how God
increased some and preferred the younger before the elder.

`III.` Each of these had in his chorus the number of twelve, called their
sons and their brethren, because they observed them as sons, and
concurred with them as brethren. Probably twelve, some for the voice and
others for the instrument, made up the concert. Let us learn with one
mind and one mouth to glorify God, and that will be the best concert.
