1st Chronicles, Chapter 29
==========================

Commentary
----------

David has said what he had to say to Solomon. But he had something more
to say to the congregation before he parted with them. `I.` He pressed
them to contribute, according to their ability, towards the building and
furnishing of the temple (v. 1-5). `II.` They made their presents
accordingly with great generosity (v. 6-9). `III.` David offered up solemn
prayers and praises to God upon that occasion (v. 10-20), with
sacrifices (v. 21, 22). `IV.` Solomon was hereupon enthroned, with great
joy and magnificence (v. 23-25). `V.` David, soon after this finished his
course (v. 26-30). And it is hard to say which shines brighter here, the
setting sun or the rising sun.

### Verses 1-9

We may here observe,

`I.` How handsomely David spoke to the great men of Israel, to engage them
to contribute towards the building of the temple. It is our duty to
provoke one another to love and to good works, not only to do good
ourselves, but to draw in others to do good too as much as we can. There
were many very rich men in Israel; they were all to share in the benefit
of the temple, and of those peaceable days which were to befriend the
building of it; and therefore, though David would not impose on them, as
a tax, what they should give towards it, he would recommend the present
as a fair occasion for a free-will offering, because what is done in
works of piety and charity should be done willingly and not by
constraint; for God loves a cheerful giver. 1. He would have them
consider that Solomon was young and tender, and needed help; but that he
was the person whom God had chosen to do this work, and therefore was
well worthy their assistance. It is good service to encourage those in
the work of God that are as yet young and tender. 2. That the world was
great, and all hands should contribute to the carrying of it on. The
palace to be built was not for man, but for the Lord God; and the more
was contributed towards the building the more magnificent it would be,
and therefore the better would it answer the intention. 3. He tells them
what great preparations had been made for this work. He did not intend
to throw all the burden upon them, nor that it should be built wholly by
contributions, but that they should show their good will, by adding to
what was done (v. 2): I have prepared with all my might, that is, \"I
have made it my business.\" Work for God must be done with all our
might, or we shall bring nothing to pass in it. 4. He sets them a good
example. Besides what was dedicated to this service out of the spoils
and presents of the neighbouring nations, which was for the building of
the house (of which before, ch. 22:14), he had, out of his own share,
offered largely for the beautifying and enriching of it, 3000 talents of
gold and 7000 talents of silver (v. 4, 5), and this because he had set
his affection on the house of his God. He gave all this, not as Papists
build churches, in commutation of penance, or to make atonement for sin,
nor as Pharisees give alms, to be seen of men; but purely because he
loved the habitation of God\'s house; so he professed (Ps. 26:8) and
here he proved it. Those who set their affection upon the service of God
will think no pains nor cost too much to bestow upon it; and then our
offerings are pleasing to God when they come from love. Those that set
their affection on things above will set their affection on the house of
God, through which our way to heaven lies. Now this he gives them an
account of, to stir them up to do likewise. Note, Those who would draw
others to do that which is good must themselves lead. Those especially
who are advanced above others in place and dignity should particularly
contrive how to make their light shine before men, because the influence
of their example is more powerful and extensive than that of other
people. 5. He stirs them up to do as he had done (v. 5): And who then is
willing to concentrate his service this day unto the Lord? `(1.)` We must
each of us, in our several places, serve the Lord, and consecrate our
service to him, separate it from other things that are foreign and
interfere with it, and direct and design it for the honour and glory of
God. `(2.)` We must make the service of God our business, must fill our
hands to the Lord, so the Hebrew phrase is. Those who engage themselves
in the service of God will have their hands full; there is work enough
for the whole man in that service. The filling of our hands with the
service of God intimates that we must serve him only, serve him
liberally, and serve him in the strength of grace derived from him. `(3.)`
We must be free herein, do it willingly and speedily, do it this day,
when we are in a good mind. Who is willing? Now let him show it.

`II.` How handsomely they all contributed towards the building of the
temple when they were thus stirred up to it. Though they were persuaded
to it, yet it is said, They offered willingly, v. 6. So he said who knew
their hearts. Nay, they offered with a perfect heart, from a good
principle and with a sincere respect to the glory of God, v. 9. How
generous they were appears by the sum total of the contributions, v. 7,
`8.` They gave like themselves, like princes, like princes of Israel. And
a pleasant day\'s work it was; for, 1. The people rejoiced, which may be
meant of the people themselves that offered: they were glad of the
opportunity of honouring God thus with their substance, and glad of the
prospect of bringing this good work to perfection. Or the common people
rejoiced in the generosity of their princes, that they had such rulers
over them as were forward to this good work. Every Israelite is glad to
see temple work carried on with vigour. 2. David rejoiced with great joy
to see the good effects of his psalms and the other helps of devotion he
had furnished them with, rejoiced that his son and successor would have
those about him that were so well affected to the house of God, and that
this work, upon which his heart was so much set, was likely to go on.
Note, It is a great reviving to good men, when they are leaving the
world, to see those they leave behind zealous for religion and likely to
keep it up. Lord, now let thou thy servant depart in peace.

### Verses 10-22

We have here,

`I.` The solemn address which David made to God upon occasion of the noble
subscriptions of the princes towards the building of the temple (v. 10):
Wherefore David blessed the Lord, not only alone in his closet, but
before all the congregation. This I expected when we read (v. 9) that
David rejoiced with great joy; for such a devout man as he would no
doubt make that the matter of his thanksgiving which was so much the
matter of his rejoicing. He that looked round with comfort would
certainly look up with praise. David was now old and looked upon himself
as near his end; and it well becomes aged saints, and dying saints, to
have their hearts much enlarged in praise and thanksgiving. This will
silence their complaints of their bodily infirmities, and help to make
the prospect of death itself less gloomy. David\'s psalms, toward the
latter end of the book, are most of them psalms of praise. The nearer we
come to the world of everlasting praise the more we should speak the
language and do the work of that world. In this address,

`1.` He adores God, and ascribes glory to him as the God of Israel,
blessed for ever and ever. Our Lord\'s prayer ends with a doxology much
like this which David here begins with-for thine is the kingdom, the
power, and the glory. This is properly praising God-with holy awe and
reverence, and agreeable affection, acknowledging, `(1.)` His infinite
perfections; not only that he is great, powerful, glorious, etc., but
that his is the greatness, power, and glory, that is, he has them in and
of himself, v. 11. He is the fountain and centre of every thing that is
bright and blessed. All that we can, in our most exalted praises,
attribute to him he has an unquestionable title to. His is the
greatness; his greatness is immense and incomprehensible; and all others
are little, are nothing, in comparison of him. His is the power, and it
is almighty and irresistible; power belongs to him, and all the power of
all the creatures is derived from him and depends upon him. His is the
glory; for his glory is his own end and the end of the whole creation.
All the glory we can give him with our hearts, lips, and lives, comes
infinitely short of what is his due. His is the victory; he transcends
and surpasses all, and is able to conquer and subdue all things to
himself; and his victories are incontestable and uncontrollable. And his
is the majesty, real and personal; with him is terrible majesty,
inexpressible and inconceivable. `(2.)` His sovereign dominion, as
rightful owner and possessor of all: \"All that is in the heaven, and in
the earth, is thine, and at thy disposal, by the indisputable right of
creation, and as supreme ruler and commander of all: thine is the
kingdom, and all kings are thy subjects; for thou art head, and art to
be exalted and worshipped as head above all.\" `(3.)` His universal
influence and agency. All that are rich and honourable among the
children of men have their riches and honours from God. This
acknowledgment he would have the princes take notice of and join in,
that they might not think they had merited any thing of God by their
generosity; for from God they had their riches and honour, and what they
had returned to him was but a small part of what they had received from
him. Whoever are great among men, it is God\'s hand that makes them so;
and, whatever strength we have, it is God that gives it to us, as the
God of Israel our father, v. 10. Ps. 68:35.

`2.` He acknowledges with thankfulness the grace of God enabling them to
contribute so cheerfully towards the building of the temple (v. 13, 14):
Now therefore, our God, we thank thee. Note, The more we do for God the
more we are indebted to him for the honour of being employed in his
service, and for grace enabling us, in any measure, to serve him. Does
he therefore thank that servant? Lu. 17:9. No: but that servant has a
great deal of reason to thank him. He thanks God that they were able to
offer so willingly. Note, `(1.)` It is a great instance of the power of
God\'s grace in us to be able to do the work of God willingly. He works
both to will and to do; and it is in the day of his power that his
people are made willing, Ps. 110:3. `(2.)` We must give God all the glory
of all the good that is at any time done by ourselves or others. Our own
good works must not be the matter of our pride, nor the good works of
others the matter of our flattery, but both the matter of our praise;
for certainly it is the greatest honour and pleasure in the world
faithfully to serve God.

`3.` He speaks very humbly of himself, and his people, and the offerings
they had now presented to God. `(1.)` For himself, and those that joined
with him, though they were princes, he wondered that God should take
such notice of them and do so much for them (v. 14): Who am I, and what
is my people? David was the most honourable person, and Israel the most
honourable person, then in the world; yet thus does he speak of himself
and them, as unworthy the divine cognizance and favour. David now looks
very great, presiding in an august assembly, appointing his successor,
and making a noble present to the honour of God; and yet he is little
and low in his own eyes: Who am I, O Lord? for (v. 15) we are strangers
before thee, and sojourners, poor despicable creatures. Angels in heaven
are at home there; saints on earth are but strangers here: Our days on
the earth are as a shadow. David\'s days had as much of substance in
them as most men\'s; for he was a great man, a good man, a useful man,
and now an old man, one that lived long and lived to good purpose: and
yet he puts himself not only into the number, but in the front, of those
who must acknowledge that their days on the earth are as a shadow, which
intimates that our life is a vain life, a dark life, a transient life,
and a life that will have its periods either in perfect light or perfect
darkness. The next words explain it: There is no abiding, Heb. no
expectation. We cannot expect any great matters from it, nor can we
expect any long continuance of it. This is mentioned here as that which
forbids us to boast of the service we do to God. Alas! it is confined to
a scantling of time, it is the service of a frail and short life, and
therefore what can we pretend to merit by it? `(2.)` As to their
offerings, Lord, says he, of thy own have we given thee (v. 14), and
again (v. 16), It cometh of thy hand, and is all thy own. \"We have it
from thee as a free gift, and therefore are bound to use it for thee;
and what we present to thee is but rent or interest from thy own.\" \"In
like manner\" (says bishop Patrick) \"we ought to acknowledge God in all
spiritual things, referring every good thought, good purpose, good work,
to his grace, from whom we receive it.\" Let him that glories therefore
glory in the Lord.

`4.` He appeals to God concerning his own sincerity in what he did, v.
`17.` It is a great satisfaction to a good man to think that God tries the
heart and has pleasure in uprightness, that, whoever may misinterpret or
contemn it, he is acquainted with and approves of the way of the
righteous. It was David\'s comfort that God knew with what pleasure he
both offered his own and saw the people\'s offering. He was neither
proud of his own good work nor envious of the good works of others.

`5.` He prays to God both for the people and for Solomon, that both might
hold on as they began. In this prayer he addresses God as the God of
Abraham, Isaac, and Jacob, a God in covenant with them and with us for
their sakes. Lord, give us grace to make good our part of the covenant,
that we may not forfeit the benefit of it. Or thus: they were kept in
their integrity by the grace of God establishing their way; let the same
grace that was sufficient for them be so for us. `(1.)` For the people he
prays (v. 18) that what good God had put into their minds he would
always keep there, that they might never be worse than they were now,
might never lose the convictions they were now under, nor cool in their
affections to the house of God, but always have the same thoughts of
things as they now seemed to have. Great consequences depend upon what
is innermost, and what uppermost, in the imagination of the thoughts of
our heart, what we aim at and what we love to think of. If any good have
got possession of our hearts, or the hearts of our friends, it is good
by prayer to commit the custody of it to the grace of God: \"Lord, keep
it there, keep it for ever there. David has prepared materials for the
temple; but, Lord, do thou prepare their hearts for such a privilege;\"
establish their hearts, so the margin. \"Confirm their resolutions. They
are in a good mind; keep them so when I am gone, them and theirs for
ever.\" `(2.)` For Solomon he prays (v. 19), Give him a perfect heart. He
had charged him (ch. 28:9) to serve God with a perfect heart; now here
he prays to God to give him such a heart. He does not pray, \"Lord, make
him a rich man, a great man, a learned man;\" but, \"Lord, make him an
honest man;\" for that is better than all. \"Lord, give him a perfect
heart, not only in general to keep thy commandments, but in particular
to build the palace, that he may do that service with a single eye.\"
Yet his building the house would not prove him to have a perfect heart
unless he made conscience of keeping God\'s commandments. It is not
helping to build churches that will save us if we live in disobedience
to God\'s law.

`II.` The cheerful concurrence of this great assembly in this great
solemnity. 1. They joined with David in the adoration of God. When he
had done his prayer he called to them to testify their concurrence (Now
bless the Lord your God, v. 20), which accordingly they did, by bowing
down their heads, a gesture of adoration. Whoever is the mouth of the
congregation, those only have the benefit who join with him, not by
bowing down the head so much as by lifting up the soul. 2. They paid
their respects to the king, looking upon him as an instrument in God\'s
hand of much good to them; and, in honouring him, they honoured God. 3.
The next day they offered abundance of sacrifices to God (v. 21), both
burnt-offerings, which were wholly consumed, and peace-offerings, which
the offerer had the greatest part of to himself. Hereby they testified a
generous gratitude to God for the good posture their public affairs were
in, though David was going the way of all the earth. 4. They feasted and
rejoiced before God, v. 22. In token of their joy in God, and communion
with him, they feasted upon their peace-offerings in a religious manner
before the Lord. What had been offered to God they feasted upon, by
which was intimated to them that they should be never the poorer for
their late liberal contributions to the service of the temple; they
themselves should feast upon the comfort of it. 5. They made Solomon
king the second time. He having been before anointed in haste, upon
occasion of Adonijah\'s rebellion, it was thought fit to repeat the
ceremony, for the greater satisfaction of the people. They anointed him
to the Lord. Magistrates must look upon themselves as set apart for God,
to be his ministers, and must rule accordingly in the fear of God. Zadok
also was anointed to be priest in the room of Abiathar, who had lately
forfeited his honour. Happy art thou, O Israel! under such a prince and
such a pontiff.

### Verses 23-30

These verses bring king Solomon to his throne and king David to his
grave. Thus the rising generation thrusts out that which went before,
and says, \"Make room for us.\" Every one has his day.

`I.` Here is Solomon rising (v. 23): Solomon sat on the throne of the
Lord. Not his throne which he prepared in the heavens, but the throne of
Israel is called the throne of the Lord because not only is he King of
all nations, and all kings rule under him, but he was in a peculiar
manner King of Israel, 1 Sa. 12:12. He had the founding, he had the
filling, of their throne, by immediate direction. The municipal laws of
their kingdom were divine. Urim and prophets were the privy counsellors
of their princes; therefore is their throne called the throne of the
Lord. Solomon\'s kingdom typified the kingdom of the Messiah, and his is
indeed the throne of the Lord; for the Father judgeth no man, but hath
committed all judgment to him; hence he calls him his King, Ps. 2:6.
Being set on the throne of the Lord, the throne to which God called him,
he prospered. Those that follow the divine guidance may expect success
by the divine blessing. Solomon prospered; for, 1. His people paid
honour to him, as one to whom honour is due: All Israel obeyed him, that
is, were ready to swear allegiance to him (v. 23), the princes and
mighty men, and even the sons of David, though by seniority their title
to the crown was prior to his, and they might think themselves wronged
by his advancement. God thought fit to make him king, and made him fit
to be so, and therefore they all submitted themselves to him. God
inclined their hearts to do so, that his reign might, from the first, be
peaceable. His father was a better man than he, and yet came to the
crown with much difficulty, after long delay, and by many and slow
steps. David had more faith, and therefore had it more tried. They
submitted themselves (Heb. They gave the hand under Solomon), that is,
bound themselves by oath to be true to him (putting the hand under the
thigh was a ceremony anciently used in swearing); or they were so
entirely devoted that they would put their hand under his feet to serve
him. 2. God put honour upon him; for those that honour him he will
honour: The Lord magnified Solomon exceedingly, v. 25. His very
countenance and presence, I am apt to think, had something in them very
great and awful. All he said and all he did commanded respect. None of
all the judges or kings of Israel, his predecessors, made such a figure
as he did nor lived in such splendour.

`II.` Here is David\'s setting, that great man going off the stage. The
historian here brings him to the end of his day, leaves him asleep, and
draws the curtains about him.

`1.` He gives a summary account of the years of his reign, v. 26, 27. He
reigned forty years, as did Moses, Othniel, Deborah, Gideon, Eli,
Samuel, and Saul, who were before him, and Solomon after him.

`2.` He gives a short account of his death (v. 28), that he died full of
days, riches, and honour; that is, `(1.)` Loaded with them. He was very
old, and very rich, and very much honoured both of God and man. He had
been a man of war from his youth, and, as such, had his soul continually
in his hand; yet he was not cut off in the midst of his days, but was
preserved through all dangers of a military life, lived to a good old
age, and died in peace, died in his bed, and yet in the bed of honour.
`(2.)` Satiated with them. He was full of days, riches, and honour; that
is, he had enough of this world and of the riches and honours of it, and
knew when he had enough, for he was very willing to die and leave it,
having said (Ps. 49:15), God shall receive me, and (Ps. 23:4), Thou art
with me. A good man will soon be full of days, riches, and honour, but
will never be satisfied with them; no satisfaction but in God\'s loving
kindness.

`3.` For a fuller account of David\'s life and reign he refers to the
histories or records of those times, which were written by Samuel while
he lived, and continued, after his death, by Nathan and Gad, v. 29.
There was related what was observable in his government at home and his
wars abroad, the times, that is, the events of the times, that went over
him, v. 29, 30. These registers were then in being, but are now lost.
Note, Good use may be made of those histories of the church which are
authentic though not sacred or of divine inspiration.
