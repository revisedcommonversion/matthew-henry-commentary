1st Chronicles, Chapter 1
=========================

Commentary
----------

This chapter and many that follow it repeat the genealogies we have
hitherto met with in the sacred history, and put them all together, with
considerable additions. We may be tempted, it may be, to think it would
have been well if they had not been written, because, when they come to
be compared with other parallel places, there are differences found,
which we can scarcely accommodate to our satisfaction; yet we must not
therefore stumble at the word, but bless God that the things necessary
to salvation are plain enough. And since the wise God has thought fit to
write these things to us, we should not pass them over unread. All
scripture is profitable, though not all alike profitable; and we may
take occasion for good thoughts and meditations even from those parts of
scripture that do not furnish so much matter for profitable remarks as
some other parts. These genealogies, 1. Were then of great use, when
they were here preserved, and put into the hands of the Jews after their
return from Babylon; for the captivity, like the deluge, had put all
into confusion, and they, in that dispersion and despair, would be in
danger of losing the distinctions of their tribes and families. This
therefore revives the ancient landmarks even of some of the tribes that
were carried captive into Assyria. Perhaps it might invite the Jews to
study the sacred writings which had been neglected, to find the names of
their ancestors, and the rise of their families in them. 2. They are
still of some use for the illustrating of the scripture-story, and
especially for the clearing of the pedigrees of the Messiah, that it
might appear that our blessed Saviour was, according to the prophecies
which went before of him, the son of David, the son of Judah, the son of
Abraham, the son of Adam. And, now that he has come for whose sake these
registers were preserved, the Jews since have so lost all their
genealogies that even that of the priests, the most sacred of all, is
forgotten, and they know not of any one man in the world that can prove
himself of the house of Aaron. When the building is reared the scaffolds
are removed. When the promised Seed has come the line that was to lead
to him is broken off. In this chapter we have an abstract of all the
genealogies in the book of Genesis, till we come to Jacob. `I.` The
descents from Adam to Noah and his sons, out of Gen. 5, (v. 1-4). `II.`
The posterity of Noah\'s sons, by which the earth was repeopled, out of
Gen. 10, (v. 5-23). `III.` The descents from Shem to Abraham, out of Gen.
11, (v. 24-28). `IV.` The posterity of Ishmael, and of Abraham\'s sons by
Keturah, out of Gen. 25, (v. 29-35). `V.` The posterity of Esau, out of
Gen. 36, (v. 36-54). These, it is likely, were passed over lightly in
Genesis; and therefore, according to the law of the school, we are made
to go over that lesson again which we did not learn well.

### Verses 1-27

This paragraph has Adam for its first word and Abraham for its last.
Between the creation of the former and the birth of the latter were 2000
years, almost the one-half of which time Adam himself lived. Adam was
the common father of our flesh, Abraham the common father of the
faithful. By the breach which the former made of the covenant of
innocency, we were all made miserable; by the covenant of grace made
with the latter, we all are, or may be, made happy. We all are, by
nature, the seed of Adam, branches of that wild olive. Let us see to it
that, by faith, we become the seed of Abraham (Rom. 4:11, 12), that we
be grafted into the good olive and partake of its root and fatness.

`I.` The first four verses of this paragraph, and the last four, which are
linked together by Shem (v. 4, 24), contain the sacred line of Christ
from Adam to Abraham, and are inserted in his pedigree, Lu. 3:34-38, the
order ascending as here it descends. This genealogy proves the falsehood
of that reproach, As for this man, we know not whence he is. Bishop
Patrick well observes here that, a genealogy being to be drawn of the
families of the Jews, this appears as the peculiar glory of the Jewish
nation, that they alone were able to derive their pedigree from the
first man that God created, which no other nation pretended to, but
abused themselves and their posterity with fabulous accounts of their
originals, the Arcadians fancying that they were before the moon, the
people of Thessaly that they sprang from stones, the Athenians that they
grew out of the earth, much like the vain imaginations which some of the
philosophers had of the origin of the universe. The account which the
holy scripture gives both of the creation of the world and of the rise
of nations carries with it as clear evidences of its own truth as those
idle traditions do of their own vanity and falsehood.

`II.` All the verses between repeat the account of the replenishing of
the earth by the sons of Noah after the flood. 1. The historian begins
with those who were strangers to the church, the sons of Japhet, who
were planted in the isles of the Gentiles, those western parts of the
world, the countries of Europe. Of these he gives a short account (v.
5-7), because with these the Jews had hitherto had little or no
dealings. 2. He proceeds to those who had many of them been enemies to
the church, the sons of Ham, who moved southward towards Africa and
those parts of Asia which lay that way. Nimrod the son of Cush began to
be an oppressor, probably to the people of God in his time. But Mizraim,
from whom came the Egyptians, and Canaan, from whom came the Canaanites,
are both of them names of great note in the Jewish story; for with their
descendants the Israel of God had severe struggles to get out of the
land of Egypt and into the land of Canaan; and therefore the branches of
Mizraim are particularly recorded (v. 11, 12), and of Canaan, v. 13-16.
See at what a rate God valued Israel when he gave Egypt for their ransom
(Isa. 43:3), and cast out all these nations before them, Ps. 70:8. 3. He
then gives an account of those that were the ancestors and allies of the
church, the posterity of Shem, v. 17-23. These peopled Asia, and spread
themselves eastward. The Assyrians, Syrians, Chaldeans, Persians, and
Arabians, descended from these. At first the originals of the respective
nations were known; but at this day, we have reason to think, the
nations are so mingled with one another, by the enlargement of commerce
and dominion, the transplanting of colonies, the carrying away of
captives, and many other circumstances, that no one nation, no, nor the
greatest part of any, is descended entire from any one of these
fountains. Only this we are sure of, that God has created of one blood
all nations of men; they have all descended from one Adam, one Noah.
Have we not all one father? Has not one God created us? Mal. 2:10. Our
register hastens to the line of Abraham, breaking off abruptly from all
the other families of the sons of Noah but that of Arphaxad, from whom
Christ was to come. The great promise of the Messiah (says bishop
Patrick) was translated from Adam to Seth, from him to Shem, from him to
Eber, and so to the Hebrew nation, who were entrusted, above all
nations, with that sacred treasure, till the promise was performed and
the Messiah had come, and then that nation was made not a people.

### Verses 28-54

All nations but the seed of Abraham are already shaken off from this
genealogy: they have no part nor lot in this matter. The Lord\'s portion
is his people. Of them he keeps an account, knows them by name; but
those who are strangers to him he beholds afar off. Not that we are to
conclude that therefore no particular persons of any other nation but
the seed of Abraham found favour with God. It was a truth, before Peter
perceived it, that in every nation he that feared God and wrought
righteousness was accepted of him. Multitudes will be brought to heaven
out of all nations (Rev. 7:9), and we are willing to hope there were
many, very many, good people in the world, that lay out of the pale of
God\'s covenant of peculiarity with Abraham, whose names were in the
book of life, though not descended from any of the following families
written in this book. The Lord knows those that are his. But Israel was
a chosen nation, elect in type; and no other nation, in its national
capacity, was so dignified and privileged as the Jewish nation was. That
is the holy nation which is the subject of the sacred story; and
therefore we are next to shake off all the seed of Abraham but the
posterity of Jacob only, which were all incorporated into one nation and
joined to the Lord, while the other descendants from Abraham, for aught
that appears, were estranged both from God and from one another.

`I.` We shall have little to say of the Ishmaelites. They were the sons of
the bondwoman, that were to be cast out and not to be heirs with the
child of the promise; and their case was to represent that of the
unbelieving Jews, who were rejected (Gal. 4:22, etc.), and therefore
there is little notice taken of that nation. Ishmael\'s twelve sons are
just named here (v. 29-31), to show the performance of the promise God
made to Abraham, in answer to his prayer for him, that, for Abraham\'s
sake, he should become a great nation, and particularly that he should
beget twelve princes, Gen. 17:20.

`II.` We shall have little to say of the Midianites, who descended from
Abraham\'s children by Keturah. They were children of the east (probably
Job was one of them), and were separated from Isaac, the heir of the
promise (Gen. 25:6), and therefore they are only named here, v. 32. The
sons of Jokshan, the son of Keturah, are named also, and the sons of
Midian (v. 32, 33), who became most eminent, and perhaps gave
denomination to all these families, as Judah to the Jews.

`III.` We shall not have much to say of the Edomites. They had an
inveterate enmity to God\'s Israel; yet because they descended from
Esau, the son of Isaac, we have here an account of their families, and
the names of some of their famous men, v. 35 to the end. Some slight
differences there are between some of the names here, and as we had them
in Gen. 36, whence this whole account is taken. Three of four names that
were written with a Vau there are written with a Jod here, probably the
pronunciation being altered, as is usual in other languages. we now
write many words very differently from what they were written but 200
years ago. Let us take occasion, from the reading of these genealogies,
to think, 1. Of the multitudes that have gone through this world, have
acted their part in it, and then quitted it. Job, even in his early day,
saw not only every man drawing after him, but innumerable before him,
Job 21:33. All these, and all theirs, had their day; many of them made a
mighty noise and figure in the world; but their day came to fall, and
their place knew them no more. The paths of death are trodden paths, but
vestigia nulla retrorsum-none can retrace their steps. 2. Of the
providence of God, which keeps up the generations of men, and so
preserves that degenerate race, though guilty and obnoxious, in being
upon earth. How easily could he cut it off without either a deluge or a
conflagration! Write but all the children of men childless, as some are,
and in a few years the earth will be eased of the burden under which it
groans; but the divine patience lets the trees that cumber the ground
not only grow, but propagate. As one generation, even of sinful men,
passes away, another comes (Eccl. 1:4; Num. 32:14), and will do so while
the earth remains. Destroy it not, for a blessing is in it.
