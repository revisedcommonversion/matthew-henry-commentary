1st Chronicles, Chapter 6
=========================

Commentary
----------

Though Joseph and Judah shared between them the forfeited honours of the
birthright, yet Levi was first of all the tribes, dignified and
distinguished with an honour more valuable than either the precedency or
the double portion, and that was the priesthood. That tribe God set
apart for himself; it was Moses\'s tribe, and perhaps for his sake was
thus favoured. Of that tribe we have an account in this chapter. `I.`
Their pedigree, the first fathers of the tribe (v. 1-3), the line of the
priests, from Aaron to the captivity (v. 4-15), and of some other of
their families (v. 16-30). `II.` Their work, the work of the Levites (v.
31-48), of the priests (v. 49-53). `III.` The cities appointed them in the
land of Canaan (v. 54-81).

### Verses 1-30

The priests and Levites were more concerned than any other Israelites to
preserve their pedigree clear and to be able to prove it, because all
the honours and privileges of their office depended upon their descent.
And we read of those who, though perhaps they really were children of
the priests, yet, because they could not find the register of their
genealogies, nor make out their descent by any authentic record, were,
as polluted, put from the priesthood, and forbidden to eat of the holy
things, Ezra 2:62, 63. It is but very little that is here recorded of
the genealogies of this sacred tribe. `I.` The first fathers of it are
here named twice, v. 1, 16. Gershom, Kohath, and Merari, are three names
which we were very conversant with in the book of Numbers, when the
families of the Levites were marshalled and had their work assigned to
them. Aaron, and Moses, and Miriam, we have known much more of than
their names, and cannot pass them over here without remembering that
this was that Moses and Aaron whom God honoured in making them
instruments of Israel\'s deliverance and settlement and figures of him
that was to come, Moses as a prophet and Aaron as a priest. And the
mention of Nadab and Abihu (though, having no children, there was no
occasion to bring them into the genealogy) cannot but remind us of the
terrors of that divine justice which they were made monuments of for
offering strange fire, that we may always fear before him. 2. The line
of Eleazar, the successor of Aaron, is here drawn down to the time of
the captivity, v. 4-15. It begins with Eleazar, who came out of the
house of bondage in Egypt, and ends with Jehozadak, who went into the
house of bondage in Babylon. Thus, for their sins, they were left as
they were found, which might also intimate that the Levitical priesthood
did not make anything perfect, but this was to be done by the bringing
in of a better hope. All these here named were not high priests; for, in
the time of the judges, that dignity was, upon some occasion or other,
brought into the family of Ithamar, of which Eli was; but in Zadok it
returned again to the right line. Of Azariah it is here said (v. 10), He
it is that executed the priest\'s office in the temple that Solomon
built. It is supposed that this was that Azariah who bravely opposed the
presumption of king Uzziah when he invaded the priest\'s office (2 Chr.
26:17, 18), though he ventured his neck by so doing. This was done like
a priest, like one that was truly zealous for his God. He that thus
boldly maintained and defended the priest\'s office, and made good its
barriers against such a daring insult, might well be said to execute it;
and this honour is put upon him for it; while Urijah, one of his
successors, for a base compliance with King Ahaz, in building him an
idolatrous altar, has the disgrace put upon him of being left out of
this genealogy, as perhaps some others are. But some think that this
remark upon this Azariah should have been added to his grandfather of
the same name (v. 9), who was the son of Ahimaaz, and that he was the
priest who first officiated in Solomon\'s temple. 3. Some other of the
families of the Levites are here accounted for. One of the families of
Gershom (that of Libni) is here drawn down as far as Samuel, who had the
honour of a prophet added to that of a Levite. One of the families of
Merari (that of Mahli) is likewise drawn down for several descents, v.
29, 30.

### Verses 31-53

When the Levites were first ordained in the wilderness much of the work
then appointed them lay in carrying and taking care of the tabernacle
and the utensils of it, while they were in their march through the
wilderness. In David\'s time their number was increased; and, though the
greater part of them was dispersed all the nation over, to teach the
people the good knowledge of the Lord, yet those that attended the house
of God were so numerous that there was not constant work for them all;
and therefore David, by special commission and direction from God,
new-modelled the Levites, as we shall find in the latter part of this
book. Here we are told what the work was which he assigned them.

`I.` Singing-work, v. 31. David was raised up on high to be the sweet
psalmist of Israel (2 Sa. 23:1), not only to pen psalms, but to appoint
the singing of them in the house of the Lord (not so much because he was
musical as because he was devout), and this he did after that the ark
had rest. While that was in captivity, obscure, and unsettled, the harps
were hung upon the willow-trees: singing was then thought unseasonable
(when the bridegroom is taken away they shall fast); but the harps being
resumed, and the songs revived, at the bringing up of the ark, they were
continued afterwards. For we should rejoice as much in the prolonging of
our spiritual privileges as in the restoring of them. When the service
of the ark was much superseded by its rest they had other work cut out
for them (for Levites should never be idle) and were employed in the
service of song. Thus when the people of God come to the rest which
remains for them above they shall take leave of all their burdens and be
employed in everlasting songs. These singers kept up that service in the
tabernacle till the temple was built, and then they waited on their
office there, v. 32. When they came to that stately magnificent house
they kept as close both to their office and to their order as they had
done in the tabernacle. It is a pity that the preferment of the Levites
should ever make them remiss in their business. We have here an account
of the three great masters who were employed in the service of the
sacred song, with their respective families; for they waited with their
children, that is, such as descended from them or were allied to them,
v. 33. Heman, Asaph, and Ethan, were the three that were appointed to
this service, one of each of the three houses of the Levites, that there
might be an equality in the distribution of this work and honour, and
that every one might know his post, such an admirable order was there in
this choir service. 1. Of the house of Kohath was Heman with his family
(v. 33), a man of a sorrowful spirit, if it be the same Heman that
penned the 88th psalm, and yet a singer. He was the grandson of Samuel
the prophet, the son of Joel, of whom it is said that he walked not in
the ways of Samuel (1 Sa. 8:2, 3); but it seems, though the son did not,
the grandson did. Thus does the blessing entailed on the seed of the
upright sometimes pass over one generation and fasten upon the next. And
this Heman, though the grandson of that mighty prince, did not think it
below him to be a precentor in the house of God. David himself was
willing to be a door-keeper. Rather we may look upon this preferment of
the grandson in the church as a recompense for the humble modest
resignation which the grandfather made of his authority in the state.
Many such ways God has of making up his people\'s losses and balancing
their disgraces. Perhaps David, in making Heman the chief, had some
respect to his old friend Samuel. 2. Of the house of Gershom was Asaph,
called his brother, because in the same office and of the same tribe,
though of another family. He was posted on Heman\'s right hand in the
choir, v. 39. Several of the psalms bear his name, being either penned
by him or tuned by him as the chief musician. It is plain that he was
the penman of some psalms; for we read of those that praised the Lord in
the words of David and of Asaph. He was a seer as well as a singer, 2
Chr. 29:30. His pedigree is traced up here, through names utterly
unknown, as high as Levi, v. 39-43. 3. Of the house of Merari was Ethan
(v. 44), who was appointed to Heman\'s left hand. His pedigree is also
traced up to Levi, v. 47. If these were the Heman and Ethan that penned
the 88th and 89th psalms, there appears no reason here why they should
be called Ezrahites (see the titles of those psalms), as there does why
those should be called so who are mentioned ch. 2:6, and who were the
sons of Zerah.

`II.` There was serving-work, abundance of service to be done in the
tabernacle of the house of God (v. 48), to provide water and fuel,-to
wash and sweep, and carry out ashes,-to kill, and flay, and boil the
sacrifices; and to all such services there were Levites appointed, those
of other families, or perhaps those that were not fit to be singers,
that had either no good voice or no good ear. As every one has received
the gift, so let him minister. Those that could not sing must not
therefore be laid aside as good for nothing; though they were not fit
for that service, there was other service they might be useful in.

`III.` There was sacrificing-work, and that was to be done by the priests
only, v. 49. They only were to sprinkle the blood and burn the incense;
as for the work of the most holy place, that was to be done by the high
priest only. Each had his work, and they both needed one another and
both helped one another in it. Concerning the work of the priests we are
here told, 1. What was the end they were to have in their eye. They were
to make an atonement for Israel, to mediate between the people and God;
not to magnify and enrich themselves, but to serve the public. They were
ordained for men. 2. What was the rule they were to have in their eye.
They presided in God\'s house, yet must do as they were bidden,
according to all that God commanded. That law the highest are subject
to.

### Verses 54-81

We have here an account of the Levites\' cities. They are here called
their castles (v. 54), not only because walled and fortified, and well
guarded by the country (for it is the interest of every nation to
protect its ministers), but because they and their possessions were, in
a particular manner, the care of the divine providence: as God was their
portion, so God was their protection; and a cottage will be a castle to
those that abide under the shadow of the Almighty. This account is much
the same with that which we had, Jos. 21. We need not be critical in
comparing them (what good will it do us?) nor will it do any hurt to the
credit of the holy scripture if the names of some of the places be not
spelt just the same here as they were there. We know it is common for
cities to have several names. Sarum and Salisbury, Salop and Shrewsbury,
are more unlike than Hilen (v. 58) and Holon (Jos. 21:15), Ashan (v. 59)
and Ain (Jos. 21:16), Alemeth (v. 60) and Almon (Jos. 21:18); and time
changes names. We are only to observe that in this appointment of cities
for the Levites God took care, 1. For the accomplishment of dying
Jacob\'s prediction concerning this tribe, that it should be scattered
in Israel, Gen. 49:7. 2. For the diffusing of the knowledge of himself
and his law to all parts of the land of Israel. Every tribe had
Levites\' cities in it; and so every room was furnished with a candle,
so that none could be ignorant of his duty but it was either his own
fault or the Levites\'. 3. For a comfortable maintenance for those that
ministered in holy things. Besides their tithes and offerings, they had
glebe-lands and cities of their own to dwell in. Some of the most
considerable cities of Israel fell to the Levites\' lot. Every tribe had
benefit by the Levites, and therefore every tribe must contribute to
their support. Let him that is taught in the word communicate to him
that teacheth, and do it cheerfully.
