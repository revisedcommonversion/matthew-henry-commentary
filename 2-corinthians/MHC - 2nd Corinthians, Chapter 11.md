2nd Corinthians, Chapter 11
===========================

Commentary
----------

In this chapter the apostle goes on with his discourse, in opposition to
the false apostles, who were very industrious to lessen his interest and
reputation among the Corinthians, and had prevailed too much by their
insinuations. `I.` He apologizes for going about to commend himself, and
gives the reason for what he did (v. 1-4). `II.` He mentions, in his own
necessary vindication, his equality with the other apostles, and with
the false apostles in this particular of preaching the gospel to the
Corinthians freely, without wages (v. 5-15). `III.` He makes another
preface to what he was about further to say in his own justification (v.
16-21). And, `IV.` He gives a large account of his qualifications,
labours, and sufferings, in which he exceeded the false apostles (v. 22
to the end).

### Verses 1-4

Here we may observe, 1. The apology the apostle makes for going about to
commend himself. He is loth to enter upon this subject of
self-commendation: Would to God you could bear with me a little in my
folly, v. 1. He calls this folly, because too often it is really no
better. In his case it was necessary; yet, seeing others might apprehend
it to be folly in him, he desires them to bear with it. Note, As much
against the grain as it is with a proud man to acknowledge his
infirmities, so much is it against the grain with a humble man to speak
in his own praise. It is no pleasure to a good man to speak well of
himself, yet in some cases it is lawful, namely, when it is for the
advantage of others, or for our own necessary vindication; as thus it
was here. For, 2. We have the reasons for what the apostle did. `(1.)` To
preserve the Corinthians from being corrupted by the insinuations of the
false apostles, v. 2, 3. He tells them he was jealous over them with
godly jealousy; he was afraid lest their faith should be weakened by
hearkening to such suggestions as tended to lessen their regard to his
ministry, by which they were brought to the Christian faith. He had
espoused them to one husband, that is, converted them to Christianity
(and the conversion of a soul is its marriage to the Lord Jesus); and he
was desirous to present them as a chaste virgin-pure, and spotless, and
faithful, not having their minds corrupted with false doctrines by false
teachers, as Eve was beguiled by the subtlety of the serpent. This godly
jealousy in the apostle was a mixture of love and fear; and faithful
ministers cannot but be afraid and concerned for their people, lest they
should lose that which they have received, and turn from what they have
embraced, especially when deceivers have gone abroad, or have crept in
among them. `(2.)` To vindicate himself against the false apostles,
forasmuch as they could not pretend they had another Jesus, or another
Spirit, or another gospel, to preach to them, v. 4. If this had been the
case, there would have been some colour of reason to bear with them, or
to hearken to them. But seeing there is but one Jesus, one Spirit, and
one gospel, that is, or at least that ought to be, preached to them and
received by them, what reason could there be why the Corinthians should
be prejudiced against him, who first converted them to the faith, by the
artifices of any adversary? It was a just occasion of jealousy that such
persons designed to preach another Jesus, another Spirit, and another
gospel.

### Verses 5-15

After the foregoing preface to what he was about to say, the apostle in
these verses mentions,

`I.` His equality with the other apostles-that he was not a whit behind
the very chief of the apostles, v. 5. This he expresses very modestly: I
suppose so. He might have spoken very positively. The apostleship, as an
office, was equal in all the apostles; but the apostles, like other
Christians, differed one from another. These stars differed one from
another in glory, and Paul was indeed of the first magnitude; yet he
speaks modestly of himself, and humbly owns his personal infirmity, that
he was rude in speech, had not such a graceful delivery as some others
might have. Some think that he was a man of very low stature, and that
his voice was proportionably small; others think that he may have had
some impediment in his speech, perhaps a stammering tongue. However, he
was not rude in knowledge; he was not unacquainted with the best rules
of oratory and the art of persuasion, much less was he ignorant of the
mysteries of the kingdom of heaven, as had been thoroughly manifested
among them.

`II.` His equality with the false apostles in this particular-the
preaching of the gospel unto them freely, without wages. This the
apostle largely insists on, and shows that, as they could not but own
him to be a minister of Christ, so they ought to acknowledge he had been
a good friend to them. For, 1. He had preached the gospel to them
freely, v. 7-10. He had proved at large, in his former epistle to them,
the lawfulness of ministers\' receiving maintenance from the people, and
the duty of the people to give them an honourable maintenance; and here
he says he himself had taken wages of other churches (v. 8), so that he
had a right to have asked and received from them: yet he waived his
right, and chose rather to abase himself, by working with his hands in
the trade of tent-making to maintain himself, than be burdensome to
them, that they might be exalted, or encouraged to receive the gospel,
which they had so cheaply; yea, he chose rather to be supplied from
Macedonia than to be chargeable unto them. 2. He informs them of the
reason of this his conduct among them. It was not because he did not
love them (v. 11), or was unwilling to receive tokens of their love (for
love and friendship are manifested by mutual giving and receiving), but
it was to avoid offence, that he might cut off occasion from those that
desired occasion. He would not give occasion for any to accuse him of
worldly designs in preaching the gospel, or that he intended to make a
trade of it, to enrich himself; and that others who opposed him at
Corinth might not in this respect gain an advantage against him: that
wherein they gloried, as to this matter, they might be found even as he,
v. 12. It is not improbable to suppose that the chief of the false
teachers at Corinth, or some among them, were rich, and taught (or
deceived) the people freely, and might accuse the apostle or his
fellow-labourers as mercenary men, who received hire or wages, and
therefore the apostle kept to his resolution not to be chargeable to any
of the Corinthians.

`III.` The false apostles are charged as deceitful workers (v. 13), and
that upon this account, because they would transform themselves into the
likeness of the apostles of Christ, and, though they were the ministers
of Satan, would seem to be the ministers of righteousness. They would be
as industrious and as generous in promoting error as the apostles were
in preaching truth; they would endeavour as much to undermine the
kingdom of Christ as the apostles did to establish it. There were
counterfeit prophets under the Old Testament, who wore the garb and
learned the language of the prophets of the Lord. So there were
counterfeit apostles under the New Testament, who seemed in many
respects like the true apostles of Christ. And no marvel (says the
apostle); hypocrisy is a thing not to be much wondered at in this world,
especially when we consider the great influence Satan has upon the minds
of many, who rules in the hearts of the children of disobedience. As he
can turn himself into any shape, and put on almost any form, and look
sometimes like an angel of light, in order to promote his kingdom of
darkness, so he will teach his ministers and instruments to do the same.
But it follows, Their end is according to their works (v. 15); the end
will discover them to be deceitful workers, and their work will end in
ruin and destruction.

### Verses 16-21

Here we have a further excuse that the apostle makes for what he was
about to say in his own vindication. 1. He would not have them think he
was guilty of folly, in saying what he said to vindicate himself: Let no
man think me a fool, v. 16. Ordinarily, indeed, it is unbecoming a wise
man to be much and often speaking in his own praise. Boasting of
ourselves is usually not only a sign of a proud mind, but a mark of
folly also. However, says the apostle, yet as a fool receive me; that
is, if you count it folly in me to boast a little, yet give due regard
to what I shall say. 2. He mentions a caution, to prevent the abuse of
what he should say, telling them that what he spoke, he did not speak
after the Lord, v. 17. He would not have them think that boasting of
ourselves, or glorying in what we have, is a thing commanded by the Lord
in general unto Christians, nor yet that this is always necessary in our
own vindication; though it may be lawfully used, because not contrary to
the Lord, when, strictly speaking, it is not after the Lord. It is the
duty and practice of Christians, in obedience to the command and example
of the Lord, rather to humble and abase themselves; yet prudence must
direct in what circumstances it is needful to do that which we may do
lawfully, even speak of what God has wrought for us, and in us, and by
us too. 3. He gives a good reason why they should suffer him to boast a
little; namely, because they suffered others to do so who had less
reason. Seeing many glory after the flesh (of carnal privileges, or
outward advantages and attainments), I will glory also, v. 18. But he
would not glory in those things, though he had as much or more reason
than others to do so. But he gloried in his infirmities, as he tells
them afterwards. The Corinthians thought themselves wise, and might
think it an instance of wisdom to bear with the weakness of others, and
therefore suffered others to do what might seem folly; therefore the
apostle would have them bear with him. Or these words, You suffer fools
gladly, seeing you yourselves are wise (v. 19), may be ironical, and
then the meaning is this: \"Notwithstanding all your wisdom, you
willingly suffer yourselves to be brought into bondage under the Jewish
yoke, or suffer others to tyrannize over you; nay, to devour you, or
make a prey of you, and take of you hire for their own advantage, and to
exalt themselves above you, and lord it over you; nay, even to smite you
on the face, or impose upon you to your very faces (v. 20), upbraiding
you while they reproach me, as if you had been very weak in showing
regard to me,\" v. 21. Seeing this was the case, that the Corinthians,
or some among them, could so easily bear all this from the false
apostles, it was reasonable for the apostle to desire, and expect, they
should bear with what might seem to them an indiscretion in him, seeing
the circumstances of the case were such as made it needful that
whereinsoever any were bold he should be bold also, v. 21.

### Verses 22-33

Here the apostle gives a large account of his own qualifications,
labours, and sufferings (not out of pride or vain-glory, but to the
honour of God, who had enabled him to do and suffer so much for the
cause of Christ), and wherein he excelled the false apostles, who would
lessen his character and usefulness among the Corinthians. Observe,

`I.` He mentions the privileges of his birth (v. 22), which were equal to
any they could pretend to. He was a Hebrew of the Hebrews; of a family
among the Jews that never intermarried with the Gentiles. He was also an
Israelite, and could boast of his being descended from the beloved Jacob
as well as they, and was also of the seed of Abraham, and not of the
proselytes. It should seem from this that the false apostles were of the
Jewish race, who gave disturbance to the Gentile converts.

`II.` He makes mention also of his apostleship, that he was more than an
ordinary minister of Christ, v. 23. God had counted him faithful, and
had put him into the ministry. He had been a useful minister of Christ
unto them; they had found full proofs of his ministry: Are they
ministers of Christ? I am more so.

`III.` He chiefly insists upon this, that he had been an extraordinary
sufferer for Christ; and this was what he gloried in, or rather he
gloried in the grace of God that had enabled him to be more abundant in
labours, and to endure very great sufferings, such as stripes above
measure, frequent imprisonments, and often the dangers of death, v. 23.
Note, When the apostle would prove himself an extraordinary minister, he
proves that he had been an extraordinary sufferer. Paul was the apostle
of the Gentiles, and for that reason was hated of the Jews. They did all
they could against him; and among the Gentiles also he met with hard
usage. Bonds and imprisonments were familiar to him; never was the most
notorious malefactor more frequently in the hands of public justice than
Paul was for righteousness\' sake. The jail and the whipping-post, and
all other hard usages of those who are accounted the worst of men, were
what he was accustomed to. As to the Jews, whenever he fell into their
hands, they never spared him. Five times he fell under their lash, and
received forty stripes save one, v. 24. Forty stripes was the utmost
their law allowed (Deu. 25:3), but it was usual with them, that they
might not exceed, to abate one at least of that number. And to have the
abatement of one only was all the favour that ever Paul received from
them. The Gentiles were not tied up to that moderation, and among them
he was thrice beaten with rods, of which we may suppose once was at
Philippi, Acts 16:22. Once he was stoned in a popular tumult, and was
taken up for dead, Acts 14:19. He says that thrice he suffered
shipwreck; and we may believe him, though the sacred history gives a
relation but of one. A night and a day he had been in the deep (v. 25),
in some deep dungeon or other, shut up as a prisoner. Thus he was all
his days a constant confessor; perhaps scarcely a year of his life,
after his conversion, passed without suffering some hardship or other
for his religion; yet this was not all, for, wherever he went, he went
in perils; he was exposed to perils of all sorts. If he journeyed by
land, or voyaged by sea, he was in perils of robbers, or enemies of some
sort; the Jews, his own countrymen, sought to kill him, or do him a
mischief; the heathen, to whom he was sent, were not more kind to him,
for among them he was in peril. If he was in the city, or in the
wilderness, still he was in peril. He was in peril not only among avowed
enemies, but among those also who called themselves brethren, but were
false brethren, v. 26. Besides all this, he had great weariness and
painfulness in his ministerial labours, and these are things that will
come into account shortly, and people will be reckoned with for all the
care and pains of their ministers concerning them. Paul was a stranger
to wealth and plenty, power and pleasure, preferment and ease; he was in
watchings often, and exposed to hunger and thirst; in fastings often, it
may be out of necessity; and endured cold and nakedness, v. 27. Thus was
he, who was one of the greatest blessings of the age, used as if he had
been the burden of the earth, and the plague of his generation. And yet
this is not all; for, as an apostle, the care of all the churches lay on
him, v. 28. He mentions this last, as if this lay the heaviest upon him,
and as if he could better bear all the persecutions of his enemies than
the scandals that were to be found in the churches he had the oversight
of. Who is weak, and I am not weak? Who is offended, and I burn not? v.
29. There was not a weak Christian with whom he did not sympathize, nor
any one scandalized, but he was affected therewith. See what little
reason we have to be in love with the pomp and plenty of this world,
when this blessed apostle, one of the best of men that ever lived,
excepting Jesus Christ, felt so much hardship in it. Nor was he ashamed
of all this, but, on the contrary, it was what he accounted his honour;
and therefore, much against the grain as it was with him to glory, yet,
says he, if I must needs glory, if my adversaries will oblige me to it
in my own necessary vindication, I will glory in these my infirmities,
v. 30. Note, Sufferings for righteousness\' sake will, the most of any
thing, redound to our honour.

In the last two verses, he mentions one particular part of his
sufferings out of its place, as if he had forgotten it before, or
because the deliverance God wrought for him was most remarkable; namely,
the danger he was in at Damascus, soon after he was converted, and not
settled in Christianity, at least in the ministry and apostleship. This
is recorded, Acts 9:24, 25. This was his first great danger and
difficulty, and the rest of his life was a piece with this. And it is
observable that, lest it should be thought he spoke more than was true,
the apostle confirms this narrative with a solemn oath, or appeal to the
omniscience of God, v. 31. It is a great comfort to a good man that the
God and Father of our Lord Jesus Christ, who is an omniscient God, knows
the truth of all he says, and knows all he does and all he suffers for
his sake.
