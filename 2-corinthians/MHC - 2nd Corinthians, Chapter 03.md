2nd Corinthians, Chapter 3
==========================

Commentary
----------

The apostle makes an apology for his seeming to commend himself, and is
careful not to assume too much to himself, but to ascribe all praise
unto God (v. 1-5). He then draws a comparison between the Old Testament
and the New, and shows the excellency of the later above the former (v.
6-11), whence he infers what is the duty of gospel ministers, and the
advantage of those who live under the gospel above those who lived under
the law (v. 12 to the end).

### Verses 1-5

In these verses,

`I.` The apostle makes an apology for seeming to commend himself. He
thought it convenient to protest his sincerity to them, because there
were some at Corinth who endeavoured to blast his reputation; yet he was
not desirous of vain-glory. And he tells them, 1. That he neither needed
nor desired any verbal commendation to them, nor letters testimonial
from them, as some others did, meaning the false apostles or teachers,
v. 1. His ministry among them had, without controversy, been truly great
and honourable, how little soever his person was in reality, or how
contemptible soever some would have him thought to be. 2. The
Corinthians themselves were his real commendation, and a good
testimonial for him, that God was with him of a truth, that he was sent
of God: You are our epistle, v. 2. This was the testimonial he most
delighted in, and what was most dear to him-they were written in his
heart; and this he could appeal to upon occasion, for it was, or might
be, known and read of all men. Note, There is nothing more delightful to
faithful ministers, nor more to their commendation, than the success of
their ministry, evidenced in the hearts and lives of those among whom
they labour.

`II.` The apostle is careful not to assume too much to himself, but to
ascribe all the praise to God. Therefore, 1. He says they were the
epistle of Christ, v. 3. The apostle and others were but instruments,
Christ was the author of all the good that was in them. The law of
Christ was written in their hearts, and the love of Christ shed abroad
in their hearts. This epistle was not written with ink, but with the
Spirit of the living God; nor was it written in tables of stone, as the
law of God given to Moses, but on the heart; and that heart not a stony
one, but a heart of flesh, upon the fleshy (not fleshly, as fleshliness
denotes sensuality) tables of the heart, that is, upon hearts that are
softened and renewed by divine grace, according to that gracious
promise, I will take away the stony heart, and I will give you a heart
of flesh, Eze. 36:26. This was the good hope the apostle had concerning
these Corinthians (v. 4) that their hearts were like the ark of the
covenant, containing the tables of the law and the gospel, written with
the finger, that is, by the Spirit, of the living God. 2. He utterly
disclaims the taking of any praise to themselves, and ascribes all the
glory to God: \"We are not sufficient of ourselves, v. 5. We could never
have made such good impressions on your hearts, nor upon our own. Such
are our weakness and inability that we cannot of ourselves think a good
thought, much less raise any good thoughts or affections in other men.
All our sufficiency is of God; to him therefore are owing all the praise
and glory of that good which is done, and from him we must receive grace
and strength to do more.\" This is true concerning ministers and all
Christians; the best are no more than what the grace of God makes them.
Our hands are not sufficient for us, but our sufficiency is of God; and
his grace is sufficient for us, to furnish us for every good word and
work.

### Verses 6-11

Here the apostle makes a comparison between the Old Testament and the
New, the law of Moses and the gospel of Jesus Christ, and values himself
and his fellow-labourers by this, that they were able ministers of the
New Testament, that God had made them so, v. 6. This he does in answer
to the accusations of false teachers, who magnify greatly the law of
Moses.

`I.` He distinguishes between the letter and the spirit even of the New
Testament, v. 6. As able ministers of the New Testament, they were
ministers not merely of the letter, to read the written word, or to
preach the letter of the gospel only, but they were ministers of the
Spirit also; the Spirit of God did accompany their ministrations. The
letter killeth; this the letter of the law does, for that is the
ministration of death; and if we rest only in the letter of the gospel
we shall be never the better for so doing, for even that will be a
savour of death unto death; but the Spirit of the gospel, going along
with the ministry of the gospel, giveth life spiritual and life eternal.

`II.` He shows the difference between the Old Testament and the New, and
the excellency of the gospel above the law. For, 1. The Old-Testament
dispensation was the ministration of death (v. 7), whereas that of the
New Testament is the ministration of life. The law discovered sin, and
the wrath and curse of God. This showed us a God above us and a God
against us; but the gospel discovers grace, and Emmanuel, God with us.
Upon this account the gospel is more glorious than the law; and yet that
had a glory in it, witness the shining of Moses\'s face (an indication
thereof) when he came down from the mount with the tables in his hand,
that reflected rays of brightness upon his countenance. 2. The law was
the ministration of condemnation, for that condemned and cursed every
one who continued not in all things written therein to do them; but the
gospel is the ministration of righteousness: therein the righteousness
of God by faith is revealed. This shows us that the just shall live by
his faith. This reveals the grace and mercy of God through Jesus Christ,
for obtaining the remission of sins and eternal life. The gospel
therefore so much exceeds in glory that in a manner it eclipses the
glory of the legal dispensation, v. 10. As the shining of a burning lamp
is lost, or not regarded, when the sun arises and goes forth in his
strength; so there was no glory in the Old Testament, in comparison with
that of the New. 3. The law is done away, but the gospel does and shall
remain, v. 11. Not only did the glory of Moses\'s face go away, but the
glory of Moses\'s law is done away also; yea, the law of Moses itself is
now abolished. That dispensation was only to continue for a time, and
then to vanish away; whereas the gospel shall remain to the end of the
world, and is always fresh and flourishing and remains glorious.

### Verses 12-18

In these verses the apostle draws two inferences from what he had said
about the Old and New Testament:-

`I.` Concerning the duty of the ministers of the gospel to use great
plainness or clearness of speech. They ought not, like Moses, to put a
veil upon their faces, or obscure and darken those things which they
should make plain. The gospel is a more clear dispensation than the law;
the things of God are revealed in the New Testament, not in types and
shadows, and ministers are much to blame if they do not set spiritual
things, and gospel-truth and grace, in the clearest light that is
possible. Though the Israelites could not look stedfastly to the end of
what was commanded, but is now abolished, yet we may. We may see the
meaning of those types and shadows by the accomplishment, seeing the
veil is done away in, Christ and he is come, who was the end of the law
for righteousness to all those who believe, and whom Moses and all the
prophets pointed to, and wrote of.

`II.` Concerning the privilege and advantage of those who enjoy the
gospel, above those who lived under the law. For, 1. Those who lived
under the legal dispensation had their minds blinded (v. 14), and there
was a veil upon their hearts, v. 15. Thus it was formerly, and so it was
especially as to those who remained in Judaism after the coming of the
Messiah and the publication of his gospel. Nevertheless, the apostle
tells us, there is a time coming when this veil also shall be taken
away, and when it (the body of that people) shall turn to the Lord, v.
16. Or, when any particular person is converted to God, then the veil of
ignorance is taken away; the blindness of the mind, and the hardness of
the heart, are cured. 2. The condition of those who enjoy and believe
the gospel is much more happy. For, `(1.)` They have liberty: Where the
Spirit of the Lord is, and where he worketh, as he does under the
gospel-dispensation, there is liberty (v. 17), freedom from the yoke of
the ceremonial law, and from the servitude of corruption; liberty of
access to God, and freedom of speech in prayer. The heart is set at
liberty, and enlarged, to run the ways of God\'s commandments. `(2.)` They
have light; for with open face we behold the glory of the Lord, v. 18.
The Israelites saw the glory of God in a cloud, which was dark and
dreadful; but Christians see the glory of the Lord as in a glass, more
clearly and comfortably. It was the peculiar privilege of Moses for God
to converse with him face to face, in a friendly manner; but now all
true Christians see him more clearly with open face. He showeth them his
glory. `(3.)` This light and liberty are transforming; we are changed into
the same image, from glory to glory (v. 18), from one degree of glorious
grace unto another, till grace here be consummated in glory for ever.
How much therefore should Christians prize and improve these privileges!
We should not rest contented without an experimental knowledge of the
transforming power of the gospel, by the operation of the Spirit,
bringing us into a conformity to the temper and tendency of the glorious
gospel of our Lord and Saviour Jesus Christ.
