Introduction to 2nd Corinthians
===============================

In his former epistle the apostle had signified his intentions of coming
to Corinth, as he passed through Macedonia (16:5), but, being
providentially hindered for some time, he writes this second epistle to
them about a year after the former; and there seem to be these two
urgent occasions:- 1. The case of the incestuous person, who lay under
censure, required that with all speed he should be restored and received
again into communion. This therefore he gives directions about (ch. 2),
and afterwards (ch. 7) he declares the satisfaction he had upon the
intelligence he received of their good behaviour in that affair. 2.
There was a contribution now making for the poor saints at Jerusalem, in
which he exhorts the Corinthians to join (ch. 8, 9).

There are divers other things very observable in this epistle; for
example, `I.` The account the apostle gives of his labours and success in
preaching the gospel in several places, ch. 2. `II.` The comparison he
makes between the Old and New Testament dispensation, ch. 3. `III.` The
manifold sufferings that he and his fellow-labourers met with, and the
motives and encouragements for their diligence and patience, ch. 4, 5.
IV. The caution he gives the Corinthians against mingling with
unbelievers, ch. 6. `V.` The way and manner in which he justifies himself
and his apostleship from the opprobrious insinuations and accusations of
false teachers, who endeavoured to ruin his reputation at Corinth, ch.
10-12, and throughout the whole epistle.
