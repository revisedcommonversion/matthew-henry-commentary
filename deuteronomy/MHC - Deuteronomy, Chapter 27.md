Deuteronomy, Chapter 27
=======================

Commentary
----------

Moses having very largely and fully set before the people their duty,
both to God and one another, in general and in particular
instances,-having shown them plainly what is good, and what the law
requires of them,-and having in the close of the foregoing chapter laid
them under the obligation both of the command and the covenant, he comes
in this chapter to prescribe outward means, `I.` For the helping of their
memories, that they might not forget the law as a strange thing. They
must write all the words of this law upon stones (v. 1-10). `II.` For the
moving of their affections, that they might not be indifferent to the
law as a light thing. Whey they came into Canaan, the blessings and
curses which were the sanctions of the law, were to be solemnly
pronounced in the hearing of all Israel, who were to say Amen to them
(v. 11-26). And if such a solemnity as this would not make a deep
impression upon them, and affect them with the great things of God\'s
law, nothing would.

### Verses 1-10

Here is, `I.` A general charge to the people to keep God\'s commandments;
for in vain did they know them, unless they would do them. This is
pressed upon them, 1. With all authority. Moses with the elders of
Israel, the rulers of each tribe (v. 1), and again, Moses and the
priests the Levites (v. 9); so that the charge is given by Moses who was
king in Jeshurun, and by their lords, both spiritual and temporal, in
concurrence with him. Lest they should think that it was Moses only, an
old and dying man, that made such ado about religion, or the priests and
Levites only, whose trade it was to attend religion and who had their
maintenance out of it, the elders of Israel, whom God had placed in
honour and power over them, and who were men of business in the world
and likely to be so long so when Moses was gone, they commanded their
people to keep God\'s law. Moses, having put some of his honour upon
them, joins them in commission with himself, in giving this charge, as
Paul sometimes in his epistles joins with himself Silvanus and
Timotheus. Note, All that have any interest in others, or power over
them, should use it for the support and furtherance of religion among
them. Though the supreme power of a nation provide ever so good laws for
this purpose, if inferior magistrates in their places, and ministers in
theirs, and masters of families in theirs, do not execute their offices,
it will all be to little effect. 2. With all importunity. They press it
upon them with the utmost earnestness (v. 9, 10): Take heed and hearken,
O Israel. It is a thing that requires and deserves the highest degree of
caution and attention. They tell them of their privilege and honour:
\"This day thou hast become the people of the Lord thy God, the Lord
having avouched thee to be his own, and being now about to put thee in
possession of Canaan which he had long promised as thy God (Gen. 17:7,
8), and which if he had failed to do in due time, he would have been
ashamed to be called thy God, Heb. 11:16. Now thou art more than ever
his people, therefore obey his voice.\" Privileges should be improved as
engagements to duty. Should not a people be ruled by their God?

`II.` A particular direction to them with great solemnity to register the
words of this law, as soon as they came into Canaan. It was to be done
but once, and at their entrance into the land of promise, in token of
their taking possession of it under the several provisos and conditions
contained in this law. There was a solemn ratification of the covenant
between God and Israel at Mount Sinai, when an altar was erected, with
twelve pillars, and the book of the covenant was produced, Ex. 24:4.
That which is here appointed is a somewhat similar solemnity.

`1.` They must set up a monument on which they must write the words of
this law. `(1.)` The monument itself was to be very mean, only rough
unhewn stone plastered over; not polished marble or alabaster, nor brass
tables, but common plaster upon stone, v. 2. The command is repeated (v.
4), and orders are given that it be written, not very finely, to be
admired by the curious, but very plainly, that he who runs may read it,
Hab. 2:2. The word of God needs not to be set off by the art of man, nor
embellished with the enticing words of man\'s wisdom. But, `(2.)` The
inscription was to be very great: All the words of this law, v. 3, and
again, v. 8. Some understand it only of the covenant between God and
Israel, mentioned ch. 26:17, 18. Let this help be set up for a witness,
like that memorial of the covenant between Laban and Jacob, which was
nothing but a heap of stones thrown hastily together, upon which they
did eat together in token of friendship (Gen. 31:46, 47), and that stone
which Joshua set up, Jos. 24:26. Others think that the curses of the
covenant in this chapter were written upon this monument, the rather
because it was set up in Mount Ebal, v. 4. Others think that the whole
book of Deuteronomy was written upon this monument, or at least the
statutes and judgments from ch. 12 to the end of ch. 26. And it is not
improbable that the heap might be so large as, taking in all the sides
of it, to contain so copious an inscription, unless we will suppose (as
some do) that the ten commandments only were here written, as an
authentic copy of the close rolls which were laid up in the ark. They
must write this when they had gone into Canaan, and yet Moses says (v.
3), \"Write it that thou mayest go in,\" that is, \"that thou mayest go
in with comfort, and assurance of success and settlement, otherwise it
were well for thee not to go in at all. Write it as the conditions of
thy entry, and own that thou comest in upon these terms and no other:
since Canaan is given by promise, it must be held by obedience.\"

`2.` They must also set up an altar. By the words of the law which were
written upon the plaster, God spoke to them; by the altar, and the
sacrifices offered upon it, they spoke to God; and thus was communion
kept up between them and God. The word and prayer must go together.
Though they might not, of their own heads, set up any altar besides that
at the tabernacle, yet, but the appointment of God, they might upon a
special occasion. Elijah built a temporary altar of twelve unhewn
stones, similar to this, when he brought Israel back to the covenant
which was now made, 1 Ki. 18:31, 32. Now, `(1.)` This altar must be made
of such stones as they found ready upon the field, not newly cut out of
the rock, much less squared artificially: Thou shalt not lift up any
iron tool upon them, v. 5. Christ, our altar, is a stone cut out of the
mountain without hands (Dan. 2:34, 35), and therefore refused by the
builders, as having no form or comeliness, but accepted of God the
Father, and made the head of the corner. `(2.)` Burnt-offerings and
peace-offerings must be offered upon this altar (v. 6,7), that by them
they might give glory to God and obtain favour. Where the law was
written, an altar was set up close by it, to signify that we could not
look with any comfort upon the law, being conscious to ourselves of the
violation of it, if it were not for the great sacrifice by which
atonement is made for sin; and the altar was set up on Mount Ebal, the
mount on which those tribes stood that said Amen to the curses, to
intimate that through Christ we are redeemed from the curse of the law.
In the Old Testament the words of the law are written, with the curse
annexed, which would fill us with horror and amazement if we had not in
the New Testament (which is bound up with it) an altar erected close by
it, which gives us everlasting consolation. `(3.)` They must eat there,
and rejoice before the Lord their God, v. 7. This signified, `[1.]` The
consent they gave to the covenant; for the parties to a covenant
ratified the covenant by feasting together. They were partakers of the
altar, which was God\'s table, as his servants and tenants, and such
they acknowledged themselves, and, being put in possession of this good
land, bound themselves to pay the rent and to do the services reserved
by the royal grant. `[2.]` The comfort they took in the covenant; they
had reason to rejoice in the law, when they had an altar, a remedial
law, so near it. It was a great favour to them, and a token for good,
that God gave them his statutes; and that they were owned as the people
of God, and the children of the promise, was what they had reason to
rejoice in, though, when this solemnity was to be performed, they were
not put in full possession of Canaan; but God has spoken in his
holiness, and then I will rejoice, Gilead is mine, Manasseh is mine; all
my own.

### Verses 11-26

When the law was written, to be seen and read by all men, the sanctions
of it were to be published, which, to complete the solemnity of their
covenanting with God, they were deliberately to declare their
approbation of. This they were before directed to do (ch. 11:29, 30),
and therefore the appointment here begins somewhat abruptly, v. 12.
There were, it seems, in Canaan, that part of it which afterwards fell
to the lot of Ephraim (Joshua\'s tribe), two mountains that lay near
together, with a valley between, one called Gerizim and the other Ebal.
On the sides of these two mountains, which faced one another, all the
tribes were to be drawn up, six on one side and six on the other, so
that in the valley, at the foot of each mountain, they came pretty near
together, so near as that the priests standing betwixt them might be
heard by those that were next them on both sides; then when silence was
proclaimed, and attention commanded, one of the priests, or perhaps more
at some distance from each other, pronounced with a loud voice one of
the curses here following, and all the people that stood on the side and
foot of Mount Ebal (those that stood further off taking the signal from
those that stood nearer and within hearing) said Amen; then the contrary
blessing was pronounced, \"Blessed is he that doth not so or so,\" and
then those that stood on the side, and at the foot, of Mount Gerizim,
said Amen. This could not but affect them very much with the blessings
and curses, the promises and threatenings, of the law, and not only
acquaint all the people with them, but teach them to apply them to
themselves.

`I.` Something is to be observed, in general, concerning this solemnity,
which was to be done, but once and not repeated, but would be talked of
to posterity,. 1. God appointed which tribes should stand upon Mount
Gerizim and which on Mount Ebal (v. 12, 13), to prevent the disputes
that might have arisen if they had been left to dispose of themselves.
The six tribes that were appointed for blessing were all the children of
the free women, for to such the promise belongs, Gal. 4:31. Levi is here
put among the rest, to teach ministers to apply to themselves the
blessing and curse which they preach to others, and by faith to set
their own Amen to it. 2. Of those tribes that were to say Amen to the
blessings it is said, They stood to bless the people, but of the other,
They stood to curse, not mentioning the people, as loth to suppose that
any of this people whom God had taken for his own should lay themselves
under the curse. Or, perhaps, the different mode of expression intimates
that there was to be but one blessing pronounced in general upon the
people of Israel, as a happy people, and that should ever be so, if they
were obedient; and to this blessing the tribes on Mount Gerizim were to
say Amen-\"Happy art thou, O Israel, and mayest thou ever be so;\" but
then the curses come in as exceptions from the general rule, and we know
exceptio firmat regulam-the exception confirms the rule. Israel is a
blessed people, but, if there be any particular persons even among them
that do such and such things as are mentioned, let them know that they
have no part nor lot in the matter, but are under a curse. This shows
how ready God is to bestow the blessing; if any fall under the curse,
they may thank themselves, they bring it upon their own heads. 3. The
Levites or priests, such of them as were appointed for that purpose,
were to pronounce the curses as well as the blessings. They were
ordained to bless (ch. 10:8), the priests did it daily, Num. 6:23. But
they must separate between the precious and the vile; they must not give
that blessing promiscuously, but must declare it to whom it did not
belong, lest those who had no right to it themselves should think to
share in it by being in the crowd. Note, Ministers must preach the
terrors of the law as well as the comforts of the gospel; must not only
allure people to their duty with the promises of a blessing, but awe
them to it with the threatenings of a curse. 4. The curses are here
expressed, but not the blessings; for as many as were under the law were
under the curse, but it was a honour reserved for Christ to bless us,
and so to do that for us which the law could not do, in that it was
weak. In Christ\'s sermon upon the mount, which was the true Mount
Gerizim, we have blessings only, Mt. 5:3, etc. 5. To each of the curses
the people were to say Amen. It is easy to understand the meaning of
Amen to the blessings. The Jews have a saying to encourage people to say
Amen to the public prayers, Whosoever answereth Amen, after him that
blesseth, he is as he that blesseth. But how could they say Amen to the
curses? `(1.)` It was a profession of their faith in the truth of them,
that these and the like curses were not bug-bears to frighten children
and fools, but the real declarations of the wrath of God against the
ungodliness and unrighteousness of men, not one iota of which shall fall
to the ground. `(2.)` It was an acknowledgment of the equity of these
curses; when they said Amen, they did in effect say, not only, It is
certain it shall be so, but, It is just it should be so. Those who do
such things deserve to fall and lie under the curse. `(3.)` It was such an
imprecation upon themselves as strongly obliged them to have nothing to
do with those evil practices upon which the curse is here entailed.
\"Let God\'s wrath fall upon us if ever we do such things.\" We read of
those that entered into a curse (and with us that is the usual form of a
solemn oath) to walk in God\'s law Neh. 10:29. Nay, the Jews say (as the
learned bishop Patrick quotes them), \"All the people, by saying this
Amen, became bound for one another, that they would observe God\'s laws,
by which every man was obliged, as far as he could, to prevent his
neighbour from breaking these laws, and to reprove those that had
offended, lest they should bear sin and the curse for them.\"

`II.` Let us now observe what are the particular sins against which the
curses are here denounced.

`1.` Sins against the second commandment. This flaming sword is set to
keep that commandment first, v. 15. Those are here cursed, not only that
worship images, but that make them or keep them, if they be such (or
like such) as idolaters used in the service of their gods. Whether it be
a graven image or a molten image, it comes all to one, it is an
abomination to the Lord, even though it be not set up in public, but in
a secret place,-though it be not actually worshipped, nor is it said to
be designed for worship, but reserved there with respect and a constant
temptation. He that does this may perhaps escape punishment from men,
but he cannot escape the curse of God.

`2.` Against the fifth commandment, v. 16. The contempt of parents is a
sin so heinous that it is put next to the contempt of God himself. If a
man abused his parents, either in word or deed, he fell under the
sentence of the magistrate, and must be put to death, Ex. 21:15, 17. But
to set light by them in his heart was a thing which the magistrate could
not take cognizance of, and therefore it is here laid under the curse of
God, who knows the heart. Those are cursed children that carry
themselves scornfully and insolently towards their parents.

`3.` Against the eighth commandment. The curse of God is here fastened,
`(1.)` Upon an unjust neighbour that removes the land-marks, v. 17. See
ch. 19:14. Upon an unjust counsellor, who, when his advice is asked,
maliciously directs his friend to that which he knows will be to his
prejudice, which is making the blind to wander out of the way, under
pretence of directing him in the way, than which nothing can be either
more barbarous or more treacherous, v. 18. Those that seduce others from
the way of God\'s commandments, and entice them to sin, bring this curse
upon themselves, which our Saviour has explained, Mt. 15:14, The blind
lead the blind, and both shall fall into the ditch. `(3.)` Upon an unjust
judge, that perverteth the judgment of the stranger, fatherless, and
widow, whom he should protect and vindicate, v. 19. These are supposed
to be poor and friendless (nothing to be got by doing them a kindness,
nor any thing lost by disobliging them), and therefore judges may be
tempted to side with their adversaries against right and equity; but
cursed are such judges.

`4.` Against the seventh commandment. Incest is a cursed sin, with a
sister, a father\'s wife, or a mother-in-law, v. 20, 22, 23. These
crimes not only exposed men to the sword of the magistrate (Lev. 20:11),
but, which is more dreadful, to the wrath of God; bestiality likewise,
v. 21.

`5.` Against the sixth commandment. Two of the worst kinds of murder are
here specified:-`(1.)` Murder unseen, when a man does not set upon his
neighbour as a fair adversary, giving him an opportunity to defend
himself, but smites him secretly (v. 24), as by poison or otherwise,
when he sees not who hurts him. See Ps. 10:8, 9. Though such secret
murders may go undiscovered and unpunished, yet the curse of God will
follow them. `(2.)` Murder under colour of law, which is the greatest
affront to God, for it makes an ordinance of his to patronise the worst
of villains, and the greatest wrong to our neighbour, for it ruins his
honour as well as his life: cursed therefore is he that will be hired,
or bribed, to accuse, or to convict, or to condemn, and so to slay, an
innocent person, v. 25. See Ps. 15:5.

`6.` The solemnity concludes with a general curse upon him that
confirmeth not, or, as it might be read, that performeth not, all the
words of this law to do them, v. 26. By our obedience to the law we set
our seal to it, and so confirm it, as by our disobedience we do what
lies in us to disannul it, Ps. 119:126. The apostle, following all the
ancient versions, reads it, Cursed is every one that continues not, Gal.
3:10. Lest those who were guilty of other sins, not mentioned in this
commination, should think themselves safe from the curse, this last
reaches all; not only those who do the evil which the law forbids, but
those also who omit the good which the law requires: to this we must all
say Amen, owning ourselves under the curse, justly to have deserved it,
and that we must certainly have perished for ever under it, if Christ
had not redeemed us from the curse of the law, by being made a curse for
us.
