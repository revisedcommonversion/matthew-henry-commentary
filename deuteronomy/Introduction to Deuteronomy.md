Introduction to Deuteronomy
===========================

This book is a repetition of very much both of the history and of the
laws contained in the three foregoing books, which repetition Moses
delivered to Israel (both by word of mouth, that it might affect, and by
writing, that it might abide) a little before his death. There is no new
history in it but that of the death of Moses in the last chapter, nor
any new revelation to Moses, for aught that appears, and therefore the
style here is not, as before, The Lord spoke unto Moses, saying. But the
former laws are repeated and commented upon, explained and enlarged, and
some particular precepts added to them, with copius reasonings for the
enforcing of them: in this Moses was divinely inspired and assisted, so
that this is as truly the word of the Lord by Moses as that which was
spoken to him with an audible voice out of the tabernacle of the
congregation, Lev. 1:1. The Greek interpreters call it Deuteronomy,
which signifies the second law, or a second edition of the law, not with
amendments, for there needed none, but with additions, for the further
direction of the people in divers cases not mentioned before. Now, `I.` It
was much for the honour of the divine law that it should be thus
repeated; how great were the things of that law which was thus
inculcated, and how inexcusable would those be by whom they were counted
as a strange thing! Hos. 8:12. `II.` There might be a particular reason
for the repeating of it now; the men of that generation to which the law
was first given were all dead, and a new generation had sprung up, to
whom God would have it repeated by Moses himself, that, if possible, it
might make a lasting impression upon them. Now that they were just going
to take possession of the land of Canaan, Moses must read the articles
of agreement to them, that they might know upon what terms and
conditions they were to hold and enjoy that land, and might understand
that they were upon their good behaviour in it. `III.` It would be of
great use to the people to have those parts of the law thus gathered up
and put together which did more immediately concern them and their
practice; for the laws which concerned the priests and Levites, and the
execution of their offices, are not repeated: it was enough for them
that they were once delivered. But, in compassion to the infirmities of
the people, the laws of more common concern are delivered a second time.
Precept must be upon precept, and line upon line, Isa. 28:10. The great
and needful truths of the gospel should be often pressed upon people by
the ministers of Christ. To write the same things (says Paul, Phil. 3:1)
to me indeed is not grievous, but for you it is safe. What God has
spoken once we have need to hear twice, to hear many times, and it is
well if, after all, it be duly perceived and regarded. In three ways
this book of Deuteronomy was magnified and made honourable:-1. The king
was to write a copy of it with his own hand, and to read therein all the
days of his life, ch. 17, 18, 19. 2. It was to be written upon great
stones plastered, at their passing over Jordan, ch. 27:2, 3. 3. It was
to be read publicly every seventh year, at the feast of tabernacles, by
the priests, in the audience of all Israel, ch. 31:9, etc. The gospel is
a kind of Deuteronomy, a second law, a remedial law, a spiritual law, a
law of faith; by it we are under the law of Christ, and it is a law that
makes the comers thereunto perfect.

This book of Deuteronomy begins with a brief rehearsal of the most
remarkable events that had befallen the Israelites since they came from
Mount Sinai. In the fourth chapter we have a most pathetic exhortation
to obedience. In the twelfth chapter, and so on to the twenty-seventh,
are repeated many particular laws, which are enforced (ch. 27 and 28)
with promises and threatenings, blessings and curses, formed into a
covenant, ch. 29 and 30. Care is taken to perpetuate the remembrance of
these things among them (ch. 31), particularly by a song (ch. 32), and
so Moses concludes with a blessing, ch. 33. All this was delivered by
Moses to Israel in the last month of his life. The whole book contains
the history but of two months; compare ch. 1:3 with Jos. 4:19, the
latter of which was the thirty days of Israel\'s mourning for Moses; see
how busy that great and good man was to do good when he knew that his
time was short, how quick his motion when he drew near his rest. Thus we
have more recorded of what our blessed Saviour said and did in the last
week of his life than in any other. The last words of eminent persons
make or should make deep impressions. Observe, for the honour of this
book, that when our Saviour would answer the devil\'s temptations with,
It is written, he fetched all his quotations out of this book, Mt. 4:4,
7, 10.
