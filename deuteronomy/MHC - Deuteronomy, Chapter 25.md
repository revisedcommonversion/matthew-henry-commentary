Deuteronomy, Chapter 25
=======================

Commentary
----------

Here is, `I.` A law to moderate the scourging of malefactors (v. 1-3). `II.`
A law in favour of the ox the treads out the corn (v. 4). `III.` For the
disgracing of him that refused to marry his brother\'s widow (v. 5-10).
IV. For the punishment of an immodest woman (v. 11, 12). `V.` For just
weights and measures (v. 13-16). `VI.` For the destroying of Amalek (v.
17, etc.).

### Verses 1-4

Here is, `I.` A direction to the judges in scourging malefactors, v. 1-3.
`1.` It is here supposed that, if a man be charged with a crime, the
accuser and the accused (Actor and Reus) should be brought face to face
before the judges, that the controversy may be determined. 2. If a man
were accused of a crime, and the proof fell short, so that the charge
could not be made out against him by the evidence, then he was to be
acquitted: \"Thou shalt justify the righteous,\" that is, \"him that
appears to the court to be so.\" If the accusation be proved, then the
conviction of the accused is a justification of the accuser, as
righteous in the prosecution. 3. If the accused were found guilty,
judgment must be given against him: \"Thou shalt condemn the wicked;\"
for to justify the wicked is as much an abomination to the Lord as it is
to condemn the righteous, Prov. 17:15. 4. If the crime were not made
capital by the law, then the criminal must be beaten. A great many
precepts we have met with which have not any particular penalty annexed
to them, the violation of most of which, according to the constant
practice of the Jews, was punished by scourging, from which no person\'s
rank or quality did exempt him if he were a delinquent, but with this
proviso, that he should never be upbraided with it, nor should it be
looked upon as leaving any mark of infamy or disgrace upon him. The
directions here given for the scourging of criminals are, `(1.)` That it
be done solemnly; not tumultuously through the streets, but in open
court before the judge\'s face, and with so much deliberation as that
the stripes might be numbered. The Jews say that while execution was in
doing the chief justice of the court read with a loud voice Deu. 28:58,
59, and 29:9, and concluded with those words (Ps. 78:38), But he, being
full of compassion, forgave their iniquity. Thus it was made a sort of
religious act, and so much the more likely to reform the offender
himself and to be a warning to others. `(2.)` That it be done in
proportion to the crime, according to his fault, that some crimes might
appear, as they are, more heinous than others, the criminal being beaten
with many stripes, to which perhaps there is an allusion, Lu. 12:47, 48.
`(3.)` That how great soever the crime were the number of stripes should
never exceed forty, v. 3. Forty save one was the common usage, as
appears, 2 Co. 11:24. It seems, they always gave Paul as many stripes as
ever they gave to any malefactor whatsoever. They abated one for fear of
having miscounted (though one of the judges was appointed to number the
stripes), or because they would never go to the utmost rigour, or
because the execution was usually done with a whip of three lashes, so
that thirteen stripes (each one being counted for three) made up
thirty-nine, but one more by that reckoning would have been forty-two.
The reason given for this is, lest thy brother should seem vile unto
thee. He must still be looked upon as a brother (2 Th. 3:15), and his
reputation as such was preserved by this merciful limitation of his
punishment. It saves him from seeming vile to his brethren, when God
himself by his law takes this care of him. Men must not be treated as
dogs; nor must those seem vile in our sight to whom, for aught we know,
God may yet give grace to make them precious in his sight.

`II.` A charge to husbandmen not to hinder their cattle from eating when
they were working, if meat were within their reach, v. 4. This instance
of the beast that trod out the corn (to which there is an allusion in
that of the prophet, Hos. 10:11) is put for all similar instances. That
which makes this law very remarkable above its fellows (and which
countenances the like application of other such laws) is that it is
twice quoted in the New Testament to show that it is the duty of the
people to give their ministers a comfortable maintenance, 1 Co. 9:9, 10,
and 1 Tim. 5:17, 18. It teaches us in the letter of it to make much of
the brute-creatures that serve us, and to allow them not only the
necessary supports for their life, but the advantages of their labour;
and thus we must learn not only to be just, but kind, to all that are
employed for our good, not only to maintain but to encourage them,
especially those that labour among us in the word and doctrine, and so
are employed for the good of our better part.

### Verses 5-12

Here is, `I.` The law settled concerning the marrying of the brother\'s
widow. It appears from the story of Judah\'s family that this had been
an ancient usage (Gen. 38:8), for the keeping up of distinct families.
The case put is a case that often happens, of a man\'s dying without
issue, it may be in the prime of his time, soon after his marriage, and
while his brethren were yet so young as to be unmarried. Now in this
case, 1. The widow was not to marry again into any other family, unless
all the relations of her husband did refuse her, that the estate she was
endowed with might not be alienated. 2. The husband\'s brother, or next
of kin, must marry her, partly out of respect to her, who, having
forgotten her own people and her father\'s house, should have all
possible kindness shown her by the family into which she was married;
and partly out of respect to the deceased husband, that though he was
dead and gone he might not be forgotten, nor lost out of the genealogies
of his tribe; for the first-born child, which the brother or next
kinsman should have by the widow, should be denominated from him that
was dead, and entered in the genealogy as his child, v. 5, 6. Under that
dispensation we have reason to think men had not so clear and certain a
prospect of living themselves on the other side death as we have now, to
whom life and immortality are brought to light by the gospel; and
therefore they could not but be the more desirous to live in their
posterity, which innocent desire was in some measure gratified by this
law, an expedient being found out that, though a man had no child by his
wife, yet his name should not be put out of Israel, that is, out of the
pedigree, or, which is equivalent, remain there under the brand of
childlessness. The Sadducees put a case to our Saviour upon this law,
with a design to perplex the doctrine of the resurrection by it (Mt.
22:24, etc.), perhaps insinuating that there was no need of maintaining
the immortality of the soul and a future state, since the law had so
well provided for the perpetuating of men\'s names and families in the
world. But, 3. If the brother, or next of kin, declined to do this good
office to the memory of him that was gone, what must be done in that
case? Why, `(1.)` He shall not be compelled to do it, v. 7. If he like her
not, he is at liberty to refuse her, which, some think, was not
permitted in this case before this law of Moses. Affection is all in all
to the comfort of the conjugal relation; this is a thing which cannot be
forced, and therefore the relation should not be forced without it. `(2.)`
Yet he shall be publicly disgraced for not doing it. The widow, as the
person most concerned for the name and honour of the deceased, was to
complain to the elders of his refusal; if he persist in it, she must
pluck off his shoe, and spit in his face, in open court (or, as the
Jewish doctors moderate it, spit before his face), thus to fasten a mark
of infamy upon him, which was to remain with his family after him, v.
8-10. Note, Those justly suffer in their own reputation who do not do
what they ought to preserve the name and honour of others. He that would
not build up his brother\'s house deserved to have this blemish put upon
his own, that it should be called the house of him that had his shoe
loosed, in token that he deserved to go barefoot. In the case of Ruth we
find this law executed (Ruth 4:7), but because, upon the refusal of the
next kinsman, there was another ready to perform the duty of a
husband\'s brother, it was that other that plucked off the shoe, and not
the widow-Boaz, and not Ruth.

`II.` A law for the punishing of an immodest woman, v. 11, 12. The woman
that by the foregoing law was to complain against her husband\'s brother
for not marrying her, and to spit in his face before the elders, needed
a good measure of assurance; but, lest the confidence which that law
supported should grow to an excess unbecoming the sex, here is a very
severe but just law to punish impudence and immodesty. 1. The instance
of it is confessedly scandalous to the highest degree. A woman could not
do it unless she were perfectly lost to all virtue and honour. 2. The
occasion is such as might in part excuse it; it was to help her husband
out of the hands of one that was too hard for him. Now if the doing of
it in a passion, and with such a good intention, was to be so severely
punished, much more when it was done wantonly and in lust. 3. The
punishment was that her hand should be cut off; and the magistrates must
not pretend to be more merciful than God: Thy eye shall not pity her.
Perhaps our Saviour alludes to this law when he commands us to cut off
the right hand that offends us, or is an occasion of sin to us. Better
put the greatest hardships that can be upon the body than ruin the soul
for ever. Modesty is the hedge of chastity, and therefore ought to be
very carefully preserved and kept up by both sexes.

### Verses 13-19

Here is, `I.` A law against deceitful weights and measures: they must not
only not use them, but they must not have them, not have them in the
bag, not have them in the house (v. 13, 14); for, if they had them, they
would be strongly tempted to use them. They must not have a great weight
and measure to buy by and a small one to sell by, for that was to cheat
both ways, when either was bad enough; as we read of those that made the
ephah small, in which they measured the corn they sold, and the shekel
great, by which they weighed the money they received for it, Amos 8:5.
But thou shalt have a perfect and just weight, v. 15. That which is the
rule of justice must itself be just; if that be otherwise, it is a
constant cheat. This had been taken care of before, Lev. 19:35, 36. This
law is enforced with two very good reasons:-1. That justice and equity
will bring down upon us the blessing of God. The way to have our days
lengthened, and to prosper, is to be just and fair in all our dealings
Honesty is the best policy. 2. That fraud and injustice will expose us
to the curse of God, v. 16. Not only unrighteousness itself, but all
that do unrighteously, are an abomination to the Lord. And miserable is
that man who is abhorred by his Maker. How hateful, particularly, all
the arts of deceit are to God, Solomon several times observes, Prov.
11:1; 20:10, 23; and the apostle tells us that the Lord is the avenger
of all such as overreach and defraud in any matter, 1 Th. 4:6.

`II.` A law for the rooting out of Amalek. Here is a just weight and a
just measure, that, as Amalek had measured to Israel, so it should be
measure to Amalek again.

`1.` The mischief Amalek did to Israel must be here remembered, v. 17 18.
When it was first done it was ordered to be recorded (Ex. 17:14-16), and
here the remembrance of it is ordered to be preserved, not in personal
revenge (for that generation which suffered by the Amalekites was gone,
so that those who now lived, and their posterity, could not have any
personal resentment of the injury), but in a zeal for the glory of God
(which was insulted by the Amalekites), that throne of the Lord against
which the hand of Amalek was stretched out. The carriage of the
Amalekites towards Israel is here represented, `(1.)` As very base and
disingenuous. They had no occasion at all to quarrel with Israel, nor
did they give them any notice, by a manifesto or declaration of war; but
took them at an advantage, when they had just come out of the house of
bondage, and, for aught that appeared to them, were only going to
sacrifice to God in the wilderness. `(2.)` As very barbarous and cruel;
for they smote those that were more feeble, whom they should have
succoured. The greatest cowards are commonly the most cruel; while those
that have the courage of a man will have the compassion of a man. `(3.)`
As very impious and profane: they feared not God. If they had had any
reverence for the majesty of the God of Israel, which they saw a token
of in the cloud, or any dread of his wrath, which they lately heard of
the power of over Pharaoh, they durst not have made this assault upon
Israel. Well, here was the ground of the quarrel: and it shows how God
takes what is done against his people as done against himself, and that
he will particularly reckon with those that discourage and hinder young
beginners in religion, that (as Satan\'s agents) set upon the weak and
feeble, either to divert them or to disquiet them, and offend his little
ones.

`2.` This mischief must in due time be revenged, v. 19. When their wars
were finished, by which they were to settle their kingdom and enlarge
their coast, then they must make war upon Amalek (v. 19), not merely to
chase them, but to consume them, to blot out the remembrance of Amalek.
It was an instance of God\'s patience that he deferred the vengeance so
long, which should have led the Amalekites to repentance; yet an
instance of fearful retribution that the posterity of Amalek, so long
after, were destroyed for the mischief done by their ancestors to the
Israel of God, that all the world might see, and say, that he who
toucheth them toucheth the apple of his eye. It was nearly 400 years
after this that Saul was ordered to put this sentence in execution (1
Sa. 15), and was rejected of God because he did not do it effectually,
but spared some of that devoted nation, in contempt, not only of the
particular orders he received from Samuel, but of this general command
here given by Moses, which he could not be ignorant of. David afterwards
made some destruction of them; and the Simeonites, in Hezekiah\'s time,
smote the rest that remained (1 Chr. 4:43); for when God judges he will
overcome.
