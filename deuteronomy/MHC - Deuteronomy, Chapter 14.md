Deuteronomy, Chapter 14
=======================

Commentary
----------

Moses in this chapter teaches them, `I.` To distinguish themselves from
their neighbours by a singularity, 1. In their mourning (v. 1, 2). 2. In
their meat (v. 3-21). `II.` To devote themselves unto God, and, in token
of that, to give him his dues out of their estates, the yearly tithe,
and that every third year, for the maintenance of their religious
feasts, the Levites, and the poor (v. 22, etc.).

### Verses 1-21

Moses here tells the people of Israel,

`I.` How God had dignified them, as a peculiar people, with three
distinguishing privileges, which were their honour, and figures of those
spiritual blessings in heavenly things with which God has in Christ
blessed us. 1. Here is election: The Lord hath chosen thee, v: 2. Not
for their own merit, nor for any good works foreseen, but because he
would magnify the riches of his power and grace among them. He did not
choose them because they were by their own dedication and subjection a
peculiar people to him above other nations, but he chose them that they
might be so by his grace; and thus were believers chosen, Eph, 1:4. 2.
Here is adoption (v. 1): \"You are the children of the Lord your God,
formed by him into a people, owned by him as his people, nay, his
family, a people near unto him, nearer than any other.\" Israel is my
son, my first-born; not because he needed children, but because they
were orphans, and needed a father. Every Israelite is indeed a child of
God, a partaker of his nature and favour, his love and blessing Behold
what manner of love the Father has bestowed upon us! 3. Here is
sanctification (v. 2): \"Thou art a holy people, separated and set apart
for God, devoted to his service, designed for his praise, governed by a
holy law, graced by a holy tabernacle, and the holy ordinances relating
to it.\" God\'s people are under the strongest obligations to be holy,
and, if they are holy, are indebted to the grace of God that makes them
so. The Lord has set them apart for himself, and qualified them for his
service and the enjoyment of him, and so has made them holy to himself.

`II.` How they ought to distinguish themselves by a sober singularity
from all the nations that were about them. And, God having thus advanced
them, let not them debase themselves by admitting the superstitious
customs of idolaters, and, by making themselves like them, put
themselves upon the level with them. Be you the children of the Lord
your God; so the Seventy read it, as a command, that is, \"Carry
yourselves as becomes the children of God, and do nothing to disgrace
the honour and forfeit the privileges of the relation.\" In two things
particularly they must distinguish themselves:-

`1.` In their mourning: You shall not cut yourselves, v. 1. This forbids
(as some think), not only their cutting themselves at their funerals,
either to express their grief or with their own blood to appease the
infernal deities, but their wounding and mangling themselves in the
worship of their gods, as Baal\'s prophets did (1 Ki. 18:28), or their
marking themselves by incisions in their flesh for such and such
deities, which in them, above any, would be an inexcusable crime, who in
the sign of circumcision bore about with them in their bodies the marks
of the Lord Jehovah. So that, `(1.)` They are forbidden to deform or hurt
their own bodies upon any account. Methinks this is like a parent\'s
change to his little children, that are foolish, careless, and wilful,
and are apt to play with knives: Children, you shall not cut yourselves.
This is the intention of those commands which oblige us to deny
ourselves; the true meaning of them, if we understood them aright, would
appear to be, Do yourselves no harm. And this also is the design of
those providences which most cross us, to remove from us those things by
which we are in danger of doing ourselves harm. Knives are taken from
us, lest we should cut ourselves. Those that are dedicated to God as a
holy people must do nothing to disfigure themselves; the body is for the
Lord, and is to be used accordingly. `(2.)` They are forbidden to disturb
and afflict their own minds with inordinate grief for the loss of near
and dear relations: \"You shall not express or exasperate you sorrow,
even upon the most mournful occasions, by cutting yourselves, and making
baldness between your eyes, like men enraged, or resolvedly hardened in
sorrow for the dead, as those that have no hope,\" 1 Th. 4:13. It is an
excellent passage which Mr. Ainsworth here quotes from one of the Jewish
writers, who understands this as a law against immoderate grief for the
death of our relations. If your father (for instance) die, you shall not
cut yourselves, that is, you shall not sorrow more than is meet, for you
are not fatherless, you have a Father, who is great, living, and
permanent, even the holy blessed God, whose children you are, v. 1. But
an infidel (says he), when his father dies, hath no father that can help
him in time of need; for he hath said to a stock, Thou art my father,
and to a stone, Thou hast brought me forth (Jer. 2:27); therefore he
weeps, cuts himself, and makes himself bald. We that have a God to hope
in, and a heaven to hope for, must bear up ourselves with that hope
under every burden of this kind.

`2.` They must be singular in their meat. Observe,

`(1.)` Many sorts of flesh which were wholesome enough, and which other
people did commonly eat, they must religiously abstain from as unclean.
This law we had before Lev. 11:2, where it was largely opened. It seems
plainly, by the connection here, to be intended as a mark of
peculiarity; for their observance of it would cause them to be taken
notice of in all mixed companies as a separate people, and would
preserve them from mingling themselves with, and conforming themselves
to, their idolatrous neighbours. `[1.]` Concerning beasts, here is a
more particular enumeration of those which they were allowed to eat then
was in Leviticus, to show that they had no reason to complain of their
being restrained from eating swines\' flesh, and hares, and rabbits
(which were all that were then forbidden, but are now commonly used),
when they were allowed so great a variety, not only of that which we
call butcher\'s meat (v. 4), which alone was offered in sacrifice, but
of venison, which they had great plenty of in Canaan, the hart, and the
roe-buck, and the fallow deer (v. 5), which, though never brought to
God\'s altar, was allowed them at their own table. See ch. 12:22. When
of all these (as Adam of every tree of the garden) they might freely
eat, those were inexcusable who, to gratify a perverse appetite, or (as
should seem) in honour of their idols, and in participation of their
idolatrous sacrifices, ate swines\' flesh, and had broth of abominable
things (made so by this law) in their vessels, Isa. 65:4. `[2.]`
Concerning fish there is only one general rule given, that whatsoever
had not fins and scales (as shell-fish and eels, besides leeches and
other animals in the water that are not proper food) was unclean and
forbidden, v. 9, 10. `[3.]` No general rule is given concerning fowl,
but those are particularly mentioned that were to be unclean to them,
and there are few or none of them which are here forbidden that are now
commonly eaten; and whatsoever is not expressly forbidden is allowed, v.
11-20. Of all clean fowls you may eat. `[4.]` They are further
forbidden, First, To eat the flesh of any creature that died of itself,
because the blood was not separated from it, and, besides the ceremonial
uncleanness which it lay under (from Lev. 11:39), it is not wholesome
food, nor ordinarily used among us, except by the poor. Secondly, To
seethe a kid in its mother\'s milk, either to gratify their own luxury,
supposing it a dainty bit, or in conformity to some superstitious custom
of the heathen. The Chaldee paraphrasts read it, Thou shalt not eat
flesh-meats and milk-meats together; and so it would forbid the use of
butter as sauce to any flesh.

`(2.)` Now as to all these precepts concerning their food, `[1.]` It is
plain in the law itself that they belonged only to the Jews, and were
not moral, nor of perpetual use, because not of universal obligation;
for what they might not eat themselves they might give to a stranger, a
proselyte of the gate, that had renounced idolatry, and therefore was
permitted to live among them, though not circumcised; or they might sell
it to an alien, a mere Gentile, that came into their country for trade,
but might not settle it, v. 21. They might feed upon that which an
Israelite might not touch, which is a plain instance of their
peculiarity, and their being a holy people. `[2.]` It is plain in the
gospel that they are now antiquated and repealed. For every creature of
God is good, and nothing now to be refused, or called common and
unclean, 1 Tim. 4:4.

### Verses 22-29

We have here a part of the statute concerning tithes. The productions of
the ground were twice tithed, so that, putting both together, a fifth
part was devoted to God out of their increase, and only four parts of
five were for their own common use; and they could not but own they paid
an easy rent, especially since God\'s part was disposed of to their own
benefit and advantage. The first tithe was for the maintenance of their
Levites, who taught them the good knowledge of God, and ministered to
them in holy things; this is supposed as anciently due, and is entailed
upon the Levites as an inheritance, by that law, Num. 18:24, etc. But it
is the second tithe that is here spoken of, which was to be taken out of
the remainder when the Levites had had theirs.

`I.` They are here charged to separate it, and set it apart for God: Thou
shalt truly tithe all the increase of they seed, v. 22. The Levites took
care of their own, but the separating of this was left to the owners
themselves, the law encouraging them to be honest by reposing a
confidence in them, and so trying their fear of God. They are commanded
to tithe truly, that is, to be sure to do it, and to do it faithfully
and carefully, that God\'s part might not be diminished either with
design or by oversight. Note, We must be sure to give God his full dues
out of our estates; for, being but stewards of them, it is required that
we be faithful, as those that must give account.

`II.` They are here directed how to dispose of it when they had separated
it. Let every man lay by as God prospers him and gives him success, and
then let him lay out in pious uses as God gives him opportunity; and it
will be the easier to lay out, and the proportion will be more
satisfying, when first we have laid by. This second tithe may be
disposed of,

`1.` In works of piety, for the first two years after the year of
release. They must bring it up, either in kind or in the full value of
it, to the place of the sanctuary, and there must spend it in holy
feasting before the Lord. If they could do it with any convenience, they
must bring it in kind (v. 23); but, if not, they might turn it into
money (v. 24, 25), and that money must be laid out in something to feast
upon before the Lord. The comfortable cheerful using of what God has
given us, with temperance and sobriety, is really the honouring of God
with it. Contentment, holy joy, and thankfulness, make every meal a
religious feast. The end of this law we have (v. 23): That thou mayest
learn to fear the Lord thy God always; it was to keep them right and
firm to their religion, `(1.)` By acquainting them with the sanctuary, the
holy things, and the solemn services that were there performed. What
they read the appointment of their Bibles, it would do them good to see
the observance of in the tabernacle; it would make a deeper impression
upon them, which would keep them out of the snares of the idolatrous
customs. Note, It will have a good influence upon our constancy in
religion never to forsake the assembling of ourselves together, Heb.
10:25. By the comfort of the communion of saints, we may be kept to our
communion with God. `(2.)` By using them to the most pleasant and
delightful services of religion. Let them rejoice before the Lord, that
they may learn to fear him always. The more pleasure we find in the ways
of religion the more likely we shall be to persevere in those ways. One
thing they must remember in their pious entertainments-to bid their
Levites welcome to them. Thou shalt not forsake the Levites (v. 27):
\"Let him never be a stranger to thy table, especially when thou eatest
before the Lord.\"

`2.` Every third year this tithe must be disposed of at home in works of
charity (v. 28, 29): Lay it up within they own gates, and let it be
given to the poor, who, knowing the provision this law had made for
them, no doubt would come to seek it; and, that they might make the poor
familiar to them and not disdain their company, they are here directed
to welcome them to their houses. \"Thither let them come, and eat and be
satisfied.\" In this charitable distribution of the second tithe they
must have an eye to the poor ministers and add to their encouragement by
entertaining them, then to poor strangers (not only for the supply of
their necessities, but to put a respect upon them, and so to invite them
to turn proselytes), and then to the fatherless and widow, who, though
perhaps they might have a competent maintenance left them, yet could not
be supposed to live so plentifully and comfortably as they had done in
months past, and therefore they were to countenance them, and help to
make them easy by inviting them to this entertainment. God has a
particular care for widows and fatherless, and he requires that we
should have the same. It is his honour, and will be ours, to help the
helpless. And if we thus serve God, and do good with what we have, it is
promised here that the Lord our God will bless us in all the work of our
hand. Note, `(1.)` The blessing of God is all in all to our outward
prosperity, and, without that blessing, the work of our hands which we
do will bring nothing to pass. `(2.)` The way to obtain that blessing is
to be diligent and charitable. The blessing descends upon the working
hand: \"Except not that God should bless thee in thy idleness and love
of ease, but in all the work of they hand.\" It is the hand of the
diligent, with the blessing of God upon it, that makes rich, Prov. 10:4,
22. And it descends upon the giving hand; he that thus scatters
certainly increases, and the liberal soul will be made fat. It is an
undoubted truth, though little believed, that to be charitable to the
poor, and to be free and generous in the support of religion and any
good work, is the surest and safest way of thriving. What is lent to the
Lord will be repaid with abundant interest. See Eze. 44:30.
