Deuteronomy, Chapter 7
======================

Commentary
----------

Moses in this chapter exhorts Israel, `I.` In general, to keep God\'s
commandments (v. 11, 12). `II.` In particular, and in order to that, to
keep themselves pure from all communion with idolaters. 1. They must
utterly destroy the seven devoted nations, and not spare them, or make
leagues with them (v. 1, 2, 16, 24). 2. They must by no means marry with
the remainders of them (v. 3, 4). 3. They must deface and consume their
altars and images, and not so much as take the silver and gold of them
to their own use (v. 5, 25, 26). To enforce this charge, he shows that
they were bound to do so, `(1.)` In duty. Considering `[1.]` Their
election to God (v. 6). `[2.]` The reason of that election (v. 7, 8).
`[3.]` The terms they stood upon with God (v. 9, 10). `(2.)` In interest.
It is here promised, `[1.]` In general, that, if they would serve God,
he would bless and prosper them (v. 12-15). `[2.]` In particular, that
if they would drive out the nations, that they might not be a temptation
to them, God would drive them out, that they should not be any vexation
to them (v. 17, etc.).

### Verses 1-11

Here is, `I.` A very strict caution against all friendship and fellowship
with idols and idolaters. Those that are taken into communion with God
must have no communication with the unfruitful works of darkness. These
things they are charged about for the preventing of this snare now
before them.

`1.` They must show them no mercy, v. 1, 2. Bloody work is here appointed
them, and yet it is God\'s work, and good work, and in its time and
place needful, acceptable, and honourable.

`(1.)` God here engages to do his part. It is spoken of as a thing taken
for granted that God would bring them into the land of promise, that he
would cast out the nations before them, who were the present occupants
of that land; no room was left to doubt of that. His power is
irresistible, and therefore he can do it; his promise is inviolable, and
therefore he will do it. Now, `[1.]` These devoted nations are here
named and numbered (v. 1), seven in all, and seven to one are great
odds. They are specified, that Israel might know the bounds and limits
of their commission: hitherto their severity must come, but no further;
nor must they, under colour of this commission, kill all that came in
their way; no, here must its waves be stayed. The confining of this
commission to the nations here mentioned plainly intimates that
after-ages were not to draw this into a precedent; this will not serve
to justify those barbarous laws which give no quarter. How agreeable
soever this method might be, when God himself prescribed it, to that
dispensation under which such multitudes of beasts were killed and
burned in sacrifice, now that all sacrifices of atonement are perfected
in, and superseded by, the great propitiation made by the blood of
Christ, human blood has become perhaps more precious than it was, and
those that have most power yet must not be prodigal of it. `[2.]` They
are here owned to be greater and mightier than Israel. They had been
long rooted in this land, to which Israel came strangers; they were more
numerous, had men much more bulky and more expert in war than Israel
had; yet all this shall not prevent their being cast out before Israel.
The strength of Israel\'s enemies magnifies the power of Israel\'s God,
who will certainly be too hard for them.

`(2.)` He engages them to do their part. Thou shalt smite them, and
utterly destroy them, v. 2. If God cast them out, Israel must not take
them in, no, not as tenants, nor tributaries, nor servants. Not covenant
of any kind must be made with them, no mercy must be shown them. This
severity was appointed, `[1.]` By way of punishment for the wickedness
they and their fathers had been guilty of. The iniquity of the Amorites
was now full, and the longer it had been in the filling the sorer was
the vengeance when it came at last. `[2.]` In order to prevent the
mischiefs they would do to God\'s Israel if they were left alive. The
people of these abominations must not be mingled with the holy seed,
lest they corrupt them. Better that all these lives should be lost from
the earth than that religion and the true worship of God should be lost
in Israel. Thus we must deal with our lusts that was against our souls;
God has delivered them into our hands by that promise, Sin shall not
have dominion over you, unless it be your own faults; let not us them
make covenants with them, nor show them any mercy, but mortify and
crucify them, and utterly destroy them.

`2.` They must make no marriages with those of them that escaped the
sword, v. 3, 4. The families of the Canaanites were ancient, and it is
probable that some of them were called honourable, which might be a
temptation to the Israelites, especially those of them that were of
least note in their tribes, to court an alliance with them, to ennoble
their blood; and the rather because their acquaintance with the country
might be serviceable to them in the improvement of it: but religion, and
the fear of God, must overrule all these considerations. To intermarry
with them was therefore unlawful, because it was dangerous; this very
thing had proved of fatal consequence to the old world (Gen. 6:2), and
thousands in the world that now is have been undone by irreligious
ungodly marriages; for there is more ground of fear in mixed marriages
that the good will be perverted than of hope that the bad will be
converted. The event proved the reasonableness of this warning: They
will turn away thy son from following me. Solomon paid dearly for his
folly herein. We find a national repentance for this sin of marrying
strange wives, and care taken to reform (Ezra 9, 10; and Neh. 13), and a
New-Testament caution not to be unequally yoked with unbelievers, 2 Co.
6:14. Those that in choosing yokefellows keep not at least within the
bounds of a justifiable profession of religion cannot promise themselves
helps meet for them. One of the Chaldee paraphrases adds here, as a
reason of this command (v. 3), For he that marries with idolaters does
in effect marry with their idols.

`3.` They must destroy all the relics of their idolatry, v. 5. Their
altars and pillars, their groves and graven images, all must be
destroyed, both in a holy indignation against idolatry and to prevent
infection. This command was given before, Ex. 23:24; 34:13. A great deal
of good work of this kind was done by the people, in their pious zeal (2
Chr. 31:1), and by good Josiah (2 Chr. 34:3, 7), and with this may be
compared the burning of the conjuring books, Acts 19:19.

`II.` Here are very good reasons to enforce this caution.

`1.` The choice which God had made of this people for his own, v. 6.
There was such a covenant and communion established between God and
Israel as was not between him and any other people in the world. Shall
they by their idolatries dishonour him who had thus honoured them? Shall
they slight him who had thus testified his kindness for them? Shall they
put themselves upon the level with other people, when God had thus
dignified and advanced them above all people? Had God taken them to be a
special people to him, and no other but them, and will not they take God
to be a special God to them, and no other but him?

`2.` The freeness of that grace which made this choice. `(1.)` There was
nothing in them to recommend or entitle them to this favour. In
multitude of the people is the king\'s honour, Prov. 14:28. But their
number was inconsiderable; they were only seventy souls when they went
down into Egypt, and, though greatly increased there, yet there were
many other nations more numerous: You were the fewest of all people, v.
7. The author of the Jerusalem Targum passes too great a compliment upon
his nation in his reading this, You were humble in spirit, and meek
above all people; quite contrary: they were rather stiff-necked and
ill-natured above all people. `(2.)` God fetched the reason of it purely
from himself, v. 8. `[1.]` He loved you because he would love you. Even
so, Father, because it seemed good in thy eyes. All that God loves he
loves freely, Hos. 14:4. Those that perish perish by their own merits,
but all that are saved are saved by prerogative. `[2.]` He has done his
work because he would keep his word. \"He has brought you out of Egypt
in pursuance of the oath sworn to your fathers.\" Nothing in them, or
done by them, did or could make God a debtor to them; but he had made
himself a debtor to his own promise, which he would perform
notwithstanding their unworthiness.

`3.` The tenour of the covenant into which they were taken; it was in
short this, That as they were to God so God would be to them. They
should certainly find him, `(1.)` Kind to his friends, v. 9. \"The Lord
thy God is not like the gods of the nations, the creatures of fancy,
subjects fit enough for loose poetry, but no proper objects of serious
devotion; no, he is God, God indeed, God alone, the faithful God, able
and ready not only to fulfil his own promises, but to answer all the
just expectations of his worshippers, and he will certainly keep
covenant and mercy,\" that is, \"show mercy according to covenant, to
those that love him and keep his commandments\" (and in vain do we
pretend to love him if we do not make conscience of his commandments);
\"and this\" (as is here added for the explication of the promise in the
second commandment) \"not only to thousands of persons, but to thousands
of generations-so inexhaustible is the fountain, so constant are the
streams!\" `(2.)` Just to his enemies: He repays those that hate him, v.
10. Note, `[1.]` Wilful sinners are haters of God; for the carnal mind
is enmity against him. Idolaters are so in a special manner, for they
are in league with his rivals. `[2.]` Those that hate God cannot hurt
him, but certainly ruin themselves. He will repay them to their face, in
defiance of them and all their impotent malice. His arrows are said to
be made ready against the face of them, Ps. 21:12. Or, He will bring
those judgments upon them which shall appear to themselves to be the
just punishment of their idolatry. Compare Job 21:19, He rewardeth him,
and he shall know it. Though vengeance seem to be slow, yet it is not
slack. The wicked and sinner shall be recompensed in the earth, Prov.
11:31. I cannot pass the gloss of the Jerusalem Targum upon this place,
because it speaks the faith of the Jewish church concerning a future
state: He recompenses to those that hate him the reward of their good
works in this world, that he may destroy them in the world to come.

### Verses 12-26

Here, `I.` The caution against idolatry is repeated, and against communion
with idolaters: \"Thou shalt consume the people, and not serve their
gods.\" v. 16. We are in danger of having fellowship with the works of
darkness if we take pleasure in fellowship with those that do those
works. Here is also a repetition of the charge to destroy the images, v.
25, 26. The idols which the heathen had worshipped were an abomination
to God, and therefore must be so to them: all that truly love God hat
what he hates. Observe how this is urged upon them: Thou shalt utterly
detest it, and thou shalt utterly abhor it; such a holy indignation as
this must we conceive against sin, that abominable thing which the Lord
hates. They must not retain the images to gratify their covetousness:
Thou shalt not desire the silver nor gold that is on them, nor think it
a pity to have that destroyed. Achan paid dearly for converting that to
his own use which was an anathema. Nor must they retain them to gratify
their curiosity: \"Neither shalt thou bring it into thy house, to be
hung up as an ornament, or preserved as a monument of antiquity. No, to
the fire with it, that is the fittest place for it.\" Two reasons are
given for this caution:-1. Lest thou be snared therein (v. 25), that is,
\"Lest thou be drawn, ere thou art aware, to like it and love it, to
fancy it and pay respect to it\" 2. Lest thou be a cursed thing like it,
v. 26. Those that make images are said to be like the, stupid and
senseless; here they are said to be in a worse sense like them, accursed
of God and devoted to destruction. Compare these two reasons together,
and observe that whatever brings us into a snare brings us under a
curse.

`II.` The promise of God\'s favour to them, if they would be obedient, is
enlarged upon with a most affecting copiousness and fluency of
expression, which intimates how much it is both God\'s desire and our
own interest that we be religious. All possible assurance is here given
them,

`1.` That, if they would sincerely endeavour to do their part of the
covenant, God would certainly perform his part. He shall keep the mercy
which he swore to thy fathers, v. 12. Let us be constant in our duty,
and we cannot question the constancy of God\'s mercy.

`2.` That if they would love God and serve him, and devote themselves and
theirs to him, he would love them, and bless them, and multiply them
greatly, v. 13, 14. What could they desire more to make them happy? `(1.)`
\"He will love thee.\" He began in love to us (1 Jn. 4:10), and, if we
return his love in filial duty, then, and then only, we may expect the
continuance of it, Jn. 14:21. `(2.)` \"He will bless thee with the tokens
of his love above all people.\" If they would distinguish themselves
from their neighbours by singular services, God would dignify them above
their neighbours by singular blessings. `(3.)` \"He will multiply thee.\"
Increase was the ancient blessing for the peopling of the world, once
and again (Gen. 1:28; 9:1), and here for the peopling of Canaan, that
little world by itself. The increase both of their families and of their
stock is promised: they should neither have estates without heirs nor
heirs without estates, but should have the complete satisfaction of
having many children and plentiful provisions and portions for them.

`3.` That, if they would keep themselves pure from the idolatries of
Egypt, God would keep them clear form the diseases of Egypt, v. 15. It
seems to refer not only to those plagues of Egypt by the force of which
they were delivered, but to some other epidemical country disease (as we
call it), which they remembered the prevalency of among the Egyptians,
and by which God had chastised them for their national sins. Diseases
are God\'s servants; they go where he sends them, and do what he bids
them. It is therefore good for the health of our bodies to mortify the
sin of our souls.

`4.` That, if they would cut off the devoted nations, they should cut
them off, and none should be able to stand before them. Their duty in
this matter would itself be their advantage: Thou shalt consume all the
people which the Lord thy God shall deliver thee-this is the precept (v.
16); and the Lord thy God shall deliver them unto thee, and shall
destroy them-this is the promise, v. 23. Thus we are commanded not to
let sin reign, not to indulge ourselves in it nor give countenance to
it, but to hate it and strive against it; and then God has promised that
sin shall not have dominion over us (Rom. 6:12,14), but that we shall be
more than conquerors over it. The difficulty and doubtfulness of the
conquest of Canaan having been a stone of stumbling to their fathers,
Moses here animates them against those things which were most likely to
discourage them, bidding them not to be afraid of them, v. 18, and
again, v. 21. `(1.)` Let them not be disheartened by the number and
strength of their enemies: Say not, They are more than I, how can I
dispossess them? v. 17. We are apt to think that the most numerous must
needs be victorious: but, to fortify Israel against this temptation,
Moses reminds them of the destruction of Pharaoh and all the power of
Egypt, v. 18, 19. They had seen the great temptations, or miracles (so
the Chaldee reads it), the signs and wonders, wherewith God had brought
them out of Egypt, in order to his bringing them into Canaan, and thence
might easily infer that God could dispossess the Canaanites (who, though
formidable enough, had not such advantages against Israel as the
Egyptians had; he that had done the greater could do the less), and that
he would dispossess them, otherwise his bringing Israel out of Egypt had
been no kindness to them. He that begun would finish. Thou shalt
therefore well remember this, v. 18. The word and works of God are well
remembered when they are improved as helps to our faith and obedience.
That is well laid up which is ready to us when we have occasion to use
it. `(2.)` Let them not be disheartened by the weakness and deficiency of
their own forces; for God will send them in auxiliary troops of hornets,
or wasps, as some read it (v. 20), probably larger than ordinary, which
would so terrify and molest their enemies (and perhaps be the death of
many to them) that their most numerous armies would become an easy prey
to Israel. God plagued the Egyptians with flies, but the Canaanites with
hornets. Those who take not warning by less judgments on others may
expect greater on themselves. But the great encouragement of Israel was
that they had God among them, a mighty God and terrible, v. 21. And if
God be for us, if God be with us, we need not fear the power of any
creature against us. `(3.)` Let them not be disheartened by the slow
progress of their arms, nor think that the Canaanites would never be
subdued if they were not expelled the first year; no, they must be put
out by little and little, and not all at once, v. 22. Note, We must not
think that, because the deliverance of the church and the destruction of
its enemies are not effected immediately, therefore they will never be
effected. God will do his own work in his own method and time, and we
may be sure that they are always the best. Thus corruption is driven out
of the hearts of believers by little and little. The work of
sanctification is carried on gradually; but that judgment will at length
be brought forth into a complete victory. The reason here given (as
before, Ex. 23:29, 30) is, Lest the beast of the field increase upon
thee. The earth God has given to the children of men; and therefore
there shall rather be a remainder of Canaanites to keep possession till
Israel become numerous enough to replenish it than that it should be a
habitation of dragons, and a court for the wild beasts of the desert,
Isa. 34:13, 14. Yet God could have prevented this mischief from the
beasts, Lev. 26:6. But pride and security, and other sins that are the
common effects of a settled prosperity, were enemies more dangerous than
the beasts of the field, and these would be apt to increase upon them.
See Judges 3:1, 4.
