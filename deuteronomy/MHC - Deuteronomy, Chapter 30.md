Deuteronomy, Chapter 30
=======================

Commentary
----------

One would have thought that the threatenings in the close of the
foregoing chapter had made a full end of the people of Israel, and had
left their case for ever desperate; but in this chapter we have a plain
intimation of the mercy God had in store for them in the latter days, so
that mercy at length rejoices against judgment, and has the last word.
Here we have, `I.` Exceedingly great and precious promises made to them,
upon their repentance and return to God (v. 1-10). `II.` The righteousness
of faith set before them in the plainness and easiness of the
commandment that was now given them (v. 11-14). `III.` A fair reference of
the whole matter to their choice (v. 15, etc.).

### Verses 1-10

These verses may be considered either as a conditional promise or as an
absolute prediction.

`I.` They are chiefly to be considered as a conditional promise, and so
they belong to all persons and all people, and not to Israel only; and
the design of them is to assure us that the greatest sinners, if they
repent and be converted, shall have their sins pardoned, and be restored
to God\'s favour. This is the purport of the covenant of grace, it
leaves room for repentance in case of misdemeanour, and promises pardon
upon repentance, which the covenant of innocency did not. Now observe
here,

`1.` How the repentance is described which is the condition of these
promises. `(1.)` It begins in serious consideration, v. 1. \"Thou shalt
call to mind that which thou hadst forgotten or not regarded.\" Note,
Consideration is the first step towards conversion. Isa. 46:8, Bring to
mind, O you transgressors. The prodigal son came to himself first, and
then to his father. That which they should call to mind is the blessing
and the curse. If sinners would but seriously consider the happiness
they have lost by sin and the misery they have brought themselves into,
and that by repentance they may escape that misery and recover that
happiness, they would not delay to return to the Lord their God. The
prodigal called to mind the blessing and the curse when he considered
his present poverty and the plenty of bread in his father\'s house, Lu.
15:17. `(2.)` It consists in sincere conversion. The effect of the
consideration cannot but be godly sorrow and shame, Eze. 6:9; 7:16. But
that which is the life and soul of repentance, and without which the
most passionate expressions are but a jest, is returning to the Lord our
God, v. 2. If thou turn (v. 10) with all thy heart and with all thy
soul. We must return to our allegiance to God as our Lord and ruler, our
dependence upon him as our Father and benefactor, our devotedness to him
as our highest end, and our communion with him as our God in covenant.
We must return to God from all that which stands in opposition to him or
competition with him. In this return to God we must be upright-with the
heart and soul, and universal-with all the heart and all the soul. `(3.)`
It is evidenced by a constant obedience to the holy will of God: If thou
shalt obey his voice (v. 2), thou and thy children; for it is not enough
that we do our duty ourselves, but we must train up and engage our
children to do it. Or this comes in as the condition of the entail of
the blessing upon their children, provided their children kept close to
their duty. `[1.]` This obedience must be with an eye to God: Thou shalt
obey his voice (v. 8), and hearken to it, v. 10. `[2.]` It must be
sincere, and cheerful, and entire: With all thy heart, and with all thy
soul, v. 2. `[3.]` It must be from a principle of love, and that love
must be with all thy heart and with all thy soul, v. 6. It is the heart
and soul that God looks at and requires; he will have these or nothing,
and these entire or not at all. `[4.]` It must be universal: According
to all that I command thee, v. 2, and again v. 8, to do all his
commandments; for he that allows himself in the breach of one
commandment involves himself in the guilt of contemning them all, James
2:10. An upright heart has respect to all God\'s commandments, Ps.
119:6.

`2.` What the favour is which is promised upon this repentance. Though
they are brought to God by their trouble and distress, in the nations
whither they were driven (v. 1), yet God will graciously accept of them
notwithstanding; for on this errand afflictions are sent, to bring us to
repentance. Though they are driven out to the utmost parts of heaven,
yet thence their penitent prayers shall reach God\'s gracious ear, and
there his favour shall find them out, v. 4. Undique ad caelos tantundem
est viae-From every place there is the same way to heaven. This promise
Nehemiah pleads in his prayer for dispersed Israel, Neh. 1:9. It is here
promised, `(1.)` That God would have compassion upon them, as proper
objects of his pity, v. 3. Against sinners that go on in sin God has
indignation (ch. 29:20), but on those that repent and bemoan themselves
he has compassion, Jer. 31:18, 20. True penitents may take great
encouragement from the compassions and tender mercies of our God, which
never fail, but overflow. `(2.)` That he would turn their captivity, and
gather them from the nations whither they were scattered (v. 3), though
ever so remote, v. 4. One of the Chaldee paraphrasts applies this to the
Messiah, explaining it thus: The word of the Lord shall gather you by
the hand of Elias the great priest, and shall bring you by the hand of
the king Messiah; for this was God\'s covenant with him, that he should
restore the preserved of Israel, Isa. 49:6. And this was the design of
his death, to gather into one the children of God that were scattered
abroad, Jn. 11:51, 52. To him shall the gathering of the people be. `(3.)`
That he would bring them into their land again, v. 5. Note, Penitent
sinners are not only delivered out of their misery, but restored to true
happiness in the favour of God. The land they are brought into to
possess it is , though not the same, yet in some respects better than
that which our first father Adam possessed, and out of which he was
expelled. `(4.)` That he would do them good (v. 5) and rejoice over them
for good, v. 9. For there is joy in heaven upon the repentance and
conversion of sinners: the father of the prodigal rejoiced over him for
good. `(5.)` That he would multiply them (v. 5), and that, when they grew
numerous, every mouth might have meat: he would make them plenteous in
every work of their hand, v. 9. National repentance and reformation
bring national plenty, peace, and prosperity. It is promised, The Lord
will make thee plenteous in the fruit of thy cattle and land, for good.
Many have plenty for hurt; the prosperity of fools destroys them. Then
it is for good when with it God gives us grace to use it for his glory.
`(6.)` That he would transfer the curses they had been under to their
enemies, v. 7. When God was gathering them in to re-establish them they
would meet with much opposition; but the same curses that had been a
burden upon them should become a defence to them, by being turned upon
their adversaries. The cup of trembling should be taken out of their
hand, and put into the hand of those that afflicted them, Isa. 51:22,
23. `(7.)` That he would give them his grace to change their hearts, and
rule there (v. 6): The Lord thy God will circumcise thy heart, to love
the Lord. Note, `[1.]` The heart must be circumcised to love God. The
filth of the flesh must be put away; and the foolishness of the heart,
as the Chaldee paraphrase expounds it. See Col. 2:11, 12; Rom. 2:29.
Circumcision was a seal of the covenant; the heart is then circumcised
to love God when it is strongly engaged and held by that bond to this
duty. `[2.]` It is the work of God\'s grace to circumcise the heart, and
to shed abroad the love of God there; and this grace is given to all
that repent and seek it carefully. Nay, that seems to be rather a
promise than a precept (v. 8): Thou shalt return and obey the voice of
the Lord. He that requires us to return promises grace to enable us to
return: and it is our fault if that grace be not effectual. herein the
covenant of grace is well ordered, that whatsoever is required in the
covenant is promised. Turn you at my reproof: behold, I will pour out my
Spirit, Prov. 1:23.

`3.` It is observable how Moses here calls God the Lord thy God twelve
times in these ten verses, intimating, `(1.)` That penitents may take
direction and encouragement in their return to God from their relation
to him. Jer. 3:22, \"Behold, we come unto thee, for thou art the Lord
our God; therefore to thee we are bound to come, whither else should we
go? And therefore we hope to find favour with thee.\" `(2.)` That those
who have revolted from God, if they return to him and do their first
works, shall be restored to their former state of honour and happiness.
Bring hither the first robe. In the threatenings of the former chapter
he is all along called the Lord, a God of power and the Judge of all:
but, in the promises of this chapter, the Lord thy God, a God of grace,
and in covenant with thee.

`II.` This may also be considered as a prediction of the repentance and
restoration of the Jews: When all these things shall have come upon thee
(v. 1), the blessing first, and after that the curse, then the mercy in
reserve shall take place. Though their hearts were wretchedly hardened,
yet the grace of God could soften and change them; and then, though
their case was deplorably miserable, the providence of God would redress
all their grievances. Now, 1. It is certain that this was fulfilled in
their return from their captivity in Babylon. It was a wonderful
instance of their repentance and reformation that Ephraim, who had been
joined to idols, renounced them, and said, What have I to do any more
with idols? That captivity effectually cured them of idolatry; and then
God planted them again in their own land and did them good. But, 2. Some
think that it is yet further to be accomplished in the conversion of the
Jews who are now dispersed, their repentance for the sin of their
fathers in crucifying Christ, their return to God through him, and their
accession to the Christian church. But, alas! who shall live when God
doth this?

### Verses 11-14

Moses here urges them to obedience from the consideration of the
plainness and easiness of the command.

`I.` This is true of the law of Moses. They could never plead in excuse of
their disobedience that God had enjoined them that which was either
unintelligible or impracticable, impossible to be known or to be done
(v. 11): It is not hidden from thee. That is, not send messengers to
heaven (v. 12), to enquire what thou must do to please God; nor needest
thou go beyond sea (v. 13), as the philosophers did, that travelled
through many and distant regions in pursuit of learning; no, thou art
not put to that labour and expense; nor is the commandment within the
reach of those only that have a great estate or a refined genius, but it
is very nigh unto thee, v. 14. It is written in thy books, made plain
upon tables, so that he that runs may read it; thy priests\' lips keep
this knowledge, and, when any difficulty arises, thou mayest ask the law
at their mouth, Mal. 2:7. It is not communicated in a strange language;
but it is in thy mouth, that is, in the vulgar tongue that is commonly
used by thee, in which thou mayest hear it read, and talk of it
familiarly among thy children. It is not wrapped up in obscure phrases
or figures to puzzle and amuse thee, or in hieroglyphics, but it is in
thy heart; it is delivered in such a manner as that it is level to thy
capacity, even to the capacity of the meanest.\" 2. \"It is not too hard
nor heavy for thee:\" so the Septuagint reads it, v. 11. Thou needest
not say, \"As good attempt to climb to heaven, or flee upon the wings of
the morning to the uttermost part of the sea, as go about to do all the
words of this law:\" no, the matter is not so; it is no such intolerable
yoke as some ill-minded people represent it. It was indeed a heavy yoke
in comparison with that of Christ (Acts 15:10), but not in comparison
with the idolatrous services of the neighbouring nations. God appeals to
themselves that he had not made them to serve with an offering, nor
wearied them with incense, Isa. 43:23; Mic. 6:3. But he speaks
especially of the moral law, and its precepts: \"That is very nigh thee,
consonant to the law of nature, which would have been found in every
man\'s heart, and every man\'s mouth, if he would but have attended to
it. There is that in thee which consents to the law that it is good,
Rom. 7:16. Thou hast therefore no reason to complain of any insuperable
difficulty in the observance of it.\"

`II.` This is true of the gospel of Christ, to which the apostle applies
it, and makes it the language of the righteousness which is of faith,
Rom. 10:6-8. And many think this is principally intended by Moses here;
for he wrote of Christ, Jn. 5:46. This is God\'s commandment now under
the gospel that we believe in the name of his Son Jesus Christ, 1 Jn.
3:23. If we ask, as the blind man did, Lord, who is he? or where is he,
that we may believe on him? (Jn. 9:36), this scripture gives an answer,
We need not go up to heaven, to fetch him thence, for he has come down
thence in his incarnation; nor down to the deep, to fetch him thence,
for thence he has come up in his resurrection. But the word is nigh us,
and Christ in that word; so that if we believe with the heart that the
promises of the incarnation and resurrection of the Messiah are
fulfilled in our Lord Jesus, and receive him accordingly, and confess
him with our mouth, we have then Christ with us, and we shall be saved.
He is near, very near, that justifies us. The law was plain and easy,
but the gospel much more so.

### Verses 15-20

Moses here concludes with a very bright light, and a very strong fire,
that, if possible, what he had been preaching of might find entrance
into the understanding and affections of this unthinking people. What
could be said more moving, and more likely to make deep and lasting
impressions? The manner of his treating with them is so rational, so
prudent, so affectionate, and every way so apt to gain the point, that
it abundantly shows him to be in earnest, and leaves them inexcusable in
their disobedience.

`I.` He states the case very fairly. He appeals to themselves concerning
it whether he had not laid the matter as plainly as they could wish
before them. 1. Every man covets to obtain life and good, and to escape
death and evil, desires happiness and dreads misery. \"Well,\" says he,
\"I have shown you the way to obtain all the happiness you can desire
and to avoid all misery. Be obedient, and all shall be well, and nothing
amiss.\" Our first parents ate the forbidden fruit, in hopes of getting
thereby the knowledge of good and evil; but it was a miserable knowledge
they got, of good by the loss of it, and of evil by the sense of it; yet
such is the compassion of God towards man that, instead of giving him to
his own delusion, he has favoured him by his word with such a knowledge
of good and evil as will make him for ever happy if it be not his own
fault. 2. Every man is moved and governed in his actions by hope and
fear, hope of good and fear of evil, real of apparent. \"Now,\" says
Moses, \"I have tried both ways; if you will be either drawn to
obedience by the certain prospect of advantage by it, or driven to
obedience by the no less certain prospect of ruin in case you be
disobedient-if you will be wrought upon either way, you will be kept
close to God and your duty; but, if you will not, you are utterly
inexcusable.\" Let us, then, hear the conclusion of the whole matter.
`(1.)` If they and theirs would love God and serve him, they should live
and be happy, v. 16. If they would love God, and evidence the sincerity
of their love by keeping his commandments-if they would make conscience
of keeping his commandments, and do it from a principle of love-then God
would do them good, and they should be as happy as his love and blessing
could make them. `(2.)` If they or theirs should at any time turn from
God, desert his service, and worship other gods this would certainly be
their ruin, v. 17, 18. Observe, It is not for every failure in the
particulars of their duty that ruin is threatened, but for apostasy and
idolatry: though every violation of the command deserved the curse, yet
the nation would be destroyed by that only which is the violation of the
marriage covenant. The purport of the New Testament is much the same;
this, in like manner, sets before us life and death, good and evil; He
that believes shall be saved; he that believes not shall be damned, Mk.
16:16. And this faith includes love and obedience. To those who by
patient continuance in well doing seek for glory, honour, and
immortality, God will give eternal life. But to those that are
contentious, and do not obey the truth, but obey unrighteousness (and
so, in effect, worship other gods and serve them), will be rendered the
indignation and wrath of an immortal God, the consequence of which must
needs be the tribulation and anguish of an immortal soul, Rom. 2:7-9.

`II.` Having thus stated the case, he fairly puts them to their choice,
with a direction to them to choose well. He appeals to heaven and earth
concerning his fair and faithful dealing with them, v. 19. They could
not but own that whatever was the issue he had delivered his soul;
therefore, that they might deliver theirs, he bids them choose life,
that is, choose to do their duty, which would be their life. Note, 1.
Those shall have life that choose it: those that choose the favour of
God and communion with him for their felicity, and prosecute their
choice as they ought, shall have what they choose. 2. Those that come
short of life and happiness must thank themselves; they would have had
it if they had chosen it when it was put to their choice: but they die
because they will die; that is, because they do not like the life
promised upon the terms proposed.

`III.` In the last verse, 1. He shows them, in short, what their duty is,
to love God, and to love him as the Lord, a Being most amiable, and as
their God, a God in covenant with them; and, as an evidence of this
love, to obey his voice in every thing, and by a constancy in this love
and obedience to cleave to him, and never to forsake him in affection or
practice. 2. He shows them what reason there was for this duty,
inconsideration, `(1.)` Of their dependence upon God: He is thy life, and
the length of thy days. He gives life, preserves life, restores life,
and prolongs it by his power though it is a frail life, and by his
patience though it is a forfeited life: he sweetens life with his
comforts, and is the sovereign Lord of life; in his hand our breath is.
Therefore we are concerned to keep ourselves in his love; for it is good
having him our friend, and bad having him our enemy. `(2.)` Of their
obligation to him for the promise of Canaan made to their fathers and
ratified with an oath. And, `(3.)` Of their expectations from him in
performance of that promise: \"Love God, and serve him, that thou mayest
dwell in that land of promise which thou mayest be sure he can give, and
uphold to thee who is thy life and the length of thy days.\" All these
are arguments to us to continue in love and obedience to the God of our
mercies.
