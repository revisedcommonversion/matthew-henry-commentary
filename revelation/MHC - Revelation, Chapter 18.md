Revelation, Chapter 18
======================

Commentary
----------

We have here, `I.` An angel proclaiming the fall of Babylon (v. 1, 2). `II.`
Assigning the reasons of her fall (v. 3). `III.` Giving warning to all who
belonged to God to come out of her (v. 4, 5), and to assist in her
destruction (v. 6-8). `IV.` The great lamentation made for her by those
who had been large sharers in her sinful pleasures and profits (v.
9-19). `V.` The great joy that there would be among others at the sight of
her irrecoverable ruin (v. 20, etc.).

### Verses 1-8

The downfall and destruction of Babylon form an event so fully
determined in the counsels of God, and of such consequence to his
interests and glory, that the visions and predictions concerning it are
repeated. 1. Here is another angel sent from heaven, attended with great
power and lustre, v. 1. He had not only light in himself, to discern the
truth of his own prediction, but to inform and enlighten the world about
that great event; and not only light to discern it, but power to
accomplish it. 2. This angel publishes the fall of Babylon, as a thing
already come to pass; and this he does with a mighty strong voice, that
all might hear the cry, and might see how well this angel was pleased to
be the messenger of such tidings. Here seems to be an allusion to the
prediction of the fall of pagan Babylon (Isa. 21:9), where the word is
repeated as it is here: has fallen, has fallen. Some have thought a
double fall is hereby intended, first her apostasy, and then her ruin;
and they think the words immediately following favour their opinion; She
has become the habitation of devils, and the hold of every foul spirit,
and the cage of every unclean and hateful bird, v. 2. But this is also
borrowed from Isa. 21:9, and seems to describe not so much her sin of
entertaining idols (which are truly called devils) as her punishment, it
being a common notion that unclean spirits, as well as ominous and
hateful birds, used to haunt a city or house that lay in its ruins. 3.
The reason of this ruin is declared (v. 3); for, though God is not
obliged to give any account of his matters, yet he is pleased to do so,
especially in those dispensations of providence that are most awful and
tremendous. The wickedness of Babylon had been very great; for she had
not only forsaken the true God herself, and set up idols, but had with
great art and industry drawn all sorts of men into the spiritual
adultery, and by her wealth and luxury had retained them in her
interest. 4. Fair warning is given to all that expect mercy from God,
that they should not only come out of her, but be assisting in her
destruction, v. 4, 5. Here observe, `(1.)` God may have a people even in
Babylon, some who belong to the election of grace. `(2.)` God\'s people
shall be called out of Babylon, and called effectually. `(3.)` Those that
are resolved to partake with wicked men in their sins must receive of
their plagues. `(4.)` When the sins of a people reach up to heaven, the
wrath of God will reach down to the earth. `(5.)` Though private revenge
is forbidden, yet God will have his people act under him, when called to
it, in pulling down his and their inveterate and implacable enemies, v.
6. `(6.)` God will proportion the punishment of sinners to the measure of
their wickedness, pride, and security, v. 7. `(7.)` When destruction comes
on a people suddenly, the surprise is a great aggravation of their
misery, v. 8.

### Verses 9-24

Here we have,

`I.` A doleful lamentation made by Babylon\'s friends for her fall; and
here observe,

`1.` Who are the mourners, namely, those who had been bewitched by her
fornication, those who had been sharers in her sensual pleasures, and
those who had been gainers by her wealth and trade-the kings and the
merchants of the earth: the kings of the earth, whom she had flattered
into idolatry by allowing them to be arbitrary and tyrannical over their
subjects, while they were obsequious to her; and the merchants, that is,
those who trafficked with her for indulgences, pardons, dispensations,
and preferments; these will mourn, because by this craft they got their
wealth.

`2.` What was the manner of their mourning. `(1.)` They stood afar off,
they durst not come nigh her. Even Babylon\'s friends will stand at a
distance from her fall. Though they had been partakers with her in her
sins, and in her sinful pleasures and profits, they were not willing to
bear a share in her plagues. `(2.)` They made a grievous outcry: Alas!
alas! that great city, Babylon, that mighty city! `(3.)` They wept, and
cast dust upon their heads, v. 19. The pleasures of sin are but for a
season, and they will end in dismal sorrow. All those who rejoice in the
success of the church\'s enemies will share with them in their downfall;
and those who have most indulged themselves in pride and pleasure are
the least able to bear calamities; their sorrows will be as excessive as
their pleasure and jollity were before.

`3.` What was the cause of their mourning; not their sin, but their
punishment. They did not lament their fall into idolatry, and luxury,
and persecution, but their fall into ruin-the loss of their traffic and
of their wealth and power. The spirit of antichrist is a worldly spirit,
and their sorrow is a mere worldly sorrow; they did not lament for the
anger of God, that had now fallen upon them, but for the loss of their
outward comfort. We have a large schedule and inventory of the wealth
and merchandise of this city, all which was suddenly lost (v. 12, 13),
and lost irrecoverably (v. 14): All things which were dainty and goodly
have departed from thee, and thou shalt find them no more at all. The
church of God may fall for a time, but she shall rise again; but the
fall of Babylon will be an utter overthrow, like that of Sodom and
Gomorrah. Godly sorrow is some support under affliction, but mere
worldly sorrow adds to the calamity.

`II.` An account of the joy and triumph there was both in heaven and
earth at the irrecoverable fall of Babylon: while her own people were
bewailing her, the servants of God were called to rejoice over her, v.
20. Here observe, 1. How universal this joy would be: heaven and earth,
angels and saints, would join in it; that which is matter of rejoicing
to the servants of God in this world is matter of rejoicing to the
angels in heaven. 2. How just and reasonable; and that, `(1.)` Because the
fall of Babylon was an act of God\'s vindictive justice. God was then
avenging his people\'s cause. They had committed their cause to him to
whom vengeance belongs, and now the year of recompence had come for the
controversies of Zion; and, though they did not take pleasure in the
miseries of any, yet they had reason to rejoice in the discoveries of
the glorious justice of God. `(2.)` Because it was an irrecoverable ruin.
This enemy should never molest them any more, and of this they were
assured by a remarkable token (v. 21): An angel from heaven took up a
stone like a great millstone, and cast it into the sea, saying, \"Thus
shall Babylon be thrown down with violence, and be found no more at all;
the place shall be no longer habitable by man, no work shall be done
there, no comfort enjoyed, no light seen there, but utter darkness and
desolation, as the reward of her great wickedness, first in deceiving
the nations with her sorceries, and secondly in destroying and murdering
those whom she could not deceive,\" v. 24. Such abominable sins deserved
so great a ruin.
