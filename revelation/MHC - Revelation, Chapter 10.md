Revelation, Chapter 10
======================

Commentary
----------

This chapter is an introduction to the latter part of the prophecies of
this book. Whether what is contained between this and the sounding of
the seventh trumpet (11:15) be a distinct prophecy from the other, or
only a more general account of some of the principal things included in
the other, is disputed by our curious enquirers into these abstruse
writings. However, here we have, `I.` A remarkable description of a very
glorious angel with an open book in his hand (v. 1-3). `II.` An account of
seven thunders which the apostle heard, as echoing to the voice of this
angel, and communicating some discoveries, which the apostle was not yet
allowed to write (v. 4). `III.` The solemn oath taken by him who had the
book in his hand (v. 5-7). `IV.` The charge given to the apostle, and
observed by him (v. 8-11).

### Verses 1-7

Here we have an account of another vision the apostle was favoured with,
between the sounding of the sixth trumpet and that of the seventh. And
we observe,

`I.` The person who was principally concerned in communicating this
discovery to John-an angel from heaven, another mighty angel, who is so
set forth as would induce one to think it could be no other than our
Lord and Saviour Jesus Christ! 1. He was clothed with a cloud: he veils
his glory, which is too great for mortality to behold; and he throws a
veil upon his dispensations. Clouds and darkness are round about him. 2.
A rainbow was upon his head; he is always mindful of his covenant, and,
when his conduct is most mysterious, yet it is perfectly just and
faithful. 3. His face was as the sun, all bright, and full of lustre and
majesty, ch. 1:16. 4. His feet were as pillars of fire; all his ways,
both of grace and providence, are pure and steady.

`II.` His station and posture: He set his right foot upon the sea and his
left foot upon the earth, to show the absolute power and dominion he had
over the world. And he held in his hand a little book opened, probably
the same that was before sealed, but was now opened, and gradually
fulfilled by him.

`III.` His awful voice: He cried aloud, as when a lion roareth (v. 3),
and his awful voice was echoed by seven thunders, seven solemn and
terrible ways of discovering the mind of God.

`IV.` The prohibition given to the apostle, that he should not publish,
but conceal what he had learned from the seven thunders, v. 4. The
apostle was for preserving and publishing every thing he saw and heard
in these visions, but the time had not yet come.

`V.` The solemn oath taken by this mighty angel. 1. The manner of his
swearing: He lifted up his hand to heaven, and swore by him that liveth
for ever, by himself, as God often has done, or by God as God, to whom
he, as Lord, Redeemer, and ruler of the world, now appeals. 2. The
matter of the oath: that there shall be time no longer; either, `(1.)`
That there shall be now no longer delay in fulfilling the predictions of
this book than till the last angel should sound; then every thing should
be put into speedy execution: the mystery of God shall be finished, v.
7. Or, `(2.)` That when this mystery of God is finished time itself shall
be no more, as being the measure of things that are in a mutable
changing state; but all things shall be at length for ever fixed, and so
time itself swallowed up in eternity.

### Verses 8-11

Here we have, `I.` A strict charge given to the apostle, which was, 1.
That he should go and take the little book out of the hands of that
mighty angel mentioned before. This charge was given, not by the angel
himself who stood upon the earth, but by the same voice from heaven that
in the fourth verse had lain an injunction upon him not to write what he
had discerned by the seven thunders. 2. To eat the book; this part of
the charge was given by the angel himself, hinting to the apostle that
before he should publish what he had discovered he must more thoroughly
digest the predictions, and be in himself suitably affected with them.

`II.` An account of the taste and relish which this little book would
have, when the apostle had taken it in; at first, while in his mouth,
sweet. All persons feel a pleasure in looking into future events, and in
having them foretold; and all good men love to receive a word from God,
of what import soever it be. But, when this book of prophecy was more
thoroughly digested by the apostle, the contents would be bitter; these
were things so awful and terrible, such grievous persecutions of the
people of God, and such desolation made in the earth, that the foresight
and foreknowledge of them would not be pleasant, but painful to the mind
of the apostle: thus was Ezekiel\'s prophecy to him, ch. 3:3.

`III.` The apostle\'s discharge of the duty he was called to (v. 10): He
took the little book out of the angel\'s hand, and ate it up, and he
found the relish to be as was told him. 1. It becomes the servants of
God to digest in their own souls the messages they bring to others in
his name, and to be suitably affected therewith themselves. 2. It
becomes them to deliver every message with which they are charged,
whether pleasing or unpleasing to men. That which is least pleasing may
be most profitable; however, God\'s messengers must not keep back any
part of the counsel of God.

`IV.` The apostle is made to know that this book of prophecy, which he
had now taken in, was not given him merely to gratify his own curiosity,
or to affect him with pleasure or pain, but to be communicated by him to
the world. Here his prophetical commission seems to be renewed, and he
is ordered to prepare for another embassy, to convey those declarations
of the mind and will of God which are of great importance to all the
world, and to the highest and greatest men in the world, and such should
be read and recorded in many languages. This indeed is the case; we have
them in our language, and are all obliged to attend to them, humbly to
enquire into the meaning of them, and firmly to believe that every thing
shall have its accomplishment in the proper time; and, when the
prophecies shall be fulfilled, the sense and truth of them will appear,
and the omniscience, power, and faithfulness of the great God will be
adored.
