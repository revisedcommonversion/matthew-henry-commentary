Jeremiah, Chapter 46
====================

Commentary
----------

How judgment began at the house of God we have found in the foregoing
prophecy and history; but now we shall find that it did not end there.
In this and the following chapters we have predictions of the
desolations of the neighbouring nations, and those brought upon them too
mostly by the king of Babylon, till at length Babylon itself comes to be
reckoned with. The prophecy against Egypt is here put first and takes up
this whole chapter, in which we have, `I.` A prophecy of the defeat of
Pharaoh-necho\'s army by the Chaldean forces at Carchemish, which was
accomplished soon after, in the fourth year of Jehoiakim (v. 1-12) `II.` A
prophecy of the descent which Nebuchadnezzar should make upon the land
of Egypt, and his success in it, which was accomplished some years after
the destruction of Jerusalem (v. 13-26). `III.` A word of comfort to the
Israel of God in the midst of those calamities (v. 27, 28).

### Verses 1-12

The first verse is the title of that part of this book, which relates to
the neighbouring nations, and follows here. It is the word of the Lord
which came to Jeremiah against the Gentiles; for God is King and Judge
of nations, knows and will call to an account those who know him not nor
take any notice of him. Both Isaiah and Ezekiel prophesied against these
nations that Jeremiah here has a separate saying to, and with reference
to the same events. In the Old Testament we have the word of the Lord
against the Gentiles; in the New Testament we have the word of the Lord
for the Gentiles, that those who were afar off are made nigh.

He begins with Egypt, because they were of old Israel\'s oppressors and
of late their deceivers, when they put confidence in them. In these
verses he foretells the overthrow of the army of Pharaoh-necho, by
Nebuchadnezzar, in the fourth year of Jehoiakim, which was so complete a
victory to the king of Babylon that thereby he recovered from the river
of Egypt to the river Euphrates, all that pertained to the king of
Egypt, and so weakened him that he came not again any more out of his
land (as we find, 2 Ki. 24:7), and so made him pay dearly for his
expedition against the king of Assyria four years before, in which he
slew Josiah, 2 Ki. 23:29. This is the event that is here foretold in
lofty expressions of triumph over Egypt thus foiled, which Jeremiah
would speak of with a particular pleasure, because the death of Josiah,
which he had lamented, was now avenged on Pharaoh-necho. Now here,

`I.` The Egyptians are upbraided with the mighty preparations they made
for this expedition, in which the prophet calls to them to do their
utmost, for so they would: \"Come then, order the buckler, let the
weapons of war be got ready,\" v. 3. Egypt was famous for horses-let
them be harnessed and the cavalry well mounted: Get up, you horsemen,
and stand forth, etc., v. 4. See what preparations the children of men
make, with abundance of care and trouble and at a vast expense, to kill
one another, as if they did not die fast enough of themselves. He
compares their marching out upon this expedition to the rising of their
river Nile (v. 7, 8): Egypt now rises up like a flood, scorning to keep
within its own banks and threatening to overflow all the neighbouring
lands. It is a very formidable army that the Egyptians bring into the
field upon this occasion. The prophet summons them (v. 9): Come up, you
horses; rage, you chariots. He challenges them to bring all their
confederate troops together, the Ethiopians, that descended from the
same stock with the Egyptians (Gen. 10:6), and were their neighbours and
allies, the Libyans and Lydians, both seated in Africa, to the west of
Egypt, and from them the Egyptians fetched their auxiliary forces. Let
them strengthen themselves with all the art and interest they have, yet
it shall be all in vain; they shall be shamefully defeated
notwithstanding, for God will fight against them, and against him there
is no wisdom nor counsel, Prov. 21:30, 31. It concerns those that go
forth to war not only to order the buckler, and harness the horses, but
to repent of their sins, and pray to God for his presence with them, and
that they may have it to keep themselves from every wicked thing.

`II.` They are upbraided with the great expectations they had from this
expedition, which were quite contrary to what God intended in bringing
them together. They knew their own thoughts, and God knew them, and sat
in heaven and laughed at them,; but they knew not the thoughts of the
Lord, for he gathers them as sheaves into the floor, Mic. 4:11, 12.
Egypt saith (v. 8): I will go up; I will cover the earth, and none shall
hinder me; I will destroy the city, whatever city it is that stands in
my way. Like Pharaoh of old, I will pursue, I will overtake. The
Egyptians say that they shall have a day of it, but God saith that it
shall be his day: The is the day of the Lord God of hosts (v. 10), the
day in which he will be exalted in the overthrow of the Egyptians. They
meant one thing, but God meant another; they designed it for the
advancement of their dignity and the enlargement of their dominion, but
God designed it for the great abasement and weakening of their kingdom.
It is a day of vengeance for Josiah\'s death; it is a day of sacrifice
to divine justice, to which multitudes of the sinners of Egypt shall
fall as victims. Note, When men think to magnify themselves by pushing
on unrighteous enterprises, let them expect that God will glorify
himself by blasting them and cutting them off.

`III.` They are upbraided with their cowardice and inglorious flight when
they come to an engagement (v. 5, 6): \"Wherefore have I seen them,
notwithstanding all these mighty and vast preparations and all these
expressions of bravery and resolution, when the Chaldean army faces
them, dismayed, turned back, quite disheartened, and no spirit left in
them.\" 1. They make a shameful retreat. Even their mighty ones, who,
one would think, should have stood their ground, flee a flight, flee by
consent, make the best of their way, flee in confusion and with the
utmost precipitation; they have neither time nor heart to look back, but
fear is round about them, for they apprehend it so. And yet, 2. They
cannot make their escape. They have the shame of flying, and yet not the
satisfaction of saving themselves by flight; they might as well have
stood their ground and died upon the spot; for even the swift shall not
flee away. The lightness of their heels shall fail them when it comes to
the trial, as well as the stoutness of their hearts; the mighty shall
not escape, nay, they are beaten down and broken to pieces. They shall
stumble in their flight, and fall towards the north, towards their
enemy\'s country; for such confusion were they in when they took to
their feet that instead of making homeward, as men usually do in that
case, they made forward. Note, The race is not to the swift nor the
battle to the strong. Valiant men are not always victorious.

`IV.` They are upbraided with their utter inability ever to recover this
blow, which should be fatal to their nation, v. 11, 12. The damsel, the
daughter of Egypt, that lived in great pomp and state, is sorely wounded
by this defeat. Let her now seek for balm in Gilead and physicians
there; let her use all the medicines her wise men can prescribe for the
healing of this hurt, and the repairing of the loss sustained by this
defeat; but all in vain; no cure shall be to them; they shall never be
able to bring such a powerful army as this into the field again. \"The
nations that rang of thy glory and strength have now heard of thy shame,
how shamefully thou wast routed and how thou are weakened by it.\" It
needs not be spread by the triumphs of the conquerors, the shrieks and
outcries of the conquered will proclaim it: Thy cry hath filled the
country about. For, when they fled several ways, one mighty man stumbled
upon another and dashed against another, such confusion were they in, so
that both together became a pray to the pursuers, an easy prey. A
thousand such dreadful accidents there should be, which should fill the
country with the cry of those that were overcome. Let not the mighty man
therefore glory in his might, for the time may come when it will stand
him in no stead.

### Verses 13-28

In these verses we have,

`I.` Confusion and terror spoken to Egypt. The accomplishment of the
prediction in the former part of the chapter disabled the Egyptians from
making any attempts upon other nations; for what could they do when
their army was routed? But still they remained strong at home, and none
of their neighbours durst make any attempts upon them. Though the kings
of Egypt came no more out of their land (2 Ki. 24:7), yet they kept safe
and easy in their land; and what would they desire more than peaceably
to enjoy their own? One would think all men should be content to do
this, and not covet to invade their neighbours. But the measure of
Egypt\'s iniquity is full, and now they shall not long enjoy their own;
those that encroached on others shall not be themselves encroached on.
The scope of the prophecy here is to show how the king of Babylon should
shortly come and smite the land of Egypt, and bring the war into their
own bosoms which they had formerly carried into his borders, v. 13. This
was fulfilled by the same hand with the former, even Nebuchadnezzar\'s,
but many years after, twenty at least, and probably the prediction of it
was long after the former prediction, and perhaps much about the same
time with that other prediction of the same event which we had ch.
43:10.

`1.` Here is the alarm of war sounded in Egypt, to their great amazement
(v. 14), notice given to the country that the enemy is approaching, the
sword is devouring round about in the neighbouring countries, and
therefore it is time for the Egyptians to put themselves in a posture of
defence, to prepare for war, that they may give the enemy a warm
reception. This must be proclaimed in all parts of Egypt, particularly
in Migdol, Noph, and Tahpanhes, because in these places especially the
Jewish refugees, or fugitives rather, had planted themselves, in
contempt of God\'s command (ch. 44:1), and let them hear what a sorry
shelter Egypt is likely to be to them.

`2.` The retreat hereupon of the forces of other nations which the
Egyptians had in their pay is here foretold. Some considerable number of
those troops, it is probable, were posted upon the frontiers to guard
them, where they were beaten off by the invaders and put to flights.
Then were the valiant men swept away (v. 15) as with a sweeping rain (it
is the word that is used Prov. 28:3); they can none of them stand their
ground, because the Lord drives them from their respective posts; he
drives them by his terrors; he drives them by enabling the Chaldeans to
drive them. It is not possible that those should fix whom the wrath of
God chases. He it was (v. 16) that made many to fall, yea, when their
day shall come to fall, the enemy needs not throw them down, they shall
fall one upon another, every man shall be a stumbling-block to his
fellow, to his follower; nay, if God please, they shall be made to fall
upon one another, they shall be made to fall upon one another, every
man\'s sword shall be against his fellow. Her hired men, the troops
Egypt has in he service, are indeed in the midst of her like fatted
bullocks, lusty men, able bodied and high spirited, who were likely for
action and promised to make their part good against the enemy; but they
are turned back; their hearts failed them, and, instead of fighting,
they have fled away together. How could they withstand their fate when
the day of their calamity had come, the day in which God will visit them
in wrath? Some think they are compared to fatted bullocks for their
luxury; they had wantoned in pleasures, so that they were very unfit for
hardships, and therefore turned back and could not stand. In this
consternation, `(1.)` They all made homeward towards their own country (v.
16): They said, \"Arise, and let us go again to our own people, where we
may be safe from the oppressing sword of the Chaldeans, that bears down
all before it.\" In times of exigence little confidence is to be put in
mercenary troops, that fight purely for pay, and have no interest in
theirs whom they fight for. `(2.)` They exclaimed vehemently against
Pharaoh, to whose cowardice or bad management, it is probably, their
defeat was owing. When he posted them there upon the borders of his
country it is probably that he told them he would within such a time
come himself with a gallant army of his own subjects to support them;
but he failed them, and, when the enemy advanced, they found they had
none to back them, so that they were perfectly abandoned to the fury of
the invaders. No marvel then that they quitted their post and deserted
the service, crying out, Pharaoh king of Egypt is but a noise (v. 17);
he can hector, and talk big of the mighty things he would do, but that
is all; he brings nothing to pass. All his promises to those in alliance
with him, or that are employed for him, vanish into smoke. He brings not
the succours he engaged to bring, or not till it is too late: He has
passed the time appointed; he did not keep his word, nor keep his day,
and therefore they bid him farewell, they will never serve under him any
more. Note, Those that make most noise in any business are frequently
but a noise. Great talkers are little doers.

`3.` The formidable power of the Chaldean army is here described as
bearing down all before it. The King of kings, whose name is the Lord of
hosts, and before whom the mightiest kings on earth, though gods to us,
are but as grasshoppers, he hath said it, he hath sworn it, As I live,
saith this king, as Tabor overtops the mountains and Carmel overlooks
the sea, so shall the king of Babylon overpower all the force of Egypt,
such a command shall he have, such a sway shall he bear, v. 18. He and
his army shall come against Egypt with axes, as hewers of wood (v. 22),
and the Egyptians shall be no more able to resist them than the tree is
to resist the man that comes with an axe to cut it down; so that Egypt
shall be felled as a forest is by the hewers of wood, which (if there by
many of them, and those well provided with instruments for the purpose)
will be done in a little time. Egypt is very populous, full of towns and
cities, like a forest, the trees of which cannot be searched or
numbered, and very rich, full of hidden treasures, many of which will
escape the searching eye of the Chaldean soldiers; but they shall make a
great spoil in the country, for they are more than the locusts, that
come in vast swarms and overrun a country, devouring every green thing
(Joel 1:6, 7), so shall the Chaldeans do, for they are innumerable.
Note, The Lord of hosts hath numberless hosts at his command.

`4.` The desolation of Egypt hereby is foretold, and the waste that
should be made of that rich country. Egypt is now like a very fair
heifer, or calf (v. 20), fat and shining, and not accustomed to the yoke
of subjection, wanton as a heifer that is well fed, and very sportful.
Some think here is an allusion to Apis, the bull or calf which the
Egyptians worshipped, from whom the children of Israel learned to
worship the golden calf. Egypt is as fair as a goddess, and adores
herself, but destruction comes; cutting up comes (so some read it); it
comes out of the north; thence the Chaldean soldiers shall come, as so
many butchers or sacrificers, to kill and cut up this fair heifer. `(1.)`
The Egyptians shall be brought down, shall be tamed, and their tune
changed: The daughters of Egypt shall be confounded (v. 24), shall be
filled with astonishment. Their voice shall go like a serpent, that is,
it shall be very low and submissive; they shall not low like a fair
heifer, that makes a great noise, but hiss out of their holes like
serpents. They shall not dare to make loud complaints of the cruelty of
the conquerors, but vent their griefs in silent murmurs. They shall not
now, as they used to do, answer roughly, but, with the poor, use
entreaties and beg for their lives. `(2.)` They shall be carried away
prisoners into their enemy\'s land (v. 19): \"O thou daughter! dwelling
securely and delicately in Egypt, that fruitful pleasant country, do not
think this will last always, but furnish thyself to go into captivity;
instead of rich clothes, which will but tempt the enemy to strip thee,
get plain and warm clothes; instead of fine shoes, provide strong ones;
and inure thyself to hardship, that thou mayest bear it the better.\"
Note, It concerns us, among all our preparations, to prepare for
trouble. We provide for the entertainment of our friends, let us not
neglect to provide for the entertainment of our enemies, nor among all
our furniture omit furniture for captivity. The Egyptians must prepare
to flee; for their cities shall be evacuated. Noph particularly shall be
desolate, without an inhabitant, so general shall the slaughter and the
captivity be. There are some penalties which, we say, the king and the
multitude are exempted from, but here even these are obnoxious: The
multitude of No shall be punished: it is called populous No, Nah. 3:8.
Though hand join in hand, yet they shall not escape; nor can any think
to go off in the crowd. Be they ever so many, they shall find God will
be too many for them. Their kings and all their petty princes shall
fall; and their gods too (ch. 43:12, 13), their idols and their great
men. Those which they call their tutelar deities shall be no protection
to them. Pharaoh shall be brought down, and all those that trust in him
(v. 25), particularly the Jews that came to sojourn in his country,
trusting in him rather than in God. All these shall be delivered into
the hands of the northern nations (v. 24), into the hand not only of
Nebuchadnezzar that mighty potentate, but into the hands of his
servants, according to the curse on Ham\'s posterity, of which the
Egyptians were, that they should be the servants of servants. These seek
their lives, and into their hands they shall be delivered.

`5.` An intimation is given that in process of time Egypt shall recover
itself again (v. 26): Afterwards it shall be inhabited, shall be peopled
again, whereas by this destruction it was almost dispeopled. Ezekiel
foretels that this should be at the end of forty years, Eze. 29:13. See
what changes the nations of the earth are subject to, how they are
emptied and increased again; and let not nations that prosper be secure,
nor those that for the present are in thraldom despair.

`II.` Comfort and peace are here spoken to the Israel of God, v. 27, 28.
Some understand it of those whom the king of Egypt had carried into
captivity with Jehoahaz, but we read not of any that were carried away
captives with him; it may therefore rather refer to the captives in
Babylon, whom God had mercy in store for, or, more generally, to all the
people of God, designed for their encouragement in the most difficult
times, when the judgments of God are abroad among the nations. We had
these words of comfort before, ch. 30:10, 11. 1. Let the wicked of the
earth tremble, they have cause for it; but fear not thou, O my servant
Jacob! and be not dismayed, O Israel! and again, Fear thou not, O Jacob!
God would not have his people to be a timorous people. 2. The wicked of
the earth shall be put away like dross, not be looked after any more;
but God\'s people, in order to their being saved, shall be found out and
gathered though they be far off, shall be redeemed though they be held
fast in captivity, and shall return. 3. The wicked is like the troubled
sea when it cannot rest; they flee when none pursues. But Jacob, being
at home in God, shall be at rest and at ease, and none shall make him
afraid; for what time he is afraid he has a God to trust to. 4. The
wicked God beholds afar off; but, wherever thou art, O Jacob! I am with
thee, a very present help. 5. A full end shall be made of the nations
that oppressed God\'s Israel, as Egypt and Babylon; but mercy shall be
kept in store for the Israel of God: they shall be corrected, but not
cast off; the correction shall be in measure, in respect of degree and
continuance. Nations have their periods; the Jewish nation itself has
come to an end as a nation; but the gospel church, God\'s spiritual
Israel, still continues, and will to the end of time; in that this
promise is to have its full accomplishment, that, though God correct it,
he will never make a full end of it.
