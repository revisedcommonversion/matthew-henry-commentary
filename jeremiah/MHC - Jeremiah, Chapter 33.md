Jeremiah, Chapter 33
====================

Commentary
----------

The scope of this chapter is much the same with that of the foregoing
chapter-to confirm the promise of the restoration of the Jews,
notwithstanding the present desolations of their country and dispersions
of their people. And these promises have, both in type and tendency, a
reference as far forward as to the gospel church, to which this second
edition of the Jewish church was at length to resign its dignities and
privileges. It is here promised, `I.` That the city shall be rebuilt and
re-established \"in statu quo-in its former state\" (v. 1-6). `II.` That
the captives, having their sins pardoned, shall be restored (v. 7, 8).
III. That this shall redound very much to the glory of God (v. 9). `IV.`
That the country shall have both joy and plenty (v. 10-14). `V.` That way
shall be made for the coming of the Messiah (v. 15, 16). `VI.` That the
house of David, the house of Levi, and the house of Israel, shall
flourish again, and be established, and all three in the kingdom of
Christ; a gospel ministry and the gospel church shall continue while the
world stands (v. 17-26).

### Verses 1-9

Observe here, `I.` The date of this comfortable prophecy which God
entrusted Jeremiah with. It is not exact in the time, only that it was
after that in the foregoing chapter, when things were still growing
worse and worse; it was the second time. God speaketh once, yea, twice,
for the encouragement of his people. We are not only so disobedient that
we have need of precept upon precept to bring us to our duty, but so
distrustful that we have need of promise upon promise to bring us to our
comfort. This word, as the former, came to Jeremiah when he was in
prison. Note, No confinement can deprive God\'s people of his presence;
no locks nor bars can shut out his gracious visits; nay, oftentimes as
their afflictions abound their consolations much more abound, and they
have the most reviving communications of his favour when the world
frowns upon them. Paul\'s sweetest epistles were those that bore date
out of a prison.

`II.` The prophecy itself. A great deal of comfort is wrapped up in it
for the relief of the captives, to keep them from sinking into despair.
Observe,

`1.` Who it is that secures this comfort to them (v. 2): It is the Lord,
the maker thereof, the Lord that framed it, He is the maker and former
of heaven and earth, and therefore has all power in his hands; so it
refers to Jeremiah\'s prayer, ch. 32:17. He is the maker and former of
Jerusalem, of Zion, built them at first, and therefore can rebuild
them-built them for his own praise, and therefore will. He formed it, to
establish it, and therefore it shall be established till those things be
introduced which cannot be shaken, but shall remain for ever. He is the
maker and former of this promise; he has laid the scheme for
Jerusalem\'s restoration, and he that has formed it will establish it,
he that has made the promise will make it good; for Jehovah is his name,
a God giving being to his promises by the performance of them, and when
he does this he is known by that name (Ex. 6:3), a perfecting God. When
the heavens and the earth were finished, then, and not till then, the
creator is called Jehovah, Gen. 2:4.

`2.` How this comfort must be obtained and fetched in-by prayer (v. 3):
Call upon me, and I will answer them. The prophet, having received some
intimations of this kind, must be humbly earnest with God for further
discoveries of his kind intentions. He had prayed (ch. 32:16), but he
must pray again. Note, Those that expect to receive comforts from God
must continue instant in prayer. We must call upon him, and then he will
answer us. Christ himself must ask, and it shall be given him, Ps. 2:8.
I will show thee great and mighty things (give thee a clear and full
prospect of them), hidden things, which, though in part discovered
already, yet thou knowest not, thou canst not understand or give credit
to. Or this may refer not only to the prediction of these things which
Jeremiah, if he desire it, shall be favoured with, but to the
performance of the things themselves which the people of God, encouraged
by this prediction, must pray for. Note, Promises are given, not to
supersede, but to quicken and encourage prayer. See Eze. 36:37.

`3.` How deplorable the condition of Jerusalem was which made it
necessary that such comforts as these should be provided for it, and
notwithstanding which its restoration should be brought about in due
time (v. 4, 5): The houses of this city, not excepting those of the
kings of Judah, are thrown down by the mounts, or engines of battery,
and by the sword, or axes, or hammers. It is the same word that is used
Eze. 26:9, With his axes he shall break down thy towers. The strongest
stateliest houses, and those that were best furnished, were levelled
with the ground. The fifth verse comes in in a parenthesis, giving a
further instance of the present calamitous state of Jerusalem. Those
that came to fight with the Chaldeans, to beat them off from the siege,
did more hurt than good, provoked the enemy to be more fierce and
furious in their assaults, so that the houses in Jerusalem were filled
with the dead bodies of men, who died of the wounds they received in
sallying out upon the besiegers. God says that they were such as he had
slain in his anger, for the enemies\' sword was his sword and their
anger his anger. But, it seems, the men that were slain were generally
such as had distinguished themselves by their wickedness, for they were
the very men for whose wickedness God did now hide himself from this
city, so that he was just in all he brought upon them.

`4.` What the blessings are which God has in store for Judah and
Jerusalem, such as will redress all their grievances.

`(1.)` Is their state diseased? Is it wounded? God will provide
effectually for the healing of it, though the disease was thought mortal
and incurable, ch. 7:22. \"The whole head is sick, and the whole heart
faint (Isa. 1:5); but (v. 6) I will bring it health and cure; I will
prevent the death, remove the sickness, and set all to rights again,\"
ch. 30:17. Note, Be the case ever so desperate, if God undertake the
cure, he will effect it. The sin of Jerusalem was the sickness of it
(Isa. 1:6); its reformation therefore will be its recovery. And the
following words tell us how that is wrought: \"I will reveal unto them
the abundance of peace and truth; I will give it to them in due time,
and give them an encouraging prospect of it in the mean time.\" Peace
stands here for all good; peace and truth are peace according to the
promise and in pursuance of that: or peace and truth are peace and the
true religion, peace and the true worship of God, in opposition to the
many falsehoods and deceits by which they had been led away from God. We
may apply it more generally, and observe, `[1.]` That peace and truth
are the great subject-matter of divine revelation. These promises here
lead us to the gospel of Christ, and in that God has revealed to us
peace and truth, the method of true peace-truth to direct us, peace to
make us easy. Grace and truth, and abundance of both, come by Jesus
Christ. Peace and truth are the life of the soul, and Christ came that
we might have that life, and might have it more abundantly. Christ rules
by the power of truth (Jn. 18:37) and by it he gives abundance of peace,
Ps. 72:7; 85:10. `[2.]` That the divine revelation of peace and truth
brings health and cure to all those that by faith receive it: it heals
the soul of the diseases it has contracted, as it is a means of
sanctification, Jn. 17:17. He sent his word and healed them, Ps. 107:20.
And it puts the soul into good order, and keeps it in a good frame and
fit for the employments and enjoyments of the spiritual and divine life.

`(2.)` Are they scattered and enslaved, and is their nation laid in ruins?
\"I will cause their captivity to return (v. 7), both that of Israel and
that of Judah\" (for though those who returned under Zerubbabel were
chiefly of Judah, and Benjamin, and Levi, yet afterwards many of all the
other tribes returned), \"and I will rebuild them, as I built them at
first.\" When they by repentance do their first works God will by their
restoration do his first works.

`(3.)` Is sin the procuring cause of all their troubles? That shall be
pardoned and subdued, and so the root of the judgments shall be killed,
v. 8. `[1.]` By sin they have become filthy, and odious to God\'s
holiness, but God will cleanse them, and purify them from their
iniquity. As those that were ceremonially unclean, and were therefore
shut out from the tabernacle, when they were sprinkled with the water of
purification had liberty of access to it again, so had they to their own
land, and the privileges of it, when God had cleansed them from their
iniquities. In allusion to that sprinkling, David prays, Purge me with
hyssop. `[2.]` By sin they have become guilty, and obnoxious to his
justice; but he will pardon all their iniquities, will remove the
punishment to which for sin they were bound over. All who by sanctifying
grace are cleansed from the filth of sin, by pardoning mercy are freed
from the guilt of it.

`(4.)` Have both their sins and their sufferings turned to the dishonour
of God? Their reformation and restoration shall redound as much to his
praise, v. 9. Jerusalem thus rebuilt, Judah thus repeopled, shall be to
me a name of joy, as pleasing to God as ever they have been provoking,
and a praise and an honour before all the nations. They, being thus
restored, shall glorify God by their obedience to him, and he shall
glorify himself by his favours to them. This renewed nation shall be as
much a reputation to religion as formerly it has been a reproach to it.
The nations shall hear of all the good that God has wrought in them by
his grace and of all the good he has wrought for them by his providence.
The wonders of their return out of Babylon shall make as great a noise
in the world as ever the wonders of their deliverance out of Egypt did.
and they shall fear and tremble for all this goodness. `[1.]` The people
of God themselves shall fear and tremble; they shall be much surprised
at it, shall be afraid of offending so good a God and of forfeiting his
favour. Hos. 3:5, They shall fear the Lord and his goodness. `[2.]` The
neighbouring nations shall fear because of the prosperity of Jerusalem,
shall look upon the growing greatness of the Jewish nation as really
formidable, and shall be afraid of making them their enemies. When the
church is fair as the moon, and clear as the sun, she is terrible as an
army with banners.

### Verses 10-16

Here is a further prediction of the happy state of Judah and Jerusalem
after their glorious return out of captivity, issuing gloriously at
length in the kingdom of the Messiah.

`I.` It is promised that the people who were long in sorrow shall again be
filled with joy. Every one concluded now that the country would lie for
ever desolate, that no beasts would be found in the land of Judah, no
inhabitant in the streets of Jerusalem, and consequently there would be
nothing but universal and perpetual melancholy (v. 10); but, though
weeping may endure for a time, joy will return. It was threatened (ch.
7:34 and 16:9) that the voice of joy and gladness should cease there;
but here it is promised that they shall revive again, that the voice of
joy and gladness shall be heard there, because the captivity shall be
returned; for then was their mouth filled with laughter, Ps. 126:1, 2.
`1.` There shall be common joy there, the voice of the bridegroom and the
voice of the bride; marriages shall again be celebrated, as formerly,
with songs, which in Babylon they had laid aside, for their harps were
hung on the willow-trees. 2. There shall be religious joy there;
temple-songs shall be revived, the Lord\'s songs, which they could not
sing in a strange land. There shall be heard in their private houses,
and in the cities of Judah, as well as in the temple, the voice of those
that shall say, Praise the Lord of hosts. Note, Nothing is more the
praise and honour of a people than to have God the glory of it, the
glory both of the power and of the goodness by which it is effected;
they shall prise him both as the Lord of hosts and as the God who is
good and whose mercy endures for ever. This, though a song of old, yet,
being sung upon this fresh occasion, will be a new song. We find this
literally fulfilled at their return out of Babylon, Ezra 3:11. They sang
together in praising the Lord, because he is good, for his mercy endures
for ever. The public worship of God shall be diligently and constantly
attended upon: They shall bring the sacrifice of praise to the house of
the Lord. All the sacrifices were intended for the praise of God, but
this seems to be meant of the spiritual sacrifices of humble adorations
and joyful thanksgivings, the calves of our lips (Hos. 14:2), which
shall please the Lord better than an ox of bullock. The Jews say that in
the days of the Messiah all sacrifices shall cease but the sacrifice of
praise, and to those days this promise has a further reference.

`II.` It is promised that the country, which had lain long depopulated,
shall be replenished and stocked again. It was now desolate, without man
and without beast; but, after their return, the pastures shall again be
clothed with flocks, Ps. 65:13. In all the cities of Judah and Benjamin
there shall be a habitation of shepherds, v. 12, 13. This intimates, 1.
The wealth of the country, after their return. It shall not be a
habitation of beggars, who have nothing, but of shepherds and
husbandmen, men of substance, with good stocks upon the ground they have
returned to. 2. The peace of the country. It shall not be a habitation
of soldiers, not shall there be tents and barracks set up to lodge them,
but there shall be shepherds; tents; for they shall hear no more the
alarms of war, nor shall there be any to make even the shepherds afraid.
See Ps. 144:13, 14. 3. The industry of the country, and their return to
their original plainness and simplicity, from which, in the corrupt
ages, they had sadly degenerated. The seed of Jacob, in their beginning,
gloried in this, that they were shepherds (Gen. 47:3), and so they shall
now be again, giving themselves wholly to that innocent employment,
causing their flocks to lie down (v. 12) and to pass under the hands of
him that telleth them (v. 13); for, though their flocks are numerous,
they are not numberless, nor shall they omit to number them, that they
may know if any be missing and may seek after it. Note, It is the
prudence of those who have ever so much of the world to keep an account
of what they have. Some think that they pass under the hand of him that
telleth them that they may be tithed, Lev. 27:32. Then we may take the
comfort of what we have when God has had his dues out of it. Now because
it seemed incredible that a people, reduced as now they were, should
ever recover such a degree of peace and plenty as this, here is
subjoined a general ratification of these promises (v. 14): I will
perform that good thing which I have promised. Though the promise may
sometimes work slowly towards an accomplishment, it works surely. The
days will come, though they are long in coming.

`III.` To crown all these blessings which God has in store for them, here
is a promise of the Messiah, and of that everlasting righteousness which
he should bring in (v. 15, 16), and probably this is that good thing,
that great good thing, which in the latter days, days that were yet to
come, God would perform, as he had promised to Judah and Israel, and to
which their return out of captivity and their settlement again in their
own land was preparatory. From the captivity to Christ is one of the
famous periods, Mt. 1:17. This promise of the Messiah we had before (ch.
23:5, 6), and there it came in as a confirmation of the promise of the
shepherds whom God would set over them, which would make one think that
the promise here concerning the shepherds and their flocks, which
introduces it, is to be understood figuratively. Christ is here
prophesied of, 1. As a rightful King. He is a branch of righteousness,
not a usurper, for he grows up unto David, descends from his loins, with
whom the covenant of royalty was made, and is that seed with whom that
covenant should be established, so that his title is unexceptionable. 2.
As a righteous king, righteous in enacting laws, waging wars, and giving
judgment, righteous in vindicating those that suffer wrong and punishing
those that do wrong: He shall execute judgment and righteousness in the
land. This may point at Zerubbabel, in the type, who governed with
equity, not as Jehoiakim had done (ch. 22:17); but it has a further
reference to him to whom all judgment is committed and who shall judge
the world in righteousness. 3. As a king that shall protect his subjects
from all injury. By him Judah shall be saved from wrath and the curse,
and, being so saved, Jerusalem shall dwell safely, quiet from the fear
of evil, and enjoying a holy security and serenity of mind, in a
dependence upon the conduct of this prince of peace, this prince of
their peace. 4. As a king that shall be praised by his subjects: \"This
is the name whereby they shall call him\" (so the Chaldee reads it, the
Syriac, and vulgar Latin); \"this name of his they shall celebrate and
triumph in, and by this name they shall call upon him.\" It may be read,
more agreeably to the original, This is he who shall call her, The Lord
our righteousness. As Moses\'s altar is called Jehovah-nissi (Ex.
17:15), and Jerusalem Jehovah-shammah (Eze. 48:35), intimating that they
glory in Jehovah as present with them and their banner, so here the city
is called The Lord our righteousness, because they glory in Jehovah as
their righteousness. That which was before said to be the name of Christ
(says Mr. Gataker) is here made the name of Jerusalem, the city of the
Messiah, the church of Christ. He it is that imparts righteousness to
her, for he is made of God to us righteousness, and she, by bearing that
name, professes to have her whole righteousness, not from herself, but
from him. In the Lord have I righteousness and strength, Isa. 45:24. And
we are made the righteousness of God in him. The inhabitants of
Jerusalem shall have this name of the Messiah so much in their mouths
that they shall themselves be called by it.

### Verses 17-26

Three of God\'s covenants, that of royalty with David and his seed, that
of the priesthood with Aaron and his seed, and that of Peculiarity with
Abraham and his seed, seemed to be all broken and lost while the
captivity lasted; but it is here promised that, notwithstanding that
interruption and discontinuance for a time, they shall all three take
place again, and the true intents and meaning of them all shall be
abundantly answered in the New Testament blessings, typified by those
conferred on the Jews after their return out of captivity.

`I.` The covenant of royalty shall be secured and the promises of it shall
have their full accomplishment in the kingdom of Christ, the Son of
David, v. 17. The throne of Israel was overturned in the captivity; the
crown had fallen from their head; there was not a man to sit on the
throne of Israel; Jeconiah was written childless. After their return the
house of David made a figure again; but it in the Messiah that this
promise is performed that David shall never want a man to sit on the
throne of Israel, and that David shall have always a son to reign upon
his throne. For as long as the man Christ Jesus sits on the right hand
of the throne of God, rules the world, and rules it for the good of the
church, to which he is a quickening head, and glorified head over all
things, as long as he is King upon the holy hill of Zion, David does not
want a successor, nor is the covenant with him broken. When the
first-begotten was brought into the world it was declared concerning
him, The Lord God shall give him the throne of his father David and he
shall reign over the house of Jacob for ever, Lu. 1:32, 33. For the
confirmation of this it is promised, 1. That the covenant with David
shall be as firm as the ordinances of heaven, to the stability of which
that of God\'s promise is compared, ch. 31:35, 36. There is a covenant
of nature, by which the common course of providence is settled and on
which it is founded, here called a covenant of the day and the night (v.
20, 25), because this is one of the articles of it, That there shall be
day and night in their season, according to the distinction put between
them in the creation, when God divided between the light and the
darkness, and established their mutual succession, and a government to
each, that the sun should rule by day and the moon and stars by night
(Gen. 1:4, 5, 16), which establishment was renewed after the flood (Gen.
8:22), and has continued ever since, Ps. 19:2. The morning and the
evening have both of them their regular outgoings (Ps. 65:8); the
day-spring knows its place, knows its time, and keeps both, so do the
shadows of the evening; and, while the world stands, this course shall
not be altered, this covenant shall not be broken. The ordinances of
heaven and earth (of this communication between heaven and earth, the
dominion of these ordinances of heaven upon the earth), which God has
appointed (v. 25; compare Job 38:33), shall never be disappointed. Thus
firm shall the covenant of redemption be with the Redeemer-God\'s
servant, but David our King, v. 21. This intimates that Christ shall
have a church on earth to the world\'s end; he shall see a seed in which
he shall prolong his days till time and day shall be no more. Christ\'s
kingdom is an everlasting kingdom; and when the end cometh, and not till
then, it shall be delivered up to God, even the Father. But it intimates
that the condition of it in this world shall be intermixed and
counterchanged, prosperity and adversity succeeding each other, as light
and darkness, day and night. But this is plainly taught us, that, as
sure as we may be that, though the sun will set tonight, it will rise
again tomorrow morning, whether we live to see it or no, so sure we may
be that, though the kingdom of the Redeemer in the world may for a time
be clouded and eclipsed by corruptions and persecutions, yet it will
shine forth again, and recover its lustre, in the time appointed. 2.
That the seed of David shall be as numerous as the host of heaven, that
is, the spiritual seed of the Messiah, that shall be born to him by the
efficacy of his gospel and his Spirit working with it. From the womb of
the morning he shall have the dew of their youth, to be his willing
people, Ps. 110:3. Christ\'s seed are not, as David\'s were, his
successors, but his subjects; yet the day is coming when they also shall
reign with him (v. 22): As the host of heaven cannot be numbered, so
will I multiply the seed of David, so that there shall be no danger of
the kingdom\'s being extinct, or extirpated, for want of heirs. The
children are numerous; and, if children, then heirs.

`II.` The covenant of priesthood shall be secured, and the promises of
that also shall have their full accomplishment. This seemed likewise to
be forgotten during the captivity, when there was no altar, no temple
service, for the priests to attend upon; but this also shall revive. It
did so; immediately upon their coming back to Jerusalem there were
priests and Levites ready to offer burnt-offerings and to do sacrifice
continually (Ezra 3:2, 3), as is here promised, v. 18. But that
priesthood soon grew corrupt; the covenant of Levi was profaned (as
appears Mal. 2:8), and in the destruction of Jerusalem by the Romans it
came to a final period. We must therefore look elsewhere for the
performance of this word, that the covenant with the Levites, the
priests, God\'s ministers, shall be as firm, and last as long, as the
covenant with the day and the night. And we find it abundantly
performed, 1. In the priesthood of Christ, which supersedes that of
Aaron, and is the substance of that shadow. While that great high priest
of our profession is always appearing in the presence of God for us,
presenting the virtue of his blood by which he made atonement in the
incense of his intercession, it may truly be said that the Levites do
not want a man before God to offer continually, Heb. 7:3, 17. He is a
priest for ever. The covenant of the priesthood is called a covenant of
peace (Num. 25:12), of life and peace, Mal. 2:5. Now we are sure that
this covenant is not broken, nor in the least weakened, while Jesus
Christ is himself our life and our peace. This covenant of priesthood is
here again and again joined with that of royalty, for Christ is a priest
upon his throne, as Melchizedek. 2. In a settled gospel ministry. While
there are faithful ministers to preside in religious assemblies, and to
offer up the spiritual sacrifices of prayer and praise, the priests, the
Levites, do not want successors, and such as have obtained a more
excellent ministry. The apostle makes those that preach the gospel to
come in the room of those that served at the altar, 1 Co. 9:13, 14. 3.
In all true believers, who are a holy priesthood, a royal priesthood (I
Peter 2:5, 9), who are made to our God kings and priests (Rev. 1:6);
they offer up spiritual sacrifices, acceptable to God, and themselves,
in the first place, living sacrifices. Of these Levites this promise
must be understood (v. 22), that they shall be as numerous as the sand
of the sea, the same that is promised concerning Israel in general (Gen.
22:17); for all God\'s spiritual Israel are spiritual priests, Rev. 5:9,
10; 7:9, 15.

`III.` The covenant of peculiarity likewise shall be secured and the
promises of that covenant shall have their full accomplishment in the
gospel Israel. Observe, 1. How this covenant was looked upon as broken
during the captivity, v. 24. God asks the prophet, \"Hast though not
heard, and dost thou not consider, what this people have spoken?\"
either the enemies of Israel, who triumphed in the extirpation of a
people that had made such a noise in the world, or the unbelieving
Israelites themselves, \"this people among whom thou dwellest;\" they
have broken covenant with God, and then quarrel with him as if he had
not dealt faithfully with them. The two families which the Lord hath
chosen, Israel and Judah, whereas they were but one when he chose them,
he hath even cast them off. \"Thus have they despised my people, that
is, despised the privilege of being my people as if it were a privilege
of no value at all.\" The neighbouring nations despised them as now no
more a nation, but the ruins of a nation, and looked upon all their
honour as laid in the dust; but, 2. See how firm the covenant stands
notwithstanding, as firm as that with day and night; sooner will God
suffer day and night to cease then he will cast away the seed of Jacob.
This cannot refer to the seed of Jacob according to the flesh, for they
are cast away, but to the Christian church, in which all these promises
were to be lodged, as appears by the apostle\'s discourse, Rom. 11:1,
etc. Christ is that seed of David that is to be perpetual dictator to
the seed of Abraham, Isaac, and Jacob; and, as this people shall never
want such a king, so this king shall never want such a people.
Christianity shall continue in the dominion of Christ, and the
subjection of Christians to him, till day and night come to an end. And,
as a pledge of this, that promise is again repeated, I will cause their
captivity to return; and, having brought them back, I will have mercy on
them. To whom this promise refers appears Gal. 6:16, where all that walk
according to the gospel rule are made to be the Israel of God, on whom
peace and mercy shall be.
