Introduction to 3rd John
========================

Christian communion is exerted and cherished by letter. Christians are
to be commended in the practical proof of their professed subjection to
the gospel of Christ. The animating and countenancing of generous and
public-spirited persons is doing good to many-to this end the apostle
sends this encouraging epistle to his friend Gaius, in which also he
complains of the quite opposite spirit and practice of a certain
minister, and confirms the good report concerning another more worthy to
be imitated.
