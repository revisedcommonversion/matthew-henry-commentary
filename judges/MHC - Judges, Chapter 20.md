Judges, Chapter 20
==================

Commentary
----------

Into the book of the wars of the Lord the story of this chapter must be
brought, but it looks as sad and uncomfortable as any article in all
that history; for there is nothing in it that looks in the least bright
or pleasant but the pious zeal of Israel against the wickedness of the
men of Gibeah, which made it on their side a just and holy war; but
otherwise the obstinacy of the Benjamites in protecting their criminals,
which was the foundation of the war, the vast loss which the Israelites
sustained in carrying on the war, and (though the righteous cause was
victorious at last) the issuing of the war in the almost utter
extirpation of the tribe of Benjamin, make it, from first to last,
melancholy. And yet this happened soon after the glorious settlement of
Israel in the land of promise, upon which one would have expected every
thing to be prosperous and serene. In this chapter we have, `I.` The
Levite\'s cause heard in a general convention of the tribes (v. 1-7).
`II.` A unanimous resolve to avenge his quarrel upon the men of Gibeah (v.
8-11). `III.` The Benjamites appearing in defence of the criminals (v.
12-17). `IV.` The defeat of Israel in the first and second day\'s battle
(v. 18-25). `V.` Their humbling themselves before God upon that occasion
(v. 26-28). `VI.` The total rout they gave the Benjamites in the third
engagement, by a stratagem, by which they were all cut off, except 600
men (v. 29-48). And all this the effect of the indignities done to one
poor Levite and his wife; so little do those that do iniquity consider
what will be the end thereof.

### Verses 1-11

Here is, `I.` A general meeting of all the congregation of Israel to
examine the matter concerning the Levite\'s concubine, and to consider
what was to be done upon it, v. 1, 2. It does not appear that they were
summoned by the authority of any one common head, but they came together
by the consent and agreement, as it were, of one common heart, fired
with a holy zeal for the honour of God and Israel. 1. The place of their
meeting was Mizpeh; they gathered together unto the Lord there, for
Mizpeh was so very near to Shiloh that their encampment might very well
be supposed to reach from Mizpeh to Shiloh. Shiloh was a small town, and
therefore, when there was a general meeting of the people to represent
themselves before God, they chose Mizpeh for their head-quarters, which
was the next adjoining city of note, perhaps because they were not
willing to give that trouble to Shiloh which so great an assembly would
occasion, it being the resident of the priests that attended the
tabernacle. 2. The persons that met were all Israel, from Dan (the city
very lately so called, ch. 18:29) in the north to Beersheba in the
south, with the land of Gilead (that is, the tribes on the other side
Jordan), all as one man, so unanimous were they in their concern for the
public good. Here was an assembly of the people of God, not a
convocation of the Levites and priests, though a Levite was the person
principally concerned in the cause, but an assembly of the people, to
whom the Levite referred himself with an Appello populum-I appeal to the
people. The people of God were 400,000 footmen that drew the sword, that
is, were armed and disciplined, and fit for service, and some of them
perhaps such as had known the wars of Canaan, ch. 3:1. In this assembly
of all Israel, the chief (or corners) of the people (for rulers are the
corner-stones of the people, that keep all together) presented
themselves as the representatives of the rest. They rendered themselves
at their respective posts, at the head of the thousands and hundreds,
the fifties and tens, over which they presided; for so much order and
government, we may suppose, at least, they had among them, though they
had no general or commander-in-chief. So that here was, `(1.)` A general
congress of the states for counsel. The chief of the people presented
themselves, to lead and direct in this affair. `(2.)` A general rendezvous
of the militia for action, all that drew sword and were men of war (v.
17), not hirelings nor pressed men, but the best freeholders, that went
at their own charge. Israel were above 600,000 when they came into
Canaan, and we have reason to think they were at this time much
increased, rather than diminished; but then all between twenty and sixty
were military men, now we may suppose more than the one half exempted
from bearing arms to cultivate the land; so that these were as the
trained bands. The militia of the two tribes and a half were 40,000
(Jos. 4:13), but the tribes were many more.

`II.` Notice given to the tribe of Benjamin of this meeting (v. 3): They
heard that the children of Israel had gone up to Mizpeh. Probably they
had a legal summons sent them to appear with their brethren, that the
cause might be fairly debated, before any resolutions were taken up upon
it, and so the mischiefs that followed would have been happily
prevented; but the notice they had of this meeting rather hardened and
exasperated them than awakened them to think of the things that belonged
to their peace and honour.

`III.` A solemn examination of the crime charged upon the men of Gibeah.
A very horrid representation of it had been made by the report of the
messengers that were sent to call them together, but it was fit it
should be more closely enquired into, because such things are often made
worse than really they were; a committee therefore was appointed to
examine the witnesses (upon oath, no doubt) and to report the matter. It
is only the testimony of the Levite himself that is here recorded, but
it is probable his servant, and the old man, were examined, and gave in
their testimony, for that more than one were examined appears by the
original (v. 3), which is, Tell you us; and the law was that none should
be put to death, much less so many, upon the testimony of one witness
only. The Levite gives a particular account of the matter: that he came
into Gibeah only as a traveller to lodge there, not giving the least
shadow of suspicion that he designed them any ill turn (v. 4), and that
the men of Gibeah, even those that were of substance among them, that
should have been a protection to the stranger within their gates,
riotously set upon the house where he lodged, and thought to slay him;
he could not, for shame relate the demand which they, without shame,
made, ch. 19:22. They declared their sin as Sodom, even the sin of
Sodom, but his modesty would not suffer him to repeat it; it was
sufficient to say they would have slain him, for he would rather have
been slain than have submitted to their villany; and, if they had got
him into their hands, they would have abused him to death, witness what
they had done to his concubine: They have forced her that she is dead,
v. 5. And, to excite in his countrymen an indignation at this
wickedness, he had sent pieces of the mangled body to all the tribes,
which had fetched them together to bear their testimony against the
lewdness and folly committed in Israel, v. 6. All lewdness is folly, but
especially lewdness in Israel. For those to defile their own bodies who
have the honourable seal of the covenant in their flesh, for those to
defy the divine vengeance to whom it is so clearly revealed from
heaven-Nabal is their name, and folly is with them. He concludes his
declaration with an appeal to the judgment of the court (v. 7): You are
all children of Israel, and therefore you know law and judgment, Esth.
1:13. \"You are a holy people to God, and have a dread of every thing
which will dishonour God and defile the land; you are of the same
community, members of the same body, and therefore likely to feel from
the distempers of it; you are children of Israel, that ought to take
particular care of the Levites, God\'s tribe, among you, and therefore
give your advice and counsel what is to be done.\"

`IV.` The resolution they came to hereupon, which was that, being now
together, they would not disperse till they had seen vengeance taken
upon this wicked city, which was the reproach and scandal of their
nation. Observe, 1. Their zeal against the lewdness that was committed.
They would not return to their houses, how much soever their families
and their affairs at home wanted them, till they had vindicated the
honour of God and Israel, and recovered with their swords, if it could
not be had otherwise, that satisfaction for the crime which the justice
of the nation called for, v. 8. By this they showed themselves children
of Israel indeed, that they preferred the public interest before their
private concerns. 2. Their prudence in sending out a considerable body
of their forces to fetch provisions for the rest, v. 9, 10. One of ten,
and he chosen by lot, 40,000 in all, must go to their respective
countries, whence they came, to fetch bread and other necessaries for
the subsistence of this great army; for when they came from home they
took with them provisions only for a journey to Mizpeh, not for an
encampment (which might prove long) before Gibeah. This was to prevent
their scattering to forage for themselves, for, if they had done this,
it would have been hard to get them all together again, especially all
in so good a mind. Note, When there appears in people a pious zeal for
any good work it is best to strike while the iron is hot, for such zeal
is apt to cool quickly if the prosecution of the work be delayed. Let it
never be said that we left that good work to be done to-morrow which we
could as well have done to-day. 3. Their unanimity in these counsels,
and the execution of them. The resolution was voted, Nemine
contradicente-Without a dissenting voice (v. 8); it was one and all;
and, when it was put in execution, they were knit together as one man,
v. 11. This was their glory and strength, that the several tribes had no
separate interests when the common good was concerned.

### Verses 12-17

Here is, `I.` The fair and just demand which the tribes of Israel, now
encamped, sent to the tribe of Benjamin, to deliver up the malefactors
of Gibeah to justice, v. 12, 13. If the tribe of Benjamin had come up,
as they ought to have done, to the assembly, and agreed with them in
their resolution, there would have been none to deal with but the men of
Gibeah only, but they, by their absence, taking part with the criminals,
application must be made to them all. The Israelites were zealous
against the wickedness that was committed, yet they were discreet in
their zeal, and did not think it would justify them in falling upon the
whole tribe of Benjamin unless they, by refusing to give up the
criminals, and protecting them against justice, should make themselves
guilty, ex post facto-as accessaries after the fact. They desire them to
consider how great the wickedness was that was committed (v. 12), and
that it was done among them: and how necessary it was therefore that
they should either punish the malefactors with death themselves,
according to the law of Moses, or deliver them up to the general
assembly, to be so much the more publicly and solemnly punished, that
evil might be put away from Israel, the national guilt removed, the
infection stopped by cutting off the gangrened part, and national
judgments prevented; for the sin was so very like that of the Sodomites
that they might justly fear, if they did not punish it, God would rain
hail from heaven upon them, as he did, not only upon Sodom, but the
neighbouring cities. If the Israelites had not made this reasonable
demand, they would have had much more reason to lament the following
desolations of Benjamin. All methods of accommodation must be used
before we go to war or go to law. The demand was like that of Joab\'s to
Abel, 2 Sa. 20:20, 21. \"Only deliver up the traitor, and we will lay
down our arms.\" On these terms, and no other, God will be at peace with
us, that we part with our sins, that we mortify and crucify our lusts,
and then all shall be well; his anger will be turned away.

`II.` The wretched obstinacy and perverseness of the men of Benjamin, who
seem to have been as unanimous and zealous in their resolutions to stand
by the criminals as the rest of the tribes were to punish them, so
little sense had they of their honour, duty, and interest. 1. They were
so prodigiously vile as to patronise the wickedness that was committed:
They would not hearken to the voice of their brethren (v. 13), either
because those of that tribe were generally more vicious and debauched at
this time than the rest of the tribes, and therefore would not bear to
have that punished in others of which they knew themselves guilty (some
of the most fruitful and pleasant parts of Canaan fell to the lot of
this tribe; their land, like that of Sodom, was as the garden of the
Lord, which perhaps helped to make the inhabitants, like the men of
Sodom, wicked, and sinners before the Lord exceedingly, Gen. 13:10, 13),
or because (as bishop Patrick suggests) they took it ill that the other
tribes should meddle with their concerns; they would not do that which
they knew was their duty because they were reminded of it by their
brethren, by whom they scorned to be taught and controlled. If there
were any wise men among them that would have complied with the demand
made, yet they were overpowered by the majority, who thus made the crime
of the men of Gibeah their own. Thus we have fellowship with the
unfruitful works of darkness if we say A confederacy with those that
have, and make ourselves guilty of other men\'s sins by countenancing
and defending them. It seems there is no cause so bad but it will find
some patrons, some advocates, to appear for it; but woe be to those by
whom such offences come. Those will have a great deal to answer for that
obstruct the course of necessary justice, and strengthen the hands of
the wicked, by saying, O wicked man! thou shalt not die.

`2.` They were so prodigiously vain and presumptuous as to make head
against the united force of all Israel. Never, surely, were men so
wretchedly infatuated as they were when they took up arms in opposition,
`(1.)` To so good a cause as Israel had. How could they expect to prosper
when they fought against justice, and consequently against the just God
himself, against those that had the high priest and the divine oracle on
their side, and so acted in downright rebellion against the sacred and
supreme authority of the nation. `(2.)` To so great a force as Israel had.
The disproportion of their numbers was much greater than that, Lu.
14:31, 32, where he that had but 10,000 durst not meet him that came
against him with 20,000, and therefore desired conditions of peace.
There the enemy was but two to one, here above fifteen to one; yet they
despised conditions of peace. All the forces they could bring into the
field were but 26,000 men, besides 700 men of Gibeah (v. 15); yet with
these they will dare to face 400,000 men of Israel, v. 17. Thus sinners
are infatuated to their own ruin, and provoke him to jealousy who is
infinitely stronger than they, 1 Co. 10:22. But it should seem they
depended upon the skill of their men to make up what was wanting in
numbers, especially a regiment of slingers, 700 men, who, though
left-handed, were so dexterous at slinging stones that they would not be
a hair\'s breadth beside their mark, v. 16. But these good marksmen were
very much out in their aim when they espoused this bad cause. Benjamin
signifies the son of the right hand, yet we find his posterity
left-handed.

### Verses 18-25

We have here the defeat of the men of Israel in their first and second
battle with the Benjamites.

`I.` Before their first engagement they asked counsel of God concerning
the order of their battle and were directed, and yet they were sorely
beaten. They did not think it was proper to ask of God whether they
should go up at all against Benjamin (the case was plain enough, the men
of Gibeah must be punished for their wickedness, and Israel must inflict
the punishment or it will not be done), but \"Who shall go first?\" (v.
18), that is, \"Who shall be general of our army?\" for, which soever
tribe was appointed to go first, the prince of that tribe must be looked
upon as commander-in-chief of the whole body. For, if they had meant it
of the order of their march only, it would have been proper to ask,
\"Who shall go next?\" and then, \"Who next?\" But, if they know that
Judah must go first, they know they must all observe the orders of the
prince of that tribe. This honour was done to Judah because our Lord
Jesus was to spring from that tribe, who was in all things to have the
pre-eminence. The tribe that went up first had the most honourable post,
but withal the most dangerous, and probably lost most in the engagement.
Who would strive for precedency that sees the peril of it? Yet though
Judah, that strong and valiant tribe, goes up first, and all the tribes
of Israel attend them, little Benjamin (so he is called, Ps. 68:27), is
too hard for them all. The whole army lays siege to Gibeah, v. 19. The
Benjamites advance to raise the siege, and the army prepares to give
them a warm reception (v. 20). But between the Benjamites that attacked
them in the front with incredible fury, and the men of Gibeah that
sallied out upon their rear, they were put into confusion and lost
22,000 men, v. 21. Here were no prisoners taken, for there was no
quarter given, but all put to the sword.

`II.` Before their second engagement they again asked counsel of God, and
more solemnly than before; for they wept before the Lord until evening
(v. 23), lamenting the loss of so many brave men, especially as it was a
token of God\'s displeasure and would give occasion to the Benjamites to
triumph in the success of their wickedness. Also at this time they did
not ask who should go up first, but whether they should go up at all.
The intimate a reason why they should scruple to do it, especially now
that Providence had frowned upon them, because Benjamin was their
brother, and a readiness to lay down their arms if God should so order
them. God bade them go up; he allowed the attempt, for, though Benjamin
was their brother, he was a gangrened member of their body and must be
cut off. Upon this they encouraged themselves, perhaps more in their own
strength than in the divine commission, and made a second attempt upon
the forces of the rebels, in the same place where the former battle was
fought (v. 22), with the hope of retrieving their credit upon the same
spot of ground where they had lost it, which they would not
superstitiously change, as if there were any thing unlucky in the place.
But they were this second time repulsed, with the loss of 18,000 men, v.
25. The former day\'s loss and this amounted to 40,000, which was just a
tenth part of the whole army, and the same number that they had drawn
out by lot to fetch victuals, v. 10. They decimated themselves for that
service, and now God again decimated them for the slaughter. But what
shall we say to these things, that so just and honourable a cause should
thus be put to the worst once and again? Were they not fighting God\'s
battle against sin? Had they not his commission? What, and yet miscarry
thus! 1. God\'s judgments are a great deep, and his way is in the sea.
Clouds and darkness are often round about him, but judgment and justice
are always the habitation of his throne. We may be sure of the
righteousness, when we cannot see the reasons, of God\'s proceedings. 2.
God would hereby show them, and us in them, that the race is not to the
swift nor the battle to the strong, that we are not to confide in
numbers, which perhaps the Israelites did with too much assurance. We
must never lay the weight on an arm of flesh, which only the Rock of
ages will bear. 3. God designed hereby to correct Israel for their sins.
They did well to show such a zeal against the wickedness of Gibeah: but
were there not with them, even with them, sins against the Lord their
God? Those must be made to know their own iniquity that are forward in
condemning the iniquity of others. Some think it was a rebuke to them
for not witnessing against the idolatry of Micah and the Danites, by
which their religion was corrupted, as they now did against the lewdness
of Gibeah and the Benjamites, by which the public peace was disturbed,
though God had particularly ordered them to levy war upon idolaters,
Deu. 13:12, etc. 4. God would hereby teach us not to think it strange if
a good cause should suffer defeat fore a while, nor to judge of the
merits of it by the success of it. The interest of grace in the heart,
and of religion in the world, may be foiled, and suffer great loss, and
seem to be quite run down, but judgment will be brought forth to victory
at last. Vincimur in praelio, sed non in bello-We are foiled in a
battle, but not in the whole campaign. Right may fall, but it shall
arise.

### Verses 26-48

We have here a full account of the complete victory which the Israelites
obtained over the Benjamites in the third engagement: the righteous
cause was victorious at last, when the managers of it amended what had
been amiss; for, when a good cause suffers, it is for want of good
management. Observe then how the victory was obtained, and how it was
pursued.

`I.` How the victory was obtained. Two things they had trusted too much to
in the former engagements-the goodness of their cause and the
superiority of their numbers. It was true that they had both right and
strength on their side, which were great advantages; but they depended
too much upon them, to the neglect of those duties to which now, this
third time, when they see their error, they apply themselves.

`1.` They were previously so confident of the goodness of their cause
that they thought it needless to address themselves to God for his
presence and blessing. They took it for granted that God would bless
them, nay, perhaps they concluded that he owed them his favour, and
could not in justice withhold it, since it was in defence of virtue that
they appeared and took up arms. But God having shown them that he was
under no obligation to prosper their enterprise, that he neither needed
them nor was tied to them, that they were more indebted to him for the
honour of being ministers of his justice than he to them for the
service, now they became humble petitioners for success. Before they
only consulted God\'s oracle, Who shall go up first? And, Shall we go
up? But now they implored his favour, fasted and prayed, and offered
burnt-offerings and peace-offerings (v. 26), to make an atonement for
sin and an acknowledgment of their dependence upon God, and as an
expression of their desire towards him. We cannot expect the presence of
God with us, unless we thus seek it in the way he has appointed. And
when they were in this frame, and thus sought the Lord, then he not only
ordered them to go up against the Benjamites the third time, but gave
them a promise of victory: Tomorrow I will deliver them into thy hand,
v. 28.

`2.` They were previously so confident of the greatness of their strength
that they thought it needless to use any art, to lay any ambush, or form
a stratagem, not doubting but to conquer purely by a strong hand; but
now they saw it was requisite to use some policy, as if they had an
enemy to deal with them that had been superior in number; accordingly,
they set liers in wait (v. 29), and gained their point, as their fathers
did before Ai (Jos. 8), stratagems of that kind being most likely to
take effect after a previous defeat, which has flushed the enemy, and
made the pretended flight the less suspected. The management of this
artifice is here very largely described. The assurance God had given
them of success in this day\'s action, instead of making them remiss and
presumptuous, set all heads and hands on work for the effecting of what
God had promised.

`(1.)` Observe the method they took. The body of the army faced the city
of Gibeah, as they had done before, advancing towards the gates, v. 30.
The Benjamites, the body of whose army was now quartered at Gibeah,
sallied out upon them, and charged them with great bravery. The
besiegers gave back. retired with precipitation, as if their hearts
failed them upon the sight of the Benjamites, which they were willing to
believe, proudly imagining that by their former success they had made
themselves very formidable. Some loss the Israelites sustained in this
counterfeit flight, about thirty men being cut off in their rear, v. 31,
39. But, when the Benjamites were all drawn out of the city, the ambush
seized the city (v. 37), gave a signal to the body of the army (v. 38,
40), which immediately turned upon them (v. 41), and, it should seem,
another considerable party that was posted at Baal-tamar came upon them
at the same time (v. 33); so that the Benjamites were quite surrounded,
which put them into the greatest consternation that could be. A sense of
guilt now disheartened them, and the higher their hopes had been raised
the more grievous was this confusion. At first the battle was sore (v.
34), the Benjamites fought with fury; but, when they saw what a snare
they were drawn into, they thought one pair of heels (as we say) was
worth two pair of hands, and they made the best of their way towards the
wilderness (v. 42); but in vain: the battle overtook them, and, to
complete their distress, those who came out of the cities of Israel,
that waited to see the event of the battle, joined with their pursuers,
and helped to cut them off. Every man\'s hand was against them.

`(2.)` Observe in this story, `[1.]` That the Benjamites, in the beginning
of the battle, were confident that the day was their own: They are
smitten down before us, v. 32, 39. Sometimes God suffers wicked men to
be lifted up in successes and hopes, that their fall may be the sorer.
See how short their joy is, and their triumphing but for a moment. Let
not him that girdeth on the harness boast, except he has reason to boast
in God. `[2.]` Evil was near them and they did not know it, v. 34. But
(v. 41) they saw, when it was too late to prevent it, that evil had come
upon them. What evils may at any time be near us we cannot tell, but the
less they are feared the heavier they fall. Sinners will not be
persuaded to see evil near them, but how dreadful will it be when it
comes and there is no escaping! 1 Th. 5:3. `[3.]` Though the men of
Israel played their parts so well in this engagement, yet the victory is
ascribed to God (v. 35): The Lord smote Benjamin before Israel. The
battle was his, and so was the success. `[4.]` They trode down the men
of Benjamin with ease when God fought against them, v. 43. It is an easy
thing to trample upon those who have made God their enemy. See Mal. 4:3.

`II.` How the victory was prosecuted and improved in a military execution
done upon these sinners against their own souls. 1. Gibeah itself, that
nest of lewdness, was destroyed in the first place. The ambush that
entered the city by surprise drew themselves along, that is, dispersed
themselves into the several parts of it, which they might easily do, now
that all the men of war had sallied out and very presumptuously left it
defenceless; and they smote all they found, even women and children,
with the sword (v. 37), and set fire to the city, v. 40. Sin brings ruin
upon cities. 2. The army in the field was quite routed and cut off:
18,000 men of valour lay dead upon the spot, v. 44. 3. Those that
escaped from the field were pursued, and cut off in their flight, to the
number of 7000, v. 45. It is to no purpose to think of out-running
divine vengeance. Evil pursues sinners, and it will overtake them. 4.
Even those that tarried at home were involved in the ruin. They let
their sword devour for ever, not considering that it would be bitterness
in the latter end, as Abner pleads long after, when he was at the head
of an army of Benjamites, probably with an eye to this very story, 2 Sa.
2:25, 26. They put to the sword all that breathed, and set fire to all
the cities, v. 48. So that of all the tribe of Benjamin, for aught that
appears, there remained none alive but 600 men that took shelter in the
rock Rimmon, and lay close there four months, v. 47. Now, `(1.)` It is
difficult to justify this severity as it was Israel\'s act. The whole
tribe of Benjamin was culpable; but must they therefore be treated as
devoted Canaanites? That it was done in the heat of war, that this was
the way of prosecuting victories which the sword of Israel had been
accustomed to, that the Israelites were extremely exasperated against
the Benjamites for the slaughter they had made among them in the two
former engagements, will go but a little way to excuse the cruelty of
this execution. It is true they had sworn that whosoever did not come up
to Mizpeh should be put to death, ch. 21:5. But that, if it was a
justifiable oath, yet extended only to the men of war; the rest were not
expected to come. Yet, `(2.)` It is easy to justify the hand of God in it.
Benjamin had sinner against him, and God had threatened that, if they
forgot him, they should perish as the nations that were before them
perished (Deu. 8:20), who were all in this manner cut off. `(3.)` It is
easy likewise to improve it for warning against the beginnings of sin:
they are like the letting forth of water, therefore leave it off before
it be meddled with, for we know not what will be in the end thereof. The
eternal ruin of souls will be worse, and more fearful, than all these
desolations of a tribe. This affair of Gibeah is twice spoken of by the
prophet Hosea as the beginning of the corruption of Israel and a pattern
to all that followed (Hos. 9:9): They have deeply corrupted themselves
as in the days of Gibeah; and (Hos. 10:9), Thou hast sinned from the
days of Gibeah; and it is added that the battle in Gibeah against the
children of iniquity did not (that is, did not at first) overtake them.
