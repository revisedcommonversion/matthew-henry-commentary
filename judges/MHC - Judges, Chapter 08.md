Judges, Chapter 8
=================

Commentary
----------

This chapter gives us a further account of Gideon\'s victory over the
Midianites, with the residue of the story of his life and government. `I.`
Gideon prudently pacifies the offended Ephraimites (v. 1-3). `II.` He
bravely pursues the flying Midianites (v. 4, 10-12). `III.` He justly
chastises the insolence of the men of Succoth and Penuel, who basely
abused him (v. 5-9), and were reckoned with for it (v. 13-17). `IV.` He
honourably slays the two kings of Midian (v. 18-21). `V.` After all this
he modestly declines the government of Israel (v. 22, 23). `VI.` He
foolishly gratified the superstitious humour of his people by setting up
an ephod in his own city, which proved a great snare (v. 24-27). `VII.` He
kept the country quiet for forty years (v. 28). `VIII.` He died in honour,
and left a numerous family behind him (v. 29-32). `IX.` Both he and his
God were soon forgotten by ungrateful Israel (v. 33-35).

### Verses 1-3

No sooner were the Midianites, the common enemy, subdued, than, through
the violence of some hot spirits, the children of Israel were ready to
quarrel among themselves; an unhappy spark was struck, which, if Gideon
had not with a great deal of wisdom and grace extinguished immediately,
might have broken out into a flame of fatal consequence. The
Ephraimites, when they brought the heads of Oreb and Zeeb to Gideon as
general, instead of congratulating him upon his successes and addressing
him with thanks for his great services, as they ought to have done,
picked a quarrel with him and grew very hot upon it.

`I.` Their accusation was very peevish and unreasonable: Why didst thou
not call us when thou wentest to fight with the Midianites? v. 1.
Ephraim was brother to Manasseh, Gideon\'s tribe, and had the
pre-eminence in Jacob\'s blessing and in Moses\'s, and therefore was
very jealous of Manasseh, lest that tribe should at any time eclipse the
honour of theirs. Hence we find Manasseh against Ephraim and Ephraim
against Manasseh, Isa. 9:21. A brother offended is harder to be won than
a strong city, and their contentions are as the bars of a castle, Prov.
18:19. But how unjust was their quarrel with Gideon! They were angry
that he did not send for them to begin the attack upon Midian, as well
as to follow the blow. Why were they not called to lead the van? The
post of honour, they thought, belonged to them. But, 1. Gideon was
called of God, and must act as he directed; he neither took the honour
to himself nor did he himself dispose of honours, but left it to God to
do all. So that the Ephraimites, in this quarrel, reflected upon the
divine conduct; and what was Gideon that they murmured against him? 2.
Why did not the Ephraimites offer themselves willingly to the service?
They knew the enemy was in their country, and had heard of the forces
that were raising to oppose them, to which they ought to have joined
themselves, in zeal for the common cause, though they had not a formal
invitation. Those seek themselves more than God that stand upon a point
of honour to excuse themselves from doing real service to God and their
generation. In Deborah\'s time there was a root of Ephraim, ch. 5:14.
Why did not this appear now? The case itself called them, they needed
not wait for a call from Gideon. 3. Gideon had saved their credit in not
calling them. If he had sent for them, no doubt may of them would have
gone back with the faint-hearted, or been dismissed with the lazy,
slothful, and intemperate; so that by not calling them he prevented the
putting of those slurs upon them. Cowards will seem valiant when the
danger is over, but those consult their reputation who try not their
courage when danger is near.

`II.` Gideon\'s answer was very calm and peaceable, and was intended not
so much to justify himself as to please and pacify them, v. 2, 3. He
answers them, 1. With a great deal of meekness and temper. He did not
resent the affront, nor answer anger with anger, but mildly reasoned the
case with them, and he won as true honour by this command which he had
over his own passion as by his victory over the Midianites. He that is
slow to anger is better than the mighty. 2. With a great deal of modesty
and humility, magnifying their performances above his own: Is not the
gleaning of the grapes of Ephraim, who picked up the stragglers of the
enemy, and cut off those of them that escaped, better than the vintage
of Abiezer-a greater honour to them, and better service to the country,
than the first attack Gideon made upon them? The destruction of the
church\'s enemies is compared to a vintage, Rev. 14:18. In this he owns
their gleanings better than his gatherings. The improving of a victory
is often more honourable, and of greater consequence, than the winning
of it; in this they had signalized themselves, and their own courage and
conduct, or, rather, God had dignified them; for thought, to magnify
their achievements, he is willing to diminish his own performances, yet
he will not take any flowers from God\'s crown to adorn theirs with:
\"God has delivered into your hands the princes of Midian, and a great
slaughter has been made of the enemy by your numerous hosts, and what
was I able to do with 300 men, in comparison of you and your brave
exploits?\" Gideon stands here a very great example of self-denial, and
this instance shows us, `(1.)` That humility of deportment is the best way
to remove envy. It is true even right works are often envied, Eccl. 4:4.
Yet they are not so apt to be so when those who do them appear not to be
proud of them. Those are malignant indeed who seek to cast down from
their excellency those that humble and abase themselves, `(2.)` It is
likewise the surest method of ending strife, for only by pride comes
contention, Prov. 13:10. `(3.)` Humility is most amiable and admirable in
the midst of great attainments and advancements. Gideon\'s conquests did
greatly set off his condescensions. `(4.)` It is the proper act of
humility to esteem others better than ourselves, and in honour to prefer
one another.

Now what was the issue of this controversy? The Ephraimites had chidden
with him sharply (v. 1), forgetting the respect due to their general and
one whom God had honoured, and giving vent to their passion in a very
indecent liberty of speech, a certain sign of a weak and indefensible
cause. Reason runs low when the chiding flies high. But Gideon\'s soft
answer turned away their wrath, Prov. 15:1. Their anger was abated
towards him, v. 3. It is intimated that they retained some resentment,
but he prudently overlooked it and let it cool by degrees. Very great
and good men must expect to have their patience tried by the
unkindnesses and follies even of those they serve and must not think it
strange.

### Verses 4-17

In these verses we have,

`I.` Gideon, as a valiant general, pursuing the remaining Midianites, and
bravely following his blow. A very great slaughter was made of the enemy
at first: 120,000 men that drew the sword, v. 10. Such a terrible
execution did they make among themselves, and so easy a prey were they
to Israel. But, it seems, the two kings of Midian, being better provided
than the rest for an escape, with 15,000 men got over Jordan before the
passes could be secured by the Ephraimites, and made towards their own
country. Gideon thinks he does not fully execute his commission to save
Israel if he let them escape. He is not content to chase them out of the
country, but he will chase them out of the world, Job 18:18. This
resolution is here pushed on with great firmness, and crowned with great
success.

`1.` His firmness was very exemplary. He effected his purpose under the
greatest disadvantages and discouragements that could be. `(1.)` He took
none with him but his 300 men, who now laid aside their trumpets and
torches, and betook themselves to their swords and spears. God had said,
By these 300 men will I save you (ch. 7:7); and, confiding in that
promise, Gideon kept to them only, v. 4. He expected more from 300 men,
supported by a particular promise, than from so many thousands supported
only by their own valour. `(2.)` They were faint, and yet pursuing, much
fatigued with what they had done, and yet eager to do more against the
enemies of their country. Our spiritual warfare must thus be prosecuted
with what strength we have, though we have but little; it is many a time
the true Christina\'s case, fainting and yet pursuing. `(3.)` Though he
met with discouragement from those of his own people, was jeered for
what he was doing, as going about what he could never accomplish, yet he
went on with it. If those that should be our helpers in the way of our
duty prove hindrances to us, let not this drive us off from it. Those
know not how to value God\'s acceptance that know not how to despise the
reproaches and contempts of men. `(4.)` He made a very long march by the
way of those that dwelt in tents (v. 11), either because he hoped to
find them kinder to him than the men of Succoth and Penuel, that dwelt
in walled towns (sometimes there is more generosity and charity found in
country tents than in city palaces), or because that was a road in which
he would be least expected, and therefore that way it would be the
greater surprise to them. It is evident he spared no pains to complete
his victory. Now he found it an advantage to have his 300 men such as
could bear hunger, and thirst, and toil. It should seem, he set upon the
enemy by night, as he had done before, for the host was secure. The
security of sinners often proves their ruin, and dangers are most fatal
when least feared.

`2.` His success was very encouraging to resolution and industry in a
good cause. He routed the army (v. 11), and took the two kings
prisoners, v. 12. Note, The fear of the wicked shall come upon him.
Those that think to run from the sword of the Lord and of Gideon do but
run upon it. If he flee from the iron weapon, yet the bow of steel shall
strike him through; for evil pursueth sinners.

`II.` Here is Gideon, as a righteous judge, chastising the insolence of
the disaffected Israelites, the men of Succoth and the men of Penuel,
both in the tribe of Gad, on the other side Jordan.

`1.` Their crime was great. Gideon, with a handful of feeble folk was
pursuing the common enemy, to complete the deliverance of Israel. His
way led him through the city of Succoth first and afterwards of Penuel.
He expected not that the magistrates should meet him in their
formalities, congratulate him upon his victory, present him with the
keys of their city, and give him a treat, much less that they should
send forces in to his assistance, though he was entitled to all this;
but he only begs some necessary food for his soldiers that were ready to
faint for want, and he does it very humbly and importunately: Give, I
pray you, loaves of bread unto the people that follow me, v. 5. The
request would have been reasonable if they had been but poor travellers
in distress; but considering that they were soldiers, called, and chose,
and faithful (Rev. 17:14), men whom God had greatly honoured and to whom
Israel was highly obliged, who had done great service to their country
and were now doing more,-that they were conquerors, and had power to put
them under contribution,-and that they were fighting God\'s battles and
Israel\'s,-nothing could be more just than that their brethren should
furnish them with the best provisions their city afforded. But the
princes of Succoth neither feared God nor regarded man. For, `(1.)` In
contempt of God, they refused to answer the just demands of him whom God
had raised up to save them, affronted him, bantered him, despised the
success he had already been honoured with, despaired of the success of
his present undertaking, did what they could to discourage him in
prosecuting the war, and were very willing to believe that the remaining
forces of Midian, which they had now seen march through their country,
would be too hard for him: Are the hands of Zebah and Zalmunna now in
thy hand? \"No, nor ever will be,\" so they conclude, judging by the
disproportion of numbers. `(2.)` The bowels of their compassion were shut
up against their brethren; they were as destitute of love as they were
of faith, would not give morsels of bread (so some read it) to those
that were ready to perish. Were these princes? were these Israelites?
unworthy either title, base and degenerate men! Surely they were
worshippers of Baal, or in the interests of Midian. The men of Penuel
gave the same answer to the same request, defying the sword of the Lord
and of Gideon, v. 8.

`2.` The warning he gave them of the punishment of their crime was very
fair. `(1.)` He did not punish it immediately, because he would not lose
so much time from the pursuit of the enemy that were flying from him,
because he would not seem to do it in a neat of passion, and because he
would do it more to their shame and confusion when he had completed his
undertaking, which they thought impracticable. But, `(2.)` He told them
how he would punish it (v. 7, 9), to show the confidence he had of
success in the strength of God, and that, if they had the least grain of
grace and consideration left, they might upon second thoughts repent of
their folly, humble themselves, and contrive how to atone for it, by
sending after him succours and supplies, which if they had done, no
doubt, Gideon would have pardoned them. God gives notice of danger, and
space to repent, that sinners may flee from the wrath to come.

`3.` The warning being slighted, the punishment, though very severe, was
really very just.

`(1.)` The princes of Succoth were first made examples. Gideon got
intelligence of their number, seventy-seven men, their names, and places
of abode, which were described in writing to him, v. 14. And, to their
great surprise, when they thought he had scarcely overtaken the
Midianites, he returned a conqueror. His 300 men were now the ministers
of his justice; they secured all these princes, and brought them before
Gideon, who showed them his royal captives in chains. \"These are the
men you thought me an unequal match for, and would give me no assistance
in the pursuit of,\" v. 15. And he punished them with thorns and briers,
but, it should seem, not unto death. With these, `[1.]` He tormented
their bodies, either by scourging or by rolling them in the thorns and
briers; some way or other he tore their flesh, v. 7. Those shall have
judgment without mercy that have shown no mercy. Perhaps he observed
them to be soft and delicate men, who despised him and his company for
their roughness and hardiness, and therefore Gideon thus mortified them
for their effeminacy. `[2.]` He instructed their minds: With these he
taught the men of Succoth, v. 16. The correction he gave them was
intended, not for destruction, but wholesome discipline, to make them
wiser and better for the future. He made them know (so the word is),
made them know themselves and their folly, God and their duty, made them
know who Gideon was, since they would not know by the success wherewith
God had crowned him. Note, Many are taught with the briers and thorns of
affliction that would not learn otherwise. God gives wisdom by the rod
and reproof, chastens and teaches, and by correction opens the ear to
discipline. Our blessed Saviour, though he was a Son, yet learnt
obedience by the things which he suffered, Heb. 5:8. Let every pricking
brier, and grieving thorn, especially when it becomes a thorn in the
flesh, be thus interpreted, thus improved. \"By this God designs to
teach me; what good lesson shall I learn?\"

`(2.)` The doom of the men of Penuel comes next, and it should seem he
used them more severely than the other, for good reason, no doubt, v.
17. `[1.]` He beat down their tower, of which they gloried, in which
they trusted, perhaps scornfully advising Gideon and his men rather to
secure themselves in that than to pursue the Midianites. What men make
their pride is justly by its ruin made their shame. `[2.]` He slew the
men of the city, not all, perhaps not the elders or princes, but those
that had affronted him, and those only. He slew some of the men of the
city that were most insolent and abusive, for terror to the rest, and so
he taught the men of Penuel.

### Verses 18-21

Judgment began at the house of God, in the just correction of the men of
Succoth and Penuel, who were Israelites, but it did not end there. The
kings of Midian, when they had served to demonstrate Gideon\'s
victories, and grace his triumphs, must now be reckoned with. 1. They
are indicted for the murder of Gideon\'s brethren some time ago at Mount
Tabor. When the children of Israel, for fear of the Midianites, made
themselves dens in the mountains (ch. 6:2), those young men, it is
likely, took shelter in that mountain, where they were found by these
two kings, and most basely and barbarously slain in cold blood. When he
asks them what manner of men they were (v. 18), it is not because he was
uncertain of the thing, or wanted proof of it; he was not so little
concerned for his brethren\'s blood as not to enquire it out before now,
nor were these proud tyrants solicitous to conceal it. But he puts that
question to them that by their acknowledgment of the more than ordinary
comeliness of the persons they slew their crime might appear the more
heinous, and consequently their punishment the more righteous. They
could not but own that, though they were found in a mean and abject
condition, yet they had an unusual greatness and majesty in their
countenances, not unlike Gideon himself at this time: they resembled the
children of a king, born for something great. 2. Being found guilty of
this murder by their own confession, Gideon, though he might have put
them to death as Israel\'s judge for the injuries done to that people in
general, as Oreb and Zeeb (ch. 7:25), yet chooses rather to put on the
character of an avenger of blood, as next of kin to the persons slain:
They were my brethren, v. 19. Their other crimes might have been
forgiven, at least Gideon would not have slain them himself, let them
have answered it to the people; but the voice of his brethren\'s blood
cries, cries to him, now it is in the power of his hand to avenge it,
and therefore there is no remedy-by him must their blood be shed, though
they were kings. Little did they think to hear of this so long after;
but murder seldom goes unpunished even in this life. 3. The execution is
done by Gideon himself with his own hand, because he was the avenger of
blood; he bade his son slay them, for he was a near relation to the
persons murdered, and fittest to be his father\'s substitute and
representative, and he would thus train him up to the acts of justice
and boldness, v. 20. But, `(1.)` The young man himself desired to be
excused; he feared, though they were bound and could make no resistance,
because he was yet a youth, and not used to such work: courage does not
always run in the blood. `(2.)` The prisoners themselves desired that
Gideon would excuse it (v. 21), begged that, if they must die, they
might die by his own hand, which would be somewhat more honourable to
them, and more easy; for by his great strength they would sooner be
dispatched and rid out of their pain. As is the man, so is his strength.
Either they mean it of themselves (they were men of such strength as
called for a better hand than that young man\'s to overpower quickly) or
of Gideon, \"Thou art at thy full strength; he has not yet come to it;
therefore be thou the executioner.\" From those that are grown up to
maturity, it is expected that what they do in any service be done with
so much the more strength. Gideon dispatched them quickly, and seized
the ornaments that were on their camels\' necks, ornaments like the
moon, so it is in the margin, either badges of their royalty or perhaps
of their idolatry, for Ashteroth was represented by the moon, as Baal by
the sun. With there he took all their other ornaments, as appears v. 26,
where we find that he did not put them to so good a use as one would
have wished. The destruction of these two kings, and that of the two
princes (ch. 7:25) is long afterwards pleaded as a precedent in prayer
for the ruin of others of the church\'s enemies, Ps. 83:11, Make their
nobles like Oreb and Zeeb, and all their princes as Zebah and Zalmunna,
let them all be but off in like manner.

### Verses 22-28

Here is, `I.` Gideon\'s laudable modesty, after his great victory, in
refusing the government which the people offered him. 1. It was honest
in them to offer it: Rule thou over us, for thou hast delivered us, v.
22. They thought it very reasonable that he who had gone through the
toils and perils of their deliverance should enjoy the honour and power
of commanding them ever afterwards, and very desirable that he who in
this great and critical juncture had had such manifest tokens of God\'s
presence with him should ever afterwards preside in their affairs. Let
us apply it to the Lord Jesus: he hath delivered us out of the hands of
our enemies, our spiritual enemies, the worst and most dangerous, and
therefore it is fit he should rule over us; for how can we be better
ruled than by one that appears to have so great an interest in heaven
and so great a kindness for this earth? We are delivered that we may
serve him without fear, Lu. 1:74, 75. 2. It was honourable in him to
refuse it: I will not rule over you, v. 23. What he did was with a
design to serve them, not to rule them-to make them safe, easy, and
happy, not to make himself great or honourable. And, as he was not
ambitious of grandeur himself, so he did not covet to entail it upon his
family: \"My son shall not rule over you, either while I live or when I
am gone, but the Lord shall still rule over you, and constitute your
judges by the special designation of his own Spirit, as he has done.\"
This intimates, `(1.)` His modesty, and the mean opinion he had of himself
and his own merits. He thought the honour of doing good was recompence
enough for all his services, which needed not to be rewarded with the
honour of bearing sway. He that is greatest, let him be your minister.
`(2.)` His piety, and the great opinion he had of God\'s government.
Perhaps he discerned in the people a dislike of the theocracy, or divine
government, a desire of a king like the nations, and thought they
availed themselves of his merits as a colourable pretence to move for
this change of government. But Gideon would by no means admit it. No
good man can be pleased with any honour done to himself which ought to
be peculiar to God. Were you baptized in the name of Paul? 1 Co. 1:13.

`II.` Gideon\'s irregular zeal to perpetuate the remembrance of this
victory by an ephod made of the choicest of the spoils. 1. He asked the
men of Israel to give him the ear-rings of their prey; for such
ornaments they stripped the slain of in abundance. These he demanded,
either because they were the finest gold, and therefore fittest for a
religious use, or because they had had as ear-rings some superstitious
signification, which he thought too well of. Aaron called for the
ear-rings to make the golden calf of, Ex. 32:2. These Gideon begged v.
24. And he had reason enough to think that those who offered him a
crown, when he declined it, would not deny him their ear-rings, when he
begged them, nor did they, v. 25. 2. He himself added the spoil he took
from the kings of Midian, which, it should seem, had fallen to his
share, v. 26. The generals had that part of the prey which was most
splendid, the prey of divers colours, ch. 5:30. 3. Of this he made an
ephod, v. 27. It was plausible enough, and might be well intended to
preserve a memorial of so divine a victory in the judge\'s own city. But
it was a very unadvised thing to make that memorial to be an ephod, a
sacred garment. I would gladly put the best construction that can be
upon the actions of good men, and such a one we are sure Gideon was. But
we have reason to suspect that this ephod had, as usual, a teraphim
annexed to it (Hos. 3:4), and that, having an altar already built by
divine appointment (ch. 6:26), which he erroneously imagined he might
still use for sacrifice, he intended this for an oracle, to be consulted
in doubtful cases. So the learned Dr. Spencer supposes. Each tribe
having now very much its government within itself, they were too apt to
covet their religion among themselves. We read very little of Shiloh,
and the ark there, in all the story of the Judges. Sometimes by divine
dispensation, and much oftener by the transgression of men, that law
which obliged them to worship only at that one altar seems not to have
been so religiously observed as one would have expected, any more than
afterwards, when in the reigns even of very good kings the high places
were not taken away, from which we may infer that that law had a further
reach as a type of Christ, by whose mediation alone all our services are
accepted. Gideon therefore, through ignorance or inconsideration, sinned
in making this ephod, though he had a good intention in it. Shiloh, it
is true, was not far off, but it was in Ephraim, and that tribe had
lately disobliged him (v. 1), which made him perhaps not care to go so
often among them as his occasions would lead him to consult the oracle,
and therefore he would have one nearer home. However this might be
honestly intended, and at first did little hurt, yet in process of time,
`(1.)` Israel went a whoring after it, that is, they deserted God\'s altar
and priesthood, being fond of change, and prone to idolatry, and having
some excuse for paying respect to this ephod, because so good a man as
Gideon had set it up, and by degrees their respect to it grew more and
more superstitious. Note, Many are led into false ways by one false step
of a good man. The beginning of sin, particularly of idolatry and
will-worship, is as the letting forth of water, so it has been found in
the fatal corruptions of the church of Rome; therefore leave it off
before it be meddled with. `(2.)` It became a snare to Gideon himself,
abating his zeal for the house of God in his old age, and much more to
his house, who were drawn by it into sin, and it proved the ruin of the
family.

`III.` Gideon\'s happy agency for the repose of Israel, v. 28. The
Midianites that had been so vexatious gave them no more disturbance.
Gideon, though he would not assume the honour and power of a king,
governed as a judge, and did all the good offices he could for his
people; so that the country was in quietness forty years. Hitherto the
times of Israel had been reckoned by forties. Othniel judged forty
years, Ehud eighty-just two forties, Barak forty, and now Gideon forty,
providence so ordering it to bring in mind the forty years of their
wandering in the wilderness. Forty years long was I grieved with this
generation. And see Eze. 4:6. After these, Eli ruled forty years (1 Sa.
4:18), Samuel and Saul forty (Acts 13:21), David forty, and Solomon
forty. Forty years is about an age.

### Verses 29-35

We have here the conclusion of the story of Gideon. 1. He lived
privately, v. 29. He was not puffed up with his great honours, did not
covet a palace or castle to dwell in, but retired to the house he had
lived in before his elevation. Thus that brave Roman Who was called from
the plough upon a sudden occasion to command the army when the action
was over returned to his plough again. 2. His family was multiplied. He
had many wives (therein he transgressed the law); by them he had seventy
sons (v. 30), but by a concubine he had one whom he named Abimelech
(which signifies, my father a king), that proved the ruin of his family,
v. 31. 3. He died in honour, in a good old age, when he had lived as
long as he was capable of serving God and his country; and who would
desire to live any longer? And he was buried in the sepulchre of his
fathers. 4. After his death the people corrupted themselves, and went
all to naught. As soon as ever Gideon was dead, who had kept them close
to the worship of the God of Israel, they found themselves under no
restraint, and then they went a whoring after Baalim, v. 33. They went a
whoring first after another ephod (v. 27), for which irregularity Gideon
had himself given them too much occasion, and now they went a whoring
after another god. False worships made way for false deities. They now
chose a new god (ch. 5:8), a god of a new name, Baal-berith (a goddess,
say some); Berith, some think, was Berytus, the place where the
Phoenicians worshipped this idol. The name signifies the Lord of a
covenant. Perhaps he was so called because his worshippers joined
themselves by covenant to him, in imitation of Israel\'s covenanting
with God; for the devil is God\'s ape. In this revolt of Israel to
idolatry they showed, `(1.)` Great ingratitude to God (v. 34): They
remembered not the Lord, not only who had delivered them into the hands
of their enemies, to punish them for their idolatry, but who had also
delivered them out of the hands of their enemies, to invite them back
again into his service; both the judgments and the mercies were
forgotten, and the impressions of them lost. `(2.)` Great ingratitude to
Gideon, v. 35. A great deal of goodness he had shown unto Israel, as a
father to his country, for which they ought to have been kind to his
family when he was gone, for that is one way by which we ought to show
ourselves grateful to our friends and benefactors, and may be returning
their kindnesses when they are in their graves. But Israel showed not
this kindness to Gideon\'s family, as we shall find in the next chapter.
No wonder if those who forget their God forget their friends.
