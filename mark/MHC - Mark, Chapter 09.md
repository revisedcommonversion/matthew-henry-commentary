Mark, Chapter 9
===============

Commentary
----------

In this chapter, we have, `I.` Christ\'s transfiguration upon the mount
(v. 1-13). `II.` His casting the devil out of a child, when the disciples
could not do it (v. 14-29). `III.` His prediction of his own sufferings
and death (v. 30-32). `IV.` The check he gave to his disciples for
disputing who should be greatest (v. 33-37); and to John for rebuking
one who cast out devils in Christ\'s name, and did not follow with them
(v. 38-41). `V.` Christ\'s discourse with his disciples of the danger of
offending one of his little ones (v. 42), and of indulging that in
ourselves, which is an offence and an occasion of sin to us (v. 43-50),
most of which passages we had before, Mt. 17 and 18.

### Verses 1-13

Here is, `I.` A prediction of Christ\'s kingdom now near approaching, v.
`1.` That which is foretold, is, 1. That the kingdom of God would come,
and would come so as to be seen: the kingdom of the Messiah shall be set
up in the world by the utter destruction of the Jewish polity, which
stood in the way of it; this was the restoring of the kingdom of God
among men, which had been in a manner lost by the woeful degeneracy both
of Jews and Gentiles. 2. That it would come with power, so as to make
its own way, and bear down the opposition that was given to it. It came
with power, when vengeance was taken on the Jews for crucifying Christ,
and when it conquered the idolatry of the Gentile world. 3. That it
would come while some now present were alive; There are some standing
here, that shall not taste of death, till they see it; this speaks the
same with Mt. 24:34, This generation shall not pass, till all these
things be fulfilled. Those that were standing here with Christ, should
see it, when the others could not discern it to be the kingdom of God,
for it came not with observation.

`II.` A specimen of that kingdom in the transfiguration of Christ, six
days after Christ spoke that prediction. He had begun to give notice to
his disciples of his death and sufferings; and, to prevent their offence
at that, he gives them this glimpse of his glory, to show that his
sufferings were voluntary, and what a virtue the dignity and glory of
his person would put into them, and to prevent the offence of the cross.

`1.` It was on the top of a high mountain, like the converse Moses had
with God, which was on the top of mount Sinai, and his prospect of
Canaan from the top of mount Pisgah. Tradition saith, It was on the top
of the mount Tabor that Christ was transfigured; and if so, the
scripture was fulfilled, Tabor and Hermon shall rejoice in thy name, Ps.
89:12. Dr. Lightfoot, observing that the last place where we find Christ
was in the coasts of Caesarea-Philippi, which was far from mount Tabor,
rather thinks it was a high mountain which Josephus speaks of, near
Caesarea.

`2.` The witnesses of it were Peter, James, and John; these were the
three that were to bear record on earth, answering to Moses, Elias, and
the voice from heaven, the three that were to bear record from above.
Christ did not take all the disciples with him, because the thing was to
be kept very private. As there are distinguishing favours which are
given to disciples and not to the world, so there are to some disciples
and not to others. All the saints are a people near to Christ, but some
lie in his bosom. James was the first of all the twelve that died for
Christ, and John survived them all, to be the last eyewitness of this
glory; he bore record (Jn. 1:14); We saw his glory: and so did Peter, 2
Pt. 1:16-18.

`3.` The manner of it; He was transfigured before them; he appeared in
another manner than he used to do. This was a change of the accidents,
the substance remaining the same, and it was a miracle. But
transubstantiation, the change of the substance, all the accidents
remaining the same, is not a miracle, but a fraud and imposture, such a
work as Christ never wrought. See what a great change human bodies are
capable of, when God is pleased to put an honour upon them, as he will
upon the bodies of the saints, at the resurrection. He was transfigured
before them; the change, it is probable, was gradual, from glory to
glory, so that the disciples, who had their eye upon him all the while,
had the clearest and most certain evidence they could have, that this
glorious appearance was no other than the blessed Jesus himself, and
there was no illusion in it. John seems to refer to this (1 Jn. 1:1),
when he speaks of the word of life, as that which they had seen with
their eyes, and looked upon. His raiment became shining; so that, though
probably, it was sad-coloured, if not black, yet it was now exceeding
white as snow, beyond what the fuller\'s art could do toward whitening
it.

`4.` His companions in this glory were Moses and Elias (v. 4); They
appeared talking with him, not to teach him, but to testify to him, and
to be taught by him; by which it appears that there are converse and
intercourse between glorified saints, they have ways of talking one with
another, which we understand not. Moses and Elias lived at a great
distance of time one from another, but that breaks no squares in heaven,
where the first shall be last, and the last first, that is, all one in
Christ.

`5.` The great delight that the disciples took in seeing this sight, and
hearing this discourse, is expressed by Peter, the mouth of the rest; He
said, Master, it is good for us to be here, v. 5. Though Christ was
transfigured, and was in discourse with Moses and Elias, yet he gave
Peter leave to speak to him, and to be as free with him as he used to
be. Note, Our Lord Jesus, in his exaltation and glory, doth not at all
abate of his condescending kindness to his people. Many, when they are
in their greatness, oblige their friends to keep their distance; but
even to the glorified Jesus true believers have access with boldness,
and freedom of speech with him. Even in this heavenly discourse there
was room for Peter to put in a word; and this is it, \"Lord, it is good
to be here, it is good for us to be here; here let us make tabernacles;
let this be our rest for ever.\" Note, Gracious souls reckon it good to
be in communion with Christ, good to be near him, good to be in the
mount with him, though it be a cold and solitary place; it is good to be
here retired from the world, and alone with Christ: and if it is good to
be with Christ transfigured only upon a mountain with Moses and Elias,
how good it will be to be with Christ glorified in heaven with all the
saints! But observe, While Peter was for staying here, he forgot what
need there was of the presence of Christ, and the preaching of his
apostles, among the people. At this very time, the other disciples
wanted them greatly, v. 14. Note, When it is well with us, we are apt to
be mindless of others, and in the fulness of our enjoyments to forget
the necessities of our brethren; it was a weakness in Peter to prefer
private communion with God before public usefulness. Paul is willing to
abide in the flesh, rather than depart to the mountain of glory (though
that be far better), when he sees it needful for the church, Phil. 1:24,
25. Peter talked of making three distinct tabernacles for Moses, Elias,
and Christ, which was not well-contrived; for such a perfect harmony
there is between the law, the prophets, and the gospel, that one
tabernacle will hold them all; they dwell together in unity. But
whatever was incongruous in what he said, he may be excused, for they
were all sore afraid; and he, for his part, wist not what to say (v. 6),
not knowing what would be the end thereof.

`6.` The voice that came from heaven, was an attestation of Christ\'s
mediatorship, v. 7. There was a cloud that overshadowed them, and was a
shelter to them. Peter had talked of making tabernacles for Christ and
his friends; but while he yet spoke, see how his project was superseded;
this cloud was unto them instead of tabernacles for their shelter (Isa.
4:5); while he spoke of his tabernacles, God created his tabernacle not
made with hands. Now out of this cloud (which was but a shade to the
excellent glory Peter speaks of, whence this voice came) it was said,
This is my beloved Son, hear him. God owns him, and accepts him, as his
beloved Son, and is ready to accept of us in him; we must then own and
accept him as our beloved Saviour, and must give up ourselves to be
ruled by him.

`7.` The vision, being designed only to introduce the voice, when that
was delivered, disappeared (v. 8); Suddenly when they had looked round
about, as men amazed to see where they were, all was gone, they saw no
man any more. Elias and Moses were vanished out of sight, and Jesus only
remained with them, and he not transfigured, but as he used to be. Note,
Christ doth not leave the soul, when extraordinary joys and comforts
leave it. Though more sensible and ravishing communications may be
withdrawn, Christ\'s disciples have, and shall have, his ordinary
presence with them always, even to the end of the world, and that is it
we must depend upon. Let us thank God for daily bread and not expect a
continual feast on this side of heaven.

`8.` We have here the discourse between Christ and his disciples, as they
came down from the mount.

`(1.)` He charged them to keep this matter very private, till he was risen
from the dead, which would complete the proof of his divine mission, and
then this must be produced with the rest of the evidence, v. 9. And
besides, he, being now in a state of humiliation, would haves nothing
publicly taken notice of, that might be seen disagreeable to such a
state; for to that he would in every thing accommodate himself. This
enjoining of silence to the disciples, would likewise be of use to them,
to prevent their boasting of the intimacy they were admitted to, that
they might not be puffed up with the abundance of the revelations. It is
a mortification to a man, to be tied up from telling of his
advancements, and may help to hide pride from him.

`(2.)` The disciples were at a loss what the rising from the dead should
mean; they could not form any notion of the Messiah\'s dying (Lu.
18:34), and therefore were willing to think that the rising he speaks
of, was figurative, his rising from his present mean and low estate to
the dignity and dominion they were in expectation of. But if so, here is
another thing that embarrasses them (v. 11); Why say the Scribes, that
before the appearing of the Messiah in his glory, according to the order
settled in the prophecies of the Old Testament, Elias must first come?
But Elias was gone, and Moses too. Now that which raised this
difficulty, was, the scribes taught them to expect the person of Elias,
whereas the prophecy intended one in the spirit and power of Elias.
Note, The misunderstanding of scripture is a great prejudice to the
entertainment of truth.

`(3.)` Christ gave them a key to the prophecy concerning Elias (v. 12,
13); \"It is indeed prophesied that Elias will come, and will restore
all things, and set them to rights; and (though you will not understand
it) it is also prophesied of the Son of man, that he must suffer many
things, and be set at nought, must be a reproach of men, and despised of
the people: and though the scribes do not tell you so, the scriptures
do, and you have as much reason to expect that as the other, and should
not make so strange of it; but as to Elias, I tell you he is come; and
if you consider a little, you will understand whom I mean, it is one to
whom they have done whatsoever they listed;\" which was very applicable
to the ill usage they had given John Baptist. Many of the ancients, and
the Popish writers generally, think, that besides the coming of John
Baptist in the spirit of Elias, himself in his own person is to be
expected, with Enoch, before the second appearance of Christ, wherein
the prophecy of Malachi will have a more full accomplishment than it had
in John Baptist. But it is groundless fancy; the true Elias, as well as
the true Messiah promised, is come, and we are to look for no other.
These words as it is written of him, refer not to their doing to him
whatever they listed (that comes in a parenthesis), but only to his
coming. He is come, and hath been, and done, according as was written of
him.

### Verses 14-29

We have here the story of Christ casting the devil out of a child,
somewhat more fully related than it was in Mt. 17:14, etc. Observe here,

`I.` Christ\'s return to his disciples, and the perplexity he found them
in. He laid aside his robes of glory, and came to look after his family,
and to enquire what was become of them. Christ\'s glory above does not
make him forget the concerns of his church below, which he visits in
great humility, v. 14. And he came very seasonably, when the disciples
were embarrassed and run a-ground; the scribes, who were sworn enemies
both to him and them, had gained an advantage against them. A child
possessed with a devil was brought to them, and they could not cast out
the devil, whereupon the scribes insulted over them, and reflected upon
their Master, and triumphed as if the day were their own. He found the
scribes questioning with them, in the hearing of the multitude, some of
whom perhaps began to be shocked by it. Thus Moses, when he came down
from the mount, found the camp of Israel in great disorder; so soon were
Christ and Moses missed. Christ\'s return was very welcome, no doubt, to
the disciples, and unwelcome to the scribes. But particular notice is
taken of its being very surprising to the people, who perhaps were ready
to say, As for this Jesus, we wot not what is become of him; but when
they beheld him coming to them again, they were greatly amazed (some
copies add, kai exephobeµtheµsan-and they were afraid); and running to
him (some copies for prostrechontes, read proschairontes-congratulating
him, or bidding him welcome), they saluted him. It is easy to give a
reason why they should be glad to see him; but why where they amazed,
greatly amazed, when they beheld him? Probably, there might remain
something unusual in his countenance; as Moses\'s face shone when he
came down from the mount, which made the people afraid to come nigh him,
Ex. 34:30. So perhaps did Christ\'s face, in some measure; at least,
instead of seeming fatigued, there appeared a wonderful briskness and
sprightliness in his looks, which amazed them.

`II.` The case which perplexed the disciples, brought before him. He
asked the scribes, who, he knew, were always vexatious to his disciples,
and teazing them upon every occasion, \"What question ye with them? What
is the quarrel now?\" The scribes made no answer, for they were
confounded at his presence; the disciples made none, for they were
comforted, and now left all to him. But the father of the child opened
the case, v. 17, 18. 1. His child is possessed with a dumb spirit; he
has the falling-sickness, and in his fits is speechless; his case is
very sad, for, wheresoever the fit takes him, the spirit tears him,
throws him into such violent convulsions as almost pull him to pieces;
and, which is very grievous to himself, and frightful to those about
him, he foams at his mouth, and gnashes with his teeth, as one in pain
and great misery; and though the fits go off presently, yet they leave
him so weak, that he pines away, is worn to a skeleton; his flesh is
dried away; so the word signifies, Ps. 102:3-5. This was a constant
affliction to a tender father. 2. The disciples cannot give him any
relief; \"I desired they would cast him out, as they had done many, and
they would willingly have done it, but they could not; and therefore
thou couldest never have come in better time; Master, I have brought him
to thee.\"

`III.` The rebuke he gave to them all (v. 19); O faithless generation,
how long shall I be with you? How long shall I suffer you? Dr. Hammond
understands this as spoken to the disciples, reproving them for not
exerting the power he had given them, and because they did not fast and
pray, as in some cases he had directed them to do. But Dr. Whitby takes
it as a rebuke to the scribes, who gloried in this disappointment that
the disciples met with, and hoped to run them down with it. Them he
calls a faithless generation, and speaks as one weary of being with
them, and of bearing with them. We never heard him complaining, \"How
long shall I be in this low condition, and suffer that?\" But, \"How
long shall I be among these faithless people, and suffer them?\"

`IV.` The deplorable condition that the child was actually in, when he
was brought to Christ, and the doleful representation which the father
made of it. When the child saw Christ, he fell into a fit; The spirit
straightway tore him, boiled within him, troubled him (so Dr. Hammond);
as if the devil would set Christ at defiance, and hoped to be too hard
for him too, and to keep possession in spite of him. The child fell on
the ground, and wallowed foaming. We may put another construction upon
it-that the devil raged, and had so much the greater wrath, because he
knew that his time was short, Rev. 7:12. Christ asked, How long since
this came to him? And, it seems, the disease was of long standing; it
came to him of a child (v. 21), which made the case the more sad, and
the cure more difficult. We are all by nature children of disobedience,
and in such the evil spirit works, and has done so from our childhood;
for foolishness is bound in the heart of a child, and nothing but the
mighty grace of Christ can cast it out.

`V.` The pressing instances which the father of the child makes with
Christ for a cure (v. 22); Ofttimes it hath cast him into the fire, and
into the waters, to destroy him. Note, The devil aims at the ruin of
those in whom he rules and works, and seeks whom he may devour. But, if
thou canst do any thing, have compassion on us, and help us. The leper
was confident of Christ\'s power, but put an if upon his will (Mt. 8:2);
If thou wilt, thou canst. This poor man referred himself to his
good-will, but put an if upon his power, because his disciples, who cast
out devils in his name, had been non-plussed in this case. Thus Christ
suffers in his honour by the difficulties and follies of his disciples.

`VI.` The answer Christ gave to his address (v. 23); If thou canst
believe, all things are possible to him that believeth. Here, 1. He
tacitly checks the weakness of his faith. The sufferer put it upon
Christ\'s power, If thou canst do any thing, and reflected on the want
of power in the disciples; but Christ turns it upon him, and puts him
upon questioning his own faith, and will have him impute the
disappointment to the want of that; If thou canst believe. 2. He
graciously encourages the strength of his desire; \"All things are
possible, will appear possible, to him that believes the almighty power
of God, to which all things are possible;\" or \"That shall be done by
the grace of God, for them that believe in the promise of God, which
seemed utterly impossible.\" Note, In dealing with Christ, very much is
put upon our believing, and very much promised it. Canst thou believe?
Darest thou believe? Art thou willing to venture thy all in the hands of
Christ? To venture all thy spiritual concerns with him, and all thy
temporal concerns for him? Canst thou find in thy heart to do this? If
so, it is not impossible but that, though thou has been a great sinner,
thou mayest be reconciled; though thou art very mean and unworthy, thou
mayest get to heaven. If thou canst believe, it is possible that thy
hard heart may be softened, thy spiritual diseases may be cured; and
that, weak as thou art, thou mayest be able to hold out to the end.

`VII.` The profession of faith which the poor man made hereupon (v. 24);
He cried out, \"Lord, I believe; I am fully persuaded both of thy power
and of thy pity; my cure shall not be prevented by the want of faith;
Lord, I believe.\" He adds a prayer for grace to enable him more firmly
to rely upon the assurances he had of the ability and willingness of
Christ to save; Help thou my unbelief. Note, 1. Even those who through
grace can say, Lord, I believe, have reason to complain of their
unbelief; that they cannot so readily apply to themselves, and their own
case, the word of Christ as they should, no so cheerfully depend upon
it. 2. Those that complain of unbelief, must look up to Christ for grace
to help them against it, and his grace shall be sufficient for them.
\"Help mine unbelief, help me to a pardon for it, help me with power
against it; help out what is wanting in my faith with thy grace, the
strength of which is perfected in our weakness.\"

`VIII.` The cure of the child, and the conquest of this raging devil in
the child. Christ saw the people come running together, expecting to see
the issue of this trial of skill, and therefore kept them in suspense no
longer, but rebuked the foul spirit; the unclean spirit, so it should be
rendered, as in other places. Observe, 1. What the charge was which
Christ gave to this unclean spirit; \"Thou dumb and deaf spirit, that
makest the poor child dumb and deaf, but shalt thyself be made to hear
thy doom, and not be able to say any thing against it, come out of him
immediately, and enter no more into him. Let him not only be brought out
of this fit, but let his fits never return.\" Note, Whom Christ cures,
he cures effectually. Satan may go out himself, and yet recover
possession; but if Christ cast him out, he will keep him out. 2. How the
unclean spirit took it; he grew yet more outrageous, he cried, and rent
him sore, gave him such a twitch at parting, that he was as one dead; so
loth was he to quit his hold, so exasperated at the superior power of
Christ, so malicious to the child, and so desirous was he to kill him.
Many said, He is dead. Thus the toss that a soul is in at the breaking
of Satan\'s power in it may perhaps be frightful for the present, but
opens the door to lasting comfort. 3. How the child was perfectly
restored (v. 27); Jesus took him by the hand, krateµsas-took fast hold
of him, and strongly bore him up, and he arose and recovered, and all
was well.

`IX.` The reason he gave to the disciples why they could not cast out
this devil. They enquired of him privately why they could not, that
wherein they were defective might be made up another time, and they
might not again be thus publicly shamed; and he told them (v. 29), This
kind can come forth by nothing but prayer and fasting. Whatever other
difference there really might be, none appears between this and other
kinds, but that the unclean spirit had had possession of this poor
patient from a child, and that strengthened his interest, and confirmed
his hold. When vicious habits are rooted by long usage, and begin to
plead prescription, like chronical diseases that are hardly cured. Can
the Aethiopian change his skin? The disciples must not think to do their
work always with a like ease; some services call them to take more than
ordinary pains; but Christ can do that with a word\'s speaking, which
they must prevail for the doing of by prayer and fasting.

### Verses 30-40

Here, `I.` Christ foretels his own approaching sufferings. He passed
through Galilee with more expedition than usual, and would not that any
man should know of it (v. 30); because he had done many mighty and good
works among them in vain, they shall not be invited to see them and have
the benefit of them, as they have been. The time of his sufferings drew
nigh, and therefore he was willing to be private awhile, and to converse
only with his disciples, to prepare them for the approaching trial, v.
31. He said to them, The Son of man is delivered by the determinate
council and fore-knowledge of God into the hands of men (v. 31), and
they shall kill him. He had been delivered into the hands of devils, and
they had worried him, it had not been so strange; but that men, who have
reason, and should have love, that they should be thus spiteful to the
Son of man, who came to redeem and save them, is unaccountable. But
still it is observable that when Christ spoke of his death, he alway
spoke of his resurrection, which took away the reproach of it from
himself, and should have taken away the grief of it from his disciples.
But they understood not that saying, v. 32. The words were plain enough,
but they could not be reconciled to the thing, and therefore would
suppose them to have some mystical meaning which they did not
understand, and they were afraid to ask him; not because he was
difficult of access, or stern to those who consulted him, but either
because they were loth to know the truth, or because they expected to be
chidden for their backwardness to receive it. Many remain ignorant
because they are ashamed to enquire.

`II.` He rebukes his disciples for magnifying themselves. When he came to
Capernaum, he privately asked his disciples what it was they disputed
among themselves by the way, v. 33. He knew very well what the dispute
was, but he would know it from them, and would have them to confess
their fault and folly in it. Note, 1. We must all expect to be called to
an account by our Lord Jesus, concerning what passes while we are in the
way in this state of passage and probation. 2. We must in a particular
manner be called to an account about our discourses among ourselves; for
by our words we must be justified or condemned. 3. As our other
discourses among ourselves by the way, so especially our disputes, will
be all called over again, and we shall be called to an account about
them. 4. Of all disputes, Christ will be sure to reckon with his
disciples for their disputes about precedency and superiority: that was
the subject of the debate here, who should be the greater, v. 34.
Nothing could be more contrary to the two great laws of Christ\'s
kingdom, lessons of his school, and instructions of his example, which
are humility and love, than desiring preferment in the world, and
disputing about it. This ill temper he took all occasions to check, both
because it arose from a mistaken notion of his kingdom, as if it were of
this world, and because it tended so directly to be debasing of the
honour, and the corrupting of the purity, of his gospel, and, he
foresaw, would be so much the bane of the church.

Now, `(1.)` They were willing to cover this fault (v. 34); they held their
peace. As they would not ask (v. 32), because they were ashamed to own
their ignorance, so here they would not answer because they were ashamed
to own their pride. `(2.)` He was willing to amend this fault in them, and
to bring them to a better temper; and therefore sat down, that he might
have a solemn and full discourse with them about this matter; he called
the twelve to him, and told them, `[1.]` That ambition and affectation
of dignity and dominion, instead of gaining them preferment in his
kingdom, would but postpone their preferment; If any man desire and aim
to be first, he shall be last; he that exalteth himself, shall be
abased, and men\'s pride shall bring them low. `[2.]` That there is no
preferment to be had under him, but an opportunity for, and an
obligation to, so much the more labour and condescension; If any man
desire to be first, when he is so, he must be much the more busy and
serviceable to every body. He that desires the office of a bishop,
desires a good work, for he must, as St. Paul did, labour the more
abundantly, and make himself the servant of all. `[3.]` That those who
are most humble and self-denying, do most resemble Christ, and shall be
most tenderly owned by him. This he taught them by a sign; He took a
child in his arms, that had nothing of pride and ambition in it. \"Look
you,\" saith he; \"whosoever shall receive one like this child, receives
me. Those of a humble, meek, mild disposition are such as I will own and
countenance, and encourage every body else to do so too, and will take
what is done to them as done to myself; and so will my Father too, for
he who thus receiveth me, receiveth him that sent me, and it shall be
placed to his account, and repaid with interest.\"

`III.` He rebukes them for vilifying all but themselves; while they are
striving which of them should be greatest, they will not allow those who
are not in communion with them to be any thing. Observe,

`1.` The account which John gave him, of the restraint they had laid upon
one from making use of the name of Christ, because he was not of their
society. Though they were ashamed to own their contests for preferment,
they seem to boast of this exercise of their authority, and expected
their Master would not only justify them in it, but commend them for it;
and hoped he would not blame them for desiring to be great, when they
would thus use their power for maintaining the honour of the sacred
college. Master, saith John, we saw one casting out devils in thy name,
but he followeth not us, v. 38. `(1.)` It was strange that the one who was
not a professed disciple and follower of Christ, should yet have power
to cast out devils, in his name, for that seemed to be peculiar to those
whom he called, ch. 6:7. But some think that he was a disciple of John,
who made use of the name of the Messiah, not as come, but as near at
hand, not knowing that Jesus was he. It should rather seem that he made
use of the name of Jesus, believing him to be the Christ, as the other
disciples did. And why not he receive that power from Christ, whose
Spirit, like the wind, blows where it listeth, without such an outward
call as the apostles had? And perhaps there were many more such.
Christ\'s grace is not tied to the visible church. `(2.)` It was strange
that one who cast out devils in the name of Christ, did not join himself
to the apostles, and follow Christ with them, but should continue to act
in separation from them. I know of nothing that could hinder him from
following them, unless because he was loth to leave all to follow them;
and if so, that was an ill principle. The thing did not look well, and
therefore the disciples forbade him to make use of Christ\'s name as
they did, unless he would follow him as they did. This was like the
motion Joshua made concerning Eldad and Medad, that prophesied in the
camp, and went not up with the rest to the door of the tabernacle; \"My
lord Moses, forbid them (Num. 11:28); restrain them, silence them, for
it is a schism.\" Thus apt are we to imagine that those do not follow
Christ at all, who do not follow him with us, and that those do nothing
well, who do not just as we do. But the Lord knows them that are his,
however they are dispersed; and this instance gives us a needful
caution, to take heed lest we be carried, by an excess of zeal for the
unity of the church, and for that which we are sure is right and good,
to oppose that which yet may tend to the enlargement of the church, and
the advancement of its true interests another way.

`2.` The rebuke he gave to them for this (v. 39); Jesus said, \"Forbid
him not, nor any other that does likewise.\" This was like the check
Moses gave to Joshua; Enviest thou for my sake? Note, That which is
good, and doeth good, must not be prohibited, though there be some
defect or irregularity in the manner of doing it. Casting out devils,
and so destroying Satan\'s kingdom, doing this in Christ\'s name, and so
owning him to be sent of God, and giving honour to him as the Fountain
of grace, preaching down sin, and preaching up Christ, are good things,
very good things, which ought not to be forbidden to any, merely because
they follow not with us. If Christ be preached, Paul therein doth, and
will rejoice, though he be eclipsed by it, Phil. 1:18. Two reasons
Christ gives why such should not be forbidden. `(1.)` Because we cannot
suppose that any man who makes use of Christ\'s name in working
miracles, should blaspheme his name, as the scribes and Pharisees did.
There were those indeed that did in Christ\'s name cast out devils, and
yet in other respects were workers of iniquity; but they did not speak
evil of Christ. `(2.)` Because those that differed in communion, while
they agreed to fight against Satan under the banner of Christ, ought to
look upon one another as on the same side, notwithstanding that
difference. He that is not against us is on our part. As to the great
controversy between Christ an Beelzebub, he had said, He that is not
with me is against me, Mt. 12:30. He that will not own Christ, owns
Satan. But as to those that own Christ, though not in the same
circumstances, that follow him, though not with us, we must reckon that
though these differ from us, they are not against us, and therefore are
on our part, and we must not be any hindrance to their usefulness.

### Verses 41-50

Here, `I.` Christ promiseth a reward to all those that are any way kind to
his disciples (v. 41); \"Whosoever shall give you a cup of water, when
you need it, and will be a refreshment to you, because ye belong to
Christ, and are of his family, he shall not lose his reward.\" Note, 1.
It is the honour and happiness of Christians, that they belong to
Christ, they have joined themselves to him, and are owned by him; they
wear his livery and retainers to his family; nay, they are more nearly
related, they are members of his body. 2. They who belong to Christ, may
sometimes be reduced to such straits as to be glad of a cup of cold
water. 3. The relieving of Christ\'s poor in their distresses, is a good
deed, and will turn a good account; he accepts it, and will reward it.
4. What kindness is done to Christ\'s poor, must be done them for his
sake, and because they belong to him; for that is it that sanctifies the
kindness, and puts a value upon it in the sight of God. 5. This is a
reason why we must not discountenance and discourage those who are
serving the interests of Christ\'s kingdom, though they are not in every
thing of our mind and way. It comes in here as a reason why those must
not be hindered, that cast out devils in Christ\'s name, though they did
not follow him; for (as Dr. Hammond paraphrases it) \"It is not only the
great eminent performances which are done by you my constant attendants
and disciples, that are accepted by me, but every the least degree of
sincere faith and Christian performance, proportionable but to the
expressing the least kindness, as giving a cup of water to a disciple of
mine for being such, shall be accepted and rewarded.\" If Christ reckons
kindness to us services to him, we ought to reckon services to him
kindnesses to us, and to encourage them, though done by those that
follow not with us.

`II.` He threatens those that offend his little ones, that wilfully are
the occasion of sin or trouble to them, v. 42. Whosoever shall grieve
any true Christians, though they be of the weakest, shall oppose their
entrance into the ways of God, or discourage and obstruct their progress
in those ways, shall either restrain them from doing good, or draw them
in to commit sin, it were better for him that a millstone were hanged
about his neck, and he were cast into the sea: his punishment will be
very great, and the death and ruin of his soul more terrible than such a
death and ruin of his body would be. See Mt. 18:6.

`III.` He warns all his followers to take heed of ruining their own
souls. This charity must begin at home; if we must take heed of doing
any thing to hinder others from good, and to occasion their sin, much
more careful must we be to avoid every thing that will take us off from
our duty, or lead us to sin; and that which doth so we must part with,
though it be ever so dear to us. This we had twice in Matthew, ch. 5:29,
30, and ch. 18:8, 9. It is here urged somewhat more largely and
pressingly; certainly this requires our serious regard, which is so much
insisted upon. Observe,

`1.` The case supposed, that our own hand, or eye, or foot, offend us;
that the impure corruption we indulge is as dear to us as an eye or a
hand, or that that which is to us as an eye or a hand, is become an
invisible temptation to sin, or occasion of it. Suppose the beloved is
become a sin, or the sin a beloved. Suppose we cannot keep that which is
dear to us, but it will be a snare and a stumbling-block; suppose we
must part with it, or part with Christ and a good conscience.

`2.` The duty prescribed in that case; Pluck out the eye, cut off the
hand and foot, mortify the darling lust, kill it, crucify it, starve it,
make no provision for it. Let the idols that have been delectable
things, be cast away as detestable things; keep at a distance from that
which is a temptation, though ever so pleasing. It is necessary that the
part which is gangrened, should be taken off for the preservation of the
whole. Immedicabile vulnus ense recidendum est, ne pars sincera
trahatur-The part that is incurably wounded must be cut off, lest the
parts that are sound be corrupted. We must put ourselves to pain, that
we may not bring ourselves to ruin; self must be denied, that it may not
be destroyed.

`3.` The necessity of doing this. The flesh must be mortified, that we
may enter into life (v. 43, 45), into the kingdom of God, v. 47. Though,
by abandoning sin, we may, for the present, feel ourselves as if we were
halt and maimed (it may seem to be a force put upon ourselves, and may
create us some uneasiness), yet it is for life; and all that men have,
they will give for their lives: it is for a kingdom, the kingdom of God,
which we cannot otherwise obtain; these halts and maims will be the
marks of the Lord Jesus, will be in that kingdom scars of honour.

`4.` The danger of not doing this. The matter is brought to this issue,
that either sin must die, or we must die. If we will lay this Delilah in
our bosom, it will betray us; if we be ruled by sin, we shall inevitably
be ruined by it; if we must keep our two hands, and two eyes, and two
feet, we must with them be cast into hell. Our Saviour often pressed our
duty upon us, from the consideration of the torments of hell, which we
run ourselves into if we continue in sin. With what an emphasis of
terror are those words repeated three times here, Where their worm dieth
not, and the fire is not quenched! The words are quoted from Isa. 66:24.
`(1.)` The reflections and reproaches of the sinner\'s own conscience are
the worm that dieth not; which will cleave to the damned soul as the
worms do to the dead body, and prey upon it, and never leave it till it
is quite devoured. Son, remember, will set this worm gnawing; and how
terrible will it bite that word (Prov. 5:12, 23), How have I hated
instruction! The soul that is food to this worm, dies not; and the worm
is bred in it, and one with it, and therefore neither doth that die.
Damned sinners will be to eternity accusing, condemning, and upbraiding,
themselves with their own follies, which, how much soever they are now
in love with them, will at the last bite like a serpent, and sting like
an adder. `(2.)` The wrath of God fastening upon a guilty and polluted
conscience, is the fire that is not quenched; for it is the wrath of the
living God, the eternal God, into whose hands it is a fearful thing to
fall. There are no operations of the Spirit of grace upon the souls of
the damned sinners, and therefore there is nothing to alter the nature
of the fuel, which must remain for ever combustible; nor is there any
application of the merit of Christ to them, and therefore there is
nothing to appease or quench the violence of the fire. Dr. Whitby shows
that the eternity of the torments of hell was not only the constant
faith of the Christian church, but had been so of the Jewish church.
Josephus saith, The Pharisees held that the souls of the wicked were to
be punished with perpetual punishment; and that there was appointed for
them a perpetual prison. And Philo saith, The punishment of the wicked
is to live for ever dying, and to be for ever in pains and griefs that
never cease.

The two last verses are somewhat difficult, and interpreters agree not
in the sense of them; for every one in general, or rather every one of
them that are cast into hell, shall be salted with fire, and every
sacrifice shall be salted with salt. Therefore have salt in yourselves.
`[1.]` It was appointed by the law of Moses, that every sacrifice should
be salted with salt, not to preserve it (for it was to be immediately
consumed), but because it was the food of God\'s table, and no flesh is
eaten without salt; it was therefore particularly required in the
meat-offerings, Lev. 2:13. `[2.]` The nature of man, being corrupt, and
as such being called flesh (Gen. 6:3; Ps. 78:39), some way or other must
be salted, in order to its being a sacrifice to God. The salting of fish
(and I think of other things) they call the curing of it. `[3.]` Our
chief concern is, to present ourselves living sacrifices to the grace of
God (Rom. 12:1), and, in order to our acceptableness, we must be salted
with salt, our corrupt affections must be subdued and mortified, and we
must have in our souls a savour of grace. Thus the offering up or
sacrificing of the Gentiles is said to be acceptable, being sanctified
by the Holy Ghost, as the sacrifices were salted, Rom. 15:16. `[4.]`
Those that have the salt of grace, must make it appear that they have
it; that they have salt in themselves, a living principle of grace in
their hearts, which works out all corrupt dispositions, and every thing
in the soul that tends to putrefaction, and would offend our God, or our
own consciences, as unsavoury meat doth. Our speech must be always with
grace seasoned with this salt, that no corrupt communication may proceed
out of our mouth, but we may loathe it as much as we would to put putrid
meat into our mouths. `[5.]` As this gracious salt will keep our own
consciences void of offence, so it will keep our conversation with
others so, that we may not offend any of Christ\'s little ones, but may
be at peace one with another. `[6.]` We must not only have this salt of
grace, but we must always retain the relish and savour of it; for if
this salt lose its saltiness, if a Christian revolt from his
Christianity, if he loses the savour of it, and be no longer under the
power and influence of it, what can recover him, or wherewith will ye
season him? This was said Mt. 5:13. `[7.]` Those that present not
themselves living sacrifices to God\'s grace, shall be made for ever
dying sacrifices to his justice, and since they would not give honour to
him, he will get him honour upon them; they would not be salted with the
salt of divine grace, would not admit that to subdue their corrupt
affections, no, they would not submit to the operation, could not bear
the corrosives that were necessary to eat out the proud flesh, it was to
them like cutting off a hand, or plucking out an eye; and therefore in
hell they shall be salted with fire; coals of fire shall be scattered
upon them (Eze. 10:2), as salt upon the meat, and brimstone (Job 18:15),
as fire and brimstone were rained on Sodom; the pleasures they have
lived in, shall eat their flesh, as it were with fire, Jam. 5:3. The
pain of mortifying the flesh now is no more to be compared with the
punishment for not mortifying it, than salting with burning. And since
he had said, that the fire of hell shall not be quenched, but it might
be objected, that the fuel will not last always, he here intimates, that
by the power of God it shall be made to last always; for those that are
cast into hell, will find the fire to have not only the corroding
quality of salt, but its preserving quality; whence it is used to
signify that which is lasting: a covenant of salt is a perpetual
covenant, and Lot\'s wife being turned into a pillar of salt, made her a
remaining monument of divine vengeance. Now since this will certainly be
the doom of those that do not crucify the flesh with its affections and
lusts, let us, knowing this terror of the Lord, be persuaded to do it.
