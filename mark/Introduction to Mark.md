Introduction to Mark
====================

We have heard the evidence given in by the first witness to the doctrine
and miracles of our Lord Jesus; and now here is another witness
produced, who calls for our attention. The second living creature saith,
Come, and see, Rev. 6:3. Now let us enquire a little,

`I.` Concerning this witness. His name is Mark. Marcus was a Roman name,
and a very common one, and yet we have no reason to think, but that he
was by birth a Jew; but as Saul, when he went among the nations, took
the Roman name of Paul, so he of Mark, his Jewish name perhaps being
Mardocai; so Grotius. We read of John whose surname was Mark, sister\'s
son to Barnabas, whom Paul was displeased with (Acts 15:37, 38), but
afterward had a great kindness for, and not only ordered the churches to
receive him (Col. 4:10), but sent for him to be his assistant, with this
encomium, He is profitable to me for the ministry (2 Tim. 4:11); and he
reckons him among his fellow-labourers, Philemon 24. We read of Marcus
whom Peter calls his son, he having been an instrument of his conversion
(1 Pt. 5:13); whether that was the same with the other, and, if not,
which of them was the penman of this gospel, is altogether uncertain. It
is a tradition very current among the ancients, that St. Mark wrote this
gospel under the direction of St. Peter, and that it was confirmed by
his authority; so Hieron. Catal. Script. Eccles. Marcus discipulus et
interpres Petri, juxta quod Petrum referentem audierat, legatus Roma à
fratribus, breve scripsit evangelium-Mark, the disciple and interpreter
of Peter, being sent from Rome by the brethren, wrote a concise gospel;
and Tertullian saith (Adv. Marcion. lib. 4, cap. 5), Marcus quod edidit,
Petri affirmetur, cujus interpres Marcus-Mark, the interpreter of Peter,
delivered in writing the things which had been preached by Peter. But as
Dr. Whitby very well suggests, Why should we have recourse to the
authority of Peter for the support of this gospel, or say with St.
Jerome that Peter approved of it and recommended it by his authority to
the church to be read, when, though it is true Mark was no apostle, yet
we have all the reason in the world to think that both he and Luke were
of the number of the seventy disciples, who companied with the apostles
all along (Acts 1:21), who had a commission like that of the apostles
(Lu. 10:19, compared with Mk. 16:18), and who, it is highly probable,
received the Holy Ghost when they did (Acts 1:15; 2:1-4), so that it is
no diminution at all to the validity or value of this gospel, that Mark
was not one of the twelve, as Matthew and John were? St. Jerome saith
that, after the writing of this gospel, he went into Egypt, and was the
first that preached the gospel at Alexandria, where he founded a church,
to which he was a great example of holy living. Constituit ecclesiam
tantâ doctrinâ et vitae continentiâ ut omnes sectatores Christi ad
exemplum sui cogeret-He so adorned, by his doctrine and his life, the
church which he founded, that his example influenced all the followers
of Christ.

`II.` Concerning this testimony. Mark\'s gospel, 1. Is but short, much
shorter than Matthew\'s, not giving so full an account of Christ\'s
sermons as that did, but insisting chiefly on his miracles. 2. It is
very much a repetition of what we had in Matthew; many remarkable
circumstances being added to the stories there related, but not many new
matters. When many witnesses are called to prove the same fact, upon
which a judgment is to be given, it is not thought tedious, but highly
necessary, that they should each of them relate it in their own words,
again and again, that by the agreement of the testimony the thing may be
established; and therefore we must not think this book of scripture
needless, for it is written not only to confirm our belief that Jesus is
the Christ the Son of God, but to put us in mind of things which we have
read in the foregoing gospel, that we may give the more earnest heed to
them, lest at any time we let them slip; and even pure minds have need
to be thus stirred up by way of remembrance. It was fit that such great
things as these should be spoken and written, once, yea twice, because
man is so unapt to perceive them, and so apt to forget them. There is no
ground for the tradition, that this gospel was written first in Latin,
though it was written at Rome; it was written in Greek, as was St.
Paul\'s epistle to the Romans, the Greek being the more universal
language.
