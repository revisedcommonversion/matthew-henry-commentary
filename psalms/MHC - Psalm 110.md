Psalm 110
=========

Commentary
----------

This psalm is pure gospel; it is only, and wholly, concerning Christ,
the Messiah promised to the fathers and expected by them. It is plain
that the Jews of old, even the worst of them, so understood it, however
the modern Jews have endeavoured to pervert it and to rob us of it; for
when the Lord Jesus proposed a question to the Pharisees upon the first
words of this psalm, where he takes it for granted that David, in
spirit, calls Christ his Lord though he was his Son, they chose rather
to say nothing, and to own themselves gravelled, than to make it a
question whether David does indeed speak of the Messiah or no; for they
freely yield so plain a truth, though they foresee it will turn to their
own disgrace, Mt. 22:41, etc. Of him therefore, no doubt, the prophet
here speaks of him and of no other man. Christ, as our Redeemer,
executes the office of a prophet, of a priest, and of a king, with
reference both to his humiliation and his exaltation; and of each of
these we have here an account. `I.` His prophetical office (v. 2). `II.` His
priestly office (v. 4). `III.` His kingly office (v. 1, 3, 5, 6). `IV.` His
estates of humiliation and exaltation (v. 7). In singing this psalm we
must act faith upon Christ, submit ourselves entirely to him, to his
grace and government, and triumph in him as our prophet, priest, and
king, by whom we hope to be ruled, and taught, and saved, for ever, and
as the prophet, priest, and king, of the whole church, who shall reign
till he has put down all opposing rule, principality, and power, and
delivered up the kingdom to God the Father.

A psalm of David.

### Verses 1-4

Some have called this psalm David\'s creed, almost all the articles of
the Christian faith being found in it; the title calls it David\'s
psalm, for in the believing foresight of the Messiah he both praised God
and solaced himself, much more may we, in singing it, to whom that is
fulfilled, and therefore more clearly revealed, which is here foretold.
Glorious things are here spoken of Christ, and such as oblige us to
consider how great he is.

`I.` That he is David\'s Lord. We must take special notice of this because
he himself does. Mt. 22:43, David, in spirit, calls him Lord. And as the
apostle proves the dignity of Melchizedek, and in him of Christ, by
this, that so great a man as Abraham was paid him tithes (Heb. 7:4), so
we may be this prove the dignity of the Lord Jesus that David, that
great man, called him his Lord; by him that king acknowledges himself to
reign, and to him to be acceptable as a servant to his lord. Some think
he calls him his Lord because he was the Lord that was to descend from
him, his son and yet his Lord. Thus him immediate mother calls him her
Saviour (Lu. 1:47); even his parents were his subjects, his saved ones.

`II.` That he is constituted a sovereign Lord by the counsel and decree
of God himself: The Lord, Jehovah, said unto him, Sit as a king. He
receives of the Father this honour and glory (2 Pt. 1:17), from him who
is the fountain of honour and power, and takes it not to himself. He is
therefore rightful Lord, and his title is incontestable; for what God
has said cannot be gainsaid. He is therefore everlasting Lord; for what
God has said shall not be unsaid. He will certainly take and keep
possession of that kingdom which the Father has committed to him, and
none can hinder.

`III.` That he was to be advanced to the highest honour, and entrusted
with an absolute sovereign power both in heaven and in earth: Sit thou
at my right hand. Sitting is a resting posture; after his services and
sufferings, he entered into rest from all his labours. It is a ruling
posture; he sits to give law, to give judgment. It is a remaining
posture; he sits like a king for ever. Sitting at the right hand of God
denotes both his dignity and his dominion, the honour put upon him and
the trusts reposed in him by the Father. All the favours that come from
God to man, and all the service that comes from man to God, pass through
his hand.

`IV.` That all his enemies were in due time to be made his footstool, and
not till then; but then also he must reign in the glory of the Mediator,
though the work of the Mediator will be, in a manner, at an end. Note,
`1.` Even Christ himself has enemies that fight against his kingdom and
subjects, his honour and interest, in the world. There are those that
will not have him to reign over them, and thereby they join themselves
to Satan, who will not have him to reign at all. 2. These enemies will
be made his footstool; he will subdue them and triumph over them; he
will do it easily, as easily as we put a footstool in its proper place,
and such a propriety there will be in it. He will make himself easy by
the doing of it, as a man that sits with a footstool under his feet; he
will subdue them in such a way as shall be most for his honour and their
perpetual disgrace; he will tread down the wicked, Mal. 4:3. 3. God the
Father has undertaken to do it: I will make them thy footstool, who can
do it. 4. It will not be done immediately. All his enemies are now in a
chain, but not yet made his footstool. This the apostle observes. Heb.
2:8, We see not yet all things put under him. Christ himself must wait
for the completing of his victories and triumphs. 5. He shall wait till
it is done; and all their might and malice shall not give the least
disturbance to his government. His sitting at God\'s right hand is a
pledge to him of his setting his feet, at last, on the necks of all his
enemies.

`V.` That he should have a kingdom set up in the world, beginning at
Jerusalem (v. 2): \"The Lord shall send the rod or sceptre of thy
strength out of Zion, by which thy kingdom shall be erected, maintained,
and administered.\" The Messiah, when he sits on the right hand of the
Majesty in the heavens, will have a church on earth, and will have an
eye to it; for he is King upon the holy hill of Zion (Ps. 2:6), in
opposition to Mount Sinai, that frightful mountain, on which the law was
given, Heb. 12:18, 24; Gal. 4:24, 25. The kingdom of Christ took rise
from Zion, the city of David, for he was the Son of David, and was to
have the throne of his father David. By the rod of his strength, or his
strong rod, is meant his everlasting gospel, and the power of the Holy
Ghost going along with it-the report of the word, and the arm of the
Lord accompanying it (Isa. 53:1; Rom. 1:16),-the gospel coming in word,
and in power, and in the holy Ghost, 1 Th. 1:5. By the word and Spirit
of God souls were to be reduced first, and brought into obedience to
God, and then ruled and governed according to the will of God. This
strong rod God sent forth; he poured out the Spirit, and gave both
commissions and qualifications to those that preached the word, and
ministered the Spirit, Gal. 3:5. It was sent out of Zion, for there the
Spirit was given, and there the preaching of the gospel among all
nations must begin, at Jerusalem. See Lu. 24:47, 49. Out of Zion must go
forth the law of faith, Isa. 2:3. Note, The gospel of Christ, being sent
of God, is mighty through God to do wonders, 2 Co. 10:4. It is the rod
of Christ\'s strength. Some make it to allude not only to the sceptre of
a prince, denoting the glory of Christ shining in the gospel, but to a
shepherd\'s crook, his rod and staff, denoting the tender care of Christ
takes of his church; for he is both the great and the good Shepherd.

`VI.` That his kingdom, being set up, should be maintained and kept up in
the world, in spite of all the oppositions of the power of darkness. 1.
Christ shall rule, shall give laws, and govern his subjects by them,
shall perfect them, and make them easy and happy, shall do his own will,
fulfil his own counsels, and maintain his own interests among men. His
kingdom is of God, and it shall stand; his crown sits firmly on his
head, and there it shall flourish. 2. He shall rule in the midst of his
enemies. He sits in heaven in the midst of his friends; his throne of
glory there is surrounded with none but faithful worshippers of him,
Rev. 5:11. But he rules on earth in the midst of his enemies, and his
throne of government here is surrounded with those that hate him and
fight against him. Christ\'s church is a lily among thorns, and his
disciples are sent forth as sheep in the midst of wolves; he knows where
they dwell, even where Satan\'s seat is (Rev. 2:13), and this redounds
to his honour that he not only keeps his ground, but gains his point,
notwithstanding all the malignant policies and powers of hell and earth,
which cannot shake the rock on which the church is built. Great is the
truth, and will prevail.

`VII.` That he should have a great number of subjects, who should be to
him for a name and a praise, v. 3.

`1.` That they should be his own people, and such as he should have an
incontestable title to. They are given to him by the Father, who gave
them their lives and beings, and to whom their lives and beings were
forfeited. Thine they were and thou gavest them me, Jn. 17:6. They are
redeemed by him; he has purchased them to be to himself a peculiar
people, Tit. 2:14. They are his by right, antecedent to their consent.
He had much people in Corinth before they were converted, Acts 18:10.

`2.` That they should be a willing people, a people of willingness,
alluding to servants that choose their service and are not coerced to it
(they love their masters and would not go out free), to soldiers that
are volunteers and not pressed men (\"Here am I, send me\"), to
sacrifices that are free-will offerings and not offered of necessity; we
present ourselves living sacrifices. Note, Christ\'s people are a
willing people. The conversion of a soul consists in its being willing
to be Christ\'s, coming under his yoke and into his interests, with an
entire compliancy and satisfaction.

`3.` That they should be so in the day of his power, in the day of thy
muster (so some); when thou art enlisting soldiers thou shalt find a
multitude of volunteers forward to be enlisted; let but the standard be
set up and the Gentiles will seek to it, Isa. 11:10; 60:3. Or when thou
art drawing them out to battle they shall be willing to follow the Lamb
whithersoever he goes, Rev. 14:4. In the day of thy armies (so some);
\"when the first preachers of the gospel shall be sent forth, as
Christ\'s armies, to reduce apostate men, and to ruin the kingdom of
apostate angels, then all that are thy people shall be willing; that
will be thy time of setting up thy kingdom.\" In the day of thy
strength, so we take it. There is a general power which goes along with
the gospel to all, proper to make them willing to be Christ\'s people,
arising from the supreme authority of its great author and the intrinsic
excellency of the things themselves contained in it, besides the
undeniable miracles that were wrought for the confirmation of it. And
there is also a particular power, the power of the Spirit, going along
with the power of the word, to the people of Christ, which is effectual
to make them willing. The former leaves sinners without matter of
excuse; this leaves saints without matter of boasting. Whoever are
willing to be Christ\'s people, it is the free and mighty grace of God
that makes them so.

`4.` That they should be so in the beauty of holiness, that is, `(1.)` They
shall be allured to him by the beauty of holiness; they shall be charmed
into a subjection to Christ by the sight given them of his beauty, who
is the holy Jesus, and the beauty of the church, which is the holy
nation. `(2.)` They shall be admitted by him into the beauty of holiness,
as spiritual priests, to minister in his sanctuary; for by the blood of
Jesus we have boldness to enter into the holiest. `(3.)` They shall attend
upon him in the beautiful attire or ornaments of grace and
sanctification. Note, Holiness is the livery of Christ\'s family and
that which becomes his house for ever. Christ\'s soldiers are all thus
clothed; these are the colours they wear. The armies of heaven follow
him in fine linen, clean and white, Rev. 19:14.

`5.` That he should have great numbers of people devoted to him. The
multitude of the people is the honour of the prince, and that shall be
the honour of this prince. From the womb of the morning thou hast the
dew of thy youth, that is, abundance of young converts, like the drops
of dew in a summer\'s morning. In the early days of the gospel, in the
morning of the New Testament, the youth of the church, great numbers
flocked to Christ, and there were multitudes that believed, a remnant of
Jacob, that was as dew from the Lord, Mic. 5:7; Isa. 64:4, 8. Or thus?
\"From the womb of the morning (from their very childhood) thou hast the
dew of thy people\'s youth, that is, their hearts and affections when
they are young; it is thy youth, because it is dedicated to thee.\" The
dew of the youth is a numerous, illustrious, hopeful show of young
people flocking to Christ, which would be to the world as dew to the
ground, to make it fruitful. Note, The dew of our youth, even in the
morning of our days, ought to be consecrated to our Lord Jesus.

`6.` That he should be not only a king, but a priest, v. 4. The same Lord
that said, Sit thou at my right hand, swore, and will not repent, Thou
art a priest, that is, Be thou a priest; for by the word of his oath he
was consecrated. Note, `(1.)` Our Lord Jesus Christ is a priest. He was
appointed to that office and faithfully executes it; he is ordained for
men in things pertaining to God, to offer gifts and sacrifices for sin
(Heb. 5:1), to make atonement for our sins and to recommend our services
to God\'s acceptance. He is God\'s minister to us, and our advocate with
God, and so is a Mediator between us and God. `(2.)` He is a priest for
ever. He was designed for a priest, in God\'s eternal counsels; he was a
priest to the Old-Testament saints, and will be a priest for all
believers to the end of time, Heb. 13:8. He is said to be a priest for
ever, not only because we are never to expect any other dispensation of
grace than this by the priesthood of Christ, but because the blessed
fruits and consequences of it will remain to eternity. `(3.)` He is made a
priest with an oath, which the apostle urges to prove the pre-eminence
of his priesthood above that of Aaron, Heb. 7:20, 21. The Lord has
sworn, to show that in the commission there was no implied reserve of a
power of revocation; for he will not repent, as he did concerning Eli\'s
priesthood, 1 Sa. 2:30. This was intended for the honour of Christ and
the comfort of Christians. The priesthood of Christ is confirmed by the
highest ratifications possible, that it might be an unshaken foundation
for our faith and hope to build upon. `(4.)` He is a priest, not of the
order of Aaron, but of that of Melchizedek, which, as it was prior, so
it was upon many accounts superior, to that of Aaron, and a more lively
representation of Christ\'s priesthood. Melchizedek was a priest upon
his throne, so is Christ (Zec. 6:13), king of righteousness and king of
peace. Melchizedek had no successor, nor has Christ; his is an
unchangeable priesthood. The apostle comments largely upon these words
(Heb. 7) and builds on them his discourse of Christ\'s priestly office,
which he shows was no new notion, but built upon this most sure word of
prophecy. For, as the New Testament explains the Old, so the Old
Testament confirms the New, and Jesus Christ is the Alpha and Omega of
both.

### Verses 5-7

Here we have our great Redeemer,

`I.` Conquering his enemies (v. 5, 6) in order to the making of them his
footstool, v. 1. Our Lord Jesus will certainly bring to nought all the
opposition made to his kingdom, and bring to ruin all those who make
that opposition and persist in it. He will be too hard for those,
whoever they may be, that fight against him, against his subjects and
the interest of his kingdom among men, either by persecutions or by
perverse disputings. Observe here,

`1.` The conqueror: The Lord-Adonai, the Lord Jesus, he to whom all
judgment is committed, he shall make his own part good against his
enemies. The Lord at thy right hand, O church! so some; that is, the
Lord that is nigh unto his people, and a very present help to them, that
is at their right hand, to strengthen and succour them, shall appear for
them against his and their enemies. See Ps. 109:31. He shall stand at
the right hand of the poor, Ps. 16:8. Some observe that when Christ is
said to do his work at the right hand of his church it intimates that,
if we would have Christ to appear for us, we must bestir ourselves, 2
Sa. 5:24. Or, rather, At thy right hand, O God! referring to v. 1, in
the dignity and dominion to which he is advanced. Note, Christ\'s
sitting at the right hand of God speaks as much terror to his enemies as
happiness to his people.

`2.` The time fixed for this victory: In the day of his wrath, that is,
the time appointed for it, when the measure of their iniquities is full
and they are ripe for ruin. When the day of his patience has expired,
when the day of his wrath comes. Note, `(1.)` Christ has wrath of his own,
as well as grace. It concerns us to kiss the Son, for he can be angry
(Ps. 2:12) and we read of the wrath of the Lamb, Rev. 6:16. `(2.)` There
is a day of wrath set, a year of recompences for the controversy of
Zion, the year of the redeemed. The time is set for the destruction of
particular enemies, and when that time shall come it shall be done, how
unlikely soever it may seem; but the great day of his wrath will be at
the end of time, Rev. 6:17.

`3.` The extent of this victory. `(1.)` It shall reach very high: He shall
strike through kings. The greatest of men, that set themselves against
Christ, shall be made to fall before him. Though they be kings of the
earth, and rulers, accustomed to carry their point, they cannot carry it
against Christ, they do but make themselves ridiculous by the attempt,
Ps. 2:2-5. Be their power among men ever so despotic, Christ will call
them to an account; be their strength ever so great, their policies ever
so deep, Christ will be too hard for them, and wherein they deal proudly
he will be above them. Satan is the prince of this world, Death the king
of terrors, and we read of kings that make war with the Lamb; but they
shall all be brought down and broken. `(2.)` It shall reach very far. The
trophies of Christ\'s victories will be set up among the heathen, and in
many countries, wherever any of his enemies are, not his eye only, but
his hand, shall find them out (Ps. 21:8) and his wrath shall follow
them. He will plead with all nations, Joel 3:2.

`4.` The equity of this victory: He shall judge among them. It is not a
military execution, which is done in fury, but a judicial one. Before he
condemns and slays, he will judge; he will make it appear that they have
brought this ruin upon themselves, and have themselves rolled the stone
which returns upon them, that he may be justified when he speaks and the
heavens may declare his righteousness. See Rev. 19:1, 2.

`5.` The effect of this victory; it shall be the complete and utter ruin
of all his enemies. He shall strike them through, for he strikes home
and gives an incurable wound: He shall wound the heads, which seems to
refer to the first promise of the Messiah (Gen. 3:15), that he should
bruise the serpent\'s head. He shall wound the head of his enemies, Ps.
68:21. Some read it, He shall wound him that is the head over many
countries, either Satan or Antichrist, whom the Lord shall consume with
the breath of his mouth. He shall make such destruction of his enemies
that he shall fill the places with the dead bodies. The slain of the
Lord shall be many. See Isa. 34:3, etc.; Eze. 39:12, 14; Rev. 14:20;
19:17, 18. The filling of the valleys (for so some read it) with dead
bodies, perhaps denotes the filling of hell (which is sometimes compared
to the valley of Hinnom, Isa. 30:33; Jer. 7:32) with damned souls, for
that will be the portion of those that persist in their enmity to
Christ.

`II.` We have here the Redeemer saving his friends and comforting them
(v. 7); for their benefit, 1. He shall be humbled: He shall drink of the
brook in the way, that bitter cup which the Father put into his hand. He
shall be so abased and impoverished, and withal so intent upon his work,
that he shall drink puddle-water out of the lakes in the highway; so
some. The wrath of God, running in the channel of the curse of the law,
was the brook in the way, in the way of his undertaking, which must go
through, or which ran in the way of our salvation and obstructed it,
which lay between us and heaven. Christ drank of this brook when he was
made a curse for us, and therefore, when he entered upon his suffering,
he went over the brook Kidron, Jn. 18:1. He drank deeply of this black
brook (so Kidron signifies), this bloody brook, so drank of the brook in
the way as to take it out of the way of our redemption and salvation. 2.
He shall be exalted: Therefore shall he lift up the head. When he died
he bowed the head (Jn. 19:30), but he soon lifted up the head by his own
power in his resurrection. He lifted up the head as a conqueror, yea,
more than a conqueror. This denotes not only his exaltation, but his
exultation; not only his elevation, but his triumph in it. Col. 2:15,
Having spoiled principalities and powers, he made a show of them. David
spoke as a type of him in this (Ps. 27:6), Now shall my head be lifted
up above my enemies. His exaltation was the reward of his humiliation;
because he humbled himself, therefore God also highly exalted him, Phil.
2:9. Because he drank of the brook in the way therefore he lifted up his
own head, and so lifted up the heads of all his faithful followers, who,
if they suffer with him, shall also reign with him.
