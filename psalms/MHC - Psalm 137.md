Psalm 137
=========

Commentary
----------

There are divers psalms which are thought to have been penned in the
latter days of the Jewish church, when prophecy was near expiring and
the canon of the Old Testament ready to be closed up, but none of them
appears so plainly to be of a late date as this, which was penned when
the people of God were captives in Babylon, and there insulted over by
these proud oppressors; probably it was towards the latter end of their
captivity; for now they saw the destruction of Babylon hastening on
apace (v. 8), which would be their discharge. It is a mournful psalm, a
lamentation; and the Septuagint makes it one of the lamentations of
Jeremiah, naming him for the author of it. Here `I.` The melancholy
captives cannot enjoy themselves (v. 1, 2). `II.` They cannot humour their
proud oppressors (v. 3, 4). `III.` They cannot forget Jerusalem (v. 5, 6).
IV. They cannot forgive Edom and Babylon (v. 7-9). In singing this psalm
we must be much affected with the concernments of the church, especially
that part of it that is in affliction, laying the sorrows of God\'s
people near our hearts, comforting ourselves in the prospect of the
deliverance of the church and the ruin of its enemies, in due time, but
carefully avoiding all personal animosities, and not mixing the leaven
of malice with our sacrifices.

### Verses 1-6

We have here the daughter of Zion covered with a cloud, and dwelling
with the daughter of Babylon; the people of God in tears, but sowing in
tears. Observe,

`I.` The mournful posture they were in as to their affairs and as to their
spirits. 1. They were posted by the rivers of Babylon, in a strange
land, a great way from their own country, whence they were brought as
prisoners of war. The land of Babylon was now a house of bondage to that
people, as Egypt had been in their beginning. Their conquerors quartered
them by the rivers, with design to employ them there, and keep them to
work in their galleys; or perhaps they chose it as the most melancholy
place, and therefore most suitable to their sorrowful spirits. If they
must build houses there (Jer. 29:5), it shall not be in the cities, the
places of concourse, but by the rivers, the places of solitude, where
they might mingle their tears with the streams. We find some of them by
the river Chebar (Eze. 1:3), others by the river Ulai, Dan. 8:2. 2.
There they sat down to indulge their grief by poring on their miseries.
Jeremiah had taught them under this yoke to sit alone, and keep silence,
and put their mouths in the dust, Lam. 3:28, 29. \"We sat down, as those
that expected to stay, and were content, since it was the will of God
that it must be so.\" 3. Thoughts of Zion drew tears from their eyes;
and it was not a sudden passion of weeping, such as we are sometimes put
into by a trouble that surprises us, but they were deliberate tears (we
sat down and wept), tears with consideration-we wept when we remembered
Zion, the holy hill on which the temple was built. Their affection to
God\'s house swallowed up their concern for their own houses. They
remembered Zion\'s former glory and the satisfaction they had had in
Zion\'s courts, Lam. 1:7. Jerusalem remembered, in the days of her
misery, all her pleasant things which she had in the days of old, Ps.
42:4. They remembered Zion\'s present desolations, and favoured the dust
thereof, which was a good sign that the time for God to favour it was
not far off, Ps. 102:13, 14. 4. They laid by their instruments of music
(v. 2): We hung our harps upon the willows. `(1.)` The harps they used for
their own diversion and entertainment. These they laid aside, both
because it was their judgment that they ought not to use them now that
God called to weeping and mourning (Isa. 22:12), and their spirits were
so sad that they had no hearts to use them; they brought their harps
with them, designing perhaps to use them for the alleviating of their
grief, but it proved so great that it would not admit the experiment.
Music makes some people melancholy. As vinegar upon nitre, so is he that
sings songs to a heavy heart. `(2.)` The harps they used in God\'s
worship, the Levites\' harps. These they did not throw away, hoping they
might yet again have occasion to use them, but they laid them aside
because they had no present use for them; God had cut them out other
work by turning their feasting into mourning and their songs into
lamentations, Amos 8:10. Every thing is beautiful in its season. They
did not hide their harps in the bushes, or the hollows of the rocks; but
hung them up in view, that the sight of them might affect them with this
deplorable change. Yet perhaps they were faulty in doing this; for
praising God is never out of season; it is his will that we should in
every thing give thanks, Isa. 24:15, 16.

`II.` The abuses which their enemies put upon them when they were in this
melancholy condition, v. 3. They had carried them away captive from
their own land and then wasted them in the land of their captivity, took
what little they had from them. But this was not enough; to complete
their woes they insulted over them: They required of us mirth and a
song. Now, 1. This was very barbarous and inhuman; even an enemy, in
misery, is to be pitied and not trampled upon. It argues a base and
sordid spirit to upbraid those that are in distress either with their
former joys or with their present griefs, or to challenge those to be
merry who, we know, are out of tune for it. This is adding affliction to
the afflicted. 2. It was very profane and impious. No songs would serve
them but the songs of Zion, with which God had been honoured; so that in
this demand they reflected upon God himself as Belshazzar, when he drank
wine in temple-bowls. Their enemies mocked at their sabbaths, Lam. 1:7.

`III.` The patience wherewith they bore these abuses, v. 4. They had laid
by their harps, and would not resume them, no, not to ingratiate
themselves with those at whose mercy they lay; they would not answer
those fools according to their folly. Profane scoffers are not to be
humoured, nor pearls cast before swine. David prudently kept silence
even from good when the wicked were before him, who, he knew, would
ridicule what he said and make a jest of it, Ps. 39:1, 2. The reason
they gave is very mild and pious: How shall we sing the Lord\'s song in
a strange land? They do not say, \"How shall we sing when we are so much
in sorrow?\" If that had been all, they might perhaps have put a force
upon themselves so far as to oblige their masters with a song; but \"It
is the Lord\'s song; it is a sacred thing; it is peculiar to the
temple-service, and therefore we dare not sing it in the land of a
stranger, among idolaters.\" We must not serve common mirth, much less
profane mirth, with any thing that is appropriated to God, who is
sometimes to be honoured by a religious silence as well as by religious
speaking.

`IV.` The constant affection they retained for Jerusalem, the city of
their solemnities, even now that they were in Babylon. Though their
enemies banter them for talking so much of Jerusalem, and even doting
upon it, their love to it is not in the least abated; it is what they
may be jeered for, but will never be jeered out of, v. 5, 6. Observe,

`1.` How these pious captives stood affected to Jerusalem. `(1.)` Their
heads were full of it. It was always in their minds; they remembered it;
they did not forget it, though they had been long absent from it; many
of them had never seen it, nor knew any thing of it but by report, and
by what they had read in the scripture, yet it was graven upon the palms
of their hands, and even its ruins were continually before them, which
was ann evidence of their faith in the promise of its restoration in due
time. In their daily prayers they opened their windows towards
Jerusalem; and how then could they forget it? `(2.)` Their hearts were
full of it. They preferred it above their chief joy, and therefore they
remembered it and could not forget it. What we love we love to think of.
Those that rejoice in God do, for his sake, make Jerusalem their joy,
and prefer it before that, whatever it is, which is the head of their
joy, which is dearest to them in this world. A godly man will prefer a
public good before any private satisfaction or gratification whatsoever.

`2.` How stedfastly they resolved to keep up this affection, which they
express by a solemn imprecation of mischief to themselves if they should
let it fall: \"Let me be for ever disabled either to sing or play on the
harp if I so far forget the religion of my country as to make use of my
songs and harps for the pleasing of Babylon\'s sons or the praising of
Babylon\'s gods. Let my right hand forget her art\" (which the hand of
an expert musician never can, unless it be withered), \"nay, let my
tongue cleave to the roof of my mouth, if I have not a good word to say
for Jerusalem wherever I am.\" Though they dare not sing Zion\'s songs
among the Babylonians, yet they cannot forget them, but, as soon as ever
the present restraint is taken off, they will sing them as readily as
ever, notwithstanding the long disuse.

### Verses 7-9

The pious Jews in Babylon, having afflicted themselves with the thoughts
of the ruins of Jerusalem, here please themselves with the prospect of
the ruin of her impenitent implacable enemies; but this not from a
spirit of revenge, but from a holy zeal for the glory of God and the
honour of his kingdom.

`I.` The Edomites will certainly be reckoned with, and all others that
were accessaries to the destruction of Jerusalem, that were aiding and
abetting, that helped forward the affliction (Zec. 1:15) and triumphed
in it, that said, in the day of Jerusalem, the day of her judgment,
\"Rase it, rase it to the foundations; down with it, down with it; do
not leave one stone upon another.\" Thus they made the Chaldean army
more furious, who were already so enraged that they needed no spur. Thus
they put shame upon Israel, who would be looked upon as a people worthy
to be cut off when their next neighbours had such an ill-will to them.
And all this was a fruit of the old enmity of Esau against Jacob,
because he got the birthright and the blessing, and a branch of that
more ancient enmity between the seed of the woman and the seed of the
serpent: Lord, remember them, says the psalmist, which is an appeal to
his justice against them. Far be it from us to avenge ourselves, if ever
it should be in our power, but we will leave it to him who has said,
Vengeance is mine. Note, Those that are glad at calamities, especially
the calamities of Jerusalem, shall not go unpunished. Those that are
confederate with the persecutors of good people, and stir them up, and
set them on, and are pleased with what they do, shall certainly be
called to an account for it against another day, and God will remember
it against them.

`II.` Babylon is the principal, and it will come to her turn too to drink
of the cup of tremblings, the very dregs of it (v. 8, 9): O daughter of
Babylon! proud and secure as thou art, we know well, by the scriptures
of truth, thou art to be destroyed, or (as Dr. Hammond reads it) who art
the destroyer. The destroyers shall be destroyed, Rev. 13:10. And
perhaps it is with reference to this that the man of sin, the head of
the New-Testament Babylon, is called a son of perdition, 2 Th. 2:3. The
destruction of Babylon being foreseen as a sure destruction (thou art to
be destroyed), it is spoken of, 1. As a just destruction. She shall be
paid in her own coin: \"Thou shalt be served as thou hast served us, as
barbarously used by the destroyers as we have been by thee,\" See Rev.
18:6. Let not those expect to find mercy who, when they had power, did
not show mercy. 2. As an utter destruction. The very little ones of
Babylon, when it is taken by storm, and all in it are put to the sword,
shall be dashed to pieces by the enraged and merciless conqueror. None
escape if these little ones perish. Those are the seed of another
generation; so that, if they be cut off, the ruin will be not only
total, as Jerusalem\'s was, but final. It is sunk like a millstone into
the sea, never to rise. 3. As a destruction which should reflect honour
upon the instruments of it. Happy shall those be that do it; for they
are fulfilling God\'s counsels; and therefore he calls Cyrus, who did
it, his servant, his shepherd, his anointed (Isa. 44:28; 45:1), and the
soldiers that were employed in it his sanctified ones, Isa. 13:3. They
are making way for the enlargement of God\'s Israel, and happy are those
who are in any way serviceable to that. The fall of the New-Testament
Babylon will be the triumph of all the saints, Rev. 19:1.
