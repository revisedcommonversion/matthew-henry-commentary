Psalm 60
========

Commentary
----------

After many psalms which David penned in a day of distress this comes
which was calculated for a day of triumph; it was penned after he was
settled in the throne, upon occasion of an illustrious victory which God
blessed his forces with over the Syrians and Edomites; it was when David
was in the zenith of his prosperity, and the affairs of his kingdom seem
to have been in a better posture then ever they were either before or
after. See 2 Sa. 8:3, 13; 1 Chr. 18:3, 12. David, in prosperity, was as
devout as David in adversity. In this psalm, `I.` He reflects upon the bad
state of the public interests, for many years, in which God had been
contending with them (v. 1-3). `II.` He takes notice of the happy turn
lately given to their affairs (v. 4). `III.` He prays for the deliverance
of God\'s Israel from their enemies (v. 5). `IV.` He triumphs in hope of
their victories over their enemies, and begs of God to carry them on and
complete them (v. 6-12). In singing this psalm we may have an eye both
to the acts of the church and to the state of our own souls, both which
have their struggles.

To the chief musician upon Shushan-eduth, Michtam of David, to teach,
when he strove with Aram-naharaim, and with Aramzobah, when Joab
returned, and smote of Edom in the valley of salt 12,000.

### Verses 1-5

The title gives us an account, 1. Of the general design of the psalm. It
is Michtam-David\'s jewel, and it is to teach. The Levites must teach it
to the people, and by it teach them both to trust in God and to triumph
in him; we must, in it, teach ourselves and one another. In a day of
public rejoicing we have need to be taught to direct our joy to God and
to terminate it in him, to give none of that praise to the instruments
of our deliverance which is due to him only, and to encourage our hopes
with our joys. 2. Of the particular occasion of it. It was at a time,
`(1.)` When he was at war with the Syrians, and still had a conflict with
them, both those of Mesopotamia and those of Zobah. `(2.)` When he had
gained a great victory over the Edomites, by his forces, under the
command of Joab, who had left 12,000 of the enemy dead upon the spot.
David has an eye to both these concerns in this psalm: he is in care
about his strife with the Assyrians, and in reference to that he prays;
he is rejoicing in his success against the Edomites, and in reference to
that he triumphs with a holy confidence in God that he would complete
the victory. We have our cares at the same time that we have our joys,
and they may serve for a balance to each other, that neither may exceed.
They may likewise furnish us with matter both for prayer and praise, for
both must be laid before God with suitable affections and emotions. If
one point be gained, yet in another we are still striving: the Edomites
are vanquished, but the Syrians are not; therefore let not him that
girds on the harness boast as if he had put it off.

In these verses, which begin the psalm, we have,

`I.` A melancholy memorial of the many disgraces and disappointments which
God had, for some years past, put the people under. During the reign of
Saul, especially in the latter end of it, and during David\'s struggle
with the house of Saul, while he reigned over Judah only, the affairs of
the kingdom were much perplexed, and the neighbouring nations were
vexatious to them. 1. He complains of hard things which they had seen
(that is, which they had suffered), while the Philistines and other
ill-disposed neighbours took all advantages against them, v. 3. God
sometimes shows even his own people hard things in this world, that they
may not take up their rest in it, but may dwell at ease in him only. 2.
He owns God\'s displeasure to be the cause of all the hardships they had
undergone: \"Thou hast been displeased by us, displeased against us (v.
1), and in thy displeasure hast cast us off and scattered us, hast put
us out of thy protection, else our enemies could not have prevailed thus
against us. They would never have picked us up and made a prey of us if
thou hadst not broken the staff of bands (Zec. 11:14) by which we were
united, and so scattered us.\" Whatever our trouble is, and whoever are
the instruments of it, we must own the hand of God, his righteous hand,
in it. 3. He laments the ill effects and consequences of the
miscarriages of the late years. The whole nation was in a convulsion:
Thou hast made the earth (or the land) to tremble, v. 2. The generality
of the people had dreadful apprehensions of the issue of these things.
The good people themselves were in a consternation: \"Thou hast made us
to drink the wine of astonishment (v. 3); we were like men intoxicated,
and at our wits\' end, not knowing how to reconcile these dispensations
with God\'s promises and his relation to his people; we are amazed, can
do nothing, nor know we what to do.\" Now this is mentioned here to
teach, that is, for the instruction of the people. When God is turning
his hand in our favour, it is good to remember our former calamities,
`(1.)` That we may retain the good impressions they made upon us, and may
have them revived. Our souls must still have the affliction and the
misery in remembrance, that they may be humbled within us, Lam. 3:19,
20. `(2.)` That God\'s goodness to us, in relieving us and raising us up,
may be more magnified; for it is as life from the dead, so strange, so
refreshing. Our calamities serve as foils to our joys. `(3.)` That we may
not be secure, but may always rejoice with trembling, as those that know
not how soon we may be returned into the furnace again, which we were
lately taken out of as the silver is when it is not thoroughly refined.

`II.` A thankful notice of the encouragement God had given them to hope
that, though things had been long bad, they would now begin to mend (v.
4): \"Thou hast given a banner to those that fear thee (for, as bad as
the times are, there is a remnant among us that desire to fear thy name,
for whom thou hast a tender concern), that it may be displayed by thee,
because of the truth of thy promise which thou wilt perform, and to be
displayed by them, in defense of truth and equity,\" Ps. 45:4. This
banner was David\'s government, the establishment and enlargement of it
over all Israel. The pious Israelites, who feared God and had a regard
to the divine designation of David to the throne, took his elevation as
a token for good, and like the lifting up of a banner to them, 1. It
united them, as soldiers are gathered together to their colours. Those
that were scattered (v. 1), divided among themselves, and so weakened
and exposed, coalesced in him when he was fixed upon the throne. 2. It
animated them, and put life and courage into them, as the soldiers are
animated by the sight of their banner. 3. It struck a terror upon their
enemies, to whom they could now hang out a flag of defiance. Christ, the
Son of David, is given for an ensign of the people (Isa. 11:10), for a
banner to those that fear God; in him, as the centre of their unity,
they are gathered together in one; to him they seek, in him they glory
and take courage. His love is the banner over them; in his name and
strength they wage war with the powers of darkness, and under him the
church becomes terrible as an army with banners.

`III.` A humble petition for seasonable mercy. 1. That God would be
reconciled to them, though he had been displeased with them. In his
displeasure their calamities began, and therefore in his favour their
prosperity must begin: O turn thyself to us again! (v. 1) smile upon us,
and take part with us; be at peace with us, and in that peace we shall
have peace. Tranquillus Deus tranquillat omnia-A God at peace with us
spreads peace over all the scene. 2. That they might be reconciled to
one another, though they had been broken and wretchedly divided among
themselves: \"Heal the breaches of our land (v. 2), not only the
breaches made upon us by our enemies, but the breaches made among
ourselves by our unhappy divisions.\" Those are breaches which the folly
and corruption of man makes, and which nothing but the wisdom and grace
of God can make up and repair, by pouring out a spirit of love and
peace, by which only a shaken shattered kingdom is set to rights and
saved from ruin. 3. That thus they might be preserved out of the hands
of their enemies (v. 5): \"That thy beloved may be delivered, and not
made a prey of, save with thy right hand, with thy own power and by such
instruments as thou art pleased to make the men of thy right hand, and
hear me.\" Those that fear God are his beloved; they are dear to him as
the apple of his eye. They are often in distress, but they shall be
delivered. God\'s own right hand shall save them; for those that have
his heart have his hand. Save them, and hear me. Note, God\'s praying
people may take the general deliverances of the church as answers to
their payers in particular. If we improve what interest we have at the
throne of grace for blessings for the public, and those blessings be
bestowed, besides the share we have with others in the benefit of them
we may each of us say, with peculiar satisfaction, \"God has therein
heard me, and answered me.\"

### Verses 6-12

David is here rejoicing in hope and praying in hope; such are the
triumphs of the saints, not so much upon the account of what they have
in possession as of what they have in prospect (v. 6): \"God has spoken
in his holiness (that is, he has given me his word of promise, has sworn
by his holiness, and he will not lie unto David, Ps. 89:35), therefore I
will rejoice, and please myself with the hopes of the performance of the
promise, which was intended for more than a pleasing promise,\" Note,
God\'s word of promise, being a firm foundation of hope, is a full
fountain of joy to all believers.

`I.` David here rejoices; and it is in prospect of two things:-

`1.` The perfecting of this revolution in his own kingdom. God having
spoken in his holiness that David shall be king, he doubts not but the
kingdom is all his own, as sure as if it were already in his hand: I
will divide Shechem (a pleasant city in Mount Ephraim) and mete out the
valley of Succoth, as my own. Gilead is mine, and Manasseh is mine, and
both are entirely reduced, v. 7. Ephraim would furnish him with soldiers
for his life-guards and his standing forces; Judah would furnish him
with able judges for his courts of justice; and thus Ephraim would be
the strength of his head and Judah his lawgiver. Thus may an active
believer triumph in the promises, and take the comfort of all the good
contained in them; for they are all yea and amen in Christ. \"God has
spoken in his holiness, and then pardon is mine, peace mine, grace mine,
Christ mine, heaven mine, God himself mine.\" All is yours, for you are
Christ\'s, 1 Co. 3:22, 23.

`2.` The conquering of the neighbouring nations, which had been vexatious
to Israel, were still dangerous, and opposed the throne of David, v. 8.
Moab shall be enslaved, and put to the meanest drudgery. The Moabites
became David\'s servants, 2 Sa. 8:2. Edom shall be made a dunghill to
throw old shoes upon; at least David shall take possession of it as his
own, which was signified by drawing off his shoe over it, Ruth 4:7. As
for the Philistines, let them, if they dare, triumph over him as they
had done; he will soon force them to change their note. Rather let those
that know their own interest triumph because of him; for it would be the
greatest kindness imaginable to them to be brought into subjection to
David and communion with Israel. But the war is not yet brought to an
end; there is a strong city, Rabbah (perhaps) of the children of Ammon,
which yet holds out; Edom is not yet subdued. Now, `(1.)` David is here
enquiring for help to carry on the ark: \"Who will bring me into the
strong city? What allies, what auxiliaries, can I depend upon, to make
me master of the enemies\' country and their strongholds?\" Those that
have begun a good work cannot but desire to make a thorough work of it,
and to bring it to perfection. `(2.)` He is expecting it from God only:
\"Wilt not thou, O God? For thou hast spoken in thy holiness; and wilt
not thou be as good as thy word?\" He takes notice of the frowns of
Providence they had been under: Thou hadst, in appearance, cast us off;
thou didst not go forth with our armies. When they were defeated and met
with disappointments, they owned it was because they wanted (that is,
because they had forfeited) the gracious presence of God with them; yet
they do not therefore fly off from him, but rather take so much the
faster hold of him; and the less he has done for them of late the more
they hoped he would do. At the same time that they own God\'s justice in
what was past they hope in his mercy for what was to come: \"Though thou
hadst cast us off, yet thou wilt not contend for ever, thou wilt not
always chide; though thou hadst cast us off, yet thou hast begun to show
mercy; and wilt thou not perfect what thou hast begun?\" The Son of
David, in his sufferings, seemed to be cast off by his Father when he
cried out, Why hast thou forsaken me? and yet even then he obtained a
glorious victory over the powers of darkness and their strong city, a
victory which will undoubtedly be completed at last; for he has gone
forth conquering and to conquer. The Israel of God, his spiritual
Israel, are likewise, through him, more than conquerors. Though
sometimes they may be tempted to think that God has cast them off, and
may be foiled in particular conflicts, yet God will bring them into the
strong city at last. Vincimur in praelio, sed non in bello-We are foiled
in a battle, but not in the whole war. A lively faith in the promise
will assure us, not only that the God of peace shall tread Satan under
our feet shortly, but that it is our Father\'s good pleasure to give us
the kingdom.

`II.` He prays in hope. His prayer is, Give us help from trouble, v. 11.
Even in the day of their triumph they see themselves in trouble, because
still in war, which is troublesome even to the prevailing side. None
therefore can delight in war but those that love to fish in troubled
waters. The help from trouble they pray for is preservation from those
they were at war with. Though now they were conquerors, yet (so
uncertain are the issues of war), unless God gave them help in the next
engagement, they might be defeated; therefore, Lord, send us help from
the sanctuary. Help from trouble is rest from war, which they prayed
for, as those that contended for equity, not for victory. Sic quaerimus
pacem-Thus we seek for peace. The hope with which they support
themselves in this prayer has two things in it:-1. A diffidence of
themselves and all their creature-confidences: Vain is the help of man.
Then only we are qualified to receive help from God when we are brought
to own the insufficiency of all creatures to do that for us which we
expect him to do. 2. A confidence in God, and in his power and promise
(v. 12): \"Through God we shall do valiantly, and so we shall do
victoriously; for he it is, and he only, that shall tread down our
enemies, and shall have the praise of doing it.\" Note, `(1.)` Our
confidence in God must be so far from superseding that it must encourage
and quicken our endeavours in the way of our duty. Though it is God that
performs all things for us, yet there is something to be done by us.
`(2.)` Hope in God is the best principle of true courage. Those that do
their duty under his conduct may afford to do it valiantly; for what
need those fear who have God on their side? `(3.)` It is only through God,
and by the influence of his grace, that we do valiantly; it is he that
puts strength into us, and inspires us, who of ourselves are weak and
timorous, with courage and resolution. `(4.)` Though we do ever so
valiantly, the success must be attributed entirely to him; for he it is
that shall tread down our enemies, and not we ourselves. All our
victories, as well as our valour, are from him, and therefore at his
feet all our crown must be cast.
