Psalm 123
=========

Commentary
----------

This psalm was penned at a time then the church of God was brought low
and trampled upon; some think it was when the Jews were captives in
Babylon, though that was not the only time that they were insulted over
by the proud. The psalmist begins as if he spoke for himself only (v.
1), but presently speaks in the name of the church. Here is, `I.` Their
expectation of mercy from God (v. 1, 2). `II.` Their plea for mercy with
God, (v. 3, 4). In singing it we must have our eye up to God\'s favour
with a holy concern, and then an eye down to men\'s reproach with a holy
contempt.

A song of degrees.

### Verses 1-4

We have here,

`I.` The solemn profession which God\'s people make of faith and hope in
God, v. 1, 2. Observe, 1. The title here given to God: O thou that
dwellest in the heavens. Our Lord Jesus has taught us, in prayer, to
have an eye to God as our Father in heaven; not that he is confined
there, but there especially he manifests his glory, as the King in his
court. Heaven is a place of prospect and a place of power; he that
dwells there beholds thence all the calamities of his people and thence
can send to save them. Sometimes God seems to have forsaken the earth,
and the enemies of God\'s people ask, Where is now your God? But then
they can say with comfort, Our God is in the heavens. O thou that
sittest in the heavens (so some), sittest as Judge there; for the Lord
has prepared his throne in the heavens, and to that throne injured
innocency may appeal. 2. The regard here had to God. The psalmist
himself lifted up his eyes to him. The eyes of a good man are ever
towards the Lord, Ps. 25:15. In every prayer we lift up our soul, the
eye of our soul, to God, especially in trouble, which was the case here.
The eyes of the people waited on the Lord, v. 2. We find mercy coming
towards a people when the eyes of man, as of all the tribes of Israel,
are towards the Lord, Zec. 9:1. The eyes of the body are heaven-ward. Os
homini sublime dedit-To man he gave an erect mien, to teach us which way
to direct the eyes of the mind. Our eyes wait on the Lord, the eye of
desire and prayer, the begging eye, and the eye of dependence, hope, and
expectation, the longing eye. Our eyes must wait upon God as the Lord,
and our God, until that he have mercy upon us. We desire mercy from him,
we hope he will show us mercy, and we will continue our attendance on
him till the mercy come. This is illustrated (v. 2) by a similitude: Our
eyes are to God as the eyes of a servant, and handmaid, to the hand of
their master and mistress. The eyes of a servant are, `(1.)` To his
master\'s directing hand, expecting that he will appoint him his work,
and cut it out for him, and show him how he must do it. Lord, what wilt
thou have me to do? `(2.)` To his supplying hand. Servants look to their
master, or their mistress, for their portion of meat in due season,
Prov. 31:15. And to God must we look for daily bread, for grace
sufficient; from him we must receive it thankfully. `(3.)` To his
assisting hand. If the servant cannot do his work himself, where must he
look for help but to his master? And in the strength of the Lord God we
must go forth and go on. `(4.)` To his protecting hand. If the servant
meet with opposition in his work, if he be questioned for what he does,
if he be wronged and injured, who should bear him out and right him, but
his master that set him on work? The people of God, when they are
persecuted, may appeal to their Master, We are thine; save us. `(5.)` To
his correcting hand. If the servant has provoked his master to beat him,
he does not call for help against his master, but looks at the hand that
strikes him, till it shall say, \"It is enough; I will not contend for
ever.\" The people of God were now under his rebukes; and whither should
they turn but to him that smote them? Isa. 9:13. To whom should they
make supplication but to their Judge? They will not do as Hagar did, who
ran away from her mistress when she put some hardships upon her (Gen.
16:6), but they submit themselves to and humble themselves under God\'s
mighty hand. `(6.)` To his rewarding hand. The servant expects his wages,
his well-done, from his master. Hypocrites have their eye to the
world\'s hand; thence they have their reward (Mt. 6:2); but true
Christians have their eye to God as their rewarder.

`II.` The humble address which God\'s people present to him in their
calamitous condition (v. 3, 4), wherein, 1. They sue for mercy, not
prescribing to God what he shall do for them, nor pleading any merit of
their own why he should do it for them, but, Have mercy upon us, O Lord!
have mercy upon us. We find little mercy with men; their tender mercies
are cruel; there are cruel mockings. But this is our comfort, that with
the Lord there is mercy and we need desire no more to relieve us, and
make us easy, than the mercy of God. Whatever the troubles of the church
are, God\'s mercy is a sovereign remedy. 2. They set forth their
grievances: We are exceedingly filled with contempt. Reproach is the
wound, the burden, they complain of. Observe, `(1.)` Who were reproached:
\"We, who have our eyes up to thee.\" Those who are owned of God are
often despised and trampled on by the world. Some translate the words
which we render, those that are at ease, and the proud, so as to signify
the persons that are scorned and contemned. \"Our soul is troubled to
see how those that are at peace, and the excellent ones, are scorned and
despised.\" The saints are a peaceable people and yet are abused (Ps.
35:20), the excellent ones of the earth and yet undervalued, Lam. 4:1,
2. `(2.)` Who did reproach them. Taking the words as we read them, they
were the epicures who lived at ease, carnal sensual people, Job 12:5.
The scoffers are such as walk after their own lusts and serve their own
bellies, and the proud such as set God himself at defiance and had a
high opinion of themselves; they trampled on God\'s people, thinking
they magnified themselves by vilifying them. `(3.)` To what degree they
were reproached: \"We are filled, we are surfeited with it. Our soul is
exceedingly filled with it.\" The enemies thought they could never jeer
them enough, nor say enough to make them despicable; and they could not
but lay it to heart; it was a sword in their bones, Ps. 42:10. Note,
`[1.]` Scorning and contempt have been, and are, and are likely to be,
the lot of God\'s people in this world. Ishmael mocked Isaac, which is
called persecuting him; and so it is now, Gal. 4:29. `[2.]` In reference
to the scorn and contempt of men it is matter of comfort that there is
mercy with God, mercy to our good names when they are barbarously used.
Hear, O our God! for we are despised.
