Psalm 149
=========

Commentary
----------

The foregoing psalm was a hymn of praise to the Creator; this is a hymn
of praise to the Redeemer. It is a psalm of triumph in the God of
Israel, and over the enemies of Israel. Probably it was penned upon
occasion of some victory which Israel was blessed and honoured with.
Some conjecture that it was penned when David had taken the strong-hold
of Zion, and settled his government there. But it looks further, to the
kingdom of the Messiah, who, in the chariot of the everlasting gospel,
goes forth conquering and to conquer. To him, and his graces and
glories, we must have an eye, in singing this psalm, which proclaims, `I.`
Abundance of joy to all the people of God (v. 1-5). `II.` Abundance of
terror to the proudest of their enemies (v. 6-9).

### Verses 1-5

We have here,

`I.` The calls given to God\'s Israel to praise. All his works were, in
the foregoing psalm, excited to praise him; but here his saints in a
particular manner are required to bless him. Observe then, 1. Who are
called upon to praise God. Israel in general, the body of the church (v.
2), the children of Zion particularly, the inhabitants of that holy
hill, who are nearer to God than other Israelites; those that have the
word and ordinances of God near to them, that are not required to travel
far to them, are justly expected to do more in praising God than others.
All true Christians may call themselves the children of Zion, for in
faith and hope we have come unto Mount Zion, Heb. 12:22. The saints must
praise God, saints in profession, saints in power, for this is the
intention of their sanctification; they are devoted to the glory of God,
and renewed by the grace of God, that they may be unto him for a name
and a praise. 2. What must be the principle of this praise, and that is
holy joy in God: Let Israel rejoice, and the children of Zion be joyful,
and the saints be joyful in glory. Our praises of God should flow from a
heart filled with delight and triumph in God\'s attributes, and our
relation to him. Much of the power of godliness in the heart consists in
making God our chief joy and solacing ourselves in him; and our faith in
Christ is described by our rejoicing in him. We then give honour to God
when we take pleasure in him. We must be joyful in glory, that is, in
him as our glory, and in the interest we have in him; and let us look
upon it as our glory to be of those that rejoice in God. 3. What must be
the expressions of this praise. We must by all proper ways show forth
the praises of God: Sing to the Lord. We must entertain ourselves, and
proclaim his name, by singing praises to him (v. 3), singing aloud (v.
5), for we should sing psalms with all our heart, as those that are not
only not ashamed of it, but are enlarged in it. We must sing a new song,
newly composed upon every special occasion, sing with new affections,
which make the song new, though the words have been used before, and
keep them from growing threadbare. Let God be praised in the dance with
timbrel and harp, according to the usage of the Old-Testament church
very early (Ex. 15:20), where we find God praised with timbrels and
dances. Those who from this urge the use of music in religious worship
must by the same rule introduce dancing, for they went together, as in
David\'s dancing before the ark, and Jdg. 21:21. But, whereas many
scriptures in the New Testament keep up singing as a gospel-ordinance,
none provide for the keeping up of music and dancing; the gospel-canon
for psalmody is to sing with the spirit and with the understanding. 4.
What opportunities must be taken for praising God, none must be let
slip, but particularly, `(1.)` We must praise God in public, in the solemn
assembly (v. 1), in the congregation of saints. The more the better; it
is the more like heaven. Thus God\'s name must be owned before the
world; thus the service must have a solemnity put upon it, and we must
mutually excite one another to it. The principle, end, and design of our
coming together in religious assemblies is that we may join together in
praising God. Other parts of the service must be in order to this. `(2.)`
We must praise him in private. Let the saints be so transported with
their joy in God as to sing aloud upon their beds, when they awake in
the night, full of the praises of God, as David, Ps. 119:62. When God\'s
Israel are brought to a quiet settlement, let them enjoy that, with
thankfulness to God; much more may true believers, that have entered
into God\'s rest, and find repose in Jesus Christ, sing aloud for joy of
that. Upon their sick-beds, their death-beds, let them sing the praises
of their God.

`II.` The cause given to God\'s Israel for praise. Consider, 1. God\'s
doings for them. They have reason to rejoice inn God, to devote
themselves to his honour and employ themselves in his service; for it is
he that made them. He gave us our being as men, and we have reason to
praise him for that, for it is a noble and excellent being. He gave
Israel their being as a people, as a church, made them what they were,
so very different from other nations. Let that people therefore praise
him, for he formed them for himself, on purpose that they might show
forth his praise, Isa. 43:21. Let Israel rejoice in his Makers (so it is
in the original); for God said, Let us make man; and in this, some
think, is the mystery of the Trinity. 2. God\'s dominion over them. This
follows upon the former: if he made them, he is their King; he that gave
being no doubt may give law; and this ought to be the matter of our joy
and praise that we are under the conduct and protection of such a wise
and powerful King. Rejoice greatly, O daughter of Zion! for behold thy
king comes, the king Messiah, whom God has set upon his holy hill of
Zion; let all the children of Zion be joyful in him, and go forth to
meet him with their hosannas, Zec. 9:9. 3. God\'s delight in them. he is
a king that rules by love, and therefore to be praised; for the Lord
takes pleasure in his people, in their services, in their prosperity, in
communion with them, and in the communications of his favour to them. He
that is infinitely happy in the enjoyment of himself, and to whose
felicity no accession can be made, yet graciously condescends to take
pleasure in his people, Ps. 147:11. 4. God\'s designs concerning them.
Besides the present complacency he has in them, he has prepared for
their future glory: He will beautify the meek, the humble, and lowly,
and contrite in heart, that tremble at his word and submit to it, that
are patient under their afflictions and show all meekness towards all
men. These men vilify and asperse, but God will justify them, and wipe
off their reproach; nay, he will beautify them; they shall appear not
only clear, but comely, before all the world, with the comeliness that
he puts upon them. He will beautify them with salvation, with temporal
salvations (when God works remarkable deliverances for his people those
that had been among the pots become as the wings of a dove covered with
silver, Ps. 68:13), but especially with eternal salvation. The righteous
shall be beautified in that day when they shine forth as the sun. In the
hopes of this, let them now, in the darkest day, sing a new song.

### Verses 6-9

The Israel of God are here represented triumphing over their enemies,
which is both the matter of their praise (let them give to God the glory
of those triumphs) and the recompence of their praise; those that are
truly thankful to God for their tranquillity shall be blessed with
victory. Or it may be taken as a further expression of their praise (v.
6): let the high praises of God be in their mouth, and then, in a holy
zeal for his honour, let them take a two-edged sword in their hand, to
fight his battles against the enemies of his kingdom. Now this may be
applied, 1. To the many victories which God blessed his people Israel
with over the nations of Canaan and other nations that were devoted to
destruction. These began in Moses and Joshua, who, when they taught
Israel the high praises of the Lord, did withal put a two-edged sword in
their hand; David did so too, for, as he was the sweet singer of Israel,
so he was the captain of their hosts, and taught the children of Judah
the use of the bow (2 Sa. 1:18), taught their hands to war, as God had
taught his. Thus he and they went on victoriously, fighting the Lord\'s
battles, and avenging Israel\'s quarrels on those that had oppressed
them; then they executed vengeance upon the heathen (the Philistines,
Moabites, Ammonites, and others, 2 Sa. 8:1, etc.) and punishments upon
the people, for all the wrong they had done to God\'s people, v. 7.
Their kings and nobles were taken prisoners (v. 8) and on some of them
the judgment written was executed, as by Joshua on the kings of Canaan,
by Gideon on the princes of Midian, by Samuel on Agag. The honour of
this redounded to all the Israel of God; and to him who put it upon them
they return it entirely in their hallelujahs. Jehoshaphat\'s army had at
the same time the high praises of God in their mouth and a two-edged
sword in their hand, for they went forth to war singing the praises of
God, and then their sword did execution, 2 Chr. 20:23. Some apply it to
the time of the Maccabees, when the Jews sometimes gained great
advantages against their oppressors. And if it seem strange that the
meek should, notwithstanding that character, be thus severe, and upon
kings and nobles too, here is one word that justifies them in it; it is
the judgment written. They do not do it from any personal malice and
revenge, or any bloody politics that they govern themselves by, but by
commission from God, according to his direction, and in obedience to his
command; and Saul lost his kingdom for disobeying a command of this
nature. Thus the kings of the earth that shall be employed in the
destruction of the New-Testament Babylon will but execute the judgment
written, Rev. 17:16, 17. But, since now no such special commissions can
be produced, this will by no means justify the violence either of
subjects against their princes or of princes against their subjects, or
both against their neighbours, under pretence of religion; for Christ
never intended that his gospel should be propagated by fire and sword or
his righteousness wrought by the wrath of man. When the high praises of
God are in our mouth with them we should have an olive-branch of peace
in our hands. 2. To Christ\'s victories by the power of his gospel and
grace over spiritual enemies, in which all believers are more than
conquerors. The word of God is the two-edged sword (Heb. 4:12), the
sword of the Spirit (Eph. 6:17), which it is not enough to have in our
armoury, we must have it in our hand also, as our Master had, when he
said, It is written. Now, `(1.)` With this two-edged sword the first
preachers of the gospel obtained a glorious victory over the powers of
darkness; vengeance was executed upon the gods of the heathen, by the
conviction and conversion of those that had been long their worshippers,
and by the consternation and confusion of those that would not repent
(Rev. 6:15); the strongholds of Satan were cast down (2 Chr. 10:4, 5);
great men were made to tremble at the word, as Felix; Satan, the god of
this world, was cast out, according to the judgment given against him.
This is the honour of all Christians, that their holy religion has been
so victorious. `(2.)` With this two-edged sword believers fight against
their own corruptions, and, through the grace of God, subdue and mortify
them; the sin that had dominion over them is crucified; self, that once
sat king, is bound with chains and brought into subjection to the yoke
of Christ; the tempter is foiled and bruised under their feet. This
honour have all the saints. `(3.)` The complete accomplishment of this
will be in the judgment of the great day, when the Lord shall come with
ten thousands of his saints, to execute judgment upon all, Jude 14, 15.
Vengeance shall then be executed upon the heathen (Ps. 9:17), and
punishments, everlasting punishments, upon the people. Kings and nobles,
that cast away the bands and cords of Christ\'s government (Ps. 2:3),
shall not be able to cast away the chains and fetters of his wrath and
justice. Then shall be executed the judgment written, for the secrets of
men shall be judged according to the gospel. This honour shall all the
saints have, that, as assessors with Christ, they shall judge the world,
1 Co. 6:2. In the prospect of that let them praise the Lord, and
continue Christ\'s faithful servants and soldiers to the end of their
lives.
