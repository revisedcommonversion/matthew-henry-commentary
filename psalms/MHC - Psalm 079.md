Psalm 79
========

Commentary
----------

This psalm, if penned with any particular event in view, is with most
probability made to refer to the destruction of Jerusalem and the
temple, and the woeful havoc made of the Jewish nation by the Chaldeans
under Nebuchadnezzar. It is set to the same tune, as I may say, with the
Lamentations of Jeremiah, and that weeping prophet borrows two verses
out of it (v. 6, 7) and makes use of them in his prayer, Jer. 10:25.
Some think it was penned long before by the spirit of prophecy, prepared
for the use of the church in that cloudy and dark day. Others think that
it was penned then by the spirit of prayer, either by a prophet named
Asaph or by some other prophet for the sons of Asaph. Whatever the
particular occasion was, we have here, `I.` A representation of the very
deplorable condition that the people of God were in at this time (v.
1-5). `II.` A petition to God for succour and relief, that their enemies
might be reckoned with (v. 6, 7, 10, 12), that their sins might be
pardoned (v. 8, 9), and that they might be delivered (v. 11). `III.` A
plea taken from the readiness of his people to praise him (v. 13). In
times of the church\'s peace and prosperity this psalm may, in the
singing of it, give us occasion to bless God that we are not thus
trampled on and insulted. But it is especially seasonable in a day of
treading down and perplexity, for the exciting of our desires towards
God and the encouragement of our faith in him as the church\'s patron.

A psalm of Asaph.

### Verses 1-5

We have here a sad complaint exhibited in the court of heaven. The world
is full of complaints, and so is the church too, for it suffers, not
only with it, but from it, as a lily among thorns. God is complained to;
whither should children go with their grievances, but to their father,
to such a father as is able and willing to help? The heathen are
complained of, who, being themselves aliens from the commonwealth of
Israel, were sworn enemies to it. Though they knew not God, nor owned
him, yet, God having them in chain, the church very fitly appeals to him
against them; for he is King of nations, to overrule them, to judge
among the heathen, and King of saints, to favour and protect them.

`I.` They complain here of the anger of their enemies and the outrageous
fury of the oppressor, exerted,

`1.` Against places, v. 1. They did all the mischief they could, `(1.)` To
the holy land; they invaded that, and made inroads into it: \"The
heathen have come into thy inheritance, to plunder that, and lay it
waste.\" Canaan was dearer to the pious Israelites as it was God\'s
inheritance than as it was their own, as it was the land in which God
was known and his name was great rather than as it was the land in which
they were bred and born and which they and their ancestors had been long
in possession of. note, Injuries done to religion should grieve us more
than even those done to common right, nay, to our own right. We should
better bear to see our own inheritance wasted than God\'s inheritance.
This psalmist had mentioned it in the foregoing psalm as an instance of
God\'s great favour to Israel that he had cast out the heathen before
them, Ps. 78:55. But see what a change sin made; now the heathen are
suffered to pour in upon them. `(2.)` To the holy city: They have laid
Jerusalem on heaps, heaps of rubbish, such heaps as are raised over
graves, so some. The inhabitants were buried in the ruins of their own
houses, and their dwelling places became their sepulchres, their long
homes. `(3.)` To the holy house. That sanctuary which God had built like
high palaces, and which was thought to be established as the earth, was
now laid level with the ground: They holy temple have they defiled, by
entering into it and laying it waste. God\'s own people had defiled it
by their sins, and therefore God suffered their enemies to defile it by
their insolence.

`2.` Against persons, against the bodies of God\'s people; and further
their malice could not reach. `(1.)` They were prodigal of their blood,
and killed them without mercy; their eye did not spare, nor did they
give any quarter (v. 3): Their blood have they shed like water, wherever
they met with them, round about Jerusalem, in all the avenues to the
city; whoever went out or came in was waited for of the sword. Abundance
of human blood was shed, so that the channels of water ran with blood.
And they shed it with no more reluctancy or regret than if they had
spilt so much water, little thinking that every drop of it will be
reckoned for in the day when God shall make inquisition for blood. `(2.)`
They were abusive to their dead bodies. When they had killed them they
would let none bury them. Nay, those that were buried, even the dead
bodies of God\'s servants, the flesh of his saints, whose names and
memories they had a particular spite at, they dug up again, and gave
them to be meat to the fowls of the heaven and to the beasts of the
earth; or, at least, they left those so exposed whom they slew; they
hung them in chains, which was in a particular manner grievous to the
Jews to see, because God had given them an express law against this, as
a barbarous thing, Deu. 21:23. This inhuman usage of Christ\'s witnesses
is foretold (Rev. 11:9), and thus even the dead bodies were witnesses
against their persecutors. This is mentioned (says Austin, De Civitate
Dei, lib. 1 cap. 12) not as an instance of the misery of the persecuted
(for the bodies of the saints shall rise in glory, however they became
meat to the birds and the fowls), but of the malice of the persecutors.

`3.` Against their names (v. 4): \"We that survive have become a reproach
to our neighbours; they all study to abuse us and load us with contempt,
and represent us as ridiculous, or odious, or both, upbraiding us with
our sins and with our sufferings, or giving the lie to our relation to
God and expectations from him; so that we have become a scorn and
derision to those that are round about us.\" If God\'s professing people
degenerate from what themselves and their fathers were, they must expect
to be told of it; and it is well if a just reproach will help to bring
us to a true repentance. But it has been the lot of the gospel-Israel to
be made unjustly a reproach and derision; the apostles themselves were
counted as the offscouring of all things.

`II.` They wonder more at God\'s anger, v. 5. This they discern in the
anger of their neighbours, and this they complain most of: How long,
Lord, wilt thou be angry? Shall it be for ever? This intimates that they
desired no more than that God would be reconciled to them, that his
anger might be turned away, and then the remainder of men\'s wrath would
be restrained. Note, Those who desire God\'s favour as better than life
cannot but dread and deprecate his wrath as worse than death.

### Verses 6-13

The petitions here put up to God are very suitable to the present
distresses of the church, and they have pleas to enforce them,
interwoven with them, taken mostly from God\'s honour.

`I.` They pray that God would so turn away his anger from them as to turn
it upon those that persecuted and abused them (v. 6): \"Pour out thy
wrath, the full vials of it, upon the heathen; let them wring out the
dregs of it, and drink them.\" This prayer is in effect a prophecy, in
which the wrath of God is revealed from heaven against all ungodliness
and unrighteousness of men. Observe here, 1. The character of those he
prays against; they are such as have not known God, nor called upon his
name. The reason why men do not call upon God is because they do not
know him, how able and willing he is to help them. Those that persist in
ignorance of God, and neglect of prayer, are the ungodly, who live
without God in the world. There are kingdoms that know not God and obey
not the gospel, but neither their multitude nor their force united will
secure them from his just judgments. 2. Their crime: They have devoured
Jacob, v. 7. That is crime enough in the account of him who reckons that
those who touch his people touch the apple of his eye. They have not
only disturbed, but devoured, Jacob, not only encroached upon his
dwelling place, the land of Canaan, but laid it waste by plundering and
depopulating it. `(3.)` Their condemnation: \"Pour out thy wrath upon
them; do not only restrain them from doing further mischief, but reckon
with them for the mischief they have done.\"

`II.` They pray for the pardon of sin, which they own to be the procuring
cause of all their calamities. How unrighteous soever men were, God was
righteous in permitting them to do what they did. They pray, 1. That God
would not remember against them their former iniquities (v. 8), either
their own former iniquities, that now, when they were old, they might
not be made to possess the iniquities of their youth, or the former
iniquities of their people, the sins of their ancestors. In the
captivity of Babylon former iniquities were brought to account; but God
promises not again to do so (Jer. 31:29, 30), and so they pray,
\"Remember not against us our first sins,\" which some make to look as
far back as the golden calf, because God said, In the day when I visit I
will visit for this sin of theirs upon them, Ex. 32:34. If the children
by repentance and reformation cut off the entail of the parents\' sin,
they may in faith pray that God will not remember them against them.
When God pardons sin he blots it out and remembers it no more. 2. That
he would purge away the sins they had been lately guilty of, by the
guilt of which their minds and consciences had been defiled: Deliver us,
and purge away our sins, v. 9. Then deliverances from trouble are
granted in love, and are mercies indeed, when they are grounded upon the
pardon of sin and flow from that; we should therefore be more earnest
with God in prayer for the removal of our sins than for the removal of
our afflictions, and the pardon of them is the foundation and sweetness
of our deliverances.

`III.` They pray that God would work deliverance for them, and bring
their troubles to a good end and that speedily: Let thy tender mercies
speedily prevent us, v. 8. They had no hopes but from God\'s mercies,
his tender mercies; their case was so deplorable that they looked upon
themselves as the proper objects of divine compassion, and so near to
desperate that, unless divine mercy did speedily interpose to prevent
their ruin, they were undone. This whets their importunity: \"Lord, help
us; Lord, deliver us; help us under our troubles, that we may bear them
well; help us out of our troubles, that the spirit may not fail. Deliver
us from sin, from sinking.\" Three things they plead:-1. The great
distress they were reduced to: \"We are brought very low, and, being
low, shall be lost if thou help us not.\" The lower we are brought the
more need we have of help from heaven and the more will divine power be
magnified in raising us up. 2. Their dependence upon him: \"Thou art the
God of our salvation, who alone canst help. Salvation belongs to the
Lord, from whom we expect help; for in the Lord alone is the salvation
of his people.\" Those who make God the God of their salvation shall
find him so. 3. The interest of his own honour in their case. They plead
no merit of theirs; they pretend to none; but, \"Help us for the glory
of thy name; pardon us for thy name\'s sake.\" The best encouragements
in prayer are those that are taken from God only, and those things
whereby he has made himself known. Two things are insinuated in this
plea:-`(1.)` That God\'s name and honour would be greatly injured if he
did not deliver them; for those that derided them blasphemed God, as if
he were weak and could not help them, or had withdrawn and would not;
therefore they plead (v. 10), \"Wherefore should the heathen say, Where
is their God? He has forsaken them, and forgotten them; and this they
get by worshipping a God whom they cannot see.\" (Nil praeter nubes et
coeli numen adorant. Juv.-They adore no other divinity than the clouds
and the sky.) That which was their praise (that they served a God that
is every where) was now turned to their reproach and his too, as if they
served a God that is nowhere. \"Lord,\" say they, \"Make it to appear
that thou art by making it to appear that thou art with us and for us,
that when we are asked, Where is your God? we may be able to say, He is
nigh unto us in all that which we call upon him for, and you see he is
so by what he does for us.\" `(2.)` That God\'s name and honour would be
greatly advanced if he did deliver them; his mercy would be glorified in
delivering those that were so miserable and helpless. By making bare his
everlasting arm on their behalf he would make unto himself an
everlasting name; and their deliverance would be a type and figure of
the great salvation, which in the fulness of time Messiah the Prince
would work out, to the glory of God\'s name.

`IV.` They pray that God would avenge them on their adversaries, 1. For
their cruelty and barbarity (v. 10): \"Let the avenging of our blood\"
(according to the ancient law, Gen. 9:6) \"be known among the heathen;
let them be made sensible that what judgments are brought upon them are
punishments of the wrong they have done to us; let this be in our sight,
and by this means let God be known among the heathen as the God to whom
vengeance belongs (Ps. 94:1) and the God that espouses his people\'s
cause.\" Those that have intoxicated themselves with the blood of the
saints shall have blood given them to drink, for they are worthy. 2. For
their insolence and scorn (v. 12): \"Render to them their reproach. The
indignities which by word and deed they have done to the people of God
himself and his name let them be repaid to them with interest.\" The
reproach wherewith men have reproached us only we must leave it to God
whether he will render to them or no, and must pray that he would
forgive them; but the reproach wherewith they have blasphemed God
himself we may in faith pray that God would render seven-fold into their
bosoms, so as to strike at their hearts, to humble them, and bring them
to repentance. This prayer is a prophecy, of the same import with that
of Enoch, that God will convince sinners of all their hard speeches
which they have spoken against him (Jude 15) and will return them into
their own bosoms by everlasting terrors at the remembrance of them.

`V.` They pray that God would find out a way for the rescue of his poor
prisoners, especially the condemned prisoners, v. 11. The case of their
brethren who had fallen into the hands of the enemy was very sad; they
were kept close prisoners, and, because they durst not be heard to
bemoan themselves, they vented their griefs in deep and silent sighs.
All their breathing was sighing, and so was their praying. They were
appointed to die, as sheep for the slaughter, and had received the
sentence of death within themselves. This deplorable case the psalmist
recommends, 1. To the divine pity: \"Let their sighs come up before
thee, and be thou pleased to take cognizance of their moans.\" 2. To the
divine power: \"According to the greatness of thy arm, which no creature
can contest with, preserve thou those that are appointed to die from the
death to which they are appointed.\" Man\'s extremity is God\'s
opportunity to appear for his people. See 2 Co. 1:8-10.

Lastly, They promise the returns of praise for the answers of prayer (v.
13): So we will give thee thanks for ever. Observe, 1. How they please
themselves with their relation to God. \"Though we are oppressed and
brought low, yet we are the sheep of thy pasture, not disowned and cast
off by thee for all this: We are thine; save us.\" 2. How they promise
themselves an opportunity of praising God for their deliverance, which
they therefore desired, and would bid welcome, because it would furnish
them with matter for thanksgiving and put their hearts in tune for that
excellent work, the work of heaven. 3. How they oblige themselves not
only to give God thanks at present, but to show forth his praise unto
all generations, that is, to do all they could both to perpetuate the
remembrance of God\'s favours to them and to engage their posterity to
keep up the work of praise. 4. How they plead this with God: \"Lord,
appear for us against our enemies; for, if they get the better, they
will blaspheme thee (v. 12); but, if we be delivered, we will praise
thee. Lord, we are that people of thine which thou hast formed for
thyself, to show forth thy praise; if we be cut off, whence shall that
rent, that tribute, be raised?\" Note, Those lives that are entirely
devoted to God\'s praise are assuredly taken under his protection.
