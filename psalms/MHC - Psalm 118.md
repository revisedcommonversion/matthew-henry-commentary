Psalm 118
=========

Commentary
----------

It is probable that David penned this psalm when he had, after many a
story, weathered his point at last, and gained a full possession of the
kingdom to which he had been anointed. He then invites and stirs up his
friends to join with him, not only in a cheerful acknowledgment of
God\'s goodness and a cheerful dependence upon that goodness for the
future, but in a believing expectation of the promised Messiah, of whose
kingdom and his exaltation to it his were typical. To him, it is
certain, the prophet here bears witness, in the latter part of the
psalm. Christ himself applies it to himself (Mt. 21:42), and the former
part of the psalm may fairly, and without forcing, be accommodated to
him and his undertaking. Some think it was first calculated for the
solemnity of the bringing of the ark to the city of David, and was
afterwards sung at the feast of tabernacles. In it, `I.` David calls upon
all about him to give to God the glory of his goodness (v. 1-4). `II.` He
encourages himself and others to trust in God, from the experience he
had had of God\'s power and pity in the great and kind things he had
done for him (v. 5-18). `III.` He gives thanks for his advancement to the
throne, as it was a figure of the exaltation of Christ (v. 19-23). `IV.`
The people, the priests, and the psalmist himself, triumph in the
prospect of the Redeemer\'s kingdom (v. 24-29). In singing this psalm we
must glorify God for his goodness, his goodness to us, and especially
his goodness to us in Jesus Christ.

### Verses 1-18

It appears here, as often as elsewhere, that David had his heart full of
the goodness of God. He loved to think of it, loved to speak of it, and
was very solicitous that God might have the praise of it and others the
comfort of it. The more our hearts are impressed with a sense of God\'s
goodness the more they will be enlarged in all manner of obedience. In
these verses,

`I.` He celebrates God\'s mercy in general, and calls upon others to
acknowledge it, from their own experience of it (v. 1): O give thanks
unto the Lord, for he is not only good in himself, but good to you, and
his mercy endures for ever, not only in the everlasting fountain, God
himself, but in the never-failing streams of that mercy, which shall run
parallel with the longest line of eternity, and in the chosen vessels of
mercy, who will be everlasting monuments of it. Israel, and the house of
Aaron, and all that fear God, were called upon to trust in God (Ps.
115:9-11); here they are called upon to confess that his mercy endures
for ever, and so to encourage themselves to trust in him, v. 2-4.
Priests and people, Jews and proselytes, must all own God\'s goodness,
and all join in the same thankful song; if they can say no more, let
them say this for him, that his mercy endures for ever, that they have
had experience of it all their days, and confide in it for good things
that shall last for ever. The praises and thanksgivings of all that
truly fear the Lord shall be as pleasing to him as those of the house of
Israel or the house of Aaron.

`II.` He preserves an account of God\'s gracious dealings with him in
particular, which he communicates to others, that they might thence
fetch both songs of praise and supports of faith, and both ways God
would have the glory. David had, in his time, waded through a great deal
of difficulty, which gave him great experience of God\'s goodness. Let
us therefore observe here,

`1.` The great distress and danger that he had been in, which he reflects
upon for the magnifying of God\'s goodness to him in his present
advancement. There are many who, when they are lifted up, care not for
hearing or speaking of their former depressions; but David takes all
occasions to remember his own low estate. He was in distress (v. 5),
greatly straitened and at a loss; there were many that hated him (v. 7),
and this could not but be a great grief to one of an ingenuous spirit,
that strove to gain the good affections of all. All nations compassed me
about, v. 10. All the nations adjacent to Israel set themselves to give
disturbance to David, when he had newly come to the throne, Philistines,
Moabites, Syrians, Ammonites, etc. We read of his enemies round about;
they were confederate against him, and thought to cut off all succours
from him. This endeavour of his enemies to surround him is repeated (v.
11): They compassed me about, yea, they compassed me about, which
intimates that they were virulent and violent, and, for a time,
prevalent, in their attempts against him, and when put into disorder
they rallied again and pushed on their design. They compassed me about
like bees, so numerous were they, so noisy, so vexatious; they came
flying upon him, came upon him in swarms, set upon him with their
malignant stings; but it was to their own destruction, as the bee, they
say, loses her life with her sting, Animamque in vulnere ponit-She lays
down her life in the wound. Lord, how are those increased that trouble
me! Two ways David was brought into trouble:-`(1.)` By the injuries that
men did him (v. 13): Thou (O enemy!) hast thrust sore at me, with many a
desperate push, that I might fall into sin and into ruin. Thrusting thou
hast thrust at me (so the word is), so that I was ready to fall. Satan
is the great enemy that thrusts sorely at us by his temptations, to cast
us down from our excellency, that we may fall from our God and from our
comfort in him; and, if Go had not upheld us by his grace, his thrusts
would have been fatal to us. `(2.)` By the afflictions which God laid upon
him (v. 18): The Lord has chastened me sore. Men thrust at him for his
destruction; God chastened him for his instruction. They thrust at him
with the malice of enemies; God chastened him with the love and
tenderness of a Father. Perhaps he refers to the same trouble which God,
the author of it, designed for his profit, that by it he might partake
of his holiness (Heb. 12:10, 11); howbeit, men, who were the instruments
of it, meant not so, neither did their heart think so, but it was in
their heart to cut off and destroy, Isa. 10:7. What men intend for the
greatest mischief God intends for the greatest good, and it is easy to
say whose counsel shall stand. God will sanctify the trouble to his
people, as it is his chastening, and secure the good he designs; and he
will guard them against the trouble, as it is the enemies\' thrusting,
and secure them from the evil they design, and then we need not fear.

This account which David gives of his troubles is very applicable to our
Lord Jesus. Many there were that hated him, hated him without a cause.
They compassed him about; Jews and Romans surrounded him. They thrust
sorely at him; the devil did so when he tempted him; his persecutors did
so when they reviled him; nay, the Lord himself chastened him sorely,
bruised him, and put him to grief, that by his stripes we might be
healed.

`2.` The favour God vouchsafed to him in his distress. `(1.)` God heart his
prayer (v. 5): \"He answered me with enlargements; he did more for me
than I was able to ask; he enlarged my heart in prayer and yet gave more
largely than I desired.\" He answered me, and set me in a large place
(so we read it), where I had room to bestir myself, room to enjoy
myself, and room to thrive; and the large place was the more comfortable
because he was brought to it out of distress, Ps. 4:1. `(2.)` God baffled
the designs of his enemies against him: They are quenched as the fire of
thorns (v. 12), which burns furiously for a while, makes a great noise
and a great blaze, but is presently out, and cannot do the mischief that
it threatened. Such was the fury of David\'s enemies; such is the
laughter of the fool, like the crackling of thorns under a pot (Eccl.
7:6), and such is the anger of the fool, which therefore is not to be
feared, any more than his laughter is to be envied, but both to be
pitied. They thrust sorely at him, but the Lord helped him (v. 13),
helped him to keep his feet and maintain his ground. Our spiritual
enemies would, long before this, have been our ruin if God had not been
our helper. `(3.)` God preserved his life when there was but a step
between him and death (v. 18): \"He has chastened me, but he has not
given me over unto death, for he has not given me over to the will of my
enemies.\" To this St. Paul seems to refer in 2 Co. 6:9. As dying, and
behold we live; as chastened, and not killed. We ought not therefore,
when we are chastened sorely, immediately to despair of life, for God
sometimes, in appearance, turns men to destruction, and yet says,
Return; says unto them, Live.

This also is applicable to Jesus Christ. God answered him, and set him
in a large place. He quenched the fire of his enemies; rage, which did
but consume themselves; for through death he destroyed him that had the
power of death. He helped him through his undertaking; and thus far he
did not give him over unto death that he did not leave him in the grave,
nor suffer him to see corruption. Death had no dominion over him.

`3.` The improvement he made of this favour. `(1.)` It encouraged him to
trust in God; from his own experience he can say, It is better, more
wise, more comfortable, and more safe, there is more reason for it, and
it will speed better, to trust in the Lord, than to put confidence in
man, yea, though it be in princes, v. 8, 9. He that devotes himself to
God\'s guidance and government, with an entire dependence upon God\'s
wisdom, power, and goodness, has a better security to make him easy than
if all the kings and potentates of the earth should undertake to protect
him. `(2.)` It enabled him to triumph in that trust. `[1.]` He triumphs in
God, and in his relation to him and interest in him (v. 6): \"The Lord
is on my side. He is a righteous God, and therefore espouses my
righteous cause and will plead it.\" If we are on God\'s side, he is on
ours; if we be for him and with him, he will be for us and with us (v.
7): \"The Lord takes my part, and stands up for me, with those that help
me. He is to me among my helpers, and so one of them that he is all in
all both to them and me, and without him I could not help myself nor
could any friend I have in the world help me.\" Thus (v. 14), \"The Lord
is my strength and my song; that is, I make him so (without him I am
weak and sad, but on him I stay myself as my strength, both for doing
and suffering, and in him I solace myself as my song, by which I both
express my joy and ease my grief), and, making him so, I find him so: he
strengthens my heart with his graces and gladdens my heart with his
comforts.\" If God be our strength, he must be our song; if he work all
our works in us, he must have all praise and glory from us. God is
sometimes the strength of his people when he is not their song; they
have spiritual supports when they want spiritual delights. But, if he be
both to us, we have abundant reason to triumph in him; for, he be our
strength and our song, he has become not only our Saviour, but our
salvation; for his being our strength is our protection to the
salvation, and his being our song is an earnest and foretaste of the
salvation. `[2.]` He triumphs over his enemies. Now shall his head be
lifted up above them; for, First, He is sure they cannot hurt him: \"God
is for me, and then I will not fear what man can do against me,\" v. 6.
He can set them all at defiance, and is not disturbed at any of their
attempts. \"They can do nothing to me but what God permits them to do;
they can do no real damage, for they cannot separate between me and God;
they cannot do any thing but what God can make to work for my good. The
enemy is a man, a depending creature, whose power is limited, and
subordinate to a higher power, and therefore I will not fear him.\" Who
art thou, that thou shouldst be afraid of a man that shall die? Isa.
51:12. The apostle quotes this, with application to all Christians, Heb.
13:6. They may boldly say, as boldly as David himself, The Lord is my
helper, and I will not fear what man shall do unto me; let him do his
worst. Secondly, He is sure that he shall be too hard for them at last:
\"I shall see my desire upon those that hate me (v. 7); I shall see them
defeated in their designs against me; nay, In the name of the Lord I
will destroy them (v. 10-12); I trust in the name of the Lord that I
shall destroy them, and in his name I will go forth against them,
depending on his strength, by warrant from him, and with an eye to his
glory, not confiding in myself nor taking vengeance for myself.\" Thus
he went forth against Goliath, in the name of the God of Israel, 1 Sa.
17:45. David says this as a type of Christ, who triumphed over the
powers of darkness, destroyed them, and made a show of them openly.
`[3.]` He triumphs in an assurance of the continuance of his comfort,
his victory, and his life. First, Of his comfort (v. 15): The voice of
rejoicing and salvation is in the tabernacles of the righteous, and in
mine particularly, in my family. The dwellings of the righteous in this
world are but tabernacles, mean and movable; here we have no city, no
continuing city. But these tabernacles are more comfortable to them than
the palaces of the wicked are to them; for in the house where religion
rules, 1. There is salvation; safety from evil, earnests of eternal
salvation, which has come to this house, Lu. 19:9. 2. Where there is
salvation there is cause for rejoicing, for continual joy in God. Holy
joy is called the joy of salvation, for in that there is abundant matter
for joy. 3. Where there is rejoicing there ought to be the voice of
rejoicing, that is, praise and thanksgiving. Let God be served with
joyfulness and gladness of heart, and let the voice of that rejoicing be
heard daily in our families, to the glory of God and encouragement of
others. Secondly, Of his victory: The right hand of the Lord does
valiantly (v. 15) and is exalted; for (as some read it) it has exalted
me. The right hand of God\'s power is engaged for his people, and it
acts vigorously for them and therefore victoriously. For what difficulty
can stand before the divine valour? We are weak, and act but cowardly
for ourselves; but God is mighty, and acts valiantly for us, with
jealousy and resolution, Isa. 63:5, 6. There is spirit, as well as
strength, in all God\'s operations for his people. And, when God\'s
right hand does valiantly for our salvation, it ought to be exalted in
our praises. Thirdly, Of his life (v. 17): \"I shall not die by the
hands of my enemies that seek my life, but live and declare the works of
the Lord; I shall live a monument of God\'s mercy and power; his works
shall be declared in me, and I will make it the business of my life to
praise and magnify God, looking upon that as the end of my
preservation.\" Note, It is not worth while to live for any other
purpose than to declare the works of God, for his honour and the
encouragement of others to serve him and trust in him. Such as these
were the triumphs of the Son of David in the assurance he had of the
success of his undertaking and that the good pleasure of the Lord should
prosper in his hand.

### Verses 19-29

We have here an illustrious prophecy of the humiliation and exaltation
of our Lord Jesus, his sufferings, and the glory that should follow.
Peter thus applies it directly to the chief priests and scribes, and
none of them could charge him with misapplying it, Acts 4:11. Now
observe here,

`I.` The preface with which this precious prophecy is introduced, v.
19-21. 1. The psalmist desires admission into the sanctuary of God,
there to celebrate the glory of him that cometh in the name of the Lord:
Open to me the gates of righteousness. So the temple-gates are called,
because they were shut against the uncircumcised, and forbade the
stranger to come nigh, as the sacrifices there offered are called
sacrifices of righteousness. Those that would enter into communion with
God in holy ordinances must become humble suitors to God for admission.
And when the gates of righteousness are opened to us we must go into
them, must enter into the holiest, as far as we have leave, and praise
the Lord. Our business within God\'s gates is to praise God; therefore
we should long till the gates of heaven be opened to us, that we may go
into them to dwell in God\'s house above, where we shall be still
praising him. 2. He sees admission granted him (v. 20): This is the gate
of the Lord, the gate of his appointing, into which the righteous shall
enter; as if he had said, \"The gate you knocked at is opened, and you
are welcome. Knock, and it shall be opened unto you.\" Some by this gate
understand Christ, by whom we are taken into fellowship with God and our
praises are accepted; he is the way; there is no coming to the Father
but by him (Jn. 14:6), he is the door of the sheep (Jn. 10:9); he is the
gate of the temple, by whom, and by whom only, the righteous, and they
only, shall enter, and come into God\'s righteousness, as the expression
is, Ps. 69:27. The psalmist triumphs in the discovery that the gate of
righteousness, which had been so long shut, and so long knocked at, was
now at length opened. 3. He promises to give thanks to God for this
favour (v. 21): I will praise thee. Those that saw Christ\'s day at so
great a distance saw cause to praise God for the prospect; for in him
they saw that God had heard them, had heard the prayers of the
Old-Testament saints for the coming of the Messiah, and would be their
salvation.

`II.` The prophecy itself, v. 22, 23. This may have some reference to
David\'s preferment; he was the stone which Saul and his courtiers
rejected, but was by the wonderful providence of God advanced to be the
headstone of the building. But its principal reference is to Christ; and
here we have, 1. His humiliation. He is the stone which the builders
refused; he is the stone cut out of the mountain without hands, Dan.
2:34. He is a stone, not only for strength, and firmness, and duration,
but for life, in the building of the spiritual temple; and yet a
precious stone (1 Pt. 2:6), for the foundation of the gospel-church must
be sapphires, Isa. 54:11. This stone was rejected by the builders, by
the rulers and people of the Jews (Acts 4:8, 10, 11); they refused to
own him as the stone, the Messiah promised; they would not build their
faith upon him nor join themselves to him; they would make no use of
him, but go on in their building without him; they denied him in the
presence of Pilate (Acts 3:13) when they said, We have no king but
Caesar. They trampled upon this stone, threw it among the rubbish out of
the city; nay, they stumbled at it. This was a disgrace to Christ, but
it proved the ruin of those that thus made light of him. Rejecters of
Christ are rejected of God. 2. His exaltation. He has become the
headstone of the corner; he is advanced to the highest degree both of
honour and usefulness, to be above all, and all in all. He is the chief
corner-stone in the foundation, in whom Jew and Gentile are united, that
they may be built up one holy house. He is the chief top-stone in the
corner, in whom the building is completed, and who must in all things
have the pre-eminence, as the author and finisher of our faith. Thus
highly has God exalted him, because he humbled himself; and we, in
compliance with God\'s design, must make him the foundation of our hope,
the centre of our unity, and the end of our living. To me to live is
Christ. 3. The hand of God in all this: This is the Lord\'s doing; it is
from the Lord; it is with the Lord; it is the product of his counsel; it
is his contrivance. Both the humiliation and the exaltation of the Lord
Jesus were his work, Acts 2:23; 4:27, 28. He sent him, sealed him; his
hand went with him throughout his whole undertaking, and from first to
last he did his Father\'s will; and this ought to be marvellous in our
eyes. Christ\'s name is Wonderful; and the redemption he wrought out is
the most amazing of all God\'s works of wonder; it is what the angels
desire to look into, and will be admiring to eternity; much more ought
we to admire it, who owe our all to it. Without controversy, great is
the mystery of godliness.

`III.` The joy wherewith it is entertained and the acclamations which
attend this prediction.

`1.` Let the day be solemnized to the honour of God with great joy (v.
24): This is the day the Lord has made. The whole time of the
gospel-dispensation, that accepted time, that day of salvation, is what
the Lord has made so; it is a continual feast, which ought to be kept
with joy. Or it may very fitly be understood of the Christian sabbath,
which we sanctify in remembrance of Christ\'s resurrection, when the
rejected stone began to be exalted; and so, `(1.)` Here is the doctrine of
the Christian sabbath: It is the day which the Lord has made, has made
remarkable, made holy, has distinguished from other days; he has made it
for man: it is therefore called the Lord\'s day, for it bears his image
and superscription. `(2.)` The duty of the sabbath, the work of the day
that is to be done in his day: We will rejoice and be glad in it, not
only in the institution of the day, that there is such a day appointed,
but in the occasion of it, Christ\'s becoming the head of the corner.
This we ought to rejoice in both as his honour and our advantage.
Sabbath days must be rejoicing days, and then they are to us as the days
of heaven. See what a good Master we serve, who, having instituted a day
for his service, appoints it to be spent in holy joy.

`2.` Let the exalted Redeemer be met, and attended, with joyful hosannas,
v. 25, 26.

`(1.)` Let him have the acclamations of the people, as is usual at the
inauguration of a prince. Let every one of his loyal subjects shout for
joy, Save now, I beseech thee, O Lord! This is like Vivat rex-Long live
the king, and expresses a hearty joy for his accession to the crown, an
entire satisfaction in his government, and a zealous affection to the
interests and honour of it. Hosanna signifies, Save now, I beseech thee.
`[1.]` \"Lord, save me, I beseech thee; let this Saviour be my Saviour,
and, in order to that, my ruler; let me be taken under his protection
and owned as one of his willing subjects. His enemies are my enemies;
Lord, I beseech thee, save me from them. Send me an interest in that
prosperity which his kingdom brings with it to all those that entertain
it. Let my soul prosper and be in health, in that peace and
righteousness which his government brings, Ps. 72:3. Let me have victory
over those lusts that war against my soul, and let divine grace go on in
my heart conquering and to conquer.\" `[2.]` \"Lord, preserve him, I
beseech thee, even the Saviour himself, and send him prosperity in all
his undertakings; give success to his gospel, and let it be mighty,
through God, to the pulling down of strong-holds and reducing souls to
their allegiance to him. Let his name be sanctified, his kingdom come,
his will be done.\" Thus let prayer be made for him continually, Ps.
72:15. On the Lord\'s day, when we rejoice and are glad in his kingdom,
we must pray for the advancement of it more and more, and its
establishment upon the ruins of the devil\'s kingdom. When Christ made
his public entry into Jerusalem he was thus met by his well-wishers (Mt.
21:9): Hosanna to the Son of David; long live King Jesus; let him reign
for ever.

`(2.)` Let the priests, the Lord\'s ministers, do their part in this great
solemnity, v. 26. `[1.]` Let them bless the prince with their praises:
Blessed is he that cometh in the name of the Lord. Jesus Christ is he
that cometh-ho erchomenos, he that was to come and is yet to come again,
Rev. 1:8. He comes in the name of the Lord, with a commission from him,
to act for him, to do his will and to seek his glory; and therefore we
must say, Blessed be he that cometh; we must rejoice that he has come;
we must speak well of him, admire him, and esteem him highly, as one we
are eternally obliged to, call him blessed Jesus, blessed for ever, Ps.
45:2. We must bid him welcome into our hearts, saying, \"Come in, thou
blessed of the Lord; come in by thy grace and Spirit, and take
possession of me for thy own.\" We must bless his faithful ministers
that come in his name, and receive them for his sake, Isa. 52:7; Jn.
13:20. We must pray for the enlargement and edification of his church,
for the ripening of things for his second coming, and then that he who
has said, Surely I come quickly, would even so come. `[2.]` Let them
bless the people with their prayers: We have blessed you out of the
house of the Lord. Christ\'s ministers are not only warranted, but
appointed to pronounce a blessing, in his name, upon all his loyal
subjects that love him and his government in sincerity, Eph. 6:24. We
assure you that in and through Jesus Christ you are blessed; for he came
to bless you. \"You are blessed out of the house of the Lord, that is,
with spiritual blessings in heavenly places (Eph. 1:3), and therefore
have reason to bless him who has thus blessed you.\"

`3.` Let sacrifices of thanksgiving be offered to his honour who offered
for us the great atoning sacrifice, v. 27. Here is, `(1.)` The privilege
we enjoy by Jesus Christ: God is the Lord who has shown us light. God is
Jehovah, is known by that name, a God performing what he has promised
and perfecting what he has begun, Ex. 6:3. He has shown us light, that
is, he has given us the knowledge of himself and his will. He has shined
upon us (so some); he has favoured us, and lifted up upon us the light
of his countenance; he has given us occasion for joy and rejoicing,
which is light to the soul, by giving us a prospect of everlasting light
in heaven. The day which the Lord has made brings light with it, true
light. `(2.)` The duty which this privilege calls for: Bind the sacrifice
with cords, that, being killed, the blood of it may be sprinkled upon
the horns of the altar, according to the law; or perhaps it was the
custom (though we read not of it elsewhere) to bind the sacrifice to the
horns of the altar while things were getting ready for the slaying of
it. Or this may have a peculiar significancy here; the sacrifice we are
to offer to God, in gratitude for redeeming love, is ourselves, not to
be slain upon the altar, but living sacrifices (Rom. 12:1), to be bound
to the altar, spiritual sacrifices of prayer and praise, in which our
hearts must be fixed and engaged, as the sacrifice was bound with cords
to the horns of the altar, not to start back.

`4.` The psalmist concludes with his own thankful acknowledgments of
divine grace, in which he calls upon others to join with him, v. 28, 29.
`(1.)` He will praise God himself, and endeavour to exalt him in his own
heart and in the hearts of others, and this because of his
covenant-relation to him and interest in him: \"Thou art my God, on whom
I depend, and to whom I am devoted, who ownest me and art owned by me;
and therefore I will praise thee.\" `(2.)` He will have all about him to
give thanks to God for these glad tidings of great joy to all people,
that there is a Redeemer, even Christ the Lord. In him it is that God is
good to man and that his mercy endures for ever; in him the covenant of
grace is made, and in him it is made sure, made good, and made an
everlasting covenant. He concludes this psalm as he began it (v. 1), for
God\'s glory must be the Alpha and Omega, the beginning and the end, of
all our addresses to him. Hallowed by thy name, and thine is the glory.
And this fitly closes a prophecy of Christ. The angels give thanks for
man\'s redemption. Glory to God in the highest (Lu. 2:14), for there is
on earth peace, to which we must echo with our hosannas, as they did,
Lu. 19:38. Peace in heaven to us through Christ, and therefore glory in
the highest.
