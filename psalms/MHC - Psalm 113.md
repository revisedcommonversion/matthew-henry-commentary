Psalm 113
=========

Commentary
----------

This psalm begins and ends with \"Hallelujah;\" for, as many others, it
is designed to promote the great and good work of praising God. `I.` We
are here called upon and urged to praise God (v. 1-3). `II.` We are here
furnished with matter for praise, and words are put into our mouths, in
singing which we must with holy fear and love give to God the glory of,
`1.` The elevations of his glory and greatness (v. 4, 5). 2. The
condescensions of his grace and goodness (v. 6-9), which very much
illustrate one another, that we may be duly affected with both.

### Verses 1-9

In this psalm,

`I.` We are extorted to give glory to God, to give him the glory due to
his name.

`1.` The invitation is very pressing: praise you the Lord, and again and
again, Praise him, praise him; blessed be his name, for it is to be
praised, v. 1-3. This intimates, `(1.)` That it is a necessary and most
excellent duty, greatly pleasing to God, and has a large room in
religion. `(2.)` That it is a duty we should much abound in, in which we
should be frequently employed and greatly enlarged. `(3.)` That it is work
which we are very backward to, and which we need to be engaged and
excited to by precept upon precept and line upon line. `(4.)` That those
who are much in praising God themselves will court others to it, both
because they find the weight of the work, and that there is need of all
the help they can fetch in (there is employment for all hearts, all
hands, and all little enough), and because they find the pleasure of it,
which they wish all their friends may share in.

`2.` The invitation is very extensive. Observe, `(1.)` From whom God has
praise-from his own people; they are here called upon to praise God, as
those that will answer the call: Praise, O you servants of the Lord!
They have most reason to praise him; for those that attend him as his
servants know him best and receive most of his favours. And it is their
business to praise him; that is the work required of them as his
servants: it is easy pleasant work to speak well of their Master, and do
him what honour they can; if they do not, who should? Some understand it
of the Levites; but, if so, all Christians are a royal priesthood, to
show forth the praises of him that has called them, 1 Pt. 2:9. The
angels are the servants of the Lord; they need not be called upon by us
to praise God, yet it is a comfort to us that they do praise him, and
that they praise him better than we can. `(2.)` From whom he ought to have
praise. `[1.]` From all ages (v. 2)-from this time forth for evermore.
Let not this work die with us, but let us be doing it in a better world,
and let those that come after us be doing it in this. Let not our seed
degenerate, but let God be praised through all the generations of time,
and not in this only. We must bless the Lord in our day, by saying, with
the psalmist, Blessed be his name now and always. `[2.]` From all
places-from the rising of the sun to the going down of the same, that
is, throughout the habitable world. Let all that enjoy the benefit of
the sun rising (and those that do so must count upon it that the sun
will set) give thanks for that light to the Father of lights. God\'s
name is to be praised; it ought to be praised by all nations; for in
every place, from east to west, there appear the manifest proofs and
products of his wisdom, power, and goodness; and it is to be lamented
that so great a part of mankind are ignorant of him, and give that
praise to others which is due to him alone. But perhaps there is more in
it; as the former verse gave us a glimpse of the kingdom of glory,
intimating that God\'s name shall be blessed for ever (when time shall
be no more that praise shall be the work of heaven), so this verse gives
us a glimpse of the kingdom of grace in the gospel-dispensation of it.
When the church shall no longer be confined to the Jewish nation, but
shall spread itself all the world over, when in every place spiritual
incense shall be offered to our God (Mal. 1:11), then from the rising to
the setting of the sun the Lord\'s name shall be praised by some in all
countries.

`II.` We are here directed what to give him the glory of.

`1.` Let us look up with an eye of faith, and see how high his glory is
in the upper world, and mention that to his praise, v. 4, 5. We are, in
our praises, to exalt his name, for he is high, his glory is high. `(1.)`
High above all nations, their kings though ever so pompous, their people
though ever so numerous. Whether it be true of an earthly king or no
that though he is major singulis-greater than individuals, he is minor
universis-less than the whole, we will not dispute; but we are sure it
is not true of the King of kings. Put all the nations together, and he
is above them all; they are before him as the drop of the bucket and the
small dust of the balance, Isa. 60:15, 17. Let all nations think and
speak highly of God, for he is high above them all. `(2.)` High above the
heavens; the throne of his glory is in the highest heavens, which should
raise our hearts in praising him, Lam. 3:41. His glory is above the
heavens, that is, above the angels; he is above what they are, for their
brightness is nothing to his,-above what they do, for they are under his
command and do his pleasure,-and above what even they can speak him to
be. He is exalted above all blessing and praise, not only all ours, but
all theirs. We must therefore say, with holy admiration, Who is like
unto the Lord our God? who of all the princes and potentates of the
earth? who of all the bright and blessed spirits above? None can equal
him, none dare compare with him. God is to be praised as transcendently,
incomparably, and infinitely great; for he dwells on high, and from on
high sees all, and rules all, and justly attracts all praise to himself.

`2.` Let us look around with an eye of observation, and see how extensive
his goodness is in the lower world, and mention that to his praise. He
is a God who exalts himself to dwell, who humbles himself in heaven, and
in earth. Some think there is a transposition, He exalts himself to
dwell in heaven, he humbles himself to behold on earth; but the sense is
plain enough as we take it, only observe, God is said to exalt himself
and to humble himself, both are his own act and deed; as he is
self-existent, so he is both the fountain of his own honour and the
spring of his own grace; God\'s condescending goodness appears,

`(1.)` In the cognizance he takes of the world below him. His glory is
above the nations and above the heavens, and yet neither is neglected by
him. God is great, yet he despises not any, Job 36:5. He humbles himself
to behold all his creatures, all his subjects, though he is infinitely
above them. Considering the infinite perfection, sufficiency, and
felicity of the divine nature, it must be acknowledged as an act of
wonderful condescension that God is pleased to take into the thoughts of
his eternal counsel, and into the hand of his universal Providence, both
the armies of heaven and the inhabitants of the earth (Dan. 4:35); even
in this dominion he humbles himself. `[1.]` It is condescension in him
to behold the things in heaven, to support the beings, direct the
motions, and accept the praises and services, of the angels themselves;
for he needs them not, nor is benefited by them. `[2.]` Much more is it
condescension in him to behold the things that are in the earth, to
visit the sons of men, and regard them, to order and overrule their
affairs, and to take notice of what they say and do, that he may fill
the earth with his goodness, and so set us an example of stooping to do
good, of taking notice of, and concerning ourselves about, our
inferiors. If it be such condescension for God to behold things in
heaven and earth, what an amazing condescension was it for the Son of
God to come from heaven to earth and take our nature upon him, that he
might seek and save those that were lost! Herein indeed he humbled
himself.

`(2.)` In the particular favour he sometimes shows to the least and lowest
of the inhabitants of this meaner lower world. He not only beholds the
great things in the earth, but the meanest, and those things which great
men commonly overlook. Not does he merely behold them, but does wonders
for them, and things that are very surprising, out of the common road of
providence and chain of causes, which shows that the world is governed,
not by a course of nature, for that would always run in the same
channel, but by a God of nature, who delights in doing things we looked
not for. `[1.]` Those that have been long despicable are sometimes, on a
sudden, made honourable (v. 7, 8): He raises up the poor out of the
dust, that he may set him with princes. First, Thus God does sometimes
magnify himself, and his own wisdom, power, and sovereignty. When he has
some great work to do he chooses to employ those in it that were least
likely, and least thought of for it by themselves or others, to the
highest post of honour: Gideon is fetched from threshing, Saul from
seeking the asses, and David from keeping the sheep; the apostles are
sent from fishing to be fishers of men. The treasure of the gospel is
put into earthen vessels, and the weak and foolish ones of the world are
pitched upon to be preachers of it, to confound the wise and mighty (1
Co. 1:27, 28), that the excellency of the power may be of God, and all
may see that promotion comes from him. Secondly, Thus God does sometimes
reward the eminent piety and patience of his people who have long
groaned under the burden of poverty and disgrace. When Joseph\'s virtue
was tried and manifested he was raised from the prison-dust and set with
princes. Those that are wise will observe such returns of Providence,
and will understand by them the loving-kindness of the Lord. Some have
applied this to the work of redemption by Jesus Christ, and not unfitly;
for through him poor fallen men are raised out of the dust (one of the
Jewish rabbies applies it to the resurrection of the dead), nay, out of
the dunghill of sin, and set among princes, among angels, those princes
of his people. Hannah had sung to this purport, 1 Sa. 2:6-8. `[2.]`
Those that have been long barren are sometimes, on a sudden, made
fruitful, v. 9. This may look back to Sarah and Rebecca, Rachel, Hannah,
and Samson\'s mother, or forward to Elizabeth; and many such instances
there have been, in which God has looked on the affliction of his
handmaids and taken away their reproach. He makes the barren woman to
keep house, not only builds up the family, but thereby finds the heads
of the family something to do. Note, Those that have the comfort of a
family must take the care of it; bearing children and guiding the house
are put together, 1 Tim. 5:14. When God sets the barren in a family he
expects that she should look well to the ways of her household, Prov.
31:27. She is said to be a joyful mother of children, not only because,
even in common cases, the pain is forgotten, for joy that a man-child is
born into the world, but there is particular joy when a child is born to
those that have been long childless (as Lu. 1:14) and therefore there
ought to be particular thanksgiving. Praise you the Lord. Yet, in this
case, rejoice with trembling; for, though the sorrowful mother be made
joyful, the joyful mother may be made sorrowful again, if the children
be either removed from her or embittered to her. This, therefore, may be
applied to the gospel-church among the Gentiles (the building of which
is illustrated by this similitude, Isa. 54:1, Sing, O barren! thou that
didst not bear, and Gal. 4:27), for which we, who, being sinners of the
Gentiles, are children of the desolate, have reason to say, Praise you
the Lord.
