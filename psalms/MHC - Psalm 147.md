Psalm 147
=========

Commentary
----------

This is another psalm of praise. Some think it was penned after the
return of the Jews from their captivity; but it is so much of a piece
with Ps. 145 that I rather think it was penned by David, and what is
said (v. 2, 13) may well enough be applied to the first building and
fortifying of Jerusalem in his time, and the gathering in of those that
had been out-casts in Saul\'s time. The Septuagint divides it into two;
and we may divide it into the first and second part, but both of the
same import. `I.` We are called upon to praise God (v. 1, 7, 12). `II.` We
are furnished with matter for praise, for God is to be glorified, 1. As
the God of nature, and so he is very great (v. 4, 5, 8, 9, 15-18). 2. As
the God of grace, comforting his people (v. 3, 6, 10, 11). 3. As the God
of Israel, Jerusalem, and Zion, settling their civil state (v. 2, 13,
14), and especially settling religion among them (v. 19, 20). It is
easy, in singing this psalm, to apply it to ourselves, both as to
personal and national mercies, were it but as easy to do so with
suitable affections.

### Verses 1-11

Here, `I.` The duty of praise is recommended to us. It is not without
reason that we are thus called to it again and again: Praise you the
Lord (v. 1), and again (v. 7), Sing unto the Lord with thanksgiving,
sing praise upon the harp to our God (let all our praises be directed to
him and centre in him), for it is good to do so; it is our duty, and
therefore good in itself; it is our interest, and therefore good for us.
It is acceptable to our Creator and it answers the end of our creation.
The law for it is holy, just, and good; the practice of it will turn to
a good account. It is good, for 1. It is pleasant. Holy joy or delight
are required as the principle of it, and that is pleasant to us as men;
giving glory to God is the design and business of it, and that is
pleasant to us as saints that are devoted to his honour. Praising God is
work that is its own wages; it is heaven upon earth; it is what we
should be in as in our element. 2. It is comely; it is that which
becomes us as reasonable creatures, much more as people in covenant with
God. In giving honour to God we really do ourselves a great deal of
honour.

`II.` God is recommended to us as the proper object of our most exalted
and enlarged praises, upon several accounts.

`1.` The care he takes of his chosen people, v. 2. Is Jerusalem to be
raised out of small beginnings? Is it to be recovered out of its ruins?
In both cases, The Lord builds up Jerusalem. The gospel-church, the
Jerusalem that is from above, is of this building. He framed the model
of it in his own counsels; he founded it by the preaching of his gospel;
he adds to it daily such as shall be saved, and so increases it. He will
build it up unto perfection, build it up as high as heaven. Are any of
his people outcasts? Have they made themselves so by their own folly? He
gathers them by giving them repentance and bringing them again into the
communion of saints. Have they been forced out by war, famine, or
persecution? He opens a door for their return; many that were missing,
and thought to be lost, are brought back, and those that were scattered
in the cloudy and dark day are gathered together again.

`2.` The comforts he has laid up for true penitents, v. 3. They are
broken in heart, and wounded, humbled, and troubled, for sin, inwardly
pained at the remembrance of it, as a man is that is sorely wounded.
Their very hearts are not only pricked, but rent, under the sense of the
dishonour they have done to God and the injury they have done to
themselves by sin. To those whom God heals with the consolations of his
Spirit he speaks peace, assures them that their sins are pardoned and
that he is reconciled to them, and so makes them easy, pours the balm of
Gilead into the bleeding wounds, and then binds them up, and makes them
to rejoice. Those who have had experience of this need not be called
upon to praise the Lord; for when he brought them out of the horrible
pit, and set their feet upon a rock, he put a new song into their
mouths, Ps. 40:2, 3. And for this let others praise him also.

`3.` The sovereign dominion he has over the lights of heaven, v. 4, 5.
The stars are innumerable, many of them being scarcely discernible with
the naked eye, and yet he counts them, and knows the exact number of
them, for they are all the work of his hands and the instruments of his
providence. Their bulk and power are very great; but he calleth them all
by their names, which shows his dominion over them and the command he
has them at, to make what use of them he pleases. They are his servants,
his soldiers; he musters them, he marshals them; they come and go at his
bidding, and all their motions are under his direction. He mentions this
as one instance of many, to show that great is our Lord and of great
power (he can do what he pleases), and of his understanding there is no
computation, so that he can contrive every thing for the best. Man\'s
knowledge is soon drained, and you have his utmost length; hitherto his
wisdom can reach and no further. But God\'s knowledge is a depth that
can never be fathomed.

`4.` The pleasure he takes in humbling the proud and exalting those of
low degree (v. 6): The Lord lifts up the meek, who abase themselves
before him, and whom men trample on; but the wicked, who conduct
themselves insolently towards God and scornfully towards all mankind,
who lift up themselves in pride and folly, he casteth down to the
ground, sometimes by very humbling providences in this world, at
furthest in the day when their faces shall be filled with everlasting
shame. God proves himself to be God by looking on the proud and abasing
them, Job 40:12.

`5.` The provision he makes for the inferior creatures. Though he is so
great as to command the stars, he is so good as not to forget even the
fowls, v. 8, 9. Observe in what method he feeds man and beast. `(1.)` He
covereth the heaven with clouds, which darken the air and intercept the
beams of the sun, and yet in them he prepareth that rain for the earth
which is necessary to its fruitfulness. Clouds look melancholy, and yet
without them we could have no rain and consequently no fruit. Thus
afflictions, for the present, look black, and dark, and unpleasant, and
we are in heaviness because of them, as sometimes when the sky is
overcast it makes us dull; but they are necessary, for from these clouds
of affliction come those showers that make the harvest to yield the
peaceable fruits of righteousness (Heb. 12:11), which should help to
reconcile us to them. Observe the necessary dependence which the earth
has upon the heavens, which directs us on earth to depend on God in
heaven. All the rain with which the earth is watered is of God\'s
preparing. `(2.)` By the rain which distils on the earth he makes grass to
grow upon the mountains, even the high mountains, which man neither
takes care of nor reaps the benefit of. The mountains, which are not
watered with the springs and rivers, as the valleys are, are yet watered
so that they are not barren. `(3.)` This grass he gives to the beast for
his food, the beast of the mountains which runs wild, which man makes no
provision for. And even the young ravens, which, being forsaken by their
old ones, cry, are heard by him, and ways are found to feed them, so
that they are kept from perishing in the nest.

`6.` The complacency he takes in his people, v. 10, 11. In times when
great things are doing, and there are great expectations of the success
of them, it concerns us to know (since the issue proceeds from the Lord)
whom, and what, God will delight to honour and crown with victory. It is
not the strength of armies, but the strength of grace, that God is
pleased to own. `(1.)` Not the strength of armies-not in the cavalry, for
he delighteth not in the strength of the horse, the war-horse, noted for
his courage (Job 39:19,. etc.)-nor in the infantry, for he taketh no
pleasure in the legs of a man; he does not mean the swiftness of them
for flight, to quit the field, but the steadiness of them for charging,
to stand the ground. If one king, making war with another king, goes to
God to pray for success, it will not avail him to plead, \"Lord, I have
a gallant army, the horse and foot in good order; it is a pity that they
should suffer any disgrace;\" for that is no argument with God, Ps.
20:7. Jehoshaphat\'s was much better: Lord, we have no might, 2 Chr.
20:12. But, `(2.)` God is pleased to own the strength of grace. A serious
and suitable regard to God is that which is, in the sight of God, of
great price in such a case. The Lord accepts and takes pleasure in those
that fear him and that hope in his mercy. Observe, `[1.]` A holy fear of
God and hope in God not only may consist, but must concur. In the same
heart, at the same time, there must be both a reverence of his majesty
and a complacency in his goodness, both a believing dread of his wrath
and a believing expectation of his favour; not that we must hang in
suspense between hope and fear, but we must act under the gracious
influences of hope and fear. Our fear must save our hope from swelling
into presumption, and our hope must save our fear from sinking into
despair; thus must we take our work before us. `[2.]` We must hope in
God\'s mercy, his general mercy, even when we cannot find a particular
promise to stay ourselves upon. A humble confidence in the goodness of
God\'s nature is very pleasing to him, as that which turns to the glory
of that attribute in which he most glories. Every man of honour loves to
be trusted.

### Verses 12-20

Jerusalem, and Zion, the holy city, the holy hill, are here called upon
to praise God, v. 12. For where should praise be offered up to God but
where his altar is? Where may we expect that glory should be given to
him but in the beauty of holiness? Let the inhabitants of Jerusalem
praise the Lord in their own houses; let the priests and Levites, who
attend in Zion, the city of their solemnities, in a special manner
praise the Lord. They have more cause to do it than others, and they lie
under greater obligations to do it than others; for it is their
business, it is their profession. \"Praise thy God, O Zion! he is thine,
and therefore thou art bound to praise him; his being thine includes all
happiness, so that thou canst never want matter for praise.\" Jerusalem
and Zion must praise God,

`I.` For the prosperity and flourishing state of their civil interests, v.
13, 14. 1. For their common safety. They had gates, and kept their gates
barred in times of danger; but that would not have been an effectual
security to them if God had not strengthened the bars of their gates and
fortified their fortifications. The most probable means we can devise
for our own preservation will not answer the end, unless God give his
blessing with them; we must therefore in the careful and diligent use of
those means, depend upon him for that blessing, and attribute the
undisturbed repose of our land more to the wall of fire than to the wall
of water round about us, Zec. 2:5. 2. For the increase of their people.
This strengthens the bars of the gates as much as any thing: He hath
blessed thy children within thee, with that first and great blessing, Be
fruitful, and multiply, and replenish the land. It is a comfort to
parents to see their children blessed of the Lord (Isa. 61:9), and a
comfort to the generation that is going off to see the rising generation
numerous and hopeful, for which blessing God must be blessed. 3. For the
public tranquillity, that they were delivered from the terrors and
desolations of war: He makes peace in thy borders, by putting an end to
the wars that were, and preventing the wars that were threatened and
feared. He makes peace within thy borders, that is, in all parts of the
country, by composing differences among neighbours, that there may be no
intestine broils and animosities, and upon thy borders, that they may
not be attacked by invasions from abroad. If there be trouble any where,
it is in the borders, the marches of a country; the frontier-towns lie
most exposed, so that, if there be peace in the borders, there is a
universal peace, a mercy we can never be sufficiently thankful for. 4.
For great plenty, the common effect of peace: He filleth thee with the
finest of the wheat-wheat, the most valuable grain, the fat, the finest
of that, and a fulness thereof. What would they more? Canaan abounded
with the best wheat (Deu. 32:14) and exported it to the countries
abroad, as appears, Eze. 27:17. The land of Israel was not enriched with
precious stones nor spices, but with the finest of the wheat, with
bread, which strengthens man\'s heart. This made it the glory of all
lands, and for this God was praised in Zion.

`II.` For the wonderful instances of his power in the weather,
particularly the winter-weather. He that protects Zion and Jerusalem is
that God of power from whom all the powers of nature are derived and on
whom they depend, and who produces all the changes of the seasons,
which, if they were not common, would astonish us.

`1.` In general, whatever alterations there are in this lower world (and
it is that world that is subject to continual changes) they are produced
by the will, and power, and providence of God (v. 15): He sendeth forth
his commandment upon earth, as one that has an incontestable authority
to give orders, and innumerable attendants ready to carry his orders and
put them in execution. As the world was at first made, so it is still
upheld and governed, by a word of almighty power. God speaks and it is
done, for all are his servants. That word takes effect, not only surely,
but speedily. His word runneth very swiftly, for nothing can oppose or
retard it. As the lightning, which passes through the air in an instant,
such is the word of God\'s providence, and such the word of his grace,
when it is sent forth with commission, Lu. 17:24. Angels, who carry his
word and fulfil it, fly swiftly, Dan. 9:21.

`2.` In particular, frosts and thaws are both of them wonderful changes,
and in both we must acknowledge the word of his power.

`(1.)` Frosts are from God. With him are the treasures of the snow and the
hail (Job 38:22, 23), and out of these treasures he draws as he pleases.
`[1.]` He giveth snow like wool. It is compared to wool for its
whiteness (Isa. 1:18), and its softness; it falls silently, and makes no
more noise than the fall of a lock of wool; it covers the earth, and
keeps it warm like a fleece of wool, and so promotes its fruitfulness.
See how God can work by contraries, and bring meat out of the eater, can
warm the earth with cold snow. `[2.]` He scatters the hoar-frost, which
is dew congealed, as the snow and hail are rain congealed. This looks
like ashes scattered upon the grass, and is sometimes prejudicial to the
products of the earth and blasts them as if it were hot ashes, Ps.
78:47. `[3.]` He casts forth his ice like morsels, which may be
understood either of large hail-stones, which are as ice in the air, or
of the ice which covers the face of the waters, and when it is broken,
though naturally it was as drops of drink, it is as morsels of meat, or
crusts of bread. `[4.]` When we see the frost, and snow, and ice, we
feel it in the air: Who can stand before his cold? The beasts cannot;
they retire into dens (Job 37:8); they are easily conquered then, 2 Sa.
23:20. Men cannot, but are forced to protect themselves by fires, or
furs, or both, and all little enough where and when the cold is in
extremity. We see not the causes when we feel the effects; and therefore
we must call it his cold; it is of his sending, and therefore we must
bear it patiently, and be thankful for warm houses, and clothes, and
beds, to relieve us against the rigour of the season, and must give him
the glory of his wisdom and sovereignty, his power and faithfulness,
which shall not cease any more than summer, Gen. 8:22. And let us also
infer from it, If we cannot stand before the cold of his frosts, how can
we stand before the heat of his wrath?

`(2.)` Thaws are from God. When he pleases (v. 18) he sends out his word
and melts them; the frost, the snow, the ice, are all dissolved quickly,
in order to which he causes the wind, the south wind, to blow, and the
waters, which were frozen, flow again as they did before. We are soon
sensible of the change, but we see not the causes of it, but must
resolve it into the will of the First Cause. And in it we must take
notice not only of the power of God, that he can so suddenly, so
insensibly, make such a great and universal alteration in the temper of
the air and the face of the earth (what cannot he do that does this
every winter, perhaps often every winter?) but also of the goodness of
God. Hard weather does not always continue; it would be sad if it
should. He does not contend for ever, but renews the face of the earth.
As he remembered Noah, and released him (Gen. 8:1), so he remembers the
earth, and his covenant with the earth, Cant. 2:11, 12. This thawing
word may represent the gospel of Christ, and this thawing wind the
Spirit of Christ (for the Spirit is compared to the wind, Jn. 3:8); both
are sent for the melting of frozen souls. Converting grace, like the
thaw, softens the heart that was hard, moistens it, and melts it into
tears of repentance; it warms good affections, and makes them to flow,
which, before, were chilled and stopped up. The change which the thaw
makes is universal and yet gradual; it is very evident, and yet how it
is done is unaccountable: such is the change wrought in the conversion
of a soul, when God\'s word and Spirit are sent to melt it and restore
it to itself.

`III.` For his distinguishing favour to Israel, in giving them his word
and ordinances, a much more valuable blessing than their peace and
plenty (v. 14), as much as the soul is more excellent than the body.
Jacob and Israel had God\'s statutes and judgments among them. They were
under his peculiar government; the municipal laws of their nation were
of his framing and enacting, and their constitution was a theocracy.
They had the benefit of divine revelation; the great things of God\'s
law were written to them. They had a priesthood of divine institution
for all things pertaining to God, and prophets for all extraordinary
occasions. No people besides went upon sure grounds in their religion.
Now this was, 1. A preventing mercy. They did not find out God\'s
statutes and judgments of themselves, but God showed his word unto
Jacob, and by that word he made known to them his statutes and
judgments. It is a great mercy to any people to have the word of God
among them; for faith comes by hearing and reading that word, that faith
without which it is impossible to please God. 2. A distinguishing mercy,
and upon that account the more obliging: \"He hath not dealt so with
every nation, not with any nation; and, as for his judgments, they have
not known them, nor are likely to know them till the Messiah shall come
and take down the partition-wall between Jew and Gentile, that the
gospel may be preached to every creature.\" Other nations had plenty of
outward good things; some nations were very rich, others had pompous
powerful princes and polite literature, but none were blessed with
God\'s statutes and judgments as Israel were. Let Israel therefore
praise the Lord in the observance of these statutes. Lord, how is it
that thou wilt manifest thyself to us, and not to the world! Even so,
Father, because it seemed good in thy eyes.
