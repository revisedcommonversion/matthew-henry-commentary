Psalm 81
========

Commentary
----------

This psalm was penned, as is supposed, not upon occasion of any
particular providence, but for the solemnity of a particular ordinance,
either that of the new-moon in general or that of the feast of trumpets
on the new moon of the seventh month, Lev. 23:24; Num. 29:1. When David,
by the Spirit, introduced the singing of psalms into the temple-service
this psalm was intended for that day, to excite and assist the proper
devotions of it. All the psalms are profitable; but, if one psalm be
more suitable than another to the day and observances of it, we should
choose that. The two great intentions of our religious assemblies, and
which we ought to have in our eye in our attendance on them, are
answered in this psalm, which are, to give glory to God and to receive
instruction from God, to \"behold the beauty of the Lord and to enquire
in his temple;\" accordingly by this psalm we are assisted on our solemn
feast days, `I.` In praising God for what he is to his people (v. 1-3),
and has done for them (v. 4-7). `II.` In teaching and admonishing one
another concerning the obligations we lie under to God (v. 8-10), the
danger of revolting from him (v. 11, 12), and the happiness we should
have if we would but keep close to him (v. 13-16). This, though spoken
primarily of Israel of old, is written for our learning, and is
therefore to be sung with application.

To the chief musician upon Gittith. A psalm of Asaph.

### Verses 1-7

When the people of God were gathered together in the solemn day, the day
of the feast of the Lord, they must be told that they had business to
do, for we do not go to church to sleep nor to be idle; no, there is
that which the duty of every day requires, work of the day, which is to
be done in its day. And here,

`I.` The worshippers of God are excited to their work, and are taught, by
singing this psalm, to stir up both themselves and one another to it, v.
1-3. Our errand is, to give unto God the glory due unto his name, and in
all our religious assemblies we must mind this as our business. 1. In
doing this we must eye God as our strength, and as the God of Jacob, v.
`1.` He is the strength of Israel, as a people; for he is a God in
covenant with them, who will powerfully protect, support, and deliver
them, who fights their battles and makes them do valiantly and
victoriously. He is the strength of every Israelite; by his grace we are
enabled to go through all our services, sufferings, and conflicts; and
to him, as our strength, we must pray, and we must sing praise to him as
the God of all the wrestling seed of Jacob, with whom we have a
spiritual communion. 2. We must do this by all the expressions of holy
joy and triumph. It was then to be done by musical instruments, the
timbrel, harp, and psaltery; and by blowing the trumpet, some think in
remembrance of the sound of the trumpet on Mount Sinai, which waxed
louder and louder. It was then and is now to be done by singing psalms,
singing aloud, and making a joyful noise. The pleasantness of the harp
and the awfulness of the trumpet intimate to us that God is to be
worshipped with cheerfulness and joy with reverence and godly fear.
Singing aloud and making a noise intimate that we must be warm and
affectionate in praising God, that we must with a hearty good-will show
forth his praise, as those that are not ashamed to own our dependence on
him and obligations to him, and that we should join many together in
this work; the more the better; it is the more like heaven. 3. This must
be done in the time appointed. No time is amiss for praising God (Seven
times a day will I praise thee; nay, at midnight will I rise and give
thanks unto thee); but some are times appointed, not for God to meet us
(he is always ready), but for us to meet one another, that we may join
together in praising Do. The solemn feast-day must be a day of praise;
when we are receiving the gifts of God\'s bounty, and rejoicing in them,
then it is proper to sing his praises.

`II.` They are here directed in their work. 1. They must look up to the
divine institution which it is the observation of. In all religious
worship we must have an eye to the command (v. 4): This was a statute
for Israel, for the keeping up of a face of religion among them; it was
a law of the God of Jacob, which all the seed of Jacob are bound by, and
must be subject to. Note, Praising God is not only a good thing, which
we do well to do, but it is our indispensable duty, which we are obliged
to do; it is at our peril if we neglect it; and in all religious
exercises we must have an eye to the institution as our warrant and
rule: \"This I do because God has commanded me; and therefore I hope he
will accept me;\" then it is done in faith. 2. They must look back upon
those operations of divine Providence which it is the memorial of. This
solemn service was ordained for a testimony (v. 5), a standing
traditional evidence, for the attesting of the matters of fact. It was a
testimony to Israel, that they might know and remember what God had done
for their fathers, and would be a testimony against them if they should
be ignorant of them and forget them. `(1.)` The psalmist, in the people\'s
name, puts himself in mind of the general work of God on Israel\'s
behalf, which was kept in remembrance by this and other solemnities, v.
5. When God went out against the land of Egypt, to lay it waste, that he
might force Pharaoh to let Israel go, then he ordained solemn feast-days
to be observed by a statute for ever in their generations, as a memorial
of it, particularly the passover, which perhaps is meant by the solemn
feast-day (v. 3); that was appointed just then when God went out through
the land of Egypt to destroy the first-born, and passed over the houses
of the Israelites, Ex. 12:23, 24. By it that work of wonder was to be
kept in perpetual remembrance, that all ages might in it behold the
goodness and severity of God. The psalmist, speaking for his people,
takes notice of this aggravating circumstance of their slavery in Egypt
that there they heard a language that they understood not; there they
were strangers in a strange land. The Egyptians and the Hebrews
understood not one another\'s language; for Joseph spoke to his brethren
by an interpreter (Gen. 42:23), and the Egyptians are said to be to the
house of Jacob a people of a strange language, Ps. 114:1. To make a
deliverance appear the more gracious, the more glorious, it is good to
observe every thing that makes the trouble we are delivered from appear
the more grievous. `(2.)` The psalmist, in God\'s name, puts the people in
mind of some of the particulars of their deliverance. Here he changes
the person, v. 6. God speaks by him, saying, I removed the shoulder from
the burden. Let him remember this on the feast-day, `[1.]` That God had
brought them out of the house of bondage, had removed their shoulder
from the burden of oppression under which they were ready to sink, had
delivered their hands from the pots, or panniers, or baskets, in which
they carried clay or bricks. Deliverance out of slavery is a very
sensible mercy and one which ought to be had in everlasting remembrance.
But this was not all. `[2.]` God had delivered them at the Red Sea; then
they called in trouble, and he rescued them and disappointed the designs
of their enemies against them, Ex. 14:10. Then he answered them with a
real answer, out of the secret place of thunder; that is, out of the
pillar of fire, through which God looked upon the host of the Egyptians
and troubled it, Ex. 14:24, 25. Or it may be meant of the giving of the
law at Mount Sinai, which was the secret place, for it was death to gaze
(Ex. 19:21), and it was in thunder that God then spoke. Even the terrors
of Sinai were favours to Israel, Deu. 4:33. `[3.]` God had borne their
manners in the wilderness: \"I proved thee at the waters of Meribah;
thou didst there show thy temper, what an unbelieving murmuring people
thou wast, and yet I continued my favour to thee.\" Selah-Mark that;
compare God\'s goodness and man\'s badness, and they will serve as foils
to each other. Now if they, on their solemn feast-days, were thus to
call to mind their redemption out of Egypt, much more ought we, on the
Christian sabbath, to call to mind a more glorious redemption wrought
out for us by Jesus Christ from worse than Egyptian bondage, and the
many gracious answers he has given to us, notwithstanding our manifold
provocations.

### Verses 8-16

God, by the psalmist, here speaks to Israel, and in them to us, on whom
the ends of the world are come.

`I.` He demands their diligent and serious attention to what he was about
to say (v. 8): \"Hear, O my people! and who should hear me if my people
will not? I have heard and answered thee; now wilt thou hear me? Hear
what is said with the greatest solemnity and the most unquestionable
certainty, for it is what I will testify unto thee. Do not only give me
the hearing, but hearken unto me, that is, be advised by me, be ruled by
me.\" Nothing could be more reasonably nor more justly expected, and yet
God puts an if upon it: \"If thou wilt hearken unto me. It is thy
interest to do so, and yet it is questionable whether thou wilt or no;
for thy neck is an iron sinew.\"

`II.` He puts them in mind of their obligation to him as the Lord their
God and Redeemer (v. 10): I am the Lord thy God, who brought thee out of
the land of Egypt; this is the preface to the ten commandments, and a
powerful reason for the keeping of them, showing that we are bound to it
in duty, interest, and gratitude, all which bonds we break asunder if we
be disobedient.

`III.` He gives them an abstract both of the precepts and of the promises
which he gave them, as the Lord and their God, upon their coming out of
Egypt. 1. The great command was that they should have no other gods
before him (v. 9): There shall no strange god be in thee, none besides
thy own God. Other gods might well be called strange gods, for it was
very strange that ever any people who had the true and living God for
their God should hanker after any other. God is jealous in this matter,
for he will not suffer his glory to be given to another; and therefore
in this matter they must be circumspect, Ex. 23:13. 2. The great promise
was that God himself, as a God all-sufficient, would be nigh unto them
in all that which they called upon him for (Deu. 4:7), that, if they
would adhere to him as their powerful protector and ruler, they should
always find him their bountiful benefactor: \"Open thy mouth wide and I
will fill it, as the young ravens that cry open their mouths wide and
the old ones fill them.\" See here, `(1.)` What is our duty-to raise our
expectations from God and enlarge our desires towards him. We cannot
look for too little from the creature nor too much from the Creator. We
are not straitened in him; why therefore should we be straitened in our
own bosoms? `(2.)` What is God\'s promise. I will fill thy mouth with good
things, Ps. 103:5. There is enough in God to fill our treasures (Prov.
8:21), to replenish every hungry soul (Jer. 31:25), to supply all our
wants, to answer all our desires, and to make us completely happy. The
pleasures of sense will surfeit and never satisfy (Isa. 55:2); divine
pleasures will satisfy and never surfeit. And we may have enough from
God if we pray for it in faith. Ask, and it shall be given you. He gives
liberally, and upbraids not. God assured his people Israel that it would
be their own fault if he did not do as great and kind things for them as
he had done for their fathers. Nothing should be thought too good, too
much, to give them, if they would but keep close to God. He would
moreover have given them such and such things, 2 Sa. 12:8.

`IV.` He charges them with a high contempt of his authority as their
lawgiver and his grace and favour as their benefactor, v. 11. He had
done much for them, and designed to do more; but all in vain: \"My
people would not hearken to my voice, but turned a deaf ear to all I
said.\" Two things he complains of:-1. Their disobedience to his
commands. They did hear his voice, so as never any people did; but they
would not hearken to it, they would not be ruled by it, neither by the
law nor by the reason of it. 2. Their dislike of his covenant-relation
to them: They would none of me. They acquiesced not in my word (so the
Chaldee); God was willing to be to them a God, but they were not willing
to be to him a people; they did not like his terms. \"I would have
gathered them, but they would not.\" They had none of him; and why had
they not? It was not because they might not; they were fairly invited
into covenant with God. It was not because they could not; for the word
was nigh them, even in their mouth and in their heart. But it was purely
because they would not. God calls them hi people, for they were bought
by him, bound to him, his by a thousand ties, and yet even they had not
hearkened, had not obeyed. \"Israel, the seed of Jacob my friend, set me
at nought, and would have none of me.\" Note, All the wickedness of the
wicked world is owing to the wilfulness of the wicked will. The reason
why people are not religious is because they will not be so.

`V.` He justifies himself with this in the spiritual judgments he had
brought upon them (v. 12): So I gave them up unto their own hearts\'
lusts, which would be more dangerous enemies and more mischievous
oppressors to them than any of the neighbouring nations ever were. God
withdrew his Spirit from them, took off the bridle of restraining grace,
left them to themselves, and justly; they will do as they will, and
therefore let them do as they will. Ephraim is joined to idols; let him
alone. It is a righteous thing with God to give those up to their own
hearts\' lusts that indulge them, and give up themselves to be led by
them; for why should his Spirit always strive? His grace is his own, and
he is debtor to no man, and yet, as he never gave his grace to any that
could say they deserved it, so he never took it away from any but such
as had first forfeited it: They would none of me, so I gave them up; let
them take their course. And see what follows: They walked in their own
counsels, in the way of their heart and in the sight of their eye, both
in their worships and in their conversations. \"I left them to do as
they would, and then they did all that was ill;\" they walked in their
own counsels, and not according to the counsels of God and his advice.
God therefore was not the author of their sin; he left them to the lusts
of their own hearts and the counsels of their own heads; if they do not
well, the blame must lie upon their own hearts and the blood upon their
own heads.

`VI.` He testifies his good-will to them in wishing they had done well
for themselves. He saw how sad their case was, and how sure their ruin,
when they were delivered up to their own lusts; that is worse than being
given up to Satan, which may be in order to reformation (1 Tim. 1:20)
and to salvation (1 Co. 5:5); but to be delivered up to their own
hearts\' lusts is to be sealed under condemnation. He that is filthy,
let him be filthy still. What fatal precipices will not these hurry a
man to! Now here God looks upon them with pity, and shows that it was
with reluctance that he thus abandoned them to their folly and fate. How
shall I give thee up, Ephraim? Hos. 11:8, 9. So here, O that my people
had hearkened! See Isa. 48:18. Thus Christ lamented the obstinacy of
Jerusalem. If thou hadst known, Lu. 19:42. The expressions here are very
affecting (v. 13-16), designed to show how unwilling God is that any
should perish and desirous that all should come to repentance (he
delights not in the ruin of sinful persons or nations), and also what
enemies sinners are to themselves and what an aggravation it will be of
their misery that they might have been happy upon such easy terms.
Observe here,

`1.` The great mercy God had in store for his people, and which he would
have wrought for them if they had been obedient. `(1.)` He would have
given them victory over their enemies and would soon have completed the
reduction of them. They should not only have kept their ground, but have
gained their point, against the remaining Canaanites, and their
encroaching vexatious neighbours (v. 14): I should have subdued their
enemies; and it is God only that is to be depended on for the subduing
of our enemies. Not would had have put them to the expense and fatigue
of a tedious war: he would soon have done it; for he would have turned
his hand against their adversaries, and then they would not have been
able to stand before them. It intimates how easily he would have done it
and without any difficulty. With the turn of a hand, nay, with the
breath of his mouth, shall he slay the wicked, Isa. 11:4. If he but turn
his hand, the haters of the Lord will submit themselves to him (v. 15);
and, though they are not brought to love him, yet they shall be made to
fear him and to confess that he is too hard for them and that it is in
vain to contend with him. God is honoured, and so is his Israel, by the
submission of those that have been in rebellion against them, though it
be but a forced and feigned submission. `(2.)` He would have confirmed and
perpetuated their posterity, and established it upon sure and lasting
foundations. In spite of all the attempts of their enemies against them,
their time should have endured for ever, and they should never have been
disturbed in the possession of the good land God had given them, much
less evicted and turned out of possession. `(3.)` He would have given them
great plenty of all good things (v. 16): He should have fed them with
the finest of the wheat, with the best grain and the best of the kind.
Wheat was the staple commodity of Canaan, and they exported a great deal
of it, Eze. 27:17. He would not only have provided for them the best
sort of bread, but with honey out of the rock would he have satisfied
them. Besides the precious products of the fruitful soil, that there
might not be a barren spot in all their land, even the clefts of the
rock should serve for bee-hives and in them they should find honey in
abundance. See Deu. 32:13, 14. In short, God designed to make them every
way easy and happy.

`2.` The duty God required from them as the condition of all this mercy.
He expected no more than that they should hearken to him, as a scholar
to his teacher, to receive his instructions-as a servant to his master,
to receive his commands; and that they should walk in his ways, those
ways of the Lord which are right and pleasant, that they should observe
the institutions of his ordinances and attend the intimations of his
providence. There was nothing unreasonable in this.

`3.` Observe how the reason of the withholding of the mercy is laid in
their neglect of the duty: If they had hearkened to me, I would soon
have subdued their enemies. National sin or disobedience is the great
and only thing that retards and obstructs national deliverance. When I
would have healed Israel, and set every thing to-rights among them, then
the iniquity of Ephraim was discovered, and so a stop was put to the
cure, Hos. 7:1. We are apt to say, \"If such a method had been taken,
such an instrument employed, we should soon have subdued our enemies:\"
but we mistake; if we had hearkened to God, and kept to our duty, the
thing would have been done, but it is sin that makes our troubles long
and salvation slow. And this is that which God himself complains of, and
wishes it had been otherwise. Note, Therefore God would have us do our
duty to him, that we may be qualified to receive favour from him. He
delights in our serving him, not because he is the better for it, but
because we shall be.
