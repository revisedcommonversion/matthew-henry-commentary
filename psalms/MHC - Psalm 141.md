Psalm 141
=========

Commentary
----------

David was in distress when he penned this psalm, pursued, it is most
likely, by Saul, that violent man. Is any distressed? Let him pray;
David did so, and had the comfort of it. `I.` He prays for God\'s
favourable acceptance (v. 1, 2). `II.` For his powerful assistance (v. 3,
4). `III.` That others might be instrumental of good to his soul, as he
hoped to be to the souls of others (v. 5, 6). `IV.` That he and his
friends being now brought to the last extremity God would graciously
appear for their relief and rescue (v. 7-10). The mercy and grace of God
are as necessary to us as they were to him, and therefore we should be
humbly earnest for them in singing this psalm.

A psalm of David.

### Verses 1-4

Mercy to accept what we do well, and grace to keep us from doing ill,
are the two things which we are here taught by David\'s example to pray
to God for.

`I.` David loved prayer, and he begs of God that his prayers might be
heard and answered, v. 1, 2. David cried unto God. His crying denotes
fervency in prayer; he prayed as one in earnest. His crying to God
denotes faith and fixedness in prayer. And what did he desire as the
success of his prayer? 1. That God would take cognizance of it: \"Give
ear to my voice; let me have a gracious audience.\" Those that cry in
prayer may hope to be heard in prayer, not for their loudness, but their
liveliness. 2. That he would visit him upon it: Make haste unto me.
Those that know how to value God\'s gracious presence will be
importunate for it and humbly impatient of delays. He that believes does
not make haste, but he that prays may be earnest with God to make haste.
3. That he would be well pleased with him in it, well pleased with his
praying and the lifting up of his hands in prayer, which denotes both
the elevation and enlargement of his desire and the out-goings of his
hope and expectation, the lifting up of the hand signifying the lifting
up of the heart, and being used instead of lifting up the sacrifices
which were heaved and waved before the Lord. Prayer is a spiritual
sacrifice; it is the offering up of the soul, and its best affections,
to God. Now he prays that this may be set forth and directed before God
as the incense which was daily burnt upon the golden altar, and as the
evening sacrifice, which he mentions rather than the morning sacrifice,
perhaps because this was an evening prayer, or with an eye to Christ,
who, in the evening of the world and in the evening of the day, was to
offer up himself a sacrifice of atonement, and establish the spiritual
sacrifices of acknowledgement, having abolished all the carnal
ordinances of the law. Those that pray in faith may expect it will
please God better than an ox or bullock. David was now banished from
God\'s court, and could not attend the sacrifice and incense, and
therefore begs that his prayer might be instead of them. Note, Prayer is
of a sweet-smelling savour to God, as incense, which yet has no savour
without fire; nor has prayer without the fire of holy love and fervour.

`II.` David was in fear of sin, and he begs of God that he might be kept
from sin, knowing that his prayers would not be accepted unless he took
care to watch against sin. We must be as earnest for God\'s grace in us
as for his favour towards us. 1. He prays that he might not be surprised
into any sinful words (v. 3): \"Set a watch, O Lord! before my mouth,
and, nature having made my lips to be a door to my words, let grace keep
that door, that no word may be suffered to go out which may in any way
tend to the dishonour of God or the hurt of others.\" Good men know the
evil of tongue-sins, and how prone they are to them (when enemies are
provoking we are in danger of carrying our resentment too far, and of
speaking unadvisedly, as Moses did, though the meekest of men), and
therefore they are earnest with God to prevent their speaking amiss, as
knowing that no watchfulness or resolution of their own is sufficient
for the governing of their tongues, much less of their hearts, without
the special grace of God. We must keep our mouths as with a bridle; but
that will not serve: we must pray to God to keep them. Nehemiah prayed
to the Lord when he set a watch, and so must we, for without him the
watchman walketh but in vain. 2. That he might not be inclined to any
sinful practices (v. 4): \"Incline not my heart to any evil thing;
whatever inclination there is in me to sin, let it be not only
restrained, but mortified, by divine grace.\" The example of those about
us, and the provocations of those against us, are apt to stir up and
draw out corrupt inclinations. We are ready to do as others do, and to
think that if we have received injuries we may return them; and
therefore we have need to pray that we may never be left to ourselves to
practise any wicked work, either in confederacy with or in opposition to
the men that work iniquity. While we live in such an evil world, and
carry about with us such evil hearts, we have need to pray that we may
neither be drawn in by any allurement nor driven on by any provocation
to do any sinful thing. 3. That he might not be ensnared by any sinful
pleasures: \"Let me not eat of their dainties. Let me not join with them
in their feasts and sports, lest thereby I be inveigled into their
sins.\" Better is a dinner of herbs, out of the way of temptation, than
a stalled ox in it. Sinners pretend to find dainties in sin. Stolen
waters are sweet; forbidden fruit is pleasant to the eye. But those that
consider how soon the dainties of sin will turn into wormwood and gall,
how certainly it will, at last, bite like a serpent and sting like an
adder, will dread those dainties, and pray to God by his providence to
take them out of their sight, and by his grace to turn them against
them. Good men will pray even against the sweets of sin.

### Verses 5-10

Here, `I.` David desires to be told of his faults. His enemies reproached
him with that which was false, which he could not but complain of; yet,
at the same time, he desired his friends would reprove him for that
which was really amiss in him, particularly if there was any thing that
gave the least colour to those reproaches (v. 5): let the righteous
smite me; it shall be a kindness. The righteous God (so some); \"I will
welcome the rebukes of his providence, and be so far from quarrelling
with them that I will receive them as tokens of love and improve them as
means of grace, and will pray for those that are the instruments of my
trouble.\" But it is commonly taken for the reproofs given by righteous
men; and it best becomes those that are themselves righteous to reprove
the unrighteousness of others, and from them reproof will be best taken.
But if the reproof be just, though the reprover be not so, we must make
a good use of it and learn obedience by it. We are here taught how to
receive the reproofs of the righteous and wise. 1. We must desire to be
reproved for whatever is amiss in us, or is done amiss by us: \"Lord,
put it into the heart of the righteous to smite me and reprove me. If my
own heart does not smite me, as it ought, let my friend do it; let me
never fall under that dreadful judgment of being let alone in sin.\" 2.
We must account it a piece of friendship. We must not only bear it
patiently, but take it as a kindness; for reproofs of instruction are
the way of life (Prov. 6:23), are means of good to us, to bring us to
repentance for the sins we have committed, and to prevent relapses into
sin. Though reproofs cut, it is in order to a cure, and therefore they
are much more desirable than the kisses of an enemy (Prov. 27:6) or the
song of fools, Eccl. 7:5. David blessed God for Abigail\'s seasonable
admonition, 1 Sa. 25:32. 3. We must reckon ourselves helped and healed
by it: It shall be as an excellent oil to a wound, to mollify it and
close it up; it shall not break my head, as some reckon it to do, who
could as well bear to have their heads broken as to be told of their
faults; but, says David, \"I am not of that mind; it is my sin that has
broken my head, that has broken my bones, Ps. 51:8. The reproof is an
excellent oil, to cure the bruises sin has given me. It shall not break
my head, if it may but help to break my heart.\" 4. We must requite the
kindness of those that deal thus faithfully, thus friendly with us, at
least by our prayers for them in their calamities, and hereby we must
show that we take it kindly. Dr. Hammond gives quite another reading of
this verse: \"Reproach will bruise me that am righteous, and rebuke me;
but that poisonous oil shall not break my head (shall not destroy me,
shall not do me the mischief intended), for yet my prayer shall be in
their mischiefs, that God would preserve me from them, and my prayer
shall not be in vain.\"

`II.` David hopes his persecutors will, some time or other, bear to be
told of their faults, as he was willing to be told of his (v. 6): \"When
their judges\" (Saul and his officers who judged and condemned David,
and would themselves be sole judges) \"are overthrown in stony places,
among the rocks in the wilderness, then they shall hear my words, for
they are sweet.\" Some think this refers to the relentings that were in
Saul\'s breast when he said, with tears, Is this thy voice, my son
David? 1 Sa. 24:16; 26:21. Or we may take it more generally: even
judges, great as they are, may come to be overthrown. Those that make
the greatest figure in this world do not always meet with level smooth
ways through it. And those that slighted the word of God before will
relish it, and be glad of it, when they are in affliction, for that
opens the ear to instruction. When the world is bitter the word is
sweet. Oppressed innocency cannot gain a hearing with those that live in
pomp and pleasure, but when they come to be overthrown themselves they
will have more compassionate thoughts of the afflicted.

`III.` David complains of the great extremity to which he and his friends
were reduced (v. 7): Our bones are scattered at the grave\'s mouth, out
of which they are thrown up, so long have we been dead, or into which
they are ready to be thrown, so near are we to the pit; and they are as
little regarded as chips among the hewers of wood, which are thrown in
neglected heaps: As one that cuts and cleaves the earth (so some read
it), alluding to the ploughman who tears the earth in pieces with his
plough-share, Ps. 129:3. Can these dry bones live?

`IV.` David casts himself upon God, and depends upon him for deliverance:
\"But my eyes are unto thee (v. 8); for, when the case is ever so
deplorable, thou canst redress all the grievances. From thee I expect
relief, bad as things are, and in thee is my trust.\" Those that have
their eye towards God may have their hopes in him.

`V.` He prays that God would succour and relieve him as his necessity
required. 1. That he would comfort him: \"Leave not my soul desolate and
destitute; still let me see where my help is.\" 2. That he would prevent
the designs of his enemies against him (v. 9): \"Keep me from being
taken in the snare they have laid for me; give me to discover it and to
evade it.\" Be the gin placed with ever so much subtlety, God can and
will secure his people from being taken in it. 3. That God would, in
justice, turn the designs of his enemies upon themselves, and, in mercy,
deliver him from being ruined by them (v. 10): let the wicked fall into
their own net, the net which, intentionally, they procured for me, but
which, meritoriously, they prepared for themselves. Nec lex est justioir
ulla quam necis artifices arte perire sua-No law can be more just than
that the architects of destruction should perish by their own
contrivances. All that are bound over to God\'s justice are held in the
cords of their own iniquity. But let me at the same time obtain a
discharge. The entangling and ensnaring of the wicked sometimes prove
the escape and enlargement of the righteous.
