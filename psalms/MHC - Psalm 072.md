Psalm 72
========

Commentary
----------

The foregoing psalm was penned by David when he was old, and, it should
seem, so was this too; for Solomon was now standing fair for the crown;
that was his prayer for himself, this for his son and successor, and
with these two the prayers of David the son of Jesse are ended, as we
find in the close of this psalm. If we have but God\'s presence with us
while we live, and good hopes concerning those that shall come after us
that they shall be praising God on earth when we are praising him in
heaven, it is enough. This is entitled \"a psalm for Solomon:\" it is
probable that David dictated it, or, rather, that it was by the blessed
Spirit dictated to him, when, a little before he died, by divine
direction he settled the succession, and gave orders to proclaim Solomon
king, 1 Ki. 1:30, etc. But, though Solomon\'s name is here made use of,
Christ\'s kingdom is here prophesied of under the type and figure of
Solomon\'s. David knew what the divine oracle was, That \"of the fruit
of his loins, according to the flesh, he would raise up Christ to sit on
his throne,\" Acts 2:30. To him he here bears witness, and with the
prospect of the glories of his kingdom he comforted himself in his dying
moments when he foresaw that his house would not be so with God, not so
great not so good, as he wished. David, in spirit, `I.` Begins with a
short prayer for his successor (v. 1). `II.` He passes immediately into a
long prediction of the glories of his reign (v. 2-17). And, `III.` He
concludes with praise to the God of Israel (v. 18-20). In singing this
psalm we must have an eye to Christ, praising him as a King, and
pleasing ourselves with our happiness as his subjects.

A psalm for Solomon.

### Verse 1

This verse is a prayer for the king, even the king\'s son.

`I.` We may apply it to Solomon: Give him thy judgments, O God! and thy
righteousness; make him a man, a king; make him a good man, a good king.
`1.` It is the prayer of a father for his child, a dying blessing, such as
the patriarchs bequeathed to their children. The best thing we can ask
of God for our children is that God will give them wisdom and grace to
know and do their duty; that is better than gold. Solomon learned to
pray for himself as his father had prayed for him, not that God would
give him riches and honour, but a wise and understanding heart. It was a
comfort to David that his own son was to be his successor, but more so
that he was likely to be both judicious and righteous. David had given
him a good education (Prov. 4:3), had taught him good judgment and
righteous, yet that would not do unless God gave him his judgments.
Parents cannot give grace to their children, but may by prayer bring
them to the God of grace, and shall not seek him in vain, for their
prayer shall either be answered or it shall return with comfort into
their own bosom. 2. It is the prayer of a king for his successor. David
had executed judgment and justice during his reign, and now he prays
that his son might do so too. Such a concern as this we should have for
posterity, desiring and endeavouring that those who come after us may do
God more and better service in their day than we have done in ours.
Those have little love either to God or man, and are of a very narrow
selfish spirit, who care not what becomes of the world and the church
when they are gone. 3. It is the prayer of subjects for their king. It
should seem, David penned this psalm for the use of the people, that
they, in singing, might pray for Solomon. Those who would live quiet and
peaceable lives must pray for kings and all in authority, that God would
give them his judgments and righteousness.

`II.` We may apply it to Christ; not that he who intercedes for us needs
us to intercede for him; but, 1. It is a prayer of the Old-Testament
church for sending the Messiah, as the church\'s King, King on the holy
hill of Zion, of whom the King of kings had said, Thou art my Son, Ps.
2:6, 7. \"Hasten his coming to whom all judgment is committed;\" and we
must thus hasten the second coming of Christ, when he shall judge the
world in righteousness. 2. It is an expression of the satisfaction which
all true believers take in the authority which the Lord Jesus has
received from the Father: \"Let him have all power both in heaven and
earth, and be the Lord our righteousness; let him be the great trustee
of divine grace for all that are his; give it to him, that he may give
it to us.\"

### Verses 2-17

This is a prophecy of the prosperity and perpetuity of the kingdom of
Christ under the shadow of the reign of Solomon. It comes in, 1. As a
plea to enforce the prayer: \"Lord, give him thy judgments and thy
righteousness, and then he shall judge thy people with righteousness,
and so shall answer the end of his elevation, v. 2. Give him thy grace,
and then thy people, committed to his charge, will have the benefit of
it.\" Because God loved Israel, he made him king over them to do
judgment and justice, 2 Chr. 9:8. We may in faith wrestle with God for
that grace which we have reason to think will be of common advantage to
his church. 2. As an answer of peace to the prayer. As by the prayer of
faith we return answers to God\'s promises of mercy, so by the promises
of mercy God returns answers to our prayers of faith. That this prophecy
must refer to the kingdom of the Messiah is plain, because there are
many passages in it which cannot be applied to the reign of Solomon.
There was indeed a great deal of righteousness and peace, at first, in
the administration of his government; but, before the end of his reign,
there were both trouble and unrighteousness. The kingdom here spoken of
is to last as long as the sun, but Solomon\'s was soon extinct.
Therefore even the Jewish expositors understand it of the kingdom of the
Messiah.

Let us observe the many great and precious promises here made, which
were to have their full accomplishment only in the kingdom of Christ;
and yet some of them were in part fulfilled in Solomon\'s reign.

`I.` That it should be a righteous government (v. 2): He shall judge thy
people with righteousness. Compare Isa. 11:4. All the laws of Christ\'s
kingdom are consonant to the eternal rules of equity; the chancery it
erects to relieve against the rigours of the broken law is indeed a
court of equity; and against the sentence of his last judgment there
will lie no exception. The peace of his kingdom shall be supported by
righteousness (v. 3); for then only is the peace like a river, when the
righteousness is as the waves of the sea. The world will be judged in
righteousness, Acts 17:31.

`II.` That it should be a peaceable government: The mountains shall bring
peace, and the little hills (v. 3); that is (says Dr. Hammond), both the
superior and the inferior courts of judicature in Solomon\'s kingdom.
There shall be abundance of peace, v. 7. Solomon\'s name signifies
peaceable, and such was his reign; for in it Israel enjoyed the
victories of the foregoing reign and preserved the tranquillity and
repose of that reign. But peace is, in a special manner, the glory of
Christ\'s kingdom; for, as far as it prevails, it reconciles men to God,
to themselves, and to one another, and slays all enmities; for he is our
peace.

`III.` That the poor and needy should be, in a particular manner, taken
under the protection of this government: He shall judge thy poor, v. 2.
Those are God\'s poor that are impoverished by keeping a good
conscience, and those shall be provided for with a distinguishing care,
shall be judged for with judgment, with a particular cognizance taken of
their case and a particular vengeance taken for their wrongs. The poor
of the people, and the children of the needy, he will be sure so to
judge as to save, v. 4. This is insisted upon again (v. 12, 13),
intimating that Christ will be sure to carry his cause on behalf of his
injured poor. He will deliver the needy that lie at the mercy of their
oppressors, the poor also, both because they have no helper and it is
for his honour to help them and because they cry unto him and he has
promised, in answer to their prayers, to help them; they by prayer
commit themselves unto him, Ps. 10:14. He will spare the needy that
throw themselves on his mercy, and will not be rigorous and severe with
them; he will save their souls, and that is all they desire. Blessed are
the poor in spirit, for theirs is the kingdom of heaven. Christ is the
poor man\'s King.

`IV.` That proud oppressors shall be reckoned with: He shall break them
in pieces (v. 4), shall take away their power to hurt, and punish them
for all the mischief they have done. This is the office of a good king,
Parcere subjectis, et debellare superbos-To spare the vanquished and
debase the proud. The devil is the great oppressor, whom Christ will
break in pieces and of whose kingdom he will be the destruction. With
the breath of his mouth shall he slay that wicked one (Isa. 11:4), and
shall deliver the souls of his people from deceit and violence, v. 14.
He shall save from the power of Satan, both as an old serpent working by
deceit to ensnare them and as a roaring lion working by violence to
terrify and devour them. So precious shall their blood be unto him that
not a drop of it shall be shed, by the deceit or violence of Satan or
his instruments, without being reckoned for. Christ is a King, who,
though he calls his subjects sometimes to resist unto blood for him, yet
is not prodigal of their blood, nor will ever have it parted with but
upon a valuable consideration to his glory and theirs, and the filling
up of the measure of their enemies\' iniquity.

`V.` That religion shall flourish under Christ\'s government (v. 5): They
shall fear thee as long as the sun and moon endure. Solomon indeed built
the temple, and the fear and worship of God were well kept up, for some
time, under his government, but it did not last long; this therefore
must point at Christ\'s kingdom, all the subjects of which are brought
to and kept in the fear of God; for the Christian religion has a direct
tendency to, and a powerful influence upon, the support and advancement
of natural religion. Faith in Christ will set up, and keep up, the fear
of God; and therefore this is the everlasting gospel that is preached,
Fear God, and give honour to him, Rev. 14:7. And, as Christ\'s
government promotes devotion towards God, so it promotes both justice
and charity among men (v. 7): In his days shall the righteous flourish;
righteousness shall be practised, and those that practise righteousness
shall be preferred. Righteousness shall abound and be in reputation,
shall command and be in power. The law of Christ, written in the heart,
disposes men to be honest and just, and to render to all their due; it
likewise disposes men to live in love, and so it produces abundance of
peace and beats swords into ploughshares. Both holiness and love shall
be perpetual in Christ\'s kingdom, and shall never go to decay, for the
subjects of it shall fear God as long as the sun and moon endure;
Christianity, in the profession of it, having got footing in the world,
shall keep its ground till the end of time, and having, in the power of
it, got footing in the heart, it will continue there till, by death, the
sun, and the moon, and the stars (that is, the bodily senses) are
darkened. Through all the changes of the world, and all the changes of
life, Christ\'s kingdom will support itself; and, if the fear of God
continue as long as the sun and moon, abundance of peace will. The peace
of the church, the peace of the soul, shall run parallel with its purity
and piety, and last as long as these last.

`VI.` That Christ\'s government shall be very comfortable to all his
faithful loving subjects (v. 6): He shall, by the graces and comforts of
his Spirit, come down like rain upon the mown grass; not on that which
is cut down, but that which is left growing, that it may spring again,
though it was beheaded. The gospel of Christ distils as the rain, which
softens the ground that was hard, moistens that which was dry, and so
makes it green and fruitful, Isa. 55:10. Let our hearts drink in the
rain, Heb. 6:7.

`VII.` That Christ\'s kingdom shall be extended very far, and greatly
enlarged; considering,

`1.` The extent of his territories (v. 8): He shall have dominion from
sea to sea (from the South Sea to the North, or from the Red Sea to the
Mediterranean) and from the river Euphrates, or Nile, to the ends of the
earth. Solomon\'s dominion was very large (1 Ki. 4:21), according to the
promise, Gen. 15:18. But no sea, no river, is named, that it might, by
these proverbial expressions, intimate the universal monarchy of the
Lord Jesus. His gospel has been, or shall be, preached to all nations
(Mt. 24:14), and the kingdoms of the world shall become his kingdoms
(Rev. 11:15) when the fulness of the Gentiles shall be brought in. His
territories shall be extended to those countries, `(1.)` That were
strangers to him: Those that dwell in the wilderness, out of all high
roads, that seldom hear news, shall hear the glad tidings of the
Redeemer and redemption by him, shall bow before him, shall believe in
him, accept of him, worship him, and take his yoke upon them. Before the
Lord Jesus we must all either bow or break; if we break, we are
ruined-if we bow, we are certainly made for ever. `(2.)` That were enemies
to him, and had fought against him: They shall lick the dust; they shall
be brought down and laid in the dust, shall bite the ground for
vexation, and be so hunger-bitten that they shall be glad of dust, the
serpent\'s meat (Gen. 3:15), for of his seed they are; and over whom
shall not he rule, when his enemies themselves are thus humbled and
brought low?

`2.` The dignity of his tributaries. He shall not only reign over those
that dwell in the wilderness, the peasants and cottagers, but over those
that dwell in the palaces (v. 10): The kings of Tarshish, and of the
isles, that lie most remote from Israel and are the isles of the
Gentiles (Gen. 10:5), shall bring presents to him as their sovereign
Lord, by and under whom they hold their crowns and all their crown
lands. They shall court his favour, and make an interest in him, that
they may hear his wisdom. This was literally fulfilled in Solomon (for
all the kings of the earth sought the wisdom of Solomon, and brought
every man his present, 2 Chr. 9:23, 24), and in Christ too, when the
wise men of the east, who probably were men of the first rank in their
own country, came to worship him and brought him presents, Mt. 2:11.
They shall present themselves to him; that is the best present we can
bring to Christ, and without that no other present is acceptable, Rom.
12:1. They shall offer gifts, spiritual sacrifices of prayer and praise,
offer them to Christ as their God, on Christ as their altar, which
sanctifies every gift. Their conversion to God is called the offering
up, or sacrificing, of the Gentiles, Rom. 15:16. Yea, all kings shall,
sooner or later, fall down before him, either to do their duty to him or
to receive their doom from him, v. 11. They shall fall before him,
either as his willing subjects or as his conquered captives, as
suppliants for his mercy or expectants of his judgment. And, when the
kings submit, the people come in of course: All nations shall serve him;
all shall be invited into his service; some of all nations shall come
into it, and in every nation incense shall be offered to him and a pure
offering, Mal. 1:11; Rev. 7:9.

`VIII.` That he shall be honoured and beloved by all his subjects (v.
15): He shall live; his subjects shall desire his life (O king! live for
ever) and with good reason; for he has said, Because I live, you shall
live also; and of him it is witnessed that he liveth, ever liveth,
making intercession, Heb. 7:8, 25. He shall live, and live prosperously;
and, 1. Presents shall be made to him. Though he shall be able to live
without them, for he needs neither the gifts nor the services of any,
yet to him shall be given of the gold of Sheba-gold, the best of metals,
gold of Sheba, which probably was the finest gold; for he that is best
must be served with the best. Those that have abundance of the wealth of
this world, that have gold at command, must give it to Christ, must
serve him with it, do good with it. Honour the Lord with thy substance.
2. Prayers shall be made for him, and that continually. The people
prayed for Solomon, and that helped to make him and his reign so great a
blessing to them. It is the duty of subjects to make prayers,
intercessions, and giving of thanks, for kings and all in authority, not
in compliment to them, as is too often done, but in concern for the
public welfare. But how is this applied to Christ? He needs not our
prayers, nor can have any benefit by them. But the Old-Testament saints
prayed for his coming, prayed continually for it; for they called him,
He that should come. And now that he has come we must pray for the
success of his gospel and the advancement of his kingdom, which he calls
praying for him (Hosanna to the Son of David, prosperity to his reign),
and we must pray for his second coming. It may be read, Prayer shall be
made through him, or for his sake; whatsoever we ask of the Father shall
be in his name and in dependence upon his intercession. 3. Praises shall
be made of him, and high encomiums given of his wisdom, justice, and
goodness: Daily shall he be praised. By praying daily in his name we
give him honour. Subjects ought to speak well of the government that is
a blessing to them; and much more ought all Christians to praise Jesus
Christ, daily to praise him; for they owe their all to him, and to him
they lie under the highest obligations.

`IX.` That under his government there shall be a wonderful increase both
of meat and mouths, both of the fruits of the earth in the country and
of the people inhabiting the cities, v. 16. 1. The country shall grow
rich. Sow but a handful of corn on the top of the mountains, whence one
would expect but little, and yet the fruit of it shall shake like
Lebanon; it shall come up like a wood, so thick, and tall, and strong,
like the cedars of Lebanon. Even upon the tops of the mountains the
earth shall bring forth by handfuls; that is an expression of great
plenty (Gen. 41:47), as the grass upon the house top is said to be that
wherewith the mower fills not his hand. This is applicable to the
wonderful productions of the seed of the gospel in the days of the
Messiah. A handful of that seed, sown in the mountainous and barren soil
of the Gentile world, produced a wonderful harvest gathered in to
Christ, fruit that shook like Lebanon. The fields were white to the
harvest, Jn. 4:35; Mt. 9:37. The grain of mustard-seed grew up to a
great tree. 2. The towns shall grow populous: Those of the city shall
flourish like grass, for number, for verdure. The gospel church, the
city of God among men, shall have all the marks of prosperity, many
shall be added to it, and those that are shall be happy in it.

`X.` That his government shall be perpetual, both to his honour and to the
happiness of his subjects. The Lord Jesus shall reign for ever, and of
him only this must be understood, and not at all of Solomon. It is
Christ only that shall be feared throughout all generations (v. 5) and
as long as the sun and moon endure, v. 7. 1. The honour of the princes
is immortal and shall never be sullied (v. 17): His name shall endure
for ever, in spite of all the malicious attempts and endeavours of the
powers of darkness to eclipse the lustre of it and to cut off the line
of it; it shall be preserved; it shall be perpetuated; it shall be
propagated. As the names of earthly princes are continued in their
posterity, so Christ\'s in himself. Filiabitur nomen ejus-His name shall
descend to posterity. All nations, while the world stands, shall call
him blessed, shall bless God for him, continually speak well of him, and
think themselves happy in him. To the end of time, and to eternity, his
name shall be celebrated, shall be made use of; every tongue shall
confess it and every knee shall bow before it. 2. The happiness of the
people if universal too; it is complete and everlasting: Men shall be
blessed, truly and for ever blessed, in him. This plainly refers to the
promise made unto the fathers that in the Messiah all the nations of the
earth should be blessed. Gen. 12:3.

### Verses 18-20

Such an illustrious prophecy as is in the foregoing verses of the
Messiah and his kingdom may fitly be concluded, as it is here, with
hearty prayers and praises.

`I.` The psalmist is here enlarged in thanksgivings for the prophecy and
promise, v. 18, 19. So sure is every word of God, and with so much
satisfaction may we rely upon it, that we have reason enough to give
thanks for what he has said, though it be not yet done. We must own that
for all the great things he has done for the world, for the church, for
the children of men, for his own children, in the kingdom of providence,
in the kingdom of grace, for all the power and trust lodged in the hands
of the Redeemer, God is worthy to be praised; we must stir up ourselves
and all that is within us to praise him after the best manner, and
desire that all others may do it. Blessed be the Lord, that is, blessed
be his glorious name; for it is only in his name that we can contribute
any thing to his glory and blessedness, and yet that is also exalted
above all blessing and praise. Let it be blessed for ever, it shall be
blessed for ever, it deserves to be blessed for ever, and we hope to be
for ever blessing it. We are here taught to bless the name of Christ,
and to bless God in Christ, for all that which he has done for us by
him. We must bless him, 1. As the Lord God, as a self-existent
self-sufficient Being, and our sovereign Lord. 2. As the God of Israel,
in covenant with that people and worshipped by them, and who does this
in performance of the truth unto Jacob and the mercy to Abraham, 3. As
the God who only does wondrous things, in creation and providence, and
especially this work of redemption, which excels them all. Men\'s works
are little, common, trifling things, and even these they could not do
without him. But God does all by his own power, and they are wondrous
things which he does, and such as will be the eternal admiration of
saints and angels.

`II.` He is earnest in prayer for the accomplishment of this prophecy and
promise: Let the whole earth be filled with his glory, as it will be
when the kings of Tarshish, and the isles, shall bring presents to him.
It is sad to think how empty the earth is of the glory of God, how
little service and honour he has from a world to which he is such a
bountiful benefactor. All those, therefore, that wish well to the honour
of God and the welfare of mankind, cannot but desire that the earth may
be filled with the discoveries of his glory, suitably returned in
thankful acknowledgments of his glory. Let every heart, and every mouth,
and every assembly, be filled with the high praises of God. We shall see
how earnest David is in this prayer, and how much his heart is in it, if
we observe, 1. How he shuts up the prayer with a double seal: \"Amen and
amen; again and again I say, I say it and let all others say the same,
so be it. Amen to my prayer; Amen to the prayers of all the saints to
this purport-Hallowed be thy name; thy kingdom come.\" 2. How he ever
shuts up his life with this prayer, v. 20. This was the last psalm that
ever he penned, though not placed last in this collection; he penned it
when he lay on his death-bed, and with this he breathes his last: \"Let
God be glorified, let the kingdom of the Messiah be set up, and kept up,
in the world, and I have enough, I desire no more. With this let the
prayers of David the son of Jesse be ended. Even so, come, Lord Jesus,
come quickly.\"
