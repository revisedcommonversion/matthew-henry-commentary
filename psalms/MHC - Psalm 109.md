Psalm 109
=========

Commentary
----------

Whether David penned this psalm when he was persecuted by Saul, or when
his son Absalom rebelled against him, or upon occasion of some other
trouble that was given him, is uncertain; and whether the particular
enemy he prays against was Saul, or Doeg, or Ahithophel, or some other
not mentioned in the story, we cannot determine; but it is certain that
in penning it he had an eye to Christ, his sufferings and his
persecutors, for that imprecation (v. 8) is applied to Judas, Acts 1:20.
The rest of the prayers here against his enemies were the expressions,
not of passion, but of the Spirit of prophecy. `I.` He lodges a complaint
in the court of heaven of the malice and base ingratitude of his enemies
and with it an appeal to the righteous God (v. 1-5). `II.` He prays
against his enemies, and devotes them to destruction (v. 6-20). `III.` He
prays for himself, that God would help and succour him in his low
condition (v. 21-29). `IV.` He concludes with a joyful expectation that
God would appear for him (v. 30, 31). In singing this psalm we must
comfort ourselves with the believing foresight of the certain
destruction of all the enemies of Christ and his church, and the certain
salvation of all those that trust in God and keep close to him.

To the chief Musician. A psalm of David.

### Verses 1-5

It is the unspeakable comfort of all good people that, whoever is
against them, God is for them, and to him they may apply as to one that
is pleased to concern himself for them. Thus David here.

`I.` He refers himself to God\'s judgment (v. 1): \"Hold not thy peace,
but let my sentence come forth from thy presence, Ps. 17:2. Delay not to
give judgment upon the appeal made to thee.\" God saw what his enemies
did against him, but seemed to connive at it, and to keep silence:
\"Lord,\" says he, \"do not always do so.\" The title he gives to God is
observable: \"O God of my praise! the God in whom I glory, and not in
any wisdom or strength of my own, from whom I have every thing that is
my praise, or the God whom I have praised, and will praise, and hope to
be for ever praising.\" He had before called God the God of his mercy
(Ps. 59:10), here he calls him the God of his praise. Forasmuch as God
is the God of our mercies we must make him the God of our praises; if
all is of him and from him, all must be to him and for him.

`II.` He complains of his enemies, showing that they were such as it was
fit for the righteous God to appear against. 1. They were very spiteful
and malicious: They are wicked; they delight in doing mischief (v. 2);
their words are words of hatred, v. 3. They had an implacable enmity to
a good man because of his goodness. \"They open their mouths against me
to swallow me up, and fight against me to cut me off if they could.\" 2.
They were notorious liars; and lying comprehends two of the seven things
which the Lord hates. \"They are deceitful in their protestations and
professions of kindness, while at the same time they speak against me
behind my back, with a lying tongue.\" They were equally false in their
flatteries and in their calumnies. 3. They were both public and restless
in their designs; \"They compassed me about on all sides, so that, which
way soever I looked, I could see nothing but what made against me.\" 4.
They were unjust; their accusations of him, and sentence against him,
were all groundless: \"They have fought against me without a cause; I
never gave them any provocation.\" Nay, which was worst of all, 5. They
were very ungrateful, and rewarded him evil for good, v. 5. Many a
kindness he had done them, and was upon all occasions ready to do them,
and yet he could not work upon them to abate their malice against him,
but, on the contrary, they were the more exasperated because they could
not provoke him to give them some occasion against him (v. 4): For my
love they are my adversaries. The more he endeavoured to gratify them
the more they hated him. We may wonder that it is possible that any
should be so wicked; and yet, since there have been so many instances of
it, we should not wonder if any be so wicked against us.

`III.` He resolves to keep close to his duty and take the comfort of
that: But I give myself unto prayer (v. 4), I prayer (so it is in the
original); \"I am for prayer, I am a man of prayer, I love prayer, and
prize prayer, and practise prayer, and make a business of prayer, and am
in my element when I am at prayer.\" A good man is made up of prayer,
gives himself to prayer, as the apostles, Acts 6:4. When David\'s
enemies falsely accused him, and misrepresented him, he applied to God
and by prayer committed his cause to him. Though they were his
adversaries for his love, yet he continued to pray for them; if others
are abusive and injurious to us, yet let not us fail to do our duty to
them, nor sin against the Lord in ceasing to pray for them, 1 Sa. 12:23.
Though they hated and persecuted him for his religion, yet he kept close
to it; they laughed at him for his devotion, but they could not laugh
him out of it. \"Let them say what they will, I give myself unto
prayer.\" Now herein David was a type of Christ, who was compassed about
with words of hatred and lying words, whose enemies not only persecuted
him without cause, but for his love and his good works (Jn. 10:32); and
yet he gave himself to prayer, to pray for them. Father, forgive them.

### Verses 6-20

David here fastens upon some one particular person that was worse than
the rest of his enemies, and the ringleader of them, and in a devout and
pious manner, not from a principle of malice and revenge, but in a holy
zeal for God and against sin and with an eye to the enemies of Christ,
particularly Judas who betrayed him, whose sin was greater than
Pilate\'s that condemned him (Jn. 19:11), he imprecates and predicts his
destruction, foresees and pronounces him completely miserable, and such
a one as our Saviour calls him, A son of perdition. Calvin speaks of it
as a detestable piece of sacrilege, common in his time among Franciscan
friars and other monks, that if any one had malice against a neighbour
he might hire some of them to curse him every day, which he would do in
the words of these verses; and particularly he tells of a lady in France
who, being at variance with her own and only son, hired a parcel of
friars to curse him in these words. Greater impiety can scarcely be
imagined than to vent a devilish passion in the language of sacred writ,
to kindle strife with coals snatched from God\'s altar, and to call for
fire from heaven with a tongue set on fire of hell.

`I.` The imprecations here are very terrible-woe, and a thousand woes, to
that man against whom God says Amen to them; and they are all in full
force against the implacable enemies and persecutors of God\'s church
and people, that will not repent, to give him glory. It is here foretold
concerning this bad man,

`1.` That he should be cast and sentenced as a criminal, with all the
dreadful pomp of a trial, conviction, and condemnation (v. 6, 7): Set
thou a wicked man over him, to be as cruel and oppressive to him as he
has been to others; for God often makes one wicked man a scourge to
another, to spoil the spoilers and to deal treacherously with those that
have dealt treacherously. Set the wicked one over him (so some), that
is, Satan, as it follows; and then it was fulfilled in Judas, into whom
Satan entered, to hurry him into sin first and then into despair. Set
his own wicked heart over him, set his own conscience against him; let
that fly in his face. Let Satan stand on his right hand, and be let
loose against him to deceive him, as he did Ahab to his destruction, and
then to accuse him and resist him, and then he is certainly cast, having
no interest in that advocate who alone can say, The Lord rebuke thee,
Satan (Zec. 3:1, 2); when he shall be judged at men\'s bar let not his
usual arts to evade justice do him any service, but let his sin find him
out and let him be condemned; nor shall he escape before God\'s
tribunal, but be condemned there when the day of inquisition and
recompence shall come. Let his prayer become sin, as the clamours of a
condemned malefactor not only find no acceptance, but are looked upon as
an affront to the court. The prayers of the wicked now become sin,
because soured with the leaven of hypocrisy and malice; and so they will
in the great day, because then it will be too late to cry, Lord, Lord,
open to us. Let every thing be turned against him and improved to his
disadvantage, even his prayers.

`2.` That, being condemned, he should be executed as a most notorious
malefactor. `(1.)` That he should lose his life, and the number of his
months be cut off in the midst, by the sword of justice: Let his days be
few, or shortened, as a condemned criminal has but a few days to live
(v. 8); such bloody and deceitful men shall not live out half their
days. `(2.)` That consequently all his places should be disposed of to
others, and they should enjoy his preferments and employments: Let
another take his office. This Peter applies to the filling up of
Judas\'s place in the truly sacred college of the apostles, by the
choice of Matthias, Acts 1:20. Those that mismanage their trusts will
justly have their office taken from them and given to those that will
approve themselves faithful. `(3.)` That his family should be beheaded and
beggared, that his wife should be made a widow and his children
fatherless, by his untimely death, v. 9. Wicked men, by their wicked
courses, bring ruin upon their wives and children, whom they ought to
take care of and provide for. Yet his children, if, when they lost their
father, they had a competency to live upon, might still subsist in
comfort; but they shall be vagabonds and shall beg; they shall not have
a house of their own to live in, nor any certain dwelling-place, nor
know where to have a meal\'s-meat, but shall creep out of their desolate
places with fear and trembling, like beasts out of their dens, to seek
their bread (v. 10), because they are conscious to themselves that all
mankind have reason to hate them for their father\'s sake. `(4.)` That his
estate should be ruined, as the estates of malefactors are confiscated
(v. 11): Let the extortioner, the officer, seize all that he has and let
the stranger, who was nothing akin to his estate, spoil his labour,
either for his crimes or for his debts, Job 5:4, 5. `(5.)` That his
posterity should be miserable. Fatherless children, though they have
nothing of their own, yet sometimes are well provided for by the
kindness of those whom God inclines to pity them; but this wicked man
having never shown mercy there shall be none to extend mercy to him, by
favouring his fatherless children when he is gone, v. 12. The children
of wicked parents often fare the worse for their parents\' wickedness in
this way that the bowels of men\'s compassion are shut up from them,
which yet ought not to be, for why should children suffer for that which
was not their fault, but their infelicity? `(6.)` That his memory should
be infamous, and buried in oblivion and disgrace (v. 13): Let his
posterity be cut off; let his end be to destruction (so Dr. Hammond);
and in the next generation let their name be blotted out, or remembered
with contempt and indignation, and (v. 15) let an indelible mark of
disgrace be left upon it. See here what hurries some to shameful deaths,
and brings the families and estates of others to ruin, makes them and
their despicable and odious, and entails poverty, and shame, and misery,
upon their posterity; it is sin, that mischievous destructive thing. The
learned Dr. Hammond applies this to the final dispersion and desolation
of the Jewish nation for their crucifying Christ; their princes and
people were cut off, their country was laid waste, and their posterity
were made fugitives and vagabonds.

`II.` The ground of these imprecations bespeaks them very just, though
they sound very severe. 1. To justify the imprecations of vengeance upon
the sinner\'s posterity, the sin of his ancestors is here brought into
the account (v. 14, 15), the iniquity of his fathers and the sin of his
mother. These God often visits even upon the children\'s children, and
is not unrighteous therein: when wickedness has long run in the blood
justly does the curse run along with it. Thus all the innocent blood
that had been shed upon the earth, from that of righteous Abel, was
required from that persecuting generation, who, by putting Christ to
death, filled up the measure of their fathers, and left as long a train
of vengeance to follow them as the train of guilt was that went before
them, which they themselves agreed to by saying, His blood be upon us
and on our children. 2. To justify the imprecations of vengeance upon
the sinner himself, his own sin is here charged upon him, which called
aloud for it. `(1.)` He had loved cruelty, and therefore give him blood to
drink (v. 16): He remembered not to show mercy, remembered not those
considerations which should have induced him to show mercy, remembered
not the objects of compassion that had been presented to him, but
persecuted the poor, whom he should have protected and relieved, and
slew the broken in heart, whom he should have comforted and healed. Here
is a barbarous man indeed, not it to live. `(2.)` He had loved cursing,
and therefore let the curse come upon his head, v. 17-19. Those that
were out of the reach of his cruelty he let fly at with his curses,
which were impotent and ridiculous; but they shall return upon him. He
delighted not in blessing; he took no pleasure in wishing well to
others, nor in seeing others do well; he would give nobody a good word
or a good wish, much less would he do any body a good turn; and so let
all good be far from him. He clothed himself with cursing; he was proud
of it as an ornament that he could frighten all about him with the
curses he was liberal of; he confided in it as armour, which would
secure him from the insults of those he feared. And let him have enough
of it. Was he fond of cursing? Let God\'s curse come into his bowels
like water and swell him as with a dropsy, and let it soak like oil into
his bones. The word of the curse is quick and powerful, and divides
between the joints and the marrow; it works powerfully and effectually;
it fastens on the soul; it is a piercing thing, and there is no antidote
against it. Let is compass him on every side as a garment, v. 19. Let
God\'s cursing him be his shame, as his cursing his neighbour was his
pride; let it cleave to him as a girdle, and let him never be able to
get clear of it. Let it be to him like the waters of jealousy, which
caused the belly to swell and the thigh to rot. This points at the utter
ruin of Judas, and the spiritual judgments which fell on the Jews for
crucifying Christ. The psalmist concludes his imprecations with a
terrible Amen, which signifies not only, \"I wish it may be so,\" but
\"I know it shall be so.\" Let this be the reward of my adversaries from
the Lord, v. 20. And this will be the reward of all the adversaries of
the Lord Jesus; his enemies that will not have him to reign over them
shall be brought forth and slain before him. And he will one day
recompense tribulation to those that trouble his people.

### Verses 21-31

David, having denounced God\'s wrath against his enemies, here takes
God\'s comforts to himself, but in a very humble manner, and without
boasting.

`I.` He pours out his complaint before God concerning the low condition he
was in, which, probably, gave advantage to his enemies to insult over
him: \"I am poor and needy, and therefore a proper object of pity, and
one that needs and craves thy help.\" 1. He was troubled in mind (v.
22): My heart is wounded within me, not only broken with outward
troubles, which sometimes prostrate and sink the spirits, but wounded
with a sense of guilt; and a wounded spirit who can bear? who can heal?
2. He apprehended himself drawing near to his end: I am gone like the
shadow when it declines, as good as gone already. Man\'s life, at best,
is like a shadow; sometimes it is like the evening shadow, the presage
of night approaching, like the shadow when it declines. 3. He was
unsettled, tossed up and down like the locust, his mind fluctuating and
unsteady, still putting him upon new counsels, his outward condition far
from any fixation, but still upon the remove, hunted like a partridge on
the mountains. 4. His body was wasted, and almost worn away (v. 24): My
knees are weak through fasting, either forced fasting (for want of food
when he was persecuted, or for want of appetite when he was sick) or
voluntary fasting, when he chastened his soul either for sin or
affliction, his own or other\'s, Ps. 35:13; 69:10. \"My flesh fails of
fatness; that is, it has lost the fatness it had, so that I have become
a skeleton, nothing but skin and bones.\" But it is better to have this
leanness in the body, while the soul prospers and is in health, than,
like Israel, to have leanness sent into the soul, while the body is
feasted. 5. He was ridiculed and reproached by his enemies (v. 25); his
devotions and his afflictions they made the matter of their laughter,
and, upon both those accounts, God\'s people have been exceedingly
filled with the scorning of those that were at ease. In all this David
was a type of Christ, who in his humiliation was thus wounded, thus
weakened, thus reproached; he was also a type of the church, which is
often afflicted, tossed with tempests, and not comforted.

`II.` He prays for mercy for himself. In general (v. 21): \"Do thou for
me, O God the Lord! appear for me, act for me.\" If God be for us, he
will do for us, will do more abundantly for us than we are able either
to ask or think. He does not prescribe to God what he should do for him,
but refers himself to his wisdom: \"Lord, do for me what seems good in
thy eyes. Do that which thou knowest will be for me, really for me, in
the issue for me, though for the present it may seem to make against
me.\" More particularly, he prays (v. 26): \"Help me, O Lord my God! O
save me! Help me under my trouble, save me out of my trouble; save me
from sin, help me to do my duty.\" He prays (v. 28), Though they curse,
bless thou. Here `(1.)` He despises the causeless curses of his enemies:
Let them curse. He said of Shimei, So let him curse. They can but show
their malice; they can do him no more mischief than the bird by
wandering or the swallow by flying, Prov. 26:2. He values the blessing
of God as sufficient to counterbalance their curses: Bless thou, and
then it is no matter though they curse. If God bless us, we need not
care who curses us; for how can they curse those whom God has not
cursed, nay, whom he has blessed? Num. 23:8. Men\'s curses are impotent;
God\'s blessings are omnipotent; and those whom we unjustly curse may in
faith expect and pray for God\'s blessing, his special blessing. When
the Pharisees cast out the poor man for his confessing Christ, Christ
found him, Jn. 9:35. When men without cause say all the ill they can of
us, and wish all the ills they can to us, we may with comfort lift up
our heart to God in this petition: Let them curse, but bless thou. He
prays (v. 28), Let thy servant rejoice. Those that know how to value
God\'s blessing, let them but be sure of it, and they will be glad of
it.

`III.` He prays that his enemies might be ashamed (v. 28), clothed with
shame (v. 29), that they might cover themselves with their own
confusion, that they might be left to themselves, to do that which would
expose them and manifest their folly before all men, or rather that they
might be disappointed in their designs and enterprises against David,
and thereby might be filled with shame, as the adversaries of the Jews
were, Neh. 6:16. Nay, in this he prays that they might be brought to
repentance, which is the chief thing we should beg of God for our
enemies. Sinners indeed bring shame upon themselves, but they are true
penitents that take shame to themselves and cover themselves with their
own confusion.

`IV.` He pleads God\'s glory, the honour of his name:-Do for me, for thy
name\'s sake (v. 21), especially the honour of his goodness, by which he
has proclaimed his name: \"Deliver me, because thy mercy is good; it is
what thou thyself dost delight in, and it is what I do depend upon. Save
me, not according to my merit, for I have none to pretend to, but
according to thy mercy; let that be the fountain, the reason, the
measure, of my salvation.\"

Lastly, He concludes the psalm with joy, the joy of faith, joy in
assurance that his present conflicts would end in triumphs. 1. He
promises God that he will praise him (v. 30): \"I will greatly praise
the Lord, not only with my heart, but with my mouth; I will praise him,
not in secret only, but among the multitude.\" 2. He promises himself
that he shall have cause to praise God (v. 31): He shall stand at the
right hand of the poor, night to him, a present help; he shall stand at
his right hand as his patron and advocate to plead his cause against his
accusers and to bring him off, to save him from those that condemn his
soul and would execute their sentence if they could. God was David\'s
protector in his sufferings, and was present also with the Lord Jesus in
his, stood at his right hand, so that he was not moved (Ps. 16:8), saved
his soul from those that pretended to be the judges of it, and received
it into his own hands. Let all those that suffer according to the will
of God commit the keeping of their souls to him.
