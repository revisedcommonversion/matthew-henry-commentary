Psalm 106
=========

Commentary
----------

We must give glory to God by making confession, not only of his goodness
but our own badness, which serve as foils to each other. Our badness
makes his goodness appear the more illustrious, as his goodness makes
our badness the more heinous and scandalous. The foregoing psalm was a
history of God\'s goodness to Israel; this is a history of their
rebellions and provocations, and yet it begins and ends with Hallelujah;
for even sorrow for sin must not put us out of tune for praising God.
Some think it was penned at the time of the captivity in Babylon and the
dispersion of the Jewish nation thereupon, because of that prayer in the
close (v. 47). I rather think it was penned by David at the same time
with the foregoing psalm, because we find the first verse and the last
two verses in that psalm which David delivered to Asaph, at the bringing
up of the ark to the place he had prepared for it (1 Chr. 16:34-36),
\"Gather us from among the heathen;\" for we may suppose that in Saul\'s
time there was a great dispersion of pious Israelites, when David was
forced to wander. In this psalm we have, `I.` The preface to the
narrative, speaking honour to God (v. 1, 2), comfort to the saints (v.
3), and the desire of the faithful towards God\'s favour (v. 4, 5). `II.`
The narrative itself of the sins of Israel, aggravated by the great
things God did for them, an account of which is intermixed. Their
provocations at the Red Sea (v. 6-12), lusting (v. 13-15), mutinying (v.
16-18), worshipping the golden calf (v. 19-23), murmuring (v. 24-27),
joining themselves to Baal-peor (v. 28-31), quarrelling with Moses (v.
32, 33), incorporating themselves with the nations of Canaan (v. 34-39).
To this is added an account how God had rebuked them for their sins, and
yet saved them from ruin (v. 40-46). `III.` The conclusion of the psalm
with prayer and praise (v. 47, 48). It may be of use to us to sing this
psalm, that, being put in mind by it of our sins, the sins of our land,
and the sins of our fathers, we may be humbled before God and yet not
despair of mercy, which even rebellious Israel often found with God.

### Verses 1-5

We are here taught,

`I.` To bless God (v. 1, 2): Praise you the Lord, that is, 1. Give him
thanks for his goodness, the manifestation of it to us, and the many
instances of it. He is good and his mercy endures for ever; let us
therefore own our obligations to him and make him a return of our best
affections and services. 2. Give him the glory of his greatness, his
mighty acts, proofs of his almighty power, wherein he has done great
things, and such as would be opposed. Who can utter these? Who is worthy
to do it? Who is able to do it? They are so many that they cannot be
numbered, so mysterious that they cannot be described; when we have said
the most we can of the mighty acts of the Lord, the one half is not
told; still there is more to be said; it is a subject that cannot be
exhausted. We must show forth his praise; we may show forth some of it,
but who can show forth all? Not the angels themselves. This will not
excuse us in not doing what we can, but should quicken us to do all we
can.

`II.` To bless the people of God, to call and account them happy (v. 3):
Those that keep judgment are blessed, for they are fit to be employed in
praising God. God\'s people are those whose principles are sound-They
keep judgment (they adhere to the rules of wisdom and religion, and
their practices are agreeable); they do righteousness, are just to God
and to all men, and herein they are steady and constant; they do it at
all times, in all manner of conversation, at every turn, in every
instance, and herein persevering to the end.

`III.` To bless ourselves in the favour of God, to place our happiness in
it, and to seek it, accordingly, with all seriousness, as the psalmist
here, v. 4, 5. 1. He has an eye to the lovingkindness of God, as the
fountain of all happiness: \"Remember me, O Lord! to give me that mercy
and grace which I stand in need of, with the favour which thou bearest
to thy people.\" As there are a people in the world who are in a
peculiar manner God\'s people, so there is a peculiar favour which God
bears to that people, which all gracious souls desire an interest in;
and we need desire no more to make us happy. 2. He has an eye to the
salvation of God, the great salvation, that of the soul, as the
foundation of happiness: O visit me with thy salvation. \"Afford me
(says Dr. Hammond) that pardon and that grace which I stand in need of,
and can hope for from none but thee.\" Let that salvation be my portion
for ever, and the pledges of it my present comfort. 3. He has an eye to
the blessedness of the righteous, as that which includes all good (v.
5): \"That I may see the good of thy chosen and be as happy as the
saints are; and happier I do not desire to be.\" God\'s people are here
called his chosen, his nation, his inheritance; for he has set them
apart for himself, incorporated them under his own government, is served
by them and glorified in them. The chosen people of God have a good
which is peculiar to them, which is the matter both of their gladness
and of their glorying, which is their pleasure, and their praise. God\'s
people have reason to be a cheerful people, and to boast in their God
all the day long; and those who have that gladness, that glory, need not
envy any of the children of men their pleasure or pride. The gladness of
God\'s nation, and the glory of his inheritance, are enough to satisfy
any man; for they have everlasting joy and glory at the end of them.

### Verses 6-12

Here begins a penitential confession of sin, which was in a special
manner seasonable now that the church was in distress; for thus we must
justify God in all that he brings upon us, acknowledging that therefore
he has done right, because we have done wickedly; and the remembrance of
former sins, notwithstanding which God did not cast off his people, is
an encouragement to us to hope that, though we are justly corrected for
our sins, yet we shall not be utterly abandoned.

`I.` God\'s afflicted people here own themselves guilty before God (v. 6):
\"We have sinned with our fathers, that is, like our fathers, after the
similitude of their transgression. We have added to the stock of
hereditary guilt, and filled up the measure of our fathers\' iniquity,
to augment yet the fierce anger of the Lord,\" Num. 32:14; Mt. 23:32.
And see how they lay a load upon themselves, as becomes penitents: \"We
have committed iniquity, that which is in its own nature sinful, and we
have done wickedly; we have sinned with a high hand presumptuously.\" Or
this is a confession, not only of their imitation of, but their interest
in, their fathers\' sins: We have sinned with our fathers, for we were
in their loins and we bear their iniquity, Lam. 5:7.

`II.` They bewail the sins of their fathers when they were first formed
into a people, which, since children often smart for, they are concerned
to sorrow for, even further than to the third and fourth generation.
Even we now ought to take occasion from the history of Israel\'s
rebellions to lament the depravity and perverseness of man\'s nature and
its unaptness to be amended by the most probable means. Observe here,

`1.` The strange stupidity of Israel in the midst of the favours God
bestowed upon them (v. 7): They understood not thy wonders in Egypt.
They saw them, but they did not rightly apprehend the meaning and design
of them. Blessed are those that have not seen, and yet have understood.
They thought the plagues of Egypt were intended for their deliverance,
whereas they were intended also for their instruction and conviction,
not only to force them out of their Egyptian slavery, but to cure them
of their inclination to Egyptian idolatry, by evidencing the sovereign
power and dominion of the God of Israel, above all gods, and his
particular concern for them. We lose the benefit of providences for want
of understanding them. And, as their understandings were dull, so their
memories were treacherous; though one would think such astonishing
events should never have been forgotten, yet they remembered them not,
at least they remembered not the multitude of God\'s mercies in them.
Therefore God is distrusted because his favours are not remembered.

`2.` Their perverseness arising from this stupidity: They provoked him at
the sea, even at the Red Sea. The provocation was, despair of
deliverance (because the danger was great) and wishing they had been
left in Egypt still, Ex. 14:11, 12. Quarrelling with God\'s providence,
and questioning his power, goodness, and faithfulness, are as great
provocations to him as any whatsoever. The place aggravated the crime;
it was at the sea, at the Red Sea, when they had newly come out of Egypt
and the wonders God had wrought for them were fresh in their minds; yet
they reproach him, as if all that power had no mercy in it, but he had
brought them out of Egypt on purpose to kill them in the wilderness.
They never lay at God\'s mercy so immediately as in their passage
through the Red Sea, yet there they affront it, and provoke his wrath.

`3.` The great salvation God wrought for them notwithstanding their
provocations, v. 8-11. `(1.)` He forced a passage for them through the
sea: He rebuked the Red Sea for standing in their way and retarding
their march, and it was dried up immediately; as, in the creation, at
God\'s rebuke the waters fled, Ps. 104:7. Nay, he not only prepared them
a way, but, by the pillar of cloud and fire, he led them into the sea,
and, by the conduct of Moses, led them through it as readily as through
the wilderness. He encouraged them to take those steps, and subdued
their fears, when those were their most dangerous and threatening
enemies. See Isa. 63:12-14. `(2.)` He interposed between them and their
pursuers, and prevented them from cutting them off, as they designed.
The Israelites were all on foot, and the Egyptians had all of them
chariots and horses, with which they were likely to overtake them
quickly, but God saved them from the hand of him that hated them,
namely, Pharaoh, who never loved them, but now hated them the more for
the plagues he had suffered on their account. From the hand of his
enemy, who was just ready to seize them, God redeemed them (v. 10),
interposing himself, as it were, in the pillar of fire, between the
persecuted and the persecutors. `(3.)` To complete the mercy, and turn the
deliverance into a victory, the Red Sea, which was a lane to them, was a
grave to the Egyptians (v. 11): The waters covered their enemies, so as
to slay them, but not so as to conceal their shame; for, the next tide,
they were thrown up dead upon the shore, Ex. 14:30. There was not one of
them left alive, to bring tidings of what had become of the rest. And
why did God do this for them? Nay, why did he not cover them, as he did
their enemies, for their unbelief and murmuring? He tells us (v. 8): it
was for his name\'s sake. Though they did not deserve this favour, he
designed it; and their undeservings should not alter his designs, nor
break his measures, nor make him withdraw his promise, or fail in the
performance of it. He did this for his own glory, that he might make his
mighty power to be known, not only in dividing the sea, but in doing it
notwithstanding their provocations. Moses prays (Num. 14:17, 19), Let
the power of my Lord be great and pardon the iniquity of this people.
The power of the God of grace in pardoning sin and sparing sinners is as
much to be admired as the power of the God of nature in dividing the
waters.

`4.` The good impression this made upon them for the present (v. 12):
Then believed they his words, and acknowledged that God was with them of
a truth, and had, in mercy to them, brought them out of Egypt, and not
with any design to slay them in the wilderness; then they feared the
Lord and his servant Moses, Ex. 14:31. Then they sang his praise, in
that song of Moses penned on this great occasion, Ex. 15:1. See in what
a gracious and merciful way God sometimes silences the unbelief of his
people, and turns their fears into praises; and so it is written, Those
that erred in spirit shall come to understanding, and those that
murmured shall learn doctrine, Isa. 29:24.

### Verses 13-33

This is an abridgment of the history of Israel\'s provocations in the
wilderness, and of the wrath of God against them for those provocations:
and this abridgment is abridged by the apostle, with application to us
Christians (1 Co. 10:5, etc.); for these things were written for our
admonition, that we sin not like them, lest we suffer like them.

`I.` The cause of their sin was disregard to the works and word of God, v.
13. 1. They minded not what he had done for them: They soon forgot his
works, and lost the impressions they had made upon them. Those that do
not improve God\'s mercies to them, nor endeavour in some measure to
render according to the benefit done unto them, do indeed forget them.
This people soon forgot them (God took notice of this, Ex. 32:8, They
have turned aside quickly): They made haste, they forgot his works (so
it is in the margin), which some make to be two separate instances of
their sin. They made haste; their expectations anticipated God\'s
promises; they expected to be in Canaan shortly, and because they were
not they questioned whether they should ever be there and quarrelled
with all the difficulties they met with in their way; whereas he that
believeth does not make haste, Isa. 28:16. And, withal, they forgot his
works, which were the undeniable evidences of his wisdom, power, and
goodness, and denied the conclusion as confidently as if they had never
seen the premises proved. This is mentioned again (v. 21, 22): They
forgot God their Saviour; that is, they forgot that he had been their
Saviour. Those that forget the works of God forget God himself, who
makes himself known by his works. They forgot what was done but a few
days before, which we may suppose they could not but talk of, even then,
when, because they did not make a good use of it, they are said to
forget it: it was what God did for them in Egypt, in the land of Ham,
and by the Red Sea, things which we at this distance cannot, or should
not, be unmindful of. They are called great things (for, though the
great God does nothing mean, yet he does some things that are in a
special manner great), wondrous works, out of the common road of
Providence, therefore observable, therefore memorable, and terrible
things, awful to them, and dreadful to their enemies, and yet soon
forgotten. Even miracles that were seen passed away with them as tales
that are told. 2. They minded not what God had said to them nor would
they depend upon it: They waited not for his counsel, did not attend his
word, though they had Moses to be his mouth to them; they took up
resolves about which they did not consult him and made demands without
calling upon him. They would be in Canaan directly, and had not patience
to tarry God\'s time. The delay was intolerable, and therefore the
difficulties were looked upon as insuperable. This is explained (v. 24):
They believed not his word, his promise that he would make them masters
of Canaan; and (v. 25), They hearkened not to the voice of the Lord, who
gave them counsel which they would not wait for, not only by Moses and
Aaron, but by Caleb and Joshua, Num. 14:6, 7, etc. Those that will not
wait for God\'s counsel shall justly be given up to their own hearts\'
lusts, to walk in their own counsels.

`II.` Many of their sins are here mentioned, together with the tokens of
God\'s displeasure which they fell under for those sins.

`1.` They would have flesh, and yet would not believe that God could give
it to them (v. 14): They lusted a lust (so the word is) in the
wilderness; there, where they had bread enough and to spare, yet nothing
would serve them but they must have flesh to eat. They were now purely
at God\'s finding, being supported entirely by miracles, so that this
was a reflection upon the wisdom and goodness of their Creator. They
were also, in all probability, within a step of Canaan, yet had not
patience to stay for dainties till they came thither. They had flocks
and herds of their own, but they will not kill them; God must give them
flesh as he gave them bread, or they will never give him credit, or
their good word. They did not only wish for flesh, but they lusted
exceedingly after it. A desire, even of lawful things, when it is
inordinate and violent, becomes sinful; and therefore this is called
lusting after evil things (1 Co. 10:6), though the quails, as God\'s
gift, were good things, and were so spoken of, Ps. 105:40. Yet this was
not all: They tempted God in the desert, where they had had such
experience of his goodness and power, and questioned whether he could
and would gratify them herein. See Ps. 78:19, 20. Now how did God show
his displeasure against them for this. We are told how (v. 15): He gave
them their request, but gave it them in anger, and with a curse, for he
sent leanness into their soul; he filled them with uneasiness of mind,
and terror of conscience, and a self-reproach, occasioned by their
bodies being sick with the surfeit, such as sometimes drunkards
experience after a great debauch. Or this is put for that great plague
with which the Lord smote them, while the flesh was yet between their
teeth, as we read, Num. 11:33. It was the consumption of the life. Note,
`(1.)` What is asked in passion is often given in wrath. `(2.)` Many that
fare deliciously every day, and whose bodies are healthful and fat,
have, at the same time, leanness in their souls, no love to God, no
thankfulness, no appetite to the bread of life, and then the soul must
needs be lean. Those wretchedly forget themselves that feast their
bodies and starve their souls. Then God gives the good things of this
life in love, when with them he gives grace to glorify him in the use of
them; for then the soul delights itself in fatness, Isa. 55:2.

`2.` They quarrelled with the government which God had set over them both
in church and state (v. 16): They envied Moses his authority in the
camp, as generalissimo of the armies of Israel and chief justice in all
their courts; they envied Aaron his power, as saint of the Lord,
consecrated to the office of high priest, and Korah would needs put in
for the pontificate, while Dathan and Abiram, as princes of the tribe of
Reuben, Jacob\'s eldest son, would claim to be chief magistrates, by the
so-much-admired right of primogeniture. Note, Those are preparing ruin
for themselves who envy those whom God has put honour upon and usurp the
dignities they were never designed for. And justly will contempt be
poured upon those who put contempt upon any of the saints of the Lord.
How did God show his displeasure for this? We are told how, and it is
enough to make us tremble (v. 17, 18); we have the story, Num. 16:32,
35. `(1.)` Those that flew in the face of the civil authority were
punished by the earth, which opened and swallowed them up, as not fit to
go upon God\'s ground, because they would not submit to God\'s
government. `(2.)` Those that would usurp the ecclesiastical authority in
things pertaining to God suffered the vengeance of heaven, for fire came
out from the Lord and consumed them, and the pretending sacrificers were
themselves sacrificed to divine justice. The flame burnt up the wicked;
for though they vied with Aaron, the saint of the Lord, for holiness
(Num. 16:3, 5), yet God adjudged them wicked, and as such cut them off,
as in due time he will destroy the man of sin, that wicked one,
notwithstanding his proud pretensions to holiness.

`3.` They made and worshipped the golden calf, and this in Horeb, where
the law was given, and where God had expressly said, Thou shalt neither
make any graven image nor bow down to it; they did both: They made a
calf and worshipped it, v. 19.

`(1.)` Herein they bade defiance to, and put an affront upon, the two
great lights which God has made to rule the moral world:-`[1.]` That of
human reason; for they changed their glory, their God, at least the
manifestation of him, which always had been in a cloud (either a dark
cloud or a bright one), without any manner of visible similitude, into
the similitude of Apis, one of the Egyptian idols, an ox that eateth
grass, than which nothing could be more grossly and scandalously absurd,
v. 20. Idolaters are perfectly besotted, and put the greatest
disparagement possible both upon God, in representing him by the image
of a beast, and upon themselves, in worshipping it when they have so
done. That which is here said to be the changing of their glory is
explained by St. Paul (Rom. 1:23) to be the changing of the glory of the
incorruptible God. `[2.]` That of divine revelation, which was afforded
to them, not only in the words God spoke to them, but in the works he
wrought for them, wondrous works, which declared aloud that the Lord
Jehovah is the only true and living God and is alone to be worshipped,
v. 21, 22.

`(2.)` For this God showed his displeasure by declaring the decree that he
would cut them off from being a people, as they had, as far as lay in
their power, in effect cut him off from being a God; he spoke of
destroying them (v. 23), and certainly he would have done it if Moses,
his chosen, had not stood before him in the breach (v. 23), if he had
not seasonably interposed to deal with God as an advocate about the
breach or ruin God was about to devote them to and wonderfully prevailed
to turn away his wrath. See here the mercy of God, and how easily his
anger is turned away, even from a provoking people. See the power of
prayer, and the interest which God\'s chosen have in heaven. See a type
of Christ, God\'s chosen, his elect, in whom his soul delights, who
stood before him in the breach to turn away his wrath from a provoking
world, and ever lives, for this end, making intercession.

`4.` They gave credit to the report of the evil spies concerning the land
of Canaan, in contradiction to the promise of God (v. 24): They despised
the pleasant land. Canaan was a pleasant land, Deu. 8:7. They
undervalued it when they thought it not worth venturing for, no, not
under the guidance of God himself, and therefore were for making a
captain and returning to Egypt again. They believed not God\'s word
concerning it, but murmured in their tents, basely charging God with a
design upon them in bringing them thither that they might become a prey
to the Canaanites, Num. 14:2, 3. And, when they were reminded of God\'s
power and promise, they were so far from hearkening to that voice of the
Lord that they attempted to stone those who spoke to them, Num. 14:10.
The heavenly Canaan is a pleasant land. A promise is left us of entering
into it; but there are many that despise it, that neglect and refuse the
offer of it, that prefer the wealth and pleasure of this world before
it, and grudge the pains and hazards of this life to obtain that. This
also was so displeasing to God that he lifted up his hand against them,
in a way of threatening, to destroy them in the wilderness; nay, in a
way of swearing, for he swore in his wrath that they should not enter
into his rest (Ps. 95:11; Num. 14:28); nay, and he threatened that their
children also should be overthrown and scattered (v. 26, 27), and the
whole nation dispersed and disinherited; but Moses prevailed for mercy
for their seed, that they might enter Canaan. Note, Those who despise
God\'s favours, and particularly the pleasant land, forfeit his favours,
and will be shut out for ever from the pleasant land.

`5.` They were guilty of a great sin in the matter of Peor; and this was
the sin of the new generation, when they were within a step of Canaan
(v. 28): They joined themselves to Baal-peor, and so were entangled both
in idolatry and in adultery, in corporeal and in spiritual whoredom,
Num. 25:1-3. Those that did often partake of the altar of the living God
now ate the sacrifices of the dead, of the idols of Moab (that were dead
images, or dead men canonized or deified), or sacrifices to the infernal
deities on the behalf of their dead friends. Thus they provoked God to
anger with their inventions (v. 29), in contempt of him and his
institutions, his commands, and his threatenings. The iniquity of Peor
was so great that, long after, it is said, They were not cleansed from
it, Jos. 22:17. God testified his displeasure at this, `(1.)` By sending a
plague among them, which in a little time swept away 24,000 of those
impudent sinners. `(2.)` By stirring up Phinehas to use his power as a
magistrate for the suppressing of the sin and checking the contagion of
it. He stood up in his zeal for the Lord of hosts, and executed judgment
upon Zimri and Cozbi, sinners of the first rank, genteel sinners; he put
the law in execution upon them, and this was a service so pleasing to
God that upon it the plague was stayed, v. 30. By this, and some other
similar acts of public justice on that occasion (Num. 25:4, 5), the
guilt ceased to be national, and the general controversy was let fall.
When the proper officers did their duty God left it to them, and did not
any longer keep the work in his own hands by the plague. Note, National
justice prevents national judgments. But, Phinehas herein signalizing
himself, a special mark of honour was put upon him, for what he did was
counted to him for righteousness to all generations (v. 31), and, in
recompence of it, the priesthood was entailed on his family. He shall
make an atonement by offering up the sacrifices, who had so bravely made
an atonement (so some read it, v. 30) by offering up the sinners. Note,
It is the honour of saints to be zealous against sin.

`6.` They continued their murmurings to the very last of their
wanderings; for in the fortieth year they angered God at the waters of
strife (v. 32), which refers to that story, Num. 20:3-5. And that which
aggravated it now was that it went ill with Moses for their sakes; for,
though he was the meekest of all the men in the earth, yet their
clamours at that time were so peevish and provoking that they put him
into a passion, and, having now grown very old and off his guard, he
spoke unadvisedly with his lips (v. 33), and not as became him on that
occasion; for he said in a heat, Hear now, you rebels, must we fetch
water out of this rock for you? This was Moses\'s infirmity, and is
written for our admonition, that we may learn, when we are in the midst
of provocation, to keep our mouth as with a bridle (Ps. 39:1-3), and to
take heed to our spirits, that they admit not resentments too much; for,
when the spirit is provoked, it is much ado, even for those that have a
great deal of wisdom and grace, not to speak unadvisedly. But it is
charged upon the people as their sin: They provoked his spirit with that
with which they angered God himself. Note, We must answer not only for
our own passions, but for the provocation which by them we give to the
passions of others, especially of those who, if not greatly provoked,
would be meek and quiet. God shows his displeasure against this sin of
theirs by shutting Moses and Aaron out of Canaan for their misconduct
upon this occasion, by which, `(1.)` God discovered his resentment of all
such intemperate heats, even in the dearest of his servants. If he deals
thus severely with Moses for one unadvised word, what does their sin
deserve who have spoken so many presumptuous wicked words? If this was
done in the green tree, what shall be done in the dry? `(2.)` God deprived
them of the blessing of Moses\'s guidance and government at a time when
they most needed it, so that his death was more a punishment to them
than to himself. It is just with God to remove those relations from us
that are blessings to us, when we are peevish and provoking to them and
grieve their spirits.

### Verses 34-48

Here, `I.` The narrative concludes with an account of Israel\'s conduct in
Canaan, which was of a piece with that in the wilderness, and God\'s
dealings with them, wherein, as all along, both justice and mercy
appeared.

`1.` They were very provoking to God. The miracles and mercies which
settled them in Canaan made no more deep and durable impressions upon
them than those which fetched them out of Egypt; for by the time they
were just settled in Canaan they corrupted themselves, and forsook God.
Observe,

`(1.)` The steps of their apostasy. `[1.]` They spared the nations which
God had doomed to destruction (v. 34); when they had got the good land
God had promised them they had no zeal against the wicked inhabitants
whom the Lord commanded them to extirpate, pretending pity; but so
merciful is God that no man needs to be in any case more compassionate
than he. `[2.]` When they spared them they promised themselves that,
notwithstanding this, they would not join in any dangerous affinity with
them. But the way of sin is down-hill; omissions make way for
commissions; when they neglect to destroy the heathen the next news we
hear is, They were mingled among the heathen, made leagues with them and
contracted an intimacy with them, so that they learned their works, v.
35. That which is rotten will sooner corrupt that which is sound than be
cured or made sound by it. `[3.]` When they mingled with them, and
learned some of their works that seemed innocent diversions and
entertainments, yet they thought they would never join with them in
their worship; but by degrees they learned that too (v. 36): They served
their idols in the same manner, and with the same rites, that they
served them; and they became a snare to them. That sin drew on many
more, and brought the judgments of God upon them, which they themselves
could not but be sensible of and yet knew not how to recover themselves.
`[4.]` When they joined with them in some of their idolatrous services,
which they thought had least harm in them, they little thought that ever
they should be guilty of that barbarous and inhuman piece of idolatry
the sacrificing of their living children to their dead gods; but they
came to that at last (v. 37, 38), in which Satan triumphed over his
worshippers, and regaled himself in blood and slaughter: They sacrificed
their sons and daughters, pieces of themselves, to devils, and added
murder, the most unnatural murder, to their idolatry; one cannot think
of it without horror. They shed innocent blood, the most innocent, for
it was infant-blood, nay, it was the blood of their sons and their
daughters. See the power of the spirit that works in the children of
disobedience, and see his malice. The beginning of idolatry and
superstition, like that of strife, is as the letting forth of water, and
there is no villany which those that venture upon it can be sure they
shall stop short of, for God justly gives them up to a reprobate mind,
Rom. 1:28.

`(2.)` Their sin was, in part, their own punishment; for by it, `[1.]`
They wronged their country: The land was polluted with blood, v. 38.
That pleasant land, that holy land, was rendered uncomfortable to
themselves, and unfit to receive those kind tokens of God\'s favour and
presence in it which were designed to be its honour. `[2.]` They wronged
their consciences (v. 39): They went a whoring with their own
inventions, and so debauched their own minds, and were defiled with
their own works, and rendered odious in the eyes of the holy God, and
perhaps of their own consciences.

`2.` God brought his judgments upon them; and what else could be
expected? For his name is Jealous, and he is a jealous God. `(1.)` He fell
out with them for it, v. 40. He was angry with them: The wrath of God,
that consuming fire, was kindled against his people; for from them he
took it as more insulting and ungrateful than from the heathen that
never knew him. Nay, he was sick of them: He abhorred his own
inheritance, which once he had taken pleasure in; yet the change was not
in him, but in them. This is the worst thing in sin, that it makes us
loathsome to God; and the nearer any are to God in profession the more
loathsome are they if they rebel against him, like a dunghill at our
door. `(2.)` Their enemies then fell upon them, and, their defence having
departed, made an easy prey of them (v. 41, 42): He gave them into the
hands of the heathen. Observe here how the punishment answered to the
sin: They mingled with the heathen and learned their works; from them
they willingly took the infection of sin, and therefore God justly made
use of them as the instruments of their correction. Sinners often see
themselves ruined by those by whom they have suffered themselves to be
debauched. Satan, who is a tempter, will be a tormentor. The heathen
hated them. Apostates lose all the love on God\'s side, and get none on
Satan\'s; and when those that hated them ruled over them, and they were
brought into subjection under them, no marvel that they oppressed them
and ruled them with rigour; and thus God made them know the difference
between his service and the service of the kings of the countries, 2
Chr. 12:8. `(3.)` When God granted them some relief, yet they went on in
their sins, and their troubles also were continued, v. 43. This refers
to the days of the Judges, when God often raised up deliverers and
wrought deliverances for them, and yet they relapsed to idolatry and
provoked God with their counsel, their idolatrous inventions, to deliver
them up to some other oppressor, so that at last they were brought very
low for their iniquity. Those that by sin disparage themselves, and will
not by repentance humble themselves, are justly debased, and humbled,
and brought low, by the judgments of God. `(4.)` At length they cried unto
God, and God returned in favour to them, v. 44-46. They were chastened
for their sins, but not destroyed, cast down, but not cast off. God
appeared for them, `[1.]` As a God of mercy, who looked upon their
grievances, regarded their affliction, beheld when distress was upon
them (so some), who looked over their complaints, for he heard their cry
with tender compassion (Ex. 3:7) and overlooked their provocations; for
though he had said, and had reason to say it, that he would destroy
them, yet he repented, according to the multitude of his mercies, and
reversed the sentence. Though he is not a man that he should repent, so
as to change his mind, yet he is a gracious God, who pities us, and
changes his way. `[2.]` As a God of truth, who remembered for them his
covenant, and made good every word that he had spoken; and therefore,
bad as they were, he would not break with them, because he would not
break his own promise. `[3.]` As a God of power, who has all hearts in
his hand, and turns them which way soever he pleases. He made them to be
pitied even of those that carried them captives, and hated them, and
ruled them with rigour. He not only restrained the remainder of their
enemies\' wrath, that it should not utterly consume them, but he infused
compassion even into their stony hearts, and made them relent, which was
more than any art of man could have done with the utmost force of
rhetoric. Note, God can change lions into lambs, and, when a man\'s ways
please the Lord, will make even his enemies to pity him and be at peace
with him. When God pities men shall. Tranquillus Deus tranquillat
omnia-A God at peace with us makes every thing at peace.

`II.` The psalm concludes with prayer and praise. 1. Prayer for the
completing of his people\'s deliverance. Even when the Lord brought back
the captivity of his people still there was occasion to pray, Lord, turn
again our captivity (Ps. 126:1, 4); so here (v. 47), Save us, O Lord our
God! and gather us from among the heathen. We may suppose that many who
were forced into foreign countries, in the times of the Judges (as Naomi
was, Ruth 1:1), had not returned in the beginning of David\'s reign,
Saul\'s time being discouraging, and therefore it was seasonable to
pray, Lord, gather the dispersed Israelites from among the heathen, to
give thanks to thy holy name, not only that they may have cause to give
thanks and hearts to give thanks, that they may have opportunity to do
it in the courts of the Lord\'s house, from which they were now
banished, and so may triumph in thy praise, over those that had in scorn
challenged them to sing the Lord\'s song in a strange land. 2. Praise
for the beginning and progress of it (v. 48): Blessed be the Lord God of
Israel from everlasting to everlasting. He is a blessed God from
eternity, and will be so to eternity, and so let him be praised by all
his worshippers. Let the priests say this, and then let all the people
say, Amen, Hallelujah, in token of their cheerful concurrence in all
these prayers, praises, and confessions. According to this rubric, or
directory, we find that when this psalm (or at least the closing verses
of it) was sung all the people said Amen, and praised the Lord by
saying, Hallelujah. By these two comprehensive words it is very proper,
in religious assemblies, to testify their joining with their ministers
in the prayers and praises which, as their mouth, they offer up to God,
according to his will, saying Amen to the prayers and Hallelujah to the
praises.
