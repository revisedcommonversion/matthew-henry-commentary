Psalm 132
=========

Commentary
----------

It is probable that this psalm was penned by Solomon, to be sung at the
dedication of the temple which he built according to the charge his
father gave him, 1 Chr. 28:2, etc. Having fulfilled his trust, he begs
of God to own what he had done. `I.` He had built this house for the
honour and service of God; and when he brings the ark into it, the token
of God\'s presence, he desires that God himself would come and take
possession of it (v. 8-10). With these words Solomon concluded his
prayer, 2 Chr. 6:41, 42. `II.` He had built it in pursuance of the orders
he had received from his father, and therefore his pleas to enforce
these petitions refer to David. 1. He pleads David\'s piety towards God
(v. 1-7). 2. He pleads God\'s promise to David (v. 11-18). The former
introduces his petition: the latter follows it as an answer to it. In
singing this psalm we must have a concern for the gospel church as the
temple of God, and a dependence upon Christ as David our King, in whom
the mercies of God are sure mercies.

A song of degrees.

### Verses 1-10

In these verses we have Solomon\'s address to God for his favour to him
and to his government, and his acceptance of his building a house to
God\'s name. Observe,

`I.` What he pleads-two things:-

`1.` That what he had done was in pursuance of the pious vow which his
father David had made to build a house for God. Solomon was a wise man,
yet pleads not any merit of his own: \"I am not worthy, for whom thou
shouldst do this; but, Lord, remember David, with whom thou madest the
covenant\" (as Moses prayed, Ex. 32:13, Remember Abraham, the first
trustee of the covenant); \"remember all his afflictions, all the
troubles of his life, which his being anointed was the occasion of,\" or
his care and concern about the ark, and what an uneasiness it was to him
that the ark was in curtains, 2 Sa. 7:2. Remember all his humility and
meekness (so some read it), all that pious and devout affection with
which he had made the following vow. Note, It is not amiss for us to put
God in mind of our predecessors in profession, of their afflictions,
their services, and their sufferings, of God\'s covenant with them, the
experiences they have had of his goodness, the care they took of, and
the many prayers they put up for, those that should come after them. We
may apply it to Christ, the Son of David, and to all his afflictions:
\"Lord, remember the covenant made with him and the satisfaction made by
him. Remember all his offerings (Ps. 20:3), that is, all his
sufferings.\" He especially pleads the solemn vow that David had made as
soon as ever he was settled in his government, and before he was well
settled in a house of his own, that he would build a house for God.
Observe, `(1.)` Whom he bound himself to, to the Lord, to the mighty God
of Jacob. Vows are to be made to God, who is a party as well as a
witness. The Lord is the Mighty One of Jacob, Jacob\'s God, and a mighty
one, whose power is engaged for Jacob\'s defence and deliverance. Jacob
is weak, but the God of Jacob is a mighty one. `(2.)` What he bound
himself to do, to find out a place for the Lord, that is, for the ark,
the token of his presence. He had observed in the law frequent mention
of the place that God would choose to put his name there, to which all
the tribes should resort. When he came to the crown there was no such
place; Shiloh was deserted, and no other place was pitched upon, for
want of which the feasts of the Lord were not kept with due solemnity.
\"Well,\" says David, \"I will find out such a place for the general
rendezvous of all the tribes, a place of habitation for the Mighty One
of Jacob, a place for the ark, where there shall be room both for the
priests and people to attend upon it.\" `(3.)` How intent he was upon it;
he would not settle in his bed, till he had brought this matter to some
head, v. 3, 4. The thing had been long talked of, and nothing done, till
at last David, when he went out one morning about public business, made
a vow that before night he would come to a resolution in this matter,
and would determine the place either where the tent should be pitched
for the reception of the ark, at the beginning of his reign, or rather
where Solomon should build the temple, which was not fixed till the
latter end of his reign, just after the pestilence with which he was
punished for numbering the people (1 Chr. 22:1, Then David said, This is
the house of the Lord); and perhaps it was upon occasion of that
judgment that he made this vow, being apprehensive that one of God\'s
controversies with him was for his dilatoriness in this matter. Note,
When needful work is to be done for God it is good for us to task
ourselves, and tie ourselves to a time, because we are apt to put off.
It is good in the morning to cut out work for the day, binding ourselves
that we will do it before we sleep, only with submission to Providence;
for we know not what a day may bring forth. Especially in the great work
of conversion to God we must be thus solicitous, thus zealous; we have
good reason to resolve that we will not enjoy the comforts of this life
till we have laid a foundation for hopes of a better.

`2.` That it was in pursuance of the expectations of the people of
Israel, v. 6, 7. `(1.)` They were inquisitive after the ark; for they
lamented its obscurity, 1 Sa. 7:2. They heard of it at Ephratah (that
is, at Shiloh, in the tribe of Ephraim); there they were told it had
been, but it was gone. They found it, at last, in the fields of the
wood, that is, in Kirjath-jearim, which signifies the city of woods.
Thence all Israel fetched it, with great solemnity, in the beginning of
David\'s reign (1 Chr. 13:6), so that in building his house for the ark
Solomon had gratified all Israel. They needed not to go about to seek
the ark anymore; they now knew where to find it. `(2.)` They were resolved
to attend it: \"Let us but have a convenient place, and we will go into
his tabernacle, to pay our homage there; we will worship at his
footstool as subjects and suppliants, which we neglected to do, for want
of such a place, in the days of Saul,\" 1 Chr. 13:3.

`II.` What he prays for, v. 8-10. 1. That God would vouchsafe, not only
to take possession of, but to take up his residence in, this temple
which he had built: Arise, O Lord! into thy rest, and let this be it,
thou, even the ark of thy strength, the pledge of thy presence, thy
mighty presence. 2. That God would give grace to the ministers of the
sanctuary to do their duty: Let thy priests be clothed with
righteousness; let them appear righteous both in their administrations
and in their conversations, and let both be according to the rule. Note,
Righteousness is the best ornament of a minister. Holiness towards God,
and goodness towards all men, are habits for ministers of the necessity
of which there is no dispute. \"They are thy priests, and will therefore
discredit their relation to thee if they be not clothed with
righteousness.\" 3. That the people of God might have the comfort of the
due administration of holy ordinances among them: Let thy saints shout
for joy. They did so when the ark was brought into the city of David (2
Sa. 6:15); they will do so when the priests are clothed with
righteousness. A faithful ministry is the joy of the saints; it is the
matter of it; it is a friend and a furtherance to it; we are helpers of
your joy, 2 Co. 1:24. 4. That Solomon\'s own prayer, upon occasion of
the dedicating of the temple, might be accepted of God: \"Turn not away
the face of thy anointed, that is, deny me not the things I have asked
of thee, send me not away ashamed.\" He pleads, `(1.)` That he was the
anointed of the Lord, and this he pleads as a type of Christ, the great
anointed, who, in his intercession, urges his designation to his office.
He is God\'s anointed, and therefore the Father hears him always. `(2.)`
That he was the son of David: \"For his sake do not deny me;\" and this
is the Christian\'s plea: \"For the sake of Christ\" (our David), \"in
whom thou art well pleased, accept me.\" He is David, whose name
signifies beloved; and we are made accepted in the beloved. He is God\'s
servant, whom he upholds, Isa. 42:1. \"We have no merit of our own to
plead, but for his sake, in whom there is a fulness of merit, let us
find favour.\" When we pray for the prosperity of the church we may pray
with great boldness, for Christ\'s sake, who purchased the church with
his own blood. \"Let both ministers and people do their duty.\"

### Verses 11-18

These are precious promises, confirmed by an oath, that the heirs of
them might have strong consolation, Heb. 6:17, 18. It is all one whether
we take them as pleas urged in the prayer or as answers returned to the
prayer; believers know how to make use of the promises both ways, with
them to speak to God and in them to hear what God the Lord will speak to
us. These promises relate to the establishment both in church and state,
both to the throne of the house of David and to the testimony of Israel
fixed on Mount Zion. The promises concerning Zion\'s hill are as
applicable to the gospel-church as these concerning David\'s seed are to
Christ, and therefore both pleadable by us and very comfortable to us.
Here is,

`I.` The choice God made of David\'s house and Zion hill. Both were of
divine appointment.

`1.` God chose David\'s family for the royal family and confirmed his
choice by an oath, v. 11, 12. David, being a type of Christ, was made
king with an oath: The Lord hath sworn and will not repent, will not
turn from it. Did David swear to the Lord (v. 2) that he would find him
a house? The Lord swore to David that he would build him a house; for
God will be behind with none of his people in affections or assurances.
The promise made to David refers, `(1.)` To a long succession of kings
that should descend from his loins: Of the fruit of thy body will I set
upon thy throne, which was fulfilled in Solomon; David himself lived to
see it with great satisfaction, 1 Ki. 1:48. The crown was also entailed
conditionally upon his heirs for ever: If thy children, in following
ages, will keep my covenant and my testimony that I shall teach them.
God himself engaged to teach them, and he did his part; they had Moses
and the prophets, and all he expects is that they should keep what he
taught them, and keep to it, and then their children shall sit upon thy
throne for evermore. Kings are before God upon their good behaviour, and
their commission from him runs quamdiu se bene gesserint-during good
behaviour. The issue of this was that they did not keep God\'s covenant,
and so the entail was at length cut off, and the sceptre departed from
Judah by degrees. `(2.)` To an everlasting successor, a king that should
descend from his loins of the increase of whose government and peace
there shall be no end. St. Peter applies this to Christ, nay, he tells
us that David himself so understood it. Acts 2:30, He knew that God had
sworn with an oath to him that of the fruit of his loins, according to
the flesh, he would raise up Christ to sit on his throne; and in the
fulness of time he did so, and gave him the throne of his father David,
Lu. 1:32. He did fulfill the condition of the promise; he kept God\'s
covenant and his testimony, did his Father\'s will, and in all things
pleased him; and therefore to him, and his spiritual seed, the promise
shall be made good. He, and the children God has given him, all
believers, shall sit upon the throne for evermore, Rev. 3:21.

`2.` God chose Zion hill for the holy hill, and confirmed his choice by
the delight he took in it, v. 13, 14. He chose the Mount Zion which he
loved (Ps. 78:68); he chose it for the habitation of his ark, and said
of it, This is my rest for ever, and not merely my residence for a time,
as Shiloh was. Zion was the city of David; he chose it for the royal
city because God chose it for the holy city. God said, Here will I
dwell, and therefore David said, Here will I dwell, for here he adhered
to his principle, It is good for me to be near to God. Zion must be here
looked upon as a type of the gospel-church, which is called Mount Zion
(Heb. 12:22), and in it what is here said of Zion has its full
accomplishment. Zion was long since ploughed as a field, but the church
of Christ is the house of the living God (1 Tim. 3:15), and it is his
rest for ever, and shall be blessed with his presence always, even to
the end of the world. The delight God takes in his church, and the
continuance of his presence with his church, are the comfort and joy of
all its members.

`II.` The choice blessings God has in store for David\'s house and Zion
hill. Whom God chooses he will bless.

`1.` God, having chosen Zion hill, promises to bless that,

`(1.)` With the blessings of the life that now is; for godliness has the
promise of them, v. 15. The earth shall yield her increase; where
religion is set up there shall be provision, and in blessing God will
bless it (Ps. 67:6); he will surely and abundantly bless it. And a
little provision, with an abundant blessing upon it, will be more
serviceable, as well as more comfortable, than a great deal without that
blessing. God\'s people have a special blessing upon common enjoyments,
and that blessing puts a peculiar sweetness into them. Nay, the promise
goes further: I will satisfy her poor with bread. Zion has her own poor
to keep; and it is promised that God will take care even of them. `[1.]`
By his providence they shall be kept from wanting; they shall have
provision enough. If there be scarcity, the poor are the first that feel
it, so that it is a sure sign of plenty if they have sufficient. Zion\'s
poor shall not want, for God has obliged all the sons of Zion to be
charitable to the poor, according to their ability, and the church must
take care that they be not neglected, Acts 6:1. `[2.]` By his grace they
shall be kept from complaining; though they have but dry bread, yet they
shall be satisfied. Zion\'s poor have, of all others, reason to be
content with a little of this world, because they have better things
prepared for them. And this may be understood spiritually of the
provision that is made for the soul in the word and ordinances; God will
abundantly bless that for the nourishment of the new man, and satisfy
the poor in spirit with the bread of life. What God sanctifies to us we
shall and may be satisfied with.

`(2.)` With the blessings of the life that is to come, things pertaining
to godliness (v. 16), which is an answer to the prayer, v. 9. `[1.]` It
was desired that the priests might be clothed with righteousness; it is
here promised that God will clothe them with salvation, not only save
them, but make them and their administrations instrumental for the
salvation of his people; they shall both save themselves and those that
hear them, and add those to the church that shall be saved. Note, Whom
God clothes with righteousness he will clothe with salvation; we must
pray for righteousness and then with it God will give salvation. `[2.]`
It was desired that the saints might shout for joy; it is promised that
they shall shout aloud for joy. God gives more than we ask, and when he
gives salvation he will give an abundant joy.

`2.` God, having chosen David\'s family, here promises to bless that also
with suitable blessings. `(1.)` Growing power: There, in Zion, will I make
the horn of David to bud, v. 17. The royal dignity shall increase more
and more, and constant additions he made to the lustre of it. Christ is
the horn of salvation (denoting a plentiful and powerful salvation)
which God has raised up, and made to bud, in the house of his servant
David. David had promised to use his power for God\'s glory, to cut off
the horns of the wicked, and to exalt the horns of the righteous (Ps.
75:10); in recompence for it God here promises to make his horn to bud,
for to those that have power, and use it well, more shall be given. `(2.)`
Lasting honour: I have ordained a lamp for my anointed. Thou wilt light
my candle, Ps. 18:28. That lamp is likely to burn brightly which God
ordains. A lamp is a successor, for, when a lamp is almost out, another
may be lighted by it; it is a succession, for by this means David shall
not want a man to stand before God. Christ is the lamp and the light of
the world. `(3.)` Complete victory: \"His enemies, who have formed designs
against him, will I clothe with shame, when they shall see their designs
baffled.\" Let the enemies of all good governors expect to be clothed
with shame, and especially the enemies of the Lord Jesus and his
government, who shall rise, in the great day, to everlasting shame and
contempt. `(4.)` Universal prosperity: Upon himself shall his crown
flourish, that is, his government shall be more and more his honour.
This was to have its full accomplishment in Jesus Christ, whose crown of
honour and power shall never fade, nor the flowers of it wither. The
crowns of earthly princes endure not to all generations (Prov. 27:24),
but Christ\'s crown shall endure to all eternity and the crowns reserved
for his faithful subjects are such as fade not away.
