Psalm 38
========

Commentary
----------

This is one of the penitential psalms; it is full of grief and complaint
from the beginning to the end. David\'s sins and his afflictions are the
cause of his grief and the matter of his complaints. It should seem he
was now sick and in pain, which reminded him of his sins and helped to
humble him for them; he was, at the same time, deserted by his friends
and persecuted by his enemies; so that the psalm is calculated for the
depth of distress and a complication of calamities. He complains, `I.` Of
God\'s displeasure, and of his own sin which provoked God against him
(v. 1-5). `II.` Of his bodily sickness (v. 6-10). `III.` Of the unkindness
of his friends (v. 11). `IV.` Of the injuries which his enemies did him,
pleading his good conduct towards them, yet confessing his sins against
God (v. 12-20). Lastly, he concludes the psalm with earnest prayers to
God for his gracious presence and help (v. 21, 22). In singing this
psalm we ought to be much affected with the malignity of sin; and, if we
have not such troubles as are here described, we know not how soon we
may have, and therefore must sing of them by way of preparation and we
know that others have them, and therefore we must sing of the by way of
sympathy.

A psalm of David to bring to remembrance.

### Verses 1-11

The title of this psalm is very observable; it is a psalm to bring to
remembrance; the 70th psalm, which was likewise penned in a day of
affliction, is so entitled. It is designed, 1. To bring to his own
remembrance. We will suppose it penned when he was sick and in pain, and
then it teaches us that times of sickness are times to bring to
remembrance, to bring the sin to remembrance, for which God contended
with us, to awaken our consciences to deal faithfully and plainly with
us, and set our sins in order before us, for our humiliation. In a day
of adversity consider. Or we may suppose it penned after his recovery,
but designed as a record of the convictions he was under and the
workings of his heart when he was in affliction, that upon every review
of this psalm he might call to mind the good impressions then made upon
him and make a fresh improvement of them. To the same purport was the
writing of Hezekiah when he had been sick. 2. To put others in mind of
the same things which he was himself mindful of, and to teach them what
to think and what to say when they are sick and in affliction; let them
think as he did, and speak as he did.

`I.` He deprecates the wrath of God and his displeasure in his affliction
(v. 1): O Lord! rebuke me not in thy wrath. With this same petition he
began another prayer for the visitation of the sick, Ps. 6:1. This was
most upon his heart, and should be most upon ours when we are in
affliction, that, however God rebukes and chastens us, it may not be in
wrath and displeasure, for that will be wormwood and gall in the
affliction and misery. Those that would escape the wrath of God must
pray against that more than any outward affliction, and be content to
bear any outward affliction while it comes from, and consists with, the
love of God.

`II.` He bitterly laments the impressions of God\'s displeasure upon his
soul (v. 2): Thy arrows stick fast in me. Let Job\'s complaint (ch. 7:4)
expound this of David. By the arrows of the Almighty he means the
terrors of God, which did set themselves in array against him. He was
under a very melancholy frightful apprehension of the wrath of God
against him for his sins, and thought he could look for nothing but
judgment and fiery indignation to devour him. God\'s arrows, as they are
sure to hit the mark, so they are sure to stick where they hit, to stick
fast, till he is pleased to draw them out and to bind up with his
comforts the wound he has made with his terrors. This will be the
everlasting misery of the damned-the arrows of God\'s wrath will stick
fast in them and the wound will be incurable. \"Thy hand, thy heavy
hand, presses me sore, and I am ready to sink under it; it not only lies
hard upon me, but it lies long; and who knows the power of God\'s anger,
the weight of his hand?\" Sometimes God shot his arrows, and stretched
forth his hand, for David (Ps. 18:14), but now against him; so uncertain
is the continuance of divine comforts, where yet the continuance of
divine grace is assured. He complains of God\'s wrath as that which
inflicted the bodily distemper he was under (v. 3): There is no
soundness in my flesh because of thy anger. The bitterness of it,
infused in his mind, affected his body; but that was not the worst: it
caused the disquietude of his heart, by reason of which he forgot the
courage of a soldier, the dignity of a prince, and all the cheerfulness
of the sweet psalmist of Israel, and roared terribly, v. 8. Nothing will
disquiet the heart of a good man so much as the sense of God\'s anger,
which shows what a fearful thing it is to fall into his hands. The way
to keep the heart quiet is to keep ourselves in the love of God and to
do nothing to offend him.

`III.` He acknowledges his sin to be the procuring provoking cause of all
his troubles, and groans more under the load of guilt than any other
load, v. 3. He complains that his flesh had no soundness, his bones had
no rest, so great an agitation he was in. \"It is because of thy anger;
that kindles the fire which burns so fiercely;\" but, in the next words,
he justifies God herein, and takes all the blame upon himself: \"It is
because of my sin. I have deserved it, and so have brought it upon
myself. My own iniquities do correct me.\" If our trouble be the fruit
of God\'s anger, we may thank ourselves; it is our sin that is the cause
of it. Are we restless? It is sin that makes us so. If there were not
sin in our souls, there would be no pain in our bones, no illness in our
bodies. It is sin therefore that this good man complains most of, 1. As
a burden, a heavy burden (v. 4): \"My iniquities have gone over my head,
as proud waters over a man that is sinking and drowning, or as a heavy
burden upon my head, pressing me down more than I am able to bear or to
bear up under.\" Note, Sin is a burden. The power of sin dwelling in us
is a weight, Heb. 12:1. All are clogged with it; it keeps men from
soaring upward and pressing forward. All the saints are complaining of
it as a body of death they are loaded with, Rom. 7:24. The guilt of sin
committed by us is a burden, a heavy burden; it is a burden to God (he
is pressed under it, Amos 2:13), a burden to the whole creation, which
groans under it, Rom. 8:21, 22. It will, first or last, be a burden to
the sinner himself, either a burden of repentance when he is pricked to
the heart for it, labours, and is heavy-laden, under it, or a burden of
ruin when it sinks him to the lowest hell and will for ever detain him
there; it will be a talent of lead upon him, Zec. 5:8. Sinners are said
to bear their iniquity. Threatenings are burdens. 2. As wounds,
dangerous wounds (v. 5): \"My wounds stink and are corrupt (as wounds in
the body rankle, and fester, and grow foul, for want of being dressed
and looked after), and it is through my own foolishness.\" Sins are
wounds (Gen. 4:23), painful mortal wounds. Our wounds by sin are often
in a bad condition, no care taken of them, no application made to them,
and it is owing to the sinner\'s foolishness in not confessing sin, Ps.
32:3, 4. A slight sore, neglected, may prove of fatal consequence, and
so may a slight sin slighted and left unrepented of.

`IV.` He bemoans himself because of his afflictions, and gives ease to
his grief by giving vent to it and pouring out his complaint before the
Lord.

`1.` He was troubled in mind, his conscience was pained, and he had no
rest in his own spirit; and a wounded spirit who can bear? He was
troubled, or distorted, bowed down greatly, and went mourning all the
day long, v. 6. He was always pensive and melancholy, which made him a
burden and terror to himself. His spirit was feeble and sorely broken,
and his heart disquieted, v. 8. Herein David, in his sufferings, was a
type of Christ, who, being in his agony, cried out, My soul is
exceedingly sorrowful. This is a sorer affliction than any other in this
world; whatever God is pleased to lay upon us, we have no reason to
complain as long as he preserves to us the use of our reason and the
peace of our consciences.

`2.` He was sick and weak in body; his loins were filled with a loathsome
disease, some swelling, or ulcer, or inflammation (some think a
plague-sore, such as Hezekiah\'s boil), and there was no soundness in
his flesh, but, like Job, he was all over distempered. See `(1.)` What
vile bodies these are which we carry about with us, what grievous
diseases they are liable to, and what an offence and grievance they may
soon be made by some diseases to the souls that animate them, as they
always are a cloud and cog. `(2.)` That the bodies both of the greatest
and of the best of men have in them the same seeds of diseases that the
bodies of others have, and are liable to the same disasters. David
himself, though so great a prince and so great a saint, was not exempt
from the most grievous diseases: there was no soundness even in his
flesh. Probably this was after his sin in the matter of Uriah, and thus
did he smart in his flesh for his fleshly lusts. When, at any time, we
are distempered in our bodies, we ought to remember how God has been
dishonoured in and by our bodies. He was feeble and sorely broken, v. 8.
His heart panted, and was in a continual palpitation, v. 10. His
strength and limbs failed him. As for the light of his eyes, that had
gone from him, either with much weeping or by a defluxion of rheum upon
them, or perhaps through the lowness of his spirits and the frequent
returns of fainting. Note, Sickness will tame the strongest body and the
stoutest spirit. David was famed for his courage and great exploits; and
yet, when God contended with him by bodily sickness and the impressions
of his wrath upon his mind, his hair is cut, his heart fails him, and he
becomes weak as water. Therefore let not the strong man glory in his
strength, nor any man set grief at defiance, however it may be thought
at a distance.

`3.` His friends were unkind to him (v. 11): My lovers (such as had been
merry with him in the day of his mirth) now stand aloof from my sore;
they would not sympathize with him in his griefs, nor so much as come
within hearing of his complaints, but, like the priest and Levite (Lu.
10:31), passed on the other side. Even his kinsmen, that were bound to
him by blood and alliance, stood afar off. See what little reason we
have to trust in man or to wonder if we disappointed in our expectations
of kindness from men. Adversity tries friendship, and separates between
the precious and the vile. It is our wisdom to make sure a friend in
heaven, who will not stand aloof from our sore and from whose love no
tribulation nor distress shall be able to separate us. David, in his
troubles, was a type of Christ in his agony, Christ, on his cross,
feeble and sorely broken, and then deserted by his friends and kinsmen,
who beheld afar off.

`V.` In the midst of his complaints, he comforts himself with the
cognizance God graciously took both of his griefs and of his prayers (v.
9): \"Lord, all my desire is before thee. Thou knowest what I want and
what I would have: My groaning is not hidden from thee. Thou knowest the
burdens I groan under and the blessings I groan after.\" The groanings
which cannot be uttered are not hidden from him that searches the heart
and knows what is the mind of the Spirit, Rom. 8:26, 27.

In singing this, and praying it over, whatever burden lies upon our
spirits, we would by faith cast it upon God, and all our care concerning
it, and then be easy.

### Verses 12-22

In these verses,

`I.` David complains of the power and malice of his enemies, who, it
should seem, not only took occasion from the weakness of his body and
the trouble of his mind to insult over him, but took advantage thence to
do him a mischief. He has a great deal to say against them, which he
humbly offers as a reason why God should appear for him, as Ps. 25:19,
Consider my enemies. 1. \"They are very spiteful and cruel: They seek my
hurt; nay, they seek after my life,\" v. 12. That life which was so
precious in the sight of the Lord and all good men was aimed at, as if
it had been forfeited, or a public nuisance. Such is the enmity of the
serpent\'s seed against the seed of the woman; it would wound the head,
though it can but reach the heel. It is the blood of the saints that is
thirsted after. 2. \"They are very subtle and politic. They lay snares,
they imagine deceits, and herein they are restless and unwearied: they
do it all the day long. They speak mischievous things one to another;
every one has something or other to propose that may be a mischief to
me.\" Mischief, covered and carried on by deceit, may well be called a
snare. 3. \"They are very insolent and abusive: When my foot slips, when
I fall into any trouble, or when I make any mistake, misplace a word, or
take a false step, they magnify themselves against me; they are pleased
with it, and promise themselves that it will ruin my interest, and that
if I slip I shall certainly fall and be undone.\" 4. \"They are not only
unjust, but very ungrateful: They hate me wrongfully, v. 19. I never did
them any ill turn, nor so much as bore them any ill-will, nor ever gave
them any provocation; nay, they render evil for good, v. 20. Many a
kindness I have done them, for which I might have expected a return of
kindness; but for my love they are my adversaries,\" Ps. 109:4. Such a
rooted enmity there is in the hearts of wicked men to goodness for its
own sake that they hate it, even when they themselves have the benefit
of it; they hate prayer even in those that pray for them, and hate peace
even in those that would be at peace with them. Very ill-natured indeed
those are whom no courtesy will oblige, but who are rather exasperated
by it. 5. \"They are very impious and devilish: They are my adversaries
merely because I follow the thing that good is.\" They hated him, not
only for his kindness to them, but for his devotion and obedience to
God; they hated him because they hated God and all that bear his image.
If we suffer ill for doing well, we must not think it strange; from the
beginning it was so (Cain slew Abel, because his works were righteous);
nor must we think it hard, because it will not be always so; for so much
the greater will our reward be. 6. \"They are many and mighty: They are
lively; they are strong; they are multiplied, v. 19. Lord, how are those
increased that trouble me?\" Ps. 3:1. Holy David was weak and faint; his
heart panted, and his strength failed; he was melancholy and of a
sorrowful spirit, and persecuted by his friends; but at the same time
his wicked enemies were strong and lively, and their number increased.
Let us not therefore pretend to judge of men\'s characters by their
outward condition; none knows love or hatred by all that is before him.
It should seem that David in this, as in other complaints he makes of
his enemies, has an eye to Christ, whose persecutors were such as are
here described, perfectly lost to all honour and virtue. None hate
Christianity but such as have first divested themselves of the first
principles of humanity and broken through its most sacred bonds.

`II.` He reflects, with comfort, upon his own peaceable and pious
behaviour under all the injuries and indignities that were done him. It
is then only that our enemies do us a real mischief when they provoke us
to sin (Neh. 6:13), when they prevail to put us out of the possession of
our own souls, and drive us from God and our duty. If by divine grace we
are enabled to prevent this mischief, we quench their fiery darts, and
are saved from harm. If still we hold fast our integrity and our peace,
who can hurt us? This David did here. 1. He kept his temper, and was not
ruffled nor discomposed by any of the slights that were put upon him or
the mischievous things that were said or done against him (v. 13, 14):
\"I, as a deaf man, heard not; I took no notice of the affronts put upon
me, did not resent them, nor was put into disorder by them, much less
did I meditate revenge, or study to return the injury.\" Note, The less
notice we take of the unkindness and injuries that are done us the more
we consult the quiet of our own minds. Being deaf, he was dumb, as a man
in whose mouth there are no reproofs; he was as silent as if he had
nothing to say for himself, for fear of putting himself into a heat and
incensing his enemies yet more against him; he would not only not
recriminate upon them, but not so much as vindicate himself, lest his
necessary defence should be construed his offence. Though they sought
after his life, and his silence might be taken for a confession of his
guilt, yet he was as a dumb man that opens not his mouth. Note, When our
enemies are most clamorous it is generally our prudence to be silent, or
to say little, lest we make bad worse. David could not hope by his
mildness to win upon his enemies, nor by his soft answers to turn away
their wrath; for they were men of such base spirits that they rendered
him evil for good; and yet he conducted himself thus meekly towards
them, that he might prevent his own sin and might have the comfort of it
in the reflection. Herein David was a type of Christ, who was as a sheep
dumb before the shearer, and, when he was reviled, reviled not again;
and both are examples to us not to render railing for railing. 2. He
kept close to his God by faith and prayer, and so both supported himself
under these injuries and silenced his own resentments of them. `(1.)` He
trusted in God (v. 15): \"I was as a man that opens not his mouth, for
in thee, O Lord! do I hope. I depend upon thee to plead my cause and
clear my innocency, and, some way or other, to put my enemies to silence
and shame.\" His lovers and friends, that should have owned him, and
stood by him, and appeared as witnesses for him, withdrew from him, v.
10. but God is a friend that will never fail us if we hope in him. \"I
was as a man that heareth not, for thou wilt hear. Why need I hear, and
God hear too?\" He careth for you (1 Pt. 5:7), and why need you care and
God care too? \"Thou wilt answer\" (so some) \"and therefore I will say
nothing.\" Note, It is a good reason why we should bear reproach and
calumny with silence and patience, because God is a witness to all the
wrong that is done us, and, in due time, will be a witness for us and
against those that do us wrong; therefore let us be silent, because, if
we be, then we may expect that God will appear for us, for this is an
evidence that we trust in him; but, if we undertake to manage for
ourselves, we take God\'s work out of his hands and forfeit the benefit
of his appearing for us. Our Lord Jesus, when he suffered, threatened
not, because he committed himself to him that judges righteously (1 Pt.
2:23); and we shall lose nothing, at last, by doing so. Thou shalt
answer, Lord, for me. `(2.)` He called upon God (v. 16): For I said, Hear
me (that is supplied); \"I said so\" (as v. 15); \"in thee do I hope,
for thou wilt hear, lest they should rejoice over me. I comforted myself
with that when I was apprehensive that they would overwhelm me.\" It is
a great support to us, when men are false and unkind, that we have a God
to go to whom we may be free with and who will be faithful to us.

`III.` He here bewails his own follies and infirmities. 1. He was very
sensible of the present workings of corruption in him, and that he was
now ready to repine at the providence of God and to be put into a
passion by the injuries men did him: I am ready to halt, v. 17. This
will best be explained by a reflection like this which the psalmist made
upon himself in a similar case (Ps. 73:2): My feet were almost gone,
when I saw the prosperity of the wicked. So here: I was ready to halt,
ready to say, I have cleansed my hands in vain. His sorrow was
continual: All the day long have I been plagued. (Ps. 73:13, 14), and it
was continually before him; he could not forbear poring upon it, and
that made him almost ready to halt between religion and irreligion. The
fear of this drove him to his God: \"In thee do I hope, not only that
thou wilt plead my cause, but that thou wilt prevent my falling into
sin.\" Good men, by setting their sorrow continually before them, have
been ready to halt, who, by setting God always before them, have kept
their standing. 2. He remembered against himself his former
transgressions, acknowledging that by them he had brought these troubles
upon himself and forfeited the divine protection. Though before men he
could justify himself, before God he will judge and condemn himself (v.
18): \"I will declare my iniquity, and not cover it; I will be sorry for
my sin, and not make a light matter of it;\" and this helped to make him
silent under the rebukes of Providence and the reproaches of men. Note,
If we be truly penitent for sin, that will make us patient under
affliction, and particularly under unjust censures. Two things are
required in repentance:-`(1.)` Confession of sin: \"I will declare my
iniquity; I will not only in general own myself a sinner, but I will
make a particular acknowledgment of what I have done amiss.\" We must
declare our sins before God freely and fully, and with their aggravating
circumstances, that we may give glory to God and take shame to
ourselves. `(2.)` Contrition for sin: I will be sorry for it. Sin will
have sorrow; every true penitent grieves for the dishonour he has done
to God and the wrong he has done to himself. \"I will be in care or fear
about my sin\" (so some), \"in fear lest it ruin me and in care to get
it pardoned.\"

`IV.` He concludes with very earnest prayers to God for his gracious
presence with him and seasonable powerful succour in his distress (v.
21, 11): \"Forsake me not, O Lord! though my friends forsake me, and
though I deserve to be forsaken by thee. Be not far from me, as my
unbelieving heart is ready to fear thou art.\" Nothing goes nearer to
the heart of a good man in affliction than to be under the apprehension
of God\'s deserting him in wrath; nor does any thing therefore come more
feelingly from his heart than this prayer: \"Lord, be not thou far from
me; make haste for my help; for I am ready to perish, and in danger of
being lost if relief do not come quickly.\" God gives us leave, not only
to call upon him when we are in trouble, but to hasten him. He pleads,
\"Thou art my God, whom I serve, and on whom I depend to bear me out;
and my salvation, who alone art able to save me, who hast engaged
thyself by promise to save me, and from whom alone I expect salvation.\"
Is any afflicted? let him thus pray, let him thus plead, let him thus
hope, in singing this psalm.
