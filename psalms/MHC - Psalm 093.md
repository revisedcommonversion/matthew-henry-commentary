Psalm 93
========

Commentary
----------

This short psalm sets forth the honour of the kingdom of God among men,
to his glory, the terror of his enemies, and the comfort of all his
loving subjects. It relates both to the kingdom of his providence, by
which he upholds and governs the world, and especially to the kingdom of
his grace, by which he secures the church, sanctifies and preserves it.
The administration of both these kingdoms is put into the hands of the
Messiah, and to him, doubtless, the prophet here hears witness, and to
his kingdom, speaking of it as present, because sure; and because, as
the eternal Word, even before his incarnation he was Lord of all.
Concerning God\'s kingdom glorious things are here spoken. `I.` Have other
kings their royal robes? So has he (v. 1). `II.` Have they their thrones?
So has he (v. 2). `III.` Have they their enemies whom they subdue and
triumph over? So has he (v. 3, 4). `IV.` Is it their honour to be faithful
and holy? So it is his (v. 5). In singing this psalm we forget ourselves
if we forget Christ, to whom the Father has given all power both in
heaven and in earth.

### Verses 1-5

Next to the being of God there is nothing that we are more concerned to
believe and consider than God\'s dominion, that Jehovah is God, and that
this God reigns (v. 1), not only that he is King of right, and is the
owner and proprietor of all persons and things, but that he is King in
fact, and does direct and dispose of all the creatures and all their
actions according to the counsel of his own will. This is celebrated
here, and in many other psalms: The Lord reigns. It is the song of the
gospel church, of the glorified church (Rev. 19:6), Hallelujah; the Lord
God omnipotent reigns. Here we are told how he reigns.

`I.` The Lord reigns gloriously: He is clothed with majesty. The majesty
of earthly princes, compared with God\'s terrible majesty, is but like
the glimmerings of a glow-worm compared with the brightness of the sun
when he goes forth in his strength. Are the enemies of God\'s kingdom
great and formidable? Yet let us not fear them, for God\'s majesty will
eclipse theirs.

`II.` He reigns powerfully. He is not only clothed with majesty, as a
prince in his court, but he is clothed with strength, as a general in
the camp. He has wherewithal to support his greatness and to make it
truly formidable. See him not only clad in robes, but clad in armour.
Both strength and honour are his clothing. He can do every thing, and
with him nothing is impossible. 1. With this power he has girded
himself; it is not derived from any other, nor does the executing of it
depend upon any other, but he has it of himself and with it does
whatsoever he pleases. Let us not fear the power of man, which is
borrowed and bounded, but fear him who has power to kill and cast into
hell. 2. To this power it is owing that the world stands to this day.
The world also is established; it was so at first, by the creating power
of God, when he founded it upon the seas; it is so still, by that
providence which upholds all things and is a continued creation; it is
so established that though he has hanged the earth upon nothing (Job
26:7) yet it cannot be moved; all things continue to this day, according
to his ordinance. Note, The preserving of the powers of nature and the
course of nature is what the God of nature must have the glory of; and
we who have the benefit thereof daily are very careless and ungrateful
if we give him not the glory of it. Though God clothes himself with
majesty, yet he condescends to take care of this lower world and to
settle its affairs; and, if he established the world, much more will he
establish his church, that it cannot be moved.

`III.` He reigns eternally (v. 2): Thy throne is established of old. 1.
God\'s right to rule the world is founded in his making it; he that gave
being to it, no doubt, may give law to it, and so his title to the
government is incontestable: Thy throne is established; it is a title
without a flaw in it. And it is ancient: it is established of old, from
the beginning of time, before any other rule, principality, or power was
erected, as it will continue when all other rule, principality, and
power shall be put down, 1 Co. 15:24. 2. The whole administration of his
government was settled in his eternal counsels before all worlds; for he
does all according to the purpose which he purposed in himself; The
chariots of Providence came down from between the mountains of brass,
from those decrees which are fixed as the everlasting mountains (Zec.
6:1): Thou art from everlasting, and therefore thy throne is established
of old; because God himself was from everlasting, his throne and all the
determinations of it were so too; for in an eternal mind there could not
but be eternal thoughts.

`IV.` He reigns triumphantly, v. 3, 4. We have here, 1. A threatening
storm supposed: The floods have lifted up, O Lord! (to God himself the
remonstrance is made) the floods have lifted up their voice, which
speaks terror; nay, they have lifted up their waves, which speaks real
danger. It alludes to a tempestuous sea, such as the wicked are compared
to, Isa. 57:20. The heathen rage (Ps. 2:1) and think to ruin the church,
to overwhelm it like a deluge, to sink it like a ship at sea. The church
is said to be tossed with tempests (Isa. 54:11), and the floods of
ungodly men make the saints afraid, Ps. 18:4. We may apply it to the
tumults that are sometimes in our own bosoms, through prevailing
passions and frights, which put the soul into disorder, and are ready to
overthrow its graces and comforts; but, if the Lord reign there, even
the winds and seas shall obey him. 2. An immovable anchor cast in this
storm (v. 4): The Lord himself is mightier. Let this keep our minds
fixed, `(1.)` That God is on high, above them, which denotes his safety
(they cannot reach him, Ps. 29:10) and his sovereignty; they are ruled
by him, they are overruled, and, wherein they rebel, overcome, Ex.
18:11. `(2.)` That he is mightier, does more wondrous things than the
noise of many waters; they cannot disturb his rest or rule; they cannot
defeat his designs and purposes. Observe, The power of the church\'s
enemies is but as the noise of many waters; there is more of sound than
substance in it. Pharaoh king of Egypt is but a noise, Jer. 46:17. The
church\'s friends are commonly more frightened than hurt. God is
mightier than this noise; he is mighty to preserve his people\'s
interests from being ruined by these many waters and his people\'s
spirits from being terrified by the noise of them. He can, when he
pleases, command peace to the church (Ps. 65:7), peace in the soul, Isa.
26:3. Note, The unlimited sovereignty and irresistible power of the
great Jehovah are very encouraging to the people of God, in reference to
all the noises and hurries they meet with in this world, Ps. 46:1, 2.

`V.` He reigns in truth and holiness, v. 5. 1. All his promises are
inviolably faithful: Thy testimonies are very sure. As God is able to
protect his church, so he is true to the promises he has made of its
safety and victory. His word is passed, and all the saints may rely upon
it. Whatever was foretold concerning the kingdom of the Messiah would
certainly have its accomplishment in due time. Those testimonies upon
which the faith and hope of the Old-Testament saints were built were
very sure, and would not fail them. 2. All his people ought to be
conscientiously pure: Holiness becomes thy house, O Lord! for ever.
God\'s church is his house; it is a holy house, cleansed from sin,
consecrated by God, and employed in his service. The holiness of it is
its beauty (nothing better becomes the saints than conformity to God\'s
image and an entire devotedness to his honour), and it is its strength
and safety; it is the holiness of God\'s house that secures it against
the many waters and their noise. Where there is purity there shall be
peace. Fashions change, and that which is becoming at one time is not so
at another; but holiness always becomes God\'s house and family, and
those who belong to it; it is perpetually decent; and nothing so ill
becomes the worshippers of the holy God as unholiness.
