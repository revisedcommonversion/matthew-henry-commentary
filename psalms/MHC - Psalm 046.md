Psalm 46
========

Commentary
----------

This psalm encourages us to hope and trust in God, and his power, and
providence, and gracious presence with his church in the worst of times,
and directs us to give him the glory of what he has done for us and what
he will do: probably it was penned upon occasion of David\'s victories
over the neighbouring nations (2 Sa. 8), and the rest which God gave him
from all his enemies round about. We are here taught, `I.` To take comfort
in God when things look very black and threatening (v. 1-5). `II.` To
mention, to his praise, the great things he had wrought for his church
against its enemies (v. 6-9). `III.` To assure ourselves that God who has
glorified his own name will glorify it yet again, and to comfort
ourselves with that (v. 10, 11). We may, in singing it, apply it either
to our spiritual enemies, and be more than conquerors over them, or to
the public enemies of Christ\'s kingdom in the world and their
threatening insults, endeavouring to preserve a holy security and
serenity of mind when they seem most formidable. It is said of Luther
that, when he heard any discouraging news, he would say, Come let us
sing the forty-sixth psalm.

To the chief musician for the sons of Korah. A song upon Alamoth.

### Verses 1-5

The psalmist here teaches us by his own example.

`I.` To triumph in God, and his relation to us and presence with us,
especially when we have had some fresh experiences of his appearing in
our behalf (v. 1): God is our refuge and strength; we have found him so,
he has engaged to be so, and he ever will be so. Are we pursued? God is
our refuge to whom we may flee, and in whom we may be safe and think
ourselves so; secure upon good grounds, Prov. 18:10. Are we oppressed by
troubles? Have we work to do and enemies to grapple with? God is our
strength, to bear us up under our burdens, to fit us for all our
services and sufferings; he will by his grace put strength into us, and
on him we may stay ourselves. Are we in distress? He is a help, to do
all that for us which we need, a present help, a help found (so the word
is), one whom we have found to be so, a help on which we may write
Probatum est-It is tried, as Christ is called a tried stone, Isa. 28:16.
Or, a help at hand, one that never is to seek for, but that is always
near. Or, a help sufficient, a help accommodated to every case and
exigence; whatever it is, he is a very present help; we cannot desire a
better help, nor shall ever find the like in any creature.

`II.` To triumph over the greatest dangers: God is our strength and our
help, a God all-sufficient to us; therefore will not we fear. Those that
with a holy reverence fear God need not with any amazement to be afraid
of the power of hell or earth. If God be for us, who can be against us;
to do us any harm? It is our duty, it is our privilege, to be thus
fearless; it is an evidence of a clear conscience, of an honest heart,
and of a lively faith in God and his providence and promise: \"We will
not fear, though the earth be removed, though all our
creature-confidences fail us and sink us; nay, though that which should
support us threaten to swallow us up, as the earth did Korah,\" for
whose sons this psalm was penned, and, some think, by them; yet while we
keep close to God, and have him for us, we will not fear, for we have no
cause to fear;

-Si fractus illabatur orbis,

Impavidum ferient ruinae-Hor.

-Let Jove\'s dread arm

With thunder rend the spheres,

Beneath the crush of worlds undaunted he appears.

Observe here, 1. How threatening the danger is. We will suppose the
earth to be removed, and thrown into the sea, even the mountains, the
strongest and firmest parts of the earth, to lie buried in the
unfathomed ocean; we will suppose the sea to roar and rage, and make a
dreadful noise, and its foaming billows to insult the shore with so much
violence as even to shake the mountains, v. 3. Though kingdoms and
states be in confusion, embroiled in wars, tossed with tumults, and
their governments in continual revolution-though their powers combine
against the church and people of God, aim at no less than their ruin,
and go very near to gain their point-yet will not we fear, knowing that
all these troubles will end well for the church. See Ps. 93:4. If the
earth be removed, those have reason to fear who have laid up their
treasures on earth, and set their hearts upon it; but not those who have
laid up for themselves treasures in heaven, and who expect to be most
happy when the earth and all the works that are therein shall be burnt
up. Let those be troubled at the troubling of the waters who build their
confidence on such a floating foundation, but not those who are led to
the rock that is higher than they, and find firm footing upon that rock.
2. How well-grounded the defiance of this danger is, considering how
well guarded the church is, and that interest which we are concerned
for. It is not any private particular concern of our own that we are in
pain about; no, it is the city of God, the holy place of the tabernacles
of the Most High; it is the ark of God for which our hearts tremble.
But, when we consider what God has provided for the comfort and safety
of his church, we shall see reason to have our hearts fixed, and set
above the fear of evil tidings. Here is, `(1.)` Joy to the church, even in
the most melancholy and sorrowful times (v. 4): There is a river the
streams whereof shall make it glad, even then when the waters of the sea
roar and threaten it. It alludes to the waters of Siloam, which went
softly by Jerusalem (Isa. 8:6, 7): though of no great depth or breadth,
yet the waters of it were made serviceable to the defence of Jerusalem
in Hezekiah\'s time, Isa. 22:10, 11. But this must be understood
spiritually; the covenant of grace is the river, the promises of which
are the streams; or the Spirit of grace is the river (Jn. 7:38, 39), the
comforts of which are the streams, that make glad the city of our God.
God\'s word and ordinances are rivers and streams with which God makes
his saints glad in cloudy and dark days. God himself is to his church a
place of broad rivers and streams, Isa. 33:21. The streams that make
glad the city of God are not rapid, but gentle, like those of Siloam.
Note, The spiritual comforts which are conveyed to the saints by soft
and silent whispers, and which come not with observation, are sufficient
to counterbalance the most loud and noisy threatenings of an angry and
malicious world. `(2.)` Establishment to the church. Though heaven and
earth are shaken, yet God is in the midst of her, she shall not be
moved, v. 5. God has assured his church of his special presence with her
and concern for her; his honour is embarked in her, he has set up his
tabernacle in her and has undertaken the protection of it, and therefore
she shall not be moved, that is, `[1.]` Not destroyed, not removed, as
the earth may be v. 2. The church shall survive the world, and be in
bliss when that is in ruins. It is built upon a rock, and the gates of
hell shall not prevail against it. `[2.]` Not disturbed, not much moved,
with fears of the issue. If God be for us, if God be with us, we need
not be moved at the most violent attempts made against us. `(3.)`
Deliverance to the church, though her dangers be very great: God shall
help her; and who then can hurt her? He shall help her under her
troubles, that she shall not sink; nay, that the more she is afflicted
the more she shall multiply. God shall help her out of her troubles, and
that right early-when the morning appears; that is, very speedily, for
he is a present help (v. 1), and very seasonably, when things are
brought to the last extremity and when the relief will be most welcome.
This may be applied by particular believers to themselves; if God be in
our hearts, in the midst of us, by his word dwelling richly in us, we
shall be established, we shall be helped; let us therefore trust and not
be afraid; all is well, and will end well.

### Verses 6-11

These verses give glory to God both as King of nations and as King of
saints.

`I.` As King of nations, ruling the world by his power and providence, and
overruling all the affairs of the children of men to his own glory; he
does according to his will among the inhabitants of the earth, and none
may say, What doest thou? 1. He checks the rage and breaks the power of
the nations that oppose him and his interests in the world (v. 6): The
heathen raged at David\'s coming to the throne, and at the setting up of
the kingdom of the Son of David; compare Ps. 2:1, 2. The kingdoms were
moved with indignation, and rose in a tumultuous furious manner to
oppose it; but God uttered his voice, spoke to them in his wrath, and
they were moved in another sense, they were struck into confusion and
consternation, put into disorder, and all their measures broken; the
earth itself melted under them, so that they found no firm footing;
their earthly hearts failed them for fear, and dissolved like snow
before the sun. Such a melting of the spirits of the enemies is
described, Jdg. 5:4, 5; and see Lu. 21:25, 26. 2. When he pleases to
draw his sword, and give it commission, he can make great havoc among
the nations and lay all waste (v. 8): Come, behold the works of the
Lord; they are to be observed (Ps. 66:5), and to be sought out, Ps.
111:2. All the operations of Providence must be considered as the works
of the Lord, and his attributes and purposes must be taken notice of in
them. Particularly take notice of the desolations he has made in the
earth, among the enemies of his church, who thought to lay the land of
Israel desolate. The destruction they designed to bring upon the church
has been turned upon themselves. War is a tragedy which commonly
destroys the stage it is acted on; David carried the war into the
enemies\' country; and O what desolations did it make there! Cities were
burnt, countries laid waste, and armies of men cut off and laid in heaps
upon heaps. Come and see the effects of desolating judgments, and stand
in awe of God; say, How terrible art thou in thy works! Ps. 66:3. Let
all that oppose him see this with terror, and expect the same cup of
trembling to be put into their hands; let all that fear him and trust in
him see it with pleasure, and not be afraid of the most formidable
powers armed against the church. Let them gird themselves, but they
shall be broken to pieces. 3. When he pleases to sheathe his sword, he
puts an end to the wars of the nations and crowns them with peace, v. 9.
War and peace depend on his word and will, as much as storms and calms
at sea do, Ps. 107:25, 29. He makes wars to cease unto the end of the
earth, sometimes in pity to the nations, that they may have a
breathing-time, when, by long wars with each other, they have run
themselves out of breadth. Both sides perhaps are weary of the war, and
willing to let it fall; expedients are found out for accommodation;
martial princes are removed, and peace-makers set in their room; and
then the bow is broken by consent, the spear cut asunder and turned into
a pruning-hook, the sword beaten into a ploughshare, and the chariots of
war are burned, there being no more occasion for them; or, rather, it
may be meant of what he does, at other times, in favour of his own
people. He makes those wars to cease that were waged against them and
designed for their ruin. He breaks the enemies\' bow that was drawn
against them. No weapon formed against Zion shall prosper, Isa. 54:17.
The total destruction of Gog and Magog is prophetically described by the
burning of their weapons of war (Eze. 39:9, 10), which intimates
likewise the church\'s perfect security and assurance of lasting peace,
which made it needless to lay up those weapons of war for their own
service. The bringing of a long war to a good issue is a work of the
Lord, which we ought to behold with wonder and thankfulness.

`II.` As King of saints, and as such we must own that great and
marvellous are his works, Rev. 15:3. He does and will do great things,

`1.` For his own glory (v. 10): Be still, and know that I am God. `(1.)`
Let his enemies be still, and threaten no more, but know it, to their
terror, that he is God, one infinitely above them, and that will
certainly be too hard for them; let them rage no more, for it is all in
vain: he that sits in heaven, laughs at them; and, in spite of all their
impotent malice against his name and honour, he will be exalted among
the heathen and not merely among his own people, he will be exalted in
the earth and not merely in the church. Men will set up themselves, will
have their own way and do their own will; but let them know that God
will be exalted, he will have his way will do his own will, will glorify
his own name, and wherein they deal proudly he will be above them, and
make them know that he is so. `(2.)` Let his own people be still; let them
be calm and sedate, and tremble no more, but know, to their comfort,
that the Lord is God, he is God alone, and will be exalted above the
heathen; let him alone to maintain his honour, to fulfil his own
counsels and to support his own interest in the world. Though we be
depressed, yet let us not be dejected, for we are sure that God will be
exalted, and that may satisfy us; he will work for his great name, and
then no matter what becomes of our little names. When we pray, Father,
glorify thy name, we ought to exercise faith upon the answer given to
that prayer when Christ himself prayed it, I have both glorified it and
I will glorify it yet again. Amen, Lord, so be it.

`2.` For his people\'s safety and protection. He triumphs in the former:
I will be exalted; they triumph in this, v. 7 and again v. 11. It is the
burden of the song, \"The Lord of hosts is with us; he is on our side,
he takes our part, is present with us and president over us; the God of
Jacob is our refuge, to whom we may flee, and in whom we may confide and
be sure of safety.\" Let all believers triumph in this. `(1.)` They have
the presence of a God of power, of all power: The Lord of hosts is with
us. God is the Lord of hosts, for he has all the creatures which are
called the hosts of heaven and earth at his beck and command, and he
makes what use he pleases of them, as the instruments either of his
justice or of his mercy. This sovereign Lord is with us, sides with us,
acts with us, and has promised he will never leave us. Hosts may be
against us, but we need not fear them if the Lord of hosts be with us.
`(2.)` They are under the protection of a God in covenant, who not only is
able to help them, but is engaged in honour and faithfulness to help
them. He is the God of Jacob, not only Jacob the person, but Jacob the
people; nay, and of all praying people, the spiritual seed of wrestling
Jacob; and he is our refuge, by whom we are sheltered and in whom we are
satisfied, who by his providence secures our welfare when without are
fightings, and who by his grace quiets our minds, and establishes them,
when within are fears. The Lord of hosts, the God of Jacob, has been,
is, and will be with us-has been, is and will be our refuge: the
original includes all; and well may Selah be added to it. Mark this, and
take the comfort of it, and say, If God be for us, who can be against
us?
