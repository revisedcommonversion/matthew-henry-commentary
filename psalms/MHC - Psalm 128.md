Psalm 128
=========

Commentary
----------

This, as the former, is a psalm for families. In that we were taught
that the prosperity of our families depends upon the blessing of God; in
this we are taught that the only way to obtain that blessing which will
make our families comfortable is to live in the fear of God and in
obedience to him. Those that do so, in general, shall be blessed (v. 1,
2, 4), In particular, `I.` They shall be prosperous and successful in
their employments (v. 2). `II.` Their relations shall be agreeable (v. 3).
III. They shall live to see their families brought up (v. 6). `IV.` They
shall have the satisfaction of seeing the church of God in a flourishing
condition (v. 5, 6). We must sing this psalm in the firm belief of this
truth, That religion and piety are the best friends to outward
prosperity, giving God the praise that it is so and that we have found
it so, and encouraging ourselves and others with it.

A song of degrees.

### Verses 1-6

It is here shown that godliness has the promise of the life that now is
and of that which is to come.

`I.` It is here again and again laid down as an undoubted truth that those
who are truly holy are truly happy. Those whose blessed state we are
here assured of are such as fear the Lord and walk in his ways, such as
have a deep reverence of God upon their spirits and evidence it by a
regular and constant conformity to his will. Where the fear of God is a
commanding principle in the heart the tenour of the conversation will be
accordingly; and in vain do we pretend to be of those that fear God if
we do not make conscience both of keeping to his ways and not trifling
in them or drawing back. Such are blessed (v. 1), and shall be blessed,
v. 4. God blesses them, and his pronouncing them blessed makes them so.
They are blessed now, they shall be blessed still, and for ever. This
blessedness, arising from this blessing, is here secured, 1. To all the
saints universally: Blessed is everyone that fears the Lord, whoever he
be; in every nation he that fears God and works righteousness is
accepted of him, and therefore is blessed whether he be high or low,
rich or poor, in the world; if religion rule him, it will protect and
enrich him. 2. To such a saint in particular: Thus shall the man be
blessed, not only the nation, the church in its public capacity, but the
particular person in his private interests. 3. We are encouraged to
apply it to ourselves (v. 2): \"Happy shalt thou be; thou mayest take
the comfort of the promise, and expect the benefit of it, as if it were
directed to thee by name, if thou fear God and walk in his ways. Happy
shalt thou be, that is, It shall be well with thee; whatever befals
thee, good shall be brought out of it; it shall be well with thee while
thou livest, better when thou diest, and best of all to eternity.\" It
is asserted (v. 4) with a note commanding attention: Behold, thus shall
the man be blessed; behold it by faith in the promise; behold it by
observation in the performance of the promise; behold it with assurance
that it shall be so, for God is faithful, and with admiration that it
should be so, for we merit no favour, no blessing, from him.

`II.` Particular promises are here made to godly people, which they may
depend upon, as far as is for God\'s glory and their good; and that is
enough.

`1.` That, by the blessing of God, they shall get an honest livelihood
and live comfortably upon it. It is not promised that they shall live at
ease, without care or pains, but, Thou shalt eat the labour of thy
hands. Here is a double promise, `(1.)` That they shall have something to
do (for an idle life is a miserable uncomfortable life) and shall have
health, and strength, and capacity of mind to do it, and shall not be
forced to be beholden to others for necessary food, and to live, as the
disabled poor do, upon the labours of other people. It is as much a
mercy as it is a duty with quietness to work and eat our own bread, 2
Th. 3:12. `(2.)` That they shall succeed in their employments, and they
and theirs shall enjoy what they get; others shall not come and eat the
bread out of their mouths, nor shall it be taken from them either by
oppressive rulers or invading enemies. God will not blast it and blow
upon it (as he did, Hag. 1:9), and his blessing will make a little go a
great way. It is very pleasant to enjoy the fruits of our own industry;
as the sleep, so the food, of a labouring man is sweet.

`2.` That they shall have abundance of comfort in their family-relations.
As a wife and children are very much a man\'s care, so, if by the grace
of God they are such as they should be, they are very much a man\'s
delight, as much as any creature-comfort. `(1.)` The wife shall be as a
vine by the sides of the house, not only as a spreading vine which
serves for an ornament, but as a fruitful vine which is for profit, and
with the fruit whereof both God and man are honoured, Jdg. 9:13. The
vine is a weak and tender plant, and needs to be supported and
cherished, but it is a very valuable plant, and some think (because all
the products of it were prohibited to the Nazarites) it was the tree of
knowledge itself. The wife\'s place is the husband\'s house; there her
business lies, and that is her castle. Where is Sarah thy wife? Behold,
in the tent; where should she be else? Her place is by the sides of the
house, not under-foot to be trampled on, nor yet upon the house-top to
domineer (if she be so, she is but as the grass upon the house-top, in
the next psalm), but on the side of the house, being a rib out of the
side of the man. She shall be a loving wife, as the vine, which cleaves
to the house-side, an obedient wife, as the vine, which is pliable, and
grows as it is directed. She shall be fruitful as the vine, not only in
children, but in the fruits of wisdom, and righteousness, and good
management, the branches of which run over the wall (Gen. 49:22; Ps.
80:11), like a fruitful vine, not cumbering the ground, nor bringing
forth sour grapes, or grapes of Sodom, but good fruit. `(2.)` The children
shall be as olive plants, likely in time to be olive-trees, and, though
wild by nature, yet grafted into the good olive, and partaking of its
root and fatness, Rom. 11:17. It is pleasant to parents who have a table
spread, though but with ordinary fare, to see their children round about
it, to have many children, enough to surround it, and those with them,
and not scattered, or the parents forced from them. Job makes it one of
the first instances of his former prosperity that his children were
about him, Job 29:5. Parents love to have their children at table, to
keep up the pleasantness of the table-talk, to have them in health,
craving food and not physic, to have them like olive-plants, straight
and green, sucking in the sap of their good education, and likely in due
time to be serviceable.

`3.` That they shall have those things which God has promised and which
they pray for: The Lord shall bless thee out of Zion, where the ark of
the covenant was, and where the pious Israelites attended with their
devotions. Blessings out of Zion are the best-blessings, which flow, not
from common providence, but from special grace, Ps. 20:2.

`4.` That they shall live long, to enjoy the comforts of the rising
generations: \"Thou shalt see thy children\'s children, as Joseph, Gen.
50:23. Thy family shall be built up and continued, and thou shalt have
the pleasure of seeing it.\" Children\'s children, if they be good
children, are the crown of old men (Prov. 17:6), who are apt to be fond
of their grandchildren.

`5.` That they shall see the welfare of God\'s church, and the land of
their nativity, which every man who fears God is no less concerned for
than for the prosperity of his own family. \"Thou shalt be blessed in
Zion\'s blessing, and wilt think thyself so. Thou shalt see the good of
Jerusalem as long as thou shalt live, though thou shouldest live long,
and shalt not have thy private comforts allayed and embittered by public
troubles.\" A good man can have little comfort in seeing his children\'s
children, unless withal he see peace upon Israel, and have hopes of
transmitting the entail of religion pure and entire to those that shall
come after him, for that is the best inheritance.
