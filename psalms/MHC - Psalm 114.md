Psalm 114
=========

Commentary
----------

The deliverance of Israel out of Egypt gave birth to their church and
nation, which were then founded, then formed; that work of wonder ought
therefore to be had in everlasting remembrance. God gloried in it, in
the preface to the ten commandments, and Hos. 11:1, \"Out of Egypt have
I called my son.\" In this psalm it is celebrated in lively strains of
praise; it was fitly therefore made a part of the great Hallelujah, or
song of praise, which the Jews were wont to sing at the close of the
passover-supper. It must never be forgotten, `I.` That they were brought
out of slavery (v. 1). `II.` That God set up his tabernacle among them (v.
2). `III.` That the sea and Jordan were divided before them (v. 3, 5). `IV.`
That the earth shook at the giving of the law, when God came down on
Mount Sinai (v. 4, 6, 7). `V.` That God gave them water out of the rock
(v. 8). In singing this psalm we must acknowledge God\'s power and
goodness in what he did for Israel, applying it to the much greater work
of wonder, our redemption by Christ, and encouraging ourselves and
others to trust in God in the greatest straits.

### Verses 1-8

The psalmist is here remembering the days of old, the years of the right
hand of the Most High, and the wonders which their fathers told them of
(Jdg. 6:13), for time, as it does not wear out the guilt of sin, so it
should not wear out the sense of mercy. Let it never be forgotten,

`I.` That God brought Israel out of the house of bondage with a high hand
and a stretched-out arm: Israel went out of Egypt, v. 1. They did not
steal out clandestinely, nor were they driven out, but fairly went out,
marched out with all the marks of honour; they went out from a barbarous
people, that had used them barbarously, from a people of a strange
language, Ps. 81:5. The Israelites, it seems, preserved their own
language pure among them, and cared not for learning the language of
their oppressors. By this distinction from them they kept up an earnest
of their deliverance.

`II.` That he himself framed their civil and sacred constitution (v. 2):
Judah and Israel were his sanctuary, his dominion. When he delivered
them out of the hand of their oppressors it was that they might serve
him both in holiness and in righteousness, in the duties of religious
worship and in obedience to the moral law, in their whole conversation.
Let my people go, that they may serve me. In order to this, 1. He set up
his sanctuary among them, in which he gave them the special tokens of
his presence with them and promised to receive their homage and tribute.
Happy are the people that have God\'s sanctuary among them (see Ex.
25:8, Eze. 37:26), much more those that, like Judah here, are his
sanctuaries, his living temples, on whom Holiness to the Lord is
written. 2. He set up his dominion among them, was himself their
lawgiver and their judge, and their government was a theocracy: The Lord
was their King. All the world is God\'s dominion, but Israel was so in a
peculiar manner. What is God\'s sanctuary must be his dominion. Those
only have the privileges of his house that submit to the laws of it; and
for this end Christ has redeemed us that he might bring us into God\'s
service and engage us for ever in it.

`III.` That the Red Sea was divided before them at their coming out of
Egypt, both for their rescue and the ruin of their enemies; and the
river Jordan, when they entered into Canaan, for their honour, and the
confusion and terror of their enemies (v. 3): The sea saw it, saw there
that Judah was God\'s sanctuary, and Israel his dominion, and therefore
fled; for nothing could be more awful. It was this that drove Jordan
back, and was an invincible dam to his streams; God was at the head of
that people, and therefore they must give way to them, must make room
for them, they must retire, contrary to their nature, when God speaks
the word. To illustrate this the psalmist asks, in a poetical strain (v.
5), What ailed thee, O thou sea! that thou fleddest? And furnishes the
sea with an answer (v. 7); it was at the presence of the Lord. This is
designed to express, 1. The reality of the miracle, that it was not by
any power of nature, or from any natural cause, but it was at the
presence of the Lord, who gave the word. 2. The mercy of the miracle:
What ailed thee? Was it in a frolic? Was it only to amuse men? No; it
was at the presence of the God of Jacob; it was in kindness to the
Israel of God, for the salvation of that chosen people, that God was
thus displeased against the rivers, and his wrath was against the sea,
as the prophet speaks, Hab. 3:8-13; Isa. 51:10; 63:11, etc. 3. The
wonder and surprise of the miracle. Who would have thought of such a
thing? Shall the course of nature be changed, and its fundamental laws
dispensed with, to serve a turn for God\'s Israel? Well may the dukes of
Edom be amazed and the mighty men of Moab tremble, Ex. 15:15. 4. The
honour hereby put upon Israel, who are taught to triumph over the sea,
and Jordan, as unable to stand before them. Note, There is no sea, no
Jordan, so deep, so broad, but, when God\'s time shall come for the
redemption of his people, it shall be divided and driven back if it
stand in their way. Apply this, `(1.)` To the planting of the Christian
church in the world. What ailed Satan and the powers of darkness, that
they trembled and truckled as they did? Mk. 1:34. What ailed the heathen
oracles, that they were silenced, struck dumb, struck dead? What ailed
their idolatries and witchcrafts, that they died away before the gospel,
and melted like snow before the sun? What ailed the persecutors and
opposers of the gospel, that they gave up their cause, hid their guilty
heads, and called to rocks and mountains for shelter? Rev. 6:15. It was
at the presence of the Lord, and that power which went along with the
gospel. `(2.)` To the work of grace in the heart. What turns the stream in
a regenerate soul? What ails the lusts and corruptions, that they fly
back, that the prejudices are removed and the whole man has become new?
It is at the presence of God\'s Spirit that imaginations are cast down,
2 Co. 10:5.

`IV.` That the earth shook and trembled when God came down on Mount Sinai
to give the law (v. 4): The mountains skipped like rams, and then the
little hills might well be excused if they skipped like lambs, either
when they are frightened or when they sport themselves. The same power
that fixed the fluid waters and made them stand still shook the stable
mountains and made them tremble for all the powers of nature are under
the check of the God of nature. Mountains and hills are, before God, but
like rams and lambs; even the bulkiest and the most rocky are as
manageable by him as they are by the shepherd. The trembling of the
mountains before the Lord may shame the stupidity and obduracy of the
children of men, who are not moved at the discoveries of his glory. The
psalmist asks the mountains and hills what ailed them to skip thus; and
he answers for them, as for the seas, it was at the presence of the
Lord, before whom, not only those mountains, but the earth itself, may
well tremble (v. 7), since it has lain under a curse for man\'s sin. See
Ps. 104:32; Isa. 64:3, 4. He that made the hills and mountains to skip
thus can, when he pleases, dissipate the strength and spirit of the
proudest of his enemies and make them tremble.

`V.` That God supplied them with water out of the rock, which followed
them through the dry and sandy deserts. Well may the earth and all its
inhabitants tremble before that God who turned the rock into a standing
water (v. 8), and what cannot he do who did that? The same almighty
power that turned waters into a rock to be a wall to Israel (Ex. 14:22)
turned the rock into waters to be a well to Israel: as they were
protected, so they were provided for, by miracles, standing miracles;
for such was the standing water, that fountain of waters into which the
rock, the flinty rock, was turned, and that rock was Christ, 1 Co. 10:4.
For he is a fountain of living waters to his Israel, from whom they
receive grace for grace.
