Psalm 112
=========

Commentary
----------

This psalm is composed alphabetically, as the former is, and is (like
the former) entitled \"Hallelujah,\" though it treats of the happiness
of the saints, because it redounds to the glory of God, and whatever we
have the pleasure of he must have the praise of. It is a comment upon
the last verse of the foregoing psalm, and fully shows how much it is
our wisdom to fear God and do his commandments. We have here, `I.` The
character of the righteous (v. 1). `II.` The blessedness of the righteous.
`1.` There is a blessing entailed upon their posterity (v. 2). 2. There is
a blessing conferred upon themselves. `(1.)` Prosperity outward and inward
(v. 3). `(2.)` Comfort (v. 4). `(3.)` Wisdom (v. 5). `(4.)` Stability (v.
6-8). `(5.)` Honour (v. 6, 9). `III.` The misery of the wicked (v. 10). So
that good and evil are set before us, the blessing and the curse. In
singing this psalm we must not only teach and admonish ourselves and one
another to answer to the characters here given of the happy, but comfort
and encourage ourselves and one another with the privileges and comforts
here secured to the holy.

### Verses 1-5

The psalmist begins with a call to us to praise God, but immediately
applies himself to praise the people of God; for whatever glory is
acknowledged to be on them it comes from God, and must return to him; as
he is their praise, so they are his. We have reason to praise the Lord
that there are a people in the world who fear him and serve him, and
that they are a happy people, both which are owing entirely to the grace
of God. Now here we have,

`I.` A description of those who are here pronounced blessed, and to whom
these promises are made.

`1.` They are well-principled with pious and devout affections. Those
have the privileges of God\'s subjects, not who cry, Lord, Lord, but who
are indeed well affected to his government. `(1.)` They are such as stand
in awe of God and have a constant reverence for his majesty and
deference to his will. The happy man is he that fears the Lord, v. 1.
`(2.)` They are such as take a pleasure in their duty. He that fears the
Lord, as a Father, with the disposition of a child, not of a slave,
delights greatly in his commandments, is well pleased with them and with
the equity and goodness of them; they are written in his heart; it is
his choice to be under them, and he calls them an easy, a pleasant,
yoke; it is his delight to be searching into and conversing with God\'s
commandments, by reading, hearing, and meditation, Ps. 1:2. He delights
not only in God\'s promises, but in his precepts, and thinks himself
happy under God\'s government as well as in his favour. It is a pleasure
to him to be found in the way of his duty, and he is in his element when
he is in the service of God. Herein he delights greatly, more than in
any of the employments and enjoyments of this world. And what he does in
religion is done from principle, because he sees amiableness in religion
and advantage by it.

`2.` They are honest and sincere in their professions and intentions.
They are called the upright (v. 2, 4), who are really as good as they
seem to be, and deal faithfully both with God and man. There is no true
religion without sincerity; that is gospel-perfection.

`3.` They are both just and kind in all their dealings: He is gracious,
full of compassion, and righteous (v. 4), dares not do any wrong to any
man, but does to every man all the good he can, and that from a
principle of compassion and kindness. It was said of God, in the
foregoing psalm (v. 4), He is gracious, and full of compassion; and here
it is said of the good man that he is so; for herein we must be
followers of God as dear children; be merciful as he is. He is full of
compassion, and yet righteous; what he does good with is what he came
honestly by. God hates robbery for burnt-offerings, and so does he. One
instance is given of his beneficence (v. 5): He shows favour and lends.
Sometimes there is as much charity in lending as in giving, as it
obliges the borrower both to industry and honesty. He is gracious and
lends (Ps. 37:26); he does it from a right principle, not as the usurer
lends for his own advantage, nor merely out of generosity, but out of
pure charity; he does it in a right manner, not grudgingly, but
pleasantly, and with a cheerful countenance.

`II.` The blessedness that is here entailed upon those that answer to
these characters. Happiness, all happiness, to the man that feareth the
Lord. Whatever men think or say of them, God says that they are blessed;
and his saying so makes them so.

`1.` The posterity of good men shall fare the better for his goodness (v.
2): His seed shall be mighty on earth. Perhaps he himself shall not be
so great in the world, nor make such a figure, as his seed after him
shall for his sake. Religion has been the raising of many a family, if
not so as to advance it high, yet so as to fix it firmly. When good men
themselves are happy in heaven their seed perhaps are considerable on
earth, and will themselves own that it is by virtue of a blessing
descending from them. The generation of the upright shall be blessed; if
they tread in their steps, they shall be the more blessed for their
relation to them, beloved for the Father\'s sake (Rom. 11:28), for so
runs the covenant-I will be a God to thee, and to thy seed; while the
seed of evil-doers shall never be renowned. Let the children of godly
parents value themselves upon it, and take heed of doing any thing to
forfeit the blessing entailed upon the generation of the upright.

`2.` They shall prosper in the world, and especially their souls shall
prosper, v. 3. `(1.)` They shall be blessed with outward prosperity as far
as is good for them: Wealth and riches shall be in the upright man\'s
house, not in his heart (for he is none of those in whom the love of
money reigns), perhaps not so much in his hand (for he only begins to
raise the estate), but in his house; his family shall grow rich when he
is gone. But, `(2.)` That which is much better is that they shall be
blessed with spiritual blessings, which are the true riches. His wealth
shall be in his house, for he must leave that to others; but his
righteousness he himself shall have the comfort of to himself, it
endures for ever. Grace is better than gold, for it will outlast it. He
shall have wealth and riches, and yet shall keep up his religion, and in
a prosperous condition shall still hold fast his integrity, which many,
who kept it in the storm, throw off and let go in the sunshine. Then
worldly prosperity is a blessing when it does not make men cool in their
piety, but they still persevere in that; and when this endures in the
family, and goes along with the wealth and riches, and the heirs of the
father\'s estate inherit his virtues too, that is a happy family indeed.
However, the good man\'s righteousness endures for ever in the crown of
righteousness which fades not away.

`3.` They shall have comfort in affliction (v. 4): Unto the upright there
arises light in the darkness. It is here implied that good men may be in
affliction; the promise does not exempt them from that. They shall have
their share in the common calamities of human life; but, when they sit
in darkness, the Lord shall be a light to them, Mic. 7:8. They shall be
supported and comforted under their troubles; their spirits shall be
lightsome when their outward condition is clouded. Sat lucis intus-There
is light enough within. During the Egyptian darkness the Israelites had
light in their dwellings. They shall be in due time, and perhaps when
they least expect it, delivered out of their troubles; when the night is
darkest the day dawns; nay, at evening-time, when night was looked for,
it shall be light.

`4.` They shall have wisdom for the management of all their concerns, v.
5. He that does good with his estate shall, through the providence of
God, increase it, not by miracle, but by his prudence: He shall guide
his affairs with discretion, and his God instructs him to discretion and
teaches him, Isa. 28:26. It is part of the character of a good man that
he will use his discretion in managing his affairs, in getting and
saving, that he may have to give. It may be understood of the affairs of
his charity: He shows favour and lends; but then it is with discretion,
that his charity may not be misplaced, that he may give to proper
objects what is proper to be given and in due time and proportion. And
it is part of the promise to him who thus uses discretion that God will
give him more. Those who most use their wisdom see most of their need of
it, and ask it of God, who has promised to give it liberally, Jam. 1:5.
He will guide his words with judgment (so it is in the original); and
there is nothing in which we have more occasion for wisdom than in the
government of the tongue; blessed is he to whom God gives that wisdom.

### Verses 6-10

In these verses we have,

`I.` The satisfaction of saints, and their stability. It is the happiness
of a good man that he shall not be moved for ever, v. 6. Satan and his
instruments endeavour to move him, but his foundation is firm and he
shall never be moved, at least not moved for ever; if he be shaken for a
time, yet he settles again quickly.

`1.` A good man will have a settled reputation, and that is a great
satisfaction. A good man shall have a good name, a name for good things,
with God and good people: The righteous shall be in everlasting
remembrance (v. 6); in this sense his righteousness (the memorial of it)
endures for ever, v. 9. There are those that do all they can to sully
his reputation and to load him with reproach; but his integrity shall be
cleared up, and the honour of it shall survive him. Some that have been
eminently righteous are had in a lasting remembrance on earth; wherever
the scripture is read their good deeds are told for a memorial of them.
And the memory of many a good man that is dead and gone is still
blessed; but in heaven their remembrance shall be truly everlasting, and
the honour of their righteousness shall there endure for ever, with the
reward of it, in the crown of glory that fades not away. Those that are
forgotten on earth, and despised, are remembered there, and honoured,
and their righteousness found unto praise, and honour, and glory (1 Pt.
1:7); then, at furthest, shall the horn of a good man be exalted with
honour, as that of the unicorn when he is a conqueror. Wicked men, now
in their pride, lift up their horns on high, but they shall all be cut
off, Ps. 75:5, 10. The godly, in their humility and humiliation, have
defiled their horn in the dust (Job 16:15); but the day is coming when
it shall be exalted with honour. That which shall especially turn to the
honour of good men is their liberality and bounty to the poor: He has
dispersed, he has given to the poor; he has not suffered his charity to
run all in one channel, or directed it to some few objects that he had a
particular kindness for, but he has dispersed it, given a portion to
seven and also to eight, has sown beside all waters, and by thus
scattering he has increased: and this is his righteousness, which
endures for ever. Alms are called righteousness, not because they will
justify us by making atonement for our evil deeds, but because they are
good deeds, which we are bound to perform; so that if we are not
charitable we are not just; we withhold good from those to whom it is
due. The honour of this endures for ever, for it shall be taken notice
of in the great day. I was hungry, and you gave me meat. This is quoted
as an inducement and encouragement to charity, 2 Co. 9:9.

`2.` A good man shall have a settled spirit, and that is a much greater
satisfaction than the former; for so shall a man have rejoicing in
himself alone, and not in another. Surely he shall not be moved,
whatever happens, not moved either from his duty or from his comfort;
for he shall not be afraid; his heart is established, v. 7, 8. This is a
part both of the character and of the comfort of good people. It is
their endeavour to keep their minds stayed upon God, and so to keep them
calm, and easy, and undisturbed; and God has promised them both cause to
do so and grace to do so. Observe, `(1.)` It is the duty and interest of
the people of God not to be afraid of evil tidings, not to be afraid of
hearing bad news; and, when they do, not to be put into confusion by it
and into an amazing expectation of worse and worse, but whatever
happens, whatever threatens, to be able to say, with blessed Paul, None
of these things move me, neither will I fear, though the earth be
removed, Ps. 46:2. `(2.)` The fixedness of the heart is a sovereign remedy
against the disquieting fear of evil tidings. If we keep our thoughts
composed, and ourselves masters of them, our wills resigned to the holy
will of God, our temper sedate, and our spirits even, under all the
unevenness of Providence, we are well fortified against the agitations
of the timorous. `(3.)` Trusting in the Lord is the best an surest way of
fixing and establishing the heart. By faith we must cast anchor in the
promise, in the word of God, and so return to him and repose in him as
our rest. The heart of man cannot fix any where, to its satisfaction,
but in the truth of God, and there it finds firm footing. `(4.)` Those
whose hearts are established by faith will patiently wait till they have
gained their point: He shall not be afraid, till he see his desire upon
his enemies, that is, till he come to heaven, where he shall see Satan,
and all his spiritual enemies, trodden under his feet, and, as Israel
saw the Egyptians, dead on the sea-shore. Till he look upon his
oppressors (so Dr. Hammond), till he behold them securely, and look
boldly in their faces, as being now no longer under their power. It will
complete the satisfaction of the saints, when they shall look back upon
their troubles and pressures, and be able to say with St. Paul, when he
had recounted the persecutions he endured (2 Tim. 3:11), But out of them
all the Lord delivered me.

`II.` The vexation of sinners, v. 10. Two things shall fret them:-1. The
felicity of the righteous: The wicked shall see the righteous in
prosperity and honour and shall be grieved. It will vex them to see
their innocency cleared and their low estate regarded, and those whom
they hated and despised, and whose ruin they sought and hoped to see,
the favourites of Heaven, and advanced to have dominion over them (Ps.
49:14); this will make them gnash with their teeth and pine away. This
is often fulfilled in this world. The happiness of the saints is the
envy of the wicked, and that envy is the rottenness of their bones. But
it will most fully be accomplished in the other world, when it shall
make damned sinners gnash with their teeth, to see Abraham afar off, and
Lazarus in him bosom, to see all the prophets in the kingdom of God and
themselves thrust out. 2. Their own disappointment: The desire of the
wicked shall perish. Their desire was wholly to the world and the flesh,
and they ruled over them; and therefore, when these perish, their joy is
gone, and their expectations from them are cut off, to their everlasting
confusion; their hope is as a spider\'s web.
