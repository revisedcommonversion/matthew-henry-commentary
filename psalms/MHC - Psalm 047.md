Psalm 47
========

Commentary
----------

The scope of this psalm is to stir us up to praise God, to stir up all
people to do so; and, `I.` We are directed in what manner to do it,
publicly, cheerfully, and intelligently (v. 1, 6, 7). `II.` We are
furnished with matter for praise. 1. God\'s majesty (v. 2). 2. His
sovereign and universal dominion (v. 2, 7-9). 3. The great things he had
done, and will do, for his people (v. 3-5). Many suppose that this psalm
was penned upon occasion of the bringing up of the ark to Mount Zion
which v. 5 seems to refer to (\"God has gone up with a shout\");-but it
looks further, to the ascension of Christ into the heavenly Zion, after
he had finished his undertaking on earth, and to the setting up of his
kingdom in the world, to which the heathen should become willing
subjects. In singing this psalm we are to give honour to the exalted
Redeemer, to rejoice in his exaltation, and to celebrate his praises,
confessing that he is Lord, to the glory of God the Father.

To the chief musician. A psalm for the sons of Korah.

### Verses 1-4

The psalmist, having his own heart filled with great and good thoughts
of God, endeavours to engage all about him in the blessed work of
praise, as one convinced that God is worthy of all blessing and praise,
and as one grieved at his own and others\' backwardness to and
barrenness in this work. Observe, in these verses,

`I.` Who are called upon to praise God: \"All you people, all you people
of Israel;\" those were his own subjects, and under his charge, and
therefore he will engage them to praise God, for on them he has an
influence. Whatever others do, he and his house, he and his people,
shall praise the Lord. Or, \"All you people and nations of the earth;\"
and so it may be taken as a prophecy of the conversion of the Gentiles
and the bringing of them into the church; see Rom. 15:11.

`II.` What they are called upon to do: \"O clap your hands, in token of
your own joy and satisfaction in what God has done for you, of your
approbation, nay, your admiration, of what God has done in general, and
of your indignation against all the enemies of God\'s glory, Job 27:23.
Clap your hands, as men transported with pleasure, that cannot contain
themselves; shout unto God, not to make him hear (his ear is not heavy),
but to make all about you hear, and take notice how much you are
affected and filled with the works of God. Shout with the voice of
triumph in him, and in his power and goodness, that others may join with
you in the triumph.\" Note, Such expressions of pious and devout
affections as to some may seem indecent and imprudent ought not to be
hastily censured and condemned, much less ridiculed, because, if they
come from an upright heart, God will accept the strength of the
affection and excuse the weakness of the expressions of it.

`III.` What is suggested to us as matter for our praise. 1. That the God
with whom we have to do is a God of awful majesty (v. 2): The Lord most
high is terrible. He is infinitely above the noblest creatures, higher
than the highest; there are those perfections in him that are to be
reverenced by all, and particularly that power, holiness, and justice,
that are to be dreaded by all those that contend with him. 2. That he is
a God of sovereign and universal dominion. He is a King that reigns
alone, and with an absolute power, a King over all the earth; all the
creatures, being made by him, are subject to him, and therefore he is a
great King, the King of kings. 3. That he takes a particular care of his
people and their concerns, has done so and ever will; `(1.)` In giving
them victory and success (v. 3), subduing the people and nations under
them, both those that stood in their way (Ps. 44:2) and those that made
attempts upon them. This God had done for them, witness the planting of
them in Canaan, and their continuance there unto this day. This they
doubted not but he would still do for them by his servant David, who
prospered which way soever he turned his victorious arms. But this looks
forward to the kingdom of the Messiah, which was to be set over all the
earth, and not confined to the Jewish nation. Jesus Christ shall subdue
the Gentiles; he shall bring them in as sheep into the fold (so the word
signifies), not for slaughter, but for preservation. He shall subdue
their affections, and make them a willing people in the day of his
power, shall bring their thoughts into obedience to him, and reduce
those who had gone astray, under the guidance of the great shepherd and
bishop of souls, 1 Pt. 2:25. `(2.)` In giving them rest and settlement (v.
4): He shall choose our inheritance for us. He had chosen the land of
Canaan to be an inheritance for Israel; it was the land which the Lord
their God spied out for them; see Deu. 32:8. This justified their
possession of that land, an d gave them a good title; and this sweetened
their enjoyment of it, and made it comfortable; they had reason to think
it a happy lot, and to be satisfied in it, when it was that which
Infinite Wisdom chose for them. And the setting up of God\'s sanctuary
in it made it the excellency, the honour, of Jacob (Amos 6:8); and he
chose so good an inheritance for Jacob because he loved him, Deu. 7:8.
Apply this spiritually, and it bespeaks, `[1.]` The happiness of the
saints, that God himself has chosen their inheritance for them, and it
is a goodly heritage: he has chosen it who knows the soul, and what will
serve to make it happy; and he has chosen so well that he himself has
undertaken to be the inheritance of his people (Ps. 16:5), and he has
laid up for them in the other world an inheritance incorruptible, 1 Pt.
1:4. This will be indeed the excellency of Jacob, for whom, because he
loved them, he prepared such a happiness as eye has not seen. `[2.]` The
faith and submission of the saints to God. This is the language of every
gracious soul, \"God shall choose my inheritance for me; let him appoint
me my lot, and I will acquiesce in the appointment. He knows what is
good for me better than I do for myself, and therefore I will have no
will of my own but what is resolved into his.\"

### Verses 5-9

We are here most earnestly pressed to praise God, and to sing his
praises; so backward are we to this duty that we have need to be urged
to it by precept upon precept, and line upon line; so we are here (v.
6): Sing praises to God, and again, Sing praises, Sing praises to our
King, and again, Sing praises. This intimates that it is a very
necessary and excellent duty, that it is a duty we ought to be frequent
and abundant in; we may sing praises again and again in the same words,
and it is no vain repetition if it be done with new affections. Should
not a people praise their God? Dan. 5:4. Should not subjects praise
their king? God is our God, our King, and therefore we must praise him;
we must sing his praises, as those that are pleased with them and that
are not ashamed of them. But here is a needful rule subjoined (v. 7):
Sing you praises with understanding, with Maschil. 1. \"Intelligently;
as those that do yourselves understand why and for what reasons you
praise God and what is the meaning of the service.\" This is the
gospel-rule (1 Co. 14:15), to sing with the spirit and with the
understanding also; it is only with the heart that we make melody to the
Lord, Eph. 5:19. It is not an acceptable service if it be not a
reasonable service. 2. \"Instructively, as those that desire to make
others understand God\'s glorious perfections, and to teach them to
praise him.\" Three things are mentioned in these verses as just matter
for our praises, and each of them will admit of a double sense:-

`I.` We must praise God going up (v. 5): God has gone up with a shout,
which may refer, 1. To the carrying up of the ark to the hill of Zion,
which was done with great solemnity, David himself dancing before it,
the priests, it is likely, blowing the trumpets, and the people
following with their loud huzzas. The ark being the instituted token of
God\'s special presence with them, when that was brought up by warrant
from him he might be said to go up. The emerging of God\'s ordinances
out of obscurity, in order to the more public and solemn administration
of them, is a great favour to any people, which they have reason to
rejoice in and give thanks for. 2. To the ascension of our Lord Jesus
into heaven, when he had finished his work on earth, Acts 1:9. Then God
went up with a shout, the shout of a King, of a conqueror, as one who,
having spoiled principalities and powers, then led captivity captive,
Ps. 68:18. He went up as a Mediator, typified by the ark and the
mercy-seat over it, and was brought as the ark was into the most holy
place, into heaven itself; see Heb. 9:24. We read not of a shout, or of
the sound of a trumpet, at the ascension of Christ, but they were the
inhabitants of the upper world, those sons of God, that then shouted for
joy, Job 38:7. He shall come again in the same manner as he went (Acts
1:11) and we are sure that he shall come again with a shout and the
sound of a trumpet.

`II.` We must praise God reigning, v. 7. 8. God is not only our King, and
therefore we owe our homage to him, but he is King of all the earth (v.
7), over all the kings of the earth, and therefore in every place the
incense of praise is to be offered up to him. Now this may be
understood, 1. Of the kingdom of providence. God, as Creator, and the
God of nature, reigns over the heathen, disposes of them and all their
affairs, as he pleases, though they know him not, nor have any regard to
him: He sits upon the throne of his holiness, which he has prepared in
the heavens, and there he rules over all, even over the heathen, serving
his own purposes by them and upon them. See here the extent of God\'s
government; all are born within his allegiance; even the heathen that
serve other gods are ruled by the true God, our God, whether they will
or no. See the equity of his government; it is a throne of holiness, on
which he sits, whence he gives warrants, orders, and judgment, in which
we are sure there is no iniquity. 2. Of the kingdom of the Messiah.
Jesus Christ, who is God, and whose throne is for ever and ever reigns
over the heathen; not only he is entrusted with the administration of
the providential kingdom, but he shall set up the kingdom of his grace
in the Gentile world, and rule in the hearts of multitudes that were
bred up in heathenism, Eph. 2:12, 13. This the apostle speaks of as a
great mystery that the Gentiles should be fellow-heirs, Eph. 3:6. Christ
sits upon the throne of his holiness, his throne in the heavens, where
all the administrations of his government are intended to show forth
God\'s holiness and to advance holiness among the children of men.

`III.` We must praise God as attended and honoured by the princes of the
people, v. 9. This may be understood, 1. Of the congress or convention
of the states of Israel, the heads and rulers of the several tribes, at
the solemn feasts, or to despatch the public business of the nation. It
was the honour of Israel that they were the people of the God of
Abraham, as they were Abraham\'s seed and taken into his covenant; and,
thanks be to God, this blessing of Abraham has come upon the isles of
the Gentiles, Gal. 3:14. It was their happiness that they had a settled
government, princes of their people, who were the shields of their land.
Magistracy is the shield of a nation, and it is a great mercy to any
people to have this shield, especially when their princes, their
shields, belong unto the Lord, are devoted to his honour, and their
power is employed in his service, for then he is greatly exalted. It is
likewise the honour of God that, in another sense, the shields of the
earth do belong to him; magistracy is his institution, and he serves his
own purposes by it in the government of the world, turning the hearts of
kings as the rivers of water, which way soever he pleases. It was well
with Israel when the princes of their people were gathered together to
consult for the public welfare. The unanimous agreement of the great
ones of a nation in the things that belong to its peace is a very happy
omen, which promises abundance of blessings. 2. It may be applied to the
calling of the Gentiles into the church of Christ, and taken as a
prophecy that in the days of the Messiah the kings of the earth and
their people should join themselves to the church, and bring their glory
and power into the New Jerusalem, that they should all become the people
of the God of Abraham, to whom it was promised that he should be the
father of many nations. The volunteers of the people (so it may be
read); it is the same word that is used in Ps. 110:3, Thy people shall
be willing; for those that are gathered to Christ are not forced, but
made freely willing, to be his. When the shields of the earth, the
ensigns of royal dignity (1 Ki. 14:27, 28,), are surrendered to the Lord
Jesus, as the keys of a city are presented to the conqueror or
sovereign, when princes use their power for the advancement of the
interests of religion, then Christ is greatly exalted.
