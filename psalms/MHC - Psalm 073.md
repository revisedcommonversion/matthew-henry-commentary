Psalm 73
========

Commentary
----------

This psalm, and the ten that next follow it, carry the name of Asaph in
the titles of them. If he was the penman of them (as many think), we
rightly call them psalms of Asaph. If he was only the chief musician, to
whom they were delivered, our marginal reading is right, which calls
them psalms for Asaph. It is probable that he penned them; for we read
of the words of David and of Asaph the seer, which were used in praising
God in Hezekiah\'s time, 2 Chr. 29:30. Though the Spirit of prophecy by
sacred songs descended chiefly on David, who is therefore styled \"the
sweet psalmist of Israel,\" yet God put some of that Spirit upon those
about him. This is a psalm of great use; it gives us an account of the
conflict which the psalmist had with a strong temptation to envy the
prosperity of wicked people. He begins his account with a sacred
principle, which he held fast, and by the help of which he kept his
ground and carried his point (v. 1). He then tells us, `I.` How he got
into the temptation (v. 2-14). `II.` How he got out of the temptation and
gained a victory over it (v. 15-20). `III.` How he got by the temptation
and was the better for it (v. 21-23). If, in singing this psalm, we
fortify ourselves against the life temptation, we do not use it in vain.
The experiences of others should be our instructions.

A psalm of Asaph.

### Verses 1-14

This psalm begins somewhat abruptly: Yet God is good to Israel (so the
margin reads it); he had been thinking of the prosperity of the wicked;
while he was thus musing the fire burned, and at last he spoke by way of
check to himself for what he had been thinking of. \"However it be, yet
God is good.\" Though wicked people receive many of the gifts of his
providential bounty, yet we must own that he is, in a peculiar manner,
good to Israel; they have favours from him which others have not.

The psalmist designs an account of a temptation he was strongly
assaulted with-to envy the prosperity of the wicked, a common
temptation, which has tried the graces of many of the saints. Now in
this account,

`I.` He lays down, in the first place, that great principle which he is
resolved to abide by and not to quit while he was parleying with this
temptation, v. 1. Job, when he was entering into such a temptation,
fixed for his principle the omniscience of God: Times are not hidden
from the Almighty, Job 24:1. Jeremiah\'s principle is the justice of
God: Righteous art thou, O God! when I plead with thee, Jer. 12:1.
Habakkuk\'s principle is the holiness of God: Thou art of purer eyes
than to behold iniquity, Hab. 1:13. The psalmist\'s, here, is the
goodness of God. These are truths which cannot be shaken and which we
must resolve to live and die by. Though we may not be able to reconcile
all the disposals of Providence with them, we must believe they are
reconcilable. Note, Good thoughts of God will fortify us against many of
Satan\'s temptations. Truly God is good; he had had many thoughts in his
mind concerning the providences of God, but this word, at last, settled
him: \"For all this, God is good, good to Israel, even to those that are
of a clean heart.\" Note, 1. Those are the Israel of God that are of a
clean heart, purified by the blood of Christ, cleansed from the
pollutions of sin, and entirely devoted to the glory of God. An upright
heart is a clean heart; cleanness is truth in the inward part. 2. God,
who is good to all, is in a special manner good to his church and
people, as he was to Israel of old. God was good to Israel in redeeming
them out of Egypt, taking them into covenant with himself, giving them
his laws and ordinances, and in the various providences that related to
them; he is, in like manner, good to all those that are of a clean
heart, and, whatever happens, we must not think otherwise.

`II.` He comes now to relate the shock that was given to his faith in
God\'s distinguishing goodness to Israel by a strong temptation to envy
the prosperity of the wicked, and therefore to think that the Israel of
God are no happier than other people and that God is no kinder to them
than to others.

`1.` He speaks of it as a very narrow escape that he had not been quite
foiled and overthrown by this temptation (v. 2): \"But as for me, though
I was so well satisfied in the goodness of God to Israel, yet my feet
were almost gone (the tempter had almost tripped up my heels), my steps
had well-nigh slipped (I had like to have quitted my religion, and given
up all my expectations of benefit by it); for I was envious at the
foolish.\" Note, 1. The faith even of strong believers may sometimes be
sorely shaken and ready to fail them. There are storms that will try the
firmest anchors. 2. Those that shall never be quite undone are sometimes
very near it, and, in their own apprehension, as good as gone. Many a
precious soul, that shall live for ever, had once a very narrow turn for
its life; almost and well-nigh ruined, but a step between it and fatal
apostasy, and yet snatched as a brand out of the burning, which will for
ever magnify the riches of divine grace in the nations of those that are
saved. Now,

`2.` Let us take notice of the process of the psalmist\'s temptation,
what he was tempted with and tempted to.

`(1.)` He observed that foolish wicked people have sometimes a very great
share of outward prosperity. He saw, with grief, the prosperity of the
wicked, v. 3. Wicked people are really foolish people, and act against
reason and their true interest, and yet every stander-by sees their
prosperity. `[1.]` They seem to have the least share of the troubles and
calamities of this life (v. 5): They are not in the troubles of other
men, even of wise and good men, neither are they plagued like other men,
but seem as if by some special privilege they were exempted from the
common lot of sorrows. If they meet with some little trouble, it is
nothing to what others endure that are less sinners and yet greater
sufferers. `[2.]` They seem to have the greatest share of the comforts
of this life. They live at ease, and bathe themselves in pleasures, so
that their eyes stand out with fatness, v. 7. See what the excess of
pleasure is; the moderate use of it enlightens the eyes, but those that
indulge themselves inordinately in the delights of sense have their eyes
ready to start out of their heads. Epicures are really their own
tormentors, by putting a force upon nature, while they pretend to
gratify it. And well may those feed themselves to the full who have more
than heart could wish, more than they themselves ever thought of or
expected to be masters of. They have, at least, more than a humble,
quiet, contented heart could wish, yet not so much as they themselves
wish for. There are many who have a great deal of this life in their
hands, but nothing of the other life in their hearts. They are ungodly,
live without the fear and worship of God, and yet they prosper and get
on in the world, and not only are rich, but increase in riches, v. 12.
They are looked upon as thriving men; while others have much ado to keep
what they have, they are still adding more, more honour, power,
pleasure, by increasing in riches. They are the prosperous of the age,
so some read it. `[3.]` Their end seems to be peace. This is mentioned
first, as the most strange of all, for peace in death was every thought
to be the peculiar privilege of the godly (Ps. 37:37), yet, to outward
appearance, it is often the lot of the ungodly (v. 4): There are no
bands in their death. They are not taken off by a violent death; they
are foolish, and yet die not as fools die; for their hands are not bound
nor their feet put into fetters, 2 Sa. 3:33, 34. They are not taken off
by an untimely death, like the fruit forced from the tree before it is
ripe, but are left to hang on, till, through old age, they gently drop
of themselves. They do not die of sore and painful diseases: There are
no pangs, no agonies, in their death, but their strength is firm to the
last, so that they scarcely feel themselves die. They are of those who
die in their full strength, being wholly at ease and quiet, not of those
that die in the bitterness of their souls and never eat with pleasure,
Job 21:23, 25. Nay, they are not bound by the terrors of conscience in
their dying moments; they are not frightened either with the remembrance
of their sins or the prospect of their misery, but die securely. We
cannot judge of men\'s state on the other side death either by the
manner of their death or the frame of their spirits in dying. Men may
die like lambs, and yet have their place with the goats.

`(2.)` He observed that they made a very bad use of their outward
prosperity and were hardened by it in their wickedness, which very much
strengthened the temptation he was in to fret at it. If it had done them
any good, if it had made them less provoking to God or less oppressive
to man, it would never have vexed him; but it had quite a contrary
effect upon them. `[1.]` It made them very proud and haughty. Because
they live at ease, pride compasses them as a chain, v. 6. They show
themselves (to all that see them) to be puffed up with their prosperity,
as men show their ornaments. The pride of Israel testifies to his face,
Hos. 5:5; Isa. 3:9. Pride ties on their chain, or necklace; so Dr.
Hammond reads it. It is no harm to wear a chain or necklace; but when
pride ties it on, when it is worn to gratify a vain mind, it ceases to
be an ornament. It is not so much what the dress or apparel is (though
we have rules for that, 1 Tim. 2:9) as what principle ties it on and
with what spirit it is worn. And, as the pride of sinners appears in
their dress, so it does in their talk: They speak loftily (v. 8); they
affect great swelling words of vanity (2 Pt. 2:18), bragging of
themselves and disdaining all about them. Out of the abundance of the
pride that is in their heart they speak big. `[1.]` It made them
oppressive to their poor neighbours (v. 6): Violence covers them as a
garment. What they have got by fraud and oppression they keep and
increase by the same wicked methods, and care not what injury they do to
others, nor what violence they use, so they may but enrich and
aggrandize themselves. They are corrupt, like the giants, the sinners of
the old world, when the earth was filled with violence, Gen. 6:11, 13.
They care not what mischief they do, either for mischief-sake or for
their own advantage-sake. They speak wickedly concerning oppression;
they oppress, and justify themselves in it. Those that speak well of sin
speak wickedly of it. They are corrupt, that is, dissolved in pleasures
and every thing that is luxurious (so some), and then they deride and
speak maliciously; they care not whom they wound with the poisoned darts
of calumny; from on high they speak oppression. `[3.]` It made them very
insolent in their demeanour towards both God and man (v. 9): They set
their mouth against the heavens, putting contempt upon God himself and
his honour, bidding defiance to him and his power and justice. They
cannot reach the heavens with their hands, to shake God\'s throne, else
they would; but they show their ill-will by setting their mouth against
the heavens. Their tongue also walks through the earth, and they take
liberty to abuse all that come in their way. No man\'s greatness or
goodness can secure him from the scourge of the virulent tongue. They
take a pride and pleasure in bantering all mankind; they are pests of
the country, for they neither fear God nor regard man. `[4.]` In all
this they were very atheistical and profane. They could not have been
thus wicked if they had not learned to say (v. 11), How doth God know?
And is there knowledge in the Most High? So far were they from desiring
the knowledge of God, who gave them all the good things they had and
would have taught them to use them well, that they were not willing to
believe God had any knowledge of them, that he took any notice of their
wickedness or would ever call them to an account. As if, because he is
Most High, he could not or would not see them, Job 22:12, 13. Whereas
because he is Most High therefore he can, and will, take cognizance of
all the children of men and of all they do, or say, or think. What an
affront is it to the God of infinite knowledge, from whom all knowledge
is, to ask, Is there knowledge in him? Well may he say (v. 12), Behold,
these are the ungodly.

`(3.)` He observed that while wicked men thus prospered in their impiety,
and were made more impious by their prosperity, good people were in
great affliction, and he himself in particular, which very much
strengthened the temptation he was in to quarrel with Providence. `[1.]`
He looked abroad and saw many of God\'s people greatly at a loss (v.
10): \"Because the wicked are so very daring therefore his people return
hither; they are at the same pause, the same plunge, that I am at; they
know not what to say to it any more than I do, and the rather because
waters of a full cup are wrung out to them; they are not only made to
drink, and to drink deeply, of the bitter cup of affliction, but to
drink all. Care is taken that they lose not a drop of that unpleasant
potion; the waters are wrung out unto them, that they may have the dregs
of the cup. They pour out abundance of tears when they hear wicked
people blaspheme God and speak profanely,\" as David did, Ps. 119:136.
These are the waters wrung out to them. `[2.]` He looked at home, and
felt himself under the continual frowns of Providence, while the wicked
were sunning themselves in its smiles (v. 14): \"For my part,\" says he,
\"all the day long have I been plagued with one affliction or another,
and chastened every morning, as duly as the morning comes.\" His
afflictions were great-he was chastened and plagued; the returns of them
were constant, every morning with the morning, and they continued,
without intermission, all the day long. This he thought was very hard,
that, when those who blasphemed God were in prosperity, he that
worshipped God was under such great affliction. He spoke feelingly when
he spoke of his own troubles; there is no disputing against sense,
except by faith.

`(4.)` From all this arose a very strong temptation to cast off his
religion. `[1.]` Some that observed the prosperity of the wicked,
especially comparing it with the afflictions of the righteous, were
tempted to deny a providence and to think that God had forsaken the
earth. In this sense some take v. 11. There are those, even among God\'s
professing people, that say, \"How does God know? Surely all things are
left to blind fortune, and not disposed of by an all-seeing God.\" Some
of the heathen, upon such a remark as this, have asked, Quis putet esse
deos?-Who will believe that there are gods? `[2.]` Though the
psalmist\'s feet were not so far gone as to question God\'s omniscience,
yet he was tempted to question the benefit of religion, and to say (v.
13), Verily, I have cleansed my heart in vain, and have, to no purpose,
washed my hands in innocency. See here what it is to be religious; it is
to cleanse our hearts, in the first place, by repentance and
regeneration, and then to wash our hands in innocency by a universal
reformation of our lives. It is not in vain to do this, not in vain to
serve God and keep his ordinances; but good men have been sometimes
tempted to say, \"It is in vain,\" and \"Religion is a thing that there
is nothing to be got by,\" because they see wicked people in prosperity.
But, however the thing may appear now, when the pure in heart, those
blessed ones, shall see God (Mt. 5:8), they will not say that they
cleansed their hearts in vain.

### Verses 15-20

We have seen what a strong temptation the psalmist was in to envy
prospering profaneness; now here we are told how he kept his footing and
got the victory.

`I.` He kept up a respect for God\'s people, and with that he restrained
himself from speaking what he had thought amiss, v. 15. He got the
victory by degrees, and this was the first point he gained; he was ready
to say, Verily, I have cleansed my heart in vain, and thought he had
reason to say it, but he kept his mouth with this consideration, \"If I
say, I will speak thus, behold, I should myself revolt and apostatize
from, and so give the greatest offence imaginable to, the generation of
thy children.\" Observe here, 1. Though he thought amiss, he took care
not to utter that evil thought which he had conceived. Note, It is bad
to think ill, but it is worse to speak it, for that is giving the evil
thought an imprimatur-a sanction; it is allowing it, giving consent to
it, and publishing it for the infection of others. But it is a good sign
that we repent of the evil imagination of the heart if we suppress it,
and the error remains with ourselves. If therefore thou hast been so
foolish as to think evil, be so wise as to lay thy hand upon thy mouth,
and let it go no further, Prov. 30:32. If I say, I will speak thus.
Observe, Though his corrupt heart made this inference from the
prosperity of the wicked, yet he did not mention it to those whether it
were fit to be mentioned or no. Note, We must think twice before we
speak once, both because some things may be thought which yet may not be
spoken and because the second thoughts may correct the mistakes of the
first. 2. The reason why he would not speak it was for fear of giving
offence to those whom God owned for his children. Note, `(1.)` There are a
people in the world that are the generation of God\'s children, a set of
men that hear and love God as their Father. `(2.)` We must be very careful
not to say or do any thing which may justly offend any of these little
ones (Mt. 18:6), especially which may offend the generation of them, may
sadden their hearts, or weaken their hands, or shake their interest.
`(3.)` There is nothing that can give more general offence to the
generation of God\'s children than to say that we have cleansed our
heart in vain or that it is vain to serve God; for there is nothing more
contrary to their universal sentiment and experience nor any thing that
grieves them more than to hear God thus reflected on. `(4.)` Those that
wish themselves in the condition of the wicked do in effect quit the
tents of God\'s children.

`II.` He foresaw the ruin of wicked people. By this he baffled the
temptation, as by the former he gave some check to it. Because he durst
not speak what he had thought, for fear of giving offence, he began to
consider whether he had any good reason for that thought (v. 16): \"I
endeavoured to understand the meaning of this unaccountable dispensation
of Providence; but it was too painful for me. I could not conquer it by
the strength of my own reasoning.\" It is a problem, not to be solved by
the mere light of nature, for, if there were not another life after
this, we could not fully reconcile the prosperity of the wicked with the
justice of God. But (v. 17) he went into the sanctuary of God; he
applied to his devotions, meditated upon the attributes of God, and the
things revealed, which belong to us and to our children; he consulted
the scriptures, and the lips of the priests who attended the sanctuary;
he prayed to God to make this matter plain to him and to help him over
this difficulty; and, at length, he understood the wretched end of
wicked people, which he plainly foresaw to be such that even in the
height of their prosperity they were rather to be pitied than envied,
for they were but ripening for ruin. Note, There are many great things,
and things needful to be known, which will not be known otherwise than
by going into the sanctuary of God, by the word and prayer. The
sanctuary must therefore be the resort of a tempted soul. Note, further,
We must judge of persons and things as they appear by the light of
divine revelation, and then we shall judge righteous judgment;
particularly we must judge by the end. All is well that ends well,
everlastingly well; but nothing well that ends ill, everlastingly ill.
The righteous man\'s afflictions end in peace, and therefore he is
happy; the wicked man\'s enjoyments end in destruction, and therefore he
is miserable.

`1.` The prosperity of the wicked is short and uncertain. The high places
in which Providence sets them are slippery places (v. 18), where they
cannot long keep footing; but, when they offer to climb higher, that
very attempt will be the occasion of their sliding and falling. Their
prosperity has no firm ground; it is not built upon God\'s favour or his
promise; and they have not the satisfaction of feeling that it rests on
firm ground.

`2.` Their destruction is sure, and sudden, and very great. This cannot
be meant of any temporal destruction; for they were supposed to spend
all their days in wealth and their death itself had no bands in it: In a
moment they go down to the grace, so that even that could scarcely be
called their destruction; it must therefore be meant of eternal
destruction on the other side death-hell and destruction. They flourish
for a time, but are undone for ever. `(1.)` Their ruin is sure and
inevitable. He speaks of it as a thing done-They are cast down; for
their destruction is as certain as if it were already accomplished. He
speaks of it as God\'s doing, and therefore it cannot be resisted: Thou
castest them down. It is destruction from the Almighty (Joel 1:15), from
the glory of his power, 2 Th. 1:9. Who can support those whom God will
cast down, on whom God will lay burdens? `(2.)` It is swift and sudden;
their damnation slumbers not; for how are they brought into desolation
as in a moment! v. 19. It is easily effected, and will be a great
surprise to themselves and all about them. `(3.)` It is severe and very
dreadful. It is a total and final ruin: They are utterly consumed with
terrors, It is the misery of the damned that the terrors of the
Almighty, whom they have made their enemy, fasten upon their guilty
consciences, which can neither shelter themselves from them nor
strengthen themselves under them; and therefore not their being, but
their bliss, must needs be utterly consumed by them; not the least
degree of comfort or hope remains to them; the higher they were lifted
up in their prosperity the sorer will their fall be when they are cast
down into destructions (for the word is plural) and suddenly brought
into desolation.

`3.` Their prosperity is therefore not to be envied at all, but despised
rather, quod erat demonstrandum-which was the point to be established,
v. 20. As a dream when one awaketh, so, O Lord! when thou awakest, or
when they awake (as some read it), thou shalt despise their image, their
shadow, and make it to vanish. In the day of the great judgment (so the
Chaldee paraphrase reads it), when they are awaked out of their graves,
thou shalt, in wrath, despise their image; for they shall rise to shame
and everlasting contempt. See here, `(1.)` What their prosperity now is;
it is but an image, a vain show, a fashion of the world that passes
away; it is not real, but imaginary, and it is only a corrupt
imagination that makes it a happiness; it is not substance, but a mere
shadow; it is not what it seems to be, nor will it prove what we promise
ourselves from it; it is as a dream, which may please us a little, while
we are asleep, yet even then it disturbs our repose; but, how pleasing
soever it is, it is all but a cheat, all false; when we awake we find it
so. A hungry man dreams that he eats, but he awakes and his soul is
empty, Isa. 29:8. A man is never the more rich or honourable for
dreaming he is so. Who therefore will envy a man the pleasure of a
dream? `(2.)` What will be the issue of it; God will awake to judgment, to
plead his own and his people\'s injured cause; they shall be made to
awake out of the sleep of their carnal security, and then God shall
despise their image; he shall make it appear to all the world how
despicable it is; so that the righteous shall laugh at them, Ps. 52:6,
7. How did God despise that rich man\'s image when he said, Thou fool,
this night thy soul shall be required of thee! Lu. 12:19, 20. We ought
to be of God\'s mind, for his judgment is according to truth, and not to
admire and envy that which he despises and will despise; for, sooner or
later, he will bring all the world to be of his mind.

### Verses 21-28

Behold Samson\'s riddle again unriddled, Out of the eater came forth
meat, and out of the strong sweetness; for we have here an account of
the good improvement which the psalmist made of that sore temptation
with which he had been assaulted and by which he was almost overcome. He
that stumbles and does not fall, by recovering himself takes so much the
longer steps forward. It was so with the psalmist here; many good
lessons he learned from his temptation, his struggles with it, and his
victories over it. Nor would God suffer his people to be tempted if his
grace were not sufficient for them, not only to save them from harm, but
to make them gainers by it; even this shall work for good.

`I.` He learned to think very humbly of himself and to abase and accuse
himself before God (v. 21, 22); he reflects with shame upon the disorder
and danger he was in, and the vexation he gave himself by entertaining
the temptation and parleying with it: My heart was grieved, and I was
pricked in my reins, as one afflicted with the acute pain of the stone
in the region of the kidneys. If evil thoughts at any time enter into
the mind of a good man, he does not roll them under his tongue as a
sweet morsel, but they are grievous and painful to him; temptation was
to Paul as a thorn in the flesh, 2 Co. 12:7. This particular temptation,
the working of envy and discontent, is as painful as any; where it
constantly rests it is the rottenness of the bones (Prov. 14:30); where
it does but occasionally come it is the pricking of the reins.
Fretfulness is a corruption that is its own correction. Now in the
reflection upon it, 1. He owns it was his folly thus to vex himself:
\"So foolish was I to be my own tormentor.\" Let peevish people thus
reproach themselves for, and shame themselves out of, their discontents.
\"What a fool am I thus to make myself uneasy without a cause?\" 2. He
owns it was his ignorance to vex himself at this: \"So ignorant was I of
that which I might have known, and which, if I had known it aright,
would have been sufficient to silence my murmurs. I was as a beast
(Behemoth-a great beast) before thee. Beasts mind present things only,
and never look before at what is to come; and so did `I.` If I had not
been a great fool, I should never have suffered such a senseless
temptation to prevail over me so far. What! to envy wicked men upon
account of their prosperity! To be ready to wish myself one of them, and
to think of changing conditions with them! So foolish was `I.`\" Note, If
good men do at any time, through the surprise and strength of
temptation, think, or speak, or act amiss, when they see their error
they will reflect upon it with sorrow, and shame, and self-abhorrence,
will call themselves fools for it. Surely I am more brutish than any
man, Prov. 30:2; Job 42:5, 6. Thus David, 2 Sa. 24:10.

`II.` He took occasion hence to own his dependence on and obligations to
the grace of God (v. 23): \"Nevertheless, foolish as I am, I am
continually with thee and in thy favour; thou hast holden me by my right
hand.\" This may refer either, 1. To the care God had taken of him, and
the kindness he had shown him, all along from his beginning hitherto. He
had said, in the hour of temptation (v. 14), All the day long have I
been plagued; but here he corrects himself for that passionate
complaint: \"Though God has chastened me, he has not cast me off;
notwithstanding all the crosses of my life, I have been continually with
thee; I have had thy presence with me, and thou hast been nigh unto me
in all that which I have called upon thee for; and therefore, though
perplexed, yet not in despair. Though God has sometimes written bitter
things against me, yet he has still holden me by my right hand, both to
keep me, that I should not desert him or fly off from him, and to
prevent my sinking and fainting under my burdens, or losing my way in
the wildernesses through which I have walked.\" If we have been kept in
the way with God, kept closely in our duty and upheld in our integrity,
we must own ourselves indebted to the free grace of God for our
preservation: Having obtained help of God, I continue hitherto. And, if
he has thus maintained the spiritual life, the earnest of eternal life,
we ought not to complain, whatever calamities of this present time we
have met with. Or, 2. To the late experience he had had of the power of
divine grace in carrying him through this strong temptation and bringing
him off a conqueror: \"I was foolish and ignorant, and yet thou hast had
compassion on me and taught me (Heb. 5:2), and kept me under thy
protection;\" for the unworthiness of man is no bar to the free grace of
God. We must ascribe our safety in temptation, and our victory over it,
not to our own wisdom, for we are foolish and ignorant, but to the
gracious presence of God with us and the prevalency of Christ\'s
intercession for us, that our faith may not fail: \"My feet were almost
gone, and they would have quite gone, past recovery, but that thou hast
holden me by my right hand and so kept me from falling.\"

`III.` He encouraged himself to hope that the same God who had delivered
him from this evil work would preserve him to his heavenly kingdom, as
St. Paul does (2 Tim. 4:18): \"I am now upheld by thee, therefore thou
shalt guide me with thy counsel, leading me, as thou hast done hitherto,
many a difficult step; and, since I am now continually with thee, thou
shalt afterwards receive me to glory\" v. 24. This completes the
happiness of the saints, so that they have no reason to envy the worldly
prosperity of sinners. Note, 1. All those who commit themselves to God
shall be guided with his counsel, with the counsel both of his word and
of his Spirit, the best counsellors. The psalmist had like to have paid
dearly for following his own counsels in this temptation and therefore
resolves for the future to take God\'s advice, which shall never be
wanting to those that duly seek it with a resolution to follow it. 2.
All those who are guided and led by the counsel of God in this world
shall be received to his glory in another world. If we make God\'s glory
in us the end we aim at, he will make our glory with him the end we
shall for ever be happy in. Upon this consideration, let us never envy
sinners, but rather bless ourselves in our own blessedness. If God
direct us in the way of our duty, and prevent our turning aside out of
it, he will afterwards, when our state of trial and preparation is over,
receive us to his kingdom and glory, the believing hopes and prospects
of which will reconcile us to all the dark providences that now puzzle
and perplex us, and ease us of the pain we have been put into by some
threatening temptations.

`IV.` He was hereby quickened to cleave the more closely to God, and very
much confirmed and comforted in the choice he had made of him, v. 25,
26. His thoughts here dwell with delight upon his own happiness in God,
as much greater then the happiness of the ungodly that prospered in the
world. He saw little reason to envy them what they had in the creature
when he found how much more and better, surer and sweeter, comforts he
had in the Creator, and what cause he had to congratulate himself on
this account. He had complained of his afflictions (v. 14); but this
makes them very light and easy, All is well if God be mine. We have here
the breathings of a sanctified soul towards God, and its repose in him,
as that to a godly man really which the prosperity of a worldly man is
to him in conceit and imagination: Whom have I in heaven but thee? There
is scarcely a verse in all the psalms more expressive than this of the
pious and devout affections of a soul to God; here it soars up towards
him, follows hard after him, and yet, at the same time, has an entire
satisfaction and complacency in him.

`1.` It is here supposed that God alone is the felicity and chief good of
man. He, and he only, that made the soul, can make it happy; there is
none in heaven, none in earth, that can pretend to do it besides.

`2.` Here are expressed the workings and breathings of a soul towards God
accordingly. If God be our felicity,

`(1.)` Then we must have him (Whom have I but thee?), we must choose him,
and make sure to ourselves an interest in him. What will it avail us
that he is the felicity of souls if he be not the felicity of our souls,
and if we do not by a lively faith make him ours, by joining ourselves
to him in an everlasting covenant?

`(2.)` Then our desire must be towards him and our delight in him (the
word signifies both); we must delight in what we have of God and desire
what we yet further hope for. Our desires must not only be offered up to
God, but they must all terminate in him, desiring nothing more than God,
but still more and more of him. This includes all our prayers, Lord,
give us thyself; as that includes all the promises, I will be to them a
God. The desire of our souls is to thy name.

`(3.)` We must prefer him in our choice and desire before any other.
`[1.]` \"There is none in heaven but thee, none to seek to or trust in,
none to court or covet acquaintance with, but thee.\" God is in himself
more glorious than any celestial being (Ps. 89:6), and must be, in our
eyes, infinitely more desirable. Excellent beings there are in heaven,
but God alone can make us happy. His favour is infinitely more to us
than the refreshment of the dews of heaven or the benign influence of
the stars of heaven, more than the friendship of the saints in heaven or
the good offices of the angels there. `[2.]` I desire none on earth
besides thee; not only none in heaven, a place at a distance, which we
have but little acquaintance with, but none on earth neither, where we
have many friends and where much of our present interest and concern
lie. \"Earth carries away the desires of most men, and yet I have none
on earth, no persons, no things, no possessions, no delights, that I
desire besides thee or with thee, in comparison or competition with
thee.\" We must desire nothing besides God but what we desire for him
(nil praeter te nisi propter te-nothing besides thee except for thy
sake), nothing but what we desire from him, and can be content without
so that it be made up in him. We must desire nothing besides God as
needful to be a partner with him in making us happy.

`(4.)` Then we must repose ourselves in God with an entire satisfaction,
v. 26. Observe here, `[1.]` Great distress and trouble supposed: My
flesh and my heart fail. Note, Others have experienced and we must
expect, the failing both of flesh and heart. The body will fail by
sickness, age, and death; and that which touches the bone and the flesh
touches us in a tender part, that part of ourselves which we have been
but too fond of; when the flesh fails the heart is ready to fail too;
the conduct, courage, and comfort fail. `[2.]` Sovereign relief provided
in this distress: But God is the strength of my heart and my portion for
ever. Note, Gracious souls, in their greatest distresses, rest upon God
as their spiritual strength and their eternal portion. First, \"He is
the strength of my heart, the rock of my heart, a firm foundation, which
will bear my weight and not sink under it. God is the strength of my
heart; I have found him so; I do so still, and hope ever to find him
so.\" In the distress supposed, he had put the case of a double failure,
both flesh and heart fail; but, in the relief, he fastens on a single
support: he leaves out the flesh and the consideration of that, it is
enough that God is the strength of his heart. He speaks as one careless
of the body (let that fail, there is no remedy), but as one concerned
about the soul, to be strengthened in the inner man. Secondly, \"He is
my portion for ever; he will not only support me while I am here, but
make me happy when I go hence.\" The saints choose God for their
portion, they have him for their portion, and it is their happiness that
he will be their portion, a portion that will last as long as the
immortal soul lasts.

`V.` He was fully convinced of the miserable condition of all wicked
people. This he learned in the sanctuary upon this occasion, and he
would never forget it (v. 27): \"Lo, those that are far from thee, in a
state of distance and estrangement, that desire the Almighty to depart
from them, shall certainly perish; so shall their doom be; they choose
to be far from God, and they shall be far from him for ever. Thou wilt
justly destroy all those that go a whoring from thee, that is, all
apostates, that in profession have been betrothed to God, but forsake
him, their duty to him and their communion with him, to embrace the
bosom of a stranger.\" The doom is sever, no less than perishing and
being destroyed. It is universal: \"They shall all be destroyed without
exception.\" It is certain: \"Thou hast destroyed; it is as sure to be
done as if done already; and the destruction of some ungodly men is an
earnest of the perdition of all.\" God himself undertakes to do it, into
whose hands it is a fearful thing to fall: \"Thou, though infinite in
goodness, wilt reckon for thy injured honour and abused patience, and
wilt destroy those that go a whoring from thee.\"

`VI.` He was greatly encouraged to cleave to God and to confide in him,
v. 28. If those that are far from God shall perish, then, 1. Let this
constrain us to live in communion with God; \"if it fare so ill with
those that live at a distance from him, then it is good, very good, the
chief good, that good for a man, in this life, which he should most
carefully pursue and secure, it is best for me to draw near to God, and
to have God draw near to me;\" the original may take in both. But for my
part (so I would read it) the approach of God is good for me. Our
drawing near to God takes rise from his drawing near to us, and it is
the happy meeting that makes the bliss. Here is a great truth laid down,
That it is good to draw near to God; but the life of it lies in the
application, \"It is good for me.\" Those are the wise who know what is
good for themselves: \"It is good, says he (and every good man agrees
with him in it), it is good for me to draw near to God; it is my duty;
it is my interest.\" 2. Let us therefore live in a continual dependence
upon him: \"I have put my trust in the Lord God, and will never go a
whoring from him after any creature confidences.\" If wicked men,
notwithstanding all their prosperity, shall perish and be destroyed,
then let us trust in the Lord God, in him, not in them (see Ps.
146:3-5), in him, and not in our worldly prosperity; let us trust in
God, and neither fret at them nor be afraid of them; let us trust in him
for a better portion than theirs is. 3. While we do so, let us not doubt
but that we shall have occasion to praise his name. Let us trust in the
Lord, that we may declare all his works. Note, Those that with an
upright heart put their trust in God shall never want matter for
thanksgiving to him.
