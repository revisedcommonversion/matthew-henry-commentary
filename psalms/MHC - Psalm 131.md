Psalm 131
=========

Commentary
----------

This psalm is David\'s profession of humility, humbly made, with
thankfulness to God for his grace, and not in vain-glory. It is probable
enough that (as most interpreters suggest) David made this protestation
in answer to the calumnies of Saul and his courtiers, who represented
David as an ambitious aspiring man, who, under pretence of a divine
appointment, sought the kingdom, in the pride of his heart. But he
appeals to God, that, on the contrary, `I.` He aimed at nothing high nor
great (v. 1). `II.` He was very easy in every condition which God allotted
him (v. 2); and therefore, `III.` He encourages all good people to trust
in God as he did (v. 3). Some have made it an objection against singing
David\'s psalms that there are many who cannot say, \"My heart is not
haughty,\" etc. It is true there are; but we may sing it for the same
purpose that we read it, to teach and admonish ourselves, and one
another, what we ought to be, with repentance that we have come short of
being so, and humble prayer to God for his grace to make us so.

A song of degrees of David.

### Verses 1-3

Here are two things which will be comforts to us:-

`I.` Consciousness of our integrity. This was David\'s rejoicing, that his
heart could witness for him that he had walked humbly with his God,
notwithstanding the censures he was under and the temptations he was in.

`1.` He aimed not at a high condition, nor was he desirous of making a
figure in the world, but, if God had so ordered, could have been well
content to spend all his days, as he did in the beginning of them, in
the sheep-folds. His own brother, in a passion, charged him with pride
(1 Sa. 17:28), but the charge was groundless and unjust. God, who
searches the heart, knew, `(1.)` That he had no conceited opinion of
himself, or his own merits: Lord, my heart is not haughty. Humble saints
cannot think so well of themselves as others think of them, are not in
love with their own shadow, nor do they magnify their own attainments or
achievements. The love of God reigning in the heart will subdue all
inordinate self-love. `(2.)` That he had neither a scornful nor an
aspiring look: \"My eyes are not lofty, either to look with envy upon
those that are above me or to look with disdain upon those that are
below me.\" Where there is a proud heart there is commonly a proud look
(Prov. 6:17), but the humble publican will not so much as lift up his
eyes. `(3.)` That he did not employ himself in things above his station,
in things too great or too high for him. He did not employ himself in
studies too high; he made God\'s word his meditation, and did not amuse
himself with matters of nice speculation or doubtful disputation, or
covet to be wise above what is written. To know God and our duty is
learning sufficiently high for us. He did not employ himself in affairs
too great; he followed his ewes, and never set up for a politician; no,
nor for a soldier; for, when his brethren went to the wars, he staid at
home to keep the sheep. It is our wisdom, and will be our praise, to
keep within our sphere, and not to intrude into things which we have not
seen, or meddle with that which does not belong to us. Princes and
scholars must not exercise themselves in matters too great, too high,
for men: and those in a low station, and of ordinary capacities, must
not pretend to that which is out of their reach, and which they were not
cut out for. Those will fall under due shame that affect undue honours.

`2.` He was well reconciled to every condition that God placed him in (v.
2): I have behaved and quieted myself as a child that is weaned of his
mother. As he had not proudly aimed at the kingdom, so, since God had
appointed him to it, he had not behaved insolently towards any, nor been
restless in his attempts to get the crown before the time set; but, `(1.)`
He had been as humble as a little child about the age of a weanling, as
manageable and governable, and as far from aiming at high things; as
entirely at God\'s disposal as the child at the disposal of the mother
or nurse; as far from taking state upon him, though anointed to be king,
or valuing himself upon the prospect of his future advancement, as a
child in the arms. Our Saviour has taught us humility by this comparison
(Mt. 18:3); we must become as little children. `(2.)` He had been as
indifferent to the wealth and honour of this world as a child is to the
breast when it is thoroughly weaned from it. I have levelled and quieted
myself (so Dr. Hammond reads it) as a child that is weaned. This
intimates that our hearts are naturally as desirous of worldly things as
the babe is of the breast, and in like manner relish them, cry for them,
are fond of them, play with them, and cannot live without them. But, by
the grace of God, a soul that is sanctified, is weaned from those
things. Providence puts wormwood upon the breast, and that helps to wean
us. The child is perhaps cross and fretful while it is in the weaning
and thinks itself undone when it has lost the breast. But in a day or
two it is forgotten; the fret is over, and it accommodates itself well
enough to a new way of feeding, cares no longer for milk, but can bear
strong meat. Thus does a gracious soul quiet itself under the loss of
that which it loved and disappointment in that which it hoped for, and
is easy whatever happens, lives, and lives comfortably, upon God and the
covenant-grace, when creatures prove dry breasts. When our condition is
not to our mind we must bring our mind to our condition; and then we are
easy to ourselves and all about us; then our souls are as a weaned
child.

`II.` Confidence in God; and this David recommends to all Israel of God,
no doubt from his own experience of the benefit of it (v. 3): Let Israel
hope in the Lord, and let them continue to do so henceforth and for
ever. Though David could himself wait patiently and quietly for the
crown designed him, yet perhaps Israel, the people whose darling he was,
would be ready to attempt something in favour of him before the time;
and therefore endeavours to quiet them too, and bids them hope in the
Lord that they should see a happy change of the face of affairs in due
time. Thus it is good to hope and quietly to wait for the salvation of
the Lord.
