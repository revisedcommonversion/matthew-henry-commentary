Psalm 21
========

Commentary
----------

As the foregoing psalm was a prayer for the king that God would protect
and prosper him, so this is a thanksgiving for the success God had
blessed him with. Those whom we have prayed for we ought to give thanks
for, and particularly for kings, in whose prosperity we share. They are
here taught, `I.` To congratulate him on his victories, and the honour he
had achieved (v. 1-6). `II.` To confide in the power of God for the
completing of the ruin of the enemies of his kingdom (v. 7-13). In this
there is an eye to Messiah the Prince, and the glory of his kingdom; for
to him divers passages in this psalm are more applicable than to David
himself.

To the chief musician. A psalm of David.

### Verses 1-6

David here speaks for himself in the first place, professing that his
joy was in God\'s strength and in his salvation, and not in the strength
or success of his armies. He also directs his subjects herein to rejoice
with him, and to give God all the glory of the victories he had
obtained; and all with an eye to Christ, of whose triumphs over the
powers of darkness David\'s victories were but shadows. 1. They here
congratulate the king on his joys and concur with him in them (v. 1):
\"The king rejoices, he uses to rejoice in thy strength, and so do we;
what pleases the king pleases us,\" 2 Sa. 3:36. Happy the people the
character of whose king it is that he makes God\'s strength his
confidence and God\'s salvation his joy, that is pleased with all the
advancements of God\'s kingdom and trusts God to bear him out in all he
does for the service of it. Our Lord Jesus, in his great undertaking,
relied upon help from heaven, and pleased himself with the prospect of
that great salvation which he was thereby to work out. 2. They gave God
all the praise of those things which were the matter of their king\'s
rejoicing. `(1.)` That God had heard his prayers (v. 2): Thou hast given
him his heart\'s desire (and there is no prayer accepted but what is the
heart\'s desire), the very thing they begged of God for him, Ps. 20:4.
Note, God\'s gracious returns of prayer do, in a special manner, require
our humble returns of praise. When God gives to Christ the heathen for
his inheritance, gives him to see his seed, and accepts his intercession
for all believers, he give him his heart\'s desire. `(2.)` That God had
surprised him with favours, and much outdone his expectations (v. 3):
Thou preventest him with the blessings of goodness. All our blessings
are blessings of goodness, and are owing, not at all to any merit of
ours, but purely and only to God\'s goodness. But the psalmist here
reckons it in a special manner obliging that these blessings were given
in a preventing way; this fixed his eye, enlarged his soul, and endeared
his God, as one expresses it. When God\'s blessings come sooner and
prove richer than we imagine, when they are given before we prayed for
them, before we were ready for them, nay, when we feared the contrary,
then it may be truly said that he prevented us with them. Nothing indeed
prevented Christ, but to mankind never was any favour more preventing
than our redemption by Christ and all the blessed fruits of his
mediation. `(3.)` That God had advanced him to the highest honour and the
most extensive power: \"Thou hast set a crown of pure gold upon his head
and kept it there, when his enemies attempted to throw it off.\" Note,
Crowns are at God\'s disposal; no head wears them but God sets them
there, whether in judgment to his land or for mercy the event will show.
On the head of Christ God never set a crown of gold, but of thorns
first, and then of glory. `(4.)` That God had assured him of the
perpetuity of his kingdom, and therein had done more for him than he was
able either to ask or think (v. 4): \"When he went forth upon a perilous
expedition he asked his life of thee, which he then put into his hand,
and thou not only gavest him that, but withal gavest him length of days
for ever and ever, didst not only prolong his life far beyond his
expectation, but didst assure him of a blessed immortality in a future
state and of the continuance of his kingdom in the Messiah that should
come of his loins.\" See how God\'s grants often exceed our petitions
and hopes, and infer thence how rich he is in mercy to those that call
upon him. See also and rejoice in the length of the days of Christ\'s
kingdom. He was dead, indeed, that we might live through him; but he is
alive, and lives for evermore, and of the increase of his government and
peace there shall be no end; and because he thus lives we shall thus
live also. `(5.)` That God had advanced him to the highest honour and
dignity (v. 5): \"His glory is great, far transcending that of all the
neighbouring princes, in the salvation thou hast wrought for him and by
him.\" The glory which every good man is ambitious of is to see the
salvation of the Lord. Honour and majesty hast thou laid upon him, as a
burden which he must bear, as a charge which he must account for. Jesus
Christ received from God the Father honour and glory (2 Pt. 1. 17), the
glory which he had with him before the worlds were, Jn. 17:5. And on him
is laid the charge of universal government and to him all power in
heaven and earth is committed. `(6.)` That God had given him the
satisfaction of being the channel of all bliss to mankind (v. 6): \"Thou
hast set him to be blessings for ever\" (so the margin reads it), \"thou
hast made him to be a universal blessing to the world, in whom the
families of the earth are, and shall be blessed; and so thou hast made
him exceedingly glad with the countenance thou hast given to his
undertaking and to him in the prosecution of it.\" See how the spirit of
prophecy gradually rises here to that which is peculiar to Christ, for
none besides is blessed for ever, much less a blessing for ever to that
eminency that the expression denotes: and of him it is said that God
made him full of joy with his countenance.

In singing this we should rejoice in his joy and triumph in his
exaltation.

### Verses 7-13

The psalmist, having taught his people to look back with joy and praise
on what God had done for him and them, here teaches them to look forward
with faith, and hope, and prayer, upon what God would further do for
them: The king rejoices in God (v. 1), and therefore we will be
thankful; the king trusteth in God (v. 7), therefore will we be
encouraged. The joy and confidence of Christ our King is the ground of
all our joy and confidence.

`I.` They are confident of the stability of David\'s kingdom. Through the
mercy of the Most High, and not through his own merit or strength, he
shall not be moved. His prosperous state shall not be disturbed; his
faith and hope in God, which are the stay of his spirit, shall not be
shaken. The mercy of the Most High (the divine goodness, power, and
dominion) is enough to secure our happiness, and therefore our trust in
that mercy should be enough to silence all our fears. God being at
Christ\'s right hand in his sufferings (Ps. 16:8) and he being at God\'s
right hand in his glory, we may be sure he shall not, he cannot, be
moved, but continues ever.

`II.` They are confident of the destruction of all the impenitent
implacable enemies of David\'s kingdom. The success with which God had
blessed David\'s arms hitherto was an earnest of the rest which God
would give him from all his enemies round about, and a type of the total
overthrow of all Christ\'s enemies who would not have him to reign over
them. Observe, 1. The description of his enemies. They are such as hate
him, v. 8. They hated David because God had set him apart for himself,
hated Christ because they hated the light; but both were hated without
any just cause, and in both God was hated, Jn. 15:23, 25. 2. The designs
of his enemies (v. 11): They intended evil against thee, and imagined a
mischievous device; they pretended to fight against David only, but
their enmity was against God himself. Those that aimed to un-king David
aimed, in effect, to un-God Jehovah. What is devised and designed
against religion, and against the instruments God raises up to support
and advance it, is very evil and mischievous, and God takes it as
devised and designed against himself and will so reckon for it. `(3.)` The
disappointment of them: \"They devise what they are not able to
perform,\" v. 11. Their malice is impotent, and they imagine a vain
thing, Ps. 2:1. `(4.)` The discovery of them (v. 8): \"Thy hand shall find
them out. Though ever so artfully disguised by the pretences and
professions of friendship, though mingled with the faithful subjects of
this kingdom and hardly to be distinguished from them, though flying
from justice and absconding in their close places, yet thy hand shall
find them out wherever they are.\" There is no escaping God\'s avenging
eye, no going out of the reach of his hand; rocks and mountains will be
no better shelter at last than fig-leaves were at first. `(5.)` The
destruction of them; it will be an utter destruction (Lu. 19:27); they
shall be swallowed up and devoured, v. 9. Hell, the portion of all
Christ\'s enemies, is the complete misery both of body and soul. Their
fruit and their seed shall be destroyed, v. 10. The enemies of God\'s
kingdom, in every age, shall fall under the same doom, and the whole
generation of them will at last be rooted out, and all opposing rule,
principality, and power, shall be put down. The arrows of God\'s wrath
shall confound them and put them to flight, being levelled at the face
of them, v. 12. That will be the lot of daring enemies that face God.
The fire of God\'s wrath will consume them (v. 9); they shall not only
be cast into a furnace of fire (Mt. 13:42), but he shall make them
themselves as a fiery oven or furnace; they shall be their own
tormentors; the reflections and terrors of their own consciences will be
their hell. Those that might have had Christ to rule and save them, but
rejected him and fought against him, shall find that even the
remembrance of that will be enough to make them, to eternity, a fiery
oven to themselves: it is the worm that dies not.

`III.` In this confidence they beg of God that he would still appear for
his anointed (v. 13), that he would act for him in his own strength, by
the immediate operations of his power as Lord of hosts and Father of
spirits, making little use of means and instruments. And, 1. Hereby he
would exalt himself and glorify his own name. \"We have but little
strength, and are not so active for thee as we should be, which is our
shame; Lord, take the work into thy own hands, do it, without us, and it
will be thy glory.\" 2. Hereupon they would exalt him: \"So will we
sing, and praise thy power, the more triumphantly.\" The less God has of
our service when a deliverance is in the working the more he must have
of our praises when it is wrought without us.
