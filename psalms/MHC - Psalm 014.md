Psalm 14
========

Commentary
----------

It does not appear upon what occasion this psalm was penned nor whether
upon any particular occasion. Some say David penned it when Saul
persecuted him; others, when Absalom rebelled against him. But they are
mere conjectures, which have not certainty enough to warrant us to
expound the psalm by them. The apostle, in quoting part of this psalm
(Rom. 3:10, etc.) to prove that Jews and Gentiles are all under sin (v.
9) and that all the world is guilty before God (v. 19), leads us to
understand it, in general, as a description of the depravity of human
nature, the sinfulness of the sin we are conceived and born in, and the
deplorable corruption of a great part of mankind, even of the world that
lies in wickedness, 1 Jn. 5:19. But as in those psalms which are
designed to discover our remedy in Christ there is commonly an allusion
to David himself, yea, and some passages that are to be understood
primarily of him (as in psalm 2, 16, 22, and others), so in this psalm,
which is designed to discover our wound by sin, there is an allusion to
David\'s enemies and persecutors, and other oppressors of good men at
that time, to whom some passages have an immediate reference. In all the
psalms from the 3rd to this (except the 8th) David had been complaining
of those that hated and persecuted him, insulted him and abused him; now
here he traces all those bitter streams to the fountain, the general
corruption of nature, and sees that not his enemies only, but all the
children of men, were thus corrupted. Here is, `I.` A charge exhibited
against a wicked world (v. 1). `II.` The proof of the charge (v. 2, 3).
III. A serious expostulation with sinners, especially with persecutors,
upon it (v. 4-6). `IV.` A believing prayer for the salvation of Israel and
a joyful expectation of it (v. 7).

To the chief musician. A psalm of David.

### Verses 1-3

If we apply our hearts as Solomon did (Eccl. 7:25) to search out the
wickedness of folly, even of foolishness and madness, these verses will
assist us in the search and will show us that sin is exceedingly sinful.
Sin is the disease of mankind, and it appears here to be malignant and
epidemic.

`1.` See how malignant it is (v. 1) in two things:-

`(1.)` The contempt it puts upon the honour of God: for there is something
of practical atheism at the bottom of all sin. The fool hath said in his
heart, There is no God. We are sometimes tempted to think, \"Surely
there never was so much atheism and profaneness as there is in our
days;\" but we see the former days were no better; even in David\'s time
there were those who had arrived at such a height of impiety as to deny
the very being of a God and the first and self-evident principles of
religion. Observe, `[1.]` The sinner here described. He is one that
saith in his heart, There is no God; he is an atheist. \"There is no
Elohim, no Judge or governor of the world, no providence presiding over
the affairs of men.\" They cannot doubt of the being of God, but will
question his dominion. He says this in his heart; it is not his
judgment, but his imagination. He cannot satisfy himself that there is
none, but he wishes there were none, and pleases himself with the fancy
that it is possible there may be none. He cannot be sure there is one,
and therefore he is willing to think there is none. He dares not speak
it out, lest he be confuted, and so undeceived, but he whispers it
secretly in his heart, for the silencing of the clamours of his
conscience and the emboldening of himself in his evil ways. `[2.]` The
character of this sinner. He is a fool; he is simple and unwise, and
this is an evidence of it; he is wicked and profane, and this is the
cause of it. Note, Atheistical thoughts are very foolish wicked
thoughts, and they are at the bottom of a great deal of the wickedness
that is in this world. The word of God is a discerner of these thoughts,
and puts a just brand on him that harbours them. Nabal is his name, and
folly is with him; for he thinks against the clearest light, against his
own knowledge and convictions, and the common sentiments of all the wise
and sober part of mankind. No man will say, There is no God till he is
so hardened in sin that it has become his interest that there should be
none to call him to an account.

`(2.)` The disgrace and debasement it puts upon the nature of man. Sinners
are corrupt, quite degenerated from what man was in his innocent estate:
They have become filthy (v. 3), putrid. All their faculties are so
disordered that they have become odious to their Maker and utterly
incapable of answering the ends of their creation. They are corrupt
indeed; for, `[1.]` They do no good, but are the unprofitable burdens of
the earth; they do God no service, bring him no honour, nor do
themselves any real kindness. `[2.]` They do a great deal of hurt. They
have done abominable works, for such all sinful works are. Sin is an
abomination to God; it is that abominable thing which he hates (Jer.
44:4), and, sooner or later, it will be so to the sinner; it will be
found to be hateful (Ps. 36:2), an abomination of desolation, that is,
making desolate, Mt. 24:15. This follows upon their saying, There is no
God; for those that profess they know God, but in works deny him, are
abominable, and to every good work reprobate, Tit. 1:16.

`2.` See how epidemic this disease is; it has infected the whole race of
mankind. To prove this, God himself is here brought in for a witness,
and he is an eye-witness, v. 2, 3. Observe, `(1.)` His enquiry: The Lord
looked down from heaven, a place of prospect, which commands this lower
world; thence, with an all-seeing eye, he took a view of all the
children of men, and the question was, Whether there were any among them
that did understand themselves aright, their duty and interests, and did
seek God and set him before them. He that made this search was not only
one that could find out a good man if he was to be found, though ever so
obscure, but one that would be glad to find out one, and would be sure
to take notice of him, as of Noah in the old world. `(2.)` The result of
this enquiry, v. 3. Upon search, upon his search, it appeared, They have
all gone aside, the apostasy is universal, there is none that doeth
good, no, not one, till the free and mighty grace of God has wrought a
change. Whatever good is in any of the children of men, or is done by
them, it is not of themselves; it is God\'s work in them. When God had
made the world he looked upon his own work, and all was very good (Gen.
1:31); but, some time after, he looked upon man\'s work, and, behold,
all was very bad (Gen. 6:5), every operation of the thought of man\'s
heart was evil, only evil, and that continually. They have gone aside
from the right of their duty, the way that leads to happiness, and have
turned into the paths of the destroyer.

In singing this let us lament the corruption of our own nature, and see
what need we have of the grace of God; and, since that which is born of
the flesh is flesh, let us not marvel that we are told we must be born
again.

### Verses 4-7

In these verses the psalmist endeavours,

`I.` To convince sinners of the evil and danger of the way they are in,
how secure soever they are in that way. Three things he shows them,
which, it may be, they are not very willing to see-their wickedness,
their folly, and their danger, while they are apt to believe themselves
very wise, and good, and safe. See here,

`1.` Their wickedness. This is described in four instances:-`(1.)` They are
themselves workers of iniquity; they design it, they practise it, and
take as much pleasure in it as ever any man did in his business. `(2.)`
They eat up God\'s people with as much greediness as they eat bread,
such an innate and inveterate enmity they have to them, and so heartily
do they desire their ruin, because they really hate God, whose people
they are. It is meat and drink to persecutors to be doing mischief; it
is as agreeable to them as their necessary food. They eat up God\'s
people easily, daily, securely, without either check of conscience when
they do it or remorse of conscience when they have done it; as Joseph\'s
brethren cast him into a pit and then sat down to eat bread, Gen. 37:24,
25. See Mic. 3:2, 3. `(3.)` They call not upon the Lord. Note, Those that
care not for God\'s people, for God\'s poor, care not for God himself,
but live in contempt of him. The reason why people run into all manner
of wickedness, even the worst, is because they do not call upon God for
his grace. What good can be expected from those that live without
prayer? `(4.)` They shame the counsel of the poor, and upbraid them with
making God their refuge, as David\'s enemies upbraided him, Ps. 11:1.
Note, Those are very wicked indeed, and have a great deal to answer for,
who not only shake off religion, and live without it themselves, but say
and do what they can to put others out of conceit with it that are
well-inclined-with the duties of it, as if they were mean, melancholy,
and unprofitable, and with the privileges of it, as if they were
insufficient to make a man safe and happy. Those that banter religion
and religious people will find, to their cost, it is ill jesting with
edged-tools and dangerous persecuting those that make God their refuge.
Be you not mockers, lest your bands be made strong. He shows them,

`2.` Their folly: They have no knowledge; this is obvious, for if they
had any knowledge of God, if they did rightly understand themselves, and
would but consider things as men, they would not be so abusive and
barbarous as they are to the people of God.

`3.` Their danger (v. 5): There were they in great fear. There, where
they ate up God\'s people, their own consciences condemned what they
did, and filled them with secret terrors; they sweetly sucked the blood
of the saints, but in their bowels it is turned, and become the gall of
asps. Many instances there have been of proud and cruel persecutors who
have been made like Pashur, Magormissabibs-terrors to themselves and all
about them. Those that will not fear God perhaps may be made to fear at
the shaking of a leaf.

`II.` He endeavours to comfort the people of God, 1. With what they have.
They have God\'s presence (v. 5): He is in the generation of the
righteous. They have his protection (v. 6): The Lord is their refuge.
This is as much their security as it is the terror of their enemies, who
may jeer them for their confidence in God, but cannot jeer them out of
it. In the judgment-day it will add to the terror and confusion of
sinners to see God own the generation of the righteous, which they have
hated and bantered. 2. With what they hope for; and that is the
salvation of Israel, v. 7. When David was driven out by Absalom and his
rebellious accomplices, he comforted himself with an assurance that god
would in due time turn again his captivity, to the joy of all his good
subjects. But surely this pleasing prospect looks further. He had, in
the beginning of the psalm, lamented the general corruption of mankind;
and, in the melancholy view of that, wishes for the salvation which
should be wrought out by the Redeemer, who was expected co come to Zion,
to turn away ungodliness from Jacob, Rom. 11:26. The world is bad; O
that the Messiah would come and change its character! There is a
universal corruption; O for the times of reformation! Those will be as
joyful times as these are melancholy ones. Then shall God turn again the
captivity of his people; for the Redeemer shall ascend on high, and lead
captivity captive, and Jacob shall then rejoice. The triumphs of Zion\'s
King will be the joys of Zion\'s children. The second coming of Christ,
finally to extinguish the dominion of sin and Satan, will be the
completing of this salvation, which is the hope, and will be the joy, of
every Israelite indeed. With the assurance of that we should, in singing
this, comfort ourselves and one another, with reference to the present
sins of sinners and sufferings of saints.
