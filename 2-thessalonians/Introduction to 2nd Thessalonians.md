Introduction to 2nd Thessalonians
=================================

This Second Epistle was written soon after the former, and seems to have
been designed to prevent a mistake, which might arise from some passages
in the former epistle, concerning the second coming of Christ, as if it
were near at hand. The apostle in this epistle is careful to prevent any
wrong use which some among them might make of those expressions of his
that were agreeable to the dialect of the prophets of the Old Testament,
and informs them that there were many intermediate counsels yet to be
fulfilled before that day of the Lord should come, though, because it is
sure, he had spoken of it as near. There are other things that he writes
about for their consolation under sufferings, and exhortation and
direction in duty.
