Numbers, Chapter 28
===================

Commentary
----------

Now that the people were numbered, orders given for the dividing of the
land, and a general of the forces nominated and commissioned, one would
have expected that the next chapter should begin the history of the
campaign, or at least should give us an account of the ordinances of
war; no, it contains the ordinances of worship, and provides that now,
as they were on the point of entering Canaan, they should be sure to
take their religion along with them, and not forget this, in the
prosecution of their wars (v. 1, 2). The laws are here repeated and
summed up concerning the sacrifices that were to be offered, `I.` Daily
(v. 3-8). `II.` Weekly (v. 9, 10). `III.` Monthly (v. 11-15). `IV.` Yearly. 1.
At the passover (v. 16-25). 2. At pentecost (v. 26-31). And the next
chapter is concerning the annual solemnities of the seventh month.

### Verses 1-8

Here is, `I.` A general order given concerning the offerings of the Lord,
which were to be brought in their season, v. 2. These laws are here
given afresh, not because the observance of them was wholly disused
during their thirty-eight years\' wandering in the wilderness (we cannot
think that they were so long without any public worship, but that at
least the daily lamb was offered morning and evening, and doubled on the
sabbath day; so bishop Patrick conjectures); but that many of the
sacrifices were then omitted is plainly intimated, Amos v. 25, quoted by
Stephen, Acts 7:42. Did you offer unto me sacrifices and offerings in
the wilderness forty years, O house of Israel? It is implied, \"No, you
did not.\" But, whether the course of sacrifices had been interrupted or
no, God saw fit now to repeat the law of sacrifices, 1. Because this was
a new generation of men, that were most of them unborn when the former
laws were given; therefore, that they might be left without excuse, they
have not only these laws written, to be read to them, but again repeated
from God himself, and put into a less compass and a plainer method. 2.
Because they were now entering upon war, and might be tempted to think
that while they were engaged in that they should be excused from
offering sacrifices. Inter arma silent leges-law is little regarded
amidst the clash of arms. No, says God, my bread for my sacrifices even
now shall you observe to offer, and that in the due season. They were
peculiarly concerned to keep their peace with God when they were at war
with their enemies. In the wilderness they were solitary, and quite
separate from all other people, and therefore there they needed not so
much their distinguishing badges, nor would their omission of sacrifices
be so scandalous as when they came into Canaan, when they mingled with
other people. 3. Because possession was now to be given them of the land
of promise, that land flowing with milk and honey, where they would have
plenty of all good things. \"Now\" (says God), \"When you are feasting
yourselves, forget not to offer the bread of your God.\" Canaan was
given to them upon this condition, that they should observe God\'s
statutes, Ps. 105:44, 45.

`II.` The particular law of the daily sacrifice, a lamb in the morning
and a lamb in the evening, which, for the constancy of it as duly as the
day came, is called a continual burnt-offering (v. 3), which intimates
that when we are bidden to pray always, and to pray without ceasing, it
is intended that at least every morning and every evening we offer up
our solemn prayers and praises to God. This is said to be ordained in
Mount Sinai (v. 6), when the other laws were given. The institution of
it we have, Ex. 29:38. Nothing is here added in the repetition of the
law, but that the wine to be poured out in the drink-offering is ordered
to be strong wine (v. 7), the riches and most generous and best-bodied
wine they could get. Though it was to be poured out upon the altar, and
not drunk (they therefore might be ready to think the worst would serve
to be so thrown away), yet God requires the strongest, to teach us to
serve God with the best we have. The wine must be strong (says
Ainsworth) because it was a figure of the blood of Christ, the memorial
of which is still left to the church in wine, and of the blood of the
martyrs, which was poured out as a drink-offering upon the sacrifice and
service of our faith, Phil. 2:17.

### Verses 9-15

The new moons and the sabbaths are often spoken of together, as great
solemnities in the Jewish church, very comfortable to the saints then,
and typical of gospel grace. Now we have here the sacrifices appointed,
`1.` For the sabbaths. Every sabbath day the offering must be doubled;
besides the two lambs offered for the daily burnt-offering, there must
be two more offered, one (it is probable) added to the morning
sacrifice, and the other to the evening, v. 9, 10. This teaches us to
double our devotions on sabbath days, for so the duty of the day
requires. The sabbath rest is to be observed, in order to a more close
application to the sabbath work, which ought to fill up sabbath time. In
Ezekiel\'s temple-service, which points at gospel times, the sabbath
offerings were to be six lambs and a ram, with their meat-offerings, and
drink-offerings (Eze. 46:4, 5), to intimate not only the continuance,
but the advancement, of sabbath sanctification in the days of the
Messiah. This is the burnt-offering of the sabbath in his sabbath, so it
is in the original, v. 10. We must do every sabbath day\'s work in its
day, studying to redeem every minute of sabbath time as those that
believe it precious; and not thinking to put off one sabbath\'s work to
another, for sufficient to every sabbath is the service thereof. 2. For
the new moons. Some suggest that, as the sabbath was kept with an eye to
the creation of the world, so the new moons were sanctified with an eye
to the divine providence, which appoints the moon for seasons, guiding
the revolutions of time by its changes, and governing sublunary bodies
(as many think) by its influences. Though we observe not any feast of
new moons, yet we must not forget to give God the glory of all the
precious things put forth by the moon which he has established for ever,
a faithful witness in heaven, Ps. 89:37. The offerings in the new moons
were very considerable, two bullocks, a ram, and seven lambs, with the
meat-offerings and drink-offerings that were to attend them (v. 11,
etc.), besides a sin-offering, v. 15. For, when we give glory to God by
confessing his mercies, we must give glory to him likewise by confessing
our own sins; and, when we rejoice in the gifts of common providence, we
must make the sacrifice of Christ, that great gift of special grace, the
fountain and spring-head of our joy. Some have questioned whether the
new moons were to be reckoned among their feasts; but why should they
not, when, besides the special sacrifices which were then to be offered,
they rested from servile works (Amos 8:5), blew the trumpets (ch.
10:10), and went to the prophets to hear the word? 2 Ki. 4:23. And the
worship performed in the new moons is made typical of gospel
solemnities, Isa. 66:23.

### Verses 16-31

Here is, `I.` The appointment of the pass-over sacrifices; not that which
was the chief, the paschal lamb (sufficient instructions had formerly
been given concerning that), but those which were to be offered upon the
seven days of unleavened bread, which followed it, v. 17-25. The first
and last of those seven days were to be sanctified as sabbaths, by a
holy rest and a holy convocation, and on each of the seven days they
were to be liberal in their sacrifices, in token of their great and
constant thankfulness for their deliverance out of Egypt: Two bullocks,
a ram, and seven lambs. A gospel conversation, in gratitude for Christ
our passover who was sacrificed, is called the keeping of this feast (1
Co. 5:8); for it is not enough that we purge out the leavened bread of
malice and wickedness, but we must offer the bread of our God, even the
sacrifice of praise, continually, and continue herein unto the end. 2.
The sacrifices are likewise appointed which were to be offered at the
feast of pentecost, here called the day of the first-fruits, v. 26. In
the feast of unleavened bread they offered a sheaf of their first-fruits
of barley (which with them was first ripe) to the priest (Lev. 23:10),
as an introduction to the harvest; but now, about seven weeks after,
they were to bring a new meat-offering to the Lord, at the end of
harvest, in thankfulness to God, who had not only given, but preserved
to their use, the kindly fruits of the earth, so as that in due time
they did enjoy them. It was at this feast that the Spirit was poured out
(Acts 2:1, etc.), and thousands were converted by the preaching of the
apostles, and were presented to Christ, to be a kind of first-fruits of
his creatures. The sacrifice that was to be offered with the loaves of
the first-fruits was appointed, Lev. 23:18. But over and above, besides
that and besides the daily offerings, they were to offer two bullocks,
one ram, and seven lambs, with a kid for a sin-offering, v. 27-30. When
God sows plentifully upon us he expects to reap accordingly from us.
Bishop Patrick observes that no peace-offerings are appointed in this
chapter, which were chiefly for the benefit of the offerers, and
therefore in them they were left more to themselves; but burnt-offerings
were purely for the honour of God, were confessions of his dominion, and
typified evangelical piety and devotion, by which the soul is wholly
offered up to God in the flames of holy love; and sin-offerings were
typical of Christ\'s sacrifice of himself, by which we and our services
are perfected and sanctified.
