Numbers, Chapter 8
==================

Commentary
----------

This chapter is concerning the lamps or lights of the sanctuary. `I.` The
burning lamps in the candlestick, which the priests were charged to tend
(v. 1-4). `II.` The living lamps (if I may so call them), The Levites, who
as ministers were burning and shining lights. The ordination of the
priests we had an account of, Lev. 8. Here we have an account of the
ordination of the Levites, the inferior clergy. 1. How they were
purified (v. 5-8). 2. How they were parted with by the people (v. 9,
10). 3. How they were presented to God in lieu of the firstborn (v.
11-18). 4. How they were consigned to Aaron and his sons, to be
ministers to them (v. 19). 5. How all these orders were duly executed
(v. 20-22). And, lastly, the age appointed for their ministration (v.
23, etc.).

### Verses 1-4

Directions were given long before this for the making of the golden
candlestick (Ex. 25:31), and it was made according to the pattern shown
to Moses in the mount, Ex. 38:17. But now it was that the lamps were
first ordered to be lighted, when other things began to be used.
Observe, 1. Who must light the lamps; Aaron himself, he lighted the
lamps, v. 3. As the people\'s representative to God, he thus did the
office of a servant in God\'s house, lighting his Master\'s candle; as
the representative of God to the people, he thus gave them the
intimations of God\'s will and favour, thus expressed (Ps. 18:28), Thou
wilt light my candle; and thus Aaron himself was now lately directed to
bless the people, The Lord make his face to shine upon thee, ch. 6:25.
The commandment is a lamp, Prov. 6:23. The scripture is a light shining
in a dark place, 2 Pt. 1:19. And a dark place indeed even the church
would be without it, as the tabernacle (which had no window in it)
without the lamps. Now the work of ministers is to light these lamps, by
expounding and applying the word of God. The priest lighted the middle
lamp from the fire of the altar, and the rest of the lamps he lighted
one from another, which (says Mr. Ainsworth) signifies that the fountain
of all light and knowledge is in Christ, who has the seven spirits of
God figured by the seven lamps of fire (Rev. 4:5), but that in the
expounding of scripture one passage must borrow light from another. He
also supposes that, seven being a number of perfection, by the seven
branches of the candlestick is shown the full perfection of the
scriptures, which are able to make us wise to salvation. 2. To what end
the lamps were lighted, that they might give light over against the
candlestick, that is, to that part of the tabernacle where the table
stood, with the show-bread upon it, over against the candlestick. They
were not lighted like tapers in an urn, to burn to themselves, but to
give light to the other side of the tabernacle, for therefore candles
are lighted, Mt. 5:15. Note, The lights of the world, the lights of the
church, must shine as lights. Therefore we have light, that we may give
light.

### Verses 5-26

We read before of the separating of the Levites from among the children
of Israel when they were numbered, and the numbering of them by
themselves (ch. 3:6, 15), that they might be employed in the service of
the tabernacle. Now here we have directions given for their solemn
ordination (v. 6), and the performance of it, v. 20. All Israel must
know that they took not this honour to themselves, but were called of
God to it; nor was it enough that they were distinguished from their
neighbours, but they must be solemnly devoted to God. Note, All that are
employed for God must be dedicated to him, according as the degree of
employment is. Christian musts be baptized, ministers must be ordained;
we must first give ourselves unto the Lord, and then our services.
Observe in what method this was done:

`I.` The Levites must be cleansed, and were so. The rites and ceremonies
of their cleansing were to be performed, 1. By themselves. They must
wash their clothes, and not only bathe, but shave all their flesh, as
the leper was to do when he was cleansed, Lev. 14:8. They must cause a
razor to pass over all their flesh, to clear themselves from that
defilement which would not wash off. Jacob, whom God loved, was a smooth
man; it was Esau that was hairy. The great pains they were to take with
themselves to make themselves clean teaches all Christians, and
ministers particularly, by repentance and mortification, to cleanse
themselves from all filthiness of flesh and spirit, that they may
perfect holiness. Those must be clean that bear the vessels of the Lord.
2. By Moses. He must sprinkle the water of purifying upon them, which
was prepared by divine direction. This signified the application of the
blood of Christ to our souls by faith, to purify us from an evil
conscience, that we may be fit to serve the living God. It is our duty
to cleanse ourselves, and God\'s promise that he will cleanse us.

`II.` The Levites, being thus prepared, must be brought before the Lord
in a solemn assembly of all Israel, and the children of Israel must put
their hands upon them (v. 10), so transferring their interest in them
and in their service (to which, as a part, the whole body of the people
was entitled) to God and to his sanctuary. They presented them to God as
living sacrifices, holy and acceptable, to perform a reasonable service;
and therefore, as the offerers in all other cases did, they laid their
hands upon them, desiring that their service might be accepted in lieu
of the attendance of the whole congregation, particularly the
first-born, which they acknowledge God might have insisted on. This will
not serve to prove a power in the people to ordain ministers; for this
imposition of hands by the children of Israel upon the Levites did not
make them ministers of the sanctuary, but only signified the people\'s
parting with that tribe out of their militia, and civil incorporations,
in order to their being made ministers by Aaron, who was to offer them
before the Lord. All the congregation of the children of Israel could
not lay hands on them, but it is probable that the rulers and elders did
it as the representative body of the people. Some think that the
first-born did it because in their stead the Levites were consecrated to
God. Whatever God calls for from us to serve his own glory by, we must
cheerfully resign it, lay our hands upon it, not to detain it but to
surrender it, and let it go to him that is entitled to it.

`III.` Sacrifices were to be offered for them, a sin-offering first (v.
12), and then a burnt-offering, to make an atonement for the Levites,
who, as the parties concerned, were to lay their hands upon the head of
the sacrifices. See here, 1. That we are all utterly unworthy and unfit
to be admitted into and employed in the service of God, till atonement
be made for sin, and thereby our peace made with God. That interposing
cloud must be scattered before there can be any comfortable communion
settled between God and our souls. 2. That it is by sacrifice, by Christ
the great sacrifice, that we are reconciled to God, and made fit to be
offered to him. It is by him that Christians are sanctified to the work
of their Christianity, and ministers to the work of their ministry. The
learned bishop Patrick\'s notion of the sacrifice offered by the Levites
is that the Levites were themselves considered as an expiatory
sacrifice, for they were given to make atonement for the children of
Israel, (v. 19), and yet not being devoted to death, any more than the
first-born were, these two sacrifices were substituted in their stead,
upon which therefore they were to lay their hands, that the sin which
the children of Israel laid upon them (v. 10) might be transferred to
these beasts.

`IV.` The Levites themselves were offered before the Lord for an offering
of the children of Israel, v. 11. Aaron gave them up to God, as being
first given up by themselves, and by the children of Israel. The
original word signifies a wave-offering, not that they were actually
waved, but they were presented to God as the God of heaven, and the Lord
of the whole earth, as the wave-offerings were. And in calling them
wave-offerings it was intimated to them that they must continually lift
up themselves towards God in his service, lift up their eyes, lift up
their hearts, and must move to and fro with readiness in the business of
their profession. They were not ordained to be idle, but to be active
and stirring.

`V.` God here declares his acceptance of them: The Levites shall be mine,
v. 14. God took them instead of the first-born (v. 16-18), of which
before, ch. 3:41. Note, What is in sincerity offered to God shall be
graciously owned and accepted by him. And his ministers who have
obtained mercy of him to be faithful have particular marks of favour and
honour put upon them: they shall be mine, and then (v. 15) they shall go
in to do the service of the tabernacle. God takes them for his own, that
they may serve him. All that expect to share in the privileges of the
tabernacle must resolve to do the service of the tabernacle. As, on the
one hand, none of God\'s creatures are his necessary servants (he needs
not the service of any of them), so, on the other hand, none are taken
merely as honorary servants, to do nothing. All whom God owns he
employs; angels themselves have their services.

`VI.` They are then given as a gift to Aaron and his sons (v. 19), yet so
as that the benefit accrued to the children of Israel. 1. The Levites
must act under the priests as attendants on them, and assistants to
them, in the service of the sanctuary. Aaron offers them to God (v. 11),
and then God gives them back to Aaron, v. 19. Note, Whatever we give up
to God, he will give back to us unspeakably to our advantage. Our
hearts, our children, our estates, are never more ours, more truly, more
comfortably ours, than when we have offered them up to God. 2. They must
act for the people. They were taken to do the service of the children of
Israel, that is, not only to do the service which they should do, but to
serve their interests, and do that which would really redound to the
honour, safety, and prosperity of the whole nation. Note, Those that
faithfully perform the service of God do one of the best services that
can be done to the public; God\'s ministers, while they keep within the
sphere of their office and conscientiously discharge the duty of it,
must be looked upon as some of the most useful servants of their
country. The children of Israel can as ill spare the tribe of Levi as
any of their tribes. But what is the service they do the children of
Israel? It follows, it is to make an atonement for them, that there be
no plague among them. It was the priests\' work to make atonement by
sacrifice, but the Levites made atonement by attendance, and preserved
the peace with heaven which was made by sacrifice. If the service of the
priests in the tabernacle had been left to all the first-born of Israel
promiscuously, it would have been either neglected or done unskillfully
and irreverently, being done by those that were not so closely tied to
it, nor so diligently trained to it, nor so constantly used to it, as
the Levites were; and this would bring a plague among the children of
Israel-meaning, perhaps, the death of the first-born themselves, which
was the last and greatest of the plagues of Egypt. To prevent this, and
to preserve the atonement, the Levites were appointed to do this
service, who should be bred up to it under their parents from their
infancy, and therefore would be well versed in it; and so the children
of Israel, that is, the first-born, should not need to come nigh to the
sanctuary; or, when any Israelites had occasion, the Levites would be
ready to instruct them, and introduce them, and so prevent any fatal
miscarriage or mistake. Note, It is a very great kindness to the church
that ministers are appointed to go before the people in the things of
God, as guides, overseers, and rulers, in religious worship, and to make
that their business. When Christ ascended on high, he gave these gifts,
Eph. 4:8, 11, 12.

`VII.` The time of their ministration is fixed. 1. They were to enter
upon the service at twenty-five years old, v. 24. They were not charged
with the carrying of the tabernacle and the utensils of it till they
were thirty years old, ch. 4:3. But they were entered to be otherwise
serviceable at twenty-five years old, a very good age for ministers to
begin their public work at. The work then required that strength of body
and the work now requires that maturity of judgment and steadiness of
behaviour which men rarely arrive at till about that age; and novices
are in danger of being lifted up with pride. 2. They were to have a writ
of ease at fifty years old; then they were to return from the warfare,
as the phrase is (v. 25), not cashiered with disgrace, but preferred
rather to the rest which their age required, to be loaded with the
honours of their office, as hitherto they had been with the burdens of
it. They shall minister with their brethren in the tabernacle, to direct
the junior Levites, and set them in; and they shall keep the charge, as
guards upon the avenues of the tabernacle, to see that no stranger
intruded, nor any person in his uncleanness, but they shall not be put
upon any service which may be a fatigue to them. If God\'s grace provide
that men shall have ability according to their work, man\'s prudence
should take care that men have work only according to their ability. The
aged are most fit for trusts, and to keep the charge; the younger are
most fit for work, and to do the service. Those that have used the
office of a servant well purchase to themselves a good degree, 1 Tim.
3:13. Yet indeed gifts are not tied to ages (Job 32:9), but all these
worketh that one and the self-same Spirit. Thus was the affair of the
Levites settled.
