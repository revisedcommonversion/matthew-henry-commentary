Numbers, Chapter 34
===================

Commentary
----------

In this chapter God directs Moses, and he is to direct Israel, `I.`
Concerning the bounds and borders of the land of Canaan (v. 1-15). `II.`
Concerning the division and distribution of it to the tribes of Israel
(v. 16, etc.).

### Verses 1-15

We have here a particular draught of the line by which the land of
Canaan was meted, and bounded, on all sides. God directs Moses to settle
it here, not as a geographer in his map, merely to please the curious,
but as a prince in his grant, that it may be certainly known what
passes, and is conveyed, by the grant. There was a much larger
possession promised them, which in due time they would have possessed if
they had been obedient, reaching even to the river Euphrates, Deu.
11:24. And even so far the dominion of Israel did extend in David\'s
time and Solomon\'s, 2 Chr. 9:26. But this which is here described is
Canaan only, which was the lot of the nine tribes and a half, for the
other two and a half were already settled, v. 14, 15. Now concerning the
limits of Canaan observe,

`I.` That it was limited within certain bounds: for God appoints the
bounds of our habitation, Acts 17:26. The borders are set them, 1. That
they might know whom they were to dispossess, and how far the commission
which was given them extended (ch. 33:53), that they should drive out
the inhabitants. Those that lay within these borders, and those only,
they must destroy; hitherto their bloody sword must go, and no further.
2. That they might know what to expect the possession of themselves. God
would not have his people to enlarge their desire of worldly
possessions, but to know when they have enough, and to rest satisfied
with it. The Israelites themselves must not be placed alone in the midst
of the earth, but must leave room for their neighbours to live by them.
God sets bounds to our lot; let us then set bounds to our desires, and
bring our mind to our condition.

`II.` That it lay comparatively in a very little compass: as it is here
bounded, it is reckoned to be but about 160 miles in length and about
fifty in breadth; perhaps it did not contain more than half as much
ground as England, and yet this is the country which was promised to the
father of the faithful and was the possession of the seed of Israel.
This was that little spot of ground in which only, for many ages, God
was known, and his name was great, Ps. 76:1. This was the vineyard of
the Lord, the garden enclosed; but, as it is with gardens and vineyards,
the narrowness of the extent was abundantly compensated by the
extraordinary fruitfulness of the soil, otherwise it could not have
subsisted so numerous a nation as did inhabit it. See here then, 1. How
small a part of the world God has for himself. Though the earth is his,
and the fullness thereof, yet few have the knowledge of him and serve
him; but those few are happy, very happy, because fruitful to God. 2.
How small a share of the world God often gives to his own people. Those
that have their portion in heaven have reason to be content with a small
pittance of this earth; but, as here, what is wanting in quantity is
made up in quality; a little that a righteous man has, having it from
the love of God and with his blessing, is far better and more
comfortable than the riches of many wicked, Ps. 37:16.

`III.` It is observable what the bounds and limits of it were. 1. Canaan
was itself a pleasant land (so it is called Dan. 8:9), and yet it
bordered upon wilderness and seas, and was surrounded with divers
melancholy prospects. Thus the vineyard of the church is compassed on
all hands with the desert of this world, which serves as a foil to it,
to make it appear the more beautiful for situation. 2. Many of its
borders were its defences and natural fortifications, to render the
access of enemies the more difficult, and to intimate to Israel that the
God of nature was their protector, and with his favour would compass
them as with a shield. 3. The border reached to the river of Egypt (v.
5), that the sight of that country which they could look into out of
their own might remind them of their bondage there, and their wonderful
deliverance thence. 4. Their border is here made to begin at the Salt
Sea (v. 3), and there it ends, v. 12. This was the remaining lasting
monument of the destruction of Sodom and Gomorrah. That pleasant
fruitful vale in which these cities stood became a lake, which was never
stirred by any wind, bore no vessels, was replenished with no fish, no
living creature of any sort being found in it, therefore called the Dead
Sea. This was part of their border, that it might be a constant warning
to them to take heed of those sins which had been the ruin of Sodom; yet
the iniquity of Sodom was afterwards found in Israel (Eze. 16:49), for
which Canaan was made, though not a salt sea as Sodom, yet a barren
soil, and continues so to this day. 5. Their western border was the
Great Sea (v. 6), which is now called the Mediterranean. Some consider
this sea itself to have been a part of their possession, and that by
virtue of this grant, they had the dominion of it, and, if they had not
forfeited it by sin, might have rode masters of it.

### Verses 16-29

God here appoints commissioners for the dividing of the land to them.
The conquest of it is taken for granted, though as yet there was never a
stroke struck towards it. Here is no nomination of the generals and
commanders-in-chief that should carry on the war; for they were to get
the land in possession, not by their own sword or bow, but by the power
and favour of God; and so confident must they be of victory and success
while God fought for them that the persons must now be named who should
be entrusted with the dividing of the land, that is, who should preside
in casting the lots, and determine controversies that might arise, and
see that all was done fairly. 1. The principal commissioners, who were
of the quorum, were Eleazar and Joshua (v. 17), typifying Christ, who,
as priest and king, divides the heavenly Canaan to the spiritual Israel;
yet, as they were to go by the lot, so Christ acknowledges the disposal
must be by the will of the Father, Mt. 20:23. Compare, Eph. 1:11. 2.
Besides these, that there might be no suspicion of partiality, a prince
of each tribe was appointed to inspect this matter, and to see that the
tribe he served for was in no respect injured. Public affairs should be
so managed as not only to give their right to all, but, if possible, to
give satisfaction to all that they have justice done them., It is a
happiness to a land to have the princes of their people meet together,
some out of every tribe, to concert the affairs that are of common
concern, a constitution which is the abundant honour, ease, and safety,
of the nation that is blessed with it. 3. Some observe that the order of
the tribes here very much differs from that in which they hitherto, upon
all occasions, been named, and agrees with the neighbourhood of their
lots in the division of the land. Judah, Simeon, and Benjamin, the first
three here named, lay close together; the inheritance of Dan lay next
them on one side, that of Ephraim and Manasseh on another side; Zebulun
and Issachar lay abreast more northerly, and, lastly, Asher and Naphtali
most northward of all, as is easy to observe in looking over a map of
Canaan; this (says bishop Patrick) is an evidence that Moses was guided
by a divine Spirit in his writings. Known unto God are all his works
beforehand, and what is new and surprising to us he perfectly foresaw,
without any confusion or uncertainty.
