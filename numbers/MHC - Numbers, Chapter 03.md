Numbers, Chapter 3
==================

Commentary
----------

This chapter and the next are concerning the tribe of Levi, which was to
be mustered and marshalled by itself, and not in common with the other
tribes, intimating the particular honour put upon them and the
particular duty and service required from them. The Levites are in this
chapter considered, `I.` As attendants on, and assistants to, the priests
in the temple-service. And so we have an account, 1. Of the priests
themselves (v. 1-4) and their work (v. 10). 2. Of the gift of the
Levites to them (v. 5-9), in order to which they are mustered (v.
14-16), and the sum of them taken (v. 39). Each particular family of
them is mustered, has its place assigned and its charge, the Gershonites
(v. 17-26), the Kohathites (v. 27-32), the Merarites (v. 33-39). `II.` As
equivalents for the first-born (v. 11-13). 1. The first-born are
numbered, and the Levites taken instead of them, as far as the number of
the Levites went (v. 40-45). 2. What first-born there were more than the
Levites were redeemed (v. 46, etc.).

### Verses 1-13

Here, `I.` The family of Aaron is confirmed in the priests\' office, v.
10. They had been called to it before, and consecrated; here they are
appointed to wait on their priests\' office: the apostle uses this
phrase (Rom. 12:7), Let us wait on our ministry. The office of the
ministry requires a constant attendance and great diligence; so frequent
are the returns of its work, and yet so transient its favourable
opportunities, that it must be waited on. Here is repeated what was said
before (ch. 1:51): The stranger that cometh nigh shall be put to death,
which forbids the invading of the priest\'s office by any other person
whatsoever; none must come nigh to minister but Aaron and his sons only,
all others are strangers. It also lays a charge on the priests, as
door-keepers in God\'s house, to take care that none should come near
who were forbidden by the law; they must keep off all intruders, whose
approach would be to the profanation of the holy things, telling them
that if they came near it was at their peril, they would die by the hand
of God, as Uzza did. The Jews say that afterwards there was hung over
the door of the temple a golden sword (perhaps alluding to that flaming
sword at the entrance of the garden of Eden), on which was engraven, The
stranger that cometh nigh shall be put to death.

`II.` A particular account is given of this family of Aaron; what we have
met with before concerning them is here repeated. 1. The consecration of
the sons of Aaron, v. 3. They were all anointed to minister before the
Lord, though it appeared afterwards, and God knew it, that two of them
were wise and two were foolish. 2. The fall of the two elder (v. 4):
they offered strange fire, and died for so doing, before the Lord. This
is mentioned here in the preamble to the law concerning the priesthood,
for a warning to all succeeding priests; let them know, by this example,
that God is a jealous God, and will not be mocked; the holy anointing
oil was an honour to the obedient, but not a shelter to the disobedient.
It is here said, They had no children, Providence so ordering it, for
their greater punishment, that none of their descendants should remain
to be priests, and so bear up their name who had profaned God\'s name.
3. The continuance of the two younger: Eleazar and Ithamar ministered in
the sight of Aaron. It intimates, `(1.)` The care they took about their
ministration not to make any blunders; they kept under their father\'s
eye, and took instruction from him in all they did, because, probably,
Nadab and Abihu got out of their father\'s sight when they offered
strange fire. Note, It is good for young people to act under the
direction and inspection of those that are aged and experienced. `(2.)`
The comfort Aaron took in it; it pleased him to see his younger sons
behave themselves prudently and gravely, when his two elder had
miscarried. Note, It is a great satisfaction to parents to see their
children walk in the truth, 3 Jn. 4.

`III.` A grant is made of the Levites to be assistants to the priests in
their work: Give the Levites to Aaron, v. 9. Aaron was to have a greater
propriety in, and power over, the tribe of Levi than any other of the
prices had in and over their respective tribes. There was a great deal
of work belonging to the priests\' office, and there were now only three
pairs of hands to do it all, Aaron\'s and his two sons\'; for it does
not appear that they had either of them any children at this time, at
least not any that were of age to minister, therefore God appoints the
Levites to attend upon them. Note, Those whom God finds work for his
will find help for. Here is, 1. The service for which the Levites were
designed: they were to minister to the priests in their ministration to
the Lord (v. 6), and to keep Aaron\'s charge (v. 7), as the deacons to
the bishops in the evangelical constitution, serving at tables, while
the bishops waited on their ministry. The Levites killed the sacrifices,
and then the priests needed only to sprinkle the blood and burn the fat:
the Levites prepared the incense, the priests burnt it. They were to
keep, not only Aaron\'s charge, but the charge of the whole
congregation. Note, It is a great trust that is reposed in ministers,
not only for the glory of Christ, but for the good of his church; so
that they must not only keep the charge of the great high priest, but
must also be faithful to the souls of men, in trust for whom a
dispensation is committed to them. 2. the consideration upon which the
Levites were demanded; they were taken instead of the first-born. The
preservation of the first-born of Israel, when all the first-born of the
Egyptians (with whom they were many of them mingled) were destroyed, was
looked upon by him who never makes any unreasonable demands as cause
sufficient of the appropriating of all the first-born thenceforward to
himself (v. 13): All the first-born are mine. That was sufficient to
make them his, though he had given no reason for it, for he is the sole
fountain and Lord of all beings and powers; but because all obedience
must flow from love, and acts of duty must be acts of gratitude, before
they were challenged into peculiar services they were crowned with
peculiar favours. Note, When he that made us saves us we are thereby
laid under further obligations to serve him and live to him. God\'s
right to us by redemption corroborates the right he has to us by
creation. Now because the first-born of a family are generally the
favourites, and some would think it a disparagement to have their eldest
sons servants to the priests, and attending before the door of the
tabernacle, God took the tribe of Levi entire for his own, in lieu of
the first-born, v. 12. Note, God\'s institutions put no hardships upon
men in any of their just interests or reasonable affections. It was
presumed that the Israelites would rather part with the Levites than
with the first-born, and therefore God graciously ordered the exchange;
yet for us he spared not his own Son.

### Verses 14-39

The Levites being granted to Aaron to minister to him, they are here
delivered to him by tale, that he might know what he had, and employ
them accordingly. Observe,

`I.` By what rule they were numbered: Every male from a month old and
upward, v. 15. The rest of the tribes were numbered only from twenty
years old and upwards, and of them those only that were able to go forth
to war; but into the number of the Levites they must take in both
infants, and infirm; being exempted from the war, it was not insisted
upon that they should be of age and strength for the wars. Though it
appears afterwards that little more than a third part of the Levites
were fit to be employed in the service of the tabernacle (about 8000 out
of 22,000, ch. 4:47, 48), yet God would have them all numbered as
retainers to his family; that none may think themselves disowned and
rejected of God because they are not in a capacity of doing him that
service which they see others do him. The Levites of a month old could
not honour God and serve the tabernacle, as those that had grown up; yet
out of the mouths of babes and sucklings the Levites\' praise was
perfected. Let not little children be hindered from being enrolled among
the disciples of Christ, for such was the tribe of Levi, of such is the
kingdom of heaven, that kingdom of priests. The redemption of the
first-born was reckoned from a month old (ch. 18:15, 16), therefore from
that age the Levites were numbered. They were numbered after the house
of their fathers, not their mothers, for, if the daughter of a Levite
married one of another tribe, her son was not a Levite; but we read of a
spiritual priest to out God who inherited the unfeigned faith which
dwelt in his mother and grandmother, 2 Tim. 1:5.

`II.` How they were distributed into three classes, according to the
number of the sons of Levi, Gershon, Kohath, and Merari, and these
subdivided into several families, v. 17-20.

`1.` Concerning each of these three classes we have an account, `(1.)` Of
their number. The Gershonites were 7500. The Kohathites were 8600. The
Merarites were 6200. The rest of the tribes had not their subordinate
families numbered by themselves as those of Levi; this honour God put
upon his own tribe. `(2.)` Of their post about the tabernacle on which
they were to attend. The Gershonites pitched behind the tabernacle,
westward, v. 23. The Kohathites on the right hand, southward, v. 29. The
Merarites on the left hand, northward, v. 35. And, to complete the
square, Moses and Aaron, with the priests, encamped in the front,
eastward, v. 38. Thus was the tabernacle surrounded with its guards; and
thus does the angel of the Lord encamp round about those that fear him,
those living temples, Ps. 34:7. Every one knew his place, and must
therein abide with God. `(3.)` Of their chief or head. As each class had
its own place, so each had its own prince. The commander of the
Gershonites was Eliasaph (v. 24); of the Kohathites Elizaphan (v. 30),
of whom we read (Lev. 10:4) that he was one of the bearers at the
funeral of Nadab and Abihu; of the Merarites Zuriel, v. 35. `(4.)` Of
their charge, when the camp moved. Each class knew their own business;
it was requisite they should, for that which is every body\'s work often
proves nobody\'s work. The Gershonites were charged with the custody and
carriage of all the curtains and hangings and coverings of the
tabernacle and court (v. 25, 26), the Kohathites of all the furniture of
the tabernacle-the ark, altar, table, etc. (v. 31, 32), the Merarites of
the heavy carriage, boards, bars, pillars, etc., v. 36, 37.

`2.` Here we may observe, `(1.)` That the Kohathites, though they were the
second house, yet were preferred before the elder family of the
Gershonites. Besides that Aaron and the priests were of that family,
they were more numerous, and their post and charge more honourable,
which probably was ordered to put an honour upon Moses, who was of that
family. Yet, `(2.)` The posterity of Moses were not at all dignified or
privileged, but stood upon the level with other Levites, that it might
appear he did not seek the advancement of his own family, nor to entail
any honours upon it either in church or state; he that had honour enough
himself coveted not to have his name shine by that borrowed light, but
rather to have the Levites borrow honour from his name. Let none think
contemptibly of the Levites, though inferior to the priests, for Moses
himself though it preferment enough for his sons to be Levites. Probably
it was because the family of Moses were Levites only that in the title
of this chapter, which is concerning that tribe (v. 1), Aaron is put
before Moses.

`III.` The sum total of the numbers of this tribe. They are computed in
all 22,000, v. 39. The sum of the particular families amounts to 300
more; if this had been added to the sum total, the Levites, instead of
being 273 fewer than the first-born, as they were (v. 43), would have
been twenty-seven more, and so the balance would have fallen the other
way; but it is supposed that the 300 which were struck off from the
account when the exchange was to be made were the first-born of the
Levites themselves, born since their coming out of Egypt, which could
not be put into the exchange, because they were already sanctified to
God. But that which is especially observable here is that the tribe of
Levi was by much the least of all the tribes. Note, God\'s part in the
world is too often the smallest part. His chosen are comparatively a
little flock.

### Verses 40-51

Here is the exchange made of the Levites for the first-born. 1. The
first-born were numbered from a month old, v. 42, 43. Those certainly
were not reckoned who, though first-born, had become heads of families
themselves, but those only that were under age; and the learned bishop
Patrick is decidedly of opinion that none were numbered but those only
that were born since their coming out of Egypt, when the first-born were
sanctified, Ex. 13:2. If there were 22,000 first-born males, we may
suppose as many females, and all these brought forth in the first year
after they came out of Egypt, we must hence infer that in the last year
of their servitude, even when it was in the greatest extremity, there
were abundance of marriages made among the Israelites; they were not
discouraged by the present distress, but married in faith, expecting
that God would shortly visit them with mercy, and that their children,
though born in bondage, should live in liberty and honour. And it was a
token of good to them, an evidence that they were blessed of the Lord,
that they were not only kept alive, but greatly increased, in a barren
wilderness. 2. The number of the first-born, and that of the Levites, by
a special providence, came pretty near to each other; thus, when he
divided the nations, he set the bounds of the people according to the
number of the children of Israel, Deu. 32:8. Known unto God are all his
works beforehand, and there is an exact proportion between them, and so
it will appear when they come to be compared. The Levites\' cattle are
said to be taken instead of the firstlings of the cattle of the children
of Israel, that is, the Levites, with all their possessions, were
devoted to God instead of the first-born and all theirs; for, when we
give ourselves to God, all we have passes as appurtenances with the
premises. 3. The small number of first-born which exceeded the number of
the Levites (273 in all) were to be redeemed, at five shekels apiece,
and the redemption-money given to Aaron; for it would not do well to
have them added to the Levites. It is probable that in the exchange they
began with the eldest of the first-born, and so downward, so that those
were to be redeemed with money who were the 273 youngest of the
first-born; more likely so than either that it was determined by lot or
that the money was paid out of the public stock. The church is called
the church of the first-born, which is redeemed, not as these were, with
silver and gold, but, being devoted by sin to the justice of God, is
ransomed with the precious blood of the Son of God.
