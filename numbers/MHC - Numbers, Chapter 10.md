Numbers, Chapter 10
===================

Commentary
----------

In this chapter we have, `I.` Orders given about the making and using of
silver trumpets, which seems to have been the last of all the
commandments God gave upon mount Sinai, and one of the least, yet not
without its significancy (v. 1-10). `II.` The history of the removal of
Israel\'s camp from mount Sinai, and their orderly march into the
wilderness of Paran (v. 11-28). `III.` Moses\'s treaty with Hobab, his
brother-in-law (v. 29-32). `IV.` Moses\'s prayer at the removing and
resting of the ark (v. 33, etc.).

### Verses 1-10

We have here directions concerning the public notices that were to be
given to the people upon several occasions by sound of trumpet. In a
thing of this nature, one would think, Moses needed not to have been
taught of God: his own reason might teach him the conveniency of
trumpets; but the constitution of Israel was to be in every thing
divine, and therefore even in this matter, small as it seems. Moses is
here directed, 1. About the making of them. They must be made of silver;
not cast but of beaten work (as some read it), the matter and shape, no
doubt, very fit for the purpose. He was now ordered to make but two,
because there were but two priests to use them. But in Solomon\'s time
we read of 120 priests sounding with trumpets, 2 Chr. 5:12. The form of
these trumpets is supposed to have been much like ours at this day. 2.
Who were to make use of them; not any inferior person, but the priests
themselves, the sons of Aaron, v. 8. As great as they were, they must
not think it a disparagement to them to be trumpeters in the house of
God; the meanest office there was honourable. This signified that the
Lord\'s ministers should lift up their voice like a trumpet, to show
people their sins (Isa. 58:1), to call them to Christ, Isa. 27:13. 3.
Upon what occasions the trumpets were to be sounded. `(1.)` For the
calling of assemblies, v. 2. Thus they are told to blow the trumpet in
Zion for the calling of a solemn assembly together, to sanctify a fast,
Joel 2:15. Public notice ought to be given of the time and place of
religious assemblies; for the invitation to the benefit or ordinances is
general: whoever will, let him come. wisdom cries in the chief places of
concourse. But, that the trumpet might not give an uncertain sound, they
are directed, if only the princes and elders were to meet, to blow but
one of the trumpets; less should serve to call them together, who ought
to be examples of forwardness in any thing that is good: but, if the
body of the people were to be called together, both the trumpets must be
sounded, that they might be heard at the greater distance. In allusion
to this, they are said to be blessed that hear the joyful sound (Ps.
89:15), that is, that are invited and called upon to wait upon God in
public ordinances, Ps. 122:1. And the general assembly at the great day
will be summoned by the sound of the archangel\'s trumpet, Mt. 24:31.
`(2.)` For the journeying of the camps, to give notice when each squadron
must move; for no man\'s voice could reach to give the word of command:
soldiers with us that are well disciplined may be exercised by beat of
drums. When the trumpets were blown for this purpose, they mustsound an
alarm (v. 5), a broken, quavering, interrupted sound, which was proper
to excite and encourage the minds of people in their marches against
their enemies; whereas a continued equal sound was more proper for the
calling of the assembly together (v. 7): yet when the people were called
together to deprecate God\'s judgments we find an alarm sounded, Joel
2:1. At the first sounding, Judah\'s squadron marched, at the second
Reuben\'s, at the third Ephraim\'s, at the fourth Dan\'s, v. 5, 6. And
some think that this was intended to sanctify their marches, for thus
were proclaimed by the priests, who were God\'s mouth to the people, not
only the divine orders given them to move, but the divine blessing upon
them in all their motions. He that hath ears, let him hear that God is
with them of a truth. King Abijah valued himself and his army very much
upon this (2 Chr. 13:12), God himself is with us for our captain and his
priests with sounding trumpets. `(3.)` For the animating and encouraging
of their armies, when they went out in battle (v. 9): \"If you go to
war, blow with the trumpets, signifying thereby your appeal to heaven
for the decision of the controversy, and your prayer to God to give you
victory; and God will own this his own institution, and you shall be
remembered before the Lord your God.\" God will take notice of this
sound of the trumpet, and be engaged to fight their battles, and let all
the people take notice of it, and be encouraged to fight his, as David,
when he heard a sound of a going upon the tops of the mulberry trees.
Not that God needed to be awaked by sound of trumpet any more than
Christ needed to be awaked by his disciples in the storm, Mt. 8:25. But
where he intends mercy it is his will that we should solicit it;
ministers must stir up the good soldiers of Jesus Christ to fight
manfully against sin, the world, and the devil, by assuring them that
Christ is the captain of their salvation, and will tread Satan under
their feet. `(4.)` For the solemnizing of their sacred feasts, v. 10. One
of their feasts was called a memorial of the blowing of trumpets, Lev.
23:23, etc. And it should seem they were thus to grace the solemnity of
all their feasts (Ps. 81:3), and their sacrifices (2 Chr. 29:27), to
intimate with what joy and delight they performed their duty to God, and
to raise the minds of those that attended the services to a holy triumph
in the God they worshipped. And then their performances were for a
memorial before God; for he takes pleasure in our religious exercises
when we take pleasure in them. Holy work should be done with holy joy.

### Verses 11-28

Here is, `I.` A general account of the removal of the camp of Israel from
mount Sinai, before which mountain it had lain now about a year, in
which time and place a great deal of memorable business was done. Of
this removal, it should seem, God gave them notice some time before
(Deu. 1:6, 7): You have dwelt long enough in this mountain, turn you and
take your journey towards the land of promise. The apostle tells us that
mount Sinai genders to bondage (Gal. 4:24), and signifies the law there
given, which is of use indeed as a schoolmaster to bring us to Christ,
yet we must not rest in it, but advance towards the joys and liberties
of the children of God, for our happiness is conferred not by the law,
but by promise. Observe, 1. The signal given (v. 11): The cloud was
taken up, and we may suppose it stood for some time, till they were
ready to march; and a great deal of work it was to take down all those
tents, and pack up all those goods that they had there; but every family
being employed about its own, and all at the same time, many hands made
quick work of it. 2. The march began: They took their journey according
to the commandment of the Lord, and just as the cloud led them, v. 13.
Some think that mention is thus frequently made in this and the
foregoing chapter of the commandment of the Lord, guiding and governing
them in all their travels, to obviate the calumny and reproach which
were afterwards thrown upon Israel, that they tarried so long in the
wilderness, because they had lost themselves there, and could not find
the way out. No, the matter was not so; in every stage, in every step,
they were under divine direction; and, if they knew not where they were,
yet he that led them knew. Note, Those that have given up themselves to
the direction of God\'s word and Spirit steer a steady course, even when
they seem to be bewildered. While they are sure they cannot lose their
God and guide, they need not fear losing their way. 3. The place they
rested in, after three days\' march: They went out of the wilderness of
Sinai, and rested in the wilderness of Paran. Note, All our removals in
this world are but from one wilderness to another. The changes which we
think will be for the better do not always prove so; while we carry
about with us, wherever we go, the common infirmities of human nature,
we must expect, wherever we go, to meet with its common calamities; we
shall never be at rest, never at home, till we come to heaven, and all
will be well there.

`II.` A particular draught of the order of their march, according to the
late model. 1. Judah\'s squadron marched first, v. 14-16. The leading
standard, now lodged with that tribe, was an earnest of the sceptre
which in David\'s time should be committed to it, and looked further to
the captain of our salvation, of whom it was likewise foretold that unto
him should the gathering of the people be. 2. Then came those two
families of the Levites which were entrusted to carry the tabernacle. As
soon as ever the cloud was taken up, the tabernacle was taken down, and
packed up for removing, v. 17. And here the six wagons came laden with
the more bulky part of the tabernacle. This frequent removing of the
tabernacle in all their journeys signified the movableness of that
ceremonial dispensation. That which was so often shifted would at length
vanish away, Heb. 8:13. 3. Reuben\'s squadron marched forward next,
taking place after Judah, according to the commandment of the Lord, v.
18-20. 4. Then the Kohathites followed with their charge, the sacred
furniture of the tabernacle, in the midst of the camp, the safest and
most honourable place, v. 21. And they (that is, says the margin, the
Gershonites and Merarites) did set up the tabernacle against they came;
and perhaps it is expressed thus generally because, if there was
occasion, not those Levites only, but the other Israelites that were in
the first squadron, lent a hand to the tabernacle to hasten the rearing
of it up, even before they set up their own tents. 5. Ephraim\'s
squadron followed next after the ark (v. 22-24), to which some think the
psalmist alludes when he prays (Ps. 80:2), Before Ephraim, Benjamin, and
Manasseh, the three tribes that composed this squadron, stir up thy
strength (and the ark is called his strength, Ps. 78:61), and come and
save us. 6. Dan\'s squadron followed last, v. 25-27. It is called the
rearward, or gathering host, of all the camps, because it gathered up
all that were left behind; not the women and children (these we may
suppose were taken care of by the heads of their families in their
respective tribes), but all the unclean, the mixed multitude, and all
that were weak and feeble, and cast behind in their march. Note, He that
leadeth Joseph like a flock has a tender regard to the hindmost (Eze.
34:16), that cannot keep pace with the rest, and of all that are given
him he will lose none, Jn. 17:11.

### Verses 29-36

Here is, `I.` An account of what passed between Moses and Hobab, now upon
this advance which the camp of Israel made towards Canaan. Some think
that Hobab was the same with Jethro, Moses\'s father-in-law, and that
the story, Ex. 18, should come in here; it seems more probable that
Hobab was the son of Jethro, alias Reuel, or Raguel (Ex. 2:18), and that
when the father, being aged, went to his own land (Ex. 18:27), he left
his son Hobab with Moses, as Barzillai left Chimham with David; and the
same word signifies both a father-in-law and a brother-in-law. Now this
Hobab staid contentedly with Israel while they encamped at mount Sinai,
near his own country; but, now that they were removing, he was for going
back to his own country and kindred, and his father\'s house. Here is,
`1.` The kind invitation Moses gives him to go forward with them to
Canaan, v. 29. He tempts him with a promise that they would certainly be
kind to him, and puts God\'s word in for security: The Lord hath spoken
good concerning Israel. As if he had said, \"Come, cast in thy lot among
us, and thou shalt fare as we fare; and we have the promise of God that
we shall fare well.\" Note, Those that are bound for the heavenly Canaan
should invite and encourage all their friends to go along with them, for
we shall have never the less of the treasures of the covenant, and the
joys of heaven, for others coming in to share with us. And what argument
can be more powerful with us to take God\'s people for our people than
this, that God hath spoken good concerning them? It is good having
fellowship with those that have fellowship with God (1 Jn. 1:3), and
going with those with whom God is, Zec. 8:23. 2. Hobab\'s inclination,
and present resolution, to go back to his own country, v. 30. One would
have thought that he who had seen so much of the special presence of God
with Israel, and such surprising tokens of his favour to them, would not
have needed much invitation to embark with them. But his refusal must be
imputed to the affection he had for his native air and soil, which was
not overpowered, as it ought to have been, by a believing regard to the
promise of God and a value for covenant blessings. He was indeed a son
of Abraham\'s loins (for the Midianites descended from Abraham by
Keturah), but not an heir of Abraham\'s faith (Heb. 11:8), else he would
not have given Moses this answer. Note, The things of this world, which
are seen, draw strongly from the pursuit of the things of the other
world, which are not seen. The magnetic virtue of this earth prevails
with most people above the attractives of heaven itself. 3. The great
importunity Moses used with him to alter his resolution, v. 31, 32. He
urges, `(1.)` That he might be serviceable to them: \"We are to encamp in
the wilderness\" (a country well known to Hobab), \"and thou mayest be
to us instead of eyes, not to show us where we must encamp, nor what way
we must march\" (which the cloud was to direct), \"but to show us the
conveniences and inconveniences of the place we march through and encamp
in, that we may make the best use we can of the conveniences, and the
best fence we can against the inconveniences.\" Note, It will very well
consist with our trust in God\'s providence to make use of the help of
our friends in those things wherein they are capable of being
serviceable to us. Even those that were led by miracle must not slight
the ordinary means of direction. Some think that Moses suggests this to
Hobab, not because he expected much benefit from his information, but to
please him with the thought of being some way useful to so great a body,
and so to draw him on with them, by inspiring him with an ambition to
obtain that honour. Calvin gives quite another sense of this place, very
agreeably with the original, which yet I do not find taken notice of by
any since. \"Leave us not, I pray thee, but come along, to share with us
in the promised land, for therefore hast thou known our encampment in
the wilderness, and hast been to us instead of eyes; and we cannot make
thee amends for sharing with us in our hardships, and doing us so many
good offices, unless thou go with us to Canaan. Surely for this reason
thou didst set out with us that thou mightest go on with us.\" Note,
Those that have begun well should use that as a reason for their
persevering, because otherwise they lose the benefit and recompence of
all they have done and suffered. `(2.)` That they would be kind to him:
What goodness the Lord shall do to us, the same we will do to thee, v.
32. Note, `[1.]` We can give only what we receive. We can do no more
service and kindness to our friends than God is pleased to put it into
the power of our hand to do. This is all we dare promise, to do good as
God shall enable us. `[2.]` Those that share with God\'s Israel in their
labours and hardships shall share with them in their comforts and
honours. Those that are wiling to take their lot with them in the
wilderness shall have their lot with them in Canaan; if we suffer with
them we shall reign with them, 2 Tim. 2:12; Lu. 22:28, 29.

We do not find any reply that Hobab here made to Moses, and therefore we
hope that his silence gave consent, and he did not leave them, but that,
when he perceived he might be useful, he preferred that before the
gratifying of his own inclination; in this case he left us a good
example. And we find (Jdg. 1:16; 1 Sa. 15:6) that his family was no
loser by it.

`II.` An account of the communion between God and Israel in this removal.
They left the mount of the Lord (v. 33), that Mount Sinai where they had
seen his glory and heard his voice, and had been taken into covenant
with him (they must not expect that such appearances of God to them as
they had there been blessed with should be constant); they departed from
that celebrated mountain, which we never read of in scripture any more,
unless with reference to these past stories; now farewell, Sinai; Zion
is the mountain of which God has said. This is my rest for ever (Ps.
132:14), and of which we must say so. But when they left the mount of
the Lord they took with them the ark of the covenant of the Lord, by
which their stated communion with God was to be kept up. For,

`1.` By it God did direct their paths. The ark of the covenant went
before them, some think in place, at least in this removal; others think
only in influence; though it was carried in the midst of the camp, yet
the cloud that hovered over it directed all their motions. The ark (that
is, the God of the ark) is said to search out a resting place for them;
not that God\'s infinite wisdom and knowledge need to make searches, but
every place they were directed to was as convenient for them as if the
wisest man they had among them had been employed to go before them, and
mark out their camp to the best advantage. thus Canaan is said to be a
land which God spied out, Eze. 20:6.

`2.` By it they did in all their ways acknowledge God, looking upon it as
a token of God\'s presence; when that moved, or rested, they had their
eye up unto God. Moses, as the mouth of the congregation, lifted up a
prayer, both at the removing and at the resting of the ark; thus their
going out and coming in were sanctified by prayer, and it is an example
to us to begin and end every day\'s journey, and every day\'s work, with
prayer.

`(1.)` Here is his prayer when the ark set forward: Rise up, Lord, and let
thy enemies be scattered, v. 35. They were now in a desolate country,
but they were marching towards an enemy\'s country, and their dependence
was upon God for success and victory in their wars, as well as for
direction and supply in the wilderness. David used this prayer long
after (Ps. 68:1), for he also fought the Lords\' battles. Note, `[1.]`
There are those in the world that are enemies to God, and haters of him:
secret and open enemies; enemies to his truths, his laws, his
ordinances, his people. `[2.]` The scattering and defeating of God\'s
enemies is a thing to be earnestly desired, and believingly expected, by
all the Lord\'s people. This prayer is a prophecy. Those that persist in
rebellion against God are hasting towards their own ruin. `[3.]` For the
scattering and defeating of God\'s enemies, there needs no more but
God\'s arising. When God arose to judgment, the work was soon done, Ps.
76:8, 9. \"Rise, Lord, as the sun riseth to scatter the shadows of the
night.\" Christ\'s rising from the dead scattered his enemies, Ps.
68:18.

`(2.)` His prayer when the ark rested, v. 36. `[1.]` That God would cause
his people to rest. So some read it, \"Return, O Lord, the many
thousands of Israel, return them to their rest again after this
fatigue.\" Thus it is said (Isa. 63:14), The Spirit of the Lord caused
him to rest. Thus he prays that God would give Israel success and
victory abroad, and peace and tranquillity at home. `[2.]` That God
himself would take up his rest among them. So we read it: Return to the
thousands of Israel, the ten thousand thousand, so the word is. Note,
First, The church of God is a great body; there are many thousands
belonging to God\'s Israel. Secondly, We ought in our prayers to concern
ourselves for this body. Thirdly, The welfare and happiness of the
Israel of God consist in the continual presence of God among them. Their
safety consists not in their numbers, though they are thousands, many
thousands, but in the favour of God, and his gracious return to them and
residence with them. These thousands are cyphers; he is the figure: and
upon this account, Happy art thou, O Israel! who is like unto thee, O
people!
