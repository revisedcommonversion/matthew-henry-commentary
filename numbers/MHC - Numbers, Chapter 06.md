Numbers, Chapter 6
==================

Commentary
----------

In this chapter we have, `I.` The law concerning Nazarites, 1. What it was
to which the vow of a Nazarite obliged him (v. 1-8). 2. A remedial law
in case a Nazarite happened to be polluted by the touch of a dead body
(v. 9-12). 3. The solemnity of his discharge when his time was up (v.
13-21). `II.` Instructions given to the priests how they should bless the
people (v. 22, etc.).

### Verses 1-21

After the law for the discovery and shame of those that by sin had made
themselves vile, fitly follows this for the direction and encouragement
of those who by their eminent piety and devotion had made themselves
honourable, and distinguished themselves from their neighbours. It is
very probable that there were those before the making of this law who
went under the character of Nazarites, and were celebrated by that title
as persons professing greater strictness and zeal in religion than other
people; for the vow of a Nazarite is spoken of here as a thing already
well known, but the obligation of it is reduced to a greater certainty
than hitherto it had been. Joseph is called a Nazarite among his
brethren (Gen. 49:26), not only because separate from them, but because
eminent among them. Observe,

`I.` The general character of a Nazarite: it is a person separated unto
the Lord, v. 2. Some were Nazarites for life, either by divine
designation, as Samson (Jdg. 13:5), and John Baptist (Lu. 1:15), or by
their parents\' vow concerning them, as Samuel, 1 Sa. 1:11. Of these
this law speaks not. Others were so for a certain time, and by their own
voluntary engagement, and concerning them rules are given by this law. A
woman might bind herself with the vow of a Nazarite, under the
limitations we find, ch. 30:3, where the vow which the woman is supposed
to vow unto the Lord seems to be meant especially of this vow. The
Nazarites were, 1. Devoted to the Lord during the time of their
Nazariteship, and, it is probable, spent much of their time in the study
of the law, in acts of devotion, and instructing others. An air of piety
was thereby put upon them, and upon their whole conversation. 2. They
were separated from common persons and common things. Those that are
consecrated to God must not be conformed to this world. They
distinguished themselves, not only from others, but from what they
themselves were before and after. 3. They separated themselves by vowing
a vow. Every Israelite was bound by the divine law to love God with all
his heart, but the Nazarites by their own act and deed bound themselves
to some religious observances, as fruits and expressions of that love,
which other Israelites were not bound to. Some such there were, whose
spirits God stirred up to be in their day the ornaments of the church,
the standard-bearers of religion, and patterns of piety. It is spoken of
as a great favour to their nation that God raised up of their young men
for Nazarites, Amos 2:11. The Nazarites were known in the streets and
respected as purer than snow, whiter than milk, Lam. 4:7. Christ was
called in reproach a Nazarene, so were his followers: but he was no
Nazarite according to this law; he drank wine, and touched dead bodies,
yet in his this type had its accomplishment, for in him all purity and
perfection met; and every true Christian is a spiritual Nazarite,
separated by vow unto the Lord. We find St. Paul, by the persuasion of
his friends, in complaisance to the Jews, submitting to this law of the
Nazarites; but at the same time it is declared that the Gentiles should
observe no such thing, Acts 21:24, 25. It was looked upon as a great
honour to a man to be a Nazarite, and therefore if a man speak of it as
a punishment, saying for instance, \"I will be a Nazarite rather than do
so or so,\" he is (say the Jews) a wicked man; but he that vows unto the
Lord in the way of holiness to be a Nazarite, lo, the crown of his God
is upon his head.

`II.` The particular obligations that the Nazarites lay under. That the
fancies of superstitious men might not multiply their restraints
endlessly, God himself lays down the law for them, and gives them the
rule of their profession.

`1.` They must have nothing to do with the fruit of the vine, v. 3, 4.
They must drink no wine nor string drink, nor eat grapes, no, not the
kernel nor the husk; they might not so much as eat a raisin. The learned
Dr. Lightfoot has a conjecture (Hor. Heb. in Luc. 1.15), that, as the
ceremonial pollutions by leprosy and otherwise represented the sinful
state of fallen man, so the institution of the order of Nazarites was
designed to represent the pure and perfect state of man in innocency,
and that the tree of knowledge, forbidden to Adam, was the vine, and for
that reason it was forbidden to the Nazarites, and all the produce of
it. Those who gave the Nazarites wine to drink did the tempter\'s work
(Amos 2:12), persuading them to that forbidden fruit. That it was
reckoned a perfection and praise not to drink wine appears from the
instance of the Rechabites, Jer. 35:6. They were to drink no wine, `(1.)`
That they might be examples of temperance and mortification. Those that
separate themselves to God and to his honour must not gratify the
desires of the body, but keep it under and bring it into subjection.
Drinking a little wine for the stomach\'s sake is allowed, to help that,
1 Tim. 5:23. But drinking much wine for the palate\'s sake, to please
that, does by no means become those who profess to walk not after the
flesh, but after the Spirit. `(2.)` That they might be qualified to employ
themselves in the service of God. They must not drink, lest they should
forget the law (Prov. 31:5), lest they should err through wine, Isa.
28:7. Let all Christians oblige themselves to be very moderate in the
use of wine and strong drink; for, if the love of these once gets the
mastery of a man, he becomes a very easy prey to Satan. It is observable
that because they were to drink no wine (which was the thing mainly
intended) they were to eat nothing that came of the vine, to teach us
with the utmost care and caution to avoid sin and every thing that
borders upon it and leads to it, or may be a temptation to us. Abstain
from all appearance of evil, 1 Th. 5:22.

`2.` They must not cut their hair, v. 5. They must neither poll their
heads nor shave their beards; this was that mark of Samson\'s
Nazariteship which we often read of in his story. Now, `(1.)` This
signified a noble neglect of the body and the ease and ornament of it,
which became those who, being separated to God, ought to be wholly taken
up with their souls, to secure their peace and beauty. It signified that
they had, for the present, renounced all sorts of sensual pleasures and
delights, and resolved to live a life of self-denial and mortification.
Mephibosheth in sorrow trimmed not his beard, 2 Sa. 19:24. `(2.)` Some
observe that long hair is spoken of as a badge of subjection (1 Co.
11:5, etc.); so that the long hair of the Nazarites denoted their
subjection to God, and their putting themselves under his dominion. `(3.)`
By this they were known to all that met them to be Nazarites, and so it
commanded respect. It made them look great without art; it was nature\'s
crown to the head, and a testimony for them that they had preserved
their purity. For, if they had been defiled, their hair must have been
cut, v. 9. See Jer. 7:29.

`3.` They must not come near any dead body, v. 6, 7. Others might touch
dead bodies, and contracted only a ceremonial pollution by it for some
time; some must do it, else the dead must be unburied; but the Nazarites
must not do it, upon pain of forfeiting all the honour of their
Nazariteship. They must not attend the funeral of any relation, no, not
father nor mother, any more than the high priest himself, because the
consecration of his God is upon his head. Those that separate themselves
to God must learn, `(1.)` To distinguish themselves, and do more than
others. `(2.)` To keep their consciences pure from dead works, and not to
touch the unclean thing. The greater profession of religion we make, and
the more eminent we appear, the greater care we must take to avoid all
sin, for we have so much the more honour to lose by it. `(3.)` To moderate
their affections even to their near relations, so as not to let their
sorrow for the loss of them break in upon their joy in God and
submission to his will. See Mt. 8:21, 22.

`4.` All the days of their separation they must be holy to the Lord, v.
8. This was the meaning of those external observances, and without this
they were of no account. The Nazarites must be devoted to God, employed
for him, and their minds intent upon him; they must keep themselves pure
in heart and life, and be in every thing conformable to the divine image
and will; this is to be holy, this is to be a Nazarite indeed.

`III.` The provision that was made for the cleansing of a Nazarite, if he
happened unavoidably to contract a ceremonial pollution by the touch of
a dead body. No penalty is ordered by this law for the wilful breach of
the foregoing laws; for it was not supposed that a man who had so much
religion as to make that vow could have so little as to break it
presumptuously: nor could it be supposed that he should drink wine, or
have his hair cut, but by his own fault; but purely by the providence of
God, without any fault of his own, he might be near a dead body, and
that is the case put (v. 9): If a man die very suddenly by him, he has
defiled the head of his consecration. Note, Death sometimes takes men
away very suddenly, and without any previous warning. A man might be
well and dead in so little a time that the most careful Nazarite could
not avoid being polluted by the dead body; so short a step is it
sometimes, and so soon taken, from time to eternity. God prepare us for
sudden death! In this case, 1. He must be purified from the ceremonial
pollution he had contracted, as others must, upon the seventh day, v. 9.
Nay, more was required for the purifying of the Nazarite than of any
other person that had touched a dead body; he must bring a sin-offering
and a burnt-offering, and an atonement must be made for him, v. 10, 11.
This teaches us that sins of infirmity, and the faults we are overtaken
in by surprise, must be seriously repented of, and that an application
must be made of the virtue of Christ\'s sacrifice to our souls for the
forgiveness of them every day, 1 Jn. 2:1, 2. It teaches us also that, if
those who make an eminent profession of religion do any thing to sully
the reputation of their profession, more is expected from them than
others, for the retrieving both of their peace and of their credit. 2.
He must begin the days of his separation again; for all that were past
before his pollution, though coming ever so near the period of his time
set, were lost, and not reckoned to him, v. 12. This obliged them to be
very careful not to defile themselves by the dead, for that was the only
thing that made them lose their time, and it teaches us that if a
righteous man turn away from his righteousness, and defile himself with
dead works, all his righteousness that he has done shall be lost to him,
Eze. 33:13. It is all lost, all in vain, if he do not persevere, Gal.
3:4. He must begin again, and do his first works.

`IV.` The law for the solemn discharge of a Nazarite from his vow, when
he had completed the time he fixed to himself. Before the expiration of
that term he could not be discharged; before he vowed, it was in his own
power, but it was too late after the vow to make enquiry. The Jews say
that the time of a Nazarite\'s vow could not be less than thirty days;
and if a man said, \"I will be a Nazarite but for two days,\" yet he was
bound for thirty; but it should seem Paul\'s vow was for only seven days
(Acts 21:27), or, rather, then he observed the ceremony of finishing
that vow of Nazariteship from which, being at a distance from the
temple, he had discharged himself some years before at Cenchrea only by
the ceremony of cutting his hair, Acts 18:18. When the time of the vowed
separation was out, he was to be made free, 1. Publicly, at the door of
the tabernacle (v. 13), that all might take notice of the finishing of
his vow, and none might be offended if they saw him now drink wine, who
had so lately refused. 2. It was to be done with sacrifices, v. 14. Lest
he should think that by this eminent piece of devotion he had made God a
debtor to him, he is appointed, even when he had finished his vow, to
bring an offering to God; for, when we have done our utmost in duty to
God, still we must own ourselves behind-hand with him. He must bring one
of each sort of the instituted offerings. `(1.)` A burnt-offering, as an
acknowledgment of God\'s sovereign dominion over him and all he had
still, notwithstanding his discharge from this particular vow. `(2.)` A
sin-offering. This, though mentioned second (v. 14), yet seems to have
been offered first (v. 16), for atonement must be made for our sins
before any of our sacrifices can be accepted. And it is very observable
that even the Nazarite, who in the eye of men was purer than snow and
whiter than milk, yet durst not appear before the holy God without a
sin-offering. Though he had fulfilled the vow of his separation without
any pollution, yet he must bring a sacrifice for sin; for there is guilt
insensibly contracted by the best of men, even in their best works-some
good omitted, some ill admitted, which, if we were dealt with in strict
justice, would be our ruin, and in consequence of which it is necessary
for us to receive the atonement, and plead it as our righteousness
before God. `(3.)` A peace-offering, in thankfulness to God who had
enabled him to fulfil his vow, and in supplication to God for grace to
preserve him from ever doing any thing unbecoming one that had been once
a Nazarite, remembering that, though he was now freed from the bonds of
his own vow, he still remained under the bonds of the divine law. `(4.)`
To these were added the meat-offerings and drink-offerings, according to
the manner (v. 15, 17), for these always accompanied the burnt-offerings
and peace-offerings: and, besides these, a basket of unleavened cakes,
and wafers. `(5.)` Part of the peace-offering, with a cake and wafer, was
to be waved for a wave-offering (v. 19, 20); and this was a gratuity to
the priest, who had it for his pains, after it had been first presented
to God. `(6.)` Besides all this, he might bring his free-will offerings,
such as his hand shall get, v. 21. More than this he might bring, but
not less. And, to grace the solemnity, it was common upon this occasion
to have their friends to be at charges with them, Acts 21:24. Lastly,
One ceremony more was appointed, which was like the cancelling of the
bond when the condition is performed, and that was the cutting off of
his hair, which had been suffered to grow all the time of his being a
Nazarite, and burning it in the fire over which the peace-offerings were
boiling, v. 18. This intimated that his full performance of his vow was
acceptable to God in Christ the great sacrifice, and not otherwise.
Learn hence to vow and pay to the Lord our God, for he has no pleasure
in fools.

### Verses 22-27

Here, `I.` The priests, among other good offices which they were to do,
are appointed solemnly to bless the people in the name of the Lord, v.
23. It was part of their work, Deu. 21:5. Hereby God put an honour upon
the priests, for the less is blessed of the better; and hereby he gave
great comfort and satisfaction to the people, who looked upon the priest
as God\'s mouth to them. Though the priests of himself could do no more
than beg a blessing, yet being an intercessor by office, and doing that
in his name who commands the blessing, the prayer carried with it a
promise, and he pronounced it as one having authority with his hands
lifted up and his face towards the people. Now, 1. This was a type of
Christ\'s errand into the world, which was to bless us (Acts 3:26), as
the high priest of our profession. The last thing he did on earth was
with uplifted hands to bless his disciples, Lu. 24:50, 51. The learned
bishop Pearson observes it as a tradition of the Jews that the priests
blessed the people only at the close of the morning sacrifice, not of
the evening sacrifice, to show (says he) that in the last days, the days
of the Messiah, which are (as it were) the evening of the world, the
benediction of the law should cease, and the blessing of Christ should
take place. 2. It was a pattern to gospel ministers, the masters of
assemblies, who are in like manner to dismiss their solemn assemblies
with a blessing. The same that are God\'s mouth to his people, to teach
and command them, are his mouth likewise to bless them; and those that
receive the law shall receive the blessing. The Hebrew doctors warn the
people that they say not, \"What availeth the blessing of this poor
simple priest? \"For,\" say they, \"the receiving of the blessing
depends, not on the priest, but on the holy blessed God.\"

`II.` A form of blessing is here prescribed them. In their other
devotions no form was prescribed, but this being God\'s command
concerning benediction, that it might not look like any thing of their
own, he puts the very words in their mouths, v. 24-26. Here observe, 1.
That the blessing is commanded upon each particular person: The Lord
bless thee. They must each of them prepare themselves to receive the
blessing, and then they should find enough in it to make them every man
happy. Blessed shalt thou be, Deu. 28:3. If we take the law to
ourselves, we may take the blessing to ourselves, as if our names were
inserted. 2. That the name Jehovah is three times repeated in it, and
(as the critics observe) each with a different accent in the original;
the Jews themselves think there is some mystery in this, and we know
what it is, the New Testament having explained it, which directs us to
expect the blessing from the grace of our Lord Jesus Christ, the love of
the Father, and the communion of the Holy Ghost, each of which persons
is Jehovah, and yet they are \"not three Lords, but one Lord,\" 2 Co.
13:14. 3. That the favour of God is all in all in this blessing, for
that is the fountain of all good. `(1.)` The Lord bless thee! Our blessing
God is only our speaking well of him; his blessing us is doing well for
us; those whom he blesses are blessed indeed. `(2.)` The Lord make his
face shine upon thee, alluding to the shining of the sun upon the earth,
to enlighten and comfort it, and to renew the face of it. \"The Lord
love thee and cause thee to know that he loves thee.\" We cannot but be
happy if we have God\'s love; and we cannot but be easy if we know that
we have it. `(3.)` The Lord lift up his countenance upon thee. This is to
the same purport with the former, and it seems to allude to the smiles
of a father upon his child, or of a man upon his friend whom he takes
pleasure in. If God give us the assurances of his special favour and his
acceptance of us, this will put gladness into the heart, Ps. 4:7, 8. 4.
That the fruits of this favour conveyed by this blessing are protection,
pardon, and peace. `(1.)` Protection from evil, v. 24. The Lord keep thee,
for it is he that keeps Israel, and neither slumbers nor sleeps (Ps.
121:4), and all believers are kept by the power of God. `(2.)` Pardon of
sin, v. 25. The Lord be gracious, or merciful, unto thee. `(3.)` Peace (v.
26), including all that good which goes to make up a complete happiness.

`III.` God here promises to ratify and confirm the blessing: They shall
put my name upon the children of Israel, v. 27. God gives them leave to
make use of his name in blessing the people, and to bless them as his
people, called by his name. This included all the blessings they could
pronounce upon them, to mark them for God\'s peculiar, the people of his
choice and love. God\'s name upon them was their honour, their comfort,
their safety, their plea. We are called by thy name, leave us not. It is
added, and I will bless them. Note, A divine blessing goes along with
divine institutions, and puts virtue and efficacy into them. What Christ
says of the peace is true of the blessing, \"Peace to this
congregation,\" if the sons of peace and heirs of blessing be there, the
peace, the blessing, shall rest upon them, Lu. 10:5, 6. For in every
place where God records his name he will meet his people and bless them.
