Numbers, Chapter 5
==================

Commentary
----------

In this chapter we have, `I.` An order, pursuant to the laws already made,
for the removing of the unclean out of the camp (v. 1-4). `II.` A
repetition of the laws concerning restitution, in case of wrong done to
a neighbour (v. 5-8), and concerning the appropriating of the hallowed
things to the priests (v. 9, 10). `III.` A new law made concerning the
trial of a wife suspected of adultery, by the waters of jealousy (v. 11,
etc.).

### Verses 1-10

Here is, `I.` A command for the purifying of the camp, by turning out from
within its lines all those that were ceremonially unclean, by issues,
leprosies, or the touch of dead bodies, until they were cleansed
according to the law, v. 2, 3.

`1.` These orders are executed immediately, v. 4. `(1.)` The camp was now
newly-modelled and put in order, and therefore, to complete the
reformation of it, it is next to be cleansed. Note, The purity of the
church must be as carefully consulted and preserved as the peace and
order of it. It is requisite, not only that every Israelite be confined
to his own standard, but that every polluted Israelite be separated from
it. The wisdom from above is first pure, then peaceable. `(2.)` God\'s
tabernacle was now fixed in the midst of their camp, and therefore they
must be careful to keep it clean. Note, The greater profession of
religion any house or family make the more they are obliged to put away
iniquity far from their tabernacle, Job 22:23. The person, the place, in
the midst of which God dwells, must not be defiled; for, if it be, he
will be affronted, offended, and provoked to withdraw, 1 Co. 3:16, 17.

`2.` This expulsion of the unclean out of the camp was to signify, `(1.)`
What the governors of the church ought to do: they must separate between
the precious and the vile, and purge out scandalous persons, as old
leaven (1 Co. 5:8, 13), lest others should be infected and defiled, Heb.
12:15. It is for the glory of Christ and the edification of his church
that those who are openly and incorrigibly profane and vicious should be
put out and kept from Christian communion till they repent. `(2.)` What
God himself will do in the great day: he will thoroughly purge his
floor, and gather out of his kingdom all things that offend. As here the
unclean were shut out of the camp, so into the new Jerusalem no unclean
thing shall enter, Rev. 21:27.

`II.` A law concerning restitution, in case of wrong done to a neighbour.
It is called a sin that men commit (v. 6), because it is common among
men; a sin of man, that is, a sin against man, so it is thought it
should be translated and understood. If a man overreach or defraud his
brother in any matter, it is to be looked upon as a trespass against the
Lord, who is the protector of right, the punisher of wrong, and who
strictly charges and commands us to do justly. Now what is to be done
when a man\'s awakened conscience charges him with guilt of this kind,
and brings it to his remembrance though done long ago? 1. He must
confess his sin, confess it to God, confess it to his neighbour, and so
take shame to himself. If he have denied it before, though it go against
the grain to own himself in a lie, yet he must do it; because his heart
was hardened he denied it, therefore he has no other way of making it
appear that his heart is now softened but by confessing it. 2. He must
bring a sacrifice, a ram of atonement, v. 8. Satisfaction must be made
for the offence done to God, whose law is broken, as well as for the
loss sustained by our neighbour; restitution in this case is not
sufficient without faith and repentance. 3. Yet the sacrifices would not
be accepted till full amends were made to the party wronged, not only
the principal, but a fifth part added to it, v. 7. It is certain that
while that which is got by injustice is knowingly retained in the hands
the guilt of the injustice remains upon the conscience, and is not
purged by sacrifice nor offering, prayers not tears, for it is one and
the same continued act of sin persisted in. This law we had before (Lev.
6:4), and it is here added that if the party wronged was dead, and he
had no near kinsman who was entitled to the debt, or if it was any way
uncertain to whom the restitution should be made, this should not serve
for an excuse to detain what was unjustly gotten; to whomsoever it
pertained, it was certainly none of his that got it by sin, and
therefore it must be given to the priest, v. 8. If there were any that
could make out a title to it, it must not be given to the priest (God
hates robbery for burnt-offerings); but, if there were not, then it
lapsed to the great Lord (ob defectum sanguinis-for want of issue), and
the priests were his receivers. Note, Some work of piety or charity is a
piece of necessary justice to be done by those who are conscience to
themselves that they have done wrong, but know not how otherwise to make
restitution; what is not our property will never be our profit.

`III.` A general rule concerning hallowed things given upon this
occasion, that, whatever was given to the priest, his it shall be, v. 9,
10. 1. He that gave it was not to receive his gift again upon any
pretence whatsoever. This law ratifies and confirms all grants for pious
uses, that people might not give things to the priests in a fit of zeal,
and then recall them in a fit of vexation. 2. The other priests should
not come in sharers with that priest who then officiated, and to whom
the hallowed thing, whatever it was, was given. Let him that was most
ready and diligent in attending fare the better for it: if he do the
work, let him have the pay, and much good may it do him.

### Verses 11-31

We have here the law concerning the solemn trial of a wife whose husband
was jealous of her. Observe,

`I.` What was the case supposed: That a man had some reason to suspect his
wife to have committed adultery, v. 12-14. Here, 1. The sin of adultery
is justly represented as an exceedingly sinful sin; it is going aside
from God and virtue, and the good way, Prov. 2:17. It is committing a
trespass against the husband, robbing him of his honour, alienating his
right, introducing a spurious breed into his family to share with his
children in his estate, and violating her covenant with him. It is being
defiled; for nothing pollutes the mind and conscience more than this sin
does. 2. It is supposed to be a sin which great care is taken by the
sinners to conceal, which there is no witness of. The eye of the
adulterer waits for the twilight, Job 24:15. And the adulteress takes
her opportunity when the good man is not at home, Prov. 7:19. It would
not covet to be secret if it were not shameful; and the devil who draws
sinners to this sin teaches them how to cover it. 3. The spirit of
jealousy is supposed to come upon the husband, of which Solomon says, It
is the rage of a man (Prov. 6:34), and that it is cruel as the grave,
Cant. 8:6. 4. \"Yet\" (say the Jewish writers) \"he must make it appear
that he has some just cause for the suspicion.\" The rule they give is,
\"If the husband have said unto his wife before witnesses, \'Be not thou
in secret with such a man;\' and, notwithstanding that admonition, it is
afterwards proved that she was in secret with that man, though her
father or her brother, then he may compel her to drink the bitter
water.\" But the law here does not tie him to that particular method of
proving the just cause of his suspicion; it might be otherwise proved.
In case it could be proved that she had committed adultery, she was to
be put to death (Lev. 20:10); but, if it was uncertain, then this law
took place. Hence, `(1.)` Let all wives be admonished not to give any the
least occasion for the suspicion of their chastity; it is not enough
that they abstain from the evil of uncleanness, but they must abstain
from all appearance of it, from every thing that looks like it, or leads
to it, or may give the least umbrage to jealousy; for how great a matter
may a little fire kindle! `(2.)` Let all husbands be admonished not to
entertain any causeless or unjust suspicions of their wives. If charity
in general, much more conjugal affection, teaches to think no evil, 1
Co. 13:5. It is the happiness of the virtuous woman that the heart of
her husband does safely trust in her, Prov. 31:11.

`II.` What was the course prescribed in this case, that, if the suspected
wife was innocent, she might not continue under the reproach and
uneasiness of her husband\'s jealousy, and, if guilty, her sin might
find her out, and others might hear, and fear, and take warning.

`1.` The process of the trial must be thus:-`(1.)` Her husband must bring
her to the priest, with the witnesses that could prove the ground of his
suspicion, and desire that she might be put upon her trial. The Jews say
that the priest was first to endeavour to persuade her to confess the
truth, saying to this purport, \"Dear daughter, perhaps thou wast
overtaken by drinking wine, or wast carried away by the heat of youth or
the examples of bad neighbours; come, confess the truth, for the sake of
his great name which is described in the most sacred ceremony, and do
not let it be blotted out with the bitter water.\" If she confessed,
saying, \"I am defiled,\" she was not put to death, but was divorced and
lost her dowry; if she said, \"I am pure,\" then they proceeded. `(2.)` He
must bring a coarse offering of barley-meal, without oil or
frankincense, agreeably to the present afflicted state of his family;
for a great affliction it was either to have cause to be jealous or to
be jealous without cause. It is an offering of memorial, to signify that
what was to be done was intended as a religious appeal to the
omniscience and justice of God. `(3.)` The priest was to prepare the water
of jealousy, the holy water out of the laver at which the priests were
to wash when they ministered; this must be brought in an earthen vessel,
containing (they say) about a pint; and it must be an earthen vessel,
because the coarser and plainer every thing was the more agreeable it
was to the occasion. Dust must be put into the water, to signify the
reproach she lay under, and the shame she ought to take to herself,
putting her mouth in the dust; but dust from the floor of the
tabernacle, to put an honour upon every thing that pertained to the
place God had chosen to put his name there, and to keep up in the people
a reverence for it; see Jn. 8:6. `(4.)` The woman was to be set before the
Lord, at the east gate of the temple-court (say the Jews), and her head
was to be uncovered, in token of her sorrowful condition; and there she
stood for a spectacle to the world, that other women might learn not to
do after her lewdness, Eze. 23:48. Only the Jews say, \"Her own servants
were not to be present, that she might not seem vile in their sight, who
were to give honour to her; her husband also must be dismissed.\" `(5.)`
The priest was to adjure her to tell the truth, and to denounce the
curse of God against her if she were guilty, and to declare what would
be the effect of her drinking the water of jealousy, v. 19-22. He must
assure her that, if she were innocent, the water would do her no harm,
v. 19. None need fear the curse of the law if they have not broken the
commands of the law. But, if she were guilty, this water would be poison
to her, it would make her belly to swell and her thigh to rot, and she
should be a curse or abomination among her people, v. 21, 22. To this
she must say, Amen, as Israel must do to the curses pronounced on mount
Ebal, Deu. 27:15-26. Some think the Amen, being doubled, respects both
parts of the adjuration, both that which freed her if innocent and that
which condemned her if guilty. No woman, if she were guilty, could say
Amen to this adjuration, and drink the water upon it, unless she
disbelieved the truth of God or defied his justice, and had come to such
a pitch of impudence and hard-heartedness in sin as to challenge God
Almighty to do his worst, and choose rather to venture upon his curse
than to give him glory by making confession; thus has whoredom taken
away the heart. `(6.)` The priest was to write this curse in a scrip or
scroll o parchment, verbatim-word for word, as he had expressed it, and
then to wipe or scrape out what he had written into the water (v. 23),
to signify that it was that curse which impregnated the water, and gave
it its strength to effect what was intended. It signified that, if she
were innocent, the curse should be blotted out and never appear against
her, as it is written, Isa. 43:25, I am he that blotteth out thy
transgression, and Ps. 51:9, Blot out my iniquities; but that, if she
were guilty, the curse, as it was written, being infused into the water,
would enter into her bowels with the water, even like oil into her bones
(Ps. 109:18), as we read of a curse entering into a house, Zec. 5:4.
`(7.)` The woman must then drink the water (v. 24); it is called the
bitter water, some think because they put wormwood in it to make it
bitter, or rather because it caused the curse. Thus sin is called an
evil thing and a bitter for the same reason, because it causeth the
curse, Jer. 2:19. If she had been guilty (and otherwise it did not cause
the curse), she was made to know that though her stolen waters had been
sweet, and her bread eaten in secret pleasant, yet the end was bitter as
wormwood, Prov. 9:17, and ch. 5:4. Let all that meddle with forbidden
pleasures know that they will be bitterness in the latter end. The Jews
say that if, upon denouncing the curse, the woman was so terrified that
she durst not drink the water, but confessed she was defiled, the priest
flung down the water, and cast her offering among the ashes, and she was
divorced without dowry: if she confessed not, and yet would not drink,
they forced her to it; and, if she was ready to throw it up again, they
hastened her away, that she might not pollute the holy place. `(8.)`
Before she drank the water, the jealousy-offering was waved and offered
upon the altar (v. 25, 26); a handful of it was burnt for a memorial,
and the remainder of it eaten by the priest, unless the husband was a
priest, and then it was scattered among the ashes. This offering in the
midst of the transaction signified that the whole was an appeal to God,
as a God that knows all things, and from whom no secret is hid. `(9.)` All
things being thus performed according to the law, they were to wait the
issue. The water, with a little dust put into it, and the scrapings of a
written parchment, had no natural tendency at all to do either good or
hurt; but if God was thus appealed to in the way of an instituted
ordinance, though otherwise the innocent might have continued under
suspicion and the guilty undiscovered, yet God would so far own his own
institution as that in a little time, by the miraculous operation of
Providence, the innocency of the innocent should be cleared, and the sin
of the guilty should find them out. `[1.]` If the suspected woman was
really guilty, the water she drank would be poison to her (v. 27), her
belly would swell and her thigh rot by a vile disease for vile deserts,
and she would mourn at the last when her flesh and body were consumed,
Prov. 5:11. Bishop Patrick says, from some of the Jewish writers, that
the effect of these waters appeared immediately, she grew pale, and her
eyes ready to start out of her head. Dr. Lightfoot says that sometimes
it appeared not for two or three years, but she bore no children, was
sickly, languished, and rotted at last; it is probable that some
indications appeared immediately. The rabbin say that the adulterer also
died in the same day and hour that the adulteress did, and in the same
manner too, that he belly swelled, and his secret parts rotted: a
disease perhaps not much unlike that which in these latter ages the
avenging hand of a righteous God has made the scourge of uncleanness,
and with which whores and whoremongers infect, and plague, and ruin one
another, since they escape punishment from men. The Jewish doctors add
that the waters had this effect upon the adulteress only in case the
husband had never offended in the same kind; but that, if he had at any
time defiled the marriage-bed, God did not thus right him against his
injurious wife; and that therefore in the latter and degenerate ages of
the Jewish church, when uncleanness did abound, this way of trial was
generally disused and laid aside; men, knowing their own crimes, were
content not to know their wives\' crimes. And to this perhaps may refer
the threatening (Hos. 4:14), I will not punish your spouses when they
commit adultery, for you yourselves are separated with whores. `[2.]` If
she were innocent, the water she drank would be physic to her: She shall
be free, and shall conceive seed, v. 28. The Jewish writers magnify the
good effects of this water to the innocent woman, that, to recompense
her for the wrong done to her by the suspicion, she should, after the
drinking of these waters, be stronger and look better than ever; if she
was sickly, she should become healthful, should bear a man-child, and
have easy labour.

`2.` From the whole we may learn, `(1.)` That secret sins are known to God,
and sometimes are strangely brought to light in this life; however,
there is a day coming when God will, by Jesus Christ, as here by the
priest, judge the secrets of men according to the gospel, Rom. 2:16.
`(2.)` That, in particular, Whoremongers and adulterers God will judge.
The violation of conjugal faith and chastity is highly provoking to the
God of heaven, and sooner or later it will be reckoned for. Though we
have not now the waters of jealousy to be a sensible terror to the
unclean, yet we have a word from God which ought to be as great a
terror, that if any man defile the temple of God, him shall God destroy,
1 Co. 3:17. `(3.)` That God will find out some way or other to clear the
innocency of the innocent, and to bring forth their righteousness as the
light. `(4.)` That to the pure all things are pure, but to the defiled
nothing is so, Tit. 1:15. The same word is to some a savour of life unto
life, to others a savour of death unto death, like those waters of
jealousy, according as they receive it; the same providence is for good
to some and for hurt to others, Jer. 24:5, 8, 9. And, whatsoever it is
intended for, it shall not return void.
