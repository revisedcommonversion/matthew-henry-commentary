Numbers, Chapter 2
==================

Commentary
----------

The thousands of Israel, having been mustered in the former chapter, in
this are marshalled, and a regular disposition is made of their camp, by
a divine appointment. Here is, `I.` A general order concerning it (v. 1,
2). `II.` Particular directions for the posting of each of the tribes, in
four distinct squadrons, three tribes in each squadron. 1. In the
van-guard on the east were posted Judah, Issachar, and Zebulun (v. 3-9).
2. In the right wing, southward, Reuben, Simeon, and Gad (v. 10-16). 3.
In the rear, westward, Ephraim, Manasseh, and Benjamin, (v. 18-24). 4.
In the left wing, northward, Dan, Asher, and Naphtali (v. 25-31). 5. The
tabernacle in the centre (v. 17). `III.` The conclusion of this
appointment (v. 32, etc.).

### Verses 1-2

Here is the general appointment given both for their orderly encampment
where they rested and their orderly march when they moved. Some order,
it is possible, they had observed hitherto; they came out of Egypt in
rank and file (Ex. 13:18), but now they were put into a better model. 1.
They all dwelt in tents, and when they marched carried all their tents
along with them, for they found no city to dwell in, Ps. 107:4. This
represents to us our state in this world. It is a movable state (we are
here to-day and gone to-morrow); and it is a military state: is not our
life a warfare? We do but pitch our tents in this world, and have in it
no continuing city. Let us, therefore, while we are pitching in this
world, be pressing through it. 2. Those of a tribe were to pitch
together, every man by his own standard. Note, It is the will of God
that mutual love and affection, converse and communion, should be kept
up among relations. Those that are of kin to each other should, as much
as they can, be acquainted with each other; and the bonds of nature
should be improved for the strengthening of the bonds of Christian
communion. 3. Every one must know his place and keep in it; they were
not allowed to fix where they pleased, nor to remove when they pleased,
but God quarters them, with a charge to abide in their quarters. Note,
It is God that appoints us the bounds of our habitation, and to him we
must refer ourselves. He shall choose our inheritance for us (Ps. 47:4),
and in his choice we must acquiesce, and not love to flit, nor be as the
bird that wanders from her nest. 4. Every tribe had its standard, flag,
or ensign, and it should seem every family had some particular ensign of
their father\'s house, which was carried as with us the colours of each
troop or company in a regiment are. These were of use for the
distinction of tribes and families, and the gathering and keeping of
them together, in allusion to which the preaching of the gospel is said
to lift up an ensign, to which the Gentiles shall seek, and by which
they shall pitch, Isa. 11:10, 12. Note, God is the God of order, and not
of confusion. These standards made this mighty army seem more beautiful
to its friends and more formidable to its enemies. The church of Christ
is said to be as terrible as an army with banners, Cant. 6:10. It is
uncertain how these standards were distinguished: some conjecture that
the standard of each tribe was of the same colour with the precious
stone in which the name of that tribe was written in the high priest\'s
ephod, and that this was all the difference. Many of the modern Jews
think there was some coat of arms painted in each standard, which had
reference to the blessing of that tribe by Jacob. Judah bore a lion, Dan
a serpent, Naphtali a hind, Benjamin a wolf, etc. Some of them say the
four principal standards were, Judah a lion, Reuben a man, Joseph an ox,
and Dan an eagle, making the appearances in Ezekiel\'s vision to allude
it. Others say the name of each tribe was written in its standard.
Whatever it was, no doubt it gave a certain direction. 5. They were to
pitch about the tabernacle, which was to be in the midst of them, as the
tent of pavilion of a general in the centre of an army. They must encamp
round the tabernacle, `(1.)` That it might be equally a comfort and joy to
them all, as it was a token of God\'s gracious presence with them. Ps.
46:5, God is in the midst of her, she shall not be moved. Their camp had
reason to be hearty, when thus they had God in the heart of them. To
have bread from heaven every day round about their camp, and fire from
heaven, with other tokens of God\'s favour, in the midst of their camp,
was abundantly sufficient to answer that question, Is the Lord among us,
or is he not? Happy art thou, O Israel! It is probable that the doors of
all their tents were made to look towards the tabernacle from all sides,
for every Israelite should have his eyes always towards the Lord;
therefore they worshipped at the tent-door. The tabernacle was in the
midst of the camp, that it might be near to them; for it is a very
desirable thing to have the solemn administrations of holy ordinances
near us and within our reach. The kingdom of God is among you. `(2.)` That
they might be a guard and defence upon the tabernacle and the Levites on
every side. No invader could come near God\'s tabernacle without first
penetrating the thickest of their squadrons. Note, If God undertake the
protection of our comforts, we ought in our places to undertake the
protection of his institutions, and stand up in defence of his honour,
and interest, and ministers. 6. Yet they were to pitch afar off, in
reverence to the sanctuary, that it might not seem crowded and thrust up
among them, and that the common business of the camp might be no
annoyance to it. They were also taught to keep their distance, lest too
much familiarity should breed contempt. It is supposed (from Joshua 3:4)
that the distance between the nearest part of the camp and the
tabernacle (or perhaps between them and the camp of the Levites, who
pitched near the tabernacle) was 2000 cubits, that is, 1000 yards,
little more than half a measured mile with us; but the outer parts of
the camp must needs be much further off. Some compute that the extent of
their camp could be no less than twelve miles square; for it was like a
movable city, with streets and lanes, in which perhaps the manna fell,
as well as on the outside of the camp, that they might have it at their
doors. In the Christian church we read of a throne (as in the tabernacle
there was a mercy-seat) which is called a glorious high throne from the
beginning (Jer. 17:12), and that throne surrounded by spiritual
Israelites, twenty-four elders, double to the number of the tribes,
clothed in white raiment (Rev. 4:4), and the banner over them is Love;
but we are not ordered, as they were, to pitch afar off; no, we are
invited to draw near, and come boldly. The saints of the Most High are
said to be round about him, Ps. 76:11. God by his grace keep us close to
him!

### Verses 3-34

We have here the particular distribution of the twelve tribes into four
squadrons, three tribes in a squadron, one of which was to lead the
other two. Observe, 1. God himself appointed them their place, to
prevent strife and envy among them. Had they been left to determine
precedency among themselves, they would have been in danger of
quarrelling with one another (as the disciples who strove which should
be greatest); each would have had a pretence to be first, or at least
not to be last. Had it been left to Moses to determine, they would have
quarrelled with him, and charged him with partiality; therefore God does
it, who is himself the fountain and judge of honour, and in his
appointment all must acquiesce. If God in his providence advance others
above us, and abase us, we ought to be as well satisfied in his doing it
in that way as if he did it, as this was done here, by a voice out of
the tabernacle; and this consideration, that it appears to be the will
of God it should be so, should effectually silence all envies and
discontents. And as far as our place comes to be our choice our Saviour
has given us a rule in Lu. 14:8, Sit not down in the highest room; and
another in Mt. 20:27, He that will be chief, let him be your servant.
Those that are most humble and most serviceable are really most
honourable. 2. Every tribe had a captain, a prince, or
commander-in-chief, whom God himself nominated, the same that had been
appointed to number them, ch. 1:5. Our being all the children of one
Adam is so far from justifying the levellers, and taking away the
distinction of place and honour, that even among the children of the
same Abraham, the same Jacob, the same Judah, God himself appointed that
one should be captain of all the rest. There are powers ordained of God,
and those to whom honour and fear are due and must be paid. Some observe
the significancy of the names of these princes, at least, in general,
how much God was in the thoughts of those that gave them their names,
for most of them have El, God, at one end or other of their names.
Nethaneel, the gift of God; Eliab, my God a Father; Elizur, my God a
rock; Shelumiel, God my peace; Eliasaph, God has added; Elishama, my God
has heard: Gamaliel, God my reward; Pagiel, God has met me. By this it
appears that the Israelites in Egypt did not quite forget the name of
their God, but, when they wanted other memorials, preserved the
remembrance of it in the names of their children, and therewith
comforted themselves in their affliction. 3. Those tribes were placed
together under the same standard that were nearest of kin to each other;
Judah, Issachar, and Zebulun, were the three younger sons of Leah, and
they were put together; and Issachar and Zebulun would not grudge to be
under Judah, since they were his younger brethren. Reuben and Simeon
would not have been content in their place. Therefore Reuben, Jacob\'s
eldest son, is made chief of the next squadron; Simeon, no doubt, is
willing to be under him, and Gad, the son of Zilpah, Leah\'s handmaid,
is fitly added to them in Levi\'s room: Ephraim, Manasseh, and Benjamin,
are all the posterity of Rachel. Dan, the eldest son of Bilhah, is made
a leading tribe, though the son of a concubine, that more abundant
honour might be bestowed on that which lacked; and it was said, Dan
should judge his people, and to him were added two younger sons of the
handmaids. Thus unexceptionable was the order in which they were placed.
4. The tribe of Judah was in the first post of honour, encamped towards
the rising sun, and in their marches led the van, not only because it
was the most numerous tribe, but chiefly because from that tribe Christ
was to come, who is the Lion of the tribe of Judah, and was to descend
from the loins of him who was now nominated chief captain of that tribe.
Nahshon is reckoned among the ancestors of Christ, Mt. 1:4. So that,
when he went before them, Christ himself went before them in effect, as
their leader. Judah was the first of the twelve sons of Jacob that was
blessed. Reuben, Simeon, and Levi, were censured by their dying father;
he therefore being first in blessing, though not in birth, is put first,
to teach children how to value the smiles of their godly parents and
dread their frowns. 5. The tribes of Levi pitched closely about the
tabernacle, within the rest of their tribes, v. 17. They must defend the
sanctuary, and then the rest of the tribes must defend them. Thus, in
the vision which John saw of the glory of heaven, between the elders and
the throne were four living creatures full of eyes, Rev. 4:6, 8. Civil
powers should protect the religious interests of a nation, and be a
defence upon that glory. 6. The camp of Dan (and so that tribe is called
long after their settlement in Canaan (Jdg. 13:25), because celebrated
for their military prowess), though posted in the left wing when they
encamped, was ordered in their march to bring up the rear, v. 31. They
were the most numerous, next to Judah, and therefore were ordered into a
post which, next to the front, required the most strength, for as the
strength is so shall the day be. Lastly, The children of Israel observed
the orders given them, and did as the Lord commanded Moses, v. 34. They
put themselves in the posts assigned them, without murmuring or
disputing, and, as it was their safety, so it was their beauty; Balaam
was charmed with the sight of it: How goodly are thy tents, O Jacob! ch.
24:5. Thus the gospel church, called the camp of saints, ought to be
compact according to the scripture model, every one knowing and keeping
his place, and then all that wish well to the church rejoice, beholding
their order, Col. 2:5.
