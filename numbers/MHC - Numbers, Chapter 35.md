Numbers, Chapter 35
===================

Commentary
----------

Orders having been given before for the dividing of the land of Canaan
among the lay-tribes (as I may call them), care is here taken for a
competent provision for the clergy, the tribe of Levi, which ministered
in holy things. `I.` Forty-eight cities were to be assigned them, with
their suburbs, some in every tribe (v. 1-8). `II.` Six cities out of these
were to be for cities of refuge, for any man that killed another
unawares (v. 9-15). In the law concerning these observe, 1. In what case
sanctuary was not allowed, namely, that of wilful murder (v. 16-21). 2.
In what cases it was allowed (v. 22-24). 3. What was the law concerning
those that took shelter in these cities of refuge (v. 25, etc.).

### Verses 1-8

The laws about the tithes and offerings had provided very plentifully
for the maintenance of the Levites, but it was not to be thought, nor
indeed was it for the public good, that when they came to Canaan they
should all live about the tabernacle, as they had done in the
wilderness, and therefore care must be taken to provide habitations for
them, in which they might live comfortably and usefully. It is this
which is here taken care of.

`I.` Cities were allotted them, with their suburbs, v. 2. They were not to
have any ground for tillage; they needed not to sow, nor reap, nor
gather into barns, for their heavenly Father fed them with the tithe of
the increase of other people\'s labours, that they might the more
closely attend to the study of the law, and might have more leisure to
teach the people; for they were not fed thus easily that they might live
in idleness, but that they might give themselves wholly to the business
of their profession, and not be entangled in the affairs of this life.
`1.` Cities were allotted them, that they might live near together, and
converse with one another about the law, to their mutual edification;
and that in doubtful cases they might consult one another, and in all
cases strengthen one another\'s hands. 2. These cities had suburbs
annexed to them for their cattle (v. 3), a thousand cubits from the wall
was allowed them for out-houses to keep their cattle in, and then two
thousand more for fields to graze their cattle in, v. 4, 5. Thus was
care taken that they should not only live, but live plentifully, and
have all desirable conveniences about them, that they might not be
looked upon with contempt by their neighbours.

`II.` These cities were to be assigned them out of the possessions of
each tribe, v. 8. 1. That each tribe might thus make a grateful
acknowledgment to God out of their real as well as out of their personal
estates (for what was given to the Levites was accepted as given to the
Lord) and thus their possessions were sanctified to them. 2. That each
tribe might have the benefit of the Levites\' dwelling among them, to
teach them the good knowledge of the Lord; thus that light was diffused
through all parts of the country, and none were left to sit in darkness,
Deu. 33:10, They shall teach Jacob thy judgments. Jacob\'s curse on
Levi\'s anger was, I will scatter them in Israel, Gen. 49:7. But that
curse was turned into a blessing, and the Levites, by being thus
scattered, were put into a capacity of doing so much the more good. It
is a great mercy to a country to be replenished in all parts with
faithful ministers.

`III.` The number allotted them was forty-eight in all, four out of each
of the twelve tribes, one with another. Out of the united tribes of
Simeon and Judah nine, out of Naphtali three, and four apiece out of the
rest, as appears, Jos. 21. Thus were they blessed with a good ministry,
and that ministry with a comfortable maintenance, not only in tithes,
but in glebe-lands. And, though the gospel is not so particular as the
law was in this matter, yet it expressly provides that he that is taught
in the word should communicate unto him that teaches in all good things,
Gal. 6:6.

### Verses 9-34

We have here the orders given concerning the cities of refuge, fitly
annexed to what goes before, because they were all Levites\' cities. In
this part of the constitution there is a great deal both of good law and
pure gospel.

`I.` Here is a great deal of good law, in the case of murder and
manslaughter, a case of which the laws of all nations have taken
particular cognizance. It is here enacted and provided, consonant to
natural equity,

`1.` That wilful murder should be punished with death, and in that case
no sanctuary should be allowed, no ransom taken, nor any commutation of
the punishment accepted: The murderer shall surely be put to death, v.
16. It is supposed to be done of hatred (v. 20), or in enmity (v. 21),
upon a sudden provocation (for our Saviour makes rash anger, as well as
malice prepense, to be murder, Mt. 5:21, 22), whether the person be
murdered with an instrument of iron (v. 16) or wood (v. 18), or with a
stone thrown at him (v. 17, 20); nay, if he smite him with his hand in
enmity, and death ensue, it is murder (v. 21); and it was an ancient
law, consonant to the law of nature, that whoso sheds man\'s blood, by
man shall his blood be shed, Gen. 9:6. Where wrong has been done
restitution must be made; and, since the murderer cannot restore the
life he has wrongfully taken away, his own must be exacted from him in
lieu of it, not (as some have fancied) to satisfy the manes or ghost of
the person slain, but to satisfy the law and the justice of a nation;
and to be a warning to all others not to do likewise. It is here said,
and it is well worthy the consideration of all princes and states, that
blood defiles not only the conscience of the murderer, who is thereby
proved not to have eternal life abiding in him (1 Jn. 3:15), but also
the land in which it is shed; so very offensive is it to God and all
good men, and the worst of nuisances. And it is added that the land
cannot be cleansed from the blood of the murdered, but by the blood of
the murderer, v. 33. If murderers escape punishment from men, those that
suffer them to escape will have a great deal to answer for, and God will
nevertheless not suffer them to escape his righteous judgments. Upon the
same principle it is provided that no satisfaction should be taken for
the life of a murderer (v. 31): If a man would give all the substance of
his house to the judges, to the country, or to the avenger of blood, to
atone for his crime, it must utterly be contemned. The redemption of the
life is so precious that it cannot be obtained by the multitude of
riches (Ps. 49:6-8), which perhaps may allude to this law. A rule of law
comes in here (which is a rule of our law in cases of treason only) that
no man shall be put to death upon the testimony of one witness, but it
was necessary there should be two (v. 30); this law is settled in all
capital cases, Deu. 17:6; 19:15. And, lastly, not only the prosecution,
but the execution, of the murderer, is committed to the next of kin,
who, as he was to be the redeemer of his kinsman\'s estate if it were
mortgaged, so he was to be the avenger of his blood if he were murdered
(v. 19): The avenger of blood himself shall slay the murderer, if he be
convicted by the notorious evidence of the fact, and he needed not to
have recourse by a judicial process to the court of judgment. But if it
were uncertain who the murderer was, and the proof doubtful, we cannot
think that his bare suspicion, or surmise, would empower him to do that
which the judges themselves could not do but upon the testimony of two
witnesses. Only if the fact were plain then the next heir of the person
slain might himself, in a just indignation, slay the murderer wherever
he met him. Some think this must be understood to be after the lawful
judgment of the magistrate, and so the Chaldee says, \"He shall slay
him, when he shall be condemned unto him by judgment;\" but it should
seem, by v. 24, that the judges interposed only in a doubtful case, and
that if the person on whom he took vengeance was indeed the murderer,
and a wilful murderer, the avenger was innocent (v. 27), only, if it
proved otherwise, it was at his peril. Our law allows an appeal to be
brought against a murderer by the widow, or next heir, of the person
murdered, yea, though the murderer have been acquitted upon an
indictment; and, if the murderer be found guilty upon that appeal,
execution shall be awarded at the suit of the appellant, who may
properly be called the avenger of blood.

`2.` But if the homicide was not voluntary, nor done designedly, if it
was without enmity, or lying in wait (v. 22), not seeing the person or
not seeking his harm (v. 23), which our law calls chance-medley, or
homicide per infortunium-through misfortune, in this case there were
cities of refuge appointed for the manslayer to flee to. By our law this
incurs a forfeiture of goods, but a pardon is granted of course upon the
special matter found. Concerning the cities of refuge the law was, `(1.)`
That, if a man killed another, in these cities he was safe, and under
the protection of the law, till he had his trial before the
congregation, that is, before the judges in open court. If he neglected
thus to surrender himself, it was at his peril; if the avenger of blood
met him elsewhere, or overtook him loitering in his way to the city of
refuge, and slew him, his blood was upon his own head, because he did
not make use of the security which God had provided for him. `(2.)` If,
upon trial, it were found to be willful murder, the city of refuge
should no longer be a protection to him; it was already determined: Thou
shalt take him from my altar, that he may die, Ex. 21:14. `(3.)` But if it
were found to be by error or accident, and that the stroke was given
without any design upon the life of the person slain or any other, then
the man-slayer should continue safe in the city of refuge, and the
avenger of blood might not meddle with him, v. 25. There he was to
remain in banishment from his own house and patrimony till the death of
the high priest; and, if at any time he went out of that city or the
suburbs of it, he put himself out of the protection of the law, and the
avenger of blood, if he met him, might slay him, v. 26-28. Now, `[1.]`
By the preservation of the life of the man-slayer God would teach us
that men ought not to suffer for that which is rather their unhappiness
than their crime, rather the act of Providence than their own act, for
God delivered him into his hand, Ex. 21:13. `[2.]` By the banishment of
the man-slayer from his own city, and his confinement to the city of
refuge, where he was in a manner a prisoner, God would teach us to
conceive a dread and horror of the guilt of blood, and to be very
careful of life, and always afraid lest by oversight or negligence we
occasion the death of any. `[3.]` By the limiting of the time of the
offender\'s banishment to the death of the high priest, an honour was
put upon that sacred office. The high priest was to be looked upon as so
great a blessing to his country that when he died their sorrow upon that
occasion should swallow up all other resentments. The cities of refuge
being all of them Levites\' cities, and the high priest being the head
of that tribe, and consequently having a peculiar dominion over these
cites, those that were confined to them might properly be looked upon as
his prisoners, and so his death must be their discharge; it was, as it
were, at his suit that the delinquent was imprisoned, and therefore at
his death it fell. Actio moritur cum persona-The suit expires with the
party. Anisworth has another notion of it, That as the high priests,
while they lived, by their service and sacrificing made atonement for
sin, wherein they prefigured Christ\'s satisfaction, so, at their death,
those were released that had been exiled for casual murder, which
typified redemption in Israel. `[4.]` By the abandoning of the prisoner
to the avenger of blood, in case he at any time went out of the limits
of the city of refuge, they were taught to adhere to the methods which
Infinite Wisdom prescribed for their security. It was for the honour of
a remedial law that it should be so strictly observed. How can we expect
to be saved if we neglect the salvation, which is indeed a great
salvation!

`II.` Here is a great deal of good gospel couched under the type and
figure of the cities of refuge; and to them the apostle seems to allude
when he speaks of our fleeing for refuge to the hope set before is (Heb.
6:18), and being found in Christ, Phil. 3:9. We never read in the
history of the Old Testament of any use made of these cities of refuge,
any more than of other such institutions, which yet, no doubt, were made
use of upon the occasions intended; only we read of those that, in
dangerous cases, took hold of the horns of the altar (1 Ki. 1:50; 2:28);
for the altar, wherever that stood, was, as it were the capital city of
refuge. But the law concerning these cities was designed both to raise
and to encourage the expectations of those who looked for redemption in
Israel, which should be to those who were convinced of sin, and in
terror by reason of it, as the cities of refuge were to the man-slayer.
Observe, 1. There were several cities of refuge, and they were so
appointed in several parts of the country that the man-slayer, wherever
he dwelt in the land of Israel, might in half a day reach one or other
of them; so, though there is but one Christ appointed for our refuge,
yet, wherever we are, he is a refuge at hand, a very present help, for
the word is nigh us and Christ in the word. 2. The man-slayer was safe
in any of these cities; so in Christ believers that flee to him, and
rest in him, are protected from the wrath of God and the curse of the
law. There is no condemnation to those that are in Christ Jesus, Rom.
8:1. Who shall condemn those that are thus sheltered? 3. They were all
Levites\' cities; it was a kindness to the poor prisoner that though he
might not go up to the place where the ark was, yet he was in the midst
of Levites, who would teach him the good knowledge of the Lord, and
instruct him how to improve the providence he was now under. It might
also be expected that the Levites would comfort and encourage him, and
bid him welcome; so it is the work of gospel ministers to bid poor
sinners welcome to Christ, and to assist and counsel those that through
grace are in him. 4. Even strangers and sojourners, though they were not
native Israelites, might take the benefit of these cities of refuge, v.
15. So in Christ Jesus no difference in made between Greek and Jew; even
the sons of the stranger that by faith flee to Christ shall be safe in
him. 5. Even the suburbs or borders of the city were a sufficient
security to the offender, v. 26, 27. So there is virtue even in the hem
of Christ\'s garment for the healing and saving of poor sinners. If we
cannot reach to a full assurance, we may comfort ourselves in a good
hope through grace. 6. The protection which the man-slayer found in the
city of refuge was not owing to the strength of its walls, or gates, or
bars, but purely to the divine appointment; so it is the word of the
gospel that gives souls safety in Christ, for him hath God the Father
sealed. 7. If the offender was ever caught struggling out of the borders
of his city of refuge, or stealing home to his house again, he lost the
benefit of his protection, and lay exposed to the avenger of blood; so
those that are in Christ must abide in Christ, for it is at their peril
if they forsake him and wander from him. Drawing back is to perdition.
