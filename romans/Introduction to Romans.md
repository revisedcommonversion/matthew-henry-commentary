Introduction to Romans
======================

If we may compare scripture with scripture, and take the opinion of some
devout and pious persons, in the Old Testament David\'s Psalms, and in
the New Testament Paul\'s Epistles, are stars of the first magnitude,
that differ from the other stars in glory. The whole scripture is indeed
an epistle from heaven to earth: but in it we have upon record several
particular epistles, more of Paul\'s than of any other, for he was the
chief of the apostles, and laboured more abundantly than they all. His
natural parts, I doubt not, were very pregnant; his apprehension was
quick and piercing; his expressions were fluent and copious; his
affections, wherever he took, very warm and zealous, and his resolutions
no less bold and daring: this made him, before his conversion, a very
keen and bitter persecutor; but when the strong man armed was
dispossessed, and the stronger than he came to divide the spoil and to
sanctify these qualifications, he became the most skilful zealous
preacher; never any better fitted to win souls, nor more successful.
Fourteen of his epistles we have in the canon of scripture; many more,
it is probable, he wrote in the course of his ministry, which might be
profitable enough for doctrine, for reproof, etc., but, not being given
by inspiration of God, they were not received as canonical scripture,
nor handed down to us. Six epistles, said to be Paul\'s, written to
Seneca, and eight of Seneca\'s to him, are spoken of by some of the
ancients \[Sixt. Senens. Biblioth. Sanct. lib. 2\] and are extant; but,
upon the first view, they appear spurious and counterfeit.

This epistle to the Romans is placed first, not because of the priority
of its date, but because of the superlative excellency of the epistle,
it being one of the longest and fullest of all, and perhaps because of
the dignity of the place to which it is written. Chrysostom would have
this epistle read over to him twice a week. It is gathered from some
passages in the epistle that it was written Anno Christi 56, from
Corinth, while Paul made a short stay there in his way to Troas, Acts
20:5, 6. He commendeth to the Romans Phebe, a servant of the church at
Cenchrea (ch. 16), which was a place belonging to Corinth. He calls
Gaius his host, or the man with whom he lodged (ch. 16:23), and he was a
Corinthian, not the same with Gaius of Derbe, mentioned Acts 20. Paul
was now going up to Jerusalem, with the money that was given to the poor
saints there; and of that he speaks, ch. 15:26. The great mysteries
treated of in this epistle must needs produce in this, as in other
writings of Paul, many things dark and hard to be understood, 2 Peter
3:16. The method of this (as of several other of the epistles) is
observable; the former part of it doctrinal, in the first eleven
chapters; the latter part practical, in the last five: to inform the
judgment and to reform the life. And the best way to understand the
truths explained in the former part is to abide and abound in the
practice of the duties prescribed in the latter part; for, if any man
will do his will, he shall know of the doctrine, Jn. 7:17.

`I.` The doctrinal part of the epistles instructs us,

`1.` Concerning the way of salvation `(1.)` The foundation of it laid in
justification, and that not by the Gentiles\' works of nature (ch. 1),
nor by the Jews\' works of the law (ch. 2, 3), for both Jews and
Gentiles were liable to the curse; but only by faith in Jesus Christ,
ch. 3:21, etc.; ch. 4. `(2.)` The steps of this salvation are, `[1.]`
Peace with God, ch. 5. `[2.]` Sanctification, ch. 6, 7. `[3.]`
Glorification, ch. 8.

`2.` Concerning the persons saved, such as belong to the election of
grace (ch. 9), Gentiles and Jews, ch. 10, 11. By this is appears that
the subject he discourses of were such as were then the present truths,
as the apostle speaks, 2 Peter 1:12. Two things the Jews then stumbled
at-justification by faith without the works of the law, and the
admission of the Gentiles into the church; and therefore both these he
studied to clear and vindicate.

`II.` The practical part follows, wherein we find, 1. Several general
exhortations proper for all Christians, ch. 12. 2. Directions for our
behaviour, as members of civil society, ch. 13. 3. Rules for the conduct
of Christians to one another, as members of the Christian church, ch. 14
and ch. 15:1-14.

`III.` As he draws towards a conclusion, he makes an apology for writing
to them (ch. 15:14-16), gives them an account of himself and his own
affairs (v. 17-21), promises them a visit (v. 22-29), begs their prayers
(v. 30-32), sends particular salutations to many friends there (ch.
16:1-16), warns them against those who caused divisions (v. 17-20), adds
the salutations of his friends with him (v. 21-23), and ends with a
benediction to them and a doxology to God (v. 24-27).
