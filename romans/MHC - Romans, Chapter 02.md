Romans, Chapter 2
=================

Commentary
----------

The scope of the first two chapters of this epistle may be gathered from
ch. 3:9, \"We have before proved both Jews and Gentiles that they are
all under sin.\" This we have proved upon the Gentiles (ch. 1), now in
this chapter he proves it upon the Jews, as appears by v. 17, \"thou art
called a Jew.\" `I.` He proves in general that Jews and Gentiles stand
upon the same level before the justice of God, to v. 11. `II.` He shows
more particularly what sins the Jews were guilty of, notwithstanding
their profession and vain pretensions (v. 17 to the end).

### Verses 1-16

In the former chapter the apostle had represented the state of the
Gentile world to be as bad and black as the Jews were ready enough to
pronounce it. And now, designing to show that the state of the Jews was
very bad too, and their sin in many respects more aggravated, to prepare
his way he sets himself in this part of the chapter to show that God
would proceed upon equal terms of justice with Jews and Gentiles; and
now with such a partial hand as the Jews were apt to think he would use
in their favour.

`I.` He arraigns them for their censoriousness and self-conceit (v. 1):
Thou art inexcusable, O man, whosoever thou art that judgest. As he
expresses himself in general terms, the admonition may reach those many
masters (Jam. 3:1), of whatever nation or profession they are, that
assume to themselves a power to censure, control, and condemn others.
But he intends especially the Jews, and to them particularly he applies
this general charge (v. 21), Thou who teachest another teachest thou not
thyself? The Jews were generally a proud sort of people, that looked
with a great deal of scorn and contempt upon the poor Gentiles, as not
worthy to be set with the dogs of their flock; while in the mean time
they were themselves as bad and immoral-though not idolaters, as the
Gentiles, yet sacrilegious, v. 22. Therefore thou art inexcusable. If
the Gentiles, who had but the light of nature, were inexcusable (ch.
1:20), much more the Jews, who had the light of the law, the revealed
will of God, and so had greater helps than the Gentiles.

`II.` He asserts the invariable justice of the divine government, v. 2,
3. To drive home the conviction, he here shows what a righteous God that
is with whom we have to do, and how just in his proceedings. It is usual
with the apostle Paul, in his writings, upon mention of some material
point, to make large digressions upon it; as here concerning the justice
of God (v. 2), That the judgment of God is according to truth,-according
to the eternal rules of justice and equity,-according to the heart, and
not according to the outward appearance (1 Sa. 16:7),-according to the
works, and not with respect to persons, is a doctrine which we are all
sure of, for he would not be God if he were not just; but it behoves
those especially to consider it who condemn others for those things
which they themselves are guilty of, and so, while they practise sin and
persist in that practice, think to bribe the divine justice by
protesting against sin and exclaiming loudly upon others that are
guilty, as if preaching against sin would atone for the guilt of it. But
observe how he puts it to the sinner\'s conscience (v. 3): Thinkest thou
this, O man? O man, a rational creature, a dependent creature, made by
God, subject under him, and accountable to him. The case is so plain
that we may venture to appeal to the sinner\'s own thoughts: \"Canst
thou think that thou shalt escape the judgment of God? Can the
heart-searching God be imposed upon by formal pretences, the righteous
Judge of all so bribed and put off?\" The most plausible politic
sinners, who acquit themselves before men with the greatest confidence,
cannot escape the judgment of God, cannot avoid being judged and
condemned.

`III.` He draws up a charge against them (v. 4, 5) consisting of two
branches:-

`1.` Slighting the goodness of God (v. 4), the riches of his goodness.
This is especially applicable to the Jews, who had singular tokens of
the divine favour. Means are mercies, and the more light we sin against
the more love we sin against. Low and mean thoughts of the divine
goodness are at the bottom of a great deal of sin. There is in every
wilful sin an interpretative contempt of the goodness of God; it is
spurning at his bowels, particularly the goodness of his patience, his
forbearance and long-suffering, taking occasion thence to be so much the
more bold in sin, Eccl. 8:11. Not knowing, that is, not considering, not
knowing practically and with application, that the goodness of God
leadeth thee, the design of it is to lead thee, to repentance. It is not
enough for us to know that God\'s goodness leads to repentance, but we
must know that it leads us-thee in particular. See here what method God
takes to bring sinners to repentance. He leads them, not drives them
like beasts, but leads them like rational creatures, allures them (Hos.
2:14); and it is goodness that leads, bands of love, Hos. 11:4. Compare
Jer. 31:3. The consideration of the goodness of God, his common goodness
to all (the goodness of his providence, of his patience, and of his
offers), should be effectual to bring us all to repentance; and the
reason why so many continue in impenitency is because they do not know
and consider this.

`2.` Provoking the wrath of God, v. 5. The rise of this provocation is a
hard and impenitent heart; and the ruin of sinners is their walking
after such a heart, being led by it. To sin is to walk in the way of the
heart; and when that is a hard and impenitent heart (contracted hardness
by long custom, besides that which is natural), how desperate must the
course needs be! The provocation is expressed by treasuring up wrath.
Those that go on in a course of sin are treasuring up unto themselves
wrath. A treasure denotes abundance. It is a treasure that will be
spending to eternity, and yet never exhausted; and yet sinners are still
adding to it as to a treasure. Every wilful sin adds to the score, and
will inflame the reckoning; it brings a branch to their wrath, as some
read that (Eze. 8:17), they put the branch to their nose. A treasure
denotes secrecy. The treasury or magazine of wrath is the heart of God
himself, in which it lies hid, as treasures in some secret place sealed
up; see Deu. 32:34; Job 14:17. But withal it denotes reservation to some
further occasion; as the treasures of the hail are reserved against the
day of battle and war, Job 38:22, 23. These treasures will be broken
open like the fountains of the great deep, Gen. 7:11. They are treasured
up against the day of wrath, when they will be dispensed by the
wholesale, poured out by full vials. Though the present day be a day of
patience and forbearance towards sinners, yet there is a day of wrath
coming-wrath, and nothing but wrath. Indeed, every day is to sinners a
day of wrath, for God is angry with the wicked every day (Ps. 7:11), but
there is the great day of wrath coming, Rev. 6:17. And that day of wrath
will be the day of the revelation of the righteous judgment of God. The
wrath of God is not like our wrath, a heat and passion; no, fury is not
in him (Isa. 27:4): but it is a righteous judgment, his will to punish
sin, because he hates it as contrary to his nature. This righteous
judgment of God is now many times concealed in the prosperity and
success of sinners, but shortly it will be manifested before all the
world, these seeming disorders set to rights, and the heavens shall
declare his righteousness, Ps. 50:6. Therefore judge nothing before the
time.

`IV.` He describes the measures by which God proceeds in his judgment.
Having mentioned the righteous judgment of God in v. 5, he here
illustrates that judgment, and the righteousness of it, and shows what
we may expect from God, and by what rule he will judge the world. The
equity of distributive justice is the dispensing of frowns and favours
with respect to deserts and without respect to persons: such is the
righteous judgment of God.

`1.` He will render to every man according to his deeds (v. 6), a truth
often mentioned in scripture, to prove that the Judge of all the earth
does right.

`(1.)` In dispensing his favours; and this is mentioned twice here, both
in v. 7 and v. 10. For he delights to show mercy. Observe,

`[1.]` The objects of his favour: Those who by patient continuance, etc.
By this we may try our interest in the divine favour, and may hence be
directed what course to take, that we may obtain it. Those whom the
righteous God will reward are, First, Such as fix to themselves the
right end, that seek for glory, and honour, and immortality; that is,
the glory and honour which are immortal-acceptance with God here and for
ever. There is a holy ambition which is at the bottom of all practical
religion. This is seeking the kingdom of God, looking in our desires and
aims as high as heaven, and resolved to take up with nothing short of
it. This seeking implies a loss, sense of that loss, desire to retrieve
it, and pursuits and endeavours consonant to those desires. Secondly,
Such as, having fixed the right end, adhere to the right way: A patient
continuance in well-doing. 1. There must be well-doing, working good, v.
10. It is not enough to know well, and speak well, and profess well, and
promise well, but we must do well: do that which is good, not only for
the matter of it, but for the manner of it. We must do it well. 2. A
continuance in well-doing. Not for a fit and a start, like the morning
cloud and the early dew; but we must endure to the end: it is
perseverance that wins the crown. 3. A patient continuance. This
patience respects not only the length of the work, but the difficulties
of it and the oppositions and hardships we may meet with in it. Those
that will do well and continue in it must put on a great deal of
patience.

`[2.]` The product of his favour. He will render to such eternal life.
Heaven is life, eternal life, and it is the reward of those that
patiently continue in well-doing; and it is called (v. 10) glory,
honour, and peace. Those that seek for glory and honour (v. 7) shall
have them. Those that seek for the vain glory and honour of this world
often miss of them, and are disappointed; but those that seek for
immortal glory and honour shall have them, and not only glory and
honour, but peace. Worldly glory and honour are commonly attended with
trouble; but heavenly glory and honour have peace with them, undisturbed
everlasting peace.

`(2.)` In dispensing his frowns (v. 8, 9). Observe, `[1.]` The objects of
his frowns. In general those that do evil, more particularly described
to be such as are contentious and do not obey the truth. Contentious
against God. every wilful sin is a quarrel with God, it is striving with
our Maker (Isa. 45:9), the most desperate contention. The Spirit of God
strives with sinners (Gen. 6:3), and impenitent sinners strive against
the Spirit, rebel against the light (Job 24:13), hold fast deceit,
strive to retain that sin which the Spirit strives to part them from.
Contentious, and do not obey the truth. The truths of religion are not
only to be known, but to be obeyed; they are directing, ruling,
commanding; truths relating to practice. Disobedience to the truth is
interpreted a striving against it. But obey unrighteousness-do what
unrighteousness bids them do. Those that refuse to be the servants of
truth will soon be the slaves of unrighteousness. `[2.]` The products or
instances of these frowns: Indignation and wrath, tribulation and
anguish. These are the wages of sin. Indignation and wrath the
causes-tribulation and anguish the necessary and unavoidable effects.
And this upon the soul; souls are the vessels of that wrath, the
subjects of that tribulation and anguish. Sin qualifies the soul for
this wrath. The soul is that in or of man which is alone immediately
capable of this indignation, and the impressions or effects of anguish
therefrom. Hell is eternal tribulation and anguish, the product of wrath
and indignation. This comes of contending with God, of setting briers
and thorns before a consuming fire, Isa. 27:4. Those that will not bow
to his golden sceptre will certainly be broken by his iron rod. Thus
will God render to every man according to his deeds.

`2.` There is no respect of persons with God, v. 11. As to the spiritual
state, there is a respect of persons; but not as to outward relation or
condition. Jews and Gentiles stand upon the same level before God. This
was Peter\'s remark upon the first taking down of the partition-wall
(Acts 10:34), that God is no respecter of persons; and it is explained
in the next words, that in every nation he that fears God, and works
righteousness, is accepted of him. God does not save men with respect to
their external privileges or their barren knowledge and profession of
the truth, but according as their state and disposition really are. In
dispensing both his frowns and favours it is both to Jew and Gentile. If
to the Jews first, who had greater privileges, and made a greater
profession, yet also to the Gentiles, whose want of such privileges will
neither excuse them from the punishment of their ill-doing nor bar them
out from the reward of their well-doing (see Col. 3:11); for shall not
the Judge of all the earth do right?

`V.` He proves the equity of his proceedings with all, when he shall
actually come to Judge them (v. 12-16), upon this principle, that that
which is the rule of man\'s obedience is the rule of God\'s judgment.
Three degrees of light are revealed to the children of men:-

`1.` The light of nature. This the Gentiles have, and by this they shall
be judged: As many as have sinned without law shall perish without law;
that is, the unbelieving Gentiles, who had no other guide but natural
conscience, no other motive but common mercies, and had not the law of
Moses nor any supernatural revelation, shall not be reckoned with for
the transgression of the law they never had, nor come under the
aggravation of the Jews\' sin against and judgment by the written law;
but they shall be judged by, as they sin against, the law of nature, not
only as it is in their hearts, corrupted, defaced, and imprisoned in
unrighteousness, but as in the uncorrupt original the Judge keeps by
him. Further to clear this (v. 14, 15), in a parenthesis, he evinces
that the light of nature was to the Gentiles instead of a written law.
He had said (v. 12) they had sinned without law, which looks like a
contradiction; for where there is no law there is no transgression. But,
says he, though they had not the written law (Ps. 147:20), they had that
which was equivalent, not to the ceremonial, but to the moral law. They
had the work of the law. He does not mean that work which the law
commands, as if they could produce a perfect obedience; but that work
which the law does. The work of the law is to direct us what to do, and
to examine us what we have done. Now, `(1.)` They had that which directed
them what to do by the light of nature: by the force and tendency of
their natural notions and dictates they apprehended a clear and vast
difference between good and evil. They did by nature the things
contained in the law. They had a sense of justice and equity, honour and
purity, love and charity; the light of nature taught obedience to
parents, pity to the miserable, conservation of public peace and order,
forbade murder, stealing, lying, perjury, etc. Thus they were a law unto
themselves. `(2.)` They had that which examined them as to what they had
done: Their conscience also bearing witness. They had that within them
which approved and commended what was well done and which reproached
them for what was done amiss. Conscience is a witness, and first or last
will bear witness, though for a time it may be bribed or brow-beaten. It
is instead of a thousand witnesses, testifying of that which is most
secret; and their thoughts accusing or excusing, passing a judgment upon
the testimony of conscience by applying the law to the fact. Conscience
is that candle of the Lord which was not quite put out, no, not in the
Gentile world. The heathen have witnessed to the comfort of a good
conscience.

-Hic murus ahoncus esto,

Nil conscire sib-parBe this thy brazen bulwark of defence,

Still to preserve thy conscious innocence.-Hos.

and to the terror of a bad one:

-Quos diri consein facti

Mens habet attonitos, et surdo verbere cuodi-parNo lash is heard, and
yet the guilty heart

Is tortur\'d with a self-inflicted smar-uv. Sat. 13.

Their thoughts the meanwhile, metaxy alleµloµn-among themselves, or one
with another. The same light and law of nature that witnesses against
sin in them, and witnessed against it in others, accused or excused one
another. Vicissim, so some read it, by turns; according as they observed
or broke these natural laws and dictates, their consciences did either
acquit or condemn them. All this did evince that they had that which was
to them instead of a law, which they might have been governed by, and
which will condemn them, because they were not so guided and governed by
it. So that the guilty Gentiles are left without excuse. God is
justified in condemning them. They cannot plead ignorance, and therefore
are likely to perish if they have not something else to plead.

`2.` The light of the law. This the Jews had, and by this they shall be
judged (v. 12): As many as have sinned in the law shall be judged by the
law. They sinned, not only having the law, but en nomoµ-in the law, in
the midst of so much law, in the face and light of so pure and clear a
law, the directions of which were so very full and particular, and the
sanctions of it so very cogent and enforcing. These shall be judged by
the law; their punishment shall be, as their sin is, so much the greater
for their having the law. The Jew first, v. 9. It shall be more
tolerable for Tyre and Sidon. Thus Moses did accuse them (Jn. 5:45), and
they fell under the many stripes of him that knew his master\'s will,
and did it not, Lu. 12:47. The Jews prided themselves very much in the
law; but, to confirm what he had said, the apostle shows (v. 13) that
their having, and hearing, and knowing the law, would not justify them,
but their doing it. The Jewish doctors bolstered up their followers with
an opinion that all that were Jews, how bad soever they lived, should
have a place in the world to come. This the apostle here opposes: it was
a great privilege that they had the law, but not a saving privilege,
unless they lived up to the law they had, which it is certain the Jews
did not, and therefore they had need of a righteousness wherein to
appear before God. We may apply it to the gospel: it is not hearing, but
doing that will save us, Jn. 13:17; James 1:22.

`3.` The light of the gospel: and according to this those that enjoyed
the gospel shall be judge (v. 16): According to my gospel; not meant of
any fifth gospel written by Paul, as some conceit; or of the gospel
written by Luke, as Paul\'s amanuensis (Euseb. Hist. lib 3, cap. 8), but
the gospel in general, called Paul\'s because he was a preacher of it.
As many as are under that dispensation shall be judged according to that
dispensation, Mk. 16:16. Some refer those words, according to my gospel,
to what he says of the day of judgment: \"There will come a day of
judgment, according as I have in my preaching often told you; and that
will be the day of the final judgment both of Jews and Gentiles.\" It is
good for us to get acquainted with what is revealed concerning that day.
`(1.)` There is a day set for a general judgment. The day, the great day,
his day that is coming, Ps. 37:13. `(2.)` The judgment of that day will be
put into the hands of Jesus Christ. God shall judge by Jesus Christ,
Acts 17:31. It will be part of the reward of his humiliation. Nothing
speaks more terror to sinners, or more comfort to saints, than this,
that Christ shall be the Judge. `(3.)` The secrets of men shall then be
judged. Secret services shall be then rewarded, secret sins shall be
then punished, hidden things shall be brought to light. That will be the
great discovering day, when that which is now done in corners shall be
proclaimed to all the world.

### Verses 17-29

In the latter part of the chapter the apostle directs his discourse more
closely to the Jews, and shows what sins they were guilty of,
notwithstanding their profession and vain pretensions. He had said (v.
13) that not the hearers but the doers of the law are justified; and he
here applies that great truth to the Jews. Observe,

`I.` He allows their profession (v. 17-20) and specifies their particular
pretensions and privileges in which they prided themselves, that they
might see he did not condemn them out of ignorance of what they had to
say for themselves; no, he knew the best of their cause.

`1.` They were a peculiar people, separated and distinguished from all
others by their having the written law and the special presence of God
among them. `(1.)` Thou art called a Jew; not so much in parentage as
profession. It was a very honourable title. Salvation was of the Jews;
and this they were very proud of, to be a people by themselves; and yet
many that were so called were the vilest of men. It is no new thing for
the worst practices to be shrouded under the best names, for many of the
synagogue of Satan to say they are Jews (Rev. 2:9), for a generation of
vipers to boast they have Abraham to their father, Mt. 3:7-9. `(2.)` And
restest in the law; that is, they took a pride in this, that they had
the law among them, had it in their books, read it in their synagogues.
They were mightily puffed up with this privilege, and thought this
enough to bring them to heaven, though they did not live, up to the law.
To rest in the law, with a rest of complacency and acquiescence, is
good; but to rest in it with a rest of pride, and slothfulness, and
carnal security, is the ruin of souls. The temple of the Lord, Jer. 7:4.
Bethel their confidence, Jer. 48:13. Haughty because of the holy
mountain, Zep. 3:11. It is a dangerous thing to rest in external
privileges, and not to improve them. `(3.)` And makest thy boast of God.
See how the best things may be perverted and abused. A believing,
humble, thankful glorying in God, is the root and summary of all
religion, Ps. 34:2; Isa. 45:15; 1 Co. 1:31. But a proud vainglorious
boasting in God, and in the outward profession of his name, is the root
and summary of all hypocrisy. Spiritual pride is of all kinds of pride
the most dangerous.

`2.` They were a knowing people (v. 18): and knowest his will, to
theleµma-the will. God\'s will is the will, the sovereign, absolute,
irresistible will. The world will then, and not till then, be set to
rights, when God\'s will is the only will, and all other wills are
melted into it. They did not only know the truth of God, but the will of
God, that which he would have them to do. It is possible for a hypocrite
to have a great deal of knowledge in the will of God.-And approvest the
things that are more excellent-dokimazeis ta diapheronta. Paul prays for
it for his friends as a very great attainment, Phil. 1:10. Eis to
dokimazein hymas ta diapheronta. Understand it, `(1.)` Of a good
apprehension in the things of God, reading it thus, Thou discernest
things that differ, knowest how to distinguish between good and evil, to
separate between the precious and the vile (Jer. 15:19), to make a
difference between the unclean and the clean, Lev. 11:47. Good and bad
lie sometimes so near together that it is not easy to distinguish them;
but the Jews, having the touchstone of the law ready at hand, were, or
at least thought they were, able to distinguish, to cleave the hair in
doubtful cases. A man may be a good casuist and yet a bad
Christian-accurate in the notion, but loose and careless in the
application. Or, we may, with De Dieu, understand controversies by the
ta diapheronta. A man may be well skilled in the controversies of
religion, and yet a stranger to the power of godliness. `(2.)` Of a warm
affection to the things of God, as we read it, Approvest the things that
are excellent. There are excellences in religion which a hypocrite may
approve of: there may be a consent of the practical judgment to the law,
that it is good, and yet that consent overpowerd by the lusts of the
flesh, and of the mind:-

-Video meliora proboque

Deteriora sequor.

I see the better, but pursue the worse.

and it is common for sinners to make that approbation an excuse which is
really a very great aggravation of a sinful course. They got this
acquaintance with, and affection to, that which is good, but being
instructed out of the law, kateµchoumenos-being catechised. The word
signifies an early instruction in childhood. It is a great privilege and
advantage to be well catechised betimes. It was the custom of the Jews
to take a great deal of pains in teaching their children when they were
young, and all their lessons were out of the law; it were well if
Christians were but as industrious to teach their children out of the
gospel. Now this is called (v. 20), The form of knowledge, and of the
truth in the law, that is, the show and appearance of it. Those whose
knowledge rests in an empty notion, and does not make an impression on
their hearts, have only the form of it, like a picture well drawn and in
good colours, but which wants life. A form of knowledge produces but a
form of godliness, 2 Tim. 3:5. A form of knowledge may deceive men, but
cannot impose upon the piercing eye of the heart-searching God. A form
may be the vehicle of the power; but he that takes up with that only is
like sounding brass and a tinkling cymbal.

`3.` They were a teaching people, or at least thought themselves so (v.
19, 20): And art confident that thou thyself art a guide of the blind.
Apply it, `(1.)` To the Jews in general. They thought themselves guides to
the poor blind Gentiles that sat in darkness, were very proud of this,
that whoever would have the knowledge of God must be beholden to them
for it. All other nations must come to school to them, to learn what is
good, and what the Lord requires; for they had the lively oracles. `(2.)`
To their rabbis, and doctors, and leading men among them, who were
especially those that judged others, v. 1. These prided themselves much
in the possession they had got of Moses\'s chair, and the deference
which the vulgar paid to their dictates; and the apostle expresses this
in several terms, a guide of the blind, a light of those who are in
darkness, an instructor of the foolish, a teacher of babes, the better
to set forth their proud conceit of themselves, and contempt of others.
This was a string they loved to be harping upon, heaping up titles of
honour upon themselves. The best work, when it is prided in, is
unacceptable to God. It is good to instruct the foolish, and to teach
the babes: but considering our own ignorance, and folly, and inability
to make these teachings successful without God, there is nothing in it
to be proud of.

`II.` He aggravates their provocations (v. 21-24) from two things:-

`1.` That they sinned against their knowledge and profession, did that
themselves which they taught others to avoid: Thou that teachest
another, teachest thou not thyself? Teaching is a piece of that charity
which begins at home, though it must not end there. It was the hypocrisy
of the Pharisees that they did not do as they taught (Mt. 23:3), but
pulled down with their lives what they built up with their preaching;
for who will believe those who do not believe themselves? Examples will
govern more than rules. The greatest obstructors of the success of the
word are those whose bad lives contradict their good doctrine, who in
the pulpit preach so well that it is a pity they should ever come out,
and out of the pulpit live so ill that it is a pity they should ever
come in. He specifies three particular sins that abound among the
Jews:-`(1.)` Stealing. This is charged upon some that declared God\'s
statutes (Ps. 50:16, 18), When thou sawest a thief, then thou
consentedst with him. The Pharisees are charged with devouring widows\'
houses (Mt. 23:14), and that is the worst of robberies. `(2.)` Adultery,
v. 22. This is likewise charged upon that sinner (Ps. 50:18), Thou hast
been partaker with adulterers. Many of the Jewish rabbin are said to
have been notorious for this sin. `(3.)` Sacrilege-robbing in holy things,
which were then by special laws dedicated and devoted to God; and this
is charged upon those that professed to abhor idols. So the Jews did
remarkably, after their captivity in Babylon; that furnace separated
them for ever from the dross of their idolatry, but they dealt very
treacherously in the worship of God. It was in the latter days of the
Old-Testament church that they were charged with robbing God in tithes
and offerings (Mal. 3:8, 9), converting that to their own use, and to
the service of their lusts, which was, in a special manner, set apart
for God. And this is almost equivalent to idolatry, though this
sacrilege was cloaked with the abhorrence of idols. Those will be
severely reckoned with another day who, while they condemn sin in
others, do the same, or as bad, or worse, themselves.

`2.` That they dishonoured God by their sin, v. 23, 24. While God and his
law were an honour to them, which they boasted of and prided themselves
in, they were a dishonour to God and his law, by giving occasion to
those that were without to reflect upon their religion, as if that did
countenance and allow of such things, which, as it is their sin who draw
such inferences (for the faults of professors are not to be laid upon
professions), so it is their sin who give occasion for those inferences,
and will greatly aggravate their miscarriages. This was the condemnation
in David\'s case, that he had given great occasion to the enemies of the
Lord to blaspheme, 2 Sa. 12:14. And the apostle here refers to the same
charge against their forefathers: As it is written, v. 24. He does not
mention the place, because he wrote this to those that were instructed
in the law (in labouring to convince, it is some advantage to deal with
those that have knowledge and are acquainted with the scripture), but he
seems to point at Isa. 52:5; Eze. 36:22, 23; and 2 Sa. 12:14. It is a
lamentation that those who were made to be to God for a name and for a
praise should be to him a shame and dishonour. The great evil of the
sins of professors is the dishonour done to God and religion by their
profession. \"Blasphemed through you; that is, you give the occasion for
it, it is through your folly and carelessness. The reproaches you bring
upon yourselves reflect upon your God, and religion is wounded through
your sides.\" A good caution to professors to walk circumspectly. See 1
Tim. 6:1.

`III.` He asserts the utter insufficiency of their profession to clear
them from the guilt of these provocations (v. 25-20): Circumcision
verily profiteth, if thou keep the law; that is, obedient Jews shall not
lose the reward of their obedience, but will gain this by their being
Jews, that they have a clearer rule of obedience than the Gentiles have.
God did not give the law nor appoint circumcision in vain. This must be
referred to the state of the Jews before the ceremonial polity was
abolished, otherwise circumcision to one that professed faith in Christ
was forbidden, Gal. 5:1. But he is here speaking to the Jews, whose
Judaism would benefit them, if they would but live up to the rules and
laws of it; but if not \"thy circumcision is made uncircumcision; that
is, thy profession will do thee no good; thou wilt be no more justified
than the uncircumcised Gentiles, but more condemned for sinning against
greater light.\" The uncircumcised are in scripture branded as unclean
(Isa. 52:1), as out of the covenant, (Eph. 2:11, 12) and wicked Jews
will be dealt with as such. See Jer. 9:25, 26. Further to illustrate
this,

`1.` He shows that the uncircumcised Gentiles, if they live up to the
light they have, stand upon the same level with the Jews; if they keep
the righteousness of the law (v. 26), fulfil the law (v. 27); that is,
by submitting sincerely to the conduct of natural light, perform the
matter of the law. Some understand it as putting the case of a perfect
obedience to the law: \"If the Gentiles could perfectly keep the law,
they would be justified by it as well as the Jews.\" But it seems rather
to be meant of such an obedience as some of the Gentiles did attain to.
The case of Cornelius will clear it. Though he was a Gentile, and
uncircumcised, yet, being a devout man, and one that feared God with all
his house (Acts 10:2), he was accepted, v. 4. Doubtless, there were many
such instances: and they were the uncircumcision, that kept the
righteousness of the law; and of such he says, `(1.)` That they were
accepted with God, as if they had been circumcised. Their uncircumcision
was counted for circumcision. Circumcision was indeed to the Jews a
commanded duty, but it was not to all the world a necessary condition of
justification and salvation. `(2.)` That their obedience was a great
aggravation of the disobedience of the Jews, who had the letter of the
law, v. 27. Judge thee, that is, help to add to thy condemnation, who by
the letter and circumcision dost transgress. Observe, To carnal
professors the law is but the letter; they read it as a bare writing,
but are not ruled by it as a law. They did transgress, not only
notwithstanding the letter and circumcision, but by it, that is, they
thereby hardened themselves in sin. External privileges, if they do not
do us good, do us hurt. The obedience of those that enjoy less means,
and make a less profession, will help to condemn those that enjoy
greater means, and make a greater profession, but do not live up to it.

`2.` He describes the true circumcision, v. 28, 29. `(1.)` It is not that
which is outward in the flesh and in the letter. This is not to drive us
off from the observance of external institutions (they are good in their
place), but from trusting to them and resting in them as sufficient to
bring us to heaven, taking up with a name to live, without being alive
indeed. He is not a Jew, that is, shall not be accepted of God as the
seed of believing Abraham, nor owned as having answered the intention of
the law. To be Abraham\'s children is to do the works of Abraham, Jn.
8:39, 40. `(2.)` It is that which is inward, of the heart, and in the
spirit. It is the heart that God looks at, the circumcising of the heart
that renders us acceptable to him. See Deu. 30:6. This is the
circumcision that is not made with hands, Col. 2:11, 12. Casting away
the body of sin. So it is in the spirit, in our spirit as the subject,
and wrought by God\'s Spirit as the author of it. `(3.)` The praise
thereof, though it be not of men, who judge according to outward
appearance, yet it is of God, that is, God himself will own and accept
and crown this sincerity; for he seeth not as man seeth. Fair pretences
and a plausible profession may deceive men: but God cannot be so
deceived; he sees through shows to realities. This is alike true of
Christianity. He is not a Christian that is one outwardly, nor is that
baptism which is outward in the flesh; but he is a Christian that is one
inwardly, and baptism is that of the heart, in the spirit, and not in
the letter, whose praise is not of men but of God.
