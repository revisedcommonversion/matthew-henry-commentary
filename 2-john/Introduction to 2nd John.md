Introduction to 2nd John
========================

Here we find a canonical epistle inscribed, principally, not only to a
single person, but to one also of the softer sex. And why not to one of
that sex? In gospel redemption, privilege, and dignity, there is neither
male nor female; they are both one in Christ Jesus. Our Lord himself
neglected his own repast, to commune with the woman of Samaria, in order
to show her the fountain of life; and, when almost expiring upon the
cross, he would with his dying lips bequeath his blessed mother to the
care of his beloved disciple, and thereby instruct him to respect female
disciples for the future. It was to one of the same sex that our Lord
chose to appear first after his return from the grave, and to send by
her the news of his resurrection to this as well as to the other
apostles; and we find afterwards a zealous Priscilla so well acquitting
herself in her Christian race, and particularly in some hazardous
service towards the apostle Paul, that she is not only often mentioned
before her husband, but to her as well as to him, not only the apostle
himself, but also all the Gentile churches, were ready to return their
thankful acknowledgments. No wonder then that a heroine in the Christian
religion, honoured by divine providence, and distinguished by divine
grace, should be dignified also by an apostolical epistle.
