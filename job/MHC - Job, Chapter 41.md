Job, Chapter 41
===============

Commentary
----------

The description here given of the leviathan, a very large, strong,
formidable fish, or water-animal, is designed yet further to convince
Job of his own impotency, and of God\'s omnipotence, that he might be
humbled for his folly in making so bold with him as he had done. `I.` To
convince Job of his own weakness he is here challenged to subdue and
tame this leviathan if he can, and make himself master of him (v. 1-9),
and, since he cannot do this, he must own himself utterly unable to
stand before the great God (v. 10). `II.` To convince Job of God\'s power
and terrible majesty several particular instances are here given of the
strength and terror of the leviathan, which is no more than what God has
given him, nor more than he has under his check, (v. 11, 12). The face
of the leviathan is here described to be terrible (v. 12, 14), his
scales close (v. 15-17), his breath and neesings sparkling (v. 18-21),
his flesh firm (v. 22-24), his strength and spirit, when he is attacked,
insuperable (v. 25-30), his motions turbulent, and disturbing to the
waters (v. 31, 32), so that, upon the whole, he is a very terrible
creature, and man is no match for him (v. 33, 34).

### Verses 1-10

Whether this leviathan be a whale or a crocodile is a great dispute
among the learned, which I will not undertake to determine; some of the
particulars agree more easily to the one, others to the other; both are
very strong and fierce, and the power of the Creator appears in them.
The ingenious Sir Richard Blackmore, though he admits the more received
opinion concerning the behemoth, that it must be meant of the elephant,
yet agrees with the learned Bochart\'s notion of the leviathan, that it
is the crocodile, which was so well known in the river of Egypt. I
confess that that which inclines me rather to understand it of the whale
is not only because it is much larger and a nobler animal, but because,
in the history of the Creation, there is such an express notice taken of
it as is not of any other species of animals whatsoever (Gen. 1:21, God
created great whales), by which it appears, not only that whales were
well known in those parts in the time of Moses, who lived a little after
Job, but that the creation of whales was generally looked upon as a most
illustrious proof of the eternal power and godhead of the Creator; and
we may conjecture that this was the reason (for otherwise it seems
unaccountable) why Moses there so particularly mentions the creation of
the whales, because God had so lately insisted upon the bulk and
strength of that creature than of any other, as the proof of his power;
and the leviathan is here spoken of as an inhabitant of the sea (v. 31),
which the crocodile is not; and Ps. 104:25, 26, there in the great and
wide sea, is that leviathan. Here in these verses,

`I.` He shows how unable Job was to master the leviathan. 1. That he could
not catch him, as a little fish, with angling, v. 1, 2. He had no bait
wherewith to deceive him, no hook wherewith to catch him, no fish-line
wherewith to draw him out of the water, nor a thorn to run through his
gills, on which to carry him home. 2. That he could not make him his
prisoner, nor force him to cry for quarter, or surrender himself at
discretion, v. 3, 4. \"He knows his own strength too well to make many
supplications to thee, and to make a covenant with thee to be thy
servant on condition thou wilt save his life.\" 3. That he could not
entice him into a cage, and keep him there as a bird for the children to
play with, v. 5. There are creatures so little, so weak, as to be easily
restrained thus, and triumphed over; but the leviathan is not one of
these: he is made to be the terror, not the sport and diversion, of
mankind. 4. That he could not have him served up to his table; he and
his companions could not make a banquet of him; his flesh is too strong
to be fit for food, and, if it were not, he is not easily caught. 5.
That they could not enrich themselves with the spoil of him: Shall they
part him among the merchants, the bones to one, the oil to another? If
they can catch him, they will; but it is probable that the art of
fishing for whales was not brought to perfection then, as it has been
since. 6. That they could not destroy him, could not fill his head with
fish-spears, v. 7. He kept out of the reach of their instruments of
slaughter, or, if they touched him, they could not touch him to the
quick. 7. That it was to no purpose to attempt it: The hope of taking
him is in vain, v. 9. If men go about to seize him, so formidable is he
that the very sight of him will appal them, and make a stout man ready
to faint away: Shall not one be cast down even at the sight of him? and
will not that deter the pursuers from their attempt? Job is told, at his
peril, to lay his hand upon him, v. 8. \"Touch him if thou dare;
remember the battle, how unable thou art to encounter such a force, and
what is therefore likely to be the issue of the battle, and do no more,
but desist from the attempt.\" It is good to remember the battle before
we engage in a war, and put off the harness in time if we foresee it
will be to no purpose to gird it on. Job is hereby admonished not to
proceed in his controversy with God, but to make his peace with him,
remembering what the battle will certainly end in if he come to an
engagement. See Isa. 27:4, 5.

`II.` Thence he infers how unable he was to contend with the Almighty.
None is so fierce, none so fool-hardy, that he dares to stir up the
leviathan (v. 10), it being known that he will certainly be too hard for
them; and who then is able to stand before God, either to impeach and
arraign his proceedings or to out-face the power of his wrath? If the
inferior creatures that are put under the feet of man, and over whom he
has dominion, keep us in awe thus, how terrible must the majesty of our
great Lord be, who has a sovereign dominion over us and against whom man
has been so long in rebellion! Who can stand before him when once he is
angry?

### Verses 11-34

God, having in the foregoing verses shown Job how unable he was to deal
with the leviathan, here sets forth his own power in that massy mighty
creature. Here is,

`I.` God\'s sovereign dominion and independency laid down, v. 11. 1. That
he is indebted to none of his creatures. If any pretend he is indebted
to them, let them make their demand and prove their debt, and they shall
receive it in full and not by composition: \"Who has prevented me?\"
that is, \"who has laid any obligations upon me by any services he has
done me? Who can pretend to be before-hand with me? If any were, I would
not long be behind-hand with them; I would soon repay them.\" The
apostle quotes this for the silencing of all flesh in God\'s presence,
Rom. 11:35. Who hath first given to him, and it shall be recompensed to
him again? As God does not inflict upon us the evils we have deserved,
so he does bestow upon us the favours we have not deserved. 2. That he
is the rightful Lord and owner of all the creatures: \"Whatsoever is
under the whole heaven, animate or inanimate, is mine (and particularly
this leviathan), at my command and disposal, what I have an
incontestable property in and dominion over.\" All is his; we are his,
all we have and do; and therefore we cannot make God our debtor; but of
thy own, Lord, have we given thee. All is his, and therefore, if he were
indebted to any, he has wherewithal to repay them; the debt is in good
hands. All is his, and therefore he needs not our services, nor can he
be benefited by them. If I were hungry I would not tell thee, for the
world is mind and the fulness thereof, Ps. 50:12.

`II.` The proof and illustration of it, from the wonderful structure of
the leviathan, v. 12.

`1.` The parts of his body, the power he exerts, especially when he is
set upon, and the comely proportion of the whole of him, are what God
will not conceal, and therefore what we must observe and acknowledge the
power of God in. Though he is a creature of monstrous bulk, yet there is
in him a comely proportion. In our eye beauty lies in that which is
small (inest sua gratia parvis-little things have a gracefulness all
their own) because we ourselves are so; but in God\'s eye even the
leviathan is comely; and, if he pronounce even the whale, event he
crocodile, so, it is not for us to say of any of the works of his hands
that they are ugly of ill-favoured; it is enough to say so, as we have
cause, of our own works. God here goes about to give us an anatomical
view (as it were) of the leviathan; for his works appear most beautiful
and excellent, and his wisdom and power appear most in them, when they
are taken in pieces and viewed in their several parts and proportions.
`(1.)` The leviathan, even prima facie-at first sight, appears formidable
and inaccessible, v. 13, 14. Who dares come so near him while he is
alive as to discover or take a distinct view of the face of the garment,
the skin with which he is clothed as with a garment, so near him as to
bridle him like a horse and so lead him away, so near him as to be
within reach of his jaws, which are like a double bridle? Who will
venture to look into his mouth, as we do into a horse\'s mouth? He that
opens the doors of his face will see his teeth terrible round about,
strong and sharp, and fitted to devour; it would make a man tremble to
think of having a leg or an arm between them. `(2.)` His scales are his
beauty and strength, and therefore his pride, v. 15-17. The crocodile is
indeed remarkable for his scales; if we understand it of the whale, we
must understand by these shields (for so the word is) the several coats
of his skin; or there might be whales in that country with scales. That
which is remarkable concerning the scales is that they stick so close
together, by which he is not only kept warm, for no air can pierce him,
but kept safe, for no sword can pierce him through those scales. Fishes,
that live in the water, are fortified accordingly by the wisdom of
Providence, which gives clothes as it gives cold. `(3.)` He scatters
terror with his very breath and looks; if he sneeze or spout up water,
it is like a light shining, either with the froth or the light of the
sun shining through it, v. 18. The eyes of the whale are reported to
shine in the night-time like a flame, or, as here, like the eye-lids of
the morning; the same they say of the crocodile. The breath of this
creature is so hot and fiery, from the great natural heat within, that
burning lamps and sparks of fire, smoke and a flame, are said to go out
of his mouth, even such as one would think sufficient to set coals on
fire, v. 19-21. Probably these hyperbolical expressions are used
concerning the leviathan to intimate the terror of the wrath of God, for
that is it which all this is designed to convince us of. Fire out of his
mouth devours, Ps. 18:7, 8. The breath of the Almighty, like a stream of
brimstone, kindles Tophet, and will for ever keep it burning, Isa.
30:33. The wicked one shall be consumed with the breath of his mouth, 2
Th. 2:8. `(4.)` He is of invincible strength and most terrible fierceness,
so that he frightens all that come in his way, but is not himself
frightened by any. Take a view of his neck, and there remains strength,
v. 22. his head and his body are well set together. Sorrow rejoices (or
rides in triumph) before him, for he makes terrible work wherever he
comes. Or, Those storms which are the sorrow of others are his joys;
what is tossing to others is dancing to him. His flesh is well knit, v.
23. The flakes of it are joined so closely together, and are so firm,
that it is hard to pierce it; he is as if he were all bone. His flesh is
of brass, which Job had complained his was not, ch. 6:12. His heart is
as firm as a stone, v. 24. He has spirit equal to his bodily strength,
and, though he is bulky, he is sprightly, and not unwieldy. As his flesh
and skin cannot be pierced, so his courage cannot be daunted; but, on
the contrary, he daunts all he meets and puts them into a consternation
(v. 25): When he raises up himself like a moving mountain in the great
waters even the mighty are afraid lest he should overturn their ships or
do them some other mischief. By reason of the breakings he makes in the
water, which threaten death, they purify themselves, confess their sins,
betake themselves to their prayers, and get ready for death. We read
(ch. 3:8) of those who, when they raise up a leviathan, are in such a
fright that they curse the day. It was a fear which, it seems, used to
drive some to their curses and others to their prayers; for, as now, so
then there were seafaring men of different characters and on whom the
terrors of the sea have contrary effects; but all agree there is a great
fright among them when the leviathan raises up himself. `(5.)` All the
instruments of slaughter that are used against him do him no hurt and
therefore are not error to him, v. 26-29. The sword and the spear, which
wound nigh at hand, are nothing to him; the darts, arrows, and
sling-stones, which wound at a distance, do him no damage; nature has so
well armed him cap-a-pie-at all points, against them all. The defensive
weapons which men use when they engage with the leviathan, as the
habergeon, or breast-plate, often serve men no more than their offensive
weapons; iron and brass are to him as straw and rotten wood, and he
laughs at them. It is the picture of a hard-hearted sinner, that
despises the terrors of the Almighty and laughs at all the threatenings
of his word. The leviathan so little dreads the weapons that are used
against him that, to show how hardy he is, he chooses to lie on the
sharp stones, the sharp-pointed things (v. 30), and lies as easy there
as if he lay on the soft mire. Those that would endure hardness must
inure themselves to it. `(6.)` His very motion in the water troubles it
and puts it into a ferment, v. 31, 32. When he rolls, and tosses, and
makes a stir in the water, or is in pursuit of his prey, he makes the
deep to boil like a pot, he raises a great froth and foam upon the
water, such as is upon a boiling pot, especially a pot of boiling
ointment; and he makes a path to shine after him, which even a ship in
the midst of the sea does not, Prov. 30:19. One may trace the leviathan
under water by the bubbles on the surface; and yet who can take that
advantage against him in pursuing him? Men track hares in the snow and
kill them, but he that tracks the leviathan dares not come near him.

`2.` Having given this particular account of his parts, and his power,
and his comely proportion, he concludes with four things in general
concerning this animal:-`(1.)` That he is a non-such among the inferior
creatures: Upon earth there is not his like, v. 33. No creature in this
world is comparable to him for strength and terror. Or the earth is here
distinguished from the sea: His dominion is not upon the earth (so
some), but in the waters. None of all the savage creatures upon earth
come near him for bulk and strength, and it is well for man that he is
confined to the waters and there has a watch set upon him (ch. 7:12) by
the divine Providence, for, if such a terrible creature were allowed to
roam and ravage upon this earth, it would be an unsafe and uncomfortable
habitation for the children of men, for whom it is intended. `(2.)` That
he is more bold and daring than any other creature whatsoever: He is
made without fear. The creatures are as they are made; the leviathan has
courage in his constitution, nothing can frighten him; other creatures,
quite contrary, seem as much designed for flying as this for fighting.
So, among men, some are in their natural temper bold, others are
timorous. `(3.)` That he is himself very proud; though lodged in the deep,
yet he beholds all high things, v. 34. The rolling waves, the impending
rocks, the hovering clouds, and the ships under sail with top and
top-gallant, this mighty animal beholds with contempt, for he does not
think they either lessen him or threaten him. Those that are great are
apt to be scornful. `(4.)` That he is a king over all the children of
pride, that is, he is the proudest of all proud ones. He has more to be
proud of (so Mr. Caryl expounds it) than the proudest people in the
world have; and so it is a mortification to the haughtiness and lofty
looks of men. Whatever bodily accomplishments men are proud of, and
puffed up with, the leviathan excels them and is a king over them. Some
read it so as to understand it of God: He that beholds all high things,
even he, is King over all the children of pride; he can tame the
behemoth (ch. 40:19) and the leviathan, big as they are, and
stout-hearted as they are. This discourse concerning those two animals
was brought in to prove that it is God only who can look upon proud men
and abase them, bring them low and tread them down, and hide them in the
dust (ch. 40:11-13), and so it concludes with a quod erat
demonstrandum-which was to be demonstrated; there is one that beholds
all high things, and, wherein men deal proudly, is above them; he is
King over all the children of pride, whether brutal or rational, and can
make them all either bend or break before him, Isa. 2:11. The lofty
looks of man shall be humbled, and the haughtiness of men shall be bowed
down, and thus the Lord alone shall be exalted.
