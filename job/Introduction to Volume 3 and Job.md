> [[Introduction to Volume 3]{.underline}](#introduction-to-volume-3)
>
> [[Introduction to Job]{.underline}](#introduction-to-job)

Introduction to Volume 3
========================

These five books of scripture which are contained in this third volume
and which I have here endeavoured, according to the measure of the gift
given to me, to explain and improve, for the use of those who desire to
read them, not only with understanding, but to their edification-though
they have the same divine origin, design, and authority, as those that
went before, yet, upon some accounts, are of a very different nature
from them, and from the rest of the sacred writings, such variety of
methods has Infinite Wisdom seen fit to take in conveying the light of
divine revelation to the children of men, that this heavenly food might
have (as the Jews say of the manna) something in it agreeable to every
palate and suited to every constitution. If every eye be not thus
opened, every mouth will be stopped, and such as perish in their
ignorance will be left without excuse. We have piped unto you, and you
have not danced, we have mourned unto you, and you have not lamented,
Mt. 11:17.

`I.` The books of scripture have hitherto been, for the most part, very
plain and easy, narratives of matter of fact, which he that runs may
read and understand, and which are milk for babes, such as they can
receive and digest, and both entertain and nourish themselves with. The
waters of the sanctuary have hitherto been but to the ankles or to the
knees, such as a lamb might wade in, to drink of and wash in; but here
we are advanced to a higher form in God\'s school, and have books put
into our hands wherein are many things dark and hard to be understood,
which we do not apprehend the meaning of so suddenly and so certainly as
we could wish, the study of which requires a more close application of
mind, a greater intenseness of thought, and the accomplishing of a
diligent search, which yet the treasure hid in them, when it is found,
will abundantly recompense. The waters of the sanctuary are here to the
loins, and still as we go forward we shall find the waters still risen
in the prophetical books, waters to swim in (Eze. 47:3-5), not fordable,
nor otherwise to be passed over-depths in which an elephant will not
find footing, strong meat for strong men. The same method is observable
in the New Testament, where we find the plain history of Christ and his
gospel placed first in the Evangelists and the Acts of the Apostles;
then the mystery of both in the Epistles, which are more difficult to be
understood; and, lastly, the prophecies of things to come in the
apocalyptic visions. This method, so exactly observed in both the
Testaments, directs us in what order to proceed both in studying the
things of God ourselves and in teaching them to others; we must go in
the order that the scripture does; and where can we expect to find a
better method of divinity and a better method of preaching?

`1.` We must begin with those things that are most plain and easy, as,
blessed be God, those things are which are most necessary to salvation
and of the greatest use. We must lay our foundation firm, in a sound
experimental knowledge of the principles of religion, and then the
super-structure will be well reared and will stand firmly. It is not
safe to launch out into the deep at first, nor to venture into points
difficult and controverted until we have first thoroughly digested the
elements of the oracles of God and turned them in succum et
sanguinem-into juice and blood. Those that begin their Bible at the
wrong end commonly use their knowledge of it in the wrong way. And, in
training up others, we must be sure to ground them well at first in
those truths of God which are plain, and in some measure level to their
capacity, which we find they comprehend, and relish, and know how to
make use of, and not amuse those that are weak with things above them,
things of doubtful disputation, which they cannot apprehend any
certainty of nor advantage by. Our Lord Jesus spoke the word to the
people as they were able to hear it (Mk. 4:33) and had many things to
say to his disciples which he did not say because as yet they could not
bear them, Jn. 16:12, 13. And those whom St. Paul could not speak to as
unto spiritual-though he blamed them for their backwardness, yet he
accommodated himself to their weakness, and spoke to them as unto babes
in Christ, 1 Co. 3:1, 2.

`2.` Yet we must not rest in these things. We must not be always children
that have need of milk, but nourished up with that, and gaining
strength, we must go on to perfection (Heb. 6:1), that having, by reason
of use, our spiritual senses exercised (Heb. 5:14), we may come to full
age, and put away childish things, and, forgetting the things which are
behind, that is, so well remembering them (Phil. 3:13) that we need not
be still poring over them as those that are ever learning the same
lesson, we may reach forth to the things which are before. Though we
must never think to learn above our Bible, as long as we are here in
this world, yet we must still be getting forward in it. You have dwelt
long enough in this mountain; now turn and take your journey onward in
the wilderness towards Canaan. Our motto must be Plus ultra-Onward. And
then shall we know if thus, by regular steps (Hos. 6:3), we follow on to
know the Lord and what the mind of the Lord is.

`II.` The books of scripture have hitherto been mostly historical, but
now the matter is of another nature; it is doctrinal and devotional,
preaching and praying; and in this way of writing, as well as in the
former, a great deal of excellent knowledge is conveyed, which serves
very valuable purposes. It will be of good use to know not only what
others did that went before us, and how they fared, but what their
notions and sentiments were, what their thoughts and affections were,
that we may, with the help of them, form our minds aright. Plutarch\'s
Morals are reputed as a useful treasure in the commonwealth of learning
as Plutarch\'s Lives, and the wise disquisitions and discourses of the
philosophers as the records of the historians; nor is this divine
philosophy (if I may so call it), which we have in these books, less
needful, nor less serviceable, to the church, than the sacred history
was. Blessed be God for both.

`III.` The Jews make these books to be given by a divine inspiration
somewhat different from that both of Moses and the prophets. They
divided the books of the Old Testament into the Law, the Prophets and
the ktwbym-Writings, which Epiphanius emphatically translates
grapheia-things written, and these books are more commonly called among
the Greeks Hagiographa-Holy writings: the Jews attribute them to that
distinct kind of inspiration which they call rwh hqds-The Holy Spirit.
Moses they supposed to write by the Spirit in a way above all the other
prophets, for with him God spoke mouth to mouth, even apparently (Num.
12:8) knew him, that is, conversed with him face to face, Deu. 34:10. He
was made partaker of divine revelation (as Maimonides distinguishes, De
Fund. Legis, ch. 7) per vigiliam-while awake, (See Mr. Smith\'s
Discourses on Prophecy, ch. 11.) whereas God manifested himself to all
the other prophets in a dream or vision: and he adds that Moses
understood the words of prophecy without any perturbation or
astonishment of mind, whereas the other prophets commonly fainted and
were troubled. But the writers of the Hagiographa they suppose to be
inspired in a degree somewhat below that of the other prophets, and to
receive divine revelation, not as they did by dreams, and visions, and
voices, but (as Maimonides describes it, More Nevochim-part 2 c. 45)
they perceived some power to rise within them, and rest upon them, which
urged and enabled them to write or speak far above their own natural
ability, in psalms or hymns, or in history or in rules of good living,
still enjoying the ordinary vigour and use of their senses. Let David
himself describe it. The Spirit of the Lord spoke by me, and his word
was in my tongue; the God of Israel said, the Rock of Israel spoke to
me, 2 Sa. 23:2, 3. This gives such a magnificent account of the
inspiration by which David wrote that I see not why it should be made
inferior to that of the other prophets, for David is expressly called a
prophet, Acts 2:29, 30. But, since our hand is in with the Jewish
masters, let us see what books they account Hagiographa. These five that
are now before us come, without dispute, into this rank of sacred
writers, and the book of the Lamentations is not unfitly added to them.
Indeed the Jews, when they would speak critically, reckon all those
songs which we meet with in the Old Testament among the Hagiographa; for
though they were penned by prophets, and under the direction of the Holy
Ghost, yet, because they were not the proper result of a visum
propheticum-prophetic vision, they were not strictly prophecy. As to the
historical books, they distinguish (but I think it is a distinction
without a difference); some of them they assign to the prophets, calling
them the prophetae priores-the former prophets, namely, Joshua, Judges,
and the two books of the Kings; but others they rank among the
Hagiographa, as the book of Ruth (which yet is but an appendix to the
book of Judges), the two books of Chronicles, with Ezra, Nehemiah, and
the book of Esther, which last the rabbin have a great value for, and
think it is to be had in equal esteem with the law of Moses itself, that
it shall last as long as that lasts, and shall survive the writings of
the Prophets. And, lastly, they reckon the book of Daniel among the
Hagiographa, (Hil. Megil. c. 2, 11) for which no reason can be given,
since he was not inferior to any of the prophets in the gift of
prophecy; and therefore the learned Mr. Smith thinks that their placing
him among the Hagiographical writers was fortuitous by mistake (Vid.
Hottinger. Thesaur. lib. 2, cap. 1, 3). Mr. Smith, in his Discourse
before quoted, though he supposes this kind of divine inspiration to be
more \"pacate and serene than that which was strictly called prophecy,
not acting so much upon the imagination, but seating itself in the
higher and purer faculties of the soul, yet shows that it manifested
itself to be of a divine nature, not only as it always elevated pious
souls into strains of devotion, or moved them strangely to dictate
matters of true piety and goodness, but as it came in abruptly upon the
minds of those holy men, and transported them from the temper of mind
they were in before, so that they perceived themselves captivated by the
power of some higher light than that which their own understanding
commonly poured out upon them; and this, says he, was a kind of vital
form to that light of divine and sanctified reason which they were
perpetually possessed of and that constant frame of holiness and
goodness which dwelt in their hallowed minds.\" We have reason to
glorify the God of Israel who gave such power unto men and has here
transmitted to us the blessed products of that power.

`IV.` The style and composition of these books are different from those
that go before and those that follow. Our Saviour divides the books of
the Old Testament into the Law, the Prophets, and the Psalms (Lu.
24:44), and thereby teaches us to distinguish those books that are
poetical, or metrical, from the Law and the Prophets; and such are all
these that are now before us, except Ecclesiastes, which yet, having
something restrained in its style, may well enough be reckoned among
them. They are books in verse, according to the ancient rules of
versifying, though not according to the Greek and Latin prosodies. Some
of the ancients call these five books the second Pentateuch of the Old
Testament (Damascen. Orthod. Fid. 1.4, cap. 18.), five sacred volumes
which are as the satellites to the five books of the law of Moses.
Gregory Nazianzen (Vid. Suicer. Thesaur. in sticheµra-carm. 33, p. 98)
calls these hai sticheµrai pente-the five metrical books; first Job (so
he reckons them up), then David, then the three of Solomon-Ecclesiastes,
the Song, and Proverbs. Amphilochius, bishop at Iconium, in his iambic
poem to Seleucus, reckons them up particularly, and calls them
sticheµras pente Biblos-the five verse-books. Epiphanius (lib. de
ponder. et mensur. p. 533) pente sticheµreis-the five verse-books. And
Cyril. Hierosol. Collect. 4, p. 30 (mihi-in my copy), calls these five
books ta sticheµra-books in verse. Polychronius, in his prologue to Job,
says that as those that are without call their tragedies and comedies
poieµtika-poetics, so, in sacred writ, those books which are composed in
Hebrew metre (of which he reckons Job the first) we call sticheµra
biblia-books in verse, written kata stichon-according to order. What is
written in metre, or rhythm, is so called from metros-a measure, and
arithmos-a number, because regulated by certain measures, or numbers of
syllables, which please the ear with their smoothness and cadency, and
so insinuate the matter the more movingly and powerfully into the fancy.
Sir William Temple, (Miscell, part 2.) in his essay upon poetry, thinks
it is generally agreed to have been the first sort of writing that was
used in the world, nay, that, in several nations, poetical compositions
preceded the very invention or usage of letters. The Spaniards (he says)
found in America many strains of poetry, and such as seemed to flow from
a true poetic vein, before any letters were known in those regions. The
same (says he) is probable of the Scythians and Grecians: the oracles of
Apollo were delivered in verse. Homer and Hesiod wrote their poems (the
very Alcoran of the pagan daemonology) many ages before the appearing of
any of the Greek philosophers or historians; and long before them (if we
may give credit to the antiquities of Greece), even before the days of
David, Orpheus and Linus were celebrated poets and musicians in Greece;
and at the same time Carmenta, the mother of Evander, who was the first
that introduced letters among the natives of Greece, was so called a
carmine-from a song, because she expressed herself in verse. And in such
veneration was this way of writing among the ancients that their poets
were called vates-prophets, and their muses were deified. But, which is
more certain and considerable, the most ancient composition that we meet
with in scripture was the song of Moses at the Red Sea (Ex. 15), which
we find before the very first mention of writing, for that occurs not
until Ex. 17:14, when God bade Moses write a memorial of the war with
Amalek. The first, and indeed the true and general end of writing, is a
help of memory; and poetry does in some measure answer that end, and
even in the want of writing, much more with writing, helps to preserve
the remembrance of ancient things. The book of wars of the Lord (Num.
21:14), and the book of Jasher (Jos. 10:13; 2 Sa. 1:18), seem to have
been both written in poetic measures. Many sacred songs we meet with in
the Old Testament, scattered both in the historical and prophetical
books, penned on particular occasions, which, in the opinion of very
competent judges, \"have in them as true and noble strains of poetry and
picture as are met with in any other language whatsoever, in spite of
all disadvantages from translations into such different tongues and
common prose, (Sir W. Temple, p. 329.) nay, are nobler examples of the
true sublime style of poetry than any that can be found in the Pagan
writers; the images are so strong, the thoughts so great, the
expressions so divine, and the figures so admirably bold and moving,
that the wonderful manner of these writers is quite inimitable.\" (Sir
R. Blackmore\'s preface to Job.) It is fit that what is employed in the
service of the sanctuary should be the best in its kind.

The books here put together are poetical. Job is an heroic poem, the
book of Psalms a collection of divine odes or lyrics, Solomon\'s Song a
pastoral and an epithalamium; they are poetical, and yet sacred and
serious, grave and full of majesty. They have a poetic force and flame,
without poetic fury and fiction, and strangely command and move the
affections, without corrupting the imagination or putting a cheat upon
it; and, while they gratify the ear, they edify the mind and profit the
more by pleasing. It is therefore much to be lamented that so powerful
an art, which was at first consecrated to the honour of God, and has
been so often employed in his service, should be debauched, as it has
been, and is at this day, into the service of his enemies-that his corn,
and wine, and oil should be prepared for Baal.

`V.` As the manner of the composition of these books is excellent, and
very proper to engage the attention, move the affections, and fix them
in the memory, so the matter is highly useful, and such as will be every
way serviceable to us. They have in them the very sum and substance of
religion, and what they contain is more fitted to our hand, and made
ready for use, than any part of the Old Testament, upon which account,
if we may be allowed to compare one star with another in the firmament
of the scripture, these will be reckoned stars of the first magnitude.
All scripture is profitable (and this part of it in a special manner)
for instruction in doctrine, in devotion, and in the right ordering of
the conversation. The book of Job directs us what we are to believe
concerning God, the book of Psalms how we are to worship him, pay our
homage to him, and maintain our communion with him, and then the book of
the Proverbs shows very particularly how we are to govern ourselves en
paseµ anastropheµ-in every turn of human life; thus shall the man of
God, by a due attention to these lights, be perfect, thoroughly
furnished for every good work. And these are placed according to their
natural order, as well as according to the order of time; for very fitly
are we first led into the knowledge of God, our judgments rightly formed
concerning him, and our mistakes rectified, and then instructed how to
worship him and to choose the things that please him. We have here much
of natural religion, its principles, its precepts-much of God, his
infinite perfections, his relations to man, and his government both of
the world and of the church; here is much of Christ, who is the spring,
and soul, and centre, of revealed religion, and whom both Job and David
were eminent types of, and had clear and happy prospects of. We have
here that which will be of use to enlighten our understandings, and to
acquaint us more and more with the things of God, with the deep things
of God-speculations to entertain the most contemplative, and discoveries
to satisfy the most inquisitive and increase the knowledge of those that
are most knowing. Here is that also which, with a divine light, will
bring into the soul the heat and influence of a divine fire, will kindle
and inflame pious and devout affections, on which wings we may soar
upwards until we enter into the holiest. We may here be in the mount
with God, to behold his beauty; and when we come down from that mount,
if we retain (as we ought) the impressions of our devotion upon our
spirits and make conscience of doing that good which the Lord our God
here requires of us, our faces shall shine before all with whom we
converse, who shall take occasion thence to glorify our Father who is in
heaven, Mt. 5:16. Thus great, thus noble, thus truly excellent, is the
subject, and thus capable of being improved, which gives me the more
reason to be ashamed of the meanness of my performance, that the comment
breathes so little of the life and spirit of the text. We often wonder
at those that are not at all affected with the great things of God, and
have no taste nor relish of them, because they know little of them; but
perhaps we have more reason to wonder at ourselves, that conversing so
frequently, so intimately, with them, we are not more affected with
them, so as even to be wholly taken up with them, and in a continual
transport of delight in the contemplation of them. We hope to be so
shortly; in the mean time, though like the three disciples that were the
witnesses of Christ\'s transfiguration upon the mount we are but dull
and sleepy, yet we can say, Master, it is good to be here; here let us
make tabernacles, Lu. 9:32, 33.

I have nothing here to boast of-nothing at all, but a great deal to be
humbled for, that I have not come up to what I have aimed at in respect
of fulness and exactness. In the review of the work, I find many
defects, and those who are critical, perhaps, will meet with some
mistakes in it; but I have done it with what care I could, and desire to
be thankful to God who by his grace has carried me on in his work thus
far: let that grace have all the glory (Phil. 2:13), which works in us
both to will and to do whatever we will or do that is good or serves any
good purpose. What is from God will, I trust, be to him, will be
graciously accepted by him, according to what a man has, and not
according to what he had not, and will be of some use to his church; and
what is from myself (that is, all the defects and errors) will, I trust,
be favourably passed by and pardoned. That prayer of St. Austin is mine,
Domine Deus, quaecunque dixi in his libris de tuo, agnoscant et tui; et
quae de meo, et tu ignosce et tui-Lord God, whatever I have maintained
in these books correspondent with what is contained in thine grant that
thy people may approve as well as thyself; whatever is but the doctrine
of my book forgive thou, and grant that thy people may forgive it also.
I must beg likewise to own, to the honour of our great Master, that I
have found the work to be its own wages, and that the more we converse
with the word of God the more it is to us as the honey and the
honeycomb, Ps. 19:10. In gathering some gleaning of this harvest for
others we may feast ourselves; and, when we are enabled by the grace of
God to do so, we are best qualified to feed others. I was much pleased
with a passage I lately met with of Erasmus, that great scholar and
celebrated wit, in an epistle dedicatory before his book De Ratione
Concionandi, where, as one weary of the world and the hurry of it, he
expresses an earnest desire to spend the rest of his days in secret
communion with Jesus Christ, encouraged by his gracious invitation to
those who labour and are heavy laden to come unto him for rest (Mt.
11:28), and this alone is that which he thinks will yield him true
satisfaction. I think his words worth transcribing, and such as deserve
to be inserted among the testimonies of great men to serious godliness.
Neque quisquam facilè credat quàm miserè animus jamdudum affectet ab his
laboribus in tranquillam otium secedere, quodque superest vitae
(superest autem vix brevis palmus sive pugillus), solum cum eo solo
colloqui, qui clamavit olim (nec hodiè mutat vocem suam), \"Venite ad
me, omnes qui laboratis et onerati estis, ego reficiam vos;\"
quandoquidem in tam turbulento, ne dicam furente, saeculo, in tot
molestiis quas vel ipsa tempora publicè invehunt, vel privatim adfert
oetas ac valetudo, nihil reperio in quo mens mea libentius conquiescat
quàm in hoc arcano colloquio-No one will easily believe how anxiously,
for a long time past, I have wished to retire from these labours into a
scene of tranquility, and, during the remainder of life (dwindled, it is
true, to the shortest span), to converse only with him who once cried
(nor does he now retract), \"Come unto me, all you that labour and are
heavy laden, and I will refresh you,\" for in this turbulent, not to say
furious, age, the many public sources of disquietude, connected with the
infirmities of advancing age, leave no solace to my mind to be compared
with this secret communion. In the pleasing contemplation of the divine
beauty and benignity we hope to spend a blessed eternity, and therefore
in this work it is good t o spend as much as may be of our time.

One volume more, containing the prophetical books, will finish the Old
Testament, if the Lord continue my life, and leisure, and ability of
mind and body for this work. It is begun, and I find it will be larger
than any of the other volumes, and longer in the doing; but, as God by
his grace shall furnish me for it and assist me in it (without which
grace I am nothing, less than nothing, worse than nothing), it shall be
carried on with all convenient speed; and sat citò, si sat benè-if with
sufficient ability, it will be with sufficient speed. I desire the
prayers of my friends that God would minister seed to the sower and
bread to the eaters (Isa. 55:10), that he would multiply the seed sown
and increase the fruits of our righteousness (2 Co. 9:10), that so he
who sows and those who reap may rejoice together (Jn. 4:36); and the
great Lord of the harvest shall have the glory of all.

Chester, May 13, 1710

M.H.

Introduction to Job
===================

This book of Job stands by itself, is not connected with any other, and
is therefore to be considered alone. Many copies of the Hebrew Bible
place it after the book of Psalms, and some after the Proverbs, which
perhaps has given occasion to some learned men to imagine it to have
been written by Isaiah or some of the later prophets. But, as the
subject appears to have been much more ancient, so we have no reason to
think but that the composition of the book was, and that therefore it is
most fitly placed first in this collection of divine morals: also, being
doctrinal, it is proper to precede and introduce the book of Psalms,
which is devotional, and the book of Proverbs, which is practical; for
how shall we worship or obey a God whom we know not? As to this book,

`I.` We are sure that it is given by inspiration of God, though we are not
certain who was the penman of it. The Jews, though no friends to Job,
because he was a stranger to the commonwealth of Israel, yet, as
faithful conservators of the oracles of God committed to them, always
retained this book in their sacred canon. The history is referred to by
one apostle (James 5:11) and one passage (ch. 5:13) is quoted by another
apostle, with the usual form of quoting scripture, It is written, 1 Co.
3:19. It is the opinion of many of the ancients that this history was
written by Moses himself in Midian, and delivered to his suffering
brethren in Egypt, for their support and comfort under their burdens,
and the encouragement of their hope that God would in due time deliver
and enrich them, as he did this patient sufferer. Some conjecture that
it was written originally in Arabic, and afterwards translated into
Hebrew, for the use of the Jewish church, by Solomon (so Monsieur
Jurieu) or some other inspired writer. It seems most probable to me that
Elihu was the penman of it, at least of the discourses, because (ch.
32:15, 16) he mingles the words of a historian with those of a
disputant: but Moses perhaps wrote the first two chapters and the last,
to give light to the discourses; for in them God is frequently called
Jehovah, but not once in all the discourses, except ch. 12:9. That name
was but little known to the patriarchs before Moses, Ex. 6:3. If Job
wrote it himself, some of the Jewish writers themselves own him a
prophet among the Gentiles; if Elihu, we find he had a spirit of
prophecy which filled him with matter and constrained him, ch. 32:18.

`II.` We are sure that it is, for the substance of it, a true history,
and not a romance, though the dialogues are poetical. No doubt there was
such a man as Job; the prophet Ezekiel names him with Noah and Daniel,
Eze. 14:14. The narrative we have here of his prosperity and piety, his
strange afflictions and exemplary patience, the substance of his
conferences with his friends, and God\'s discourse with him out of the
whirlwind, with his return at length to a very prosperous condition, no
doubt is exactly true, though the inspired penman is allowed the usual
liberty of putting the matter of which Job and his friends discoursed
into his own words.

`III.` We are sure that it is very ancient, though we cannot fix the
precise time either when Job lived or when the book was written. So
many, so evident, are its hoary hairs, the marks of its antiquity, that
we have reason to think it of equal date with the book of Genesis
itself, and that holy Job was contemporary with Isaac and Jacob; though
not coheir with them of the promise of the earthly Canaan, yet a
joint-expectant with them of the better country, that is, the heavenly.
Probably he was of the posterity of Nahor, Abraham\'s brother, whose
first-born was Uz (Gen. 22:21), and in whose family religion was for
some ages kept up, as appears, Gen. 31:53, where God is called, not only
the God of Abraham, but the God of Nahor. He lived before the age of man
was shortened to seventy or eighty, as it was in Moses\'s time, before
sacrifices were confined to one altar, before the general apostasy of
the nations from the knowledge and worship of the true God, and while
yet there was no other idolatry known than the worship of the sun and
moon, and that punished by the Judges, ch. 31:26-28. He lived while God
was known by the name of God Almighty more than by the name of Jehovah;
for he is called Shaddai-the Almighty, above thirty times in this book.
He lived while divine knowledge was conveyed, not by writing, but by
tradition; for to that appeals are here made, ch. 8:8; 21:29; 15:18;
5:1. And we have therefore reason to think that he lived before Moses,
because here is no mention at all of the deliverance of Israel out of
Egypt, or the giving of the law. There is indeed one passage which might
be made to allude to the drowning of Pharaoh (ch. 26:12): He divideth
the sea with his power, and by his understanding he smiteth through
Rahab, which name Egypt is frequently called by in scripture, as Ps.
87:4; 89:10; Isa. 51:9. But that may as well refer to the proud waves of
the sea. We conclude therefore that we are here got back to the
patriarchal age, and, besides its authority, we receive this book with
veneration for its antiquity.

`IV.` We are sure that it is of great use to the church, and to every
good Christian, though there are many passages in it dark and hard to be
understood. We cannot perhaps be confident of the true meaning of every
Arabic word and phrase we meet with in it. It is a book that finds a
great deal of work for the critics; but enough is plain to make the
whole profitable, and it was all written for our learning.

`1.` This noble poem presents to us, in very clear and lively characters,
these five things among others:-`(1.)` A monument of primitive theology.
The first and great principles of the light of nature, on which natural
religion is founded, are here, in a warm, and long, and learned dispute,
not only taken for granted on all sides and not the least doubt made of
them, but by common consent plainly laid down as eternal truths,
illustrated and urged as affecting commanding truths. Wherever the being
of God, his glorious attributes and perfections, his unsearchable
wisdom, his irresistible power, his inconceivable glory, his inflexible
justice, and his incontestable sovereignty, discoursed of with more
clearness, fulness, reverence, and divine eloquence, than in this book?
The creation of the world, and the government of it, are here admirably
described, not as matters of nice speculation, but as laying most
powerful obligations upon us to fear and serve, to submit to and trust
in, our Creator, owner, Lord, and ruler. Moral good and evil, virtue and
vice, were never drawn more to the life (the beauty of the one and the
deformity of the other) than in this book; nor the inviolable rule of
God\'s judgment more plainly laid down, That happy are the righteous, it
shall be well with them; and Woe to the wicked, it shall be ill with
them. These are not questions of the schools to keep the learned world
in action, nor engines of state to keep the unlearned world in awe; no,
it appears by this book that they are sacred truths of undoubted
certainty, and which all the wise and sober part of mankind have in
every age subscribed and submitted to. `(2.)` It presents us with a
specimen of Gentile piety. This great saint descended probably not from
Abraham, but Nahor; or, if from Abraham, not from Isaac, but from one of
the sons of the concubines that were sent into the east-country (Gen.
25:6); or, if from Isaac, yet not from Jacob, but Esau; so that he was
out of the pale of the covenant of peculiarity, no Israelite, no
proselyte, and yet none like him for religion, nor such a favourite of
heaven upon this earth. It was a truth therefore, before St. Peter
perceived it, that in every nation he that fears God and works
righteousness is accepted of him, Acts 10:35. There were children of God
scattered abroad (Jn. 11:52) besides the incorporated children of the
kingdom, Mt. 8:11, 12. `(3.)` It presents us with an exposition of the
book of Providence, and a clear and satisfactory solution of many of the
difficult and obscure passages of it. The prosperity of the wicked and
the afflictions of the righteous have always been reckoned two as hard
chapters as any in that book; but they are here expounded, and
reconciled with the divine wisdom, purity, and goodness, by the end of
these things. `(4.)` It presents us with a great example of patience and
close adherence to God in the midst of the sorest calamities. Sir
Richard Blackmore\'s most ingenious pen, in his excellent preface to his
paraphrase on this book, makes Job a hero proper for an epic poem; for,
says he, \"He appears brave in distress and valiant in affliction,
maintains his virtue, and with that his character, under the most
exasperating provocations that the malice of hell could invent, and
thereby gives a most noble example of passive fortitude, a character no
way inferior to that of the active hero,\" etc. `(5.)` It presents us with
an illustrious type of Christ, the particulars of which we shall
endeavour to take notice of as we go along. In general, Job was a great
sufferer, was emptied and humbled, but in order to his greater glory. So
Christ abased himself, that we might be exalted. The learned bishop
Patrick quotes St. Jerome more than once speaking of Job as a type of
Christ, who for the job that was set before him endured the cross, who
was persecuted, for a time, by men and devils, and seemed forsaken of
God too, but was raised to be an intercessor even for his friends and
had added affliction to his misery. When the apostle speaks of the
patience of Job he immediately takes notice of the end of the Lord, that
is, of the Lord Jesus (as some understand it), typified by Job, James
5:11.

`2.` In this book we have, `(1.)` The history of Job\'s sufferings, and his
patience under them (ch. 1, 2, not without a mixture of human frailty,
ch. 3. `(2.)` A dispute between him and his friends upon them, in which,
`[1.]` The opponents were Eliphaz, Bildad, and Zophar. `[2.]` The
respondent was Job. `[3.]` The moderators were, First, Elihu, ch. 32-37.
Secondly, God himself, ch. 38-41. `(3.)` The issue of all in Job\'s honour
and prosperity, ch. 42. Upon the whole, we learn that many are the
afflictions of the righteous, but that when the Lord delivers them out
of them all the trial of their faith will be found to praise, and
honour, and glory.
