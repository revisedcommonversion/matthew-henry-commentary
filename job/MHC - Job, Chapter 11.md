Job, Chapter 11
===============

Commentary
----------

Poor Job\'s wound\'s were yet bleeding, his sore still runs and ceases
not, but none of his friends bring him any oil, any balm; Zophar, the
third, pours into them as much vinegar as the two former had done. `I.` He
exhibits a very high charge against Job, as proud and false in
justifying himself (v. 1-4). `II.` He appeals to God for his conviction,
and begs that God would take him to task (v. 5) and that Job might be
made sensible, 1. Of God\'s unerring wisdom and his inviolable justice
(v. 6). 2. Of his unsearchable perfections (v. 7-9). 3. Of his
incontestable sovereignty and uncontrollable power (v. 10). 4. Of the
cognizance he takes of the children of men (v. 11, 12). `III.` He assures
him that, upon his repentance and reformation (v. 13, 14), God would
restore him to his former prosperity and safety (v. 15-19); but that, if
he were wicked it was in vain to expect it (v. 20).

### Verses 1-6

It is sad to see what intemperate passions even wise and good men are
sometimes betrayed into by the heat of disputation, of which Zophar here
is an instance. Eliphaz began with a very modest preface, ch. 4:2.
Bildad was a little more rough upon Job, ch. 8:2. But Zophar falls upon
him without mercy, and gives him very bad language: Should a man full of
talk be justified? And should thy lies make men hold their peace? Is
this the way to comfort Job? No, nor to convince him neither. Does this
become one that appears as an advocate for God and his justice? Tantaene
animis coelestibus irae?-In heavenly breasts can such resentment dwell?
Those that engage in controversy will find it very hard to keep their
temper. All the wisdom, caution, and resolution they have will be little
enough to prevent their breaking out into such indecencies as we here
find Zophar guilty of.

`I.` He represents Job otherwise than what he was, v. 2, 3. He would have
him thought idle and impertinent in his discourse, and one that loved to
hear himself talk; he gives him the lie, and calls him a mocker; and all
this that it might be looked upon as a piece of justice to chastise him.
Those that have a mind to fall out with their brethren, and to fall foul
upon them, find it necessary to put the worst colours they can upon them
and their performances, and, right or wrong, to make them odious. We
have read and considered Job\'s discourses in the foregoing chapters,
and have found them full of good sense and much to the purpose, that his
principles are right, his reasonings strong, many of his expressions
weighty and very considerable, and that what there is in them of heat
and passion a little candour and charity will excuse and overlook; and
yet Zophar here invidiously represents him, 1. As a man that never
considered what he said, but uttered what came uppermost, only to make a
noise with the multitude of words, hoping by that means to carry his
cause and run down his reprovers: Should not the multitude of words be
answered? Truly, sometimes it is no great matter whether it be or no;
silence perhaps is the best confutation of impertinence and puts the
greatest contempt upon it. Answer not a fool according to his folly.
But, if it be answered, let reason and grace have the answering of it,
not pride and passion. Should a man full of talk (margin, a man of lips,
that is all tongue, vox et praeterea nihil-mere voice) be justified?
Should he be justified in his loquacity, as in effect he is if he be not
reproved for it? No, for in the multitude of words there wanteth not
sin. Should he be justified by it? Shall many words pass for valid
pleas? Shall he carry the day with the flourishes of language? No, he
shall not be accepted with God, or any wise men, for his much speaking,
Mt. 6:7. 2. As a man that made no conscience of what he said-a liar, and
one that hoped by the impudence of lies to silence his adversaries
(should thy lies make men hold their peace?)-a mocker, one that bantered
all mankind, and knew how to put false colours upon any thing, and was
not ashamed to impose upon every one that talked with him: When thou
mockest shall no man make thee ashamed? Is it not time to speak, to stem
such a violent tide as this? Job was not mad, but spoke the words of
truth and soberness, and yet was thus misrepresented. Eliphaz and Bildad
had answered him, and said what they could to make him ashamed; it was
therefore no instance of Zophar\'s generosity to set upon a man so
violently who was already thus harassed. Here were three matched against
one.

`II.` He charges Job with saying that which he had not said (v. 4): Thou
hast said, My doctrine is pure. And what if he had said so? It was true
that Job was sound in the faith, and orthodox in his judgment, and spoke
better of God than his friends did. If he had expressed himself
unwarily, yet it did not therefore follow but that his doctrine was
true. But he charges him with saying, I am clean in thy eyes. Job had
not said so: he had indeed said, Thou knowest that I am not wicked (ch.
10:7); but he had also said, I have sinned, and never pretended to a
spotless perfection. He had indeed maintained that he was not a
hypocrite as they charged him; but to infer thence that he would not own
himself a sinner was an unfair insinuation. We ought to put the best
construction on the words and actions of our brethren that they will
bear; but contenders are tempted to put the worst.

`III.` He appeals to God, and wishes him to appear against Job. So very
confident is he that Job is in the wrong that nothing will serve him but
that God must immediately appear to silence and condemn him. We are
commonly ready with too much assurance to interest God in our quarrels,
and to conclude that, if he would but speak, he would take our part and
speak for us, as Zophar here: O that God would speak! for he would
certainly open his lips against thee; whereas, when God did speak, he
opened his lips for Job against his three friends. We ought indeed to
leave all controversies to be determined by the judgment of God, which
we are sure is according to truth; but those are not always in the right
who are most forward to appeal to that judgment and prejudge it against
their antagonists. Zophar despairs to convince Job himself, and
therefore desires God would convince him of two things which it is good
for every one of us duly to consider, and under all our afflictions
cheerfully to confess:-

`1.` The unsearchable depth of God\'s counsels. Zophar cannot pretend to
do it, but he desires that God himself would show Job so much of the
secrets of the divine wisdom as might convince him that they are at
least double to that which is, v. 6. Note, `(1.)` There are secrets in the
divine wisdom, arcana imperii-state-secrets. God\'s way is in the sea.
Clouds and darkness are round about him. He has reasons of state which
we cannot fathom and must not pry into. `(2.)` What we know of God is
nothing to what we cannot know. What is hidden is more than double to
what appears, Eph. 3:9. `(3.)` By employing ourselves in adoring the depth
of those divine counsels of which we cannot find the bottom we shall
very much tranquilize our minds under the afflicting hand of God. `(4.)`
God knows a great deal more evil of us than we do of ourselves; so some
understand it. When God gave David a sight and sense of sin he said that
he had in the hidden part made him to know wisdom, Ps. 51:6.

`2.` The unexceptionable justice of his proceedings. \"Know therefore
that, how sore soever the correction is that thou art under, God
exacteth of thee less than thy iniquity deserves,\" or (as some read
it), \"he remits thee part of thy iniquity, and does not deal with thee
according to the full demerit of it.\" Note, `(1.)` When the debt of duty
is not paid it is justice to insist upon the debt of punishment. `(2.)`
Whatever punishment is inflicted upon us in this world we must own that
it is less than our iniquities deserve, and therefore, instead of
complaining of our troubles, we must be thankful that we are out of
hell, Lam. 3:39; Ps. 103:10.

### Verses 7-12

Zophar here speaks very good things concerning God and his greatness and
glory, concerning man and his vanity and folly: these two compared
together, and duly considered, will have a powerful influence upon our
submission to all the dispensations of the divine Providence.

`I.` See here what God is, and let him be adored.

`1.` He is an incomprehensible Being, infinite and immense, whose nature
and perfections our finite understandings cannot possibly form any
adequate conceptions of, and whose counsels and actings we cannot
therefore, without the greatest presumption, pass a judgment upon. We
that are so little acquainted with the divine nature are incompetent
judges of the divine providence; and, when we censure the dispensations
of it, we talk of things that we do not understand. We cannot find out
God; how dare we then find fault with him? Zophar here shows, `(1.)` That
God\'s nature infinitely exceeds the capacities of our understandings:
\"Canst thou find out God, find him out to perfection? No, What canst
thou do? What canst thou know?\" v. 7, 8. Thou, a poor, weak,
short-sighted creature, a worm of the earth, that art but of yesterday?
Thou, though ever so inquisitive after him, ever so desirous and
industrious to find him out, yet darest thou attempt the search, or
canst thou hope to speed in it? We may, by searching find God (Acts
17:27), but we cannot find him out in any thing he is pleased to
conceal; we may apprehend him, but we cannot comprehend him; we may know
that he is, but cannot know what he is. The eye can see the ocean but
not see over it. We may, by a humble, diligent, and believing search,
find out something of God, but cannot find him out to perfection; we may
know, but cannot know fully, what God is, nor find out his work from the
beginning to the end, Eccl. 3:11. Note, God is unsearchable. The ages of
his eternity cannot be numbered, nor the spaces of his immensity
measured; the depths of his wisdom cannot be fathomed, nor the reaches
of his power bounded; the brightness of his glory can never be
described, nor the treasures of his goodness reckoned up. This is a good
reason why we should always speak of God with humility and caution and
never prescribe to him nor quarrel with him, why we should be thankful
for what he has revealed of himself and long to be where we shall see
him as he is, 1 Co. 13:9, 10. `(2.)` That it infinitely exceeds the limits
of the whole creation: It is higher than heaven (so some read it),
deeper than hell, the great abyss, longer than the earth, and broader
than the sea, many parts of which are to this day undiscovered, and more
were then. It is quite out of our reach to comprehend God\'s nature.
Such knowledge is too wonderful for us, Ps. 139:6. We cannot fathom
God\'s designs, nor find out the reasons of his proceedings. His
judgments are a great deep. Paul attributes such immeasurable dimensions
to the divine love as Zophar here attributes to the divine wisdom, and
yet recommends it to our acquaintance. Eph. 3:18, 19, That you may know
the breadth, and length, and depth, and height, of the love of Christ.

`2.` God is a sovereign Lord (v. 10): If he cut off by death (margin, If
he make a change, for death is a change; if he make a change in nations,
in families, in the posture of our affairs),-if he shut up in prison, or
in the net of affliction (Ps. 66:11),-if he seize any creature as a
hunter his prey, he will gather it (so bishop Patrick) and who shall
force him to restore? or if he gather together, as tares for the fire,
or if he gather to himself man\'s spirit and breath (ch. 34:14), then
who can hinder him? Who can either arrest the sentence or oppose the
execution? Who can control his power or arraign his wisdom and justice?
If he that made all out of nothing think fit to reduce all to nothing,
or to their first chaos again,-if he that separated between light and
darkness, dry land and sea, at first, please to gather them together
again,-if he that made unmakes, who can turn him away, alter his mind or
stay his hand, impede or impeach his proceedings?

`3.` God is a strict and just observer of the children of men (v. 11): He
knows vain men. We know little of him, but he knows us perfectly: He
sees wickedness also, not to approve it (Hab. 1:13), but to animadvert
upon it. `(1.)` He observes vain men. Such all are (every man, at his best
estate, is altogether vanity), and he considers it in his dealings with
them. He knows what the projects and hopes of vain men are, and can
blast and defeat them, the workings of their foolish fancies; he sits in
heaven, and laughs at them. He takes knowledge of the vanity of men
(that is, their little sins; so some) their vain thoughts and vain
words, and unsteadiness in that which is good. `(2.)` He observes bad men:
He sees gross wickedness also, though committed ever so secretly and
ever so artfully palliated and disguised. All the wickedness of the
wicked is naked and open before the all-seeing eye of God: Will he not
then consider it? Yes, certainly he will, and will reckon for it, though
for a time he seem to keep silence.

`II.` See here what man is, and let him be humbled, v. 12. God sees this
concerning vain man that he would be wise, would be thought so, though
he is born like a wild ass\'s colt, so sottish and foolish, unteachable
and untameable. See what man is. 1. He is a vain creature-empty; so the
word is. God made him full, but he emptied himself, impoverished
himself, and now he is raca, a creature that has nothing in him. 2. He
is a foolish creature, has become like the beasts that perish (Ps.
49:20, 73:22), an idiot, born like an ass, the most stupid animal, an
ass\'s colt, not yet brought to any service. If ever he come to be good
for any thing, it is owing to the grace of Christ, who once, in the day
of his triumph, served himself by an ass\'s colt. 3. He is a wilful
ungovernable creature. An ass\'s colt may be made good for something,
but the wild ass\'s colt will never be reclaimed, nor regards the crying
of the driver. See Job 39:5-7. Man thinks himself as much at liberty,
and his own master, as the wild ass\'s colt does, that is used to the
wilderness (Jer. 2:24), eager to gratify his own appetites and passions.
4. Yet he is a proud creature and self-conceited. He would be wise,
would he thought so, values himself upon the honour of wisdom, though he
will not submit to the laws of wisdom. He would be wise, that is, he
reaches after forbidden wisdom, and, like his first parents, aiming to
be wise above what is written, loses the tree of life for the tree of
knowledge. Now is such a creature as this fit to contend with God or
call him to an account? Did we but better know God and ourselves, we
should better know how to conduct ourselves towards God.

### Verses 13-20

Zophar, as the other two, here encourages Job to hope for better times
if he would but come to a better temper.

`I.` He gives him good counsel (v. 13, 14), as Eliphaz did (ch. 5:8), and
Bildad, ch. 8:5. He would have him repent and return to God. Observe the
steps of that return. 1. He must look within, and get his mind changed
and the tree made good. He must prepare his heart; there the work of
conversion and reformation must begin. The heart that wandered from God
must be reduced-that was defiled with sin and put into disorder must be
cleansed and put in order again-that was wavering and unfixed must be
settled and established; so the word here signifies. The heart is then
prepared to seek God when it is determined and fully resolved to make a
business of it and to go through with it. 2. He must look up, and
stretch out his hands towards God, that is, must stir up himself to take
hold on God, must pray to him with earnestness and importunity, striving
in prayer, and with expectation to receive mercy and grace from him. To
give the hand to the Lord signifies to yield ourselves to him and to
covenant with him, 2 Chr. 30:8. This Job must do, and, for the doing of
it, must prepare his heart. Job had prayed, but Zophar would have him to
pray in a better manner, not as an appellant, but as a petitioner and
humble suppliant. 3. He must amend what was amiss in his own
conversation, else his prayers would be ineffectual (v. 14): \"If
iniquity be in thy hand (that is, if there be any sin which thou dost
yet live in the practice of) put it far away, forsake it with
detestation and a holy indignation, stedfastly resolving not to return
to it, nor ever to have any thing more to do with it. Eze. 18:31; Hos.
14:9; Isa. 30:22. If any of the gains of iniquity, any goods gotten by
fraud or oppression, be in thy hand, make restitution thereof\" (as
Zaccheus, Lu. 19:8), \"and shake thy hands from holding them,\" Isa.
33:15. The guilt of sin is not removed if the gain of sin be not
restored. 4. He must do his utmost to reform his family too: \"Let not
wickedness dwell in thy tabernacles; let not thy house harbour or
shelter any wicked persons, any wicked practices, or any wealth gotten
by wickedness.\" He suspected that Job\'s great household had been
ill-governed, and that, where there were many, there were many wicked,
and the ruin of his family was the punishment of the wickedness of it;
and therefore, if he expected God should return to him, he must reform
what was amiss there, and, though wickedness might come into his
tabernacles, he must not suffer it to dwell there, Ps. 101:3, etc.

`II.` He assures him of comfort if he took this counsel, v. 15, etc. If
he would repent and reform, he should, without doubt, be easy and happy,
and all would be well. Perhaps Zophar might insinuate that, unless God
did speedily make such a change as this in his condition, he and his
friends would be confirmed in their opinion of him as a hypocrite and a
dissembler with God. A great truth, however, is conveyed, That, the work
of righteousness will be peace, and the effect of righteousness
quietness and assurance for ever, Isa. 32:17. Those that sincerely turn
to God may expect,

`1.` A holy confidence towards God: \"Then shalt thou lift up thy face
towards heaven without spot; thou mayest come boldly to the throne of
grace, and not with that terror and amazement expressed,\" ch. 9:34. If
our hearts condemn us not for hypocrisy and impenitency, then have we
confidence in our approaches to God and expectations from him, 1 Jn.
3:21. If we are looked upon in the face of the anointed, our faces, that
were dejected, may be lifted up-that were polluted, being washed with
the blood of Christ, may be lifted up without spot. We may draw near in
full assurance of faith when we are sprinkled from an evil conscience,
Heb. 10:22. Some understand this of the clearing up of his credit before
men, Ps. 37:6. If we make our peace with God, we may with cheerfulness
look our friends in the face.

`2.` A holy composedness in themselves: Thou shalt be stedfast, and shalt
not fear, not be afraid of evil tidings, thy heart being fixed, Ps.
112:7. Job was now full of confusion (ch. 10:15), while he looked upon
God as his enemy and quarrelled with him; but Zophar assures him that,
if he would submit and humble himself, his mind would be composed, and
he would be freed from those frightful apprehensions he had of God,
which put him into such an agitation. The less we are frightened the
more we are fixed, and consequently the more fit we are for our services
and for our sufferings.

`3.` A comfortable reflection upon their past troubles (v. 16): \"Thou
shalt forget thy misery, as the mother forgets her travailing pains, for
joy that the child is born; thou shalt be perfectly freed from the
impressions it makes upon thee, and thou shalt remember it as waters
that pass away, or are poured out of a vessel, which leave no taste or
tincture behind them, as other liquors do. The wounds of thy present
affliction shall be perfectly healed, not only without a remaining scar,
but without a remaining pain.\" Job had endeavoured to forget his
complaint (ch. 9:27), but found he could not; his soul had still in
remembrance the wormwood and the gall: but here Zophar puts him in a way
to forget it; let him by faith and prayer bring his griefs and cares to
God, an leave them with him, and then he shall forget them. Where sin
sits heavily affliction sits lightly. If we duly remember our sins, we
shall, in comparison with them, forget our misery, much more if we
obtain the comfort of a sealed pardon and a settled peace. He whose
iniquity is forgiven shall not say, I am sick, but shall forget his
sickness, Isa. 33:24.

`4.` A comfortable prospect of their future peace. This Zophar here
thinks to please Job with, in answer to the many despairing expressions
he had used, as if it were to no purpose for him to hope ever to see
good days again in this world: \"Yea, but thou mayest\" (says Zophar)
\"and good nights too.\" A blessed change he here puts him in hopes of.

`(1.)` That though now his light was eclipsed it should shine out again,
and more brightly than ever (v. 17),-that even his setting sun should
out-shine his noon-day sun, and his evening be fair and clear as the
morning, in respect both of honour and pleasure.-that his light should
shine out of obscurity (Isa. 58:10), and the thick and dark cloud, from
behind which his sun should break forth, would serve as a foil to its
lustre,-that it should shine even in old age, and those evil days should
be good days to him. Note, Those that truly turn to God then begin to
shine forth; their path is as the shining light which increases, the
period of their day will be the perfection of it, and their evening to
this world will be their morning to a better.

`(2.)` That, though now he was in a continual fear and terror, he should
live in a holy rest and security, and find himself continually safe and
easy (v. 18): Thou shalt be secure, because there is hope. Note, Those
who have a good hope, through grace, in God, and of heaven, are
certainly safe, and have reason to be secure, how difficult soever the
times are through which they pass in this world. He that walks uprightly
may thus walk surely, because, though there are trouble and danger, yet
there is hope that all will be well at last. Hope is an anchor of the
soul, Heb. 6:19. \"Thou shalt dig about thee,\" that is, \"Thou shalt be
as safe as an army in its entrenchments.\" Those that submit to God\'s
government shall be taken under his protection, and then they are safe
both day and night. `[1.]` By day, when they employ themselves abroad:
\"Thou shalt dig in safety, thou and thy servants for thee, and not be
again set upon by the plunderers, who fell upon thy servants at
plough,\" ch. 1:14, 15. It is no part of the promised prosperity that he
should live in idleness, but that he should have a calling and follow
it, and, when he was about the business of it, should be under the
divine protection. Thou shalt dig and be safe, not rob and be safe,
revel and be safe. The way of duty is the way of safety. `[2.]` By
night, when they repose themselves at home: Thou shalt take thy rest
(and the sleep of the labouring man is sweet) in safety, notwithstanding
the dangers of the darkness. The pillar of cloud by day shall be a
pillar of fire by night: \"Thou shalt lie down (v. 19), not forced to
wander where there is no place to lay thy head on, nor forced to watch
and sit up in expectation of assaults; but thou shalt go to bed at
bedtime, and not only shall none hurt thee, but none shall make thee
afraid nor so much as give thee an alarm.\" Note, It is a great mercy to
have quiet nights and undisturbed sleeps; those say so that are within
the hearing of the noise of war. And the way to be quiet is to seek unto
God and keep ourselves in his love. Nothing needs make those afraid who
return to God as their rest and take him for their habitation.

`(3.)` That, though now he was slighted, yet he should be courted: \"Many
shall make suit to thee, and think it their interest to secure thy
friendship.\" Suit is made to those that are eminently wise or reputed
to be so, that are very rich or in power. Zophar knew Job so well that
he foresaw that, how low soever this present ebb was, if once the tide
turned, it would flow as high as ever; and he would be again the darling
of his country. Those that rightly make suit to God will probably see
the day when others will make suit to them, as the foolish virgins to
the wise, Give us of your oil.

`III.` Zophar concludes with a brief account of the doom of wicked people
(v. 20): But the eyes of the wicked shall fail. It should seem, he
suspected that Job would not take his counsel, and here tells him what
would then come of it, setting death as well as life before him. See
what will become of those who persist in their wickedness, and will not
be reformed. 1. They shall not reach the good they flatter themselves
with the hopes of in this world and in the other. Disappointments will
be their doom, their shame, their endless torment. Their eyes shall fail
with expecting that which will never come. When a wicked man dies his
expectation perishes, Prov. 11:7. Their hope shall be as a puff of
breath (margin), vanished and gone past recall. Or their hope will
perish and expire as a man does when he gives up the ghost; it will fail
them when they have most need of it and when they expected the
accomplishment of it; it will die away, and leave them in utter
confusion. 2. They shall not avoid the evil which sometimes they
frighten themselves with the apprehensions of. They shall not escape the
execution of the sentence passed upon them, can neither out-brave it nor
outrun it. Those that will not fly to God will find it in vain to think
of flying from him.
