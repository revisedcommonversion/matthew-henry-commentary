Job, Chapter 36
===============

Commentary
----------

Elihu, having largely reproved Job for some of his unadvised speeches,
which Job had nothing to say in the vindication of, here comes more
generally to set him to rights in his notions of God\'s dealings with
him. His other friends had stood to it that, because he was a wicked
man, therefore his afflictions were so great and so long. But Elihu only
maintained that the affliction was sent for his trial, and that
therefore it was lengthened out because Job was not, as yet, thoroughly
humbled under it, nor had duly accommodated himself to it. He urges many
reasons, taken from the wisdom and righteousness of God, his care of his
people, and especially his greatness and almighty power, with which, in
this and the following chapter, he persuades him to submit to the hand
of God. Here we have, `I.` His preface, (v. 2-4). `II.` The account he gives
of the methods of God\'s providence towards the children of men,
according as they conduct themselves (v. 5-15). `III.` The fair warning
and good counsel he gives to Job thereupon (v. 16-21). `IV.` His
demonstration of God\'s sovereignty and omnipotence, which he gives
instances of in the operations of common providence, and which is a
reason why we should all submit to him in his dealings with us (v.
22-33). This he prosecutes and enlarges upon in the following chapter.

### Verses 1-4

Once more Elihu begs the patience of the auditory, and Job\'s
particularly, for he has not said all that he has to say, but he will
not detain them long. Stand about me a little (so some read it), v. 2.
\"Let me have your attendance, your attention, awhile longer, and I will
speak but this once, as plainly and as much to the purpose as I can.\"
To gain this he pleads, 1. That he had a good cause, and a noble and
very fruitful subject: I have yet to speak on God\'s behalf. He spoke as
an advocate for God, and therefore might justly expect the ear of the
court. Some indeed pretend to speak on God\'s behalf who really speak
for themselves; but those who sincerely appear in the cause of God, and
speak in behalf of his honour, his truths, his ways, his people, shall
be sure neither to want instructions (it shall be given them in that
same hour what they shall speak) nor to lose their cause or their fee.
Nor need they fear lest they should exhaust their subject. Those that
have spoken ever so much may yet find more to be spoken on God\'s
behalf. 2. That he had something to offer that was uncommon, and out of
the road of vulgar observation: I will fetch my knowledge from afar (v.
3), that is, \"we will have recourse to our first principles and the
highest notions we can make use of to serve any purpose.\" It is worth
while to go far for this knowledge of God, to dig for it, to travel for
it; it will recompense our pains, and, though far-fetched, is not
dear-bought. 3. That his design was undeniably honest; for all he aimed
at was to ascribe righteousness to his Maker, to maintain and clear this
truth, that God is righteous in all his ways. In speaking of God, and
speaking for him, it is good to remember that he is our Maker, to call
him so, and therefore to be ready to do him and the interests of his
kingdom the best service we can. If he be our Maker, we have our all
from him, must use our all for him, and be very jealous for his honour.
That his management should be very just and fair (v. 4): \"My words
shall not be false, neither disagreeable to the thing itself nor to my
own thoughts and apprehensions. It is truth that I am contending for,
and that for truth\'s sake, with all possible sincerity and plainness.\"
He will make use of plain and solid arguments and not the subtleties and
niceties of the schools. \"He who is perfect or upright in knowledge is
now reasoning with thee; and therefore let him not only have a fair
hearing, but let what he says be taken in good part, as meant well.\"
The perfection of our knowledge in this world is to be honest and
sincere in searching out truth, in applying it to ourselves, and in
making use of what we know for the good of others.

### Verses 5-14

Elihu, being to speak on God\'s behalf, and particularly to ascribe
righteousness to his Maker, here shows that the disposals of divine
Providence are all, not only according to the eternal counsels of his
will, but according to the eternal rules of equity. God acts as a
righteous governor, for,

`I.` He does not think it below him to take notice of the meanest of his
subjects, nor does poverty or obscurity set any at a distance from his
favour. If men are mighty, they are apt to look with a haughty disdain
upon those that are not of distinction and make no figure; but God is
mighty, infinitely so, and yet he despises not any, v. 5. He humbles
himself to take cognizance of the affairs of the meanest, to do them
justice and to show them kindness. Job thought himself and his cause
slighted because God did not immediately appear for him. \"No,\" says
Elihu, God despises not any, which is a good reason why we should honour
all men. He is mighty in strength and wisdom, and yet does not look with
contempt upon those that have but a little strength and wisdom, if they
but mean honestly. Nay, for this reason he despises not any, because his
wisdom and strength are incontestably infinite and therefore the
condescensions of his grace can be no diminution to him. Those that are
wise and good will not look upon any with scorn and disdain.

`II.` He gives no countenance to the greatest, if they be bad (v. 6): He
preserves not the life of the wicked. Though their life may be
prolonged, yet not under any special care of the divine Providence, but
only its common protection. Job had said that the wicked live, become
old, and are mighty in power, ch. 21:7. \"No,\" says Elihu: \"he seldom
suffers wicked men to become old. He preserves not their life so long as
they expected, nor with that comfort and satisfaction which are indeed
our life; and their preservation is but a reservation for the day of
wrath,\" Rom. 2:5.

`III.` He is always ready to right those that are any way injured, and to
plead their cause (v. 6): He gives right to the poor, avenges their
quarrel upon their persecutors and forces them to make restitution of
what they have robbed them of. If men will not right the injured poor,
God will.

`IV.` He takes a particular care for the protection of his good subjects,
v. 7. He not only looks on them, but he never looks off them: He
withdraws not his eyes from the righteous. Though they may seem
sometimes neglected and forgotten, and that befals them which looks like
an oversight of Providence, yet tender careful eye of their heavenly
Father never withdraws from them. If our eye be ever towards God in
duty, his eye will be ever upon us in mercy, and, when we are at the
lowest, will not overlook us.

`1.` Sometimes he prefers good people to places of trust and honour (v.
7): With kings are they on the throne, and every sheaf is made to bow to
theirs. When righteous persons are advanced to places of honour and
power, it is in mercy to them; for God\'s grace in them will both arm
them against the temptations that attend preferment and enable them to
improve the opportunity it gives them of doing good. It is also in mercy
to those over whom they are set: When the righteous bear rule the city
rejoices. If the righteous be advanced, they are established. Those that
in honour keep a good conscience stand upon sure ground, and high places
are not such slippery ground to them as they are to others. But, because
it is not often that we see good men made great men in this world, this
may be supposed to refer to the honour to which the righteous shall rise
when their Redeemer shall stand at the latter day upon the earth; for
then only they shall be exalted for ever, and established for ever; then
shall they all shine forth as the sun, and be made kings and priests to
our God.

`2.` If at any time he bring them into affliction, it is for the good of
their souls, v. 8-10. Some good people are preferred to honour and
power, but others are in trouble. Now observe, `(1.)` The distress
supposed (v. 8): If they be bound in fetters, laid in prison as Joseph
was, or holden in the cords of any other affliction, confined by pain
and sickness, hampered by poverty, bound in their counsels, and,
notwithstanding all their struggles, held long in this distress. This
was Job\'s case; he was caught, and kept fast, in the cords of anguish
(as some read it); but observe, `(2.)` The design God has, in bringing his
people into such distresses as these; it is for the benefit of their
souls, the consideration of which should reconcile us to affliction and
make us think well of it. Three things God intends when he afflicts
us:-`[1.]` To discover past sins to us, and to bring them to our
remembrance. Then he shows them that amiss in them which before they did
not see. He discovers to them the fact of sin: He shows them their work.
Sin is our own work. If there be any good in us, it is God\'s work; and
we are concerned to see what work we have made by sin. He discovers the
fault of sin, shows them their transgressions of the law of God, and
withal the sinfulness of sin, that they have exceeded, and have been
beyond measure sinful. True penitents lay a load upon themselves, do not
extenuate, but aggravate, their sins, and own that they have exceeded in
them. Affliction sometimes answers to the sin; it serves, however, to
awaken the conscience and puts men upon considering. `[2.]` To dispose
our hearts to receive present instructions: Then he opens their ear to
discipline, v. 10. Whom God chastens he teaches (Ps. 94:12), and the
affliction makes people willing to learn, softens the wax, that it may
receive the impression of the seal; yet it does not do this of itself,
but the grace of God working with and by it; it is he that opens the
ear, that opens the heart, who has the key of David. `[3.]` To deter and
draw us off from iniquity for the future. This is the errand on which
the affliction is sent; it is a command to return from iniquity, to have
no more to do with sin, to turn from it with an aversion to it and a
resolution never to return to it any more, Hos. 14:8.

`3.` If the affliction do its work, and accomplish that for which it is
sent, he will comfort them again, according to the time that he has
afflicted them (v. 11): If they obey and serve him,-if they comply with
his design and serve his purpose in these dispensations,-if, when the
affliction is removed, they continue in the same good mind that they
were in when they were under the smart of it and perform the vows they
made then,-if they live in obedience to God\'s commands, particularly
those which relate to his service and worship, and in all instances make
conscience of their duty to him,-then they shall spend their days in
prosperity again and their years in true pleasures. Piety is the only
sure way to prosperity and pleasure; this is a certain truth, and yet
few will believe it. If we faithfully serve God, `(1.)` We have the
promise of outward prosperity, the promise of the life that now is, and
the comforts of it, as far as is for God\'s glory and our good; and who
would desire them any further? `(2.)` We have the possession of inward
pleasures, the comfort of communion with God and a good conscience, and
that great peace which those have that love God\'s law. If we rejoice
not in the Lord always, and in hope of eternal life, it is our own
fault; and what better pleasures can we spend our years in?

`4.` If the affliction do not do its work, let them expect the furnace to
be heated seven times hotter till they are consumed (v. 12): If they
obey not, if they are not bettered by their afflictions, are not
reclaimed and reformed, they shall perish by the sword of God\'s wrath.
Those whom his rod does not cure his sword will kill; and the consuming
fire will prevail if the refining fire do not; for when God judges he
will overcome. If Ahaz, in his distress, trespass yet more against the
Lord, this is that king Ahaz that is marked for ruin, 2 Chr. 28:22; Jer.
6:29, 30. God would have instructed them by their afflictions, but they
received not instruction, would not take the hints that were given them;
and therefore they shall die without knowledge, ere they are aware,
without any further previous notices given them; or they shall die
because they were without knowledge notwithstanding the means of
knowledge which they were blessed with. Those that die without knowledge
die without grace and are undone for ever.

`V.` He brings ruin upon hypocrites, the secret enemies of his kingdom
(such as Elihu described, v. 12), who, though they were numbered among
the righteous whom Elihu had spoken of before, yet did not obey God,
but, being children of disobedience and darkness, become children of
wrath and perdition; these are the hypocrites in heart, who heap up
wrath, v. 13. See the nature of hypocrisy: it lies in the heart, which
is for the world and the flesh when the outside seems to be for God and
religion. Many that are saints in show and saints in word are hypocrites
in heart. That spring is corrupt, and there is an evil treasure there.
See the mischievousness of hypocrisy: hypocrites heap up wrath. They are
doing that every day which is provoking to God, and will be reckoned
with for it all together in the great day. They treasure up wrath
against the day of wrath, Rom. 2:5. Their sins are laid up in store with
God among his treasures, Deu. 32:34. Compare Jam. 5:3. As what goes up a
vapour comes down a shower, so what goes up sin, if not repented of,
will come down wrath. They think they are heaping up wealth, heaping up
merits, but, when the treasures are opened, it will prove they were
heaping up wrath. Observe, 1. What they do to heap up wrath. What is it
that is so provoking? It is this, They cry not when he binds them, that
is, when they are in affliction, bound with the cords of trouble, their
hearts are hardened, they are stubborn and unhumbled, and will not cry
to God nor make their application to him. They are stupid and senseless
as stocks and stones, despising the chastening of the Lord. 2. What are
the effects of that wrath? They die in youth, and their life is among
the unclean, v. 14. This is the portion of hypocrites, whom Christ
denounced many woes against. If they continue impenitent, `(1.)` They
shall die a sudden death, die in youth, when death is most a surprise,
and death (that is, the consequence of it) is always such to hypocrites;
as those that die in youth die when they hoped to live, so hypocrites,
at death, go to hell, when they hoped to go to heaven. When a wicked man
dies his expectations shall perish. `(2.)` They shall die the second
death. Their life, after death (for so it comes in here), is among the
unclean, among the fornicators (so some), among the worst and vilest of
sinners, notwithstanding their specious and plausible profession. It is
among the Sodomites (so the margin), those filthy wretches, who going
after strange flesh, are set forth for an example, suffering the
vengeance of eternal fire, Jude 7. The souls of the wicked live after
death, but they live among the unclean, the unclean spirits, the devil
and his angels, forever separated from the new Jerusalem, into which no
unclean thing shall enter.

### Verses 15-23

Elihu here comes more closely to Job; and,

`I.` He tells him what God would have done for him before this if he had
been duly humbled under his affliction. \"We all know how ready God is
to deliver the poor in his affliction (v. 15); he always was so. The
poor in spirit, those that are of a broken and contrite heart, he looks
upon with tenderness, and, when they are in affliction, is ready to help
them. He opens their ears, and makes them to hear joy and gladness, even
in their oppressions; while he does not yet deliver them he speaks to
them good words and comfortable words, for the encouragement of their
faith and patience, the silencing of their fears, and the balancing of
their griefs; and even so (v. 16) would he have done to thee if thou
hadst submitted to his providence and conducted thyself well; he would
have delivered and comforted thee, and we should have had none of these
complaints. If thou hadst accommodated thyself to the will of God, thy
liberty and plenty would have been restored to thee with advantage.\" 1.
\"Thou wouldst have been enlarged, and not confined thus by thy sickness
and disgrace: He would have removed thee into a broad place where is no
straitness, and thou wouldst no longer have been cramped thus and have
had all thy measures broken.\" 2. \"Thou wouldst have been enriched, and
wouldst not have been left in this poor condition; thou wouldst have had
thy table richly spread, not only with food convenient, but with the
finest of the wheat\" (see Deu. 32:14) \"and the fattest of the flesh.\"
Note, It ought to silence us under our afflictions to consider that, if
we were better, it would be every way better with us: if we had answered
the ends of an affliction, the affliction would be removed; and
deliverance would come if we were ready for it. God would have done well
for us if we had conducted ourselves well; Ps. 81:13, 14; Isa. 48:18.

`II.` He charges him with standing in his own light, and makes him the
cause of the continuance of his own trouble (v. 17): \"But thou hast
fulfilled the judgment of the wicked,\" that is, \"Whatever thou art
really, in this thing thou hast conducted thyself like a wicked man,
hast spoken and done like the wicked, hast gratified them and served
their cause; and therefore judgment and justice take hold on thee as a
wicked man, because thou goest in company with them, actest as if thou
wert in their interest, aiding and abetting. Thou hast maintained the
cause of the wicked; and such as a man\'s cause is such will the
judgment of God be upon him;\" so bishop Patrick. It is dangerous being
on the wrong side: accessaries to treason will be dealt with as
principals.

`III.` He cautions him not to persist in his frowardness. Several good
cautions he gives him to this purport.

`1.` Let him not make light of divine vengeance, nor be secure, as if he
were in no danger of it (v. 18): \"Because there is wrath\" (that is,
\"because God is a righteous governor, who resents all the affronts
given to his government, because he has revealed his wrath from heaven
against all ungodliness and unrighteousness of men, and because thou
hast reason to fear that thou art under God\'s displeasure) therefore
beware lest he take thee away suddenly with his stroke, and be so wise
as to make thy peace with him quickly and get his anger turned away from
thee.\" A warning to this purport Job had given his friends (ch. 19:29):
Be you afraid of the sword, for wrath brings the punishment of the
sword. Thus contenders are apt, with too much boldness, to bind one
another over to the judgment of God and threaten one another with his
wrath; but he that keeps a good conscience needs not fear the impotent
menaces of proud men. But his was a friendly caution to Job, and
necessary. Even good men have need to be kept to their duty by the fear
of God\'s wrath. \"Thou art a wise and good man, but beware lest he take
thee away, for the wisest and best have enough in them to deserve his
stroke.\"

`2.` Let him not promise himself that, if God\'s wrath should kindle
against him, he could find out ways to escape the strokes of it. `(1.)`
There is no escaping by money, no purchasing a pardon with silver, or
gold, and such corruptible things: \"Even a great ransom cannot deliver
thee when God enters into judgment with thee. His justice cannot be
bribed, nor any of the ministers of his justice. Will he esteem thy
riches, and take from them a commutation of the punishment? No, not
gold, v. 19. If thou hadst as much wealth as ever thou hadst, that would
not ease thee, would not secure thee from the strokes of God\'s wrath,
in the day of the revelation of which riches profit not,\" Prov. 11:4.
See Ps. 49:7, 8. `(2.)` There is no escaping by rescue: \"If all the
forces of strength were at thy command, if thou couldst muster ever so
many servants and vassals to appear for thee to force thee out of the
hands of divine vengeance, it were all in vain; God would not regard it.
There is none that can deliver out of his hand.\" `(3.)` There is no
escaping by absconding (v. 20): \"Desire not the night, which often
favours the retreat of a conquered army and covers it; think not that
thou canst so escape the righteous judgment of God, for the darkness
hideth not from him,\" Ps. 139:11, 12. See ch. 34:22. \"Think not,
because in the night people retire to their place, go up to their beds,
and it is then easy to escape being discovered by them, that God also
ascends to his place, and cannot see thee. No; he neither slumbers nor
sleeps. His eyes are open upon the children of men, not only in all
places, but at all times. No rocks nor mountains can shelter us from his
eye.\" Some understand it of the night of death; that is the night by
which men are cut off from their place, and Job had earnestly breathed
for that night, as the hireling desires the evening, ch. 7:2. \"But do
not do so,\" says Elihu; \"for thou knowest not what the night of death
is.\" Those that passionately wish for death, in hopes to make that
their shelter from God\'s wrath, may perhaps be mistaken. There are
those whom wrath pursues into that night.

`3.` Let him not continue his unjust quarrel with God and his providence,
which hitherto he had persisted in when he should have submitted to the
affliction (v. 21): \"Take heed, look well to thy own spirit, and regard
not iniquity, return not to it (so some), for it is at thy peril if thou
do.\" Let us never dare to think a favourable thought of sin, never
indulge it, nor allow ourselves in it. Elihu thinks Job had need of this
caution, he having chosen iniquity rather than affliction, that is,
having chosen rather to gratify his own pride and humour in contending
with God than to mortify it by a submission to him and accepting the
punishment. We may take it more generally, and observe that those who
choose iniquity rather than affliction make a very foolish choice. Those
that ease their cares by sinful pleasures, increase their wealth by
sinful pursuits, escape their troubles by sinful projects, and evade
sufferings for righteousness\' sake by sinful compliances against their
consciences, make a choice they will repent of; for there is more evil
in the least sin than in the greatest affliction. It is an evil, and
only evil.

`4.` Let him not dare to prescribe to God, nor give him his measures (v.
22, 23): \"Behold, God exalteth by his power,\" that is, \"He does, may,
and can set up and pull down whom he pleases, and therefore it is not
for thee nor me to contend with him.\" The more we magnify God the more
do we humble and abase ourselves. Now consider, `(1.)` That God is an
absolute sovereign: He exalts by his own power, and not by strength
derived from any other. He exalts whom he pleases, exalts those that
were afflicted and cast down, by the strength and power which he gives
his people; and therefore who has enjoined him his way? Who presides
above him in his way? Is there any superior from whom he has his
commission and to whom he is accountable? No; he himself is supreme and
independent. Who puts him in mind of his way? so some. Does the eternal
Mind need a remembrancer? No; his own way, as well as ours, is ever
before him. He has not received orders or instructions from any (Isa.
60:13, 14), nor is he accountable to any. He enjoins to all the
creatures their way; let not us then enjoin him his, but leave it to him
to govern the world, who is fit to do it. `(2.)` That he is an
incomparable teacher: Who teaches like him? It is absurd for us to teach
him who is himself the fountain of light, truth, knowledge, and
instruction. He that teaches man knowledge, and so as none else can,
shall not he know? Ps. 94:9, 10. Shall we light a candle to the sun?
Observe, When Elihu would give glory to God as a ruler he praises him as
a teacher, for rulers must teach. God does so. He binds with the cords
of a man. In this, as in other things, he is unequalled. None so fit to
direct his own actions as he himself is. He knows what he has to do, and
how to do it for the best, and needs no information nor advice. Solomon
himself had a privy-council to advise him, but the King of kings has
none. Nor is any so fit to direct our actions as he is. None teaches
with such authority and convincing evidence, with such condescension and
compassion, nor with such power and efficacy, as God does. He teaches by
the Bible, and that is the best book, teaches by his Son, and he is the
best Master. `(3.)` That he is unexceptionably just in all his
proceedings: Who can say, Thou hast wrought iniquity? Not, Who dares say
it? (many do iniquity, and those who tell them of it do so at their
peril), but Who can say it? Who has any cause to say it? Who can say it
and prove it? It is a maxim undoubtedly true, without limitation, that
the King of kings can do no wrong.

### Verses 24-33

Elihu is here endeavouring to possess Job with great and high thoughts
of God, and so to persuade him into a cheerful submission to his
providence.

`I.` He represents the work of God, in general, as illustrious and
conspicuous, v. 24. His whole work is so. God does nothing mean. This is
a good reason why we should acquiesce in all the operations of his
providence concerning us in particular. His visible works, those of
nature, and which concern the world in general, are such as we admire
and commend, and in which we observe the Creator\'s wisdom, power, and
goodness; shall we then find fault with his dispensations concerning us,
and the counsels of his will concerning our affairs? We are here called
to consider the work of God, Eccl. 7:13. 1. It is plain before our eyes,
nothing more obvious: it is what men behold. Every man that has but half
an eye may see it, may behold it afar off. Look which way we will, we
see the productions of God\'s wisdom and power; we see that done, and
that doing, concerning which we cannot but say, This is the work of God,
the finger of God; it is the Lord\'s doing. Every man may see, afar off,
the heaven and all its lights, the earth and all its fruits, to be the
work of Omnipotence; much more when we behold them nigh at hand. Look at
the minutest works of nature through a microscope; do they not appear
curious? The eternal power and godhead of the Creator are clearly seen
and understood by the things that are made, Rom. 1:20. Every man, even
those that have not the benefit of divine revelation, may see this; for
there is no speech or language where the voice of these natural constant
preachers is not heard, Ps. 19:3. 2. It ought to be marvellous in our
eyes. The beauty and excellency of the work of God, and the agreement of
all the parts of it, are what we must remember to magnify and highly to
extol, not only justify it as right and good, and what cannot be blamed,
but magnify it as wise and glorious, and such as no creature could
contrive or produce. Man may see his works, and is capable of discerning
his hand in them (which the beasts are not), and therefore ought to
praise them and give him the glory of them.

`II.` He represents God, the author of them, as infinite and
unsearchable, v. 26. The streams of being, power, and perfection should
lead us to the fountain. God is great, infinitely so,-great in power,
for he is omnipotent and independent,-great in wealth, for he is
self-sufficient and all-sufficient,-great in himself,-great in all his
works,-great, and therefore greatly to be praised,-great, and therefore
we know him not. We know that he is, but not what he is. We know what he
is not, but not what he is. We know in part, but not in perfection. This
comes in here as a reason why we must not arraign his proceedings, nor
find fault with what he does, because it is speaking evil of the things
that we understand not and answering a matter before we hear if. We know
not the duration of his existence, for it is infinite. The number of his
years cannot possibly be searched out, for he is eternal; there is no
number of them. He is a Being without beginning, succession, or period,
who ever was, and ever will be, and ever the same, the great I AM. This
is a good reason why we should not prescribe to him, nor quarrel with
him, because, as he is, such are his operations, quite out of our reach.

`III.` He gives some instances of God\'s wisdom, power, and sovereign
dominion, in the works of nature and the dispensations of common
providence, beginning in this chapter with the clouds and the rain that
descends from them. We need not be critical in examining either the
phrase or the philosophy of this noble discourse. The general scope of
it is to show that God is infinitely great, and the Lord of all, the
first cause and supreme director of all the creatures, and has all power
in heaven and earth (whom therefore we ought, with all humility and
reverence, to adore, to speak well of, and to give honour to), and that
it is presumption for us to prescribe to him the rules and methods of
his special providence towards the children of men, or to expect from
him an account of them, when the operations even of common providences
about the meteors are so various and so mysterious and unaccountable.
Elihu, to affect Job with God\'s sublimity and sovereignty, had directed
him (ch. 35:5) to look unto the clouds. In these verses he shows us what
we may observe in the clouds we see which will lead us to consider the
glorious perfections of their Creator. Consider the clouds,

`1.` As springs to this lower world, the source and treasure of its
moisture, and the great bank through which it circulates-a very
necessary provision, for its stagnation would be as hurtful to this
lower world as that of the blood to the body of man. It is worth while
to observe in this common occurrence, `(1.)` That the clouds above distil
upon the earth below. If the heavens become brass, the earth becomes
iron; therefore thus the promise of plenty runs, I will hear the heavens
and they shall hear the earth. This intimates to us that every good gift
is from above, from him who is both Father of lights and Father of the
rain, and it instructs us to direct our prayers to him and to look up.
`(2.)` That they are here said to distil upon man (v. 28); for, though
indeed God causes it to rain in the wilderness where no man is (ch.
38:26, Ps. 104:11), yet special respect is had to man herein, to whom
the inferior creatures are all made serviceable and from whom the actual
return of the tribute of praise is required. Among men, he causes his
rain to fall upon the just and upon the unjust, Mt. 5:45. `(3.)` They are
said to distil the water in small drops, not in spouts, as when the
windows of heaven were opened, Gen. 7:11. God waters the earth with that
with which he once drowned it, only dispensing it in another manner, to
let us know how much we lie at his mercy, and how kind he is, in giving
rain by drops, that the benefit of it may be the further and the more
equally diffused, as by an artificial water-pot. `(4.)` Though sometimes
the rain comes in very small drops, yet, at other times, it pours down
in great rain, and this difference between one shower and another must
be resolved into the divine Providence which orders it so. `(5.)` Though
it comes down in drops, yet it distils upon man abundantly (v. 28), and
therefore is called the river of God which is full of water, Ps. 65:9.
`(6.)` The clouds pour down according to the vapour that they draw up, v.
27. So just the heavens are to the earth, but the earth is not so in the
return it makes. `(7.)` The produce of the clouds is sometimes a great
terror, and at other times a great favour, to the earth, v. 31. When he
pleases by them he judges the people he is angry with. Storms, and
tempests, and excessive rains, destroying the fruits of the earth and
causing inundations, come from the clouds; but, on the other hand, from
them, usually, he gives meat in abundance; they drop fatness upon the
pastures that are clothed with flocks, and the valleys that are covered
with corn, Ps. 65:11-13. `(8.)` Notice is sometimes given of the approach
of rain, v. 33. The noise thereof, among other things, shows concerning
it. Hence we read (1 Ki. 18:41) of the sound of abundance of rain, or
(as it is in the margin) a sound of a noise of rain, before it came; and
a welcome harbinger it was then. As the noise, so the face of the sky,
shows concerning it, Lu. 12:56. The cattle also, by a strange instinct,
are apprehensive of a change in the weather nigh at hand, and seek for
shelter, shaming man, who will not foresee the evil and hide himself.

`2.` As shadows to the upper world (v. 29): Can any understand the
spreading of the clouds? They are spread over the earth as a curtain or
canopy; how they come to be so, how stretched out, and how poised, as
they are, we cannot understand, though we daily see they are so. Shall
we then pretend to understand the reasons and methods of God\'s judicial
proceedings with the children of men, whose characters and cases are so
various, when we cannot account for the spreadings of the clouds, which
cover the light? v. 32. It is a cloud coming betwixt, v. 32; ch. 26:9.
And this we are sensible of, that, by the interposition of the clouds
between us and the sun, we are, `(1.)` Sometimes favoured; for they serve
as an umbrella to shelter us from the violent heat of the sun, which
otherwise would beat upon us. A cloud of dew in the heat of harvest is
spoken of as a very great refreshment. Isa. 18:4. `(2.)` Sometimes we are
by them frowned upon; for they darken the earth at noon-day and eclipse
the light of the sun. Sin is compared to a cloud (Isa. 44:22), because
it comes between us and the light of God\'s countenance and obstructs
the shining of it. But though the clouds darken the sun for a time, and
pour down rain, yet (post nubila Phoebus-the sun shines forth after the
rain), after he has wearied the cloud, he spreads his light upon it, v.
30. There is a clear shining after rain, 2 Sa. 23:4. The sunbeams are
darted forth, and reach to cover even the bottom of the sea, thence to
exhale a fresh supply of vapours, and so raise recruits for the clouds,
v. 30. In all this, we must remember to magnify the work of God.
