Job, Chapter 32
===============

Commentary
----------

The stage is clear, for Job and his three friends have sat down, and
neither he nor they have anything more to say; it is therefore very
seasonable for a moderator to interpose, and Elihu is the man. In this
chapter we have, `I.` Some account of him, his parentage, his presence at
this dispute, and his sentiments concerning it (v. 1-5). `II.` The apology
he made for his bold undertaking to speak to a question which had been
so largely and learnedly argued by his seniors. He pleads, 1. That,
though he had not the experience of an old man, yet he had the
understanding of a man (v. 6-10). 2. That he had patiently heard all
they had to say (v. 11-13). 3. That he had something new to offer (v.
14-17). 4. That his mind was full of this matter, and it would be a
refreshment to him to give it vent (v. 18-20). 5. That he was resolved
to speak impartially (v. 21, 22). And he did speak so well to this
matter that Job made no reply to him, and God gave him no rebuke when he
checked both Job himself and his other three friends.

### Verses 1-5

Usually young men are the disputants and old men the moderators; but
here, when old men were the disputants, as a rebuke to them for their
unbecoming heat, a young man is raised up to be the moderator. Divers of
Job\'s friends were present, that came to visit him and to receive
instruction. Now here we have,

`I.` The reason why his three friends were now silent. They ceased to
answer him, and let him have his saying, because he was righteous in his
own eyes. This was the reason they gave why they said no more, because
it was to no purpose to argue with a man that was so opinionative, v. 1.
Those that are self-conceited are indeed hard to be wrought upon; there
is more hope of a fool (a fool of God\'s making) than of those who are
fools of their own making, Prov. 26:12. But they did not judge fairly
concerning Job: he was really righteous before God, and not righteous in
his own eyes only; so that it was only to save their own credit that
they made this the reason of their silence, as peevish disputants
commonly do when they find themselves run a-ground and are not willing
to own themselves unable to make their part good.

`II.` The reasons why Elihu, the fourth, now spoke. His name Elihu
signifies My God is he. They had all tried in vain to convince Job, but
my God is he that can and will do it, and did it at last: he only can
open the understanding. He is said to be a Buzite, from Buz, Nahor\'s
second son (Gen. 22:21), and of the kindred of Ram, that is, Aram (so
some), whence the Syrians or Aramites descended and were denominated,
Gen. 22:21. Of the kindred of Abram; so the Chaldee-paraphrase,
supposing him to be first called Ram-high, then Abram-a high father, and
lastly Abraham-the high father of a multitude. Elihu was not so well
known as the rest, and therefore is more particularly described thus.

`1.` Elihu spoke because he was angry and thought he had good cause to be
so. When he had made his observations upon the dispute he did not go
away and calumniate the disputants, striking them secretly with a
malicious censorious tongue, but what he had to say he would say before
their faces, that they might vindicate themselves if they could. `(1.)` He
was angry at Job, because he thought he did not speak so reverently of
God as he ought to have done; and that was too true (v. 2): He justified
himself more than God, that is, took more care and pains to clear
himself from the imputation of unrighteousness in being thus afflicted
than to clear God from the imputation of unrighteousness in afflicting
him, as if he were more concerned for his own honour than for God\'s;
whereas he should, in the first place, have justified God and cleared
his glory, and then he might well enough have left his own reputation to
shift for itself. Note, A gracious heart is jealous for the honour of
God, and cannot but be angry when that is neglected or postponed, or
when any injury is done it. Nor is it any breach of the law of meekness
to be angry at our friends when they are offensive to God. Get thee
behind me, Satan, says Christ to Simon. Elihu owned Job to be a good
man, and yet would not say as he said when he thought he said amiss: it
is too great a compliment to our friends not to tell them of their
faults. `(2.)` He was angry at his friends because he thought they had not
conducted themselves so charitably towards Job as they ought to have
done (v. 3): They had found no answer, and yet had condemned Job. They
had adjudged him to be a hypocrite, a wicked man, and would not recede
from that sentence concerning him; and yet they could not prove him so,
nor disprove the evidences he produced of his integrity. They could not
make good the premises, and yet held fast the conclusion. They had no
reply to make to his arguments, and yet they would not yield, but, right
or wrong, would run him down; and this was not fair. Seldom is a quarrel
begun, and more seldom is a quarrel carried on to the length that this
was, in which there is not a fault on both sides. Elihu, as became a
moderator, took part with neither, but was equally displeased with the
mistakes and mismanagement of both. Those that in good earnest seek for
truth must thus be impartial in their judgments concerning the
contenders, and not reject what is true and good on either side for the
sake of what is amiss, nor approve or defend what is amiss for the sake
of what is true and good, but must learn to separate between the
precious and the vile.

`2.` Elihu spoke because he thought that it was time to speak, and that
now, at length, it had come to his turn, v. 4, 5. `(1.)` He had waited on
Job\'s speeches, had patiently heard him out, until the words of Job
were ended. `(2.)` He had waited on his friends\' silence, so that, as he
would not interrupt him, so he would not prevent them, not because they
were wiser than he, but because they were older than he, and therefore
it was expected by the company that they should speak first; and Elihu
was very modest, and would by no means offer to abridge them of their
privilege. Some certain rules of precedency must be observed, for the
keeping of order. Though inward real honour will attend true wisdom and
worth, yet, since every man will think himself or his friend the wisest
and worthiest, this can afford no certain rule for the outward
ceremonial honour, which therefore must attend seniority either of age
or office; and this respect the seniors may the better require because
they paid it when they were juniors, and the juniors may the better pay
because they shall have it when they come to be seniors.

### Verses 6-14

Elihu here appears to have been,

`I.` A man of great modesty and humility. Though a young man, and a man of
abilities, yet not pert, and confident, and assuming: his face shone,
and, like Moses, he did not know it, which made it shine so much the
brighter. Let it be observed by all, especially by young people, as
worthy their imitation, 1. What a diffidence he had of himself and of
his own judgment (v. 6): \"I am young, and therefore I was afraid, and
durst not show you my opinion, for fear I should either prove mistaken
or do that which was unbecoming me.\" He was so observant of all that
passed, and applied his mind so closely to what he heard, that he had
formed in himself a judgment of it. He neither neglected it as foreign,
nor declined it as intricate; but, how clear soever the matter was to
himself, he was afraid to deliver his mind upon it, because he differed
in his sentiments from those that were older than he. Note, It becomes
us to be suspicious of our own judgment in matters of doubtful
disputation, to be swift to hear the sentiments of others and slow to
speak our own, especially when we go contrary to the judgment of those
for whom, upon the score of their learning and piety, we justly have a
veneration. 2. What a deference he paid to his seniors, and what great
expectations he had from them, (v. 7): I said, Days should speak. Note,
Age and experience give a man great advantage in judging of things, both
as they furnish a man with so much the more matter for his thoughts to
work upon and as they ripen and improve the facilities he is to work
with, which is a good reason why old people should take pains both to
learn themselves and to teach others (else the advantages of their age
are a reproach to them), and why young people should attend on their
instructions. It is a good lodging with an old disciple, Acts 21:16;
Tit. 2:4. Elihu\'s modesty appeared in the patient attention he gave to
what his seniors said, v. 11, 12. He waited for their words as one that
expected much from them, agreeably to the opinion he had of these grave
men. He gave ear to their reasons, that he might take their meaning, and
fully understand what was the drift of their discourse and what the
force of their arguments. He attended to them with diligence and care,
and this, `(1.)` Though they were slow, and took up a great deal of time
in searching out what to say. Though they had often to seek for matter
and words, paused and hesitated, and were unready at their work, yet he
overlooked that, and gave ear to their reasons, which, if really
convincing, he would not think the less so for the disadvantages of the
delivery of them. `(2.)` Though they trifled and made nothing of it,
though none of them answered Job\'s words nor said what was proper to
convince him, yet he attended to them, in hopes they would bring it to
some head at last. We must often be willing to hear what we do not like,
else we cannot prove all things. His patient attendance on their
discourses he pleads, `[1.]` As that which entitled him to a liberty of
speech in his turn and empowered him to require their attention. Hanc
veniam petimusque damusque vicissim-This liberty we mutually allow and
ask. Those that have heard may speak, and those that have learned may
teach. `[2.]` As that which enabled him to pass a judgment upon what
they had said. He had observed what they aimed at, and therefore knew
what to say to it. Let us be thoroughly apprized of the sentiments of
our brethren before we censure them; for he that answers a matter before
he hears it, or when he has heard it only by halves, it is folly and
shame to him, and bespeaks him both impertinent and imperious.

`II.` A man of great sense and courage, and one that knew as well when
and how to speak as when and how to keep silence. Though he had so much
respect to his friends as not to interrupt them with his speaking, yet
he had so much regard to truth and justice (his better friends) as not
to betray them by his silence. He boldly pleads,

`1.` That man is a rational creature, and therefore that every man has
for himself a judgment of discretion and ought to be allowed a liberty
of speech in his turn. He means the same that Job did (ch. 12:3, But I
have understanding as well as you) when he says (v. 8), But there is a
spirit in man; only he expresses it a little more modestly, that one man
has understanding as well as another, and no man can pretend to have the
monopoly of reason or to engross all the trade of it. Had he meant I
have revelation as well as you (as some understand it), he must have
proved it; but, if he meant only I have reason as well as you, they
cannot deny it, for it is every man\'s honour, and it is no presumption
to claim it, nor could they gainsay his inference from it (v. 10):
Therefore hearken to me. Learn here, `(1.)` That the soul is a spirit,
neither material itself nor dependent upon matter, but capable of
conversing with things spiritual, which are not the objects of sense.
`(2.)` It is an understanding spirit. It is able to discover and receive
truth, to discourse and reason upon it, and to direct and rule
accordingly. `(3.)` This understanding spirit is in every man; it is the
light that lighteth every man, Jn. 1:9. `(4.)` It is the inspiration of
the Almighty that gives us this understanding spirit; for he is the
Father of spirits and fountain of understanding. See Gen. 2:7; Eccl.
12:7; Zec. 12:1.

`2.` That those who are advanced above others in grandeur and gravity do
not always proportionably go beyond them in knowledge and wisdom (v. 9):
Great men are not always wise; it is a pity but they were, for then they
would never do hurt with their greatness and would do so much the more
good with their wisdom. Men should be preferred for their wisdom, and
those that are in honour and power have most need of wisdom and have the
greatest opportunity of improving in it; and yet it does not follow that
great men are always wise, and therefore it is folly to subscribe to the
dictates of any with an implicit faith. The aged do not always
understand judgment; even they may be mistaken, and therefore must not
expect to bring every thought into obedience to them: nay, therefore
they must not take it as an affront to be contradicted, but rather take
it as a kindness to be instructed, by their juniors: Therefore I said,
hearken to me, v. 10. We must be willing to hear reason from those that
are every way inferior to us, and to yield to it. He that has a good eye
can see further upon level ground than he that is purblind can from the
top of the highest mountain. Better is a poor and wise child then an old
and foolish king, Eccl. 4:13.

`3.` That it was requisite for something to be said, for the setting of
this controversy in a true light, which, by all that had hitherto been
said, was but rendered more intricate and perplexed (v. 13): \"I must
speak, lest you should say, We have found out wisdom, lest you should
think your argument against Job conclusive and irrefragable, and that
Job cannot be convinced and humbled by any other argument than this of
yours, That God casteth him down and not man, that it appears by his
extraordinary afflictions that God is his enemy, and therefore he is
certainly a wicked man. I must show you that this is a false hypothesis
and that Job may be convinced without maintaining it.\" Or, \"Lest you
should think you have found out the wisest way, to reason no more with
him, but leave it to God to thrust him down.\" It is time to speak when
we hear errors advanced and disputed for, especially under pretence of
supporting the cause of God with them. It is time to speak when God\'s
judgments are vouched for the patronizing of men\'s pride and passion
and their unjust uncharitable censures of their brethren; then we must
speak on God\'s behalf.

`4.` That he had something new to offer, and would endeavour to manage
the dispute in a better manner than it had hitherto been managed, v. 14.
He thinks he may expect a favourable hearing; for, `(1.)` He will not
reply to Job\'s protestations of his integrity, but allows the truth of
them, and therefore does not interpose as his enemy: \"He hath not
directed his words against me. I have nothing to say against the main
scope of his discourse, nor do I differ from his principles. I have only
a gentle reproof to give him for his passionate expressions.\" `(2.)` He
will not repeat their arguments, nor go upon their principles: \"Neither
will I answer him with your speeches-not with the same matter, for
should I only say what has been said I might justly be silenced as
impertinent,-nor in the same manner; I will not be guilty of that
peevishness towards him myself which I dislike in you.\" The controversy
that has already been fully handled a wise man will let alone, unless he
can amend and improve what has been done; why should he actum agere-do
that which has been done already?

### Verses 15-22

Three things here apologize for Elihu\'s interposing as he does in this
controversy which had already been canvassed by such acute and learned
disputants:-

`1.` That the stage was clear, and he did not break in upon any of the
managers on either side: They were amazed (v. 15); they stood still, and
answered no more, v. 16. They not only left off speaking themselves, but
they stood still, to hear if any of the company would speak their minds,
so that (as we say) he had room and fair play given him. They seemed not
fully satisfied themselves with what they had said, else they would have
adjourned the court, and not have stood still, expecting what might
further be offered. And therefore I said (v. 17), \"I will answer also
my part. I cannot pretend to give a definitive sentence; no, the
judgment is the Lord\'s, and by him it must be determined who is in the
right and who is in the wrong; but, since you have each of you shown
your opinion, I also will show mine, and let it take its fate with the
rest.\" When what is offered, even by the meanest, is offered thus
modestly, it is a pity but it should be fairly heard and considered. I
see no inconvenience in supposing that Elihu here discovers himself to
be the penman of this book, and that he here writes as an historian,
relating the matter of fact, that, after he had bespoken their attention
in the foregoing verses, they were amazed, they left off whispering
among themselves, did not gainsay the liberty of speech he desired, but
stood still to hear what he would say, being much surprised at the
admirable mixture of boldness and modesty that appeared in his preface.

`2.` That he was uneasy, and even in pain, to be delivered of his
thoughts upon this matter. They must give him leave to speak, for he
cannot forbear; while he is musing the fire burns (Ps. 39:3), shut up in
his bones, as the prophet speaks, Jer. 20:9. Never did nurse, when her
breasts were gorged, so long to have them drawn as Elihu did to deliver
his mind concerning Job\'s case, v. 18-20. If any of the disputants had
hit that which he thought was the right joint, he would contentedly have
been silent; but, when he thought they all missed it, he was eager to be
trying his hand at it. He pleads, `(1.)` That he had a great deal to say:
\"I am full of matter, having carefully attended to all that has
hitherto been said, and made my own reflections upon it.\" When aged men
are drawn dry, and have spent their stock, in discoursing of the divine
Providence, God can raise up others, even young men, and fill them with
matter for the edifying of his church; for it is a subject that can
never be exhausted, though those that speak upon it may. `(2.)` That he
was under a necessity of saying it: \"The spirit within me not only
instructs me what to say, but puts me on to say it; so that if I have
not vent (such a ferment are my thoughts in) I shall burst like bottles
of new wine when it is working,\" v. 19. See what a great grief it is to
a good minister to be silenced and thrust into a corner; he is full of
matter, full of Christ, full of heaven, and would speak of these things
for the good of others, but he may not. `(3.)` That it would be an ease
and satisfaction to himself to deliver his mind (v. 20): I will speak,
that I may be refreshed, not only that I may be eased of the pain of
stifling my thoughts, but that I may have the pleasure of endeavouring,
according to my place and capacity, to do good. It is a great
refreshment to a good man to have liberty to speak for the glory of God
and the edification of others.

`3.` That he was resolved to speak, with all possible freedom and
sincerity, what he thought was true, not what he thought would please
(v. 21, 22): \"Let me not accept any man\'s person, as partial judges
do, that aim to enrich themselves, not to do justice. I am resolved to
flatter no man.\" He would not speak otherwise than he thought, either,
`(1.)` In compassion to Job, because he was poor and in affliction, would
not make his case better than he really took it to be, for fear of
increasing his grief; \"but, let him bear it as he can, he shall be told
the truth.\" Those that are in affliction must not be flattered, but
dealt faithfully with. When trouble is upon any it is foolish pity to
suffer sin upon them too (Lev. 19:17), for that is the worst addition
that can be to their trouble. Thou shalt not countenance, any more than
discountenance, a poor man in his cause (Ex. 23:3), nor regard a sad
look any more than a big look, so as, for the sake of it, to pervert
justice, for that is accepting persons. Or, `(2.)` In compliment to Job\'s
friends, because they were in prosperity and reputation. Let them not
expect that he should say as they said, any further than he was
convinced that they say right, nor applaud their dictates for the sake
of their dignities. No, though Elihu is a young man, and upon his
preferment, he will not dissemble truth to court the favour of great
men. It is a good resolution he has taken up-\"I know not to give
flattering titles to men; I never used myself to flattering language;\"
and it is a good reason he gives for that resolution-in so doing my
Maker would soon take my away. It is good to keep ourselves in awe with
a holy fear of God\'s judgments. He that made us will take us away in
his wrath is we do not conduct ourselves as we should. He hates all
dissimulation and flattery, and will soon put lying lips to silence and
cut off flattering lips, Ps. 12:3. The more closely we eye the majesty
of God as our Maker, and the more we dread his wrath and justice, the
less danger shall we be in of a sinful fearing or flattering of men.
