Job, Chapter 18
===============

Commentary
----------

In this chapter Bildad makes a second assault upon Job. In his first
discourse (ch. 8) he had given him encouragement to hope that all should
yet be well with him. But here there is not a word of that; he has grown
more peevish, and is so far from being convinced by Job\'s reasonings
that he is but more exasperated. `I.` He sharply reproves Job as haughty
and passionate, and obstinate in his opinion (v. 1-4). `II.` He enlarges
upon the doctrine he had before maintained, concerning the miser of
wicked people and the ruin that attends them (v. 5-21). In this he
seems, all along, to have an eye to Job\'s complaints of the miserable
condition he was in, that he was in the dark, bewildered, ensnared,
terrified, and hastening out of the world. \"This,\" says Bildad, \"is
the condition of a wicked man; and therefore thou art one.\"

### Verses 1-4

Bildad here shoots his arrows, even bitter words, against poor Job,
little thinking that, though he was a wise and good man, in this
instance he was serving Satan\'s design in adding to Job\'s affliction.

`I.` He charges him with idle endless talk, as Eliphaz had done (ch. 15:2,
3): How long will it be ere you make an end of words? v. 2. Here he
reflects, not only upon Job himself, but either upon all the managers of
the conference (thinking perhaps that Eliphaz and Zophar did not speak
so closely to the purpose as they might have done) or upon some that
were present, who possibly took part with Job, and put in a word now and
then in his favour, though it be not recorded. Bildad was weary of
hearing others speak, and impatient till it came to his turn, which
cannot be observed to any man\'s praise, for we ought to be swift to
hear and slow to speak. It is common for contenders to monopolize the
reputation of wisdom, and then to insist upon it as their privilege to
be dictators. How unbecoming this conduct is in others every one can
see; but few that are guilty of it can see it in themselves. Time was
when Job had the last word in all debates (ch. 29:22): After my words
they spoke not again. Then he was in power and prosperity; but now that
he was impoverished and brought low he could scarcely be allowed to
speak at all, and every thing he said was as much vilified as formerly
it had been magnified. Wisdom therefore (as the world goes) is good with
an inheritance (Eccl. 7:11); for the poor man\'s wisdom is despised,
and, because he is poor, his words are not heard, Eccl. 9:16.

`II.` With a regardlessness of what was said to him, intimated in that,
Mark, and afterwards we will speak. And it is to no purpose to speak,
though what is said be ever so much to the purpose, if those to whom it
is addressed will not mark and observe it. Let the ear be opened to hear
as the learned, and then the tongues of the learned will do good service
(Isa. 50:4) and not otherwise. It is an encouragement to those that
speak of the things of God to see the hearers attentive.

`III.` With a haughty contempt and disdain of his friends and of that
which they offered (v. 3): Wherefore are we counted as beasts? This was
invidious. Job had indeed called them mockers, had represented them both
as unwise and as unkind, wanting both in the reason and tenderness of
men, but he did not count them beasts; yet Bildad so represents the
matter, 1. Because his high spirit resented what Job had said as if it
had been the greatest affront imaginable. Proud men are apt to think
themselves slighted more than really they are. 2. Because his hot spirit
was willing to find a pretence to be hard upon Job. Those that incline
to be severe upon others will have it thought that others have first
been so upon them.

`IV.` With outrageous passion: He teareth himself in his anger, v. 4.
Herein he seems to reflect upon what Job had said (ch. 13:14): Wherefore
did I take my flesh in my teeth? \"It is thy own fault,\" says Bildad.
Or he reflected upon what he said ch. 16:9, where he seemed to charge it
upon God, or, as some think, upon Eliphaz: He teareth me in his wrath.
\"No,\" says Bildad; \"thou alone shalt bear it.\" He teareth himself in
his anger. Note, Anger is a sin that is its own punishment. Fretful
passionate people tear and torment themselves. He teareth his soul (so
the word is); every sin wounds the soul, tears that, wrongs that (Prov.
8:36), unbridled passion particularly.

`V.` With a proud and arrogant expectation to give law even to Providence
itself: \"Shall the earth be forsaken for thee? Surely not; there is no
reason for that, that the course of nature should be changed and the
settled rules of government violated to gratify the humour of one man.
Job, dost thou think the world cannot stand without thee; but that, if
thou art ruined, all the world is ruined and forsaken with thee?\" Some
make it a reproof of Job\'s justification of himself, falsely
insinuating that either Job was a wicked man or we must deny a
Providence and suppose that God has forsaken the earth and the rock of
ages is removed. It is rather a just reproof of his passionate
complaints. When we quarrel with the events of Providence we forget
that, whatever befals us, it is, 1. According to the eternal purpose and
counsel of God. 2. According to the written word. Thus it is written
that in the world we must have tribulation, that, since we sin daily, we
must expect to smart for it; and, 3. According to the usual way and
custom, the track of Providence, nothing but what is common to men; and
to expect that God\'s counsels should change, his method alter, and his
word fail, to please us, is as absurd and unreasonable as to think the
earth should be forsaken for us and the rock removed out of its place.

### Verses 5-10

The rest of Bildad\'s discourse is entirely taken up in an elegant
description of the miserable condition of a wicked man, in which there
is a great deal of certain truth, and which will be of excellent use if
duly considered-that a sinful condition is a sad condition, and that
iniquity will be men\'s ruin if they do not repent of it. But it is not
true that all wicked people are visibly and openly made thus miserable
in this world; nor is it true that all who are brought into great
distress and trouble in this world are therefore to be deemed and
adjudged wicked men, when no other proof appears against them; and
therefore, though Bildad thought the application of it to Job was easy,
yet it was not safe nor just. In these verses we have,

`I.` The destruction of the wicked foreseen and foretold, under the
similitude of darkness (v. 5, 6): Yea, the light of the wicked shall be
put out. Even his light, the best and brightest part of him, shall be
put out; even that which he rejoiced in shall fail him. Or the yea may
refer to Job\'s complaints of the great distress he was in and the
darkness he should shortly make his bed in. \"Yea,\" says Bildad, \"So
it is; thou art clouded, and straitened, and made miserable, and no
better could be expected; for the light of the wicked shall be put out,
and therefore thine shall.\" Observe here, 1. The wicked may have some
light for a while, some pleasure, some joy, some hope within, as well as
wealth, and honour, and power without. But his light is but a spark (v.
5), a little thing and soon extinguished. It is but a candle (v. 6),
wasting, and burning down, and easily blown out. It is not the light of
the Lord (that is sun-light), but the light of his own fire and sparks
of his own kindling, Isa. 50:11. 2. His light will certainly be put out
at length, quite put out, so that not the least spark of it shall remain
with which to kindle another fire. Even while he is in his tabernacle,
while he is in the body, which is the tabernacle of the soul (2 Co.
5:1), the light shall be dark; he shall have no true solid comfort, no
joy that is satisfying, no hope that is supporting. Even the light that
is in him is darkness; and how great is that darkness! But, when he is
put out of this tabernacle by death, his candle shall be put out with
him. The period of his life will be the final period of all his days and
will turn all his hopes into endless despair. When a wicked man dies his
expectation shall perish, Prov. 11:7. He shall lie down in sorrow.

`II.` The preparatives for that destruction represented under the
similitude of a beast or bird caught in a snare, or a malefactor
arrested and taken into custody in order to his punishment, v. 7-10. 1.
Satan is preparing for his destruction. He is the robber that shall
prevail against him (v. 9); for, as he was a murderer, so he was a
robber, from the beginning. He, as the tempter, lays snares for sinners
in the way, wherever they go, and he shall prevail. If he make them
sinful like himself, he will make them miserable like himself. He hunts
for the precious life. 2. He is himself preparing for his own
destruction by going on in sin, and so treasuring up wrath against the
day of wrath. God gives him up, as he deserves and desires, to his own
counsels, and then his own counsels cast him down, v. 7. His sinful
projects and pursuits bring him into mischief. He is cast into a net by
his own feet (v. 8), runs upon his own destruction, is snared in the
work of his own hands (Ps. 9:16); his own tongue falls upon him, Ps.
64:8. In the transgression of an evil man there is a snare. 3. God is
preparing for his destruction. The sinner by his sin is preparing the
fuel and then God by his wrath is preparing the fire. See here, `(1.)` How
the sinner is infatuated, to run himself into the snare; and whom God
will destroy he infatuates. `(2.)` How he is embarrassed: The steps of his
strength, his mighty designs and efforts, shall be straitened, so that
he shall not compass what he intended; and the more he strives to
extricate himself the more will he be entangled. Evil men wax worse and
worse. `(3.)` How he is secured and kept from escaping the judgments of
God that are in pursuit of him. The gin shall take him by the heel. He
can no more escape the divine wrath that is in pursuit of him than a
man, so held, can flee from the pursuer. God knows how to reserve the
wicked for the day of judgment, 2 Pt. 2:9.

### Verses 11-21

Bildad here describes the destruction itself which wicked people are
reserved for in the other world, and which, in some degree, often seizes
them in this world. Come, and see what a miserable condition the sinner
is in when his day comes to fall.

`I.` See him disheartened and weakened by continual terrors arising from
the sense of his own guilt and the dread of God\'s wrath (v. 11, 12):
Terror shall make him afraid on every side. The terrors of his own
conscience shall haunt him, so that he shall never be easy. Wherever he
goes, these shall follow him; which way soever he looks, these shall
stare him in the face. It will make him tremble to see himself fought
against by the whole creation, to see Heaven frowning on him, hell
gaping for him, and earth sick of him. He that carries his own accuser,
and his own tormentor, always in his bosom, cannot but be afraid on
every side. This will drive him to his feet, like the malefactor, who,
being conscious of his own guilt, takes to his heels and flees when none
pursues, Prov. 28:1. But his feet will do him no service; they are fast
in the snare, v. 9. The sinner may as soon overpower the divine
omnipotence as flee from the divine omniscience, Amos 9:2, 3. No marvel
that the sinner is dispirited and distracted with fear, for, 1. He sees
his ruin approaching: Destruction shall be ready at his side, to seize
him whenever justice gives the word, so that he is brought into
desolation in a moment, Ps. 73:19. 2. He feels himself utterly unable to
grapple with it, either to escape it or to bear up under it. That which
he relied upon as his strength (his wealth, power, pomp, friends, and
the hardiness of his own spirit) shall fail him in the time of need, and
be hunger-bitten, that is, it shall do him no more service than a
famished man, pining away for hunger, would do in work or war. The case
being thus with him, no marvel that he is a terror to himself. Note, The
way of sin is a way of fear, and leads to everlasting confusion, of
which the present terrors of an impure and unpacified conscience are
earnests, as they were to Cain and Judas.

`II.` See him devoured and swallowed up by a miserable death; and
miserable indeed a wicked man\'s death is, how secure and jovial soever
his life was. 1. See him dying, arrested by the first-born of death
(some disease, or some stroke that has in it a more than ordinary
resemblance of death itself; so great a death, as it is called, 2 Co.
1:10, a messenger of death that has in it an uncommon strength and
terror), weakened by the harbingers of death, which devour the strength
of his skin, that is, it shall bring rottenness into his bones and
consume them. His confidence shall then be rooted out of his tabernacle
(v. 14), that is, all that he trusted to for his support shall be taken
from him, and he shall have nothing to rely upon, no, not his own
tabernacle. His own soul was his confidence, but that shall be rooted
out of the tabernacle of the body, as a tree that cumbered the ground.
\"Thy soul shall be required of thee.\" 2. See him dead, and see his
case then with an eye of faith. `(1.)` He is then brought to the king of
terrors. He was surrounded with terrors while he lived (v. 11), and
death was the king of all those terrors; they fought against the sinner
in death\'s name, for it is by reason of death that sinners are all
their lifetime subject to bondage (Heb. 2:15), and at length they will
be brought to that which they so long feared, as a captive to the
conqueror. Death is terrible to nature; our Saviour himself prayed,
Father, save me from this hour. But to the wicked it is in a special
manner the king of terrors, both as it is a period to that life in which
they placed their happiness and a passage to that life where they will
find their endless misery. How happy then are the saints, and how much
indebted to the Lord Jesus, by whom death is so far abolished, and the
property of it altered, that this king of terrors becomes a friend and
servant! `(2.)` He is then driven from the light into darkness (v. 18),
from the light of this world, and his prosperous condition in it, into
darkness, the darkness of the grave, the darkness of hell, into utter
darkness, never to see light (Ps. 49:19), not the least gleam, nor any
hopes of it. `(3.)` He is then chased out of the world, hurried and
dragged away by the messengers of death, sorely against his will, chased
as Adam out of paradise, for the world is his paradise. It intimates
that he would fain stay here; he is loth to depart, but go he must; all
the world is weary of him, and therefore chases him out, as glad to get
rid of him. This is death to a wicked man.

`III.` See his family sunk and cut off, v. 15. The wrath and curse of God
light and lie, not only upon his head and heart, but upon his house too,
to consume it with the timber and stones thereof, Zec. 5:4. Death itself
shall dwell in his tabernacle, and, having expelled him, shall take
possession of his house, to the terror and destruction of all that he
leaves behind. Even the dwelling shall be ruined for the sake of its
owner: Brimstone shall be scattered upon his habitation, rained upon it
as upon Sodom, to the destruction of which this seems to have reference.
Some think he here upbraids Job with the burning of his sheep and
servants with fire from heaven. The reason is here given why his
tabernacle is thus marked for ruin: Because it is none of his; that is,
it was unjustly got, and kept, from the rightful owner, and therefore
let him not expect either the comfort or the continuance of it. His
children shall perish, either with him or after him, v. 16. So that, his
roots being in his own person dried up beneath, above his branch (every
child of his family) shall be cut off. Thus the houses of Jeroboam,
Baasha, and Ahab, were cut off; none that descended from them were left
alive. Those who take root in the earth may expect it will thus be dried
up; but, if we be rooted in Christ, even our leaf shall not wither, much
less shall our branch be cut off. Those who consult the true honour of
their family, and the welfare of its branches, will be afraid of
withering it by sin. The extirpation of the sinner\'s family is
mentioned again (v. 19): He shall neither have son nor nephew, child nor
grandchild, to enjoy his estate and bear up his name, nor shall there be
any remaining in his dwelling akin to him. Sin entails a curse upon
posterity, and the iniquity of the fathers is often visited upon the
children. Herein, also, it is probable that Bildad reflects upon the
death of Job\'s children and servants, as a further proof of his being a
wicked man; whereas all that are written childless are not thereby
written graceless; there is a name better than that of sons and
daughters.

`IV.` See his memory buried with him, or made odious; he shall either be
forgotten or spoken of with dishonour (v. 17): His remembrance shall
perish from the earth; and, if it perish thence, it perishes wholly, for
it was never written in heaven, as the names of the saints are, Lu.
10:20. All his honour shall be laid and lost in the dust, or stained
with perpetual infamy, so that he shall have no name in the street,
departing without being desired. Thus the judgments of God follow him,
after death, in this world, as an indication of the misery his soul is
in after death, and an earnest of that everlasting shame and contempt to
which he shall rise in the great day. The memory of the just is blessed,
but the name of the wicked shall rot, Prov. 10:7.

`V.` See a universal amazement at his fall, v. 20. Those that see it are
affrighted, so sudden is the change, so dreadful the execution, so
threatening to all about him: and those that come after, and hear the
report of it, are astonished at it; their ears are made to tingle, and
their hearts to tremble, and they cry out, Lord, how terrible art thou
in thy judgments! A place or person utterly ruined is said to be made an
astonishment, Deu. 28:37; 2 Chr. 7:21; Jer. 25:9, 18. Horrible sins
bring strange punishments.

`VI.` See all this averred as the unanimous sense of the patriarchal age,
grounded upon their knowledge of God and their many observations of his
providence (v. 21): Surely such are the dwellings of the wicked, and
this is the place (this the condition) of him that knows not God! See
here what is the beginning, and what is the end, of the wickedness of
this wicked world. 1. The beginning of it is ignorance of God, and it is
a wilful ignorance, for there is that to be known of him which is
sufficient to leave them for ever inexcusable. They know not God, and
then they commit all iniquity. Pharaoh knows not the Lord, and therefore
will not obey his voice. 2. The end of it, and that is utter
destruction. Such, so miserable, are the dwellings of the wicked.
Vengeance will be taken of those that know not God, 2 Th. 1:8. For those
whom he has not honour from he will get himself honour upon. Let us
therefore stand in awe and not sin, for it will certainly be bitterness
in the latter end.
