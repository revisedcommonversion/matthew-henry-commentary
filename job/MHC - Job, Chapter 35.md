Job, Chapter 35
===============

Commentary
----------

Job being still silent, Elihu follows his blow, and here, a third time,
undertakes to show him that he had spoken amiss, and ought to recant.
Three improper sayings he here charges him with, and returns answer to
them distinctly:-I. He had represented religion as an indifferent
unprofitable thing, which God enjoins for his own sake, not for ours;
Elihu evinces the contrary (v. 1-8). `II.` He had complained of God as
deaf to the cries of the oppressed, against which imputation Elihu here
justifies God (v. 9-13). `III.` He had despaired of the return of God\'s
favour to him, because it was so long deferred, but Elihu shows him the
true cause of the delay (v. 14-16).

### Verses 1-8

We have here,

`I.` The bad words which Elihu charges upon Job, v. 2, 3. To evince the
badness of them he appeals to Job himself, and his own sober thoughts,
in the reflection: Thinkest thou this to be right? This intimates
Elihu\'s confidence that the reproof he now gave was just, for he could
refer the judgment of it even to Job himself. Those that have truth and
equity on their side sooner or later will have every man\'s conscience
on their side. It also intimates his good opinion of Job, that he
thought better than he spoke, and that, though he had spoken amiss, yet,
when he perceived his mistake, he would not stand to it. When we have
said, in our haste, that which was not right, it becomes us to own that
our second thoughts convince us that it was wrong. Two things Elihu here
reproves Job for:-1. For justifying himself more than God, which was the
thing that first provoked him, ch. 32:2. \"Thou hast, in effect, said,
My righteousness is more than God\'s,\" that is, \"I have done more for
God than ever he did for me; so that, when the accounts are balanced, he
will be brought in debtor to me.\" As if Job thought his services had
been paid less than they deserved and his sins punished more than they
deserved, which is a most unjust and wicked thought for any man to
harbour and especially to utter. When Job insisted so much upon his own
integrity, and the severity of God\'s dealings with him, he did in
effect say, My righteousness is more than God\'s; whereas, though we be
ever so good and our afflictions ever so great, we are chargeable with
unrighteousness and God is not. 2. For disowning the benefits and
advantages of religion because he suffered these things: What profit
shall I have if I be cleansed from my sin? v. 3. This is gathered from
ch. 9:30, 31. Though I make my hands ever so clean, what the nearer am
I? Thou shalt plunge me in the ditch. And ch. 10:15, If I be wicked, woe
to me; but, if I be righteous, it is all the same. The psalmist, when he
compared his own afflictions with the prosperity of the wicked, was
tempted to say, Verily I have cleansed my heart in vain, Ps. 73:13. And,
if Job said so, he did in effect say, My righteousness is more than
God\'s (v. 9); for, if he got nothing by his religion, God was more
beholden to him than he was to God. But, though there might be some
colour for it, yet it was not fair to charge these words upon Job, when
he himself had made them the wicked words of prospering sinners (ch.
21:15, What profit shall we have if we pray to him?) and had immediately
disclaimed them. The counsel of the wicked is far from me, ch. 21:16. It
is not a fair way of disputing to charge men with those consequences of
their opinions which they expressly renounce.

`II.` The good answer which Elihu gives to this (v. 4): \"I will
undertake to answer thee, and thy companions with thee,\" that is, \"all
those that approve thy sayings and are ready to justify thee in them,
and all others that say as thou sayest: \"I have that to offer which
will silence them all.\" To do this he has recourse to his old maxim
(ch. 33:12), that God is greater than man. This is a truth which, if
duly improved, will serve many good purposes, and particularly this to
prove that God is debtor to no man. The greatest of men may be a debtor
to the meanest; but such is the infinite disproportion between God and
man that the great God cannot possibly receive any benefit by man, and
therefore cannot be supposed to lie under any obligation to man; for, if
he be obliged by his purpose and promise, it is only to himself. That is
a challenge which no man can take up (Rom. 11:35), Who hath first given
to God, let him prove it, and it shall be recompensed to him again. Why
should we demand it, as a just debt, to gain by our religion (as Job
seemed to do), when the God we serve does not gain by it? 1. Elihu needs
not prove that God is above man; it is agreed by all; but he endeavours
to affect Job and us with it, by an ocular demonstration of the height
of the heavens and the clouds, v. 5. They are far above us, and God is
far above them; how much then is he set out of the reach either of our
sins or of our services! Look unto the heavens, and behold the clouds.
God made man erect, coelumque tueri jussit-and bade him look up to
heaven. Idolaters looked up, and worshipped the hosts of heaven, the
sun, moon, and stars; but we must look up to heaven, and worship the
Lord of those hosts. They are higher than we, but God is infinitely
above them. His glory is above the heavens (Ps. 8:1) and the knowledge
of him higher than heaven, ch. 11:8. 2. But hence he infers that God is
not affected, either one way or other, by any thing that we do. `(1.)` He
owns that men may be either bettered or damaged by what we do (v. 8):
Thy wickedness, perhaps, may hurt a man as thou art, may occasion him
trouble in his outward concerns. A wicked man may wound, or rob, or
slander his neighbour, or may draw him into sin and so prejudice his
soul. Thy righteousness, thy justice, thy charity, thy wisdom, thy
piety, may perhaps profit the son of man. Our goodness extends to the
saints that are in the earth, Ps. 16:3. To men like ourselves we are in
a capacity either of doing injury or of showing kindness; and in both
these the sovereign Lord and Judge of all will interest himself, will
reward those that do good and punish those that do hurt to their
fellow-creatures and fellow-subjects. But, `(2.)` He utterly denies that
God can really be either prejudiced or advantaged by what any, even the
greatest men of the earth, do, or can do. `[1.]` The sins of the worst
sinners are no damage to him (v. 6): \"If thou sinnest wilfully, and of
malice prepense, against him, with a high hand, nay, if thy
transgressions be multiplied, and the acts of sin be ever so often
repeated, yet what doest thou against him?\" This is a challenge to the
carnal mind, and defies the most daring sinner to do his worst. It
speaks much for the greatness and glory of God that it is not in the
power of his worst enemies to do him any real prejudice. Sin is said to
be against God because so the sinner intends it and so God takes it, and
it is an injury to his honour; yet it cannot do any thing against him.
The malice of sinners is impotent malice: it cannot destroy his being or
perfections, cannot dethrone him from his power and dominion, cannot
disturb his peace and repose, cannot defeat his counsels and designs,
nor can it derogate from his essential glory. Job therefore spoke amiss
in saying What profit is it that I am cleansed from my sin? God was no
gainer by his reformation; and who then would gain if he himself did
not? `[2.]` The services of the best saints are no profit to him (v. 7):
If thou be righteous, what givest thou to him? He needs not our service;
or, if he did want to have the work done, he has better hands than ours
at command. Our religion brings no accession at all to his felicity. He
is so far from being beholden to us that we are beholden to him for
making us righteous and accepting our righteousness; and therefore we
can demand nothing from him, nor have any reason to complain if we have
not what we expect, but to be thankful that we have better than we
deserve.

### Verses 9-13

Elihu here returns an answer to another word that Job had said, which,
he thought, reflected much upon the justice and goodness of God, and
therefore ought not to pass without a remark. Observe,

`I.` What it was that Job complained of; it was this, That God did not
regard the cries of the oppressed against their oppressors (v. 9): \"By
reason of the multitude of oppressions, the many hardships which proud
tyrants put upon poor people and the barbarous usage they give them,
they make the oppressed to cry; but it is to no purpose: God does not
appear to right them. They cry out, they cry on still, by reason of the
arm of the mighty, which lies heavily upon them.\" This seems to refer
to those words of Job (ch. 24:12), Men groan from out of the city, and
the soul of the wounded cries out against the oppressors, yet God lays
not folly to them, does not reckon with them for it. This is a thing
that Job knows not what to make of, nor how to reconcile to the justice
of God and his government. Is there a righteous God, and can it be that
he should so slowly hear, so slowly see?

`II.` How Elihu solves the difficulty. If the cries of the oppressed be
not heard, the fault is not in God; he is ready to hear and help them.
But the fault is in themselves; they ask and have not, but it is because
they ask amiss, James 4:3. They cry out by reason of the arm of the
mighty, but it is a complaining cry, a wailing cry, not a penitent
praying cry, the cry of nature and passion, not of grace. See Hos. 7:14,
They have not cried unto me with their heart when they howled upon their
beds. How then can we expect that they should be answered and relieved?

`1.` They do not enquire after God, nor seek to acquaint themselves with
him, under their affliction (v. 10): But none saith, Where is God my
Maker? Afflictions are sent to direct and quicken us to enquire early
after God, Ps. 78:34. But many that groan under great oppressions never
mind God, nor take notice of his hand in their troubles; if they did,
they would bear their troubles more patiently and be more benefited by
them. Of the many that are afflicted and oppressed, few get the good
they might get by their affliction. It should drive them to God, but how
seldom is this the case! It is lamentable to see so little religion
among the poor and miserable part of mankind. Every one complains of his
troubles; but none saith, Where is God my Maker? that is, none repent of
their sins, none return to him that smites them, none seek the face and
favour of God, and that comfort in him which would balance their outward
afflictions. They are wholly taken up with the wretchedness of their
condition, as if that would excuse them in living without God in the
world which should engage them to cleave the more closely to him.
Observe, `(1.)` God is our Maker, the author of our being, and, under that
notion, it concerns us to regard and remember him, Eccl. 12:1. God my
makers, in the plural number, which some think is, if not an indication,
yet an intimation, of the Trinity of persons in the unity of the
Godhead. Let us make man. `(2.)` It is our duty therefore to enquire after
him. Where is he, that we may pay our homage to him, may own our
dependence upon him and obligations to him? Where is he, that we may
apply to him for maintenance and protection, may receive law from him,
and may seek our happiness in his favour, from whose power we received
our being? `(3.)` It is to be lamented that he is so little enquired after
by the children of men. All are asking, Where is mirth? Where is wealth?
Where is a good bargain? But none ask, Where is God my Maker?

`2.` They do not take notice of the mercies they enjoy in and under their
afflictions, nor are thankful for them, and therefore cannot expect that
God should deliver them out of their afflictions. `(1.)` He provides for
our inward comfort and joy under our outward troubles, and we ought to
make use of that, and wait his time for the removal of our troubles: He
gives songs in the night, that is, when our condition is ever so dark,
and sad, and melancholy, there is that in God, in his providence and
promise, which is sufficient, not only to support us, but to fill us
with joy and consolation, and enable us in every thing to give thanks,
and even to rejoice in tribulation. When we only pore upon the
afflictions we are under, and neglect the consolations of God which are
treasured up for us, it is just with God to reject our prayers. `(2.)` He
preserves to us the use of our reason and understanding (v. 11): Who
teaches us more than the beasts of the earth, that is, who has endued us
with more noble powers and faculties than they are endued with and has
made us capable of more excellent pleasures and employments here and for
ever. Now this comes in here, `[1.]` As that which furnishes us with
matter for thanksgiving, even under the heaviest burden of affliction.
Whatever we are deprived of, we have our immortal souls, those jewels of
more worth than all the world, continued to us; even those that kill the
body cannot hurt them. And if our affliction prevail not to disturb the
exercise of their faculties, but we enjoy the use of our reason and the
peace of our consciences, we have much reason to be thankful, how
pressing soever our calamities otherwise are. `[2.]` As a reason why we
should, under our afflictions, enquire after God our Maker, and seek
unto him. This is the greatest excellency of reason, that it makes us
capable of religion, and it is in that especially that we are taught
more than the beasts and the fowls. They have wonderful instincts and
sagacities in seeking out their food, their physic, their shelter; but
none of them are capable of enquiring, Where is God my Maker? Something
like logic, and philosophy, and politics, has been observed among the
brute-creatures, but never any thing of divinity or religion; these are
peculiar to man. If therefore the oppressed only cry by reason of the
arm of the mighty, and do not look up to God, they do no more than the
brutes (who complain when they are hurt), and they forget that
instruction and wisdom by which they are advanced so far above them. God
relieves the brute-creatures because they cry to him according to the
best of their capacity, ch. 38:41; Ps. 104:21. But what reason have men
to expect relief, who are capable of enquiring after God as their Maker
and yet cry to him no otherwise than as brutes do?

`3.` They are proud and unhumbled under their afflictions, which were
sent to mortify them and to hide pride from them (v. 12): There they
cry-there they lie exclaiming against their oppressors, and filling the
ears of all about them with their complaints, not sparing to reflect
upon God himself and his providence-but none gives answer. God does not
work deliverance for them, and perhaps men do not much regard them; and
why so? It is because of the pride of evil men; they are evil men; they
regard iniquity in their hearts, and therefore God will not hear their
prayers, Ps. 66:18; Isa. 1:15. God hears not such sinners. They have, it
may be, brought themselves into trouble by their own wickedness; they
are the devil\'s poor; and then who can pity them? Yet this is not all:
they are proud still, and therefore they do not seek unto God (Ps.
10:4), or, if they do cry unto him, therefore he does not give answer,
for he hears only the desire of the humble (Ps. 10:17) and delivers
those by his providence whom he has first by his grace prepared and made
fit for deliverance, which we are not if, under humbling afflictions,
our hearts remain unhumbled and our pride unmortified. The case is plain
then, If we cry to God for the removal of the oppression and affliction
we are under, and it is not removed, the reason is not because the
Lord\'s hand is shortened or his ear heavy, but because the affliction
has not done its work; we are not sufficiently humbled, and therefore
must thank ourselves that it is continued.

`4.` They are not sincere, and upright, and inward with God, in their
supplications to him, and therefore he does not hear and answer them (v.
13): God will not hear vanity, that is, the hypocritical prayer, which
is a vain prayer, coming out of feigned lips. It is a vanity to think
that God should hear it, who searches the heart and requires truth in
the inward part.

### Verses 14-16

Here is, `I.` Another improper word for which Elihu reproves Job (v. 14):
Thou sayest thou shalt not see him; that is, 1. \"Thou complainest that
thou dost not understand the meaning of his severe dealings with thee,
nor discern the drift and design of them,\" ch. 23:8, 9. And, 2. \"Thou
despairest of seeing his gracious returns to thee, of seeing better days
again, and art ready to give up all for gone;\" as Hezekiah (Isa.
38:11), I shall not see the Lord. As, when we are in prosperity, we are
ready to think our mountain will never be brought low, so when we are in
adversity we are ready to think our valley will never be filled, but, in
both, to conclude that to morrow must be as this day, which is as absurd
as to think, when the weather is either fair or foul, that is will be
always so, that the flowing tide will always flow, or the ebbing tide
will always ebb.

`II.` The answer which Elihu gives to this despairing word that Job had
said, which is this, 1. That, when he looked up to God, he had no just
reason to speak thus despairingly: Judgment is before him, that is, \"He
knows what he has to do, and will do all in infinite wisdom and justice;
he has the entire plan and model of providence before him, and knows
what he will do, which we do not, and therefore we understand not what
he does. There is a day of judgment before him, when all the seeming
disorders of providence will be set to rights and the dark chapters of
it will be expounded. Then thou shalt see the full meaning of these dark
events, and the final period of these dismal events; then thou shalt see
his face with joy; therefore trust in him, depend upon him, wait for
him, and believe that the issue will be good at last.\" When we consider
that God is infinitely wise, and righteous, and faithful, and that he is
a God of judgment (Isa. 30:18), we shall see no reason to despair of
relief from him, but all the reason in the world to hope in him, that it
will come in due time, in the best time. 2. That if he had not yet seen
an end of his troubles, the reason was because he did not thus trust in
God and wait for him (v. 15): \"Because it is not so, because thou dost
not thus trust in him, therefore the affliction which came at first from
love has now displeasure mixed with it. Now God has visited thee in his
anger, taking it very ill that thou canst not find in thy heart to trust
him, but harbourest such hard misgiving thoughts of him.\" If there be
any mixtures of divine wrath in our afflictions, we may thank ourselves;
it is because we do not behave aright under them; we quarrel with God,
and are fretful and impatient, and distrustful of the divine Providence.
This was Job\'s case. The foolishness of man perverts his way, and then
his heart frets against the Lord, Prov. 19:3. Yet Elihu thinks that Job,
being in great extremity, did not know and consider this as he should,
that it was his own fault that he was not yet delivered. He concludes
therefore that Job opens his mouth in vain (v. 16) in complaining of his
grievances and crying for redress, or in justifying himself and clearing
up his own innocency; it is all in vain, because he does not trust in
God and wait for him, and has not a due regard to him in his
afflictions. He had said a great deal, had multiplied words, but all
without knowledge, all to no purpose, because he did not encourage
himself in God and humble himself before him. It is in vain for us
either to appeal to God or to acquit ourselves if we do not study to
answer the end for which affliction is sent, and in vain to pray for
relief if we do not trust in God; for let not that man who distrusts God
think that he shall receive any thing from him, James 1:7. Or this may
refer to all that Job had said. Having shown the absurdity of some
passages in his discourse, he concludes that there were many other
passages which were in like manner the fruits of his ignorance and
mistake. He did not, as his other friends, condemn him for a hypocrite,
but charged him only with Moses\'s sin, speaking unadvisedly with his
lips when his spirit was provoked. When at any time we do so (and who is
there that offends not in word?) it is a mercy to be told of it, and we
must take it patiently and kindly as Job did, not repeating, but
recanting, what we have said amiss.
