Introduction to 2nd Timothy
===========================

This second epistle Paul wrote to Timothy from Rome, when he was a
prisoner there and in danger of his life; this is evident from these
words, I am now ready to be offered, and the time of my departure is at
hand, ch. 4:6. It appears that his removal out of this world, in his own
apprehension, was not far off, especially considering the rage and
malice of his persecutors; and that he had been brought before the
emperor Nero, which he calls his first answer, when no man stood with
him, but all men forsook him, ch. 4:16. And interpreters agree that this
was the last epistle he wrote. Where Timothy now was is not certain. The
scope of this epistle somewhat differs from that of the former, not so
much relating to his office as an evangelist as to his personal conduct
and behaviour.
