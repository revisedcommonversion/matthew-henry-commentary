Introduction to Zechariah
=========================

This prophet was colleague with the prophet Haggai, and a worker
together with him in forwarding the building of the second temple (Ezra
5:1); for two are better than one. Christ sent forth his disciples two
and two. Zechariah began to prophesy some time after Haggai. But he
continued longer, soared higher in visions and revelations, wrote more,
and prophesied more particularly concerning Christ, than Haggai had
done; so the last shall be first: the last in time sometimes proves
first in dignity. He begins with a plain practical sermon, expressive of
that which was the scope of his prophesying, in the first five verses;
but afterwards, to the end of ch. 6, he relates the visions he saw, and
the instructions he received immediately from heaven by them. At ch. 7,
from an enquiry made by the Jews concerning fasting, he takes occasion
to show them the duty of their present day, and to encourage them to
hope for God\'s favour, to the end of ch. 8, after which there are two
sermons, which are both called burdens of the word of the Lord (one
begins with ch. 9, the other with ch. 12), which probably were preached
some time after; the scope of them is to reprove for sin, and threaten
God\'s judgments against the impenitent, and to encourage those that
feared God with assurances of the mercy God had in store for his church,
and especially of the coming of the Messiah and the setting up of his
kingdom in the world.
