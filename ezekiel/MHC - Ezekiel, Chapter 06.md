Ezekiel, Chapter 6
==================

Commentary
----------

In this chapter we have, `I.` A threatening of the destruction of Israel
for their idolatry, and the destruction of their idols with them (v.
1-7). `II.` A promise of the gracious return of a remnant of them to God,
by true repentance and reformation (v. 8-10). `III.` Directions given to
the prophet and others, the Lord\'s servants, to lament both the
iniquities and the calamities of Israel (v. 11-14).

### Verses 1-7

Here, `I.` The prophecy is directed to the mountains of Israel (v. 1, 2);
the prophet must set his face towards them. If he could see so far off
as the land of Israel, the mountains of that land would be first and
furthest seen; towards them therefore he must look, and look boldly and
stedfastly, as the judge looks at the prisoner, and directs his speech
to him, when he passes sentence upon him. Though the mountains of Israel
be ever so high and ever so strong, he must set his face against them,
as having judgments to denounce that should shake their foundation. The
mountains of Israel had been holy mountains, but now that they had
polluted them with their high places God set his face against them and
therefore the prophet must. Israel is here put, not, as sometimes, for
the ten tribes, but for the whole land. The mountains are called upon to
hear the word of the Lord, to shame the inhabitants that would not hear.
The prophets might as soon gain attention from the mountains as from
that rebellious and gainsaying people, to whom they all day long
stretched out their hands in vain. Hear, O mountains! the Lord\'s
controversy (Mic. 6:1, 2), for God\'s cause will have a hearing, whether
we hear it or no. But from the mountains the word of the Lord echoes to
the hills, to the rivers, and to the valleys; for to them also the Lord
God speaks, intimating that the whole land is concerned in what is now
to be delivered and shall be witnesses against this people that they had
fair warning given them of the judgments coming, but they would not take
it; nay, they contradicted the message and persecuted the messengers, so
that God\'s prophets might more safely and comfortably speak to the
hills and mountains than to them.

`II.` That which is threatened in this prophecy is the utter destruction
of the idols and the idolaters, and both by the sword of war. God
himself is commander-in-chief of this expedition against the mountains
of Israel. It is he that says, Behold, I, even I, will bring a sword
upon you (v. 3); the sword of the Chaldeans is at God\'s command, goes
where he sends it, comes where he brings it, and lights as he directs
it. In the desolations of that war,

`1.` The idols and all their appurtenances should be destroyed. The high
places, which were on the tops of mountains (v. 3), shall be levelled
and made desolate (v. 6); they shall not be beautified, shall not be
frequented as they had been. The altars, on which they offered sacrifice
and burnt incense to strange gods, shall be broken to pieces and laid
waste; the images and idols shall be defaced, shall be broken and cease,
and be cut down, and all the fine costly works about them shall be
abolished, v. 4, 6. Observe here, `(1.)` That war makes woeful
desolations, which those persons, places, and things that were esteemed
most sacred cannot escape; for the sword devours one as well as another.
`(2.)` That God sometimes ruins idolatries even by the hands of idolaters,
for such the Chaldeans themselves were; but, as if the deity were a
local thing, the greatest admirers of the gods of their own country were
the greatest despisers of the gods of other countries. `(3.)` It is just
with God to make that a desolation which we make an idol of; for he is a
jealous God and will not bear a rival. `(4.)` If men do not, as they
ought, destroy idolatry, God will, first or last, find out a way to do
it. When Josiah had destroyed the high places, altars, and images, with
the sword of justice, they set them up again; but God will now destroy
them with the sword of war, and let us see who dares re-establish them.

`2.` The worshippers of idols and all their adherents should be destroyed
likewise. As all their high places shall be laid waste, so shall all
their dwelling-places too, even all their cities, v. 6. Those that
profane God\'s dwelling-place as they had done can expect no other than
that he should abandon theirs, ch. 5:11. If any man defile the temple of
God, him will God destroy, 1 Co. 3:17. It is here threatened that their
slain shall fall in the midst of them (v. 7); there shall be abundance
slain, even in those places which were thought most safe; but it is
added as a remarkable circumstance that they shall fall before their
idols (v. 4), that their dead carcases should be laid, and their bones
scattered, about their altars, v. 5. `(1.)` Thus their idols should be
polluted, and those places profaned by the dead bodies which they had
had in veneration. If they will not defile the covering of their graven
images, God will, Isa. 30:22. The throwing of the carcases among them,
as upon the dunghill, intimates that they were but dunghill-deities.
`(2.)` Thus it was intimated that they were but dead things, unfit to be
rivals with the living God; for the carcases of dead men, that, like
them, have eyes and see not, ears and hear not, were the fittest company
for them. `(3.)` Thus the idols were upbraided with their inability to
help their worshippers, and idolaters were upbraided with the folly of
trusting in them; for, it should seem, they fell by the sword of the
enemy when they were actually before their idols imploring their aid and
putting themselves under their protection. Sennacherib was slain by his
sons when he was worshipping in the house of his god. `(4.)` The sin might
be read in this circumstance of the punishment; the slain men are cast
before the idols, to show that therefore they are slain, because they
worshipped those idols; see Jer. 8:1, 2. let the survivors observe it,
and take warning not to worship images; let them see it, and know that
God is the Lord, that the Lord he is God and he alone.

### Verses 8-10

Judgment had hitherto triumphed, but in these verses mercy rejoices
against judgment. A sad end is made of this provoking people, but not a
full end. The ruin seems to be universal, and yet will I leave a
remnant, a little remnant, distinguished from the body of the people, a
few of many, such as are left when the rest perish; and it is God that
leaves them. This intimates that they deserved to be cut off with the
rest, and would have been cut off if God had not left them. See Isa.
1:9. And it is God who by his grace works that in them which he has an
eye to in sparing them. Now,

`I.` It is a preserved remnant, saved from the ruin which the body of the
nation is involved in (v. 8): That you may have some who shall escape
the sword. God said (ch. 5:12) that he would draw a sword after those
who were scattered, that destruction should pursue them in their
dispersion; but here is mercy remembered in the midst of that wrath, and
a promise that some of the Jews of the dispersion, as they were
afterwards called, should escape the sword. None of those who were to
fall by the sword about Jerusalem shall escape; for they trust to
Jerusalem\'s walls for security, and shall be made ashamed of that vain
confidence. but some of them shall escape the sword among the nations,
where, being deprived of all other stays, they stay themselves upon God
only. They are said to have those who shall escape; for they shall be
the seed of another generation, out of which Jerusalem shall flourish
again.

`II.` It is a penitent remnant (v. 9): Those who escape of you shall
remember me. Note, To those whom god designs for life he will give
repentance unto life. They are reprieved, and escape the sword, that
they may have time to return to God. Note, God\'s patience both leaves
room for repentance and is an encouragement to sinners to repent. Where
God designs grace to repent he allows space to repent; yet many who have
the space want the grace, many who escape the sword do not forsake the
sin, as it is promised that these shall do. This remnant, here marked
for salvation, is a type of the remnant reserved out of the body of
mankind to be monuments of mercy, who are made safe in the same way that
these were, by being brought to repentance. Now observe here,

`1.` The occasion of their repentance, and that is a mixture of judgment
and mercy-judgment, that they were carried captives, but mercy, that
they escaped the sword in the land of their captivity. They were driven
out of their own land, but not out of the land of the living, not chased
out of the world, as other were and they deserved to be. Note, The
consideration of the just rebukes of Providence we are under, and yet of
the mercy mixed with them, should engage us to repent, that we may
answer God\'s end in both. And true repentance shall be accepted of God,
though we are brought to it by our troubles; nay, sanctified afflictions
often prove means of conversion, as to Manasseh.

`2.` The root and principle of their repentance: They shall remember me
among the nations. Those who forgot God in the land of their peace and
prosperity, who waxed fat and kicked, were brought to remember him in
the land of their captivity. The prodigal son never bethought himself of
his father\'s house till he was ready to perish for hunger in the far
country. Their remembering God was the first step they took in returning
to him. Note, Then there begins to be some hopes of sinners when they
have sinned against, and to enquire, Where is God my Maker? Sin takes
rise in forgetting God, Jer. 3:21. Repentance takes rise from the
remembrance of him and of our obligations to him. God says, They shall
remember me, that is, \"I will give them grace to do so;\" for otherwise
they would for ever forget him. That grace shall find them out wherever
they are, and by bringing God to their mind shall bring them to their
right mind. The prodigal, when he remembered his father, remembered how
he has sinned against Heaven and before him; so do these penitents. `(1.)`
They remember the base affront they had put upon God by their
idolatries, and this is that which an ingenuous repentance fastens upon
and most sadly laments. They had departed from God to idols, and given
that honour to pretended deities, the creatures of men\'s fancies and
the work of men\'s hands, which they should have given to the God of
Israel. They departed from God, from his word, which they should have
made their rule, from his work, which they should have made their
business. Their hearts departed from him. The heart, which he requires
and insists upon, and without which bodily exercise profits nothing, the
heart, which should be set upon him, and carried out towards him, when
that departs from him, is as the treacherous elopement of a wife from
her husband or the rebellious revolt of a subject from his sovereign.
Their eyes also go after their idols; they doted on them, and had great
expectations from them. Their hearts followed their eyes in the choice
of their gods (they must have gods that they could see), and then their
eyes followed their hearts in the adoration of them. Now the malignity
of this sin is that it is spiritual whoredom; it is a whorish heart that
departs from God; and they are eyes that go a whoring after their idols.
Note, Idolatry is spiritual whoredom; it is the breach of a
marriage-covenant with God; it is the setting of the affections upon
that which is a rival with him, and the indulgence of a base lust, which
deceives and defiles the soul, and is a great wrong to God in his
honour, `(2.)` They remember what a grief this was to him and how he
resented it. They shall remember that I am broken with their whorish
heart and their eyes that are full of this spiritual adultery, not only
angry at it, but grieved, as a husband is at the lewdness of a wife whom
he dearly loved, grieved to such a degree that he is broken with it; it
breaks his heart to think that he should be so disingenuously dealt
with; he is broken as an aged father is with the undutiful behaviour of
a rebellious and disobedient son, which sinks his spirits and makes him
to stoop. Forty years long was I grieved with this generation, Ps.
95:10. God\'s measures were broken (so some); a stop was put to the
current of his favours towards them, and he was even compelled to punish
them. This they shall remember in the day of their repentance, and it
shall affect and humble them more than any thing, not so much that their
peace was broken, and their country broken, as that God was broken by
their sin. Thus they shall look on him whom they have pierced and shall
mourn, Zec. 12:10. Note, Nothing grieves a true penitent so much as to
think that his sin has been a grief to God and to the Spirit of his
grace.

`3.` The product and evidence of their repentance: They shall loathe
themselves for the evils which they have committed in all their
abominations. Thus God will give them grace to qualify them for pardon
and deliverance. Though he had been broken by their whorish heart, yet
he would not quite cast them off. See Isa. 57:17, 18; Hos. 2:13, 14. His
goodness takes occasion from their badness to appear the more
illustrious. note, `(1.)` True penitents see sin to be an abominable
thing, that abominable thing which the Lord hates and which makes
sinners, and even their services, odious to him, Jer. 44:4; Isa. 1:11.
It defiles the sinner\'s own conscience, and makes him, unless he be
past feeling, an abomination to himself. An idol is particularly called
an abomination, Isa. 44:19. Those gratifications which the hearts of
sinners were set upon as delectable things the hearts of penitents are
turned against as detestable things. `(2.)` There are many evils committed
in these abominations, many included in them, attendant on them, and
flowing from them, many transgressions in one sin, Lev. 16:21. In their
idolatries they were sometimes guilty of whoredom (as in the worship of
Peor), sometimes of murder (as in the worship of Moloch); these were
evils committed in their abominations. Or it denotes the great malignity
there is in sin; it is an abomination that has abundance of evil in it.
`(3.)` Those that truly loathe sin cannot but loathe themselves because of
sin; self-loathing is evermore the companion of true repentance.
Penitents quarrel with themselves, and can never be reconciled to
themselves till they have some ground to hope that God is reconciled to
them; nay, then they shall lie down in their shame, when he is pacified
towards them, ch. 16:63.

`4.` The glory that will redound to God by their repentance (v. 10):
\"They shall know that I am the Lord; they shall be convinced of it by
experience, and shall be ready to own it, and that I have not said in
vain that I would do this evil unto them, finding that what I have said
is made good, and made to work for good, and to answer a good intention,
and that it was not without just provocation that they were thus
threatened and thus punished.\" Note, `(1.)` One way or other God will
make sinners to know and own that he is the lord, either by their
repentance or by their ruin. `(2.)` All true penitents are brought to
acknowledge both the equity and the efficacy of the word of God,
particularly the threatenings of the word, and to justify God in them
and in the accomplishment of them.

### Verses 11-14

The same threatenings which we had before in the foregoing chapter, and
in the former part of this, are here repeated, with a direction to the
prophet to lament them, that those he prophesied to might be the more
affected with the foresight of them.

`I.` He must by his gestures in preaching express the dep sense he had
both of the iniquities and of the calamities of the house of Israel (v.
11): Smite with thy hand and stamp with thy foot. Thus he must make it
to appear that he was in earnest in what he said to them, that he firmly
believed it and laid it to heart. Thus he must signify the just
displeasure he had conceived at their sins, and the just dread he was
under of the judgments coming upon them. Some would reject this use of
these gestures, and call them antic and ridiculous; but God bids him use
them because they might help to enforce the word upon some and give it
the setting on; and those that know the worth of souls will be content
to be laughed at by the wits, so they may but edify the weak. Two things
the prophet must thus lament:-1. National sins. Alas! for all the evil
abominations of the house of Israel. Note, The sins of sinners are the
sorrows of God\'s faithful servants, especially the evil abominations of
the house of Israel, whose sins are more abominable and have more evil
in them than the sins of others. Alas! What will be in the end hereof?
2. National judgments. To punish them for these abominations they shall
fall by the sword, by the famine, and by the pestilence. Note, It is our
duty to be affected not only with our own sins and sufferings, but with
the sins and sufferings of others; and to look with compassion upon the
miseries that wicked people bring upon themselves; as Christ beheld
Jerusalem and wept over it.

`II.` He must inculcate what he had said before concerning the
destruction that was coming upon them. 1. They shall be run down and
ruined by a variety of judgments which shall find them out and follow
them wherever they are (v. 12): He that is far off, and thinks himself
out of danger, because out of the reach of the Chaldeans\' arrows, shall
find himself not out of the reach of God\'s arrows, which fly day and
night (Ps. 91:5): He shall die of the pestilence. He that is near a
place of strength, which he hopes will be to him a place of safety,
shall fall by the sword, before he can retreat. He that is so cautious
as not to venture out, but remains in the city, shall there die by the
famine, the saddest death of all. Thus will God accomplish his fury,
that is, do all that against them which he had purposed to do. 2. They
shall read their sin in their punishment; for their slain men shall be
among their idols, round about their altars, as was threatened before,
v. 5-7. There, where they had prostrated themselves in honour of their
idols, God will lay them dead, to their own reproach and the reproach of
their idols. They lived among them and shall die among them. They had
offered sweet odours to their idols, but there shall their dead carcases
send forth an offensive smell, as it were to atone for that misplaced
incense. 3. The country shall be all laid waste, as, before, the cities
(v. 6): I will make the land desolate. That fruitful, pleasant, populous
country, that has been as the garden of the Lord, the glory of all
lands, shall be desolate, more desolate than the wilderness towards
Diblath, v. 14. It is called Diblathaim (Num. 33:46; Jer. 48:22), that
great and terrible wilderness which is described, Deu. 8:15, wherein
were fiery serpents and scorpions. The land of Canaan is at this day one
of the most barren desolate countries in the world. City and country are
thus depopulated, that the altars may be laid waste and made desolate,
v. 6. Rather than their idolatrous altars shall be left standing, both
town and country shall be laid in ruins. Sin is a desolating thing;
therefore stand in awe and sin not.
