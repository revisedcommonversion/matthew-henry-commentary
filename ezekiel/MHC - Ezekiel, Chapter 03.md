Ezekiel, Chapter 3
==================

Commentary
----------

In this chapter we have the further preparation of the prophet for the
work to which God called him. `I.` His eating the roll that was presented
to him in the close of the foregoing chapter (v. 1-3). `II.` Further
instructions and encouragements given him to the same purport with those
in the foregoing chapter (v. 4-11). `III.` The mighty impulse he was
under, with which he was carried to those that were to be his hearers
(v. 12-15). `IV.` A further explication of his office and business as a
prophet, under the similitude of a watchman (v. 16-21). `V.` The
restraining and restoring of the prophet\'s liberty of speech, as God
pleased (v. 22-27).

### Verses 1-15

These verses are fitly joined by some translators to the foregoing
chapter, as being of a piece with it and a continuation of the same
vision. The prophets received the word from God that they might deliver
it to the people of God, furnished themselves that they might furnish
them with the knowledge of the mind and will of God. Now here the
prophet is taught,

`I.` How he must receive divine revelation himself, v. 1. Christ (whom he
saw upon the throne, ch. 1:26) said to him, \"Son of man, eat this roll,
admit this revelation into thy understanding, take it, take the meaning
of it, understand it aright, admit it into thy heart, apply it, and be
affected with it; imprint it in thy mind, ruminate and chew the cud upon
it; take it as it is entire, and make no difficulty of it, nay, take a
pleasure in it as thou dost in thy meat, and let thy soul be nourished
and strengthened by it; let it be meat and drink to thee, and as thy
necessary food; be full of it, as thou art of the meat thou hast
eaten.\" Thus ministers should in their studies and meditations take in
that word of God which they are to preach to others. Thy words were
found, and I did eat them, Jer. 15:16. They must be both well acquainted
and much affected with the things of God, that they may speak of them
both clearly and warmly, with a great deal of divine light and heat. Now
observe, 1. How this command is inculcated upon the prophet. In the
foregoing chapter, Eat what I give thee; and here (v. 1), \"Eat that
thou findest, that which is presented to thee by the hand of Christ.\"
Note, Whatever we find to be the word of God, whatever is brought to us
by him who is the Word of God, we must receive it without disputing.
What we find set before us in the scripture, that we must eat. And again
(v. 3), \"Cause thy belly to eat, and fill thy bowels with this roll; do
not eat it and bring it up again, as that which is nauseous, but eat it
and retain it, as that which is nourishing and grateful to the stomach.
Feast upon this vision till thou be full of matter, as Elihu was, Job
32:18. Let the word have a place in thee, the innermost place.\" We must
take pains with our own hearts, that we may cause them duly to receive
and entertain the word of God, that every faculty may do its office, in
order to the due digesting of the word of God, that it may be turned in
succum et sanguinem-into blood and spirits. We must empty ourselves of
worldly things, that we may fill our bowels with this roll. 2. How this
command is explained (v. 10): \"All my words that I shall speak unto
thee, to be spoken unto the people, thou must receive in thy heart, as
well as hear with thy ears, receive them in the love of them.\" Let
these sayings sink down into your ears, Lu. 9:44. Christ demands the
prophet\'s attention not only to what he now says, but to all that he
shall at any time hereafter speak: Receive it all in thy heart; meditate
on these things and give thyself wholly to them, 1 Tim. 4:15. 3. How
this command was obeyed in vision. He opened his mouth and Christ caused
him to eat the roll, v. 2. If we be truly willing to receive the word
into our hearts, Christ will by his Spirit bring it into them and cause
it to dwell in us richly. If he that opens the roll, and by his Spirit,
as a Spirit of revelation, spreads it before us, did not also open our
understanding, and by his Spirit, as a Spirit of wisdom, give us the
knowledge of it and cause us to eat it, we should be for ever strangers
to it. The prophet had reason to fear that the roll would be an
unpleasant morsel and a sorry dish to make a meal of, but it proved to
be in his mouth as honey for sweetness. Note, if we readily obey even
the most difficult commands, we shall find that comfort in the
reflection which will make us abundant amends for all the hardships we
meet with in the way of our duty. Though the roll was filled with
lamentations, and mourning, and woe, yet it was to the prophet as honey
for sweetness. Note, Gracious souls can receive those truths of God with
great delight which speak most terror to wicked people. We find St. John
let into some part of the revelation by such a sign as this, Rev. 10:9,
10. He took the book out of the angel\'s hand, and ate it up, and it
was, as this, in his mouth sweet as honey; but it was bitter in the
belly; and we shall find that this was so too, for (v. 14) the prophet
went in bitterness.

`II.` How he must deliver that divine revelation to others which he
himself had received (v. 1): Eat this roll, and then go, speak to the
house of Israel. He must not undertake to preach the things of God to
others till he did himself fully understand them; let him not go without
his errand, nor take it by the halves. But when he does himself fully
understand them he must be both busy and bold to preach them for the
good of others. We must not conceal the words of the Holy One (Job
6:10), for that is burying a talent which was given us to trade with. He
must go and speak to the house of Israel; for it is their privilege to
have God\'s statutes and judgments made known to them; as the giving of
the law (the lively oracles), so prophecy (the living oracles) pertains
to them. He is not sent to the Chaldeans to reprove them for their sins,
but to the house of Israel to reprove them for theirs; for the father
corrects his own child if he do amiss, not the child of a stranger.

`1.` The instructions given him in speaking to them are much the same
with those in the foregoing chapter.

`(1.)` He must speak to them all that, and that only, which God spoke to
him. he had said before (ch. 2:7): Thou shalt speak my words to them;
here he says (v. 4), Thou shalt speak with my words unto them, or in my
words. He must not only say that which for substance is the same that
God had said to him, but as near as may be in the same language and
expressions. Blessed Paul, though a man of a very happy invention, yet
speaks of the things of God in the words which the Holy Ghost teaches, 1
Co. 2:13. Scripture truths look best in scripture language, their native
dress; and how can we better speak God\'s mind than with his words?

`(2.)` He must remember that they are the house of Israel whom he is sent
to speak to, God\'s house and his own; and therefore such as he ought to
have a particular concern for and to deal faithfully and tenderly with.
They were such as he had an intimate acquaintance with, being not only
their countryman, but their companion in tribulation; they and he were
fellow-sufferers, and had lately been fellow-travellers, in very
melancholy circumstances, from Judea to Babylon, and had often mingled
their tears, which could not but knit their affections to each other. It
was well for the people that they had a prophet who knew experimentally
how to sympathize with them, and could not but be touched with the
feeling of their infirmities. It was well for the prophet that he had to
do with those of his own nation, not with a people of strange speech and
a hard language, deep of lip, so that thou canst not fathom their
meaning, and heavy of tongue, whom it is intolerable and impossible to
converse with. Every strange language seems to us to be deep and heavy.
\"Thou art not sent to many such people, whom thou couldst neither speak
to nor hear from, neither understand nor be understood among but by an
interpreter.\" The apostles indeed were sent to many people of a strange
speech, but they could not have done any good among them if they had not
had the gift of tongues; but Ezekiel was sent only to one people, those
but a few, and his own, whom having acquaintance with he might hope to
find acceptance with.

`(3.)` He must remember what God had already told him of the bad character
of those to whom he was sent, that, if he met with discouragement and
disappointment in them, he might not be offended. They are impudent and
hard-hearted (v. 7), no convictions of sin would make them blush, no
denunciations of wrath would make them tremble. Two things aggravated
their obstinacy:-`[1.]` That they were more obstinate than their
neighbours would have been if the prophet had been sent to them. had God
sent him to any other people, though of a strange speech, surely they
would have hearkened to him; they would at least have given him a
patient hearing and shown him that respect which he could not obtain of
his own countrymen. The Ninevites were wrought upon by Jonah\'s
preaching when the house of Israel, that was compassed about with so
great a cloud of prophets, was unhumbled and unreformed. But what shall
we say to these things? The means of grace are given to those that will
not improve them and withheld from those that would have improved them.
We must resolve this into the divine sovereignty, and say, Lord, thy
judgments are a great deep. `[2.]` That they were obstinate against God
himself: \"They will not hearken unto thee, and no marvel, for they will
not hearken unto me;\" they will not regard the word of the prophet, for
they will not regard the rod of God, by which the Lord\'s voice cries in
the city. If they believe not God speaking to them by a minister,
neither would they believe though he should speak to them by a voice
from heaven; nay, therefore they reject what the prophet says, because
it comes from God, whom the carnal mind is enmity to. They are
prejudiced against the law of God, and for that reason turn a deaf ear
to his prophets, whose business it is to enforce his law.

`(4.)` He must resolve to put on courage, and Christ promises to steel him
with it, v. 8, 9. He is sent to such as are impudent and hard-hearted,
who will receive no impressions nor be wrought upon either by fair means
or foul, who will take a pride in affronting God\'s messenger and
confronting the message. It will be a hard task to know how to deal with
them; but, `[1.]` God will enable him to put a good face on it: \"I have
made thy face strong against their faces, endued thee with all the
firmness and boldness that the case calls for.\" Perhaps Ezekiel was
naturally bashful and timorous, but, if God did not find him fit, yet by
his grace he made him fit, to encounter the greatest difficulties. Note,
The more impudent wicked people are in their opposition to religion the
more openly and resolutely should God\'s people appear in the practice
and defence of it. let the innocent stir up himself against the
hypocrite, Job 17:8. When vice is daring let not virtue be sneaking.
And, when God has work to do, he will animate men for it and give them
strength according to the day. If there be occasion, God can and will by
his grace make the foreheads of faithful ministers as an adamant, so
that the most threatening powers shall not dash them out of countenance.
The Lord God will help men, therefore have I set my face like a flint,
Isa. 50:7. `[2.]` He is therefore commanded to have a good heart on it,
and to go on in his work with a holy security, not valuing either the
censures or the threats of his enemies: \"Fear not, neither be dismayed
at their looks; let not the menaces of their impotent malice cast either
a damp upon thee or a stumbling-block before thee.\" Bold sinners must
have bold reprovers; evil beasts must be rebuked cuttingly (Tit. 1:12,
13), must be saved with fear, Jude 23. Those that keep closely to the
service of God may be sure of the favour of God, and then they need not
be dismayed at the proud looks of men. Let not the angry countenance
that drives away a back-biting tongue give any check to a reproving
tongue.

`(5.)` He must continue instant with them in his preaching, whatever the
success was, v. 11. he must go to those of the captivity, who, being in
affliction, it was to be hoped would receive instruction; he must look
upon them as the children of his people, to whom he was nearly allied,
and for whom he therefore ought to have a very tender concern, as Paul
for his kinsmen, Rom. 9:3. And he must tell them not only what the Lord
said, but that the Lord said it; let him speak in God\'s name, and back
what he said with his authority: Thus saith the Lord God; tell them so,
whether they will hear or whether they will forbear. Not that it may be
indifferent to us what success our ministry has, but, whatever it be, we
must go on with our work and leave the issue to God. We must not say
\"Here are some so good that we do not need to speak to them,\" or,
\"Here are others so bad that it is to no purpose to speak to them;\"
but, however it be, deliver thy message faithfully, tell them, The Lord
God saith so and so, let them reject it at their peril.

`2.` Full instructions being thus given to the prophet, pursuant to his
commission, we are here told,

`(1.)` With what satisfaction this mission of his was applauded by the
holy angels, who were very well pleased to see one of a nature inferior
to their own thus honourable employed and entrusted. He heard a voice of
a great rushing (v. 12), as if the angels thronged and crowded to see
the inauguration of a prophet; for to them is known by the church (that
is, by reflection from the church) the manifold wisdom of God, Eph.
3:10. They seemed to strive who should get nearest to this great sight.
he heard the noise of their wings that touched, or (as the word is)
kissed one another, denoting the mutual affections and assistances of
the angels. He heard also the noise of the wheels of Providence moving
over-against the angels and in concert with them. All this was to engage
his attention and to convince him that the God who sent him, having such
a glorious train of attendants, no doubt had power sufficient to bear
him out in his work. But all this noise ended in the voice of praise. He
heard them saying, Blessed be the glory of the Lord from his place.
`[1.]` From heaven, his place above, whence his glory was now in vision
descending, or whither perhaps it was now returning. Let the innumerable
company of angels above join with those employed in this vision in
saying, Blessed be the glory of the Lord. Praise you the Lord from the
heavens. Praise him, all his angels, Ps. 148:1, 2. `[2.]` From the
temple, his place on earth, whence his glory was now departing. They
lament the departure of the glory, but adore the righteousness of God in
it: however it be, yet God is blessed and glorious, and ever will be so.
The prophet Isaiah heard God thus praised when he received his
commission (Isa. 6:3); and a comfort it is to all the faithful servants
of God, when they see how much God is dishonoured in this lower world,
to think how much he is admired and glorified in the upper world. The
glory of the Lord has many slights from our place, but many praises from
his place.

`(2.)` With what reluctance of his own spirit, and yet with what a mighty
efficacy of the Spirit of God, the prophet was himself brought to the
execution of his office. The grace given to him was not in vain; for,
`[1.]` The Spirit led him with a strong hand. God bade him go, but he
stirred not till the Spirit took him up. The Spirit of the living
creatures that was in the wheels now was in the prophet too, and took
him up, first to hear more distinctly the acclamations of the angels (v.
12), but afterwards (v. 14) lifted him up, and took him away to his
work, which he was backward to, being very loth either to bring trouble
upon himself or foretel it to his people. he would gladly have been
excused, but must own, as another prophet does (Jer. 20:7), Thou was
stronger than I, and hast prevailed. Ezekiel would willingly have kept
all he heard and saw to himself, that it might go no further, but the
hand of the Lord was strong upon him and overpowered him; he was carried
on contrary to his own inclinations by the prophetical impulse, so that
he could not but speak the things which he had heard and seen, as the
apostles, Acts 4:20. Note, Those whom God calls to the ministry, as he
furnishes their heads for it, so he bows their hearts to it. `[2.]` He
followed with a sad heart: The Spirit took me away, says he, and then I
went, but it was in bitterness, in the heat of my spirit. He had perhaps
seen what a hard task Jeremiah had at Jerusalem when he appeared as a
prophet, what pains he took, what opposition he met with, how he was
abused by hand and tongue, and what ill treatment he met with, and all
to no purpose. \"And\" (thinks Ezekiel) \"must I be set up for a mark
like him?\" The life of a captive was bad enough; but what would the
life of a prophet in captivity be? Therefore he went in this fret and
under this discomposure. Note, There may in some cases be a great
reluctance of corruption even where there is a manifest predominance of
grace. \"I went, not disobedient to the heavenly vision, or shrinking
from the work, as Jonah, but I went in bitterness, not at all pleased
with it.\" When he received the divine revelation himself, it was to him
sweet as honey (v. 3); he could with abundance of pleasure have spent
all his days in meditating upon it; but when he is to preach it to
others, who, he foresees, will be hardened and exasperated by it, and
have their condemnation aggravated, then he goes in bitterness. Note, It
is a great grief to faithful ministers, and makes them go on in their
work with a heavy heart, when they find people untractable and hating to
be reformed. he went in the heat of his spirit, because of the
discouragements he foresaw he should meet with; but the hand of the Lord
was strong upon him, not only to compel him to his work, but to fit him
for it, to carry him through it, and animate him against the
difficulties he would meet with (so we may understand it); and, when he
found it so, he was better reconciled to his business and applied
himself to it: Then he came to those of the captivity (v. 15), to some
place where there were many of them together, and sat where they sat,
working, or reading, or talking, and continued among them seven days to
hear what they said and observe what they did; and all that time he was
waiting for the word of the Lord to come to him. Note, Those that would
speak suitably and profitably to people about their souls must acquaint
themselves with them and with their case, must do as Ezekiel did here,
must sit where they sit, and speak familiarly to them of the things of
God, and put themselves into their condition, yea, though they sit by
the rivers of Babylon. But observe, He was there astonished, overwhelmed
with grief for the sins and miseries of his people and overpowered by
the pomp of the vision he had seen. he was there desolate (so some read
it); God showed him no visions, men made him no visit. Thus was he left
to digest his grief, and come to a better temper, before the word of the
Lord should come to him. Note, Those whom god designs to exalt and
enlarge he first humbles and straitens for a time.

### Verses 16-21

These further instructions God gave to the prophet at the end of seven
days, that is, on the seventh day after the vision he had; and it is
very probably that both that and this were on the sabbath day, which the
house of Israel, even in their captivity, observed as well as they could
in those circumstances. We do not find that their conquerors and
oppressors tied them to any constant service, as their Egyptian
task-masters had formerly done, but that they might observe the
sabbath-rest for a sign to distinguish between them and their
neighbours; but for the sabbath-work they had not the convenience of
temple or synagogue, only it should seem they had a place by the river
side where prayer was wont to be made (as Acts 16:13); there they met on
the sabbath day; there their enemies upbraided them with the songs of
Zion (Ps. 137:1, 3); there Ezekiel met them, and the word of the Lord
then and there came to him. He that had been musing and meditating on
the things of God all the week was fit to speak to the people in God\'s
name on the sabbath day, and disposed to hear God speak to him. This
sabbath day Ezekiel was not so honoured with visions of the glory of God
as he had been the sabbath before; but he is plainly, and by a very
common similitude, told his duty, which he is to communicate to the
people. Note, Raptures and transports of joy are not the daily bread of
God\'s children, however they may upon special occasions be feasted with
them. We must not deny but that we have truly communion with God (1 Jn.
1:3) though we have it not always so sensibly as at some times. And,
though the mysteries of the kingdom of heaven may sometimes be looked
into, yet ordinarily it is plain preaching that is most for edification.
God here tells the prophet what his office was, and what the duty of
that office; and this (we may suppose) he was to tell the people, that
they might attend to what he said and improve it accordingly. Note, It
is good for people to know and consider what a charge their ministers
have of them and what an account they must shortly give of that charge.
Observe,

`I.` What the office is to which the prophet is called: Son of man, I have
made thee a watchman to the house of Israel, v. 17. The vision he saw
astonished him: he knew not what to make of that, and therefore God used
this plain comparison, which served better to lead him to the
understanding of his work and so to reconcile him to it. he sat among
the captives, and said little, but God comes to him, and tells him that
will not do; he is a watchman, and has something to say to them; he is
appointed to be as a watchman in the city, to guard against fire,
robbers, and disturbers of the peace, as a watchman over the flock, to
guard against thieves and beasts of prey, but especially as a watchman
in the camp, in an invaded country or a besieged town, that is to watch
the motions of the enemy, and to sound an alarm upon the approach, nay,
upon the first appearance, of danger. This supposes the house of Israel
to be in a military state, and exposed to enemies, who are subtle and
restless in their attempts upon it; yea, and each of the particular
members of that house to be in danger and concerned to stand upon their
guard. Note, Ministers are watchmen on the church\'s walls (Isa. 62:6),
watchmen that go about the city, Cant. 3:3. It is a toilsome office.
Watchmen must keep awake, be they ever so sleepy, and keep abroad, be it
ever so cold; they must stand all weathers upon the watch-tower, Isa.
21:8; Gen. 31:40. It is a dangerous office. Sometimes they cannot keep
their post, but are in peril of death from the enemy, who gain their
point if they kill the sentinel; and yet they dare not quit their post
upon pain of death from their general. Such a dilemma are the church\'s
watchmen in; men will curse them if they be faithful, and God will curse
them if they be false. But it is a needful office; the house of Israel
cannot be safe without watchmen, and yet, except the Lord keep it, the
watchman waketh but in vain, Ps. 127:1, 2.

`II.` What is the duty of this office. The work of a watchman is to take
notice and to give notice.

`1.` The prophet, as a watchman, must take notice of what God said
concerning this people, not only concerning the body of the people, to
which the prophecies of Jeremiah and other prophets had most commonly
reference, but concerning particular persons, according as their
character was. He must not, as other watchmen, look round to spy danger
and gain intelligence, but he must look up to God, and further he need
not look: Hear the word at my mouth, v. 17. Note, Those that are to
preach must first hear; for how can those teach others who have not
first learned themselves?

`2.` He must give notice of what he heard. As a watchman must have eyes
in his head, so he must have a tongue in his head; if he be dumb, it is
as bad as if he were blind, Isa. 56:10. Thou shalt give them warning
from me, sound an alarm in the holy mountain; not in his own name, or as
from himself, but in God\'s name, and from him. Ministers are God\'s
mouth to the children of men. The scriptures are written for our
admonition. By them is thy servant warned, Ps. 19:11. But, because that
which is delivered vivâ voce-by the living voice, commonly makes the
deepest impression, God is pleased, by men like ourselves, who are
equally concerned, to enforce upon us the warnings of the written word.
Now the prophet, in his preaching, must distinguish between the wicked
and the righteous, the precious and the vile, and in his applications
must suit his alarms to each, giving every one his portion; and, if he
did this, he should have the comfort of it, whatever the success was,
but, if not, he was accountable.

`(1.)` Some of those he had to do with were wicked, and he must warn them
not to go on in their wickedness, but to turn from it, v. 18, 19. We may
observe here, `[1.]` That the God of heaven has said, and does say, to
every wicked man, that if he go on still in his trespasses he shall
surely die. His iniquity shall undoubtedly be his ruin; it tends to ruin
and will end in ruin. Dying thou shalt die, thou shalt die so great a
death, shalt die eternally, be ever dying, but never dead. The wicked
man shall die in his iniquity, shall fie under the guilt of it, die
under the dominion of it. `[2.]` That if a wicked man turn from his
wickedness, and from his wicked way, he shall live, and the ruin he is
threatened with shall be prevented; and, that he may do so, he is warned
of the danger he is in. The wicked man shall die if he go on, but shall
live if he repent. Observe, he is to turn from his wickedness and from
his wicked way. It is not enough for a man to turn from his wicked way
by an outward reformation, which may be the effect of his sins leaving
him rather than of his leaving his sins, but he must turn from his
wickedness, from the love of it and the inclination to it, by an inward
regeneration; if he do not so much as turn from his wicked way, there is
little hope that he will turn from his wickedness. `[3.]` That it is the
duty of ministers both to warn sinners of the danger of sin and to
assure them of the benefit of repentance, to set before them how
miserable they are if they go on in sin, and how happy they may be if
they will but repent and reform. Note, The ministry of the word is
concerning matters of life and death, for those are the things it sets
before us, the blessing and the curse, that we may escape the curse and
inherit the blessing. `[4.]` That, though ministers do not warn wicked
people as they ought of their misery and danger, yet that shall not be
admitted as an excuse for those that go on still in their trespasses;
for, though the watchman did not give them warning, yet they shall die
in their iniquity, for they had sufficient warning given them by the
providence of God and their own consciences; and, if they would have
taken it, they might have saved their lives. `[5.]` That if ministers be
not faithful to their trust, if they do not warn sinners of the fatal
consequences of sin, but suffer them to go on unreproved, the blood of
those that perish through their carelessness will be required at their
hand. It shall be charged upon them in the day of account that it was
owing to their unfaithfulness that such and such precious souls perished
in sin; for who knows but if they had had fair warning given them they
might have fled in time from the wrath to come? And, if it contract so
heinous a guilt as it does to be accessory to the murder of a dying
body, what is it to be accessory to the ruin of an immortal soul? `[6.]`
That if ministers do their duty in giving warning to sinners, though the
warning be not taken, yet they may have this satisfaction, that they are
jclear from their blood, and have delivered their own souls, though they
cannot prevail to deliver theirs. Those that are faithful shall have
their reward, though they be not successful.

`(2.)` Some of those he had to deal with were righteous, at least he had
reason to think, in a judgment of charity, that they were so; and he
must warn them not to apostatize and turn away from their righteousness,
v. 20, 21. We may observe here, `[1.]` That the best men in the world
have need to be warned against apostasy, and to be told of the danger
they are in of it and the danger they are in by it. God\'s servants must
be warned (Ps. 19:11) that they do not neglect his work and quit his
service. One good means to keep us from falling is to keep up a holy
fear of falling, Heb. 4:1. Let us therefore fear; and (Rom. 11:20) even
those that stand by faith must not be high-minded, but fear, and must
therefore be warned. `[2.]` There is a righteousness which a man may
turn from, a seeming righteousness, and, if men turn from this, it
thereby appears that it was never sincere, how passable, nay, how
plausible soever it was; for, if they had been of us, they would no
doubt have continued with us, 1 Jn. 2:19. There are many that begin in
the spirit, but end in the flesh, that set their faces heavenward, but
look back; that had a first love, but have lost it, and turned from the
holy commandment. `[3.]` When men turn from their righteousness they
soon learn to commit iniquity. When they grow careless and remiss in the
duties of God\'s worship, neglect them, or a re negligent in them, they
become an easy prey to the tempter. Omissions make way for commissions.
`[4.]` When men turn from their righteousness, and commit iniquity, it
is just with God to lay stumbling-blocks before them, that they may grow
worse and worse, till they are ripened for destruction. When Pharaoh
hardened his heart God hardened it. When sinners turn their back upon
God, desert his service, and so cast a reproach upon it, he does, in a
way of righteous judgment, not only withdraw his restraining grace and
give them up to their own hearts\' lusts, but order them by his
providence into such circumstances as occasion their sin and hasten
their ruin. There are those to whom Christ himself is a stone of
stumbling and a rock of offence, 1 Pt. 2:8. `[5.]` The righteousness
which men relinquish shall never be remembered to their honour or
comfort; it will stand them in no stead in this world or the other.
Apostates lose all that they have wrought; their services and sufferings
are all in vain, and shall never be brought to an account, because not
continued in. It is a rule in the law, Factum non dicitur, quod non
perseverat-We are said to do only that which we do perseveringly, Gal.
3:3, 4. `[6.]` If ministers do no give fair warning, as they ought, of
the weakness of the best, their aptness to stumble and fall, the
particular temptations they are in and the fatal consequences of
apostasy, the ruin of those that do apostatize will be laid at their
door, and they shall answer for it. Not but that there are those who are
warned against it, and yet turn from their righteousness; but that case
is not put here, as was concerning the wicked man, but, on the contrary,
that a righteous man, being warned, takes the warning and does not sin
(v. 21); for, if you give instruction to a wise man, he will be yet
wiser. We must not only not flatter the wicked, but not flatter even the
righteous as if they were perfectly safe any where on this side heaven.
`[7.]` If ministers give warning, and people take it, it is well for
both. Nothing is more beautiful than a wise reprover upon an obedient
ear; the one shall live because he is warned and the other has delivered
his soul. What can a good minister desire more than to save himself and
those that hear him? 1 Tim. 4:16.

### Verses 22-27

After all this large and magnificent discovery which God had made of
himself to the prophet, and the full instructions he had given him how
to deal with those to whom he sent him with an ample commission, we
should have expected presently to see him preaching the word of God to a
great congregation of Israel; but here we find it quite otherwise. his
work here, at first, seems not at all proportionable to the pomp of his
call.

`I.` We have him here retired for further learning. By his unwillingness
to go it should seem as if he were not so thoroughly convinced as he
might have been of the ability of him that sent him to bear him out; and
therefore, to encourage him against the difficulties he foresaw, God
will favour him with another vision of his glory, which (if any thing)
would put life into him and animate him for his work. In order for this,
God calls him out to the plain (v. 22) and there he will have some talk
with him. See and admire the condescension of God in conversing thus
familiarly with a man, a son of man, a poor captive, nay, with a sinful
man, who, when God sent him went in bitterness of spirit, and was at
this time out of humour with his work. And let us own ourselves for ever
indebted to the mediation of Christ for this blessed intercourse and
communion between God and man, between heaven and earth. See here the
benefit of solitude, and how much it befriends contemplation. It is very
comfortable to be alone with God, withdrawn from the word for converse
with him, to hear from him, to speak to him; and a good man will say
that he is never less along than when thus alone. Ezekiel went forth
into the plain more willingly than he went among those of the captivity
(v. 15); for those that know what it is to have communion with God
cannot but prefer that before any converse with this world, especially
such as is commonly met with. He went out into the plain, and there he
saw the same vision that he had seen by the river of Chebar; for God is
not tied to places. Note, Those who follow God shall meet with his
consolations, wherever they go. God called him out to talk with him, but
did more than that: he showed him his glory, v. 23. We are not now to
expect such visions, but we must own that we have a favour done us no
way inferior if we so by faith behold the glory of the Lord as to be
changed into the same image, by the Spirit of the Lord; and this honour
have all his saints. Praise you the Lord, 2 Co. 3:18.

`II.` We have him here restrained from further teaching for the present.
When he saw the glory of the Lord he fell on his face, being struck with
an awe of God\'s majesty and a dread of his displeasure; but the Spirit
entered into him to raise him up, and then he recovered himself and got
upon his feet and heard what the Spirit whispered to him, which is very
surprising. One would have expected now that God would send him directly
to the chief place of concourse, would give him favour in the eyes of
his brethren, and make him and his message acceptable to them, that he
would have a wider door of opportunity opened to him and that God would
give him a door of utterance to open his mouth boldly; but what is here
said to him is the reverse of all this.

`1.` Instead of sending him to a public assembly, he orders him to
confine himself to his own lodgings: Go, shut thyself within thy house,
v. 24. He was not willing to appear in public, and, when he did, the
people did not regard him, nor show him the respect he deserved, and as
a just rebuke both to him and them, to him for his shyness of them and
to them for their coldness towards him, God forbids him to appear in
public. Note, Our choice is often made our punishment; and it is a
righteous thing with God to remove teachers into corners when they, or
their people, or both, grow indifferent to solemn assemblies. Ezekiel
must shut up himself, some think, to give a sign of the besieging of
Jerusalem, in which the people should be closely shut up as he was in
his house, and which he speaks of in the next chapter. He must shut
himself within his house, that he might receive further discoveries of
the mind of God and might abundantly furnish himself with something to
say to the people when he went abroad. We find that the elders of Judah
visited him and sat before him sometimes in his house (ch. 8:1), to be
witnesses of his ecstasies; but it was not till ch. 11:25 that he spoke
to those of the captivity all the things that the Lord had shown him.
Note, Those that are called to preach must find time to study, and a
great deal of time too, must often shut themselves up in their houses,
that they may give attendance to reading and meditation, and so their
profiting may appear to all.

`2.` Instead of securing him an interest in the esteem and affections of
those to whom he sent him he tells him that they shall put bands upon
him and bind him (v. 25), either `(1.)` As a criminal. They shall bind him
in order to the further punishing of him as a disturber of the peace;
though they were themselves sent into bondage in Babylon for persecuting
the prophets, yet there they continue to persecute them. Or, rather,
`(2.)` As a distracted man. They would go about to bind him as one beside
himself; for to that they imputed his violent motions in his raptures.
The captains asked Jehu, Wherefore came this mad fellow unto thee?
Festus said to Paul, Thou art beside thyself; and so the Jews said of
our Lord Jesus, mark 3:21. Perhaps this was the reason why he must keep
within doors, because otherwise they would bind him, under pretence of
his being mad, and therefore he must not go out among them. Justly are
prophets forbidden to go to those that will abuse them.

`3.` Instead of opening his lips that his mouth might show forth God\'s
praise, God silence him, made his tongue cleave to the roof of his
mouth, so that he was dumb for a considerable time, v. 26. The pious
captives in Babylon used this imprecation upon themselves, that, if they
should forget Jerusalem, there tongue might cleave to the roof of their
mouth, Ps. 137:6. Ezekiel remembers Jerusalem more than any of them, and
yet his tongue cleaves to the roof of his mouth, and he that can speak
best is forbidden to speak at all; and the reason given is because they
are a rebellious house to whom he is sent, and they are not worthy to
have him for a reprover. He shall not give them instructions and
admonitions, for they are lost and thrown away upon them. he is before
commanded to speak boldly to them because they are most rebellious (ch.
2:7); but, since that proves to no purpose, he is now for that reason
enjoined silence and shall not speak at all to them. Note, Those whose
hearts are hardened against conviction are justly deprived of the mans
of conviction. Why should not the reprovers be dumb, if, after long
trials, it be found that the reproved resolve to be deaf? If Ephraim be
joined to idols, let him alone. Thou shalt be dumb, and not be a
reprover, implying that unless he were dumb he would be reproving; if he
could speak at all, he would witness against the wickedness of the
wicked. But when God speaks with him, and designs to speak by him, he
will open his mouth, v. 27. Note, Though God\'s prophets may be silenced
awhile, there will come a time when God will give them the opening of
the mouth again. And, when God speaks to his ministers, he not only
opens their ears to hear what he says, but opens their mouth to return
an answer. Moses, who had a veil on his face when he went down to the
people, took it off when he went up again to God, Ex. 34:34.

`4.` Instead of giving him assurance of success when he should at any
time speak to the people, he here leaves the matter very doubtful, and
Ezekiel must not perplex and disquiet himself about it, but let it be as
it will. He that hears, let him hear, and he is welcome to the comfort
of it; let him hear, and his soul shall live; but he that forbears, let
him forbear at his peril, and take what comes. If thou scornest, thou
alone shalt bear it; neither God nor his prophet shall be any losers by
it; but the prophet shall be rewarded for his faithfulness in reproving
the sinner, and God will have the glory of his justice in condemning him
for not taking the reproof.
