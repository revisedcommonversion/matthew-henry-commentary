Ezekiel, Chapter 35
===================

Commentary
----------

It was promised, in the foregoing chapter, that when the time to favour
Zion, yea, the set time, should come, especially the time for sending
the Messiah and setting up his kingdom in the world, God would cause the
enemies of his church to cease and the blessings and comforts of the
church to abound. This chapter enlarges upon the former promise,
concerning the destruction of the enemies of the church; the next
chapter upon the latter promise, the replenishing of the church with
blessings. Mount Seir (that is, Edom) is the enemy prophesied against in
this chapter, but fitly put here, as in the prophecy of Obadiah, for all
the enemies of the church; for, as those all walked in the way of Cain
that hated Abel, so those all walked in the way of Esau who hated Jacob,
but over whom Jacob, by virtue of a particular blessing, was to have
dominion. Now here we have, `I.` The sin charged upon the Edomites, and
that was their spite and malice to Israel (v. 5, 10-13). `II.` The ruin
threatened, that should come upon them for this sin. God will be against
them (v. 3) and then their country shall be laid waste (v. 4),
depopulated, and made quite desolate (v. 6-9), and left so when other
nations that had been wasted should recover themselves (v. 14, 15).

### Verses 1-9

Mount Seir was mentioned as partner with Moab in one of the threatenings
we had before (ch. 25:8); but here it is convicted and condemned by
itself, and has woes of its own. The prophet must boldly set his face
against Edom, and prophesy particularly against it; for the God of
Israel has said, O Mount Seir! I am against thee. Note, Those that have
God against them have the word of God against them, and the face of his
ministers, nor dare they prophesy any good to them, but evil. The
prophet must tell the Edomites that God has a controversy with them, and
let them know,

`I.` What is the cause and ground of that controversy, v. 5. God espouses
his people\'s cause, and will plead it, takes what is done against them
as done against himself, and will reckon for it; and it is upon their
account that God now contends with the Edomites. 1. Because of the
enmity they had against the people of God, that was rooted in the heart.
\"Thou hast had a perpetual hatred to them, to the very name of an
Israelite.\" The Edomites kept up an hereditary malice against Israel,
the same that Esau bore to Jacob, because he got the birth-right and the
blessing. Esau had been reconciled to Jacob, had embraced and kissed him
(Gen. 33), and we do not find that ever he quarrelled with him again.
But the posterity of Esau would never be reconciled to the seed of
Jacob, but hated them with a perpetual hatred. Note, Children will be
more apt to imitate the vices than the virtues of their parents, and to
tread in the steps of their sin than in the steps of their repentance.
Parents should therefore be careful not to set their children any bad
example, for though, through the grace of God, they may return, and
prevent the mischief of what they have done amiss to themselves, they
may not be able to obviate the bad influence of it upon their children.
It is strange how deeply rooted national antipathies sometimes are, and
how long they last; but it is not to be wondered at that profane
Edomites hate pious Israelites, since the old enmity that was put
between the seed of the woman and the seed of the serpent (Gen. 3:15)
will continue to the end. Marvel not if the world hate you. 2. Because
of the injuries they had done to the people of God. They shed their
blood by the force of the sword, in the time of their calamity; they did
not attack them as fair and open enemies, but laid wait for them, to cut
off those of them that had escaped (Obad. 14), or they drove them back
upon the sword of the pursuers, by which they fell. It was cowardly, as
well as barbarous, to take advantage of their distress; and for
neighbours, with whom they had lived peaceably, to smite them secretly
when strangers openly invaded them. It was in the time that their
iniquity had an end, when the measure of it was full and destruction
came. Note, Even those that suffer justly, and for their sins, are yet
to be pitied and not trampled upon. If the father corrects one child, he
expects the rest should tremble at it, not triumph in it.

`II.` What should be the effect and issue of that controversy. If God
stretch out his hand against the country of Edom, he will make it most
desolate, v. 3. Desolation and desolation. 1. The inhabitants shall be
slain with the sword (v. 6): I will prepare thee unto blood. Edom shall
be gradually weakened, and so be the more easily conquered, and the
enemy shall gather strength the more effectually to subdue it. Thus
preparation is in the making a great while before for this destruction.
Thou hast not hated blood; it implies, \"Thou hast delighted in it and
thirsted after it.\" Those that do not keep up a rooted hatred of sin,
when a temptation to it is very strong, will be in danger of yielding to
it. Some read it, \"Unless thou hatest blood\" (that is, \"unless thou
dost repent, and put off this bloody disposition) blood shall pursue
thee.\" And then it is an intimation that the judgment may yet be
prevented by a thorough reformation. If he turn not, he will whet his
sword, Ps. 7:12. But, if he turn, he will lay it by. Blood shall pursue
thee, the guilt of the blood which thou hast shed or the judgment of
blood; thy blood-thirsty enemies shall pursue thee, which way soever
thou seekest to make thy escape. A great and general slaughter shall be
made of the Idumeans, such as had been foretold (Is. 34:6): The
mountains and hills, the valleys and rivers, shall be filled with the
slain, v. 8. The pursuers shall overtake those that flee and shall give
no quarter, but put them all to the sword. Note, When God comes to make
inquisition for blood those that have shed the blood of his Israel shall
have blood given them to drink, for they are worthy. Satia te sanguine
quem sitisti-Glut thyself with blood, after which thou hast thirsted. 2.
The country shall be laid waste. The cities shall be destroyed (v. 4),
the country made most desolate (v. 7); for God will cut off from both
him that passes out and him that returns; and when the inhabitants are
cut off that should keep the cities in repair they will decay and go
into ruins, and when those are cut off that should till the land that
will soon be over-run with briers and thorns and become a wilderness.
Note, Those that help forward the desolations of Israel may expect to be
themselves made desolate. And that which completes the judgment is that
Edom shall be made perpetual desolations (v. 9) and the cities shall
never return to their former state, nor the inhabitants of them come
back from their captivity and dispersion. Note, Those that have a
perpetual enmity to God and his people, as the carnal mind has, can
expect no other than to be made a perpetual desolation. Implacable
malice will justly be punished with irreparable ruin.

### Verses 10-15

Here is, `I.` A further account of the sin of the Edomites, and their bad
conduct towards the people of God. We find the church complaining of
them for setting on the Babylonians, and irritating them against
Jerusalem, saying, Rase it, rase it, down with it, down with it (Ps.
137:7), inflaming a rage that needed no spur; here it is further charged
upon them that they triumphed in Jerusalem\'s ruin and in the
desolations of the country. Many blasphemies they spoke against the
mountains of Israel, saying, with pride and pleasure, They are laid
desolate, v. 12. Note, The troubles of God\'s church, as they give
proofs of the constancy and fidelity of its friends, so they discover
and draw out the corruptions of its enemies, in whom there then appears
more brutish malice than one would have thought of. Now their triumphing
in Jerusalem\'s ruin is here said to proceed, 1. From a sinful passion
against the people of Israel; from anger and envy, and hatred against
them (v. 11), that perpetual hatred spoken of v. 5. Though they were not
a match for them, and therefore could not do them a mischief themselves,
yet they were glad when the Chaldeans did them a mischief. 2. From a
sinful appetite to the land of Israel. They pleased themselves with
hopes that when the people of Israel were destroyed they should be let
into the possession of their country, which they had so often grudged
and envied them. They thought they could make out something of a title
to it, ob defectum sanguinis-for want of other heirs. If Jacob\'s issue
fail, they think that they are next in the entail, and that the
remainder will be to his brother\'s issue: \"These two nations of Judah
and Israel shall be mine. Now is the time for me to put in for them.\"
At least they hope to come in as first occupants, being near neighbours:
We will possess it when it is deserted. Ceditur occupanti-Let us get
possession and that will be title enough. Note, Those have the spirit of
Edomites who desire the death of others because they hope to get by it,
or are pleased with their failing because they expect to come into their
business. When we see the vanity of the world in the disappointments,
losses, and crosses, that others meet with in it, instead of showing
ourselves, upon such an occasion, greedy of it, we should rather be made
thereby to sit more loose to it, and both take our affections off it and
lower our expectations from it. But in this case of the Edomites\'
coveting the land of Israel, and gaping for it, there was a particular
affront to God, when they said, \"These lands are given us to devour,
and we shall have our bellies full of their riches.\" God says, You have
boasted against me and have multiplied your words against me; for they
expected possession upon a vacancy, because Israel was driven out,
whereas the Lord was still there, v. 10. His temple indeed was burnt,
and the other tokens of his presence were gone; but his promise to give
that land to the seed of Jacob for an inheritance was not made void, but
remained in full force and virtue; and by that promise he did in effect
still keep possession for Israel, till they should in due time be
restored to it. That was Immanuel\'s land (Isa. 8:8); in that land he
was to be born, and therefore that people shall continue in it of whom
he is to be born, till he has passed his time in it, and then let who
will take it. The Lord is there, the Lord Jesus is to be there; and
therefore Israel\'s discontinuance of possession is no defeasance of
their right, but it shall be kept for them, and they shall have, hold,
and enjoy it by virtue of the divine grant, till the promise of this
Canaan shall by the Messiah be changed into the promise of a far better.
Note, It is a piece of presumption highly offensive to God for Edomites
to lay claim to those privileges and comforts that are peculiar to
God\'s chosen Israel and are reserved for them. It is blasphemy against
the mountains of Israel, the holy mountains, to say, because they are
for the present made a prey of and trodden under foot of the Gentiles
(Rev. 11:2), even the holy city itself, that therefore the Lord has
forsaken them, their God has forgotten them. The apostle will by no
means admit such a thought as this, that God hath cast away his people,
Rom. 11:1. No; though they are cast down for a time, they are not cast
off for ever. Those reproach the Lord who say they are.

`II.` The notice God took of the barbarous insolence of the Edomites, and
the doom passed upon them for it: I have heard all thy blasphemies, v.
12. And again (v. 13), You have multiplied your words against me, and I
have heard them, I have observed them, I have kept an account of them.
Note, In the multitude of words, not one escapes God\'s cognizance; let
men speak ever so much, ever so fast, though they multiply words, which
they themselves regard not, but forget immediately, yet none of them are
lost in the crowd, not the most idle words; but God hears them, and will
be able to charge the sinner with them. All the haughty and hard
speeches, particularly, which are spoken against the Israel of God, the
words which are magnified (as it is in the margin, v. 13) as well as the
words which are multiplied, God takes notice of. For, as the most
trifling words are not below his cognizance, so the most daring are not
above his rebuke. I have heard all thy blasphemies. This is a good
reason why we should bear reproach as if we heard it not, because God
will hear, Ps. 38:13, 15. God has heard the Edomites\' blasphemy; let
them therefore hear their doom, v. 14, 15. It was a national sin (the
blasphemies charged upon them were the sense and language of all the
Edomites), and therefore shall be punished with a national desolation.
And, 1. It shall be a distinguishing punishment. As God has peculiar
favours for Israelites, so he has peculiar plagues for Edomites: so that
\"When the whole earth rejoices I will make thee desolate; when other
nations have their desolations repaired, to their joy, thine shall be
perpetual,\" v. 9. 2. The punishment shall answer to the sin: \"As thou
didst rejoice in the desolation of the house of Israel, God will give
thee enough of desolation; since thou art so fond of it, thou shalt be
desolate; I will make thee so.\" Note, Those who, instead of weeping
with the mourners, make a jest of their grievances, may justly be made
to weep like the mourners, and themselves to feel the weight, to feel
the smart, of those grievances which they set so light by. Some read v.
14 so as to complete the resemblance between the sin and the punishment:
The whole earth shall rejoice when I make thee desolate, as thou didst
rejoice when Israel was made desolate. Those that are glad at the death
and fall of others may expect that others will be glad of their death,
of their fall. 3. In the destruction of the enemies of the church God
designs his own glory, and we may be sure that he will not come short of
his design. `(1.)` That which he intends is to manifest himself, as a just
and jealous God, firm to his covenant and faithful to his people and
their injured cause (v. 11): I will make myself known among them when I
have judged thee. The Lord is and will be known by the judgments which
he executes. `(2.)` His intention shall be fully answered; not only his
own people shall be made to know it to their comfort, but even the
Edomites themselves, and all the other enemies of his name and people,
shall know that he is the Lord, v. 4, 9, 15. As the works of creation
and common providence demonstrate that there is a God, so the care taken
of Israel shows that Jehovah, the God of Israel, is that God alone, the
true and living God.
