Ezekiel, Chapter 40
===================

Commentary
----------

The waters of the sanctuary which this prophet saw in vision (47:1) are
a proper representation of this prophecy. Hitherto the waters have been
sometimes but to the ankles, in other places to the knees, or to the
loins, but now the waters have risen, and have become \"a river which
cannot be passed over.\" Here is one continued vision, beginning at this
chapter, to the end of the book, which is justly looked upon to be one
of the most difficult portions of scripture in all the book of God. The
Jews will not allow any to read it till they are thirty years old, and
tell those who do read it that, though they cannot understand every
thing in it, \"when Elias comes he will explain it.\" Many commentators,
both ancient and modern, have owned themselves at a loss what to make of
it and what use to make of it. But because it is hard to be understood
we must not therefore throw it by, but humbly search concerning it, get
as far as we can into it and as much as we can out of it, and, when we
despair of satisfaction in every difficulty we meet with, bless God that
our salvation does not depend upon it, but that things necessary are
plain enough, and wait till God shall reveal even this unto us. These
chapters are the more to be regarded because the last two chapters of
the Revelation seem to have a plain allusion to them, as Rev. 20 has to
the foregoing prophecy of Gog and Magog. Here is the vision of a
glorious temple (in this chapter and ch. 41 and 42), of God\'s taking
possession of it (ch. 43), orders concerning the priests that are to
minister in this temple (ch. 44), the division of the land, what portion
should be allotted for the sanctuary, what for the city, and what for
the prince, both in his government of the people and his worship of God
(ch. 45), and further instructions for him and the people, ch. 46. After
the vision of the holy waters we have the borders of the holy land, and
the portions assigned to the tribes, and the dimensions and gates of the
holy city, ch. 47, 48. Some make this to represent what had been during
the flourishing state of the Jewish church, how glorious Solomon\'s
temple was in its best days, that the captives might see what they had
lost by sin and might be the more humbled. But that seems not probable.
The general scope of it I take to be, 1. To assure the captives that
they should not only return to their own land, and be settled there,
which had been often promised in the foregoing chapters, but that they
should have, and therefore should be encouraged to build, another
temple, which God would own, and where he would meet them and bless
them, that the ordinances of worship should be revived, and the sacred
priesthood should there attend; and, though they should not have a king
to live in such splendour as formerly, yet they should have a prince or
ruler (who is often spoken of in this vision), who should countenance
the worship of God among them and should himself be an example of
diligent attendance upon it, and that prince, priests, and people,
should have a very comfortable settlement and subsistence in their own
land. 2. To direct them to look further than all this, and to expect the
coming of the Messiah, who had before been prophesied of under the name
of David because he was the man that projected the building of the
temple and that should set up a spiritual temple, even the
gospel-church, the glory of which should far exceed that of Solomon\'s
temple, and which should continue to the end of time. The dimensions of
these visionary buildings being so large (the new temple more spacious
than all the old Jerusalem and the new Jerusalem of greater extent than
all the land of Canaan) plainly intimates, as Dr. Lightfoot observes,
that these things cannot be literally, but must spiritually, understood.
At the gospel-temple, erected by Christ and his apostles, was so closely
connected with the second material temple, was erected so carefully just
at the time when that fell into decay, that it might be ready to receive
its glories when it resigned them, that it was proper enough that they
should both be referred to in one and the same vision. Under the type
and figure of a temple and altar, priests and sacrifices, is foreshown
the spiritual worship that should be performed in gospel times, more
agreeable to the nature both of God and man, and that perfected at last
in the kingdom of glory, in which perhaps these visions will have their
full accomplishment, and some think in some happy and glorious state of
the gospel-church on this side heaven, in the latter days.

In this chapter we have, `I.` A general account of this vision of the
temple and city (v. 1-4). `II.` A particular account of it entered upon;
and a description given, 1. Of the outside wall (v. 5). 2. Of the east
gate (v. 6-19). 3. Of the north gate (v. 20-23). 4. Of the south gate
(v. 24-31) and the chambers and other appurtenances belonging to these
gates. 5. Of the inner court, both towards the east and towards the
south (v. 32-38). 6. Of the tables (v. 39-43). 7. Of the lodgings for
the singers and the priests (v. 44-47). 8. Of the porch of the house (v.
48, 49).

### Verses 1-4

Here is, 1. The date of this vision. It was in the twenty-fifth year of
Ezekiel\'s captivity (v. 1), which some compute to be the thirty-third
year of the first captivity, and is here said to be the fourteenth year
after the city was smitten. See how seasonably the clearest and fullest
prospects of their deliverance were given, when they were in the depth
of their distress, and an assurance of the return of the morning when
they were in the midnight of their captivity: \"Then the hand of the
Lord was upon me and brought me thither to Jerusalem, now that it was in
ruins, desolate and deserted\"-a pitiable sight to the prophet. 2. The
scene where it was laid. The prophet was brought, in the visions of God,
to the land of Israel, v. 2. And it was not the first time that he had
been brought thither in vision. We had him carried to Jerusalem to see
it in its iniquity and shame (ch. 8:3); here he is carried thither to
have a pleasing prospect of it in its glory, though its present aspect,
now that it was quite depopulated, was dismal. He was set upon a very
high mountain, as Moses upon the top of Pisgah, to view this land, which
was now a second time a land of promise, not yet in possession. From the
top of this mountain he saw as the frame of a city, the plan and model
of it; but this city was a temple as large as a city. The New Jerusalem
(Rev. 21:22) had no temple therein; this which we have here is all
temple, which comes much to one. It is a city for men to dwell in; it is
a temple for God to dwell in; for in the church on earth God dwells with
men, in that in heaven men dwell with God. Both these are framed in the
counsel of God, framed by infinite wisdom, and all very good. 3. The
particular discoveries of this city (which he had at first a general
view of) were made to him by a man whose appearance was like the
appearance of brass (v. 3), not a created angel, but Jesus Christ, who
should be found in fashion as a man, that he might both discover and
build the gospel-temple. He brought him to this city, for it is through
Christ that we have both acquaintance with and access to the benefits
and privileges of God\'s house. He it is that shall build the temple of
the Lord, Zec. 6:13. His appearing like brass intimates both his
brightness and his strength. John, in vision, saw his feet like unto
fine brass, Rev. 1:15. 4. The dimensions of this city or temple, and the
several parts of it, were taken with a line of flax and a measuring
reed, or rod (v. 3), as carpenters have both their line and a wooden
measure. The temple of God is built by line and rule; and those that
would let others into the knowledge of it must do it by that line and
rule. The church is formed according to the scripture, the pattern in
the mount. That is the line and the measuring reed that is in the hand
of Christ. With that doctrine and laws ought to be measured, and
examined by that; for then peace is upon the Israel of God when they
walk according to that rule. 5. Directions are here given to the prophet
to receive this revelation from the Lord and transmit it pure and entire
to the church, v. 4. `(1.)` He must carefully observe every thing that was
said and done in this vision. His attention is raised and engaged (v.
4): \"Behold with thy eyes all that is shown thee (do not only see it,
but look intently upon it), and hear with thy ears all that is said to
thee; diligently hearken to it, and be sure to set thy heart upon it;
attend with a fixedness of thought and a close application of mind.\"
What we see of the works of God, and what we hear of the word of God,
will do us no good unless we set out hearts upon it, as those that
reckon ourselves nearly concerned in it, and expect advantage to our
souls by it. `(2.)` He must faithfully declare it to the house of Israel,
that they may have the comfort of it. Therefore he receives, that he may
give. Thus the Revelation of Jesus Christ was lodged in the hands of
John, that he might signify it to the churches, Rev. 1:1. And, because
he is to declare it as a message from God, he must therefore be fully
apprised of it himself and much affected with it. Note, Those who are to
preach God\'s word to others ought to study it well themselves and set
their hearts upon it. Now the reason given why he must both observe it
himself and declare it to the house of Israel is because to this intent
he is brought hither, and has it shown to him. Note, When the things of
God are shown to us it concerns us to consider to what intent they are
shown to us, and, when we are sitting under the ministry of the word, to
consider to what intent we are brought thither, that we may answer the
end of our coming, and may not receive the grace of God, in showing us
such things, in vain.

### Verses 5-26

The measuring-reed which was in the hand of the surveyor-general was
mentioned before, v. 3. Here we are told (v. 5) what was the exact
length of it, which must be observed, because the house was measured by
it. It was six cubits long, reckoning, not by the common cubit, but the
cubit of the sanctuary, the sacred cubit, by which it was fit that this
holy house should be measured, and that was a hand-breadth (that it,
four inches) longer than the common cubit: the common cubit was eighteen
inches, this twenty-two, see ch. 43:13. Yet some of the critics contend
that this measuring-reed was but six common cubits in length, and one
handbreadth added to the whole. The former seems more probable. Here is
an account,

`I.` Of the outer wall of the house, which encompassed it round, which was
three yards thick and three yards high, which denotes the separation
between the church and the world on every side and the divine protection
which the church is under. If a wall of this vast thickness will not
secure it, God himself will be a wall of fire round about it; whoever
attack it will do so at their peril.

`II.` Of the several gates with the chambers adjoining to them. Here is
no mention of the outer court of all, which was called the court of the
Gentiles, some think because in gospel-times there should be such a vast
confluence of Gentiles to the church that their court should be left
unmeasured, to signify that the worshippers in that court should be
unnumbered, Rev. 7:9, 11, 12.

`1.` He begins with the east gate, because that was the usual way of
entering into the lower end of the temple, the holy of holies being at
the west end, in opposition to the idolatrous heathen that worshipped
towards the east. Now, in the account of this gate, observe, `(1.)` That
he went up to it by stairs (v. 6), for the gospel-church was exalted
above that of the Old Testament, and when we go to worship God we must
ascend; so is the call, Rev. 4:1. Come up hither. Sursum corda-Up with
your hearts. `(2.)` That the chambers adjoining to the gates were but
little chambers, about ten feet square, v. 7. These were for those to
lodge in who attended the service of the house. And it becomes such as
are made spiritual priests to God to content themselves with little
chambers and not to seek great things to themselves; so that we may but
have a place within the verge of God\'s court we have reason to be
thankful though it be in a little chamber, a mean apartment, though we
be but door-keepers there. `(3.)` The chambers, as they were each of them
four-square, denoting their stability and due proportion and their exact
agreement with the rule (for they were each of them one reed long and
one reed broad), so they were all of one measure, that there might be an
equality among the attendants on the service of the house. `(4.)` The
chambers were very many; for in our Father\'s house there are many
mansions (Jn. 14:2), in his house above, and in that here on earth. In
the secret of his tabernacle shall those be hid, and in a safe pavilion,
whose desire is to dwell in the house of the Lord all the days of their
life, Ps. 27:4, 5. Some make these chambers to represent the particular
congregations of believers, which are parts of the great temple, the
universal church, which are, and must be, framed by the scripture-line
and rule, and which Jesus Christ takes the measure of, that is, takes
cognizance of, for he walks in the midst of the seven golden
candle-sticks. `(5.)` It is said (v. 14), He made also the posts. He that
now measured them was the same that made them; for Christ is the builder
of his church and therefore is best able to give us the knowledge of it.
And his reducing them to the rule and standard is called his making
them, for no account is made of them further than they agree with that.
To the law and to the testimony. `(6.)` Here are posts of sixty cubits,
which, some think, was literally fulfilled when Cyrus, in his edict for
rebuilding the temple at Jerusalem, ordered that the height thereof
should be sixty cubits, that is, thirty yards and more, Ezra 6:3. `(7.)`
Here were windows to the little chambers, and windows to the posts and
arches (that is, to the cloisters below), and windows round about (v.
16), to signify the light from heaven with which the church is
illuminated; divine revelation is let into it for instruction,
direction, and comfort, to those that dwell in God\'s house, light to
work by, light to walk by, light to see themselves and one another by.
There were lights to the little chambers; even the least, and least
considerable, parts and members of the church, shall have light afforded
them. All thy children shall be taught of the Lord. But they are narrow
windows, as those in the temple, 1 Ki. 6:4. The discoveries made to the
church on earth are but narrow and scanty compared with what shall be in
the future state, when we shall no longer see through a glass darkly.
`(8.)` Divers courts are here spoken of, an outermost of all, then an
outer court, then an inner, and then the innermost of all, into which
the priests only entered, which (some think) may put us in mind \"of the
diversities of gifts, and graces, and offices, in the several members of
Christ\'s mystical body here, as also of the several degrees of glory in
the courts and mansions of heaven, as there are stars in several spheres
and stars of several magnitudes in the fixed firmament.\" English
Annotations. Some draw nearer to God than others and have a more
intimate acquaintance with divine things; but to a child of God a day in
any of his courts is better than a thousand elsewhere. These courts had
porches, or piazzas, round them, for the shelter of those that attended
in them from wind and weather; for when we are in the way of our duty to
God we may believe ourselves to be under his special protection, that he
will graciously provide for us, nay, that he will himself be to us a
covert from the storm and tempest, Isa. 4:5, 6. `(9.)` On the posts were
palm-trees engraven (v. 16), to signify that the righteous shall
flourish like the palm-tree in the courts of God\'s house, Ps. 92:12.
The more they are depressed with the burden of affliction the more
strongly do they grow, as they say of the palm-trees. It likewise
intimates the saints\' victory and triumph over their spiritual enemies;
they have palms in their hands (Rev. 7:9); but lest they should drop
these, or have them snatched out of their hands, they are here engraven
upon the posts of the temple as perpetual monuments of their honour.
Thanks be to God, who always causes us to triumph. Nay, believers shall
themselves be made pillars in the temple of our God, and shall go no
more out, and shall have his name engraven on them, which will be their
brightest ornament and honour, Rev. 3:12. `(10.)` Notice is here taken of
the pavement of the court, v. 17, 18. The word intimates that the
pavement was made of porphyry-stone, which was of the colour of burning
coals; for the brightest and most sparkling glories of this world should
be put and kept under our feet when we draw near to God and are
attending upon him. The stars are, as it were, the burning coals, or
stones of a fiery colour, with which the pavement of God\'s celestial
temple is laid; and, if the pavement of the court be so bright and
glittering, how glorious must we conclude the mansions of that house to
be!

`2.` The gates that looked towards the north (v. 20) and towards the
south (v. 24), with their appurtenances, are much the same with that
towards the east, after the measure of the first gate, v. 21. But the
description is repeated very particularly. And thus largely was the
structure of the tabernacle related in Exodus, and of the temple in the
books of Kings and Chronicles, to signify the special notice God does
take, and his ministers should take, of all that belong to his church.
His delight is in them; his eye is upon them. He knows all that are his,
all his living temples and all that belongs to them. Observe, `(1.)` This
temple had not only a gate towards the east, to let into it the children
of the east, that were famous for their wealth and wisdom, but it had a
gate to the north, and another to the south, for the admission of the
poorer and less civilized nations. The new Jerusalem has twelve gates,
three towards each quarter of the world (Rev. 21:13); for many shall
come from all parts to sit down there, Mt. 8:11. `(2.)` To those gates
they went up by steps, seven steps (v. 22-26), which, as some observe,
may remind us of the necessity of advancing in grace and holiness,
adding one grace to another, going from step to step, from strength to
strength, still pressing forward towards perfection-upward, upward,
towards heaven, the temple above.

### Verses 27-38

In these verses we have a delineation of the inner court. The survey of
the outer court ended with the south side of it. This of the inner court
begins with the south side (v. 27), proceeds to the east (v. 32), and so
to the north (v. 35); for here is no gate either of the outer or inner
court towards the west. It should seem that in Solomon\'s temple there
were gates westward, for we find porters towards the west, 1 Chr. 9:24;
26:8. But Josephus says that in the second temple there was no gate on
the west side. Observe, 1. These gates into the inner court were exactly
uniform with those into the outer court, the dimensions the same, the
chambers adjoining the same, the galleries or rows round the court the
same, and the very engravings on the posts the same. The work of grace,
and its workings, are the same, for substance, in grown Christians that
they are in young beginners, only that the former have got so much
nearer their perfection. The faith of all the saints is alike precious,
though it be not alike strong. There is a great resemblance between one
child of God and another; for all they are brethren and bear the same
image. 2. The ascent into the outer court at each gate was by seven
steps, but the ascent into the inner court at each gate was by eight
steps. This is expressly taken notice of (v. 31, 34, 37), to signify
that the nearer we approach to God the more we should rise above this
world and the things of it. The people, who worshipped in the outer
court, must rise seven steps above other people, but the priests, who
attended in the inner court, must rise eight steps above them, must
exceed them at least one step more than they exceed other people.

### Verses 39-49

In these verses we have an account,

`I.` Of the tables that were in the porch of the gates of the inner court.
We find no description of the altars of burnt-offerings in the midst of
that court till ch. 43:13. But, because the one altar under the law was
to be exchanged for a multitude of tables under the gospel, here is
early notice taken of the tables, at our entrance into the inner court;
for till we come to partake of the table of the Lord we are but
professors at large; our admission to that is our entrance into the
inner court. But in this gospel-temple we meet with no altar till after
the glory of the Lord has taken possession of it, for Christ is our
altar, that sanctifies every gift. Here were eight tables provided,
whereon to slay the sacrifices, v. 41. We read not of any tables for
this purpose either in the tabernacle or in Solomon\'s temple. But here
they are provided, to intimate the multitude of spiritual sacrifices
that should be brought to God\'s house in gospel-times, and the
multitude of hands that should be employed in offering up those
sacrifices. Here were the shambles for the altar; here were the dressers
on which they laid the flesh of the sacrifice, the knives with which
they cut it up, and the hooks on which they hung it up, that it might be
ready to be offered on the altar (v. 43), and there also they washed the
burnt-offerings (v. 38), to intimate that before we draw near to God\'s
altar we must have every thing in readiness, must wash our hands, our
hearts, those spiritual sacrifices, and so compass God\'s altar.

`II.` The use that some of the chambers mentioned before were put to. 1.
Some were for the singers, v. 44. It should seem they were first
provided for before any other that attended this temple-service, to
intimate, not only that the singing of psalms should still continue a
gospel-ordinance, but that the gospel should furnish all that embrace it
with abundant matter for joy and praise, and give them occasion to break
forth into singing, which is often foretold concerning gospel times, Ps.
96:1; 98:1. Christians should be singers. Blessed are those that dwell
in God\'s house, they will be still praising him. 2. Others of them were
for the priests, both those that kept the charge of the house, to
cleanse it, and to see that none came into it to pollute it, and to keep
it in good repair (v. 45), and those that kept the charge of the altar
(v. 46), that came near to the Lord to minister to him. God will find
convenient lodging for all his servants. Those that do the work of his
house shall enjoy the comforts of it.

`III.` Of the inner court, the court of the priests, which was fifty
yards square, v. 47. The altar that was before the house was placed in
the midst of this court, over-against the three gates, and, standing in
a direct line with the three gates of the outer court, when the gates
were set open all the people in the outer court might through them be
spectators of the service done at the altar. Christ is both our altar
and our sacrifice, to whom we must look with an eye of faith in all our
approaches to God, and he is salvation in the midst of the earth (Ps.
74:12), to be looked unto from all quarters.

`IV.` Of the porch of the house. The temple is called the house,
emphatically, as if no other house were worthy to be called so. Before
this house there was a porch, to teach us not to rush hastily and
inconsiderately into the presence of God, but gradually, that is,
gravely, and with solemnity, passing first through the outer court, then
the inner, then the porch, ere we enter into the house. Between this
porch and the altar was a place where the priests used to pray, Joel
2:17. In the porch, besides the posts on which the doors were hung,
there were pillars, probably for state and ornament, like Jachin and
Boaz-He will establish; in him is strength, v. 49. In the gospel church
every thing is strong and firm, and every thing ought to be kept in its
place and to be done decently and in order.
