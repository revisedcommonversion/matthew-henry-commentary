Ezekiel, Chapter 27
===================

Commentary
----------

Still we are attending the funeral of Tyre and the lamentations made for
the fall of that renowned city. In this chapter we have, `I.` A large
account of the dignity, wealth, and splendour of Tyre, while it was in
its strength, the vast trade it drove, and the interest it had among the
nations (v. 1-25), which is designed to make its ruin the more
lamentable. `II.` A prediction of its fall and ruin, and the confusion and
consternation which all its neighbours shall thereby be put into (v.
26-36). And this is intended to stain the pride of all worldly glory,
and, by setting the one over-against the other, to let us see the vanity
and uncertainty of the riches, honours, and pleasures of the world, and
what little reason we have to place our happiness in them or to be
confident of the continuance of them; so that all this is written for
our learning.

### Verses 1-25

Here, `I.` The prophet is ordered to take up a lamentation for Tyrus, v.
2. It was yet in the height of its prosperity, and there appeared not
the least symptom of its decay; yet the prophet must lament it, because
its prosperity is its snare, is the cause of its pride and security,
which will make its fall the more grievous. Even those that live at ease
are to be lamented if they be not preparing for trouble. He must lament
it because its ruin is hastening on apace; it is sure, it is near; and
though the prophet foretel it, and justify God in it, yet he must lament
it. Note, We ought to mourn for the miseries of other nations, as well
as for our own, out of an affection for mankind in general; it is a part
of the honour we owe to all men to bewail their calamities, even those
which they have brought upon themselves by their own folly.

`II.` He is directed what to say, and to say it in the name of the Lord
Jehovah, a name not unknown in Tyre, and which shall be better known,
ch. 26:6.

`1.` He must upbraid Tyre with her pride: O Tyrus! thou hast said, I am
of perfect beauty (v. 3), of universal beauty (so the word is), every
way accomplished, and therefore every where admired. Zion, that had the
beauty of holiness, is called indeed the perfection of beauty (Ps. 1.
2); that is the beauty of the Lord. But Tyre, because well-built and
well-filled with money and trade, will set up for a perfect beauty.
Note, It is the folly of the children of this world to value themselves
on the pomp and pleasure they live in, to call themselves beauties for
the sake of them, and, if in these they excel others, to think
themselves perfect. But God takes notice of the vain conceits men have
of themselves in their prosperity when the mind is lifted up with the
condition, and often, for the humbling of the spirit, finds a way to
bring down the estate. Let none reckon themselves beautified any further
than they are sanctified, nor say that they are of perfect beauty till
they come to heaven.

`2.` He must upbraid Tyre with her prosperity, which was the matter of
her pride. In elegies it is usual to insert encomiums of those whose
fall we lament; the prophet, accordingly, praises Tyre for all that she
had that was praiseworthy. He has nothing to say of her religion, her
piety, her charity, her being a refuge to the distressed or using her
interest to do good offices among her neighbours; but she lived great,
and had a great trade, and all the trading part of mankind made court to
her. The prophet must describe her height and magnificence, that God may
be the more glorified in her fall, as the God who looks upon every one
that is proud and abases him, hides the proud in the dust together, and
binds their faces in secret, Job 40:12.

`(1.)` The city of Tyre was advantageously situated, at the entry of the
sea (v. 3), having many commodious harbours each way, not as cities
seated on rivers, which the shipping can come but one way to. It stood
at the east end of the Mediterranean, very convenient for trade by land
into all the Levant parts; so that she became a merchant of the people
for many isles. Lying between Greece and Asia, it became the great
emporium, or mart-town, the rendezvous of merchants from all parts: They
borders are in the heart of the seas, v. 4. It was surrounded with
water, which was a great advantage to its trade; it was the darling of
the sea, laid in its bosom, in its heart. Note, It is a great
convenience, upon many accounts, to live in an island: seas are the most
ancient land-mark, not which our fathers have set, but the God of our
fathers, and which cannot be removed as other land-marks may, nor so
easily got over. The people so situated may the more easily dwell alone,
if they please, as not reckoned among the nations, and yet, if they
please, may the more easily traffic abroad and keep a correspondence
with the nations. We therefore of this island must own that he who
determines the bounds of men\'s habitations has determined well for us.

`(2.)` It was curiously built, according as the fashion then was; and,
being a city on a hill, it made a glorious show and tempted the ships
that sailed by into her ports (v. 4): They builders have perfected thy
beauty; they have so improved in architecture that nothing appears in
the buildings of Tyre that can be found fault with; and yet it wants
that perfection of beauty into which the Lord does and will build up his
Jerusalem.

`(3.)` It had its haven replenished with abundance of gallant ships, Isa.
33:21. The ship-carpenters did their part, as well as the
house-carpenters theirs. The Tyrians are thought to be the first that
invented the art of navigation; at least they improved it, and brought
it to as great a perfection perhaps as it could be without the
loadstone. `[1.]` They made the boards, or planks, for the hulk of the
ship, of fir-trees fetched from Senir, a mount in the land of Israel,
joined with Hermon, Cant. 4:8. Planks of fir were smooth and light, but
not so lasting as our English oak. `[2.]` They had cedars from Lebanon,
another mountain of Israel, for their masts, v. 5. `[3.]` They had oaks
from Bashan (Isa. 2:13), to make oars of; for it is probable that their
ships were mostly galleys, that go with oars. The people of Israel built
few ships for themselves, but they furnished the Tyrians with timber for
shipping. Thus one country uses what another produced, and so they are
serviceable one to another, and cannot say to each other, I have no need
of thee. `[4.]` Such magnificence did they affect in building their
ships that they made the very benches of ivory, which they fetched from
the isles of Chittim, from Italy or Greece, and had workmen from the
Ashurites or Assyrians to make them, so rich would they have their
state-rooms in their ships to be. `[5.]` So very prodigal were they that
they made their sails of fine linen fetched from Egypt, and that
embroidered too, v. 7. Or it may be meant of their flags (which they
hoisted to notify what city they belonged to), which were very costly.
The word signifies a banner as well as a sail. `[6.]` They hung those
rooms on ship-board with blue and purple, the richest cloths and richest
colours they could get from the isles they traded with. For though Tyre
was itself famous for purple, which is therefore called the Tyrian dye,
yet they must have that which was far-fetched.

`(4.)` These gallant ships were well-manned, by men of great ingenuity and
industry. The pilots and masters of the ships, that had command in their
fleets, were of their own city, such as they could put a confidence in
(v. 8): Thy wise men, O Tyrus! that were in thee, were thy pilots. But,
for common sailors, they had men from other countries; The inhabitants
of Arvad and Zidon were thy mariners. These came from cities hear them;
Zidon was sister to Tyre, not two leagues off, to the northward; there
they bred able seamen, which it is the interest of the maritime powers
to support and give all the countenance they can to. They sent to Gebal
in Syria for calkers, or strengtheners of the clefts or chinks, to stop
them when the ships come home, after long voyages, to be repaired. To do
this they had the ancients and wise men (v. 9); for there is more need
of wisdom and prudence to repair what has gone to decay than to build
anew. In public matters there is occasion for the ancients and wise men
to be the repairers of the breaches and the restorers of paths to dwell
in. Nay, all the countries they traded with were at their service, and
were willing to send men into their pay, to put their youths apprentice
in Tyre, or to put them on board their fleets; so that all the ships in
the sea with their mariners were ready to occupy thy merchandise. Those
that give good wages shall have hands at command.

`(5.)` Their city was guarded by a military force that was very
considerable, v. 10, 11. The Tyrians were themselves wholly given to
trade; but it was necessary that they should have a good army on foot,
and therefore they took those of other states into their pay, such as
were fittest for service, though they had them from afar (which perhaps
was their policy), from Persia, Lud, and Phut. These bore their arms
when there was occasion, and in time of peace hung up the shield and
buckler in the armoury, as it were to proclaim peace, and let the world
know that they had at present no need of them, but they were ready to be
taken down whenever there was occasion for them. Their walls were
guarded by the man of Arvad; their towers were garrisoned by the
Gammadim, robust men, that had a great deal of strength in their arms;
yet the vulgar Latin renders it pygmies, men no longer than one\'s arm.
They hung their shields upon the walls in their magazines or places of
arms; or hung them out upon the walls of the city, that none might dare
to approach them, seeing how well provided they were with all things
necessary for their own defence. \"Thus they set forth thy comeliness
(v. 10), and made they beauty perfect,\" v. 11. It contributed as much
as any thing to the glory of Tyre that it had those of all the
surrounding nations in its service, except the land of Israel (though it
lay next them), which furnished them with timber, but we do not find
that it furnished them with men; that would have trenched upon the
liberty and dignity of the Jewish nation, 2 Chr. 2:17, 18. It was also
the glory of Tyre that it had such a militia, so fit for service, and in
constant pay, and such an armoury, like that in the tower of David,
where hung the shields of mighty men, Cant. 4:4. It is observable that
there and here the armouries are said to be furnished with shields and
helmets, defensive arms, not with swords and spears, offensive, though
it is probable that there were such, to intimate that the military force
of a people must be intended only for their own protection and not to
invade and annoy their neighbours, to secure their own right, not to
encroach upon the rights of others.

`(6.)` They had a vast trade and a correspondence with all parts of the
known world. Some nations they dealt with in one commodity and some in
another, according as either its products or its manufactures were, and
the fruits of nature or art were, with which it was blessed. This is
very much enlarged upon here, as that which was the principal glory of
Tyre, and which supported all the rest. We do not find any where in
scripture so many nations named together as are here; so that this
chapter, some think, gives much light to the first account we have of
the settlement of the nations after the flood, Gen. 10. The critics have
abundance of work here to find out the several places and nations spoken
of. Concerning many of them their conjectures are different and they
leave us in the dark and at much uncertainty; it is well that it is not
material. Modern surveys come short of explaining the ancient geography.
And therefore we will not amuse ourselves here with a particular enquiry
either concerning the traders or the goods they traded in. We leave it
to the critical expositors, and observe that only which is improvable.
`[1.]` We have reason to think that Ezekiel knew little, of his own
knowledge, concerning the trade of Tyre. He was a priest, carried away
captive far enough from the neighbourhood of Tyre, we may suppose when
he was young, and there he had been eleven years. And yet he speaks of
the particular merchandises of Tyre as nicely as if he had been
comptroller of the custom-house there, by which it appears that he was
divinely inspired in what he spoke and wrote. It is God that saith this,
v. 3. `[2.]` This account of the trade of Tyre intimates to us that
God\'s eye is upon men, and that he takes cognizance of what they do
when they are employed in their worldly business, not only when they are
at church, praying and hearing, but when they are in their markets and
fairs, and upon the exchange, buying and selling, which is a good reason
why we should in all our dealings keep a conscience void of offence, and
have our eye always upon him whose eye is always upon us. `[3.]` We may
here observe the wisdom of God, and his goodness, as the common Father
of mankind, in making one country to abound in one commodity and another
in another, and all more or less serviceable either to the necessity or
to the comfort or ornament of human life. Non omis fert omnia tellus-One
land does not supply all the varieties of produce. Providence dispenses
its gifts variously, some to each, and all to none, that there may be a
mutual commerce among those whom God has made of one blood, though they
are made to dwell on all the face of the earth, Acts 17:26. Let every
nations therefore thank God for the productions of its country; though
they be not so rich as those of others, yet there is use for them in the
public service of the world. `[4.]` See what a blessing trade and
merchandise are to mankind, especially when followed in the fear of God,
and with a regard not only to private advantage, but to a common
benefit. The earth is full of God\'s riches, Ps. 104:24. There is a
multitude of all kinds of riches in it (as it is here, v. 12), gathered
off its surface and dug out of its bowels. The earth is also full of the
fruits of men\'s ingenuity and industry, according as their genius leads
them. Now by exchange and barter these are made more extensively useful;
thus what can be spared is helped off, and what is wanted is fetched in,
in lieu of it, from the most distant countries. Those that are not
tradesmen themselves have reason to thank God for tradesmen and
merchants, by whom the productions of other countries are brought to our
hands, as those of our own are by our husbandmen. `[5.]` Besides the
necessaries that are here traded in, see what abundance of things are
here mentioned that only serve to please fancy, and are made valuable
only by men\'s humour and custom; and yet God allows us to use them, and
trade in them, and part with those things for them which we can spare
that are of an intrinsic worth much beyond them. Here are horns of ivory
and ebony (v. 15), that are brought for a present, exposed to sale, and
offered in exchange, or (as some think) presented to the city, or the
great men of it, to obtain their favour. Here are emeralds, coral, and
agate (v. 16), all precious stones, and gold (v. 22), which the world
could better be without than iron and common stones. Here are, to please
the taste and smell, the chief of all spices (v. 22), cassia and calamus
(v. 19), and, for ornament, purple, broidered work, and fine linen (v.
16), precious clothes for chariots (v. 20), blue clothes (which Tyre was
famous for), broidered work, and chests of rich apparel, bound with rich
cords, and made of cedar, a sweet wood to perfume the garments kept in
them, v. 24. Upon the review of this invoice, or bill of parcels, we may
justly say, What a great many things are here that we have no need of,
and can live very comfortably without! `[6.]` It is observable that
Judah and the land of Israel were merchants in Tyre too; in a way of
trade they were allowed to converse with the heathen. But they traded
mostly in wheat, a substantial commodity, and necessary, wheat of
Minnith and Pannag, two countries in Canaan famous for the best wheat,
as some think. The whole land indeed was a land of wheat (Deu. 8:8); it
had the fat of kidneys of wheat, Deu. 32:14. Tyre was maintained by corn
fetched from the land of Israel. They traded likewise in honey, and oil,
and balm, or rosin; all useful things, and not serving to pride or
luxury. And the land which these were the staple commodities of was that
which was the glory of all lands, which God reserved for his peculiar
people, not those that traded in spices and precious stones; and the
Israel of God must reckon themselves well provided for if they have food
convenient; for those that are acquainted with the delights of the
children of God will not set their hearts on the delights of the sons
and daughters of men, or the treasures of kings and provinces. We find
indeed that the New-Testament Babylon trades in such things as Tyre
traded in, Rev. 18:12, 13. For, notwithstanding its pretensions to
sanctity, it is a mere worldly interest. `[7.]` Though Tyre was a city
of great merchandise, and they got abundance by buying and selling,
importing commodities from one place and exporting them to another, yet
manufacture-trades were not neglected. The wares of their own making,
and a multitude of such wares, are here spoken of, v. 16, 18. It is the
wisdom of a nation to encourage art and industry, and not to bear hard
upon the handicraft-tradesmen; for it contributes much to the wealth and
honour of a nation to send abroad wares of their own making, which may
bring them in the multitude of all riches. `[8.]` All this made Tyrus
very great and very proud: The ships of Tarshish did sing of thee in
they market (v. 25); thou wast admired and cried up by all the nations
that had dealings with thee; for thou wast replenished in wealth and
number of people, wast beautified, and made very glorious, in the midst
of the seas. Those that grow very rich are cried up as very glorious;
for riches are glorious things in the eyes of carnal people, Gen. 31:1.

### Verses 26-36

We have seen Tyre flourishing; here we have Tyre falling, and great is
the fall of it, so much the greater for its having made such a figure in
the world. Note, The most mighty and magnificent kingdoms and states,
sooner or later, have their day to come down. They have their period;
and, when they are in their zenith, they will begin to decline. But the
destruction of Tyre was sudden. Her sun went down at noon. And all her
wealth and grandeur, pomp and power, did but aggravate her ruin, and
make it the more grievous to herself and astonishing to all about her.
Now observe here, 1. How the ruin of Tyrus will be brought about, v. 26.
She is as a great ship richly laden, that is split or sunk by the
indiscretion of her steersmen: Thy rowers have themselves brought thee
into great and dangerous waters; the governors of the city, and those
that had the management of their public affairs, by some mismanagement
or other involved them in that war with the Chaldeans which was the ruin
of their state. By their insolence, by some affront given to the
Chaldeans or some attempt made upon them, in confidence of their own
ability to contend with them, they provoked Nebuchadnezzar to make a
descent upon them, and, by their obstinacy in standing it out to the
last, enraged him to such a degree that he determined on the ruin of
their state, and, like an east wind, broke them in the midst of the
seas. Note, It is ill with a people when those that sit at the stern,
instead of putting them into the harbour, run them aground. 2. How great
and general the ruin will be. All her wealth shall be buried with her,
her riches, her fairs, and her merchandise (v. 27); all that had any
dependence upon her, and dealings with her, in trade, in war, in
conversation, shall ball with her into the midst of the seas, in the day
of her ruin. Note, Those who make creatures their confidence, place
their happiness in their interest in them and rest their hopes upon
them, will of course fall with them; happy therefore are those that have
the God of Jacob for their help, and whose hope is in the Lord their
God, who lives for ever. 3. What sad lamentation would be made for the
destruction of Tyre. The pilots, her princes and governors, when they
see how wretchedly they have mismanaged and how much they have
contributed to their own ruin, shall cry out so loud as to make even the
suburbs shake (v. 28), such a vexation shall it be to them to reflect
upon their own bad conduct. The inferior officers, that were as the
mariners of the state, shall be forced to come down from their
respective posts (v. 29), and they shall cry out against thee, as having
deceived them, in not proving so well able to hold out as they thought
thou hadst been; they shall cry bitterly for the common ruin, and their
own share in it. They shall use all the most solemn expressions of
grief; they shall cast dust on their heads, in indignation against
themselves, shall wallow themselves in ashes, as having bid a final
farewell to all ease and pleasure; they shall make themselves bald (v.
31), with tearing their hair; and, according to the custom of great
mourners, those shall gird themselves with sackcloth who used to wear
find linen, and, instead of merry songs, they shall weep with bitterness
of heart. Note, Losses and crosses are very grievous, and hard to be
borne, to those that have long been wallowing in pleasure and sleeping
in carnal security. 4. How Tyre should be upbraided with her former
honour and prosperity (v. 32, 33); she that was Tyrus the renowned shall
now be called Tyrus the destroyed in the midst of the sea. \"What city
is like Tyre? Did ever any city come down from such a height of
prosperity to such a depth of adversity? Time was when thy wares, those
of thy own making and those that passed through thy hands, went forth
out of the seas, and were exported to all parts of the world; then thou
filledst many people, and didst enrich the kings of the earth and their
kingdoms.\" The Tyrians, though they bore such a sway in trade, were
yet, it seems, fair merchants, and let their neighbours not only live,
but thrive by them. All that dealt with them were gainers; they did not
cheat or oppress the people, but did enrich them with the multitude of
their merchandise. \"But now those that used to be enriched by thee
shall be ruined with thee\" (as is usual in trade); \"when thou shalt be
broken, and all thou hast is seized on, all thy company shall fall
too,\" v. 34. There is an end of Tyre, that made such a noise and bustle
in the world. This great blaze goes out in a snuff. 5. How the fall of
Tyre should be matter of terror to some and laughter to others,
according as they were differently interested and affected. Some shall
be sorely afraid, and shall be troubled (v. 35), concluding it will be
their own turn to fall next. Others shall hiss at her (v. 36), shall
ridicule her pride, and vanity, and bad management, and think her ruin
just. She triumphed in Jerusalem\'s fall, and there are those that will
triumph in hers. When God casts his judgments on the sinner men also
shall clap their hands at him and shall hiss him out of his place, Job
27:22, 23. Is this the city which men called the perfection of beauty?
