Ezekiel, Chapter 41
===================

Commentary
----------

An account was given of the porch of the house in the close of the
foregoing chapter; this brings us to the temple itself, the description
of which here given creates much difficulty to the critical expositors
and occasions differences among them. Those must consult them who are
nice in their enquiries into the meaning of the particulars of this
delineation; it shall suffice us to observe, `I.` The dimensions of the
house, the posts of it (v. 1), the door (v. 2), the wall and the
side-chambers (v. 5, 6), the foundations and wall of the chambers, their
doors (v. 8-11), and the house itself (v. 13). `II.` The dimensions of the
oracle, or most holy place (v. 3, 4). `III.` An account of another
building over against the separate place (v. 12-15). `IV.` The manner of
the building of the house (v. 7, 16, 17). `V.` The ornaments of the house
(v. 18-20). `VI.` The altar of incense and the table (v. 22). `VII.` The
doors between the temple and the oracle (v. 23-26). There is so much
difference both in the terms and in the rules of architecture between
one age and another, one place and another, that it ought not to be any
stumbling-block to us that there is so much in these descriptions dark
and hard to be understood, about the meaning of which the learned are
not agreed. To one not skilled in mathematics the mathematical
description of a modern structure would be scarcely intelligible; and
yet to a common carpenter or mason among the Jews at that time we may
suppose that all this, in the literal sense of it, was easy enough.

### Verses 1-11

We are still attending a prophet that is under the guidance of an angel,
and therefore attend with reverence, though we are often at a loss to
know both what this is and what it is to us. Observe here, 1. After the
prophet had observed the courts he was at length brought to the temple,
v. 1. If we diligently attend to the instructions given us in the
plainer parts of religion, and profit by them, we shall be led further
into an acquaintance with the mysteries of the kingdom of heaven. Those
that are willing to dwell in God\'s courts shall at length be brought
into his temple. Ezekiel was himself a priest, but by the iniquity and
calamity of the times was cut short of his birthright privilege of
ministering in the temple; but God makes up the loss to him by
introducing him into this prophetical, evangelical, celestial temple,
and employing him to transmit a description of it to the church, in
which he was dignified above all the rest of his order. 2. When our Lord
Jesus spoke of the destroying of this temple, which his hearers
understood of this second temple of Jerusalem, he spoke of the temple of
his body (Jn. 2:19, 21); and with good reason might he speak so
ambiguously when Ezekiel\'s vision had a joint respect to them both
together, including also his mystical body the church, which is called
the house of God (1 Tim. 3:15), and all the members of that body, which
are living temples, in which the Spirit dwells. 3. The very posts of
this temple, the door-posts, were as far one from the other, and
consequently the door was as wide, as the whole breadth of the
tabernacle of Moses (v. 1), namely, twelve cubits, Ex. 26:16, 22, 25. In
comparison with what had been under the law we may say, Wide is the gate
which leads into the church, the ceremonial law, that wall of partition
which had so much straitened the gate, being taken down. 4. The most
holy place was an exact square, twenty cubits each way, v. 4. For the
new Jerusalem is exactly square (Rev. 21:16), denoting its stability;
for we look for a city that cannot be moved. 5. The upper stories were
larger than the lower, v. 7. The walls of the temple were six cubits
thick at the bottom, five in the middle story, and four in the highest,
which gave room to enlarge the chambers the higher they went; but care
was taken that the timber might have fast hold (though God builds high,
he builds firmly), yet so as not to weaken one part for the
strengthening of another; they had hold, but not in the wall of the
house. By this spreading gradually, the side-chambers that were on the
height of the house (in the uppermost story of all) were six cubits,
whereas the lowest were but four; they gained a cubit every story. The
higher we build up ourselves in our most holy faith the more should our
hearts, those living temples, be enlarged.

### Verses 12-26

Here is, 1. An account of a building that was before the separate place
(that is, before the temple), at the end towards the west (v. 12), which
is here measured, and compared (v. 13) with the measure of the house,
and appears to be of equal dimensions with it. This stood in a court by
itself, which is measured (v. 15) and its galleries, or chambers
belonging to it, its posts and windows, and the ornaments of them, v.
15-17. But what use was to be made of this other building we are not
told; perhaps, in this vision, it signified the setting up of a church
among the Gentiles not inferior to the Jewish temple, but of quite
another nature, and which should soon supersede it. 2. A description of
the ornaments of the temple, and the other building. The walls on the
inside from top to bottom were adorned with cherubim and palm-trees,
placed alternately, as in Solomon\'s temple, 1 Ki. 6:29. Each cherub is
here said to have two faces, the face of a man towards the palm tree on
one side and the face of a young lion towards the palm-tree on the other
side, v. 19. These seem to represent the angels, who have more than the
wisdom of a man and the courage of a lion; and in both they have an eye
to the palms of victory and triumph which are set before them, and which
they are sure of in all their conflicts with the powers of darkness. And
in the assemblies of the saints angels are in a special manner present,
1 Co. 11:10. 3. A description of the posts of the doors both of the
temple and of the sanctuary; they were squared (v. 21), not round like
pillars; and the appearance of the one was as the appearance of the
other. In the tabernacle, and in Solomon\'s temple, the door of the
sanctuary, or most holy, was narrower than that of the temple, but here
it was fully as broad; for in gospel-times the way into the holiest of
all is made more manifest than it was under the Old Testament (Heb. 9:8)
and therefore the door is wider. These doors are described, v. 23, 24.
The temple and the sanctuary had each of them its door, and they were
two-leaved, folding doors. 4. We have here the description of the altar
of incense, here said to be an altar of wood, v. 22. No mention is made
of its being over-laid with gold; but surely it was intended to be so,
else it would not bear the fire with which the incense was to be burned,
unless we will suppose that it served only to put the censers upon. Or
else it intimates that the incense to be offered in the gospel-temple
shall be purely spiritual, and the fire spiritual, which will not
consume an altar of wood. Therefore this altar is called a table. This
is the table that is before the Lord. Here, as before, we find the altar
turned into a table; for, the great sacrifice being now offered, that
which we have to do is to feast upon the sacrifice at the Lord\'s table.
5. Here is the adorning of the doors and windows with palm-trees, that
they might be of a piece with the walls of the house, v. 25, 26. Thus
the living temples are adorned, not with gold, or silver, or costly
array, but with the hidden man of the heart, in that which is not
corruptible.
