Ezekiel, Chapter 42
===================

Commentary
----------

This chapter continues and concludes the describing and measuring of
this mystical temple, which it is very hard to understand the particular
architecture of, and yet more hard to comprehend the mystical meaning
of. Here is, `I.` A description of the chambers that were about the
courts, their situation and structure (v. 1-13), and the uses for which
they were designed (v. 13, 14). `II.` A survey of the whole compass of
ground which was taken up with the house, and the courts belonging to it
(v. 15-20).

### Verses 1-14

The prophet has taken a very exact view of the temple and the buildings
belonging to it, and is now brought again into the outer court, to
observe the chambers that were in that square.

`I.` Here is a description of these chambers, which (as that which went
before) seems to us very perplexed and intricate, through our
unacquaintedness with the Hebrew language and the rules of architecture
at that time. We shall only observe, in general, 1. That about the
temple, which was the place of public worship, there were private
chambers, to teach us that our attendance upon God in solemn ordinances
will not excuse us from the duties of the closet. We must not only
worship in the courts of God\'s house, but must, both before and after
our attendance there, enter into our chambers, enter into our closets,
and read and meditate, and pray to our Father in secret; and a great
deal of comfort the people of God have found in their communion with God
in solitude. 2. That these chambers were many; there were three stories
of them, and, though the higher stories were not so large as the lower,
yet they served as well for retirement, v. 5, 6. There were many, that
there might be conveniences for all such devout people as Anna the
prophetess, who departed not from the temple night or day, Lu. 2:37. In
my Father\'s house are many mansions. In his house on earth there are
so; multitudes by faith have taken lodgings in his sanctuary, and yet
there is room. 3. That these chambers, though they were private, yet
were near the temple, within view of it, within reach of it, to teach us
to prefer public worship before private (the Lord loves the gates of
Zion more than all the dwellings of Jacob, and so must we), and to refer
our private worship to the public. Our religious performances in our
chambers must be to prepare us for the exercises of devotion in public,
and to further us in our improvement of them, as our opportunities are.
4. That before these chambers there were walks of five yards broad (v.
4), in which those that had lodgings in these chambers might meet for
conversation, might walk and talk together for their mutual edification,
might communicate their knowledge and experiences. For we are not to
spend all our time between the church and the chamber, though a great
deal of time may be spent to very good purpose in both. But man is made
for society, and Christians for the communion of saints; and the duties
of that communion we must make conscience of, and the privileges and
pleasures of that communion we must take the comfort of. It is promised
to Joshua, who was high priest in the second temple, that God will give
him places to walk in among those that stand by, Zec. 3:7.

`II.` Here is the use of these chambers appointed, v. 13, 14. 1. They
were for the priests that approach unto the Lord, that they may be
always near their business and may not be non-residents. Therefore they
are called holy chambers, because they were for use of those that
ministered in holy things during their ministration. Those that have
public work to do for God and the souls of men have need to be much in
private, to fit themselves for it. Ministers should spend much time in
their chambers, in reading, meditation, and prayer, that their profiting
may appear; and they ought to be provided with conveniences for this
purpose. 2. There the priests were to deposit the most holy things,
those parts of the offerings which fell to their share; and there they
were to eat them, they and their families, in a religious manner, for
the place is holy; and thus they must make a difference between those
feasts upon the sacrifice and other meals. 3. There (among other uses)
they were to lay their vestments, which God had appointed them to wear
when they ministered at the altar, their linen ephods, coats, girdles,
and bonnets. We read of the providing of priests garments after their
return out of captivity, Neh. 7:70, 72. When they had ended their
service at the altar they must lay by those garments, to signify that
the use of them should continue only during that dispensation; but they
must put on other garments, such as other people wear, when they
approached to those things which were for the people, that is, to do
that part of their service which related to the people, to teach them
the law and to answer their enquiries. Their holy garments must be laid
up, that they may be kept clean and decent for the credit of their
service.

### Verses 15-20

We have attended the measuring of this mystical temple and are now to
see how far the holy ground on which we tread extends; and that also is
here measured, and found to take in a great compass. Observe, 1. What
the dimensions of it were. It extended each way 500 reeds (v. 16-19),
each reed above three yards and a half, so that it reached every way
about an English measured mile, which, the ground lying square, was
above four miles round. Thus large were the suburbs (as I may call them)
of this mystical temple, signifying the great extent of the church in
gospel-times, when all nations should be discipled and the kingdoms of
the world made Christ\'s kingdoms. Room should be made in God\'s courts
for the numerous forces of the Gentiles that shall flow into them, as
was foretold, Isa. 49:18; 60:4. It is in part fulfilled already in the
accession of the Gentiles to the church; and we trust it shall have a
more full accomplishment when the fulness of the Gentiles shall come in
and all Israel shall be saved. 2. Why the dimensions of it were made
thus large. It was to make a separation, by putting a very large
distance between the sanctuary and the profane place; and therefore
there was a wall surrounding it, to keep off those that were unclean and
to separate between the previous and the vile. Note, A difference is to
be put between common and sacred things, between God\'s name and other
names, between his day and other days, his book and other books, his
institutions and other observances; and a distance is to be put between
our worldly and religious actions, so as still to go about the worship
of God with a solemn pause.
