Ezekiel, Chapter 26
===================

Commentary
----------

The prophet had soon done with those four nations that he set his face
against in the foregoing chapters; for they were not at that time very
considerable in the world, nor would their fall make any great noise
among the nations nor any figure in history. But the city of Tyre is
next set to the bar; this, being a place of vast trade, was known all
the world over; and therefore here are three whole chapters, this and
the two that follow, spent in the prediction of the destruction of Tyre.
We have \"the burden of Tyre,\" Isa. 23. It is but just mentioned in
Jeremiah, as sharing with the natives in the common calamity, 25:22;
27:3; 47:4. But Ezekiel is ordered to be copious upon that head. In this
chapter we have, `I.` The sin charged upon Tyre, which was triumphing in
the destruction of Jerusalem (v. 2). `II.` The destruction of Tyrus itself
foretold. 1. The extremity of this destruction: it shall be utterly
ruined (v. 4-6, 12-14). 2. The instruments of this destruction, many
nations (v. 3), and the king of Babylon by name with his vast victorious
army (v. 7-11). 3. The great surprise that this should give to the
neighbouring nations, who would all wonder at the fall of so great a
city and be alarmed at it (v. 15-21).

### Verses 1-14

This prophecy is dated in the eleventh year, which was the year that
Jerusalem was taken, and in the first day of the month, but it is not
said what month, some think the month in which Jerusalem was taken,
which was the fourth month, others the month after; or perhaps it was
the first month, and so it was the first day of the year. Observe here,

`I.` The pleasure with which the Tyrians looked upon the ruins of
Jerusalem. Ezekiel was a great way off, in Babylon, but God told him
what Tyrus said against Jerusalem (v. 2): \"Aha! she is broken, broken
to pieces, that was the gates of the people, to whom there was a great
resort and where there was a general rendezvous of all nations, some
upon one account and some upon another, and I shall get by it; all the
wealth, power, and interest, which Jerusalem had, it is hoped, shall be
turned to Tyre, and so now that she is laid waste I shall be
replenished.\" We do not find that the Tyrians had such a hatred and
enmity to Jerusalem and the sanctuary as the Ammonites and Edomites had,
or were so spiteful and mischievous to the Jews. They were men of
business, and of large acquaintance and free conversation, and therefore
were not so bigoted, and of such a persecuting spirit, as the narrow
souls that lived retired and knew not the world. All their care was to
get estates, and enlarge their trade, and they looked upon Jerusalem not
as an enemy, but as a rival. Hiram, king of Tyre, was a good friend to
David and Solomon, and we do not read of any quarrels the Jews had with
the Tyrians; but Tyre promised herself that the fall of Jerusalem would
be an advantage to her in respect of trade a commerce, that now she
shall have Jerusalem\'s customers, and the great men from all parts that
used to come to Jerusalem for the accomplishing of themselves, and to
spend their estates there, will now come to Tyre and spend them there;
and whereas many, since the Chaldean army became so formidable in those
parts, had retired into Jerusalem, and brought their estates thither for
safety, as the Rechabites did, now they will come to Tyre, which, being
in a manner surrounded with the sea, will be thought a place of greater
strength than Jerusalem, and thus the prosperity of Tyre will rise out
of the ruins of Jerusalem. Note, To be secretly pleased with the death
or decay of others, when we are likely to get by it, with their fall
when we may thrive upon it, is a sin that does most easily beset us, but
is not thought to be such a bad thing, and so provoking to God, as
really it is. We are apt to say, when those who stand in our light, in
our way, are removed, when they break of fall into disgrace, \"We shall
be replenished now that they are laid waste.\" But this comes from a
selfish covetous principle, and a desire to be placed alone in the midst
of the earth, as if we grudged that any should live by us. This comes
from a want of that love to our neighbour as to ourselves which the law
of God so expressly requires, and from that inordinate love of the world
as our happiness which the love of God so expressly forbids. And it is
just with God to blast the designs and projects of those who thus
contrive to raise themselves upon the ruins of others; and we see they
are often disappointed.

`II.` The displeasure of God against them for it. The providence of God
had done well for Tyrus. Tyrus was a pleasant and wealthy city, and
might have continued so if she had, as she ought to have done,
sympathized with Jerusalem in her calamities and sent her an address of
condolence; but when, instead of that, she showed herself pleased with
her neighbour\'s fall, and perhaps sent an address of congratulation to
the conquerors, then God says, Behold, I am against thee, O Tyrus! v. 3.
And let her not expect to prosper long if God be against her.

`1.` God will bring formidable enemies upon her: Many nations shall come
against thee, an army made up of many nations, or one nation that shall
be as strong as many. Those that have God against them may expect all
the creatures against them; for what peace can those have with whom God
is at war? They shall come pouring in as the waves of the sea, one upon
the neck of another, with an irresistible force. The person is named
that shall bring this army upon them-Nebuchadnezzar king of Babylon, a
king of kings, that had many kings tributaries to him and dependents on
him, besides those that were his captives, Can 2:37, 38. He is that head
of gold. He shall come with a vast army, horses and chariots, etc., all
land-forces. We do not find that he had any naval force, or any thing
wherewith he might attack it by sea, which made the attempt the more
difficult, as we find ch. 29:18, where it is called a great service
which he served against Tyrus. He shall besiege it in form (v. 8), make
a fort, and cast a mount, and (v. 9) shall set engines of war against
the walls. His troops shall be so numerous as to raise a dust that shall
cover the city, v. 10. They shall make a noise that shall even shake the
walls; and they shall shout at every attack, as soldiers do when they
enter a city that is broken up; the horses shall prance with so much
fury and violence that they shall even tread down the streets though so
ever well paved.

`2.` They shall do terrible execution. `(1.)` The enemy shall make
themselves masters of all their fortifications, shall destroy the walls
and break down the towers, v. 4. For what walls are so strongly built as
to be a fence against the judgments of God? Her strong garrisons shall
go down to the ground, v. 11. And the walls shall be broken down, v. 12.
The city held out a long siege, but it was taken at last. `(2.)` A great
deal of blood shall be shed: Her daughters who are in the field, the
cities upon the continent, which were subject to Tyre as the
mother-city, the inhabitants of them shall be slain by the sword, v. 6.
The invaders begin with those that come first in their way. And (v. 11)
he shall slay thy people with the sword; not only the soldiers that are
found in arms, but the burghers, shall be put to the sword, the king of
Babylon being highly incensed against them for holding out so long. `(3.)`
The wealth of the city shall all become a spoil to the conqueror (v.
12): They shall make a prey of the merchandise. It was in hope of the
plunder that the city was set upon with so much vigour. See the vanity
of riches, that they are kept for the owners to their hurt; they entice
and recompense thieves, and not only cease to benefit those who took
pains for them and were duly entitled to them, but are made to serve
their enemies, who are thereby put into a capacity of doing them so much
the more mischief. `(4.)` The city itself shall be laid in ruins. All the
pleasant houses shall be destroyed (v. 12), such as were pleasantly
situated, beautified, and furnished, shall become a heap of rubbish. Let
none please themselves too much in their pleasant houses, for they know
not how soon they may see the desolation of them. Tyre shall be utterly
ruined; the enemy shall not only pull down the houses, but shall carry
away the stones and the timber, and shall lay them in the midst of the
water, not to be recovered, or ever made use of again. Nay (v. 4), I
will scrape her dust from her; not only shall the loose dust be blown
away, but the very ground it stands upon shall be torn up by the enraged
enemy, carried off, and laid in the midst of the water, v. 12. The
foundation is in the dust; that dust shall be all taken away, and then
the city must fall of course. When Jerusalem was destroyed it was
ploughed like a field, Mic. 3:12. But the destruction of Tyre is carried
further than that; the very soil of it shall be scraped away, and it
shall be made like the top of a rock (v. 4. 14), pure rock that has no
earth to cover it; it shall only be a place for the spreading of nets
(v. 5. 14); it shall serve fishermen to dry their nets upon and mend
them. `(5.)` There shall be a full period to all its mirth and joy (v.
13): I will cause the noise of thy songs to cease. Tyre had been a
joyous city (Isa. 23:7).; with her songs she had courted customers to
deal with her in a way of trade. But now farewell all her profitable
commerce and pleasant conversation; Tyre is no more a place either of
business or of sport. Lastly, It shall be built no more (v. 14), not
built any more as it had been, with such state and magnificence, nor
built any more in the same place, within the sea, nor built any where
for a long time; the present inhabitants shall be destroyed or
dispersed, so that this Tyre shall be no more. For God has spoken it (v.
5, 14); and when what he has said is accomplished they shall know
thereby that he is the Lord, and not a man that he should lie nor the
son of man that he should repent.

### Verses 15-21

The utter ruin of Tyre is here represented in very strong and lively
figures, which are exceedingly affecting.

`1.` See how high, how great, Tyre had been, how little likely ever to
come to this. The remembrance of men\'s former grandeur and plenty is a
great aggravation of their present disgrace and poverty. Tyre was a
renowned city (v. 17), famous among the nations, the crowning city (so
she is called Isa. 23:8), a city that had crowns in her gift, honoured
all she smiled upon, crowned herself and all about her. She was
inhabited of seas, that is, of those that trade at sea, of those who
from all parts came thither by sea, bringing with them the abundance of
the seas and the treasures hidden in the sand. She was strong in the
sea, easy of access to her friends, but to her enemies inaccessible,
fortified by a wall of water, which made her impregnable. So that she
with her pomp, and her inhabitants with their pride, caused their terror
to be on all that haunted that city, and upon any account frequented it.
It was well fortified, and formidable in the eyes of all that acquainted
themselves with it. Every body stood in awe of the Tyrians and was
afraid of disobliging them. Note, Those who know their strength are too
apt to cause terror, to pride themselves in frightening those they are
an over-match for.

`2.` See how low, how little, Tyre is made, v. 19, 20. This renowned city
is made a desolate city, is no more frequented as it has been; there is
no more resort of merchants to it; it is like the cities not inhabited,
which are no cities, and having none to keep them in repair, will go to
decay of themselves. Tyre shall be like a city overflowed by an
inundation of waters, which cover it, and upon which the deep is brought
up. As the waves had formerly been its defence, so now they shall be its
destruction. She shall be brought down with those that descend into the
pit, with the cities of the old world that were under water, and with
Sodom and Gomorrah, that lie in the bottom of the Dead Sea. Or, she
shall be in the condition of those who have been long buried, of the
people of old time, who are old inhabitants of the silent grace, who are
quite rotted away under ground and quite forgotten above ground; such
shall Tyre be, free among the dead, set in the lower parts of the earth,
humbled, mortified, reduced. It shall be like the places desolate of
old, as well as like persons dead of old; it shall be like other cities
that have formerly been in like manner deserted and destroyed. It shall
not be inhabited again; none shall have the courage to attempt the
rebuilding of it upon that spot, so that it shall be no more; The
Tyrians shall be lost among the nations, so that people will look in
vain for Tyre in Tyre: Thou shalt be sought for, and never found again.
New persons may build a new city upon a new spot of ground hard by,
which they may call Tyre, but Tyre, as it is, shall never be any more.
Note, The strongest cities in this world, the best-fortified and
best-furnished, are subject to decay, and may in a little time be
brought to nothing. In the history of our own island many cities are
spoken of as in being when the Romans were here which now our
antiquaries scarcely know where to look for, and of which there remains
no more evidence than Roman urns and coins digged up there sometimes
accidentally. But in the other world we look for a city that shall stand
for ever and flourish in perfection through all the ages of eternity.

`3.` See what a distress the inhabitants of Tyre are in (v. 15): There is
a great slaughter made in the midst of thee, many slain, and great men.
It is probable that, when the city was taken, the generality of the
inhabitants were put to the sword. Then did the wounded cry, and they
cried in vain, to the pitiless conquerors; they cried quarter, but it
would not be given them; the wounded are slain without mercy, or,
rather, that is the only mercy that is shown them, that the second blow
shall rid them out of their pain.

`4.` See what a consternation all the neighbours are in upon the fall of
Tyre. This is elegantly expressed here, to show how astonishing it
should be. `(1.)` the islands shall shake at the sound of thy fall (v.
15), as, when a great merchant breaks, all that he deals with are
shocked by it, and begin to look about them; perhaps they had effects in
his hands, which they are afraid they shall lose. Or, when they see one
fail and become bankrupt of a sudden, in debt a great deal more than he
is worth, it makes them afraid for themselves, lest they should do so
too. Thus the isles, which thought themselves safe in the embraces of
the sea, when they see Tyrus fall, shall tremble and be troubled,
saying, \"What will become of us?\" And it is well if they make this
good use of it, to take warning by it not to be secure, but to stand in
awe of God and his judgments. The sudden fall of a great tower shakes
the ground round about it; thus all the islands in the Mediterranean Sea
shall feel themselves sensibly touched by the destruction of Tyre, it
being a place they had so much knowledge of, such interests in, and such
a constant correspondence with. `(2.)` The princes of the sea shall be
affected with it, who ruled in those islands. Or the rich merchants, who
live like princes (Isa. 23:8), and the masters of ships, who command
like princes, these shall condole the fall of Tyre in a most
compassionate and pathetic manner (v. 16): They shall come down from
their thrones, as neglecting the business of their thrones and despising
the pomp of them. They shall lay away their robes of state, their
broidered garments, and shall clothe themselves all over with
tremblings, with sackcloth that will make them shiver. Or they shall by
their own act and deed make themselves to tremble upon this occasion;
they shall sit upon the ground in shame and sorrow; they shall tremble
every moment at the thought of what has happened to Tyre, and for fear
of what may happen to themselves; for what island is safe if Tyre be
not? They shall take up a lamentation for thee, shall have elegies and
mournful poems penned upon the fall of Tyre, v. 17. How art thou
destroyed! `[1.]` It shall be a great surprise to them, and they shall
be affected with wonder, that a place so well fortified by nature and
art, so famed for politics and so full of money, which is the sinews of
war, that held out so long and with so much bravery, should be taken at
last (v. 21): I make thee a terror. Note, It is just with God to make
those a terror to their neighbours, by the suddenness and strangeness of
their punishment, who make themselves a terror to their neighbours by
the abuse of their power. Tyre had caused her terror (v. 17) and now is
made a terrible example. `[2.]` It shall be a great affliction to them,
and they shall be affected with sorrow (v. 17); they shall take up a
lamentation for Tyre, as thinking it a thousand pities that such a rich
and splendid city should be thus laid in ruins. When Jerusalem, the holy
city, was destroyed, there were no such lamentations for it; it was
nothing to those that passed by (Lam. 1:12); but when Tyre, the trading
city, fell, it was universally bemoaned. Note, Those who have the world
in their hearts lament the loss of great men more than the loss of good
men. `[3.]` It shall be a loud alarm to them: They shall tremble in the
day of thy fall, because they shall have reason to think that their own
turn will be next. If Tyre fall, who can stand? Howl, fir-trees, if such
a cedar be shaken. Note, The fall of others should awaken us out of our
security. The death or decay of others in the world is a check to us,
when we dream that our mountain stands strongly and shall not be moved.

`5.` See how the irreparable ruin of Tyre is aggravated by the prospect
of the restoration of Israel. Thus shall Tyre sink when I shall set
glory in the land of the living, v. 20. Note, `(1.)` The holy land is the
land of the living; for none but holy souls are properly living souls.
Where living sacrifices are offered to the living God, and where the
lively oracles are, there the land of the living is; there David hoped
to see the goodness of the Lord, Ps. 27:13. That was a type of heaven,
which is indeed the land of the living. `(2.)` Though this land of the
living may for a time lie under disgrace, yet God will again set glory
in it; the glory that had departed shall return, and the restoration of
what they had been deprived of shall be so much more their glory. God
will himself be the glory of the lands that are the lands of the living.
`(3.)` It will aggravate the misery of those that have their portion in
the land of the dying, of those that are for ever dying, to behold the
happiness of those, at the same time, that shall have their everlasting
portion in the land of the living. When the rich man was himself in
torment he saw Lazarus in the bosom of Abraham, and glory set for him in
the land of the living.
