Ezekiel, Chapter 18
===================

Commentary
----------

Perhaps, in reading some of the foregoing chapters, we may have been
tempted to think ourselves not much concerned in them (though they also
were written for our learning); but this chapter, at first view, appears
highly and nearly to concern us all, very highly, very nearly; for,
without particular reference to Judah and Jerusalem, it lays down the
rule of judgment according to which God will deal with the children of
men in determining them to their everlasting state, and it agrees with
that very ancient rule laid down, Gen. 4:7, \"If though doest well,
shalt thou not be accepted?\" But, \"if not, sin,\" the punishment of
sin,\"lies at the door.\" Here is, `I.` The corrupt proverb used by the
profane Jews, which gave occasion to the message here sent them, and
made it necessary for the justifying of God in his dealings with them
(v. 1-3). `II.` The reply given to this proverb, in which God asserts in
general his own sovereignty and justice (v. 4). Woe to the wicked; it
shall be ill with them (v. 4, 20). But say to the righteous, It shall be
ill with them (v. 4, 20). But say to the righteous, It shall be well
with them (v. 5-9). In particular, as to the case complained of, he
assures us, 1. That it shall be ill with a wicked man, though he had a
good father (v. 10-13). 2. That it shall be well with a good man, though
he had a wicked father (v. 14-18). And therefore in this God is
righteous (v. 19, 20). 3. That it shall be well with penitents, though
they began ever so ill (v. 21-23 and 27, 28). 4. That it shall be ill
with apostates, though they began ever so well (v. 24, 26). And the use
of all this is, `(1.)` To justify God and clear the equity of all his
proceedings (v. 25, 29). `(2.)` To engage and encourage us to repent of
our sins and turn to God (v. 30-32). And these are things which belong
to our everlasting peace. O that we may understand and regard them
before they be hidden from our eyes!

### Verses 1-9

Evil manners, we say, beget good laws; and in like manner sometimes
unjust reflections occasion just vindications; evil proverbs beget good
prophecies. Here is,

`I.` An evil proverb commonly used by the Jews in their captivity. We had
one before (ch. 12:22) and a reply to it; here we have another. That
sets God\'s justice at defiance: \"The days are prolonged and every
vision fails; the threatenings are a jest.\" This charges him with
injustice, as if the judgments executed were a wrong: \"You use this
proverb concerning the land of Israel, now that it is laid waste by the
judgments of God, saying, The fathers have eaten sour grapes and the
children\'s teeth are set on edge; we are punished for the sins of our
ancestors, which is as great an absurdity in the divine regimen as if
the children should have their teeth set on edge, or stupefied, by the
fathers\' eating sour grapes, whereas, in the order of natural causes,
if men eat or drink any thing amiss, they only themselves shall suffer
by it.\" Now, 1. It must be owned that there was some occasion given for
this proverb. God had often said that he would visit the iniquity of the
fathers upon the children, especially the sin of idolatry, intending
thereby to express the evil of sin, of that sin, his detestation of it,
and just indignation against it, and the heavy punishments he would
bring upon idolaters, that parents might be restrained from sin by their
affection to their children and that children might not be drawn to sin
by their reverence for their parents. He had likewise often declared by
his prophets that in bringing the present ruin upon Judah and Jerusalem
he had an eye to the sins of Manasseh and other preceding kings; for,
looking upon the nation as a body politic, and punishing them with
national judgments for national sins, and admitting the maxim in our law
that a corporation never dies, reckoning with them now for the
iniquities of former ages was but like making a man, when he is old, to
possess the iniquities of his youth, Job 13:26. And there is no
unrighteousness with God in doing so. But, 2. They intended it as a
reflection upon God, and an impeachment of his equity in his proceedings
against them. Thus far that is right which is implied in this proverbial
saying, That those who are guilty of wilful sin eat sour grapes; they do
that which they will feel from, sooner or later. The grapes may look
well enough in the temptation, but they will be bitter as bitterness
itself in the reflection. They will set the sinner\'s teeth on edge.
When conscience is awake, and sets the sin in order before them, it will
spoil the relish of their comforts as when the teeth are set on edge.
But they suggest it as unreasonable that the children should smart for
the fathers\' folly and feel the pain of that which they never tasted
the pleasure of, and that God was unrighteous in thus taking vengeance
and could not justify it. See how wicked the reflection is, how daring
the impudence; yet see how witty it is, and how sly the comparison. Many
that are impious in their jeers are ingenious in their jests; and thus
the malice of hell against God and religion is insinuated and
propagated. It is here put into a proverb, and that proverb used,
commonly used; they had it up ever and anon. And, though it had plainly
a blasphemous meaning, yet they sheltered themselves under the
similitude from the imputation of downright blasphemy. Now by this it
appears that they were unhumbled under the rod, for, instead of
condemning themselves and justifying God, they condemned him and
justified themselves; but woe to him that thus strives with his Maker.

`II.` A just reproof of, and reply to, this proverb: What mean you by
using it? That is the reproof. \"Do you intend hereby to try it out with
God? Or can you think any other than that you will hereby provoke him to
be angry with you will he has consumed you? Is this the way to reconcile
yourselves to him and make your peace with him?\" The reply follows, in
which God tells them,

`1.` That the use of the proverb should be taken away. This is said, it
is sworn (v. 3): You shall not have occasion any more to use this
proverb; or (as it may be read), You shall not have the use of this
parable. The taking away of this parable is made the matter of a
promise, Jer. 31:29. Here it is made the matter of a threatening. There
it intimates that God will return to them in ways of mercy; here it
intimates that God would proceed against them in ways of judgment. He
will so punish them for this impudent saying that they shall not dare to
use it any more; as in another case, Jer. 23:34, 36. God will find out
effectual ways to silence those cavillers. Or God will so manifest both
to themselves and others that they have wickedness of their own enough
to bring all these desolating judgments upon them that they shall no
longer for shame lay it upon the sins of their fathers that they were
thus dealt with: \"Your own consciences shall tell you, and all your
neighbours shall confirm it, that you yourselves have eaten the same
sour grapes that your fathers ate before you, or else your teeth would
not have been set on edge.\"

`2.` That really the saying itself was unjust and a causeless reflection
upon God\'s government. For,

`(1.)` God does not punish the children for the fathers\' sins unless they
tread in their fathers\' steps and fill up the measure of their iniquity
(Mt. 23:32), and then they have no reason to complain, for, whatever
they suffer, it is less than their own sin has deserved. And, when God
speaks of visiting the iniquity of the fathers upon the children, that
is so far from putting any hardship upon the children, to whom he only
renders according to their works, that it accounts for God\'s patience
with the parents, whom he therefore does not punish immediately, because
he lays up their iniquity for their children, Job 21:19.

`(2.)` It is only in temporal calamities that children (and sometimes
innocent ones) fare the worse for their parents\' wickedness, and God
can alter the property of those calamities, and make them work for good
to those that are visited with them; but as to spiritual and eternal
misery (and that is the death here spoken of) the children shall by no
means smart for the parents\' sins. This is here shown at large; and it
is a wonderful piece of condescension that the great God is pleased to
reason the case with such wicked and unreasonable men, that he did not
immediately strike them dumb or dead, but vouchsafed to state the matter
before them, that he may be clear when he is judged. Now, in his reply,

`[1.]` He asserts and maintains his own absolute and incontestable
sovereignty: Behold, all souls are mine, v. 4. God here claims a
property in all the souls of the children of men, one as well as
another. First, Souls are his. He that is the Maker of all things is in
a particular manner the Father of spirits, for his image is stamped on
the souls of men; it was so in their creation; it is so in their
renovation. He forms the spirit of man within him, and is therefore
called the God of the spirits of all flesh, of embodied spirits.
Secondly, All souls are his, all created by him and for him, and
accountable to him. As the soul of the father, so the soul of the son,
is mine. Our earthly parents are only the fathers of our flesh; our
souls are not theirs; God challenges them. Now hence it follows, for the
clearing of this matter, 1. That God may certainly do what he pleases
both with fathers and children, and none may say unto him, What doest
thou? He that gave us our being does us no wrong if he takes it away
again, much less when he only takes away some of the supports and
comforts of it; it is as absurd to quarrel with him as for the thing
formed to say to him that formed it, Why hast thou made me thus? 2. That
God as certainly bears a good-will both to father and son, and will put
no hardship upon either. We are sure that God hates nothing that he has
made, and therefore (speaking of the adult, who are capable of acting
for themselves) he has such a kindness for all souls that none die but
through their own default. All souls are his, and therefore he is not
partial in his judgment of them. Let us subscribe to his interest in us
and dominion over us. He says, All souls are mine; let us answer,
\"Lord, my soul is thine; I devote it to thee to be employed for thee
and made happy in thee.\" It is with good reason that God says, \"My
son, give me thy heart, for it is my own,\" to which we must yield,
\"Father, take my heart, it is thy own.\"

`[2.]` Though God might justify himself by insisting upon his
sovereignty, yet he waives that, and lays down the equitable and
unexceptionable rule of judgment by which he will proceed as to
particular persons; and it is this:-First, The sinner that persists in
sin shall certainly die, his iniquity shall be his ruin: The soul that
sins shall die, shall die as a soul can die, shall be excluded from the
favour of God, which is the life and bliss of the soul, and shall lie
for ever under his wrath, which is its death and misery. Sin is the act
of the soul, the body being only the instrument of unrighteousness; it
is called the sin of the soul, Mic. 6:7. And therefore the punishment of
sin is the tribulation and the anguish of the soul, Rom. 2:9. Secondly,
The righteous man that perseveres in his righteousness shall certainly
live. If a man be just, have a good principle, a good spirit and
disposition, and, as an evidence of that, do judgment and justice (v.
5), he shall surely live, saith the Lord God, v. 9. He that makes
conscience of conforming in every thing to the will of God, that makes
it his business to serve God and his aim to glorify God, shall without
fail be happy here and for ever in the love and favour of God; and,
wherein he comes short of his duty, it shall be forgiven him, through a
Mediator. Now here is part of the character of this just man. 1. He is
careful to keep himself clean from the pollutions of sin, and at a
distance from all the appearances of evil. `(1.)` From sins against the
second commandment. In the matters of God\'s worship he is jealous, for
he knows God is so. He has not only not sacrificed in the high places to
the images there set up, but he has not so much as eaten upon the
mountains, that is, not had any communion with idolaters by eating
things sacrificed to idols, 1 Co. 10:20. He would not only not kneel
with them at their altars, but not sit with them at their tables in
their high places. He detests not only the idols of the heathen but the
idols of the house of Israel, which were not only allowed of, but
generally applauded and adored, by those that were accounted the
professing people of God. He has not only not worshipped those idols,
but he has not so much as lifted up his eyes to them; he has not given
them a favourable look, has had no regard at all to them, neither
desired their favour nor dreaded their frowns. He has observed so many
bewitched by them that he has not dared so much as to look at them, lest
he should be taken in the snare. The eyes of idolaters are said to go a
whoring, Eze. 6:9. See Deu. 4:19. `(2.)` From sins against the seventh
commandment. He is careful to possess his vessel in sanctification and
honour, and not in the lusts of uncleanness; and therefore he has not
dared to defile his neighbour\'s wife, nor said or done any thing which
had the least tendency to corrupt or debauch her, no, nor will he make
any undue approaches to his own wife when she is put apart for her
uncleanness, for it was forbidden by the law, Lev. 18:19; 20:18. Note,
It is an essential branch of wisdom and justice to keep the appetites of
the body always in subjection to reason and virtue. `(3.)` From sins
against the eighth commandment. He is a just man, who has not, by fraud
and under colour of law and right, oppressed any, and who has not with
force and arms spoiled any by violence, not spoiled them of their goods
or estates, much less of their liberties and lives, v. 7. Oppression and
violence were the sins of the old world, that brought the deluge, and
are sins of which still God is and will be the avenger. Nay, he is one
that has not lent his money upon usury, nor taken increase (v. 8),
though, being done by contract, it may seem free from injustice (Volenti
non fit injuria-What is done to a person with his own consent is no
injury to him), yet, as far as it is forbidden by the law, he dares not
do it. A moderate usury they were allowed to receive from strangers, but
not from their brethren. A just man will not take advantage of his
neighbour\'s necessity to make a prey of him, nor indulge himself in
ease and idleness to live upon the sweat and toil of others, and
therefore will not take increase from those who cannot make increase of
what he lends them, nor be rigorous in exacting what was agreed for from
those who by the act of God are disabled to pay it; but he is willing to
share in loss as well as profit. Qui sentit commodum, sentire debet et
onus-He who enjoys the benefit should bear the burden. 2. He makes
conscience of doing the duties of his place. He has restored the pledge
to the poor debtor, according to the law. Ex. 22:26. \"If thou take thy
neighbour\'s raiment for a pledge, the raiment that is for necessary
use, thou shalt deliver it to him again, that he may sleep in his own
bedclothes.\" Nay, he has not only restored to the poor that which was
their own, but has given his bread to the hungry. Observe, It is called
his bread, because it is honestly come by; that which is given to some
is not unjustly taken from others; for God has said, I hate robbery for
burnt-offerings. Worldly men insist upon it that their bread is their
own, as Nabal, who therefore would not give of it to David (1 Sa.
25:11); yet let them know that it is not so their own but that they are
bound to do good to others with it. Clothes are necessary as well as
food, and therefore this just man is so charitable as to cover the naked
also with a garment, v. 7. The coats which Dorcas had made for the poor
were produced as witnesses of her charity, Acts 9:39. This just man has
withdrawn his hands from iniquity, v. 8. If at any time he has been
drawn in through inadvertency to that which afterwards has appeared to
him to be a wrong thing, he does not persist in it because he has begun
it, but withdraws his hand from that which he now perceives to be
iniquity; for he executes true judgment between man and man, according
as his opportunity is of doing it (as a judge, as a witness, as a
juryman, as a referee), and in all commerce is concerned that justice be
done, that no man be wronged, that he who is wronged be righted, and
that every man have his own, and is ready to interpose himself, and do
any good office, in order hereunto. This is his character towards his
neighbours; yet it will not suffice that he be just and true to his
brother, to complete his character he must be so to his God likewise (v.
9): He has walked in my statutes, those which relate to the duties of
his immediate worship; he has kept those and all his other judgments,
has had respect to them all, has made it his constant care and endeavour
to conform and come up to them all, to deal truly, that so he may
approve himself faithful to his covenant with God, and, having joined
himself to God, he does not treacherously depart from him, nor dissemble
with him. This is a just man, and living he shall live; he shall
certainly live, shall have life and shall have it more abundantly, shall
live truly, live comfortably, live eternally. Keep the commandments, and
thou shalt enter into life, Mt. 19:17.

### Verses 10-20

God, by the prophet, having laid down the general rule of judgment, that
he will render eternal life to those that patiently continue in
well-doing, but indignation and wrath to those that do not obey the
truth, but obey unrighteousness (Rom. 2:7, 8), comes, in these verses,
to show that men\'s parentage and relation shall not alter the case
either one way or other.

`I.` He applied it largely and particularly both ways. As it was in the
royal line of the kings of Judah, so it often happens in private
families, that godly parents have wicked children and wicked parents
have godly children. Now here he shows,

`1.` That a wicked man shall certainly perish in his iniquity, though he
be the son of a pious father. If that righteous man before described
beget a son whose character is the reverse of his father\'s, his
condition will certainly be so too. `(1.)` It is supposed as no uncommon
case, but a very melancholy one, that the child of a very godly father,
notwithstanding all the instructions given him, the good education he
has had and the needful rebukes that have been given him, and the
restraints he has been laid under, after all the pains taken with him
and prayers put up for him, may yet prove notoriously wicked and vile,
the grief of his father, the shame of his family, and the curse and
plague of his generation. He is here supposed to allow himself in all
those enormities which his good father dreaded and carefully avoided,
and to shake off all those good duties which his father made conscience
of and took satisfaction in; he undoes all that his father did, and goes
counter to his example in every thing. He is here described to be a
highwayman-a robber and a shedder of blood. He is an idolater: He has
eaten upon the mountains (v. 11) and has lifted up his eyes to the
idols, which his good father never did, and has come at length not only
to feast with the idolaters, but to sacrifice with them, which is here
called committing abomination, for the way of sin is down-hill. He is an
adulterer, has defiled his neighbour\'s wife. He is an oppressor even of
the poor and needy; he robs the spital, and squeezes those who, he
knows, cannot defend themselves, and takes a pride and pleasure in
trampling upon the weak and impoverishing those that are poor already.
He takes away from those to whom he should give. He has spoiled by
violence and open force; he has given forth upon usury, and so spoiled
by contract; and he has not restored the pledge, but unjustly detained
it even when the debt was paid. Let those good parents that have wicked
children not look upon their case as singular; it is a case put here;
and by it we see that grace does not run in the blood, nor always attend
the means of grace. The race is not always to the swift, nor the battle
to the strong, for then the children that are well taught would do well,
but God will let us know that his grace is his own and his Spirit a
free-agent, and that though we are tied to give our children a good
education he is not tied to bless it. In this, as much as any thing,
appears the power of original sin and the necessity of special grace.
`(2.)` We are here assured that this wicked man shall perish for ever in
his iniquity, notwithstanding his being the son of a good father. He may
perhaps prosper awhile in the world, for the sake of the piety of his
ancestors, but, having committed all these abominations, and never
repented of them, he shall not live, he shall not be happy in the favour
of God; though he may escape the sword of men, he shall not escape the
curse of God. He shall surely die; he shall be for ever miserable; his
blood shall be upon him. He may thank himself; he is his own destroyed.
And his relation to a good father will be so far from standing him in
stead that it will aggravate his sin and his condemnation. It made his
sin the more heinous, nay, it made him really the more vile and
profligate, and, consequently, will make his misery hereafter the more
intolerable.

`2.` That a righteous man shall be certainly happy, though he be the son
of a wicked father. Though the father did eat the sour grapes, if the
children do not meddle with them, they shall fare never the worse for
that. Here, `(1.)` It is supposed (and, blessed be God, it is sometimes a
case in fact) that the son of an ungodly father may be godly, that,
observing how fatal his father\'s errors were, he may be so wise as to
take warning, and not tread in his father\'s tests, v. 14. Ordinarily,
children partake of the parents\' temper and are drawn in to imitate
their example; but here the son, instead of seeing his father\'s sins,
and, as is usual, doing the like, sees them and dreads doing the like.
Men indeed do not gather grapes of thorns, but God sometimes does, takes
a branch from a wild olive and grafts it into a good one. Wicked Ahaz
begets a good Hezekiah, who sees all his father\'s sins which he has
done, and though he will not, like Ham, proclaim his father\'s shame, or
make the worst of it, yet he loathes it, and blushes at it, and thinks
the worse of sin because it was the reproach and ruin of his own father.
He considers and does not such like; he considers how ill it became his
father to do such things, what an offence it was to God and all good
men, what a wound and dishonour he got by it, and what calamities he
brought into his family, and therefore he does not such like. Note, If
we did but duly consider the ways of wicked men, we should all dread
being associates with them and followers of them. The particulars are
here again enumerated almost in the same words with that character given
of the just man (v. 6, etc.), to show how good men walk in the same
spirit and in the same steps. This just man here, when he took care to
avoid his father\'s sins, took care to imitate his grandfather\'s
virtues; and, if we look back, we shall find some examples for our
imitation, as well as others for our admonition. This just man can not
only say, as the Pharisee, I am no adulterer, no extortioner, no
oppressor, no usurer, no idolater; but he has given his bread to the
hungry and covered the naked. He has taken off his hand from the poor;
where he found his father had put hardships upon poor servants, tenants,
neighbours, he eased their burden. He did not say, \"What my father has
done I will abide by, and if it was a fault it was his and not mine;\"
as Rehoboam, who contemned the taxes his father had imposed. No; he
takes his hand off from the poor, and restores them to their rights and
liberties again, v. 15-17. Thus he has executed God\'s judgments and
walked in his statutes, not only done his duty for once, but one on in a
course and way of obedience. `(2.)` We are assured that the graceless
father alone shall die in his iniquity, but his gracious son shall fare
never the worse for it. As for his father (v. 18), because he was a
cruel oppressor, and did hurt, nay, because, though he had wealth and
power, he did not with them do good among his people, lo, even he, great
as he is, shall die in his iniquity, and be undone for ever; but he that
kept his integrity shall surely live, shall be easy and happy, and he
shall not die for the iniquity of his father. Perhaps his father\'s
wickedness has lessened his estate and weakened his interest, but it
shall be no prejudice at all to his acceptance with God and his eternal
welfare.

`II.` He appeals to themselves then whether they did not wrong God with
their proverb. \"Thus plain the case is, and yet you say, Does not the
son bear the iniquity of the father? No, he does not; he shall not if he
will himself do that which is lawful and right,\" v. 19. But this people
that bore the iniquity of their fathers had not done that which is
lawful and right, and therefore justly suffered for their own sin and
had no reason to complain of God\'s proceedings against them as at all
unjust, though they had reason to complain of the bad example their
fathers had left them as very unkind. Our fathers have sinned and are
not, and we have borne their iniquity, Lam. 5:7. It is true that there
is a curse entailed upon wicked families, but it is as true that the
entail may be cut off by repentance and reformation; let the impenitent
and unreformed therefore thank themselves if they fall under it. The
settled rule of judgment is therefore repeated (v. 20): The soul that
sins shall die, and not another for it. What direction God has given to
earthly judges (Deu. 24:16) he will himself pursue: The son shall not
die, not die eternally, for the iniquity of the father, if he do not
tread in the steps of it, nor the father for the iniquity of the son, if
he endeavour to do his duty for the preventing of it. In the day of the
revelation of the righteous judgment of God, which is now clouded and
eclipsed, the righteousness of the righteous shall appear before all the
world to be upon him, to his everlasting comfort and honour, upon him as
a robe, upon his as a crown; and the wickedness of the wicked shall be
upon him, to his everlasting confusion, upon him as a chain, upon him as
a load, as a mountain of lead to sink him to the bottomless pit.

### Verses 21-29

We have here another rule of judgment which God will go by in dealing
with us, by which is further demonstrated the equity of his government.
The former showed that God will reward or punish according to the change
made in the family or succession, for the better or for the worse; here
he shows that he will reward or punish according to the change made in
the person himself, whether for the better or the worse. While we are in
this world we are in a state of probation; the time of trial lasts as
long as the time of life, and according as we are found at last it will
be with us to eternity. Now see here,

`I.` The case fairly stated, much as it had been before (ch. 3:18, etc.),
and here it is laid down once (v. 21-24) and again (v. 26-28), because
it is a matter of vast importance, a matter of life and death, of life
and death eternal. Here we have,

`1.` A fair invitation given to wicked people, to turn from their
wickedness. Assurance is here given us that, if the wicked will turn, he
shall surely live, v. 21, 27. Observe,

`(1.)` What is required to denominate a man a true convert, how he must be
qualified that he may be entitled to this act of indemnity. `[1.]` The
first step towards conversion is consideration (v. 28): Because he
considers and turns. The reason why sinners go on in their evil ways is
because they do not consider what will be in the end thereof; but if the
prodigal once come to himself, if he sit down and consider a little how
bad his state is and how easily it may be bettered, he will soon return
to his father (Lu. 15:17), and the adulteress to her first husband when
she considers that then it was better with her than now, Hos. 2:7.
`[2.]` This consideration must produce an aversion to sin. When he
considers he must turn away from his wickedness, which denotes a change
in the disposition of the heart; he must turn from his sins and his
transgression, which denotes a change in the life; he must break off
from all his evil courses, and, wherein he has done iniquity, must
resolve to do so no more, and this from a principle of hatred to sin.
What have I to do any more with idols? `[3.]` This aversion to sin must
be universal; he must turn from all his sins and all his transgressions,
without a reserve for any Delilah, any house of Rimmon. We do not
rightly turn from sin unless we truly hate it, and we do not truly hate
sin, as sin, if we do not hate all sin. `[4.]` This must be accompanied
with a conversion to God and duty; he must keep all God\'s statutes (for
the obedience, if it be sincere, will be universal) and must do that
which is lawful and right, that which agrees with the word and will of
God, which he must take for his rule, and not the will of the flesh and
the way of the world.

`(2.)` What is promised to those that do thus turn from sin to God. `[1.]`
They shall save their souls alive, v. 27. They shall surely live, they
shall not die, v. 21. and again v. 28. Whereas it was said, The soul
that sins it shall die, yet let not those that have sinned despair but
that the threatened death may be prevented if they will but turn and
repent in time. When David penitently acknowledges, I have sinned, he is
immediately assured of his pardon: \"The Lord has taken away thy sin,
thou shalt not die (2 Sa. 12:13), thou shalt not die eternally.\" He
shall surely live; he shall be restored to the favour of God, which is
the life of the soul, and shall not lie under his wrath, which is as
messengers of death to the soul. `[2.]` The sins they have repented of
and forsaken shall not rise up in judgment against them, nor shall they
be so much as upbraided with them: All his transgressions that he has
committed, though numerous, though heinous, though very provoking to
God, and redounding very much to his dishonour, yet they shall not be
mentioned unto him (v. 22), not mentioned against them; not only they
shall not be imputed to him to ruin him, but in the great day they shall
not be remembered against him to grieve or shame him; they shall be
covered, shall be sought for and not found. This intimates the fulness
of pardoning mercy; when sin is forgiven it is blotted out, it is
remembered no more. `[3.]` In their righteousness they shall live; not
for their righteousness, as if that were the purchase of their pardon
and bliss and an atonement for their sins, but in their righteousness,
which qualifies them for all the blessings purchased by the Mediator,
and is itself one of those blessings.

`(3.)` What encouragement a repenting returning sinner has to hope for
pardon and life according to this promise. He is conscious to himself
that his obedience for the future can never be a valuable compensation
for his former disobedience; but he has this to support himself with,
that God\'s nature, property, and delight, is to have mercy and to
forgive, for he has said (v. 23): \"Have I any pleasure at all that the
wicked should die? No, by no means; you never had any cause given you to
think so.\" It is true God has determined to punish sinners; his justice
calls for their punishment, and, pursuant to that, impenitent sinners
will lie for ever under his wrath and curse; that is the will of his
decree, his consequent will, but it is not his antecedent will, the will
of his delight. Though the righteousness of his government requires that
sinners die, yet the goodness of his nature objects against it. How
shall I give thee up, Ephraim? It is spoken here comparatively; he has
not pleasure in the ruin of sinners, for he would rather they should
turn from their ways and live; he is better pleased when his mercy is
glorified in their salvation than when his justice is glorified in their
damnation.

`2.` A fair warning given to righteous people not to turn from their
righteousness, v. 24-26. Here is, `(1.)` The character of an apostate,
that turns away from his righteousness. He never was in sincerity a
righteous man (as appears by that of the apostle, 1 Jn. 2:19, If they
had been of us, they would, no doubt, have continued with us), but he
passed for a righteous man. He had the denomination and all the external
marks of a righteous man; he thought himself one, and others thought him
one. But he throws of his profession, leaves his first love, disowns and
forsakes the truth and ways of God, and so turns away from his
righteousness as one sick of it, and now shows, what he always had, a
secret aversion to it; and, having turned away from his righteousness,
he commits iniquity, grows loose, and profane, and sensual, intemperate,
unjust, and, in short, does according to all the abominations that the
wicked man does; for, when the unclean spirit recovers his possession of
the heart, he brings with him seven other spirits more wicked than
himself and they enter in and dwell there, Lu. 11:26. `(2.)` The doom of
an apostate: Shall he live because he was once a righteous man? No;
factum non dicitur quod non perseverat-that which does not abide is not
said to be done. In his trespass (v. 24) and for his iniquity (that is
the meritorious cause of his ruin), for the iniquity that he has done,
he shall die, shall die eternally, v. 26. The backslider in heart shall
be filled with his own ways. But will not his former professions and
performances stand him in some stead-will they not avail at least to
mitigate his punishment? No: All his righteousness that he has done,
though ever so much applauded by men, shall not be mentioned so as to be
either a credit or a comfort to him; the righteousness of an apostate is
forgotten, as the wickedness of a penitent is. Under the law, if a
Nazarite was polluted he lost all the foregoing days of his separation
(Num. 6:12), so those that have begun in the spirit and end in the flesh
may reckon all their past services and sufferings in vain (Gal. 3:3, 4);
unless we persevere we lose what we have gained, 2 Jn. 8.

`II.` An appeal to the consciences even of the house of Israel, though
very corrupt, concerning God\'s equity in all these proceedings; for he
will be justified, as well as sinners judged, out of their own mouths.
`1.` The charge they drew up against God is blasphemous, v. 25, 29. The
house of Israel has the impudence to say, The way of the Lord is not
equal, than which nothing could be more absurd as well as impious. He
that formed the eye, shall he not see? Can his ways be unequal whose
will is the eternal rule of good and evil, right and wrong? Shall not
the Judge of all the earth do right? No doubt he shall; he cannot do
otherwise. 2. God\'s reasonings with them are very gracious and
condescending, for even these blasphemers God would rather have
convinced and saved than condemned. One would have expected that God
would immediately vindicate the honour of his justice by making those
that impeached it eternal monuments of it. Must those be suffered to
draw another breath that have once breathed out such wickedness as this?
Shall that tongue ever speak again any where but in hell that has once
said, The ways of the Lord are not equal? Yes, because this is the day
of God\'s patience, he vouchsafes to argue with them; and he requires
them to own, for it is so plain that they cannot deny, `(1.)` The equity
of his ways: Are not my ways equal? No doubt they are. He never lays
upon man more than is right. In the present punishments of sinners and
the afflictions of his own people, yea, and in the eternal damnation of
the impenitent, the ways of the Lord are equal. `(2.)` The iniquity of
their ways: \"Are not your ways unequal? It is plain that they are, and
the troubles you are in you have brought upon your own heads. God does
you no wrong, but you have wronged yourselves.\" The foolishness of man
perverts his way, makes that unequal, and then his heart frets against
the Lord, as if his ways were unequal, Prov. 19:3. In all our disputes
with God, and in all his controversies with us, it will be found that
his ways are equal, but ours are unequal, that he is in the right and we
are in the wrong.

### Verses 30-32

We have here the conclusion and application of this whole matter. After
a fair trial at the bar of right reason the verdict is brought in on
God\'s side; it appears that his ways are equal. Judgment therefore is
next to be given; and one would think it should be a judgment of
condemnation, nothing short of Go, you cursed, into everlasting fire.
But, behold, a miracle of mercy; the day of grace and divine patience is
yet lengthened out; and therefore, though God will at last judge every
one according to his ways, yet he waits to be gracious, and closes all
with a call to repentance and a promise of pardon upon repentance.

`I.` Here are four necessary duties that we are called to, all amounting
to the same:-1. We must repent; we must change our mind and change our
ways; we must be sorry for what we have done amiss and ashamed of it,
and go as far as we can towards the undoing of it again. 2. We must turn
ourselves from all our transgressions, v. 30 and again v. 32. Turn
yourselves, face about; turn from sin, nay, turn against it as the enemy
you loathe, turn to God as the friend you love. 3. We must cast away
from us all our transgressions; we must abandon and forsake them with a
resolution never to return to them again, give sin a bill of divorce,
break all the leagues we have made with it, throw it overboard, as the
mariners did Jonah (for it has raised the storm), cast it out of the
soul, and crucify it as a malefactor. 4. We must make us a new heart and
a new spirit. This was the matter of a promise, ch. 11:19. Here it is
the matter of a precept. We must do our endeavour, and then God will not
be wanting to us to give us his grace. St. Austin well explains this
precept. Deus non jubet impossibilia, sed jubendo monet et facere quod
possis et petere quod non possis-God does not enjoin impossibilities,
but by his commands admonishes us to do what is in our power and to pray
for what is not.

`II.` Here are four good arguments used to enforce these calls to
repentance:-1. It is the only way, and it is a sure way, to prevent the
ruin which our sins have a direct tendency to: So iniquity shall not be
your ruin, which implies that, if we do not repent, iniquity will be our
ruin, here and for ever, but that, if we do, we are safe, we are
snatched as brands out of the burning. 2. If we repent not, we certainly
perish, and our blood will be upon our own heads. Why will you die, O
house of Israel? What an absurd thing it is for you to choose death and
damnation rather than life and salvation. Note, The reason why sinners
die is because they will die; they will go down the way that leads to
death, and not come up to the terms on which life is offered. Herein
sinners, especially sinners of the house of Israel, are most
unreasonable and act most unaccountably. 3. The God of heaven has no
delight in our ruin, but desires our welfare (v. 32): I have no pleasure
in the death of him that dies, which implies that he has pleasure in the
recovery of those that repent; and this is both an engagement and an
encouragement to us to repent. 4. We are made for ever if we repent:
Turn yourselves, and live. He that says to us, Repent, thereby says to
us, Live, yea, he says to us, Live; so that life and death are here set
before us.
