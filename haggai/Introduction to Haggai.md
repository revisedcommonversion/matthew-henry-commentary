Introduction to Haggai
======================

The captivity in Babylon gave a very remarkable turn to the affairs of
the Jewish church both in history and prophecy. It is made a signal
epocha in our Saviour\'s genealogy, Mt. 1:17. Nine of the twelve minor
prophets, whose oracles we have been hitherto consulting, lived and
preached before that captivity, and most of them had an eye to it in
their prophecies, foretelling it as the just punishment of Jerusalem\'s
wickedness. But the last three (in whom the Spirit of prophecy took its
period, until it revived in Christ\'s forerunner) lived and preached
after the return out of captivity, not immediately upon it, but some
time after. Haggai and Zechariah appeared much about the same time,
eighteen years after the return, when the building of the temple was
both retarded by its enemies and neglected by its friends. Then the
prophets, Haggai the prophet and Zechariah the son of Iddo, prophesied
unto the Jews that were in Jerusalem, in the name of the God of Israel,
even unto them (so we read Ezra 5:1), to reprove them for their
remissness, and to encourage them to revive that good work when it had
stood still for some time, and to go on with it vigorously,
notwithstanding the opposition they met with in it. Haggai began two
months before Zechariah, who was raised up to second him, that out of
the mouth of two witnesses the word might be established. But Zechariah
continued longer at the work; for all Haggai\'s prophecies that are
recorded were delivered within four months, in the second year of
Darius, between the beginning of the sixth month and the end of the
ninth. But we have Zechariah\'s prophecies dated above two years after,
Zec. 7:1. Some have the honour to lead, others to last, in the work of
God. The Jews ascribe to these two prophets the honour of being members
of the great synagogue (as they call it), which was formed after the
return out of captivity; we think it more certain, and it was their
honour, and a much greater honour, that they prophesied of Christ.
Haggai spoke of him as the glory of the latter house, and Zechariah as
the man, the branch. In them the light of that morning star shone more
brightly than in the foregoing prophecies, as they lived nearer the time
of the rising of the Sun of righteousness, and now began to see his day
approaching. The Septuagint makes Haggai and Zechariah to be the penmen
of Ps. 138 and Ps. 146, 147, and 148.
