Amos, Chapter 7
===============

Commentary
----------

In this chapter we have, `I.` God contending with Israel, by the
judgments, but are reprieved, and the judgments turned away at the
prayer of Amos (v. 1-6). 2. God\'s patience is at length worn out by
their obstinacy, and they are rejected, and sentenced to utter ruin (v.
7-9). `II.` Israel contending with God, by the opposition given to his
prophet. 1. Amaziah informs against Amos (v. 10, 11) and does what he
can to rid the country of him as a public nuisance (v. 12, 13). 2. Amos
justifies himself in what he did as a prophet (v. 14, 15) and denounces
the judgments of God against Amaziah his prosecutor (v. 16, 17); for,
when the contest is between God and man, it is easy to foresee, it is
very easy to foretel, who will come off with the worst of it.

### Verses 1-9

We here see that God bears long, but that he will not bear always, with
a provoking people, both these God here showed the prophet: Thus hath
the Lord God showed me, v. 1, 4, 7. He showed him what was present,
foreshowed him what was to come, gave him the knowledge both of what he
did and of what he designed; for the Lord God reveals his secret unto
his servants the prophets, ch. 3:7.

`I.` We have here two instances of God\'s sparing mercy, remembered in the
midst of judgment, the narratives of which are so much like one another
that they will be best considered together, and very considerable they
are.

`1.` God is here coming forth against this sinful nation, first by one
judgment and then by another. `(1.)` He begins with the judgment of
famine. The prophet saw this in vision. He saw God forming grasshoppers,
or locusts, and bringing them up upon the land, to eat up the fruits of
it, and so to strip it of its beauty and starve its inhabitants, v. 1.
God formed these grasshoppers, not only as they were his creatures (and
much of the wisdom and power of God appears in the formation of minute
animals, as much in the structure of an ant as of an elephant), but as
they were instruments of his wrath. God is said to frame evil against a
sinful people, Jer. 18:11. These grasshoppers were framed on purpose to
eat up the grass of the land; and vast numbers of them were prepared
accordingly. They were sent in the beginning of the shooting up of the
latter growth, after the king\'s mowings. See here how the judgment was
mitigated by the mercy that went before it. God could have sent these
insects to eat up the grass at the beginning of the first growth, in the
spring, when the grass was most needed, was most plentiful, and was the
best in its kind; but God suffered that to grow, and suffered them to
gather it in; the king\'s mowings were safely housed, for the king
himself is served from the field (Eccl. 5:9), and could as ill be
without his mowings as without any other branch of his revenues. Uzziah,
who was now king of Judah, loved husbandry, 2 Chr. 26:10. But the
grasshoppers were commissioned to eat up only the latter growth (the
edgrew we call it in the country), the after-grass, which is of little
value in comparison with the former. The mercies which God give us, and
continues to us, are more numerous and more valuable than those he
removes from us, which is a good reason why we should be thankful and
not complain. The remembrance of the mercies of the former growth should
make us submissive to the will of God when we meet with disappointments
in the latter growth. The prophet, in vision, saw this judgment
prevailing far. These grasshoppers ate up the grass of the land, which
should have been for the cattle, which the owners must of course suffer
by. Some understand this figuratively of a wasting destroying army
brought upon them. In the days of Jeroboam the kingdom of Israel began
to recover itself from the desolations it had been under in the former
reigns (2 Ki. 14:25); the latter growth shot up, after the mowings of
the kings of Syria, which we read of 2 Ki. 13:3. And then God
commissioned the king of Assyria with an army of caterpillars to come
upon them and lay them waste, that nation spoken of ch. 6:14, which
afflicted them from the entering of Hamath to the river of the
wilderness, which seems to refer to 2 Ki. 14:25, where Jeroboam is said
to have restored their coast from the entering of Hamath to the sea of
the plain. God can bring all to ruin when we think all is in some good
measure repaired. `(2.)` He proceeds to the judgment of fire, to show that
he has many arrows in his quiver, many ways of humbling a sinful nation
(v. 4): The Lord God called to contend by fire. He contended, for God\'s
judgment upon a people are his controversies with them; in them he
prosecutes his action against them; and his controversies are neither
causeless nor groundless. He called to contend; he did by his prophets
give them notice of his controversy, and drew up a declaration, setting
forth the meaning of it. Or he called for his angels, or other ministers
of his justice, that were to be employed in it. A fire was kindled among
them, by which perhaps is meant a great drought (the heat of the sun,
which should have warmed the earth, scorched it, and burnt up the roots
of the grass which the locusts had eaten the spires of), or a raging
fever, which was as a fire in their bones, which devoured and ate up
multitudes, or lightning, fire from heaven, which consumed their houses,
as Sodom and Gomorrah were consumed (ch. 4:11), or it was the burning of
their cities, either by accident or by the hand of the enemy, for fire
and sword used to go together; thus were the towns wasted, as the
country was by the grasshoppers. This fire, which God called for, did
terrible execution; it devoured the great deep, as the fire that fell
from heaven on Elijah\'s altar licked up the water that was in the
trench. Though the water designed for the stopping and quenching of this
fire was as the water of the great deep, yet it devoured it; for who, or
what, can stand before a fire kindled by the wrath of God! It did eat up
a part, a great part, of the cities where it was sent; or it was as the
fire at Taberah, which consumed the outermost parts of the camp (Num.
11:1); when some were overthrown others were as brands plucked out of
the fire. All deserved to be devoured, but it ate up only a part, for
God does not stir up all his wrath.

`2.` The prophet goes forth to meet him in the way of his judgments, and
by prayer seeks to turn away his wrath, v. 2. When he saw, in vision,
what dreadful work these caterpillars made, that they had eaten up in a
manner all the grass of the land (he foresaw they would do so, if
suffered to go on), then he said, O Lord God! forgive, I beseech thee
(v. 2); cease, I beseech thee, v. 5. He that foretold the judgment in
his preaching to the people, yet deprecated it in his intercessions for
them. He is a prophet, and he shall pray for thee. It was the business
of prophets to pray for those to whom they prophesied, and so to make it
appear that though they denounced they did not desire the woeful day.
Therefore, God showed his prophets the evils coming, that they might
befriend the people, not only by warning them, but by praying for them,
and standing in the gap, to turn away God\'s wrath, as Moses, that great
prophet, often did. Now observe here,

`(1.)` The prophet\'s prayer: O Lord God! `[1.]` Forgive, I beseech thee,
and take away the sin, v. 2. He sees sin at the bottom of the trouble,
and therefore concludes that the pardon of sin must be at the bottom of
deliverance, and prays for that in the first place. Note, Whatever
calamity we are under, personal or public, the forgiveness of sin is
that which we should be most earnest with God for. `[2.]` Cease, I
beseech thee, and take away the judgment; cease the fire, cease the
controversy; cause they anger towards us to cease. This follows upon the
forgiveness of sin. Take away the cause and effect will cease. Note,
Those whom God contends with will soon find what need they have to cry
for a cessation of arms; and there are hopes that though God has begun,
and proceeded far, in his controversy, yet it may be obtained.

`(2.)` The prophet\'s plea to enforce this prayer: By whom shall Jacob
arise, for he is small? v. 2. And it is repeated (v. 5) and yet no vain
repetition. Christ, in his agony, prayed earnestly, saying the same
words, again and again. `[1.]` It is Jacob that he is interceding for,
the professing people of God, called by his name, calling on his name,
the seed of Jacob, his chosen, and in covenant with him. It it Jacob\'s
case that is in this prayer spread before the God of Jacob. `[2.]` Jacob
is small, very small already, weakened and brought low by former
judgments; and therefore, it these come, he will be quite ruined and
brought to nothing. The people are few; the dust of Jacob, which was
once innumerable, is now soon counted. Those few are feeble (it is the
worm Jacob, Isa. 41:14); they are unable to help themselves or one
another. Sin will soon make a great people small, will diminish the
numerous, impoverish the plenteous, and weaken the courageous. `[3.]` By
whom shall he arise? He has fallen, and cannot help himself up, and he
has no friend to help him, none to raise him, unless the hand of God do
it; what will become of him, then, if the hand that should raise him to
stretched out against him? Note, When the state of God\'s church is very
low and very helpless it is proper to be recommended by our prayers to
God\'s pity.

`3.` God graciously lets fall his controversy, in answer to the
prophet\'s prayer, once and again (v. 3): The Lord repented for this. He
did not change his mind, for he is one mind and who can turn him? But he
changed is way, took another course, and determined to deal in mercy and
not in wrath. He said, It shall not be. And again (v. 6), This also
shall not be. The caterpillars were countermanded, were remanded; a stop
was put to the progress of the fire, and thus a reprieve was granted.
See the power of prayer, of effectual fervent prayer, and how much it
avails, what great things it prevails for. A stop has many a time been
put to a judgment by making supplication to the Judge. This was not the
first time that Israel\'s life was begged, and so saved. See what a
blessing praying people, praying prophets, are to a land, and therefore
how highly they ought to be valued. Ruin would many a time have broken
in if they had not stood in the breach, and made good the pass. See how
ready, how swift, God is to show mercy, how he waits to be gracious.
Amos moves for a reprieve, and obtains it, because God inclines to grant
it and looks about to see if there be any that will intercede for it,
Isa. 59:16. Nor are former reprieves objected against further instances
of mercy, but are rather encouragements to pray and hope for them. This
also shall not be, any more than that. It is the glory of God that he
multiplies to pardon, that he spares, and forgives, to more than seventy
times seven times.

`II.` We have here the rejection of those at last who had been often
reprieved and yet never reclaimed, reduced to straits and yet never
reduced to their God and their duty. This is represented to the prophet
by a vision (v. 7, 8) and an express prediction of utter ruin, v. 9.

`1.` The vision is of a plumb-line, a line with a plummet at the end of
it, such as masons and bricklayers use to run up a wall by, that they
may work it straight and true, and by rule. `(1.)` Israel was a wall, a
strong wall, which God himself had reared, as a bulwark, or wall of
defence, to his sanctuary, which he set up among them. The Jewish church
says of herself (Cant. 8:10), I am a wall, and my breasts are like
towers. This wall was made by a plumb-line, very exact and firm. So
happy was its constitution, so well compacted, and everything so well
ordered according to the model; it had long stood fast as a wall of
brass. But, `(2.)` God now stands upon this wall, not to hold it up, but
to tread it down, or, rather, to consider what he should do with it. He
stands upon it with a plumb-line in his hand, to take measure of it,
that it may appear to be a bowing, bulging wall. Recti est index sui et
oblique-This plumb-line would discover where it was crooked. Thus God
would bring the people of Israel to the trial, would discover their
wickedness, and show wherein they erred; and he would likewise bring his
judgments upon them according to equity, would set a plumb-line in the
midst of them, to mark how far their wall must be pulled down, as David
measured the Moabites with a line (2 Sa. 8:2) to put them to death. And,
when God is coming to the ruin of a people, he is said to lay judgment
to the line and righteousness to the plummet; for when he punishes it is
with exactness. It is now determined: \"I will not again pass by them
any more; they shall not be spared and reprieved as they have been;
their punishment shall not be turned away,\" ch. 1:3. Note, God\'s
patience, which has long been sinned against, will at length be sinned
away; and the time will come when those that have been spared often
shall be no longer spared. My spirit shall not always strive. After
frequent reprieves, yet a day of execution will come.

`2.` The prediction is of utter ruin, v. 9. `(1.)` The body of the people
shall be destroyed, with all those things that were their ornament and
defence. They are here called Isaac as well as Israel, the house of
Isaac (v. 16), some think in allusion to the signification of Isaac\'s
name; it is laughter; they shall become a jest among all their
neighbours; their neighbours shall laugh at them. The desolation shall
fasten upon their high places and their sanctuaries, either their
castles or their temples, both built on high places. Their castles they
thought safe, and their temples sacred as sanctuaries. These shall be
laid waste, to punish them for their idolatry and to make them ashamed
of their carnal confidences, which were the two things for which God had
a controversy with them. When these were made desolate they might read
their sin and folly in their punishment. `(2.)` The royal family shall
sink first, as an earnest of the ruin of the whole kingdom: I will rise
against the house of Jeroboam, Jeroboam the second, who was now king of
the ten tribes; his family was extirpated in his son Zecharias, who was
slain with the sword before the people, by Shallum who conspired against
him, 2 Ki. 15:10. How unrighteous soever the instruments were, God was
righteous, and in them God rose up against that idolatrous family. Even
king\'s houses will be no shelter against the sword of God\'s wrath.

### Verses 10-17

One would have expected, 1. That what we met with in the former part of
the chapter would awaken the people to repentance, when they saw that
they were reprieved in order that they might have space to repent and
that they could not obtain a pardon unless the did repent. 2. That it
would endear the prophet Amos to them, who had not only shown his
good-will to them in praying against the judgments that invaded them,
but had prevailed to turn away those judgments, which, if they had had
any sense of gratitude, would have gained him an interest in their
affections. But it fell out quite contrary; they continue impenitent,
and the next news we hear of Amos is that he is persecuted. Note, As it
is the praise of great saints that they pray for those that are enemies
to them, so it is the shame of many great sinners that they are enemies
to those who pray for them, Ps. 35:13, 15; 109:4. We have here,

`I.` The malicious information brought to the king against the prophet
Amos, v. 10, 11. The informer was Amaziah the priest of Bethel, the
chief of the priests that ministered to the golden calf there, the
president of Bethel (so some read it), that had the principal hand in
civil affairs there. He complained against Amos, not only because he
prophesied without license from him, but because he prophesied against
his altars, which would soon be deserted and demolished if Amos\'s
preaching could but gain credit. Thus the shrine-makers at Ephesus hated
Paul, because his preaching tended to spoil their trade. Note, Great
pretenders to sanctity are commonly the worst enemies to those who are
really sanctified. Priests have been the most bitter persecutors.
Amaziah brings an information to Jeroboam against Amos. Observe, 1. The
crime he is charged with is no less than treason: \"Amos has conspired
against thee, to depose and murder thee; he aims at succeeding thee, and
therefore is taking the most effectual way to weaken thee. He sows the
seeds of sedition in the hearts of the good subjects of the king, and
makes them disaffected to him and his government, that he may draw them
by degrees from their allegiance; upon this account the land is not able
to bear his words.\" It is slyly insinuated to the king that the country
was exasperated against him, and it is given in as their sense that his
preaching was intolerable, and such as nobody could be reconciled to,
such as the times would by no means bear, that is, the men of the times
would not. Both the impudence of his supposed treason, and the bad
influence it would have upon the country, are intimated in that part of
the charge, that he conspired against the king in the midst of the house
of Israel. Note, It is no new thing for the accusers of the brethren to
misrepresent them as enemies to the king and kingdom, as traitors to
their prince and troublers of the land, when really they are the best
friends to both. And it is common for designing men to assert that as
the sense of the country which is far from being so. And yet here, I
doubt, it was too true, that the people could not bear plain dealing any
more than the priests. 2. The words laid in the indictment for the
support of this charge (v. 11): Amos says (and they have witnesses ready
to prove it) Jeroboam shall die by the sword, and Israel shall be led
away captive; and hence they infer that he is an enemy to his king and
country, and not to be tolerated. See the malice of Amaziah; he does not
tell the king how Amos had interceded for Israel, and by his
intercession had turned away first one judgment and then another, and
did not let fall his intercession till he saw the decree had gone forth;
he does not tell him that these threatenings were conditional, and that
he had often assured them that if they would repent and reform the ruin
should be prevented. Nay, it was not true that he said, Jeroboam shall
die by the sword, nor did he so die (2 Ki. 14:28), but that God would
rise against the house of Jeroboam with the sword, v. 9. God\'s prophets
and ministers have often had occasion to make David\'s complaint (Ps.
56:5), Every day they wrest my words. But shall it be made the
watchman\'s crime, when he sees the sword coming, to give warning to the
people, that they may get themselves secured? or the physician\'s crime
to tell his patient of the danger of his disease, that he may use means
for the cure of it? What enemies are foolish men to themselves, to their
own peace, to their best friends! It does not appear that Jeroboam took
any notice of this information; perhaps he reverenced a prophet, and
stood more in awe of the divine authority than Amaziah his priest did.

`II.` The method he used to persuade Amos to withdraw and quit the
country (v. 12, 13); when he could not gain his point with the king to
have Amos imprisoned, banished, or put to death, or at least to have him
frightened into silence or flight, he tried what he could do by fair
means to get rid of him; he insinuated himself into his acquaintance,
and with all the arts of wheedling endeavored to persuade him to go and
prophesy in the land of Judah, and not at Bethel. He owns him to be a
seer, and does not pretend to enjoin him silence, but suggests to him,

`1.` That Bethel was not a proper place for him to exercise his ministry
in, for it was the king\'s chapel, or sanctuary, where he had his idols
and their altars and priests; and it was the king\'s court, or the house
of the kingdom, where the royal family resided and where were set the
thrones of judgment; and therefore prophesy not any more here. And why
not? `(1.)` Because Amos is too plain and blunt a preacher for the court
and the king\'s chapel. Those that wear silk and fine clothing, and
speak silken soft words, are fit for king\'s palaces. `(2.)` Because the
worship that is in the king\'s chapel will be a continual vexation and
trouble to Amos; let him therefore get far enough from it, and what the
eye sees not the heart grieves not for. `(3.)` Because it was not fit that
the king and his house should be affronted in their own court and chapel
by the reproofs and threatenings which Amos was continually teazing them
with in the name of the Lord; as if it were the prerogative of the
prince, and the privilege of the peers, when they are running headlong
upon a precipice, not to be told of their danger. `(4.)` Because he could
not expect any countenance or encouragement there, but, on the contrary,
to be bantered and ridiculed by some and to be threatened and
brow-beaten by others; however, he could not think to make any converts
there, or to persuade any from that idolatry which was supported by the
authority and example of the king. To preach his doctrine there was but
(as we say) to run his head against a post; and therefore prophesy no
more there. But,

`2.` He persuades him that the land of Judah was the fittest place for
him to set up in: Flee thee away thither with all speed, and there eat
bread, and prophesy there. There thou wilt be safe; there thou wilt be
welcome; the king\'s court and chapel there are on thy side; the
prophets there will second thee; the priests and princes there will take
notice of thee, and allow thee an honourable maintenance. See here, `(1.)`
How willing wicked men are to get clear of their faithful reprovers, and
how ready to say to the seers, See not, or See not for us; the two
witnesses were a torment to those that dwelt on the earth (Rev. 11:10),
and it were indeed a pity that men should be tormented before the time,
but that it is in order to the preventing of eternal torment. `(2.)` How
apt worldly men are to measure others by themselves. Amaziah, as a
priest, aimed at nothing but the profits of his place, and he thought
Amos, as a prophet, had the same views, and therefore advised him to
prophesy were he might eat bread, where he might be sure to have as much
as he chose; whereas Amos was to prophesy where God appointed him, and
where there was most need of him, not where he would get most money.
Note, Those that make gain their godliness, and are governed by the
hopes of wealth and preferment themselves, are ready to think these the
most powerful inducements with others also.

`III.` The reply which Amos made to these suggestions of Amaziah\'s. He
did not consult with flesh and blood, nor was it his care to enrich
himself, but to make full proof of his ministry, and to be found
faithful in the discharge of it, not to sleep in a whole skin, but to
keep a good conscience; and therefore he resolved to abide by his post,
and, in answer to Amaziah,

`1.` He justified himself in his constant adherence to his work and to
his place (v. 14, 15); and that which he was sure would not only bear
him out, but bind him to it, was that he had a divine warrant and
commission for it: \"I was no prophet, nor prophet\'s son, neither born
nor bred to the office, not originally designed for a prophet, as Samuel
and Jeremiah, not educated in the schools of the prophets, as many
others were; but I was a herdsman, a keeper of cattle, and a gatherer of
sycamore-fruit.\" Our sycamores bear no fruit, but, it seems, theirs
did, which Amos gathered either for his cattle or for himself and his
family, or to sell. He was a plain country-man, bred up and employed in
country work and used to country fare. He followed the flocks as well as
the herds, and thence God took him, and bade him go and prophesy to his
people Israel, deliver to them such messages as he should from time to
time receive from the Lord. God made him a prophet, and a prophet to
them, appointed him his work and appointed him his post. Therefore he
ought not to be silenced, for, `(1.)` He could produce a divine commission
for what he did. He did not run before he was sent, but pleads, as Paul,
that he was called to be an apostle; and men will find it is at their
peril if they contradict and oppose any that come in God\'s name, if
they say to his seers, See not, or silence those whom he has bidden to
speak; such fight against God. An affront done to an ambassador is an
affront to the prince that sends him. Those that have a warrant from God
ought not to fear the face of man. `(2.)` The mean character he wore
before he received that commission strengthened his warrant, so far was
it from weakening it. `[1.]` He had no thoughts at all of ever being a
prophet, and therefore his prophesying could not be imputed to a raised
expectation or a heated imagination, but purely to a divine impulse.
`[2.]` He was not educated nor instructed in the art or mystery of
prophesying, and therefore he must have his abilities for it immediately
from God, which is an undeniable proof that he had his mission from him.
The apostles, being originally unlearned and ignorant men, evidenced
that they owed their knowledge to their having been with Jesus, Acts
4:13. When the treasure is put into such earthen vessels, it is thereby
made to appear that the excellency of the power is of God, and not of
man, 2 Co. 4:7. `[3.]` He had an honest calling, by which he could
comfortably maintain himself and his family; and therefore did not need
to prophesy for bread, as Amaziah suggested (v. 12), did not take it up
as a trade to live by, but as a trust to honour God and do good with.
`[4.]` He had all his days been accustomed to a plain homely way of
living among poor husbandmen, and never affected either gaieties or
dainties, and therefore would not have thrust himself so near the
king\'s court and chapel if the business God had called him to had not
called him thither. `[5.]` Having been so meanly bred, he could not have
the courage to speak to kings and great men, especially to speak such
bold and provoking things to them, if he had not been animated by a
greater spirit than his own. If God, that sent him, had not strengthened
him, he could not thus have set his face as a flint, Isa. 50:7. Note,
God often chooses the weak and foolish things of the world to confound
the wise and mighty; and a herdman of Tekoa puts to shame a priest of
Bethel, when he receives from God authority and ability to act for him.

`2.` He condemns Amaziah for the opposition he gave them, and denounces
the judgments of God against him, not from any private resentment or
revenge, but in the name of the Lord and by authority from him, v. 16,
17. Amaziah would not suffer Amos to preach at all, and therefore he is
particularly ordered to preach against him: Now therefore hear thou the
word of the Lord, hear it and tremble. Those that cannot bear general
woes may expect woes of their own. The sin he is charged with is
forbidding Amos to prophesy; we do not find that he beat him, or put him
in the stocks, only he enjoined him silence: Prophesy not against
Israel, and drop not thy word against the house of Isaac; he must not
only thunder against them, but he must not so much as drop a word
against them; he cannot bear, no, not the most gentle distilling of that
rain, that small rain. Let him therefore hear his doom.

`(1.)` For the opposition he gave to Amos God will bring ruin upon himself
and his family. This was the sin that filled the measure of his
iniquity. `[1.]` He shall have no comfort in any of his relations, but
be afflicted in those that were nearest to him: His wife shall be a
harlot; either she shall be forcibly abused by the soldiers, as the
Levite\'s concubine by the men of Gibeah (they ravish the women of Zion,
Lam. 5:11), or she shall herself wickedly play the harlot, which, though
her sin, her great sin, would be his affliction, his great affliction
and reproach, and a just punishment upon him for promoting spiritual
whoredom. Sometimes the sins of our relations are to be looked upon as
judgments of God upon us. His children, though they keep honest, yet
shall not keep alive: His sons and his daughters shall fall by the sword
of war, and he himself shall live to see it. He has trained them up in
iniquity, and therefore God will cut them off in it. `[2.]` He shall be
stripped of all his estate; it shall fall into the hand of the enemy,
and be divided by line, by lot, among the soldiers. What is ill begotten
will not be long kept. `[3.]` He shall himself perish in a strange
country, not in the land of Israel, which had been holiness to the Lord,
but in a polluted land, in a heathen country, the fittest place for such
a heathen to end his days in, that hated and silenced God\'s prophets
and contributed so much to the polluting of his own land with idolatry.

`(2.)` Notwithstanding the opposition he gave to Amos, God will bring ruin
upon the land and nation. He was accused for saying, Israel shall be led
away captive (v. 11), but he stands to it, and repeats it; for the
unbelief of man shall not make the word of God of no effect. The burden
of the word of the Lord may be striven with, but it cannot be shaken
off. Let Amaziah rage, and fret, and say what he will to the contrary,
Israel shall surely go into captivity forth of his land. Note, it is to
no purpose to contend with the judgments of God; for when God judges he
will overcome. Stopping the mouths of God\'s ministers will not stop the
progress of God\'s word, for it shall not return void.
