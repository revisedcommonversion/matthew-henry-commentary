Introduction to Amos
====================

Though this prophet appeared a little before Isaiah, yet he was not, as
some have mistaken, that Amos who was the father of Isaiah (Isa. 1:1),
for in the Hebrew their names are very different; their families too
were of a different character, for Isaiah was a courtier, Amos a
country-farmer. Amos signifies a burden, whence the Jews have a
tradition that he was of a slow tongue and spoke with stammering lips;
we may rather, in allusion to his name, say that his speech was weighty
and his word the burden of the Lord. He was (as most think) of Judah,
yet prophesied chiefly against Israel, and at Bethel, 7:13. Some think
his style savours of his extraction, and is more plain and rustic than
that of some other of the prophets; I do not see it so; but it is plain
that his matter agreed with that of his contemporary Hosea, that out of
the mouth of these two witnesses the word might be established. It
appears by his contest with Amaziah the priest of Bethel that he met
with opposition in his work, but was a man of undaunted resolution in
it, faithful and bold in reproving sin and denouncing the judgments of
God for it, and pressing in his exhortations to repentance and
reformation. He begins with threatenings against the neighbouring
nations that were enemies to Israel, ch. 1 and 2. He then calls Israel
to account, and judges them for their idolatry, their unworthy walking
under the favours God had bestowed upon them, and their incorrigibleness
under his judgments, ch. 3 and 4. He calls them to repentance (ch. 5),
rejecting their hypocritical sacrifices unless they did repent. He
foretels the desolations that were coming upon them notwithstanding
their security (ch. 6), some particular judgments (ch. 7), particularly
on Amaziah; and, after other reproofs and threatenings (ch. 8 and 9),
concludes with a promise of the setting up of the Messiah\'s kingdom and
the happiness of God\'s spiritual Israel therein, just as the prophecy
of Joel concluded. These prophets, having opened the wound in their
reproofs and threatenings, which show all wrong, in the promises of
gospel-grace open the remedy, which alone will set all to rights.
