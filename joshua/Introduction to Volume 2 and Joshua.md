> [[Introduction to Volume 2]{.underline}](#introduction-to-volume-2)
>
> [[Introduction to Joshua]{.underline}](#introduction-to-joshua)

Introduction to Volume 2
========================

This second volume of methodized and practical expositions of the
inspired writings ventures abroad with fear and trembling in the same
plain and homely dress with the former on the Pentateuch. Ornari res
ipsa negat; contenta doceri-the subject requires no ornament; to have it
apprehended is all. But I trust, through grace, it proceeds from the
same honest design to promote the knowledge of the scripture, in order
to the reforming of men\'s hearts and lives. If I may but be
instrumental to make my readers wise and good, wiser and better, more
watchful against sin and more careful of their duty both to God and man,
and, in order thereto, more in love with the word and law of God, I have
all I desire, all I aim at. May he that ministereth seed to the sower
multiply the seed sown, by increasing the fruits of our righteousness, 2
Co. 9:10. It is the history of the Jewish church and nation that fills
this volume, from their first settlement in the promised land, after
their 430 years\' bondage in Egypt and their forty years\' wandering in
the wilderness, to their re-settlement there after their seventy years\'
captivity in Babylon-from Joshua to Nehemiah. The five books of Moses
were taken up more with their laws, institutes, and charters; but all
these books are purely historical, and in this way of writing a great
deal of very valuable learning and wisdom has been conveyed from one
generation to another. The chronology of this history, and the
ascertaining of the times when the several events contained in it
happened, would very much illustrate the history, and add to the
brightness of it; it is therefore well worthy the search of the curious
and ingenious, and they may find both pleasure and profit in perusing
the labours of many learned men who have directed their studies that
way. I confess I could willingly have entertained myself and reader, in
this preface, with a calculation of the times through which this history
passes; but I consider that such a babe in knowledge as I am could not
pretend either to add to or correct what has been done by so many great
writers, much less to decide the controversies that have been agitated
among them. I had indeed some thoughts of consulting my worthy and
ever-honoured friend Mr. Tallents of Shrewsbury, the learned author of
the \"View of Universal History,\" and of begging some advice and
assistance from him in methodizing the contents of this history; but, in
the very week in which I put my last hand to this part, it pleased God
to put an end to his useful life (and useful it was to the last) and to
call him to his rest, in the eighty-ninth year of his age: so that
purpose was broken off, that thought of my heart. But that elaborate
performance of his commonly called his \"Chronological Tables\" gives
great light to this, as indeed to all other parts of history. And Dr.
Lightfoot\'s \"Chronology of the Old Testament,\" and Mr. Cradock\'s
\"History of the Old Testament Methodized,\" may also be of great use to
such readers as I write for. As to the particular chronological
difficulties which occur in the thread of this history, I have not been
large upon them, because many times I could not satisfy myself, and how
then could I satisfy my reader concerning them? I have not indeed met
with any difficulties so great but that solutions might be given of them
sufficient to silence the atheists and antiscripturists, and roll away
from the sacred records all the reproach of contradiction and
inconsistency with themselves; for, to do that, it is enough to show
that the difference may be accommodated either this way or that, when at
the same time one cannot satisfy one\'s self which way is the right. But
it is well that these are things about which we may very safely and very
comfortably be ignorant and unresolved. What concerns our salvation is
plain enough, and we need not perplex ourselves about the niceties of
chronology, genealogy, or chorography. At least my undertaking leads me
not into those labyrinths. What is profitable for doctrine, for reproof,
for correction, and for instruction in righteousness, is what I intend
to observe, and I would endeavour to open what is dark and hard to be
understood only in order to that. Every author must be taken in his way
of writing; the sacred penman, as they have not left us formal systems,
so they have not left us formal annals, but useful narratives of things
proper for our direction in the way of duty, which some great judges of
common writers have thought to be the most pleasant and profitable
histories, and most likely to answer the end. The word of God manifestis
pascit, obscuris exercet (Aug. in Joh. Tract. 45), as one of the
ancients expresses it, that is, it has enough in it that is easy to
nourish the meanest to life eternal, yet enough that is difficult to try
the industry and humility of the greatest. There are several things
which should recommend this part of sacred writ to our diligent and
constant search.

`I.` That it is history, and therefore entertaining and very pleasant,
edifying and very serviceable to the conduct of human life. It gratifies
the inquisitive with the knowledge of that which the most intense
speculation could not discover any other way. By a retirement into
ourselves, and a serious contemplation of the objects we are surrounded
with, close reasoning may advance many excellent truths without being
beholden to any other. But for the knowledge of past events we are
entirely indebted (and must be so) to the reports and records of others.
A notion or hypothesis of man\'s own framing may gain him the reputation
of a wit, but a history of man\'s own framing will lay him under the
reproach of a cheat any further than as it respects that which he
himself is an eye or ear-witness of. How much are we indebted then to
the divine wisdom and goodness for these writings, which have made
things so long since past as familiar to us as any of the occurrences of
the age and place we live in! History is so edifying that parables and
apologues have been invented to make up the deficiencies of it for our
instruction concerning good and evil; and, whatever may be said of other
history, we are sure that in this history there is no matter of fact
recorded but what has its use and will help either to expound God\'s
providence or guide man\'s prudence.

`II.` That it is true history, and what we may rely upon the credit of,
and need not fear being deceived in. That which the heathens reckoned
tempus adeµlon (which they knew nothing at all of) and tempus mythikon
(the account of which was wholly fabulous) is to us tempus historikon,
what we have a most authentic account of. The Greeks were with them the
most celebrated historians, and yet their successors in learning and
dominion, the Romans, put them into no good name for their credibility,
witness that of the poet: Et quicquid Graecia mendax audet in
historia-All that lying Greece has dared to record, Juv. Sat. 10. But
the history which we have before us is of undoubted certainty, and no
cunningly devised fable. To be well assured of this is a great
satisfaction, especially since we meet with so many things in it truly
miraculous, and many more great and marvellous.

`III.` That it is ancient history, far more ancient than was ever
pretended to come from any other hand. Homer the most ancient genuine
heathen writer now entirely extant, is reckoned to have lived at the
beginning of the Olympiads, near the time when it is computed that the
city of Rome was founded by Romulus, which was but about the reign of
Hezekiah king of Judah. And his writings pretend not to be historical,
but poetical fiction all over: rhapsodies indeed they are, and the very
Alcoran of paganism. The most ancient authentic historians now extant
are Herodotus and Thucydides, who were contemporaries with the latest of
our historians, Ezra and Nehemiah, and could not write with any
certainty of events much before their own time. The obscurity,
deficiency, and uncertainty of all ancient history, except that which we
find in the scripture, is abundantly made out by the learned bishop
Stillingfleet, in that most useful book, his Origines Sacrae, lib. i.
Let the antiquity of this history not only recommend it to the curious,
but recommend to us all that way of religion it directs us in, as the
good old way, in which if we walk we shall find rest for our souls, Jer.
6:16.

`IV.` That it is church history, the history of the Jewish church, that
sacred society, incorporated for religion, and the custody of the
oracles and ordinances of God, by a charter under the broad seal of
heaven, a covenant confirmed by miracles. Many great ant mighty nations
there were at this time in the world, celebrated it is likely for
wisdom, and learning, and valour, illustrious men and illustrious
actions; yet the records of them are all lost, either in silence or
fables, while that little inconsiderable people of the Jews that dwelt
alone, and was not reckoned among the nations (Num. 23:9), makes so
great a figure in the best known, most ancient, and most lasting of all
histories; and no notice is taken in it of the affairs of other nations,
except only as they fall in with the affairs of the Jews: for the
Lord\'s portion is his people; Jacob is the lot of his inheritance, Deu.
32:8, 9. Such a concern has God for his church in every age, and so dear
have its interests been to him. Let them therefore be so to us, that we
may be followers of him as dear children.

`V.` That it is a divine history, given by inspiration of God, and a part
of that blessed book which is to be the standing rule of our faith and
practice. And we are not to think it a part of it which might have been
spared, or which we may now pass over or cast a careless eye upon, as if
it were indifferent whether we read it or no; but we are to read it as a
sacred record, preserved for our benefit on whom the ends of the world
have come. 1. This history is of great use for the understanding of some
parts of the Old Testament. The account we have here of David\'s life
and reign, and especially of his troubles, is a key to many of his
Psalms; and much light is given to most of the prophecies by these
histories. 2. Though we have not altogether so many types of Christ here
as we had in the history and the law of Moses, yet even here we meet
with many who were figures of him that was to come, such as Joshua,
Samson, Solomon, Cyrus, but especially David, whose kingdom was typical
of the kingdom of the Messiah and the covenant of royalty made with him,
a dark representation of the covenant of redemption made with the
eternal Word; nor know we how to call Christ the son of David unless we
be acquainted with this history nor how to receive the declaration that
John Baptist was the Elias that was to come, Mt. 11:14. 3. The state of
the Jewish church which is here set before us was typical of the gospel
church and the state of that in the days of the Messiah; and as the
prophecies which related to it looked further to the latter days, so did
the histories of it; and still these things happened to them for
ensamples, 1 Co. 10:11. By the tenour of this history we are given to
understand these three things concerning the church (for the thing that
hath been is that which shall be, Eccl. 1:9):-`(1.)` That we are not to
expect the perfect purity and unity of the church in this world, and
therefore not to be stumbled, though we are grieved, at its corruptions,
distempers, and divisions; we are not to think it strange concerning
them, as though some strange thing happened, much less to think the
worse of its laws and constitutions for the sake of them or to despair
of its perpetuity. What wretched stains of idolatry, impiety, and
immorality, appear on the Jewish church, and what a woeful breach was
there between Judah and Ephraim! yet God took them (as I may say) with
all their faults, and never wholly rejected them till they rejected the
Messiah. Israel hath not been forsaken, nor Judah, of their God, though
their land was filled with sin against the Holy One of Israel, Jer.
51:5. `(2.)` That we are not to expect the constant tranquillity and
prosperity of the church. It was then often oppressed and afflicted from
its youth, had its years of servitude as well as its days of triumph,
was often obscured, diminished, impoverished, and brought low; and yet
still God secured to himself a remnant, a holy seed, which was the
substance thereof, Isa. 6:13. Let us not then be surprised to see the
gospel church sometimes under hatches, and driven into the wilderness,
and the gates of hell prevailing far against it. `(3.)` That yet we need
not fear the utter extirpation of it. The gospel church is called the
Israel of God (Gal. 6:16), and the Jerusalem which is above (Gal. 4:26),
the heavenly Jerusalem; for as Israel after the flesh, and the Jerusalem
that then was, by the wonderful care of the divine Providence, outrode
all the storms with which they were tossed and threatened, and continued
in being till they were made to resign all their honours to the gospel
church, which they were the figures of, so shall that also,
notwithstanding all its shocks, be preserved, till the mystery of God
shall be finished, and the kingdom of grace shall have its perfection in
the kingdom of glory. 4. This history is of great use to us for our
direction in the way of our duty; it was written for our learning, that
we may see the evil we should avoid and be armed against it, and the
good we should do and be quickened to it. Though they are generally
judges, and kings, and great men, whose lives are here written, yet in
them even those of the meanest rank may see the deformity of sin and
hate it, and the beauty of holiness and be in love with it; nay, the
greater the person is the more evident are both these; for, if the great
be good, it is their goodness that makes their greatness honourable; if
bad, their greatness does but make their badness the more shameful. The
failings even of good people are also recorded here for our admonition,
that he who thinks he stands may take heed lest he fall, and that he who
has fallen may not despair of forgiveness if he recover himself by
repentance. 5. This history, as it shows what God requires of us, so it
shows what we may expect from his providence, especially concerning
states and kingdoms. By the dealings of God with the Jewish nation it
appears that, as nations are, so they must expect to fare-that while
princes and people serve the interests of God\'s kingdom among men he
will secure and advance their interests, but that when they shake off
his government, and rebel against him, they can look for no other than
an inundation of judgments. It was so all along with Israel; while they
kept close to God they prospered; when they forsook him every thing went
cross. That great man archbishop Tillotson (Vol. 1. Serm. 3. on Prov.
14:34) suggests that though, as to particular persons, the providences
of God are promiscuously administered in this world, because there is
another world of rewards and punishments for them, yet it is not so with
nations as such, but national virtues are ordinarily rewarded with
temporal blessings and national sins punished with temporal judgments,
because, as he says, public bodies and communities of men, as such, can
be rewarded and punished only in this world, for in the next they will
all be dissolved. So plainly are God\'s ways of disposing kingdoms laid
before us in the glass of this history that I could wish Christian
statesmen would think themselves as much concerned as preachers to
acquaint themselves with it; they might fetch as good maxims of state
and rules of policy from this as from the best of the Greek and Roman
historians. We are blessed (as the Jews were) with a divine revelation,
and make a national profession of religion and relation to God, and
therefore are to look upon ourselves as in a peculiar manner under a
divine regimen, so that the things which happened to them were designed
for ensamples to us.

I cannot pretend to write for great ones. But if what is here done may
be delightful to any in reading and helpful in understanding and
improving this sacred history, and governing themselves by the dictates
of it, let God have all the glory and let all the rivers return to the
ocean whence they came. When I look back on what is done I see nothing
to boast of, but a great deal to be ashamed of; and, when I look forward
on what is to be done, I see nothing in myself to trust to for the doing
of it. I have no sufficiency of my own; but by the grace of God I am
what I am, and that grace will, I trust, be sufficient for me. Surely in
the Lord have I righteousness and strength. That blessed epichoreµgia
which the apostle speaks of (Phil. 1:19), that continual supply or
communication of the Spirit of Jesus Christ, is what we may in faith
pray for, and depend upon, to furnish us for every good word and work.
The pleasantness of the study has drawn me on to the writing of this,
and the candour with which my friends have been pleased to receive my
poor endeavours on the Pentateuch encourages me to publish it; it is
done according to the best of my skill, not without some care and
application of mind, in the same method and manner with that; I wish I
could have done it in less compass, that it might have been more within
reach of the poor of the flock. But then it would not have been so plain
and full as I desire it may be for the benefit of the lambs of the
flock. Brevis esse laboro, obscurus fio-labouring to be concise I become
obscure. With a humble submission to the divine providence and its
disposals, and a humble reliance on the divine grace and its guidance
and operation, I purpose still to proceed, as I have time, in this work.
Two volumes more will, if God permit, conclude the Old Testament; and
then if my friends encourage me, and my God spare me and enable me for
it, I intend to go on to the New Testament. For though many have taken
in hand to set forth in order a declaration of those parts of scripture
which are yet before us (Lu. 1:1), whose works praise them in the gates
and are likely to outlive mine, yet while the subject is really so
copious as it is and the manner of handling it may possibly be so
various, and while one book comes into the hands of some and another
into the hands of others, and all concur in the same design to advance
the common interests of Christ\'s kingdom, the common faith once
delivered to the saints, and the common salvation of precious souls
(Tit. 1:4, Jude 3), I hope store of this kind will be thought no sore. I
make bold to mention my purpose to proceed thus publicly in hopes I may
have the advice of my friends in it, and their prayers for me that I may
be made more ready and mighty in the scriptures, that understanding and
utterance may be given to me, and that I may obtain mercy of the Lord
Jesus to be found his faithful servant, who am less than the least of
all that call him Master.

Chester, June 2, 1708

M.H.

Introduction to Joshua
======================

`I.` We have now before us the history of the Jewish nation in this book
and those that follow it to the end of the book of Esther. These books,
to the end of the books of the Kings, the Jewish writers call the first
book of the prophets, to bring them within the distribution of the books
of the Old Testament, into the Law, the Prophets, and the Chetubim, or
Hagiographa, Lu. 24:44. The rest they make part of the Hagiographa. For,
though history is their subject, it is justly supposed that prophets
were their penmen. To those books that are purely and properly
prophetical the name of the prophet is prefixed, because the credibility
of the prophecies depended much upon the character of the prophets; but
these historical books, it is probable, were collections of the
authentic records of the nation, which some of the prophets (and the
Jewish church was for many ages more or less continually blessed with
such) were divinely directed and helped to put together for the service
of the church to the end of the world; as their other officers, so their
historiographers, had their authority from heaven.-It should seem that
though the substance of the several histories was written when the
events were fresh in memory, and written under a divine direction, yet,
under the same direction, they were put into the form in which we now
have them by some other hand, long afterwards, probably all by the same
hand, or about the same time. The grounds of the conjecture are, 1.
Because former writings are so often referred to, as the Book of Jasher
(Jos. 10:13, and 2 Sa. 1:18), the Chronicles of the Kings of Israel and
Judah, and the books of Gad, Nathan, and Iddo. 2. Because the days when
the things were done are spoken of sometimes as days long since passed;
as 1 Sa. 9:9, He that is now called a prophet was formerly called a
seer. And, 3. Because we so often read of things remaining unto this
day; as stones (Jos. 4:9; 7:26; 8:29; 10:27; 1 Sa. 6:18), names of
places (Jos. 5:9; 7:26; Jdg. 1:26; 15:19; 18:12; 2 Ki. 14:7), rights and
possessions (Jdg. 1:21; 1 Sa. 27:6), customs and usages (1 Sa. 5:5; 2
Ki. 17:41), which clauses have been since added to the history by the
inspired collectors for the confirmation and illustration of it to those
of their own age. And, if one may offer a mere conjecture, it is not
unlikely that the historical books, to the end of the Kings, were put
together by Jeremiah the prophet, a little before the captivity; for it
is said of Ziklag (1 Sa. 27:6) that it pertains to the kings of Judah
(which style began after Solomon and ended in the captivity) unto this
day. And it is still more probable that those which follow were put
together by Ezra the scribe, some time after the captivity. However,
though we are in the dark concerning their authors, we are in no doubt
concerning their authority; they were a part of the oracles of God,
which were committed to the Jews, and were so received and referred to
by our Saviour and the apostles.

In the five books of Moses we had a very full account of the rise,
advance, and constitution, of the Old-Testament church, the family out
of which it was raised, the promise, that great charter by which it was
incorporated, the miracles by which it was built up, and the laws and
ordinances by which it was to be governed, from which one would conceive
and expectation of its character and state very different from what we
find in this history. A nation that had statutes and judgments so
righteous, one would think, should have been very holy; and a nation
what had promises so rich should have been very happy. But, alas! a
great part of the history is a melancholy representation of their sins
and miseries; for the law made nothing perfect, but this was to be done
by the bringing in of the better hope. And yet, if we compare the
history of the Christian church with its constitution, we shall find the
same cause for wonder, so many have been its errors and corruptions; for
neither does the gospel make any thing perfect in this world, but leaves
us still in expectation of a better hope in the future state.

`II.` We have next before us the book of Joshua, so called, perhaps, not
because it was written by him, for that is uncertain. Dr. Lightfoot
thinks that Phinehas wrote it. Bishop Patrick is clear that Joshua wrote
it himself. However that be, it is written concerning him, and, if any
other wrote it, it was collected out of his journals or memoirs. It
contains the history of Israel under the command and government of
Joshua, how he presided as general of their armies, 1. In their entrance
into Canaan, ch. 1-5. 2. In their conquest of Canaan, ch. 6-12. 3. In
the distribution of the land of Canaan among the tribes of Israel, ch.
22-24. In all which he was a great example of wisdom, courage, fidelity,
and piety, to all that are in places of public trust. But this is not
all the use that is to be made of this history. We may see in it, 1.
Much of God and his providence-his power in the kingdom of nature, his
justice in punishing the Canaanites when the measure of their iniquity
was full, his faithfulness to his covenant with the patriarchs, and his
kindness to his people Israel, notwithstanding their provocations. We
may see him as the Lord of Hosts determining the issues of war, and as
the director of the lot, determining the bounds of men\'s habitations.
2. Much of Christ and his grace. Though Joshua is not expressly
mentioned in the New Testament as a type of Christ, yet all agree that
he was a very eminent one. He bore our Saviour\'s name, as did also
another type of him, Joshua the high priest, Zec. 6:11, 12. The
Septuagint, giving the name of Joshua a Greek termination, call him all
along Ieµsous, Jesus, and so he is called Acts 7:45, and Heb. 4:8.
Justin Martyr, one of the first writers of the Christian church (Dialog.
cum Tryph. p. mihi 300), makes that promise in Ex. 23:20, My angel shall
bring thee into the place I have prepared, to point at Joshua; and these
words, My name is in him, to refer to this, that his names should be the
same with that of the Messiah. It signifies, He shall save. Joshua saves
God\'s people from the Canaanites; our Lord Jesus saves them from their
sins. Christ, as Joshua, is the captain of our salvation, a leader and
commander of the people, to tread Satan under their feet, to put them in
possession of the heavenly Canaan, and to give them rest, which (it is
said, Heb. 4:8) Joshua did not.
