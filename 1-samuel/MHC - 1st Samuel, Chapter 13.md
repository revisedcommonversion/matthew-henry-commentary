1st Samuel, Chapter 13
======================

Commentary
----------

Those that desired a king like all the nations fancied that, when they
had one, they should look very great and considerable; but in this
chapter we find it proved much otherwise. While Samuel was joined in
commission with Saul things went well (11:7). But, now that Saul began
to reign alone, all went to decay, and Samuel\'s words began to be
fulfilled: \"You shall be consumed, both you and your king;\" for never
was the state of Israel further gone in a consumption than in this
chapter. `I.` Saul appears here a very silly prince. 1. Infatuated in his
counsels (v. 1-3). 2. Invaded by his neighbours (v. 4, 5). 3. Deserted
by his soldiers (v. 6, 7). 4. Disordered in his own spirit, and
sacrificing in confusion (v. 8-10). 5. Chidden by Samuel (v. 11-13). 6.
Rejected of God from being king (v. 14). `II.` The people appear hear a
very miserable people. 1. Disheartened and dispersed (v. 6, 7). 2.
Diminished (v. 15, 16). 3. Plundered (v. 17, 18). 4. Disarmed (v.
19-23). This they got by casting off God\'s government, and making
themselves like the nations: all their glory departed from them.

### Verses 1-7

We are not told wherein it was that the people of Israel offended God,
so as to forfeit his presence and turn his hand against them, as Samuel
had threatened (ch. 12:15); but doubtless they left God, else he would
not have left them, as here it appears he did; for,

`I.` Saul was very weak and impolitic, and did not order his affairs with
discretion. Saul was the son of one year (so the first words are in the
original), a phrase which we make to signify the date of his reign, but
ordinarily it signifies the date of one\'s birth, and therefore some
understand it figuratively-he was as innocent and good as a child of a
year old; so the Chaldee paraphrase: he was without fault, like the son
of a year. But, if we admit a figurative sense, it may as well intimate
that he was ignorant and imprudent, and as unfit for business as a child
of a year old: and the subsequent particulars make this more accordant
with his character than the former. But we take it rather, as our own
translation has it, Saul reigned one year, and nothing happened that was
considerable, it was a year of no action; but in his second year he did
as follows:-1. he chose a band of 3000 men, of whom he himself commanded
2000, and his son Jonathan 1000, v. 2. The rest of the people he
dismissed to their tents. If he intended these only for the guard of his
person and his honorary attendants, it was impolitic to have so many, if
for a standing army, in apprehension of danger from the Philistines, it
was no less impolitic to have so few; and perhaps the confidence he put
in this select number, and his disbanding the rest of that brave army
with which he had lately beaten the Ammonites (ch. 11:8-11), was looked
upon as an affront to the kingdom, excited general disgust, and was the
reason he had so few at his call when he had occasion for them. The
prince that relies on a particular party weakens his own interest in the
whole community. 2. He ordered his son Jonathan to surprise and destroy
the garrison of the Philistines that lay near him in Geba, v. 3. I wish
there were no ground for supposing that this was a violation or
infraction of some articles with the Philistines, and that it was done
treacherously and perfidiously. The reason why I suspect it is because
it is said that, for doing it, Israel was had in abomination, or, as the
word is, did stink with the Philistines (v. 4), as men void of common
honesty and whose word could not be relied on. If it was so, we will lay
the blame, not on Jonathan who did it, but on Saul, his prince and
father, who ordered him to do it, and perhaps kept him in ignorance of
the truth of the matter. Nothing makes the name of Israel odious to
those that are without so much as the fraud and dishonesty of those that
are called by that worthy name. If professors of religion cheat and
over-reach, break their word and betray their trust, religion suffers by
it, and is had in abomination with the Philistines. Whom may one trust
if not an Israelite, one that, it is expected, should be without guile?
3. When he had thus exasperated the Philistines, then he began to raise
forces, which, if he had acted wisely, he would have done before. When
the Philistines had a vast army ready to pour in upon him, to avenge the
wrong he had done them, then was he blowing the trumpet through the
land, among a careless, if not a disaffected people, saying, Let the
Hebrews hear (v. 3), and so as many as thought fit came to Saul to
Gilgal, v. 4. But now the generality, we may suppose, drew back (either
in dislike of Saul\'s politics or in dread of the Philistines\' power),
who, if he had summoned them sooner, would have been as ready at his
beck as they were when he marched against the Ammonites. We often find
that after-wit would have done much better before and have prevented
much inconvenience.

`II.` Never did the Philistines appear in such a formidable body as they
did now, upon this provocation which Saul gave them. We may suppose they
had great assistance from their allies, for (v. 5), besides 6000 horse,
which in those times, when horses were not so much used in war as they
are now, was a great body, they had an incredible number of chariots,
30,000 in all: most of them, we may suppose, were carriages for the bag
and baggage of so vast an army, not chariots of war. But their foot was
innumerable as the sand of the sea-shore, so jealous were they for the
honour of their nation and so much enraged at the baseness of the
Israelites in destroying their garrison. If Saul had asked counsel of
God before he had given the Philistines this provocation, he and his
people might the better have borne this threatening trouble which they
had now brought on themselves by their own folly.

`III.` Never were the people of Israel so faint-hearted, so sneaking, so
very cowardly, as they were now. Some considerable numbers, it may be,
came to Saul to Gilgal; but, hearing of the Philistines\' numbers and
preparations, their spirits sunk within them, some think because they
did not find Samuel there with Saul. Those that, awhile ago, were weary
of him, and wished for a king, now had small joy of their king unless
they could see him under Samuel\'s direction. Sooner or later, men will
be made to see that God and his prophets are their best friends. Now
that they saw the Philistines making war upon them, and Samuel not
coming in to help them, they knew not what to do; men\'s hearts failed
them for fear. And. 1. Some absconded. Rather than run upon death among
the Philistines, they buried themselves alive in caves and thickets, v.
6. See what work sin makes; it exposes men to perils, and then robs them
of their courage and dispirits them. A single person, by faith, can say,
I will not be afraid of 10,000 (Ps. 3:6); but here thousands of
degenerate Israelites tremble at the approach of a great crowd of
Philistines. Guilt makes men cowards. 2. Others fled (v. 7): They went
over Jordan to the land of Gilead, as far as they could from the danger,
and to a place where they had lately been victorious over the Ammonites.
Where they had triumphed they hoped to be sheltered. 3. Those that staid
with Saul followed him trembling, expecting no other than to be cut off,
and having their hands and hearts very much weakened by the desertion of
so many of their troops. And perhaps Saul himself, though he had so much
honour as to stand his ground, yet had no courage to spare wherewith to
inspire his trembling soldiers.

### Verses 8-14

Here is, `I.` Saul\'s offence in offering sacrifice before Samuel came.
Samuel, when he anointed him, had ordered him to tarry for him seven
days in Gilgal, promising that, at the end of those days, he would be
sure to come to him, and both offer sacrifices for him and direct him
what he should do. This we had ch. 10:8. Perhaps that order, though
inserted there, was given him afterwards, or was given him as a general
rule to be observed in every public congress at Gilgal, or, as is most
probable, though not mentioned again, was lately repeated with reference
to this particular occasion; for it is plain that Saul himself
understood it as obliging him from God now to stay till Samuel came,
else he would not have made so many excuses as he did for not staying,
v. 11. This order Saul broke. He staid till the seventh day, yet had not
patience to wait till the end of the seventh day. Perhaps he began to
reproach Samuel as false to his word, careless of his country, and
disrespectful of his prince, and thought it more fit that Samuel should
wait for him than he for Samuel. However, 1. He presumed to offer
sacrifice without Samuel, and nothing appears to the contrary but that
he did it himself, though he was neither priest nor prophet, as if,
because he was a king, he might do any thing, a piece of presumption
which king Uzziah paid dearly for, 2 Chr. 26:16, etc. 2. He determined
to engage the Philistines without Samuel\'s directions, though he had
promised to show him what he should do. So self-sufficient Saul was that
he thought it not worth while to stay for a prophet of the Lord, either
to pray for him or to advise him. This was Saul\'s offence, and that
which aggravated it was, `(1.)` That for aught that appears, he did not
send any messenger to Samuel, to know his mind, to represent the case to
him, and to receive fresh directions from him, though he had enough
about him that were swift enough of foot at this time. `(2.)` That when
Samuel came he rather seemed to boast of what he had done than to repent
of it; for he went forth to salute him, as his brother-sacrificer, and
seemed pleased with the opportunity he had of letting Samuel know that
he needed him not, but could do well enough without him. He went out to
bless him, so the word is, as if he now thought himself a complete
priest, empowered to bless as well as sacrifice, whereas he should have
gone out to be blessed by him. `(3.)` That he charged Samuel with breach
of promise: Thou camest not within the days appointed (v. 11), and
therefore if any thing was amiss Samuel must bear the blame, who was
God\'s minister; whereas he did come according to his word, before the
seven days had expired. Thus the scoffers of the latter days think the
promise of Christ\'s coming is broken, because he does not come in their
time, though it is certain he will come at the set time. `(4.)` That when
he was charged with disobedience he justified himself in what he had
done, and gave no sign at all of repentance for it. It is not sinning
that ruins men, but sinning and not repenting, falling and not getting
up again. See what excuses he made, v. 11, 12. He would have this act of
disobedience pass, `[1.]` For an instance of his prudence. The people
were most of them scattered from him, and he had no other way than this
to keep those with him that remained and to prevent their deserting too.
If Samuel neglected the public concerns, he would not. `[2.]` For an
instance of his piety. He would be thought very devout, and in great
care not to engage the Philistines till he had by prayer and sacrifice
engaged God on his side: \"The Philistines,\" said he, \"will come down
upon me, before I have made my supplication to the Lord, and then I am
undone. What! go to war before I have said my prayers!\" Thus he covered
his disobedience to God\'s command with a pretence of concern for God\'s
favour. Hypocrites lay a great stress upon the external performances of
religion, thinking thereby to excuse their neglect of the weightier
matters of the law. And yet, lastly, He owns it went against his
conscience to do it: I forced myself and offered a burnt-offering,
perhaps boasting that he had broken through his convictions and got the
better of them, or at least thinking this extenuated his fault, that he
knew he should not have done as he did, but did it with reluctancy.
Foolish man! to think that God would be well pleased with sacrifices
offered in direct opposition both to his general and particular command.

`II.` The sentence passed upon Saul for this offence. Samuel found him
standing by his burnt-offering, but, instead of an answer of peace, was
sent to him with heavy tidings, and let him know that the sacrifice of
the wicked is abomination to the Lord, much more when he brings it, as
Saul did, with a wicked mind. 1. He shows him the aggravations of his
crime, and says to this king, Thou art wicked, which it is not for any
but a prophet of the Lord to say, Job 34:18. He charges him with being
an enemy to himself and his interest-Thou hast done foolishly, and a
rebel to God and his government-\"Thou hast not kept the commandment of
the Lord thy God, that commandment wherewith he intended to try thy
obedience.\" Note, Those that disobey the commandments of God do
foolishly for themselves. Sin is folly, and sinners are the greatest
fools. 2. He reads his doom (v. 14): \"Thy kingdom shall not continue
long to thee or thy family; God has his eye upon another, a man after
his own heart, and not like thee, that will have thy own will and way.\"
The sentence is in effect the same with Mene tekel, only now there seems
room left for Saul\'s repentance, upon which this sentence would have
been reversed; but, upon the next act of disobedience, it was made
irreversible, ch. 15:29. And now, better a thousand times he had
continued in obscurity tending his asses than to be enthroned and so
soon dethroned. But was not this hard, to pass so severe a sentence upon
him and his house for a single error, an error that seemed so small, and
in excuse for which he had so much to say? No, The Lord is righteous in
all his ways and does no man any wrong, will be justified when he speaks
and clear when he judges. By this, `(1.)` He shows that there is no sin
little, because no little god to sin against; but that every sin is a
forfeiture of the heavenly kingdom, for which we stood fair. `(2.)` He
shows that disobedience to an express command, though in a small matter,
is a great provocation, as in the case of our first parents. `(3.)` He
warns us to take heed of our spirits, for that which to men may seem but
a small offence, yet to him that knows from what principle and with what
disposition of mind it is done, may appear a heinous crime. `(4.)` God, in
rejecting Saul for an error seemingly little, sets off, as by a foil,
the lustre of his mercy in forgiving such great sins as those of David,
Manasseh, and others. `(5.)` We are taught hereby how necessary it is that
we wait on our God continually. Saul lost his kingdom for want of two or
three hours\' patience.

### Verses 15-23

Here, 1. Samuel departs in displeasure. Saul has set up for himself, and
now he is left to himself: Samuel gat him from Gilgal (v. 15), and it
does not appear that he either prayed with Saul or directed him. Yet in
going up to Gibeah of Benjamin, which was Saul\'s city, he intimated
that he had not quite abandoned him, but waited to do him a kindness
another time. Or he went to the college of the prophets there, to pray
for Saul when he did not think fit to pray with him. 2. Saul goes after
him to Gibeah, and there musters his army, and finds his whole number to
be but 600 men, v. 15, 16. Thus were they for their sin diminished and
brought low. 3. The Philistines ravage the country, and put all the
adjacent parts under contribution. The body of their army, or standing
camp (as it is called in the margin, v. 23), lay in an advantageous pass
at Michmash, but thence they sent out three separate parties or
detachments that took several ways, to plunder the country, and bring in
provisions for the army, v. 17, 18. By these the land of Israel was both
terrified and impoverished, and the Philistines were animated and
enriched. This the sin of Israel brought upon them, Isa. 42:24. 4. The
Israelites that take the field with Saul are unarmed, having only slings
and clubs, not a sword or spear among them all, except what Saul and
Jonathan themselves have, v. 19, 22. See here, `(1.)` How politic the
Philistines were, when they had power in their hands, and did what they
pleased in Israel. They put down all the smiths\' shops, transplanted
the smiths into their own country, and forbade any Israelite, under
severe penalties, to exercise the trade or mystery of working in brass
or iron, though they had rich mines of both (Deu. 8:9) in such plenty
that it was said of Asher, his shoes shall be iron and brass, Deu.
33:25. This was subtilely done of the Philistines, for hereby they not
only prevented the people of Israel from making themselves weapons of
war (by which they would be both disused to military exercises and
unfurnished when there was occasion), but obliged them to a dependence
upon them even for the instruments of husbandry; they must go to them,
that is, to some or other of their garrisons, which were dispersed in
the country, to have all their iron-work done, and no more might an
Israelite do than use a file (v. 20, 21), and no doubt the Philistines\'
smiths brought the Israelites long bills for work done. `(2.)` How
impolitic Saul was, that did not, in the beginning of his reign, set
himself to redress this grievance. Samuel\'s not doing it was very
excusable; he fought with other artillery; thunder and lightning, in
answer to his prayer, were to him instead of sword and spear; but for
Saul, that pretended to be a king like the kings of the nations, to
leave his soldiers without swords and spears, and take no care to
provide them, especially when he might have done it out of the spoils of
the Ammonites whom he conquered in the beginning of his reign, was such
a piece of negligence as could by no means be excused. `(3.)` How slothful
and mean-spirited the Israelites were, that suffered the Philistines
thus to impose upon them and had no thought nor spirit to help
themselves. It was reckoned very bad with them when there was not a
shield or spear found among 40,000 in Israel (Jdg. 5:8), and it was not
better now, when there was never an Israelite with a sword by his side
but the king and his son, never a soldier, never a gentleman; surely
they were reduced to this, or began to be so, in Samuel\'s time, for we
never find him with sword or spear in his hand. If they had not been
dispirited, they could not have been disarmed, but it was sin that made
them naked to their shame.
