1st Samuel, Chapter 26
======================

Commentary
----------

David\'s troubles from Saul here begin again; and the clouds return
after the rain, when one would have hoped the storm had blown over, and
the sky had cleared upon that side; but after Saul had owned his fault
in persecuting David, and acknowledged David\'s title to the crown, yet
here he revives the persecution, so perfectly lost was he to all sense
of honour and virtue. `I.` The Ziphites informed him where David was (v.
1), and thereupon he marched out with a considerable force in quest of
him (v. 2, 3). `II.` David gained intelligence of his motions (v. 4), and
took a view of his camp (v. 5). `III.` He and one of his men ventured into
his camp in the night and found him and all his guards fast asleep (v.
6, 7). `IV.` David, though much urged to it by his companions, would not
take away Saul\'s life, but only carried off his spear and his cruse of
water (v. 8-12). `V.` He produced these as a further witness for him that
he did not design any ill to Saul, and reasoned with him upon his
conduct (v. 13-20). `VI.` Saul was hereby convinced of his error, and once
more desisted from persecuting David (v. 21-25). The story is much like
that which we had (ch. 24). In both David is delivered out of Saul\'s
hand, and Saul out of David\'s.

### Verses 1-5

Here, 1. Saul gets information of David\'s movements and acts
offensively. The Ziphites came to him and told him where David now was,
in the same place where he was when they formerly betrayed him, ch.
23:19. Perhaps (though it is not mentioned) Saul had given them
intimation, under-hand, that he continued his design against David, and
would be glad of their assistance. If not, they were very officious to
Saul, aware of what would please him, and very malicious against David,
to whom they despaired of ever reconciling themselves, and therefore
they stirred up Saul (who needed no such spur) against him, v. 1. For
aught we know, Saul would have continued in the same good mind that he
was in (ch. 24:17), and would not have given David this fresh trouble,
if the Ziphites had not put him on. See what need we have to pray to God
that, since we have so much of the tinner of corruption in our own
hearts, the sparks of temptation may be kept far from us, lest, if they
come together, we be set on fire of hell. Saul readily caught at the
information, and went down with an army of 3000 men to the place where
David hid himself, v. 2. How soon do unsanctified hearts lose the good
impressions which their convictions have made upon them and return with
the dog to their vomit!

`2.` David gets information of Saul\'s movements and acts defensively. He
did not march out to meet and fight him; he sought only his own safety,
not Saul\'s ruin; therefore he abode in the wilderness (v. 3), putting
thereby a great force upon himself, and curbing the bravery of his own
spirit by a silent retirement, showing more true valour than he could
have done by an irregular resistance. `(1.)` He had spies who informed him
of Saul\'s descent, that he had come in very deed (v. 4.); for he would
not believe that Saul would deal so basely with him till he had the
utmost evidence of it. `(2.)` He observed with his own eyes how Saul was
encamped, v. 5. He came towards the place where Saul and his men had
pitched their tents, so near as to be able, undiscovered, to take a view
of their entrenchments, probably in the dusk of the evening.

### Verses 6-12

Here is, `I.` David\'s bold adventure into Saul\'s camp in the night,
accompanied only by his kinsman Abishai, the son of Zeruiah. He proposed
it to him and to another of his confidants (v. 6), but the other either
declined it as too dangerous an enterprise, or at least was content that
Abishai, who was forward to it, should run the risk of it rather than
himself. Whether David was prompted to do this by his own courage, or by
an extraordinary impression upon his spirits, or by the oracle, does not
appear; but, like Gideon, he ventured through the guards, with a special
assurance of the divine protection.

`II.` The posture he found the camp in Saul lay sleeping in the trench,
or, as some read it, in his chariot, and in the midst of his carriages,
with his spear stuck in the ground by him, to be ready if his quarters
should by beaten up (v. 7); and all the soldiers, even those that were
appointed to stand sentinel, were fast asleep, v. 12. Thus were their
eyes closed and their hands bound, for a deep sleep from the Lord had
fallen upon them; something extraordinary there was in it that they
should all be asleep together, and so fast asleep that David and Abishai
walked and talked among them, and yet none of them stirred. Sleep, when
God gives it to his beloved, is their rest and refreshment; but he can,
when he pleases, make it to his enemies their imprisonment. Thus are the
stout-hearted spoiled; they have slept their sleep, and none of the men
of might have found their hands, at thy rebuke, O God of Jacob! Ps.
76:5, 6. It was a deep sleep from the Lord, who has the command of the
powers of nature, and makes them to serve his purposes as he pleases.
Whom God will disable, or destroy, he binds up with a spirit of slumber,
Rom. 11:8. How helpless do Saul and all his forces lie, all, in effect,
disarmed and chained! and yet nothing is done to them; they are only
rocked asleep. How easily can God weaken the strongest, befool the
wisest, and baffle the most watchful! Let all his friends therefore
trust him and all his enemies fear him.

`III.` Abishai\'s request to David for a commission to dispatch Saul with
the spear that stuck at his bolster, which (now that he lay so fair) he
undertook to do at one blow, v. 8. He would not urge David to kill him
himself, because he had declined doing this before when he had a similar
opportunity; but he begged earnestly that David would give him leave to
do it, pleading that he was his enemy, not only cruel and implacable,
but false and perfidious, whom no reason would rule nor kindness work
upon, and that God had now delivered him into his hand, and did in
effect bid him strike. The last advantage he had of this kind was indeed
but accidental, when Saul happened to be in the cave with him at the
same time. But in this there was something extraordinary; the deep sleep
that had fallen on Saul and all his guards was manifestly from the Lord,
so that it was a special providence which gave him this opportunity; he
ought not therefore to let it slip.

`IV.` David\'s generous refusal to suffer any harm to be done to Saul,
and in it a resolute adherence to his principles of loyalty, v. 9. David
charged Abishai not to destroy him, would not only not do it himself,
but not permit another to do it. And he gave two reasons for it:-1. It
would be a sinful affront to God\'s ordinance. Saul was the Lord\'s
anointed, king of Israel by the special appointment and nomination of
the God of Israel, the power that was, and to resist him was to resist
the ordinance of God, Rom. 13:2. No man could do it and be guiltless.
The thing he feared was guilt and his concern respected his innocence
more than his safety. 2. It would be a sinful anticipation of God\'s
providence. God had sufficiently shown him, in Nabal\'s case, that, if
he left it to him to avenge him, he would do it in due time. Encouraged
therefore by his experience in that instance, he resolves to wait till
God shall think fit to avenge him on Saul, and he will by no means
avenge himself (v. 10): \"The Lord shall smite him, as he did Nabal,
with some sudden stroke, or he shall die in battle (as it proved he did
soon after), or, if not, his day shall come to die a natural death, and
I will contentedly wait till then, rather than force my way to the
promised crown by any indirect methods.\" The temptation indeed was very
strong; but, if he should yield, he would sin against God, and therefore
he will resist the temptation with the utmost resolution (v. 11): \"The
Lord forbid that I should stretch forth my hand against the Lord\'s
anointed; no, I will never do it, nor suffer it to be done.\" Thus
bravely does he prefer his conscience to his interest and trusts God
with the issue.

`V.` The improvement he made of this opportunity for the further evidence
of his own integrity. He and Abishai carried away the spear and cruse of
water which Saul had by his bed-side (v. 12), and, which was very
strange, none of all the guards were aware of it. If a physician had
given them the strongest opiate or stupifying dose, they could not have
been faster locked up with sleep. Saul\'s spear which he had by him for
defence, and his cup of water which he had for his refreshment, were
both stolen from him while he slept. Thus do we lose our strength and
our comfort when we are careless, and secure, and off our watch.

### Verses 13-20

David having got safely from Saul\'s camp himself, and having brought
with him proofs sufficient that he had been there, posts himself
conveniently, so that they might hear him and yet not reach him (v. 13),
and then begins to reason with them upon what had passed.

`I.` He reasons ironically with Abner, and keenly banters him. David knew
well that it was from the mighty power of God that Abner and the rest of
the guards were cast into so deep a sleep, and that God\'s immediate
hand was in it; but he reproaches Abner as unworthy to be captain of the
lifeguards, since he could sleep when the king his master lay so much
exposed. By this it appears that the hand of God locked them up in this
deep sleep that, as soon as ever David had got out of danger, a very
little thing awakened them, even David\'s voice at a great distance
roused them, v. 14. Abner got up (we may suppose it early in a summer\'s
morning) and enquired who called, and disturbed the king\'s repose. \"It
is I,\" says David, and then he upbraids him with his sleeping when he
should have been upon his guard. Perhaps Abner, looking upon David as a
despicable enemy and one that there was no danger from, had neglected to
set a watch; however, he himself ought to have been more wakeful. David,
to put him into confusion, told him, 1. That he had lost his honour (v.
15): \"Art not thou a man? (so the word is), a man in office, that art
bound, by the duty of thy place, to inspect the soldiery? Art not thou
in reputation for a valiant man? So thou wouldst be esteemed, a man of
such courage and conduct that there is none like thee; but now thou art
shamed for ever. Thou a general! Thou, a sluggard!\" 2. That he deserved
to lose his head (v. 16): \"You are all worthy to die, by martial law,
for being off your guard, when you had the king himself asleep in the
midst of you. Ecce signum-Behold this token. See where the king\'s spear
is, in the hand of him whom the king himself is pleased to count his
enemy. Those that took away this might as easily and safely have taken
away his life. Now see who are the king\'s best friends, you that
neglected him and left him exposed or I that protected him when he was
exposed. You pursue me as worthy to die, and irritate Saul against me;
but who is worthy to die now?\" Note, Sometimes those that unjustly
condemn others are justly left to fall into condemnation themselves.

`II.` He reasons seriously and affectionately with Saul. By this time he
was so well awake as to hear what was said, and to discern who said it
(v. 17): Is this thy voice, my son David? In the same manner he had
expressed his relentings, ch. 24:16. He had given his wife to another
and yet calls him son, thirsted after his blood and yet is glad to hear
his voice. Those are bad indeed that have never any convictions of good,
nor ever sincerely utter good expressions. And now David has as fair an
opportunity of reaching Saul\'s conscience as he had just now of taking
away his life. This he lays hold on, though not of that, and enters into
a close argument with him, concerning the trouble he still continued to
give him, endeavouring to persuade him to let fall the prosecution and
be reconciled.

`1.` He complains of the very melancholy condition he was brought into by
the enmity of Saul against him. Two things he laments:-`(1.)` That he was
driven from his master and from his business: \"My lord pursues after
his servant, v. 18. How gladly would I serve thee as formerly if my
service might be accepted! but, instead of being owned as a servant, I
am pursued as a rebel, and my lord is my enemy, and he whom I would
follow with respect compels me to flee from him.\" `(2.)` That he was
driven from his God and from his religion; and this was a much greater
grievance than the former (v. 19): \"They have driven me out from the
inheritance of the Lord, have made Canaan too hot for me, at least the
inhabited parts of it, have forced me into the deserts and mountains,
and will, ere long, oblige me entirely to quit the country.\" And that
which troubled him was not so much that he was driven out from his own
inheritance as that he was driven out from the inheritance of the Lord,
the holy land. It should be more comfortable to us to think of God\'s
title to our estates and his interest in them then of our own, and that
with them we may honour him then that with them we may maintain
ourselves. Nor was it so much his trouble that he was constrained to
live among strangers as that he was constrained to live among the
worshippers of strange gods and was thereby thrust into temptation to
join with them in their idolatrous worship. His enemies did, in effect,
send him to go and serve other gods, and perhaps he had heard that some
of them had spoken to that purport of him. Those that forbid our
attendance on God\'s ordinances do what in them lies to estrange us from
God and to make us heathens. If David had not been a man of
extraordinary grace, and firmness to his religion, the ill usage he met
with from his own prince and people, who were Israelites and worshippers
of the true God, would have prejudiced him against the religion they
professed and have driven him to communicate with idolaters. \"If these
be Israelites,\" he might have said, \"let me live and die with
Philistines;\" and no thanks to them that their conduct had not that
effect. We are to reckon that the greatest injury that can be done us
which exposes us to sin. Of those who thus led David into temptation he
here says, Cursed be they before the Lord. Those fall under a curse that
thrust out those whom God receives, and send those to the devil who are
dear to God.

`2.` He insists upon his own innocency: What have I done or what evil is
in my hand? v. 18. He had the testimony of his conscience for him that
he had never done nor ever designed any mischief to the person, honour,
or government, of his prince, nor to any of the interests of his
country. He had lately had Saul\'s own testimony concerning him (ch.
24:17): Thou art more righteous than `I.` It was very unreasonable and
wicked for Saul to pursue him as a criminal, when he could not charge
him with any crime.

`3.` He endeavours to convince Saul that his pursuit of him is not only
wrong, but mean, and much below him: \"The king of Israel, whose dignity
is great, and who has so much other work to do, has come out to seek a
flea, as when one doth hunt a partridge in the mountains,\" v. 20-a poor
game for the king of Israel to pursue. He compares himself to a
partridge, a vert innocent harmless bird, which, when attempts are made
upon its life, flies if it can, but makes no resistance. And would Saul
bring the flower of his army into the field only to hunt one poor
partridge? What a disparagement was this to his honour! What a stain
would it be on his memory to trample upon so weak and patient as well as
so innocent an enemy! James v. 6, You have killed the just, and he doth
not resist you.

`4.` He desires that the core of the controversy may be searched into and
some proper method taken to bring it to an end, v. 19. Saul himself
could not say that justice put him on thus to persecute David, or that
he was obliged to do it for the public safety. David was not willing to
say (though it was very true) that Saul\'s own envy and malice put him
on to do it; and therefore he concludes it must be attributed either to
the righteous judgment of God or to the unrighteous designs of evil men.
Now, `(1.)` \"If the Lord have stirred thee up against me, either in
displeasure to me (taking this way to punish me for my sins against him,
though, as to thee, I am guiltless) or in displeasure to thee, if it be
the effect of that evil spirit from the Lord which troubles thee, let
him accept an offering from us both-let us join in making our peace with
God, reconciling ourselves to him, which may be done, by sacrifice; and
then I hope the sin will be pardoned, whatever it is, and the trouble,
which is so great a vexation both to thee and me, will come to an end.\"
See the right method of peace-making; let us first make God our friend
by Christ the great Sacrifice, and then all other enmities shall be
slain, Eph. 2:16; Prov. 16:7. But, `(2.)` \"If thou art incited to it by
wicked men, that incense thee against me, cursed be they before the
Lord,\" that is, they are very wicked people, and it is fit that they
should be abandoned as such, and excluded from the king\'s court and
councils. He decently lays the blame upon the evil counsellors who
advised the king to that which was dishonourable and dishonest, and
insists upon it that they be removed from about him and forbidden his
presence, as men cursed before the Lord, and then he hoped he should
gain his petition, which is (v. 20), \"Let not my blood fall to the
earth, as thou threatenest, for it is before the face of the Lord, who
will take cognizance of the wrong and avenge it.\" Thus pathetically
does David plead with Saul for his life, and, in order to that, for his
favourable opinion of him.

### Verses 21-25

Here is, `I.` Saul\'s penitent confession of his fault and folly in
persecuting David and his promise to do so no more. This second instance
of David\'s respect to him wrought more upon him than the former, and
extorted from him better acknowledgements, v. 21. 1. He owns himself
melted and quite overcome by David\'s kindness to him: \"My soul was
precious in thy eyes this day, which, I thought, had been odious!\" 2.
He acknowledges he has done very wrong to persecute him, that he has
therein acted against God\'s law (I have sinned), and against his own
interest (I have played the fool), in pursuing him as an enemy who would
have been one of his best friends, if he could but have thought so.
\"Herein (says he) I have erred exceedingly, and wronged both thee and
myself.\" Note, Those that sin play the fool and err exceedingly, those
especially that hate and persecute God\'s people, Job 19:28. 3. He
invites him to court again: Return, my son David. Those that have
understanding will see it to be their interest to have those about them
that behave themselves wisely, as David did, and have God with them. 4.
He promises him that he will not persecute him as he has done, but
protect him: I will no more do thee harm. We have reason to think,
according to the mind he was now in, that he meant as he said, and yet
neither his confession nor his promise of amendment came from a
principle of true repentance.

`II.` David\'s improvement of Saul\'s convictions and confessions and the
evidence he had to produce of his own sincerity. He desired that one of
the footmen might fetch the spear (v. 22), and then (v. 23), 1. He
appeals to God as judge of the controversy: The Lord render to every man
his righteousness. David, by faith, is sure that he will do it because
he infallibly knows the true characters of all persons and actions and
is inflexibly just to render to every man according to his work, and, by
prayer, he desires he would do it. Herein he does, in effect, pray
against Saul, who had dealt unrighteously and unfaithfully with him
(Give them according to their deeds, Ps. 28:4); but he principally
intends it as a prayer for himself, that God would protect him in his
righteousness and faithfulness, and also reward him, since Saul so ill
requited him. 2. He reminds Saul again of the proof he had now given of
his respect to him from a principle of loyalty: I would not stretch
forth my hand against the Lord\'s anointed, intimating to Saul that the
anointing oil was his protection, for which he was indebted to the Lord
and ought to express his gratitude to him (had he been a common person
David would not have been so tender of him), perhaps with this further
implication, that Saul knew, or had reason to think, David was the
Lord\'s anointed too, and therefore, by the same rule, Saul ought to be
as tender of David\'s life as David had been of his. 3. Not relying much
upon Saul\'s promises, he puts himself under God\'s protection and begs
his favour (v. 24): \"Let my life be much set by in the eyes of the
Lord, how light soever thou makest of it.\" Thus, for his kindness to
Saul, he takes God to be his paymaster, which those may with a holy
confidence do that do well and suffer for it.

`III.` Saul\'s prediction of David\'s advancement. He commends him (v.
25): Blessed be thou, my son David. So strong was the conviction Saul
was now under of David\'s honesty that he was not ashamed to condemn
himself and applaud David, even in the hearing of his own soldiers, who
could not but blush to think that they had come out so furiously against
a man whom their master, when he meets him, caresses thus. He foretels
his victories, and his elevation at last: Thou shalt do great things.
Note, Those who make conscience of doing that which is truly good may
come, by the divine assistance, to do that which is truly great. He
adds, \"Thou shalt also still prevail, more and more,\" he means against
himself, but is loth to speak that out. The princely qualities which
appeared in David-his generosity in sparing Saul, his military authority
in reprimanding Abner for sleeping, his care of the public good, and the
signal tokens of God\'s presence with him-convinced Saul that he would
certainly be advanced to the throne at last, according to the prophecies
concerning him.

Lastly, A palliative cure being thus made of the wound, they parted
friends. Saul returned to Gibeah re infectâ-without accomplishing his
design, and ashamed of the expedition he had made; but David could not
take his word so far as to return with him. Those that have once been
false are not easily trusted another time. Therefore David went on his
way. And, after this parting, it does not appear that ever Saul and
David saw one another again.
