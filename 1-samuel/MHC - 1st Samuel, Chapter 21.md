1st Samuel, Chapter 21
======================

Commentary
----------

David has now quite taken leave both of Saul\'s court and of his camp,
has bidden farewell to his alter idem-his other self, the beloved
Jonathan; and henceforward to the end of this book he is looked upon and
treated as an outlaw and proclaimed a traitor. We still find him
shifting from place to place for his own safety, and Saul pursuing him.
His troubles are very particularly related in this and the following
chapters, not only to be a key to the Psalms, but that he might be, as
other prophets, an example to the saints in all ages, \"of suffering
affliction, and of patience,\" and especially that he might be a type of
Christ, who, being anointed to the kingdom, humbled himself, and was
therefore highly exalted. But the example of the suffering Jesus was a
copy without a blot, that of David was not so; witness the records of
this chapter, where we find David in his flight, `I.` Imposing upon
Abimelech the priest, to get from him both victuals and arms (v. 1-9).
`II.` Imposing upon Achish, king of Gath, by feigning himself mad (v.
10-15). Justly are troubles called temptations, for many are by them
drawn into sin.

### Verses 1-9

Here, `I.` David, in distress, flies in the tabernacle of God, now pitched
at Nob, supposed to be a city in the tribe of Benjamin. Since Shiloh was
forsaken, the tabernacle was often removed, though the ark still
remained at Kirjath-jearim. Hither David came in his flight from Saul\'s
fury (v. 1), and applied to Ahimelech the priest. Samuel the prophet
could not protect him, Jonathan the prince could not. He therefore has
recourse next to Ahimelech the priest. He foresees he must now be an
exile, and therefore comes to the tabernacle, 1. To take an affecting
leave of it, for he knows not when he shall see it again, and nothing
will be more afflictive to him in his banishment than his distance from
the house of God, and his restraint from public ordinances, as appears
by many of his psalms. He had given an affectionate farewell to his
friend Jonathan, and cannot go till he has given the like to the
tabernacle. 2. To enquire of the Lord there, and to beg direction from
him in the way both of duty and safety, his case being difficult and
dangerous. That this was his business appears ch. 22:10, where it is
said that Ahimelech enquired of the Lord for him, as he had done
formerly, v. 15. It is a great comfort to us in a day of trouble that we
have a God to go to, to whom we may open our case, and from whom we may
ask and expect direction.

`II.` Ahimelech the priest is surprised to see him in so poor an
equipage; having heard that he had fallen into disgrace at court, he
looked shy upon him, as most are apt to do upon their friends when the
world frowns upon them. He was afraid of incurring Saul\'s displeasure
by entertaining him, and took notice how mean a figure he now made to
what he used to make: Why art thou alone? He had some with him (as
appears Mk. 2:26), but they were only his own servants; he had none of
the courtiers, no persons of quality with him, as he used to have at
other times, when he came to enquire of the Lord. He says (Ps. 42:4) he
was wont to go with a multitude to the house of God; and, having now but
two or three with him, Ahimelech might well ask, Why art thou alone? He
that was suddenly advanced from the solitude of a shepherd\'s life to
the crowd and hurries of the camp is now as soon reduced to the desolate
condition of an exile and is alone like a sparrow on the housetop, such
charges are there in this world and so uncertain are its smiles! Those
that are courted to-day may be deserted to-morrow.

`III.` David, under pretence of being sent by Saul upon public services,
solicits Ahimelech to supply his present wants, v. 2, 3.

`1.` Here David did not behave like himself. He told Ahimelech a gross
untruth, that Saul had ordered him business to despatch, that his
attendants were dismissed to such a place, and that he was charged to
observe secresy and therefore durst not communicate it, no, not to the
priest himself. This was all false. What shall we say to this? The
scripture does not conceal it, and we dare not justify it. It was ill
done, and proved of bad consequence; for it occasioned the death of the
priests of the Lord, as David reflected upon it afterwards with regret,
ch. 22:22. It was needless for him thus to dissemble with the priest,
for we may suppose that, if he had told him the truth, he would have
sheltered and relieved him as readily as Samuel did, and would have
known the better how to advise him and enquire of God for him. People
should be free with their faithful ministers. David was a man of great
faith and courage, and yet now both failed him, and he fell thus foully
through fear and cowardice, and both owing to the weakness of his faith.
Had he trusted God aright, he would not have used such a sorry sinful
shift as this for his own preservation. It is written, not for our
imitation, no, not in the greatest straits, but for our admonition. Let
him that thinks he stands take heed lest he fall; and let us all pray
daily, Lord, lead us not into temptation. Let us all take occasion from
this to lament, `(1.)` The weakness and infirmity of good men; the best
are not perfect on this side heaven. There may be true grace where yet
there are many failings. `(2.)` The wickedness of bad times, which forces
good men into such straits as prove temptations too strong for them.
Oppression makes a wise man do foolishly.

`2.` Two things David begged of Ahimelech, bread and a sword.

`(1.)` He wanted bread: five loaves, v. 3. Travelling was then
troublesome, when men generally carried their provisions with them in
kind, having little money and no public houses, else David would not now
have had to seek for bread. It seems David had known the seed of the
righteous begging bread occasionally, but not constantly, Ps. 37:25.
Now, `[1.]` The priest objected that he had none but hallowed bread,
show-bread, which had stood a week on the golden table in the sanctuary,
and was taken thence for the use of the priests and their families, v.
4. It seems the priest kept no good house, but wanted either a heart to
be hospitable or provisions wherewithal to be so. Ahimelech thinks that
the young men that attended David might not eat of this bread unless
they had for some time abstained from women, even from their own wives;
this was required at the giving of the law (Ex. 19:15), but otherwise we
never find this made the matter of any ceremonial purity on the one side
or pollution on the other, and therefore the priest here seems to be
over-nice, not to say superstitious. `[2.]` David pleads that he and
those that were with him, in this case of necessity, might lawfully eat
of the hallowed bread, for they were not only able to answer his terms
of keeping from women for three days past, but the vessels (that is, the
bodies) of the young men were holy, being possessed in sanctification
and honour at all times (1 Th. 4:4, 5), and therefore God would take
particular care of them, that they wanted not necessary supports, and
would have his priest to do so. Being thus holy, holy things were not
forbidden them. Poor and pious Israelites were in effect priests to God,
and, rather than be starved, might feed on the bread which was
appropriated to the priests. Believers are spiritual priests, and the
offerings of the Lord shall be their inheritance; they eat the bread of
their God. He pleads that the bread is in a manner common, now that what
was primarily the religious use of it is over; especially (as our margin
reads it) where there is other bread (hot, v. 6) sanctified that day in
the vessel, and put in the room of it upon the table. This was David\'s
plea, and the Son of David approves it, and shows from it that mercy is
to be preferred to sacrifice, that ritual observance must give way to
moral duties, and that may be done in a case of an urgent providential
necessity which may not otherwise be done. He brings it to justify his
disciples in plucking the ears of corn on the sabbath day, for which the
Pharisees censured them, Mt. 12:3, 4. `[3.]` Ahimelech hereupon supplies
him: He gave him hallowed bread (v. 6), and some think it was about this
that he enquired of the Lord, ch. 22:10. As a faithful servant he would
not dispose of his master\'s provisions without his master\'s leave.
This bread, we may suppose, was the more agreeable to David for its
being hallowed, so precious were all sacred things to him. The
show-bread was but twelve loaves in all, yet out of these he gave David
five (v. 3), though they had no more in the house; but he trusted
Providence.

`(2.)` He wanted a sword. Persons of quality, though officers of the army,
did not then wear their swords so constantly as now they do, else surely
David would not have been without one. It was a wonder that Jonathan did
not furnish him with his, as he had before done, ch. 18:4. However, it
happened that he had now no weapons with him, the reason of which he
pretends to be because he came away in haste, v. 8. Those that are
furnished with the sword of the Spirit and the shield of faith cannot be
disarmed of them, nor need they, at any time, to be at a loss. But the
priests, it seems, had no swords: the weapons of their warfare were not
carnal. There was not a sword to be found about the tabernacle but the
sword of Goliath, which was laid up behind the ephod, as a monument of
the glorious victory David obtained over him. Probably David had an eye
to that when he asked the priest to help him with a sword; for, that
being mentioned, O! says he, there is none like that, give it to me, v.
9. He could not use Saul\'s armour, for he had not proved it; but this
sword of Goliath he had made trial of and done execution with. By this
it appears that he was now well grown in strength and stature, that he
could wear and wield such a sword as that. God had taught his hands to
war, so that he could do wonders, Ps. 18:34. Two things we may observe
concerning this sword:-`[1.]` That God had graciously given it to him,
as a pledge of his singular favour; so that whenever he drew it, nay,
whenever he looked upon it, it would be a great support to his faith, by
bringing to mind that great instance of the particular care and
countenance of the divine providence respecting him. `[2.]` That he had
gratefully given it back to God, dedicating it to him and to his honour
as a token of his thankfulness; and now in his distress it stood him
greatly in stead. Note, What we devote to God\'s praise, and serve him
with, is most likely to redound, one way or other, to our own comfort
and benefit. What we gave we have.

Thus was David well furnished with arms and victuals; but it fell out
very unhappily that there was one of Saul\'s servants then attending
before the Lord, Doeg by name, that proved a base traitor both to David
and Ahimelech. He was by birth an Edomite (v. 7), and though proselyted
to the Jewish religion, to get the preferment he now had under Saul, yet
he retained the ancient and hereditary enmity of Edom to Israel. He was
master of the herds, which perhaps was then a place of as much honour as
master of the horse is now. Some occasion or other he had at this time
to wait on the priest, either to be purified from some pollution or to
pay some vow; but, whatever his business was, it is said, he was
detained before the Lord. He must attend and could not help it, but he
was sick of the service, snuffed at it, and said, What a weariness is
it! Mal. 1:13. He would rather have been any where else than before the
Lord, and therefore, instead of minding the business he came about, was
plotting to do David a mischief and to be revenged on Ahimelech for
detaining him. God\'s sanctuary could never secure such wolves in
sheep\'s clothing. See Gal. 2:4.

### Verses 10-15

David, though king elect, is here an exile-designed to be master of vast
treasures, yet just now begging his bread-anointed to the crown, and yet
here forced to flee from his country. Thus do God\'s providences
sometimes seem to run counter to his promises, for the trial of his
people\'s faith, and the glorifying of his name, in the accomplishment
of his counsels, notwithstanding the difficulties that lay in the way.
Here is, 1. David\'s flight into the land of the Philistines, where he
hoped to be hid, and to remain undiscovered in the court or camp of
Achish king of Gath, v. 10. Israel\'s darling is necessitated to quit
the land of Israel, and he that was the Philistine\'s great enemy (upon
I know not what inducements) goes to seek for shelter among them. It
should seem that as, though the Israelites loved him, yet the king of
Israel had a personal enmity to him, which obliged him to leave his own
country, so, though the Philistines hated him, yet the king of Gath had
a personal kindness for him, valuing his merit, and perhaps the more for
his killing Goliath of Gath, who, it may be, had been no friend to
Achish. To him David now went directly, as to one he could confide in,
as afterwards (ch. 27:2, 3), and Achish would not have protected him but
that he was afraid of disobliging his own people. God\'s persecuted
people have often found better usage from Philistines than from
Israelites, in the Gentile theatres than in the Jewish synagogues. The
king of Judah imprisoned Jeremiah, and the king of Babylon set him at
liberty. 2. The disgust which the servants of Achish took at his being
there, and their complaint of it to Achish (v. 11): \"Is not this David?
Is not this he that has triumphed over the Philistines? witness that
burden of the song which was so much talked of, Saul has slain his
thousands, but David, this very man, his ten thousands. Nay, Is not this
he that (if our intelligence from the land of Israel be true) is, or is
to be, king of the land?\" As such, \"he must be an enemy to our
country; and is it safe or honourable for us to protect or entertain
such a man?\" Achish perhaps had intimated to them that it would be
policy to entertain David, because he was now an enemy to Saul, and he
might be hereafter a friend to them. It is common for the outlaws of a
nation to be sheltered by the enemies of that nation. But the servants
of Achish objected to his politics, and thought it not at all fit that
he should stay among them. 3. The fright which this put David into.
Though he had some reason to put confidence in Achish, yet, when he
perceived the servants of Achish jealous of him, he began to be afraid
that Achish would be obliged to deliver him up to them, and he was
sorely afraid (v. 12), and perhaps he was the more apprehensive of his
own danger, when he was thus discovered, because he wore Goliath\'s
sword, which, we may suppose, was well known in Gath, and with which he
had reason to expect they would cut off his head, as he had cut off
Goliath\'s with it. David now learned by experience what he has taught
us (Ps. 118:9), that it is better to trust in the Lord than to put
confidence in princes. Men of high degree are a lie, and, if we make
them our hope, they may prove our fear. It was at this time that David
penned Psalm 55 (Michtam, a golden psalm), when the Philistines took him
in Gath, where having shown before God his distresses, he resolves (v.
3), \"What time I am afraid I will trust in thee; and therefore (v. 11)
will not be afraid what man can do unto me, no, not the sons of
giants.\" 4. The course he took to get out of their hands: He feigned
himself mad, v. 13. He used the gestures and fashions of a natural fool,
or one that had gone out of his wits, supposing they would be ready
enough to believe that the disgrace he had fallen into, and the troubles
he was now in, had driven him distracted. This dissimulation of his
cannot be justified (it was a mean thing thus to disparage himself, and
inconsistent with truth thus to misrepresent himself, and therefore not
becoming the honour and sincerity of such a man as David); yet it may in
some degree be excused, for it was not a downright lie and it was like a
stratagem in war, by which he imposed upon his enemies for the
preservation of his own life. What David did here in pretence and for
his own safety, which made it partly excusable, drunkards do really, and
only to gratify a base lust: they made fools of themselves and change
their behaviour; their words and actions commonly are either as silly
and ridiculous as an idiot\'s or as furious and outrageous as a
madman\'s, which has often made me wonder that ever men of sense and
honour should allow themselves in it. 5. His escape by this means, v.
14, 15. I am apt to think Achish was aware that the delirium was but
counterfeit, but, being desirous to protect David (as we find afterwards
he was very kind to him, even when the lord of the Philistines favoured
him not, ch. 28:1, 2; 29:6), he pretended to his servants that he really
thought he was mad, and therefore had reason to question whether it was
David or no; or, if it were, they need not fear him, what harm could he
do them now that his reason had departed from him? They suspected that
Achish was inclined to entertain him: \"Not I,\" says he. \"He is a
madman. I\'ll have nothing to do with him. You need not fear that I
should employ him, or give him any countenance.\" He humours the thing
well enough when he asks, \"Have I need of madmen? Shall this fool come
into my house? I will show him no kindness, but then you shall do him no
hurt, for, if he be a madmen, he is to be pitied.\" He therefore drove
him away, as it is in the title of Ps. 34, which David penned upon this
occasion, and an excellent psalm it is, and shows that he did not change
his spirit when he changed his behaviour, but even in the greatest
difficulties and hurries his heart was fixed, trusting in the Lord; and
he concludes that psalm with this assurance, that none of those that
trust in God shall be desolate, though they may be, as he now was,
solitary and distressed, persecuted, but not forsaken.
