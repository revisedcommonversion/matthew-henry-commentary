1st Samuel, Chapter 12
======================

Commentary
----------

We left the general assembly of the states together, in the close of the
foregoing chapter; in this chapter we have Samuel\'s speech to them,
when he resigned the government into the hands of Saul, in which, `I.` He
clears himself from all suspicion or imputation of mismanagement, while
the administration was in his hands (v. 1-5). `II.` He reminds them of the
great things God had done for them and for their fathers (v. 6-13). `III.`
He sets before them good and evil, the blessing and the curse (v. 14,
15). `IV.` He awakens them to regard what he said to them, by calling to
God for thunder (v. 16-19). `V.` He encourages them with hopes that all
should be well (v. 20-25). This is his farewell sermon to that august
assembly and Saul\'s coronation sermon.

### Verses 1-5

Here, `I.` Samuel gives them a short account of the late revolution, and
of the present posture of their government, by way of preface to what he
had further to say to them, v. 1, 2. 1. For his own part, he had spent
his days in their service; he began betimes to be useful among them, and
had continued long so: \"I have walked before you, as a guide to direct
you, as a shepherd that leads his flock (Ps. 80:1), from my childhood
unto this day.\" As soon as he was illuminated with the light of
prophecy, in his early days, he began to be a burning and shining light
to Israel; \"and now my best days are done: I am old and gray-headed;\"
therefore they were the more unkind to cast him off, yet therefore he
was the more willing to resign, finding the weight of government heavy
upon his stooping shoulders. He was old, and therefore the more able to
advise them, and the more observant they should have been of what he
said, for days shall speak and the multitude of years shall teach
wisdom; and there is a particular reverence due to the aged, especially
aged magistrates and aged ministers. \"I am old, and therefore not
likely to live long, perhaps may never have an opportunity of speaking
to you again, and therefore take notice of what I say.\" 2. As for his
sons, \"Behold\" (says he), \"they are with you, you may, if you please,
call them to an account for any thing they have done amiss. They are
present with you, and have not, upon this revolution, fled from their
country. They are upon the level with you, subjects to the new king as
well as you; if you can prove them guilty of any wrong, you may
prosecute them now by a due course of law, punish them, and oblige them
to make restitution.\" 3. As for their new king, Samuel had gratified
them in setting him over them (v. 1): \"I have hearkened to your voice
in all that you said to me, being desirous to please you, if possible,
and make you easy, though to the discarding of myself and family; and
now will you hearken to me, and take my advice?\" The change was now
perfected: \"Behold, the king walketh before you\" (v. 2); he appears in
public, ready to serve you in public business. Now that you have made
yourselves like the nations in your civil government, and have cast off
the divine administration in that, take heed lest you make yourselves
like the nations in religion and cast off the worship of God.

`II.` He solemnly appeals to them concerning his own integrity in the
administration of the government (v. 3): Witness against me, whose ox
have I taken? Observe,

`1.` His design in this appeal. By this he intended, `(1.)` To convince
them of the injury they had done him in setting him aside, when they had
nothing amiss to charge him with (his government had no fault but that
it was too cheap, too easy, too gentle), and also of the injury they had
done themselves in turning off one that did not so much as take an ox or
an ass from them, to put themselves under the power of one that would
take from them their fields and vineyards, nay, and their very sons and
daughters (ch. 8:11), so unlike would the manner of the king be from
Samuel\'s manner. `(2.)` To preserve his own reputation. Those that heard
of Samuel\'s being rejected as he was would be ready to suspect that
certainly he had done some evil thing, or he would never have been so
ill treated; so that it was necessary for him to make this challenge,
that it might appear upon record that it was not for any iniquity in his
hands that he was laid aside, but to gratify the humour of a giddy
people, who owned they could not have a better man to rule them, only
they desired a bigger man. There is a just debt which every man owes to
his own good name, especially men in public stations, which is to guard
it against unjust aspersions and suspicions, that we may finish our
course with honour as well as joy. `(3.)` As he designed hereby to leave a
good name behind him, so he designed to leave his successor a good
example before him; let him write after his copy, and he will write
fair. `(4.)` He designed, in the close of his discourse, to reprove the
people, and therefore he begins with a vindication of himself; for he
that will, with confidence, tell another of his sin, must see to it that
he himself be clear.

`2.` In the appeal itself observe,

`(1.)` What it is that Samuel here acquits himself from. `[1.]` He had
never, under any pretence whatsoever, taken that which was not his own,
ox or ass, had never distrained their cattle for tribute, fines, or
forfeitures, nor used their service without paying for it. `[2.]` He had
never defrauded those with whom he dealt, nor oppressed those that were
under his power. `[3.]` He had never taken bribes to pervert justice,
nor was ever biassed by favour for affection to give judgment in a cause
against his conscience.

`(2.)` How he calls upon those that had slighted him to bear witness
concerning his conduct: \"Here I am; witness against me. If you have any
thing to lay to my charge, do it before the Lord and the king, the
proper judges.\" He puts honour upon Saul, by owning himself accountable
to him if guilty of any wrong.

`III.` Upon this appeal he is honourably acquitted. He did not expect
that they would do him honour at parting, though he well deserved it,
and therefore mentioned not any of the good services he had done them,
for which they ought to have applauded him, and returned him the thanks
of the house; all he desired was that they should do him justice, and
that they did (v. 4) readily owning, 1. That he had not made his
government oppressive to them, nor used his power to their wrong. 2.
That he had not made it expensive to them: Neither hast thou taken aught
of any man\'s hand for the support of thy dignity. Like Nehemiah, he did
not require the bread of the governor (Neh. 5:18), had not only been
righteous, but generous, had coveted no man\'s silver, or gold, or
apparel, Acts 20:33.

`IV.` This honourable testimony borne to Samuel\'s integrity is left upon
record to his honour (v. 5): \"The Lord is witness, who searcheth the
heart, and his anointed is witness, who trieth overt acts;\" and the
people agree to it: \"He is witness.\" Note, The testimony of our
neighbours, and especially the testimony of our own consciences for us,
that we have in our places lived honestly, will be our comfort under the
slights and contempts that are put upon us. Demetrius is a happy man,
that has a good report of all men and of the truth itself, 3 Jn. 12.

### Verses 6-15

Samuel, having sufficiently secured his own reputation, instead of
upbraiding the people upon it with their unkindness to him, sets himself
to instruct them, and keep them in the way of their duty, and then the
change of the government would be the less damage to them.

`I.` He reminds them of the great goodness of God to them and to their
fathers, gives them an abstract of the history of their nation, that, by
the consideration of the great things God had done for them, they might
be for ever engaged to love him and serve him. \"Come,\" says he (v. 7),
\"stand still, stand in token of reverence when God is speaking to you,
stand still in token of attention and composedness of mind, and give me
leave to reason with you.\" Religion has reason on its side, Isa. 1:18.
The work of ministers is to reason with people, not only to exhort and
direct, but to persuade, to convince men\'s judgments, and so to gain
their wills and affections. Let reason rule men, and they will be good.
He reasons of the righteous acts of the Lord, that is, \"both the
benefits he hath bestowed upon you, in performance of his promises, and
the punishments he has inflicted on you for your sins.\" His favours are
called his righteous acts (Jdg. 5:11), because in them he is just to his
own honour. He not only puts them in mind of what God had done for them
in their days, but of what he had done of old, in the days of their
fathers, because the present age had the benefit of God\'s former
favours. We may suppose that his discourse was much larger than as here
related. 1. he reminds them of their deliverance out of Egypt. Into that
house of bondage Jacob and his family came down poor and little; when
they were oppressed they cried unto God, who advanced Moses and Aaron,
from mean beginnings, to be their deliverers, and the founders of their
state and settlement in Canaan, v. 6, 8. 2. He reminds them of the
miseries and calamities which their fathers brought themselves into by
forgetting God and serving other gods, v. 9. They enslaved themselves,
for they were sold as criminals and captives into the hand of
oppressors. They exposed themselves to the desolation of war, and their
neighbours fought against them. 3. He reminds them of their fathers\'
repentance and humiliation before God for their idolatries: They said,
We have sinned, v. 10. Let not them imitate the sins of their fathers,
for what they had done amiss they had many a time wished undone again.
In the day of their distress they had sought unto God, and had promised
to serve him; let their children then reckon that good at all times
which they found good in bad times. 4. He reminds them of the glorious
deliverances God had wrought for them, the victories he had blessed them
with, and their happy settlements, many a time, after days of trouble
and distress, v. 11. He specifies some of their judges, Gideon and
Jephthah, great conquerors in their time; among the rest he mentions
Bedan, whom we read not of any where else: he might be some eminent
person, that was instrumental of salvation to them, though not recorded
in the book of Judges, such a one as Shamgar, of whom it is said that he
delivered Israel, but not that he judged them, Jdg. 3:31. Perhaps this
Bedan guarded and delivered them on one side, at the same time when some
other of the judges appeared and acted for them on another side. Some
think it was the same with Jair (so the learned Mr. Poole), others the
same with Samson, who was Ben Dan, a son of Dan, of that tribe, and the
Spirit of the Lord came upon him Be-Dan, inn Dan, in the camp of Can.
Samuel mentions himself, not to his own praise, but to the honour of
God, who had made him an instrument of subduing the Philistines. 5. At
last he puts them in mind of God\'s late favour to the present
generation, in gratifying them with a king, when they would prescribe to
God by such a one to save them out of the hand of Nahash king of Ammon,
v. 12, 13. Now it appears that this was the immediate occasion of their
desiring a king: Nahash threatened them; they desired Samuel to nominate
a general; he told them that God was commander-in-chief in all their
wars and they needed no other, that what was wanting in them should be
made up by his power: The Lord is your king. But they insisted on it,
Nay, but a king shall reign over us. \"And now,\" said he, \"you have a
king, a king of your own asking-let that be spoken to your shame; but a
king of God\'s making-let that be spoken to his honour and the glory of
his grace.\" God did not cast them off, even when they in effect cast
him off.

`II.` He shows them that they are now upon their good behaviour, they and
their king. Let them not think that they had now cut themselves off from
all dependence upon God, and that now, having a king of their own, the
making of their own fortunes (as men foolishly call it) was in their own
hands; no, still their judgment must proceed from the Lord. He tells
them plainly,

`1.` That their obedience to God would certainly be their happiness, v.
14. If they would not revolt from God to idols, nor rebel against him by
breaking his commandments, but would persevere in their allegiance to
him, would fear his wrath, serve his interests, and obey his will, then
they and their king should certainly be happy; but observe how the
promise is expressed: Then you shall continue following the Lord your
God; that is, `(1.)` \"You shall continue in the way of your duty to God,
which will be your honour and comfort.\" Note, To those that are sincere
in their religion God will give grace to persevere in it: those that
follow God faithfully will be divinely strengthened to continue
following him. And observe, Following God is a work that is its own
wages. It is the matter of a promise as well as of a precept. `(2.)` \"You
shall continue under the divine guidance and protection:\" You shall be
after the Lord, so it is in the original, that is, \"he will go before
you to lead and prosper you, and make your way plain. The Lord is with
you while you are with him.\"

`2.` That their disobedience would as certainly be their ruin (v. 15):
\"If you rebel, think not that your having a king will secure you
against God\'s judgments, and that having in this instance made
yourselves like the nations you may sin at as cheap a rate as they can.
No, the hand of the Lord will be against you, as it was against your
fathers when they offended him, in the days of the judges.\" We mistake
if we think that we can evade God\'s justice by shaking off his
dominion. If God shall not rule us, yet he will judge us.

### Verses 16-25

Two things Samuel here aims at:-

`I.` To convince the people of their sin in desiring a king. They were now
rejoicing before God in and with their king (ch. 11:15), and offering to
God the sacrifices of praise, which they hoped God would accept; and
this perhaps made them think that there was no harm in their asking a
king, but really they had done well in it. Therefore Samuel here charges
it upon them as their sin, as wickedness, great wickedness in the sight
of the Lord. Note, Though we meet with prosperity and success in a way
of sin, yet we must not therefore think the more favourably of it. They
have a king, and if they conduct themselves well their king may be a
very great blessing to them, and yet Samuel will have them perceive and
see that their wickedness was great in asking a king. We must never
think well of that which God in his law frowns upon, though in his
providence he may seem to smile upon it. Observe,

`1.` The expressions of God\'s displeasure against them for asking a
king. At Samuel\'s word, God sent prodigious thunder and rain upon them,
at a season of the year when, in that country, the like was never seen
or known before, v. 16-18. Thunder and rain have natural causes and
sometimes terrible effects. But Samuel made it to appear that this was
designed by the almighty power of God on purpose to convince them that
they had done very wickedly in asking a king; not only by its coming in
an unusual time, in wheat-harvest, and this on a fair clear day, when
there appeared not to the eye any signs of a storm, but by his giving
notice of it before. Had there happened to be thunder and rain at the
time when he was speaking to them, he might have improved it for their
awakening and conviction, as we may in a like case; but, to make it no
less than a miracle, before it came, `(1.)` He spoke to them of it (v. 16,
17): Stand and see this great thing. He had before told them to stand
and hear (v. 7); but, because he did not see that his reasoning with
them affected them (so stupid were they and unthinking), now he bids
them stand and see. If what he said in a still small voice did not reach
their hearts, nor his doctrine which dropped as the dew, they shall hear
God speaking to them in dreadful claps of thunder and the great rain of
his strength. He appealed to this as a sign: \"I will call upon the
Lord, and he will send thunder, will send it just now, to confirm the
word of his servant, and to make you see that I spoke truly when I told
you that God was angry with you for asking a king.\" And the event
proved him a true prophet; the sign and wonder came to pass. `(2.)` He
spoke to God for it. Samuel called unto the Lord, and, in answer to his
prayer, even while he was yet speaking, the Lord sent thunder and rain.
By this Samuel made it to appear, not only what a powerful influence God
has upon this earth, that he could, of a sudden, when natural causes did
not work towards it, produce this dreadful rain and thunder, and bring
them out of his treasures (Ps. 135:7), but also what a powerful interest
he had in heaven, that God would thus hearken to the voice of a man
(Jos. 10:14) and answer him in the secret place of thunder, Ps. 81:7.
Samuel, that son of prayer, was still famous for success in prayer. Now
by this extraordinary thunder and rain sent on this occasion, `[1.]` God
testified his displeasure against them in the same way in which he had
formerly testified it, and at the prayer of Samuel too, against the
Philistines. The Lord discomfited them with a great thunder, ch. 7:10.
Now that Israel rebelled, and vexed his Holy Spirit, he turned to be
their enemy, and fought against them with the same weapons which, not
long before, had been employed against their adversaries, Isa. 63:10.
`[2.]` He showed them their folly in desiring a king to save them,
rather than God or Samuel, promising themselves more from an arm of
flesh than from the arm of God or from the power of prayer. Could their
king thunder with a voice like God? Job 40:9. Could their prince command
such forces as the prophet could by his prayers? `[3.]` He intimated to
them that how serene and prosperous soever their condition seemed to be
now that they had a king, like the weather in wheat-harvest, yet, if God
pleased, he could soon change the face of their heavens, and persecute
them with his tempest, as the Psalmist speaks.

`2.` The impressions which this made upon the people. It startled them
very much, as well it might. `(1.)` They greatly feared the Lord and
Samuel. Though when they had a king they were ready to think they must
fear him only, God made them know that he is greatly to be feared and
his prophets for his sake. Now they were rejoicing in their king, God
taught them to rejoice with trembling. `(2.)` They owned their sin and
folly in desiring a king: We have added to all our sins this evil, v.
19. Some people will not be brought to a sight of their sins by any
gentler methods than storms and thunders. Samuel did not extort this
confession from them till the matter was settled and the king confirmed,
lest it should look as if he designed by it rather to establish himself
in the government than to bring them to repentance. Now that they were
flattering themselves in their own eyes, their iniquity was found to be
hateful, Ps. 36:2. `(3.)` They earnestly begged Samuel\'s prayers (v. 19):
Pray for thy servants, that we die not. They were apprehensive of their
danger from the wrath of God, and could not expect that he should hear
their prayers for themselves, and therefore they entreat Samuel to pray
for them. Now they see their need of him whom awhile ago they slighted.
Thus many that will not have Christ to reign over them would yet be glad
to have him intercede for them, to turn away the wrath of God. And the
time may come when those that have despised and ridiculed praying people
will value their prayers, and desire a share in them. \"Pray\" (say
they) \"to the Lord thy God; we know not how to call him ours, but, if
thou hast any interest in him, improve it for us.\"

`II.` He aims to confirm the people in their religion, and engage them
for ever to cleave unto the Lord. The design of his discourse is much
the same with Joshua\'s, ch. 23 and 24.

`1.` He would not that the terrors of the Lord should frighten them from
him, for they were intended to frighten them to him (v. 20): \"Fear not;
though you have done all this wickedness, and though God is angry with
you for it, yet do not therefore abandon his service, nor turn from
following him.\" Fear not, that is, \"despair not, fear not with
amazement, the weather will clear up after the storm. Fear not; for,
though God will frown upon his people, yet he will not forsake them (v.
22) for his great name\'s sake; do not you forsake him then.\" Every
transgression in the covenant, though it displease the Lord, yet does
not throw us out of covenant, and therefore God\'s just rebukes must not
drive us from our hope in his mercy. The fixedness of God\'s choice is
owing to the freeness of it; we may therefore hope he will not forsake
his people, because it has pleased him to make them his people. Had he
chosen them for their good merits, we might fear he would cast them off
for their bad merits; but, choosing them for his name\'s sake, for his
name\'s sake he will not leave them.

`2.` He cautions them against idolatry: \"Turn not aside from God and the
worship of him\" (v. 20, and again v. 21); \"for if you turn aside from
God, whatever you turn aside to, you will find it is a vain thing, that
can never answer your expectations, but will certainly deceive you if
you trust to it; it is a broken reed, a broken cistern.\" Idols could
not profit those that sought to them in their wants, nor deliver those
that sought to them in their straits, for they were vain, and not what
they pretended to be. An idol is nothing in the world, 1 Co. 8:4.

`3.` He comforts them with an assurance that he would continue his care
and concern for them, v. 23. They desired him to pray for them, v. 19.
He might have said, \"Go to Saul, the king that you have put in my
room,\" and get him to pray for you; but so far is he from upbraiding
them with their disrespect to him that he promised them much more than
they asked. `(1.)` They asked it of him as a favour; he promised it as a
duty, and startles at the thought of neglecting it. Pray for you! says
he, God forbid that I should sin against the Lord in not doing it. Note,
It is a sin against God not to pray for the Israel of God, especially
for those of them that are under our charge: and good men are afraid of
the guilt of omissions. `(2.)` They asked him to pray for them at this
time, and upon this occasion, but he promised to continue his prayers
for them and to cease as long as he lived. Our rule is to pray without
ceasing; we sin if we restrain prayer in general, and in particular if
we cease praying for the church. `(3.)` They asked him only to pray for
them, but he promised to do more for them, not only to pray for them,
but to teach them; though they were not willing to be under his
government as a judge, he would not therefore deny them his instructions
as a prophet. And they might be sure he would teach them no other than
the good and the right way: and the right way is certainly the good way:
the way of duty is the way of pleasure and profit.

`4.` He concludes with an earnest exhortation to practical religion and
serious godliness, v. 24, 25. The great duty here pressed upon us is to
fear the Lord. He had said (v. 20), \"Fear not with a slavish fear,\"
but here, \"Fear the Lord, with a filial fear.\" As the fruit and
evidence of this, serve him in the duties of religious worship and of a
godly conversation, in truth and sincerity, and not in show and
profession only, with your heart, and with all your heart, not
dissembling, not dividing. And two things he urges by way of
motive:-`(1.)` That they were bound in gratitude to serve God, considering
what great things he had done for them, to engage them for ever to his
service. `(2.)` That they were bound in interest to serve him, considering
what great things he would do against them if they should still do
wickedly: \"You shall be destroyed by the judgments of God, both you and
your king whom you are so proud of and expect so much from, and who will
be a blessing to you if you keep in with God.\" Thus, as a faithful
watchman, he gave them warning, and so delivered his own soul.
