1st Samuel, Chapter 18
======================

Commentary
----------

In the course of the foregoing chapter we left David in triumph; now in
this chapter we have, `I.` The improvement of his triumphs; he soon
became, 1. Saul\'s constant attendant (v. 2). 2. Jonathan\'s covenant
friend (v. 1, 3, 4). 3. The darling of his country (v. 5, 7, 16). `II.`
The allays of his triumphs. This is the vanity that accompanies even a
right work, that \"for it a man is envied,\" Eccl. 4:4. So David was by
Saul. 1. He hated him, and sought to kill him himself (v. 8-11). 2. He
feared him, and contrived how he might have some mischief done him (v.
12-17). He proposed to marry his daughter to him; but, `[1.]` cheated
him of the eldest to provoke him (v. 19), and, `[2.]` Gave him the
younger, upon conditions which would endanger his life (v. 20-25). But
David performed his conditions bravely (v. 26, 27), and grew to be more
and more esteemed (v. 28-30). Still David is rising, but (as all that
aim at the crown of life must expect) he had a great deal of difficulty
and opposition to grapple with.

### Verses 1-5

David was anointed to the crown to take it out of Saul\'s hand, and over
Jonathan\'s head, and yet here we find,

`I.` That Saul, who was now in possession of the crown, reposed a
confidence in him, God so ordering it, that he might by his preferment
at court be prepared for future service. Saul now took David home with
him, and would not suffer him to return again to his retirement, v. 2.
And David having signalized himself above the men of war, in taking up
the challenge which they declined, Saul set him over the men of war (v.
5), not that he made him general (Abner was in that post), but perhaps
captain of the life-guard; or, though he was youngest, he ordered him to
have the precedency, in recompence of his great services. He employed
him in the affairs of government; and David went out withersoever Saul
sent him, showing himself as dutiful as he was bold and courageous.
Those that hope to rule must first learn to obey. He had approved
himself a dutiful son to Jesse his father, and now a dutiful servant to
Saul his master; those that are good in one relation it is to be hoped
will be so in another.

`II.` That Jonathan, who was heir to the crown, entered into covenant
with him, God so ordering it, that David\'s way might be the clearer
when his rival was his friend. 1. Jonathan conceived an extraordinary
kindness and affection for him (v. 1): When he had made an end of
speaking to Saul he fell perfectly in love with him. Whether it refers
to his conference with Saul before the battle (ch. 17:34, 37), or to
that after (v. 51), in which it is probable much more was said than is
there set down, is uncertain. But, in both, David expressed himself with
so much prudence, modesty, and piety, such a felicity of expression,
with so much boldness and yet so much sweetness, and all this so natural
and unaffected, and the more surprising because of the disadvantages of
his education and appearance, that the soul of Jonathan was immediately
knit unto the soul of David. Jonathan had formerly set upon a Philistine
army with the same faith and bravery with which David had now attacked a
Philistine giant; so that there was between them a very near resemblance
of affections, dispositions, and counsels, which made their spirits
unite to easily, so quickly, so closely, that they seemed but as one
soul in two bodies. None had so much reason to dislike David as Jonathan
had, because he was to put him by the crown, yet none regards him more.
Those that are governed in their love by principles of wisdom and grace
will not suffer their affections to be alienated by any secular regards
or considerations: the greater thoughts will swallow up and overrule the
less. 2. He testified his love to David by a generous present he made
him, v. 4. He was uneasy at seeing so great a soul, though lodged in so
fair a body, yet disguised in the mean and despicable dress of a poor
shepherd, and therefore takes care to put him speedily into the habit of
a courtier (for he gave him a robe) and of a soldier, for he gave him,
instead of his staff and sling, a sword and bow, and, instead of his
shepherd\'s scrip, a girdle, either a belt or a sash; and, which made
the present much more obliging, they were the same that he himself had
worn, and (as a presage of what would follow) he stripped himself of
them to dress David in them. Saul\'s would not fit him, but Jonathan\'s
did. Their bodies were of a size, a circumstance which well agreed with
the suitableness of their minds. When Saul put these marks of honour on
David he put them off again, because he would first earn them and then
wear them; but, now that he had given proofs of the spirit of a prince
and a soldier, he was not ashamed to wear the habits of a prince and a
soldier. David is seen in Jonathan\'s clothes, that all may take notice
he is a Jonathan\'s second self. Our Lord Jesus has thus shown his love
to us, that he stripped himself to clothe us, emptied himself to enrich
us; nay, he did more than Jonathan, he clothed himself with our rags,
whereas Jonathan did not put on David\'s. 3. He endeavored to perpetuate
this friendship. So entirely satisfied were they in each other, even at
the first interview, that they made a covenant with each other, v. 3.
Their mutual affection was sincere; and he that bears an honest mind
startles not at assurances. True love desires to be constant. Those who
love Christ as their own souls will be willing to join themselves to him
in an everlasting covenant.

`III.` That both court and country agree to bless him. It is but seldom
that they agree in their favourites; yet David was accepted in the sight
of all the people, and also (which was strange) in the sight of Saul\'s
servants, v. 5. The former cordially loved him, the latter could not for
shame but caress and compliment him. And it was certainly a great
instance of the power of God\'s grace in David that he was able to bear
all this respect and honour flowing in upon him on a sudden without
being lifted up above measure. Those that climb so fast have need of
good heads and good hearts. It is more difficult to know how to abound
than how to be abased.

### Verses 6-11

Now begin David\'s troubles, and they not only tread on the heels of his
triumphs, but take rise from them, such is the vanity of that in this
world which seems greatest.

`I.` He was too much magnified by the common people. Some time after the
victory Saul went a triumphant progress through the cities of Israel
that lay next him, to receive the congratulations of the country. And,
when he made his public entry into any place, the women were most
forward to show him respect, as was usual then in public triumphs (v.
6), and they had got a song, it seems, which they sang in their dances
(made by some poet or other, that was a great admirer of David\'s
bravery, and was more just than wise, in giving his achievements in the
late action the preference before Saul\'s), the burden of which was,
Saul had slain his thousands, and David his ten thousands. Such a
difference as this Moses made between the numbers of Ephraim and
Manasseh, Deu. 33:17.

`II.` This mightily displeased Saul, and made him envy David, v. 8, 9. He
ought to have considered that they referred only to this late action,
and intended not to diminish any of Saul\'s former exploits; and that in
the action now celebrated it was undeniably true that David, in killing
Goliath, did in effect slay all the Philistines that were slain that day
and defeated the whole army; so that they did but give David his due. It
may be, he that composed the song only used a poetic liberty, and
intended not any invidious comparison between Saul and David; or, if he
did, it was below the great mind of a prince to take notice of such a
reflection upon his personal honour, when it appeared that the glory of
the public was sincerely intended. But Saul was very wroth, and
presently suspected some treasonable design at the bottom of it: What
can he have more but the kingdom? This made him eye David as one he was
jealous of and sought advantages against (v. 9): his countenance was not
towards him as it had been. Proud men cannot endure to hear any praised
but themselves, and think all their honour lost that goes by themselves.
It is a sign that the Spirit of God has departed from men if they be
peevish in their resentment of affronts, envious and suspicious of all
about them, and ill-natured in their conduct; for the wisdom from above
makes us quite otherwise.

`III.` In his fury he aimed to kill David, v. 10, 11. Jealousy is the
rage of a man; it made Saul outrageous against David and impatient to
get him out of the way. 1. His fits of frenzy returned upon him. The
very next day after he conceived malice against David the evil spirit
from God, that had formerly haunted him, seized him again. Those that
indulge themselves in envy and uncharitableness give place to the devil,
and prepare for the re-entry of the unclean spirit, with seven others
more wicked. Where envy is there is confusion. Saul pretended a
religious ecstasy: He prophesied in the midst of the house, that is, he
had the gestures and motions of a prophet, and humoured the thing well
enough to decoy David into a snare, and that he might be fearless of any
danger and off his guard; and perhaps designing, if he could but kill
him, to impute it to a divine impulse and to charge it upon the spirit
of prophecy with which he seemed to be animated: but really it was a
hellish fury that actuated him. 2. David, though advanced to a much
higher post of honour, disdained not, for his master\'s service, to
return to his harp: He played with his hand as at other times. Let not
the highest think any thing below them whereby they may do good and be
serviceable to those they are obliged to. 3. He took this opportunity to
aim at the death of David. A sword in a madman\'s hand is a dangerous
thing, especially such a madman as Saul was, that was mad with malice.
Yet he had a javelin or dart in his hand, which he projected,
endeavouring thereby to slay David, not in a sudden passion, but
deliberately: I will smite David to the wall with it, with such a
desperate force did he throw it. Justly does David complain of his
enemies that they hated him with a cruel hatred, Ps. 25:19. No life is
thought too precious to be sacrificed to malice. If a grateful sense of
the great service David had done to the public could not assuage Saul\'s
fury, yet one would think he should have allowed himself to consider the
kindness David was now doing him, in relieving him, as no one else
could, against the worst of troubles. Those are possessed with a
devilish spirit indeed that render evil for good. Compare David, with
his harp in his hand, aiming to serve Saul, and Saul, with his javelin
in his hand, aiming to slay David; and observe the meekness and
usefulness of God\'s persecuted people and the brutishness and barbarity
of their persecutors. The bloodthirsty hate the upright, but the just
seek his soul, Prov. 29:10. 4. David happily avoided the blow twice
(namely, now, and afterwards, ch. 19:10); he did not throw the javelin
at Saul again, but withdrew, not fighting but flying for his own
preservation; though he had both strength and courage enough, and colour
of right, to make resistance and revenge the injury, yet he did no more
than secure himself, by getting out of the way of it. David, no doubt,
had a watchful eye upon Saul\'s hand, and the javelin in it, and did as
bravely in running from it as he did lately in running upon Goliath. Yet
his safety must be ascribed to the watchful eye of God\'s providence
upon him, saving his servant from the hurtful sword; and by this narrow
escape it seemed he was designed for something extraordinary.

### Verses 12-30

Saul had now, in effect, proclaimed war with David. He began in open
hostility when he threw the javelin at him. Now we are here told how his
enmity proceeded, and how David received the attacks of it.

`I.` See how Saul expressed his malice against David. 1. He was afraid of
him, v. 12. Perhaps he pretended to be afraid that David would do
himself mischief, to force his way to the crown. Those that design ill
against others are commonly willing to have it thought that others
design ill against them. But David\'s withdrawal (v. 11) was a plain
evidence that he was far from such a thought. However, he really stood
in awe of him, as Herod feared John, Mk. 6:20. Saul was sensible that he
had lost the favourable presence of God himself, and that David had it,
and for this reason he feared him. Note, Those are truly great and to be
reverenced that have God with them. The more wisely David behaved
himself the more Saul feared him, v. 15, and again v. 29. Men think the
way to be feared is to hector and threaten, which makes them feared by
fools only, but despised by the wise and good; whereas the way to be
both feared and loved, feared by those to whom we would wish to be a
terror and loved by those to whom we would wish to be a delight, is to
behave ourselves wisely. Wisdom makes the face to shine and commands
respect. 2. He removed him from court, and gave him a regiment in the
country, v. 13. He made him captain over 1000, that he might be from
under his eye, because he hated the sight of him; and that he might not
secure the interest of the courtiers. Yet herein he did impolitely; for
it gave David an opportunity of ingratiating himself with the people,
who therefore loved him (v. 16) because he went out and came in before
them, that is, he presided in the business of his country, civil as well
as military, and have universal satisfaction. 3. He stirred him up to
take all occasions of quarrelling with the Philistines and engaging them
(v. 17), insinuating to him that hereby he would do good service to his
prince (be thou valiant for me), and good service to his God (fight the
Lord\'s battles), and a kindness to himself too, for hereby he would
qualify himself for the honour he designed him, which was to marry his
eldest daughter to him. This he had merited by killing Goliath, for it
was promised by proclamation to him that should do that exploit (ch.
17:25); but David was so modest as not to demand it, and now, when Saul
proposed it, it was with design of mischief to him, to make him venture
upon hazardous attempts, saying in his heart, Let the hand of the
Philistines be upon him, hoping that he would some time or other be the
death of him; yet how could he expect this when he saw that God was with
him? 4. He did what he could to provoke him to discontent and mutiny, by
breaking his promise with him, and giving his daughter to another when
the time came that she should have been given to him, v. 19. This was as
great an affront as he could possibly put upon him, and touched him both
in his honour and in his love. He therefore thought David\'s resentment
of it would break out in some indecency or other, in word or deed, which
might give him an advantage against him to take him off by the course of
law. Thus evil men seek mischief. 5. When he was disappointed in his, he
proffered him his other daughter (who it seems had a secret kindness for
David, v. 20), but with this design, that she might be a snare to him,
v. 21. `(1.)` Perhaps he hoped that she would, even after her marriage to
David, take part with her father against her husband, and give him an
opportunity of doing David an unkindness. However, `(2.)` The conditions
of the marriage, he hoped, would be his destruction; for (so zealous
will Saul seem against the Philistines) the conditions of the marriage
must be that he killed 100 Philistines, and, as proofs that those he had
slain were uncircumcised, he must bring in their foreskins cut off; this
would be a just reproach upon the Philistines, who hated circumcision as
it was an ordinance of God; and perhaps David, in doing this, would the
more exasperate them against him, and make them seek to be revenged on
him, which was the thing that Saul desired and designed, much more than
to be avenged on the Philistines: For Saul thought to make David fall by
the Philistines, v. 25. See here, `[1.]` What cheats bad men put upon
themselves. Saul\'s conscience would not suffer him, except when the
evil spirit was actually upon him, to aim at David\'s life himself, for
even he could not but conceive a horror at the thought of murdering such
an innocent and excellent person; but he thought that to expose him
designedly to the Philistines had nothing bad in it (Let not my hand be
upon him, but the hand of the Philistines), whereas that malicious
design against him was as truly murder before God as if he had slain him
with his own hands. `[2.]` What cheats they put upon the world. Saul
pretended extraordinary kindness for David even when he aimed at his
ruin, and was actually plotting it: Thou shalt be my son-in-law, says he
(v. 21), notwithstanding he hated him implacably. Perhaps David refers
to this when (Ps. 55:21) he speaks of his enemy as one whose words were
smoother than butter, but war was in his heart. It is probable that
Saul\'s employing his servants to persuade David to enter into a treaty
of a match with his daughter Michal (v. 22) arose from an apprehension
that either his having cheated him about his elder daughter (v. 19) or
the hardness of the terms he intended now to propose would make him
decline it.

`II.` See how David conducted himself when the tide of Saul\'s
displeasure ran thus high against him.

`1.` He behaved himself wisely in all his ways. He perceived Saul\'s
jealousy of him, which made him very cautious and circumspect in every
thing he said and did, and careful to give no offence. He did not
complain of hard measure more make himself the head of a party, but
managed all the affairs he was entrusted with as one that made it his
business to do real service to his king and country, looking upon that
to be the end of his preferment. And then the Lord was with him to give
him success in all his undertakings. Though he procured Saul\'s ill-will
by it, yet he obtained God\'s favour. Compare this with Ps. 101:2, where
it is David\'s promise, I will behave myself wisely; and that promise he
here performed; and it is his prayer, O, when wilt thou come unto me?
And that prayer God here answered: The Lord was with him. However blind
fortune may seem to favour fools, God will own and bless those that
behave themselves wisely.

`2.` When it was proposed to him to be son-in-law to the king he once and
again received the proposal with all possible modesty and humility. When
Saul proposed his elder daughter to him (v. 18) he said, Who am I, and
what is my life? When the courtier proposed the younger, he took no
notice of the affront Saul had put upon him in disposing of the elder
from him, but continued in the same mind (v. 23): Seemeth it a light
thing to you to be a king\'s son-in-law, seeing that I am a poor man and
lightly esteemed? He knew Michal loved him, and yet did not offer to
improve his interest in her affections for the gaining of her without
her father\'s consent, but waited till it was proposed to him. And then
see, `(1.)` How highly he speaks of the honour offered him: To be
son-in-law to the king. Though his king was but an upstart, in his
original as mean as himself, in his management no better than he should
be, yet, being a crowned head, he speaks of him and the royal family
with all due respect. Note, Religion is so far from teaching us to be
rude and unmannerly that it does not allow us to be so. We must render
honour to whom honour is due. `(2.)` How humbly he speaks of himself: Who
am I? This did not proceed from a mean, abject, sneaking spirit, for
when there was occasion he made it appear that he had as high a sense of
honour as most men; nor was it from his jealousy of Saul (though he had
reason enough to fear a snake under the green grass), but from him true
and deep humility: Who am I, a poor man, and lightly esteemed? David had
as much reason as any man to value himself. He was of an ancient and
honourable family of Judah, a comely person, a great statesman and
soldier; his achievements were great, for he had won Goliath\'s head and
Michal\'s heart. He knew himself destined by the divine counsels to the
throne of Israel, and yet, Whom am I, and what is my life? Note, It well
becomes us, however God has advanced us, always to have low thoughts of
ourselves. He that humbleth himself shall be exalted. And, if David thus
magnified the honour of being son-in-law to the king, how should we
magnify the honour of being sons (not in law, but in gospel) to the King
of kings! Behold what manner of love the Father has bestowed upon us!
Who are we that we should be thus dignified?

`3.` When the slaying of 100 Philistines was made the condition of
David\'s marrying Saul\'s daughter he readily closed with it (v. 26): It
pleased David well to be the king\'s son-in-law upon those terms; and,
before the time given him for the action had expired, he doubled the
demand, and slew 200, v. 27. He would not seem to suspect that Saul
designed his hurt by it (though he had reason enough), but would rather
act as if Saul had meant to consult his honour, and therefore cheerfully
undertook it, as became a brave soldier and a true lover, though we may
suppose it uneasy to Michal. David hereby discovered likewise, `(1.)` A
great confidence in the divine protection. He knew God was with him, and
therefore, whatever Saul hoped, David did not fear falling by the
Philistines, though he must needs expose himself much by such an
undertaking as this. `(2.)` A great zeal for the good of his country,
which he would not decline any occasion of doing service to, though with
the hazard of his life. `(3.)` A right notion of honour, which consists
not so much in being preferred as in deserving to be so. David was then
pleased with the thoughts of being the king\'s son-in-law when he found
the honour set at this high price, being more solicitous how to merit it
than how to obtain it; nor could he wear it with satisfaction till he
had won it.

`4.` Even after he was married he continued his good services to Israel.
When the princes of the Philistines began to move towards another war
David was ready to oppose them, and behaved himself more wisely than all
the servants of Saul, v. 30. The law dispensed with men from going to
war the first year after they were married (Deu. 24:5), but David loved
his country too well to make use of that dispensation. Many that have
shown themselves forward to serve the public when they have been in
pursuit of preferment have declined it when they have gained their
point; but David acted from more generous principles.

`III.` Observe how God brought good to David out of Saul\'s project
against him. 1. Saul gave him his daughter to be a snare to him, but in
this respect that marriage was a kindness to him, that his being Saul\'s
son-in-law made his succeeding him much the less invidious, especially
when so many of his sons were slain with him, ch. 31:2. 2. Saul thought,
by putting him upon dangerous services, to have him taken off, but that
very thing confirmed his interest in the people; for the more he did
against the Philistines the better they loved him, so that his name was
much set by (v. 30), which would make his coming to the crown the more
easy. Thus God makes even the wrath of man to praise him and serves his
designs of kindness to his own people by it.
