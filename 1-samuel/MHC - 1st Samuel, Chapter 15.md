1st Samuel, Chapter 15
======================

Commentary
----------

In this chapter we have the final rejection of Saul from being king, for
his disobedience to God\'s command in not utterly destroying the
Amalekites. By his wars and victories he hoped to magnify and perpetuate
his own name and honour, but, by his mismanagement of them, he ruined
himself, and laid his honour in the dust. Here is, `I.` The commission God
gave him to destroy the Amalekites, with a command to do it utterly (v.
1-3). `II.` Saul\'s preparation for this expedition (v. 4-6). `III.` His
success, and partial execution of this commission (v. 7-9). `IV.` His
examination before Samuel, and sentence passed upon him, notwithstanding
the many frivolous pleas he made to excuse himself (v. 10-31). `V.` The
slaying of Agag (v. 32, 33). `VI.` Samuel\'s final farewell to Saul (v.
34, 35).

### Verses 1-9

Here, `I.` Samuel, in God\'s name, solemnly requires Saul to be obedient
to the command of God, and plainly intimates that he was now about to
put him upon a trial, in one particular instance, whether he would be
obedient or no, v. 1. And the making of this so expressly the trial of
his obedience did very much aggravate his disobedience. 1. He reminds
him of what God had done for him: \"The Lord sent me to anoint thee to
be a king. God gave thee thy power, and therefore he expects thou
shouldst use thy power for him. He put honour upon thee, and now thou
must study how to do him honour. He made thee king over Israel, and now
thou must plead Israel\'s cause and avenge their quarrels. Thou art
advanced to command Israel, but know that thou art a subject to the God
of Israel and must be commanded by him.\" Men\'s preferment, instead of
releasing them from their obedience to God, obliges them so much the
more to it. Samuel had himself been employed to anoint Saul, and
therefore was the fitter to be send with these orders to him. 2. He
tells him, in general, that, in consideration of this, whatever God
commanded him to do he was bound to do it: Now therefore hearken to the
voice of the Lord. Note, God\'s favours to us lay strong obligations
upon us to be obedient to him. This we must render, Ps. 116:12.

`II.` He appoints him a particular piece of service, in which he must now
show his obedience to God more than in any thing he had done yet. Samuel
premises God\'s authority to the command: Thus says the Lord of hosts,
the Lord of all hosts, of Israel\'s hosts. He also gives him a reason
for the command, that the severity he must use might not seem hard: I
remember that which Amalek did to Israel, v. 2. God had an ancient
quarrel with the Amalekites, for the injuries they did to his people
Israel when he brought them out of Egypt. We have the story, Ex. 17:8,
etc., and the crime is aggravated, Deu. 25:18. He basely smote the
hindmost of them, and feared not God. God then swore that he would have
war with Amalek from generation to generation, and that in process of
time he would utterly put out the remembrance of Amalek; this is the
work that Saul is now appointed to do (v. 3): \"Go and smite Amalek.
Israel is now strong, and the measure of the iniquity of Amalek is now
full; now go and make a full riddance of that devoted nation.\" He is
expressly commanded to kill and slay all before him, man and woman,
infant and suckling, and not spare them out of pity; also ox and sheep,
camel and ass, and not spare them out of covetousness. Note, 1. Injuries
done to God\'s Israel will certainly be reckoned for sooner or later,
especially the opposition given them when they are coming out of Egypt.
2. God often bears long with those that are marked for ruin. The
sentence passed is not executed speedily. 3. Though he bear long, he
will not bear always. The year of recompence for the controversy of
Israel will come at last. Though divine justice strikes slowly it
strikes surely. 4. The longer judgment is delayed many times the more
severe it is when it comes. 5. God chooses out instruments to do his
work that are fittest for it. This was bloody work, and therefore Saul
who was a rough and severe man must do it.

`III.` Saul hereupon musters his forces, and makes a descent upon the
country of Amalek. It was an immense army that he brought into the field
(v. 4): 200,000 footmen. When he came to engage the Philistines, and the
success was hazardous, he had but 600 attending him, ch. 13:15. But now
that he was to attack the Amalekites by express order from heaven, in
which he was sure of victory, he had thousands at his call. But,
whatever it was at other times, it was not now for the honour of Judah
that their forces were numbered by themselves, for their quota was
scandalously short (whatever was the reason), but a twentieth part of
the whole, for they were by 10,000, when the other ten tribes (for I
except Levi) brought into the field 200,000. The day of Judah\'s honour
drew near, but had not yet come. Saul numbered them in Telaim, which
signifies lambs. He numbered then like lambs (so the vulgar Latin),
numbered them by the paschal lambs (so the Chaldee), allowing ten to a
lamb, a way of numbering used by the Jews in the later times of their
nation. Saul drew all his forces to the city of Amalek, that city that
was their metropolis (v. 5), that he might provoke them to give him
battle.

`IV.` He gave friendly advice to the Kenites to separate themselves from
the Amalekites among whom they dwelt, while this execution was in doing,
v. 6. Herein he did prudently and piously, and, it is probable,
according to the direction Samuel gave him. The Kenites were of the
family and kindred of Jethro, Moses\'s father-in-law, a people that
dwelt in tents, which made it easy for them, upon every occasion, to
remove to other lands not appropriated. Many of them, at this time,
dwelt among the Amalekites, where, though they dwelt in tents, they were
fortified by nature, for they put their nest in a rock, being hardy
people that could live any where, and affected fastnesses, Num. 24:21.
Balaam had foretold that they should be wasted, Num. 24:22. However,
Saul must not waste them. But, 1. He acknowledges the kindness of their
ancestors to Israel, when they came out of Egypt. Jethro and his family
had been very helpful and serviceable to them in their passage through
the wilderness, had been to them instead of eyes, and this is remembered
to their posterity many ages after. Thus a good man leaves the divine
blessing for an inheritance to his children\'s children; those that come
after us may be reaping the benefit of our good works when we are in our
graves. God is not unrighteous to forget the kindnesses shown to his
people; but they shall be remembered another day, at furthest in the
great day, and recompensed in the resurrection of the just. I was
hungry, and you gave me meat. God\'s remembering the kindness of the
Kenites\' ancestors in favour to them, at the same time when he was
punishing the injuries done by the ancestors of the Amalekites, helped
to clear the righteousness of God in that dispensation. If he entail
favours, why may he not entail frowns? He espouses his people\'s cause,
so as to bless those that bless them; and therefore so as to curse those
that curse them, Num. 24:9; Gen. 12:3. They cannot themselves requite
the kindnesses nor avenge the injuries done them, but God will do both.
2. He desires them to remove their tents from among the Amalekites: Go,
depart, get you down from among them. When destroying judgments are
abroad God will take care to separate between the precious and the vile,
and to hide the meek of the earth in the day of his anger. It is
dangerous being found in the company of God\'s enemies, and it is our
duty and interest to come out from among them, lest we share in their
sins and plagues, Rev. 18:4. The Jews have a saying, Woe to the wicked
man and woe to his neighbour.

`V.` Saul prevailed against the Amalekites, for it was rather an execution
of condemned malefactors than a war with contending enemies. The issue
could not be dubious when the cause was just and the call so clear: He
smote them (v. 7), utterly destroyed them, v. 8. Now they paid dearly
for the sin of their ancestors. God sometimes lays up iniquity for the
children. They were idolaters, and were guilty of many other sins, for
which they deserved to fall under the wrath of God; yet, when God would
reckon with them, he fastened upon the sin of their ancestors in abusing
his Israel as the ground of his quarrel. Lord, How unsearchable are thy
judgments, yet how incontestable is thy righteousness!

`VI.` Yet he did his work by halves, v. 9. 1. He spared Agag, because he
was a king like himself, and perhaps in hope to get a great ransom for
him. 2. He spared the best of the cattle, and destroyed only the refuse,
that was good for little. Many of the people, we may suppose, made their
escape, and took their effects with them into other countries, and
therefore we read of Amalekites after this; but that could not be
helped. It was Saul\'s fault that he did not destroy such as came to his
hands and were in his power. That which was now destroyed was in effect
sacrificed to the justice of God, as the God to whom vengeance
belongeth; and for Saul to think the torn and the sick, the lame and the
lean, good enough for that, while he reserved for his own fields and his
own table the firstlings and the fat, was really to honour himself more
than God.

### Verses 10-23

Saul is here called to account by Samuel concerning the execution of his
commission against the Amalekites; and remarkable instances we are here
furnished with of the strictness of the justice of God and the treachery
and deceitfulness of the heart of man. We are here told,

`I.` What passed between God and Samuel, in secret, upon this occasion, v.
10, 11. 1. God determines Saul\'s rejection, and acquaints Samuel with
it: It repenteth me that I have set up Saul to be king. Repentance in
God is not, as it is in us, a change of his mind, but a change of his
method or dispensation. He does not alter his will, but wills an
alteration. The change was in Saul: He has turned back from following
me; this construction God put upon the partiality of his obedience, and
the prevalency of his covetousness. And hereby he did himself make God
his enemy. God repented that he had given Saul the kingdom and the
honour and power that belonged to it: but he never repented that he had
given any man wisdom and grace, and his fear and love; these gifts and
callings of God are without repentance. 2. Samuel laments and deprecates
it. It grieved Samuel that Saul had forfeited God\'s favour, and that
God had resolved to cast him off; and he cried unto the Lord all night,
spent a whole night in interceding for him, that this decree might not
go forth against him. When others were in their beds sleeping, he was
upon his knees praying and wrestling with God. He did not thus deprecate
his own exclusion from the government; nor was he secretly pleased, as
many a one would have been, that Saul, who succeeded him, was so soon
laid aside, but on the contrary prayed earnestly for his establishment,
so far was he from desiring that woeful day. The rejection of sinners is
the grief of good people; God delights not in their death, nor should
we.

`II.` What passed between Samuel and Saul in public. Samuel, being sent
of God to him with these heavy tidings, went, as Ezekiel, in bitterness
of soul, to meet him, perhaps according to an appointment when Saul went
forth on this expedition, for Saul had come to Gilgal (v. 12), the place
where he was made king (ch. 11:15), and were now he would have been
confirmed if he had approved himself well in the trial of his obedience.
But Samuel was informed that Saul had set up a triumphal arch, or some
monument of his victory, at Carmel, a city in the mountains of Judah,
seeking his own honour more than the honour of God, for he set up this
place (or hand, as the word is) for himself (he had more need to have
been repenting of his sin and making his peace with God than boasting of
his victory), and also that he had marched in great state to Gilgal, for
this seems to be intimated in the manner of expression: He has gone
about, and passed on, and gone down, with a great deal of pomp and
parade. There Samuel gave him the meeting, and,

`1.` Saul makes his boast to Samuel of his obedience, because that was
the thing by which he was now to signalize himself (v. 13): \"Blessed be
thou of the Lord, for thou sendest me upon a good errand, in which I
have had great success, and I have performed the commandment of the
Lord.\" It is very likely, if his conscience had now flown in his face
at this time and charged him with disobedience, he would not have been
so forward to proclaim his disobedience; for by this he hoped to prevent
Samuel\'s reproving him. Thus sinners think, by justifying themselves,
to escape being judged of the Lord; whereas the only way to do that is
by judging ourselves. Those that boast most of their religion may be
suspected of partiality and hypocrisy in it.

`2.` Samuel convicts him by a plain demonstration of his disobedience.
\"Hast thou performed the commandment of the Lord? What means then the
bleating of the sheep?\" v. 14. Saul would needs have it thought than
God Almighty was wonderfully beholden to him for the good service he had
done; but Samuel shows him that God was so far from being a debtor to
him that he had just cause of action against him, and produces for
evidence the bleating of the sheep, and the lowing of the oxen, which
perhaps Saul appointed to bring up the rear of his triumph, but Samuel
appears to them as witnesses against him. He needed not go far to
disprove his professions. The noise the cattle made (like the rust of
silver, Jam. 5:3) would be a witness against him. Note, It is no new
thing for the plausible professions and protestations of hypocrites to
be contradicted and disproved by the most plain and undeniable evidence.
Many boast of their obedience to the command of God; but what mean then
their indulgence of the flesh, their love of the world, their passion
and uncharitableness, and their neglect of holy duties, which witness
against them?

`3.` Saul insists upon his own justification against this charge, v. 15.
The fact he cannot deny; the sheep and oxen were brought from the
Amalekites. But, `(1.)` It was not his fault, for the people spared them;
as if they durst have done it without the express orders of Saul, when
they knew it was against the express orders of Samuel. Note, Those that
are willing to justify themselves are commonly very forward to condemn
others, and to lay the blame upon any rather than take it to themselves.
Sin is a brat that nobody cares to have laid at his doors. It is the
sorry subterfuge of an impenitent heart, that will not confess its
guilt, to lay the blame on those that were tempters, or partners, or
only followers in it. `(2.)` It was with a good intention: \"It was to
sacrifice to the Lord thy God. He is thy God, and thou wilt not be
against any thing that is done, as this is, for his honour.\" This was a
false plea, for both Saul and the people designed their own profit in
sparing the cattle. But, if it had been true, it would still have been
frivolous, for God hates robbery for burnt-offering. God appointed these
cattle to be sacrificed to him in the field, and therefore will give
those no thanks that bring them to be sacrificed at his altar; for he
will be served in his own way, and according to the rule he himself has
prescribed. Nor will a good intention justify a bad action.

`4.` Samuel overrules, or rather overlooks, his plea, and proceeds, in
God\'s name, to give judgment against him. He premises his authority.
What he was about to say was what the Lord had said to him (v. 16),
otherwise he would have been far from passing so severe a censure upon
him. Those who complain that their ministers are too harsh with them
should remember that, while they keep to the word of God, they are but
messengers, and must say as they are bidden, and therefore be willing,
as Saul himself here was, that they should say on. Samuel delivers his
message faithfully. `(1.)` He reminds Saul of the honour of God had done
him in making him king (v. 17), when he was little in his own sight. God
regarded the lowness of his state and rewarded the lowliness of his
spirit. Note, Those that are advanced to honour and wealth ought often
to remember their mean beginnings, that they may never think highly of
themselves, but always study to do great things for the God that had
advanced them. `(2.)` He lays before him the plainness of the orders he
was to execute (v. 18): The Lord sent thee on a journey; so easy was the
service, and so certain the success, that it was rather to be called a
journey than a war. The work was honourable, to destroy the sworn
enemies of God and Israel; and had he denied himself, and set aside the
consideration of his own profit so far as to have destroyed all that
belonged to Amalek, he would have been no loser by it at last, nor have
gone this warfare on his own charges. God would no doubt have made it up
to him, so that he should have no need of spoil. And therefore, `(3.)` He
shows him how inexcusable he was in aiming to make a profit of this
expedition, and to enrich himself by it (v. 19): \"Wherefore then didst
thou fly upon the spoil, and convert that to thy own use which was to
have been destroyed for God\'s honour?\" See what evil the love of money
is the root of; but see what is the sinfulness of sin, and that in it
which above any thing else makes it evil in the sight of the Lord. It is
disobedience: Thou didst not obey the voice of the Lord.

`5.` Saul repeats his vindication of himself, as that which, in defiance
of conviction, he resolved to abide by, v. 20, 21. He denies the charge
(v. 20): \"Yea, I have obeyed, I have done all I should do;\" for he had
done all which he thought he needed to do, so much wiser was he in his
own eyes than God himself. God bade him kill all, and yet he puts in
among the instances of his obedience that he brought Agag alive, which
he thought was as good as if he had killed him. Thus carnal deceitful
hearts think to excuse themselves from God\'s commandments with their
own equivalents. He insists upon it that he has utterly destroyed the
Amalekites themselves, which was the main thing intended; but, as to the
spoil, he owns it should have been utterly destroyed; so that he knew
his Lord\'s will, and was under no mistake about the command. But he
thought that would be wilful waste; the cattle of the Midianites was
taken for a prey in Moses\'s time (Num. 31:32, etc.), and why not the
cattle of the Amalekites now? Better it should be prey to the Israelites
than to the fowls of the air and the wild beasts; and therefore he
connived at the people\'s carrying it away. But it was their doing and
not his; and, besides, it was for sacrifice to the Lord here at Gilgal,
whither they were now bringing them. See what a hard thing it is to
convince the children of disobedience of their sin and to strip them of
their fig-leaves.

`6.` Samuel gives a full answer to his apology, since he did insist upon
it, v. 22, 23. He appeals to his own conscience: Has the Lord as great
delight in sacrifices as in obedience? Though Saul was not a man of any
great acquaintance with religion, yet he could not but know this, `(1.)`
That nothing is so pleasing to God as obedience, no, not sacrifice and
offering, and the fat of rams. See here what we should seek and aim at
in all the exercises of religion, even acceptance with God, that he may
delight in what we do. If God be well pleased with us and our services,
we are happy, we have gained our point, but otherwise to what purpose is
it? Isa. 1:11. Now here we are plainly told that humble, sincere, and
conscientious obedience to the will of God, is more pleasing and
acceptable to him than all burnt-offerings and sacrifices. A careful
conformity to moral precepts recommends us to God more than all
ceremonial observances, Mic. 6:6-8; Hos. 6:6. Obedience is enjoyed by
the eternal law of nature, but sacrifice only by a positive law.
Obedience was the law of innocency, but sacrifice supposes sin come into
the world, and is but a feeble attempt to take that away which obedience
would have prevented. God is more glorified and self more denied by
obedience than by sacrifice. It is much easier to bring a bullock or
lamb to be burnt upon the altar than to bring every high thought into
obedience to God and the will subject to his will. Obedience is the
glory of angels (Ps. 103:20), and it will be ours. `(2.)` That nothing is
so provoking to God as disobedience, setting up our wills in competition
with his. This is here called rebellion and stubbornness, and is said to
be as bad as witchcraft and idolatry, v. 23. It is as bad to set up
other gods as to live in disobedience to the true God. Those that are
governed by their own corrupt inclinations, in opposition to the command
of God, do, in effect, consult the teraphim (as the word here is for
idolatry) or the diviners. It was disobedience that made us all sinners
(Rom. 5:19), and this is the malignity of sin, that it is the
transgression of the law, and consequently it is enmity to God, Rom.
8:7. Saul was a king, but if he disobey the command of God, his royal
dignity and power will not excuse him from the guilt of rebellion and
stubbornness. It is not the rebellion of the people against their
prince, but of a prince against God, that this text speaks of.

`7.` He reads his doom: in short, \"Because thou has rejected the word of
the Lord, hast despised it (so the Chaldee), hast made nothing of it (so
the Septuagint), hast cast off the government of it, therefore he has
rejected thee, despised and made nothing of thee, but cast thee off from
being king. He that made thee king has determined to unmake thee
again.\" Those are unfit and unworthy to rule over men who are not
willing that God should rule over them.

### Verses 24-31

Saul is at length brought to put himself into the dress of the penitent;
but it is too evident that he only acts the part of a penitent, and is
not one indeed. Observe,

`I.` How poorly he expressed his repentance. It was with much ado that he
was made sensible of his fault, and not till he was threatened with
being deposed. This touched him in a tender part. Then he began to
relent, and not till then. When Samuel told him he was rejected from
being king, then he said, I have sinned, v. 24. His confession was not
free nor ingenuous, but extorted by the rack, and forced from him. We
observe here several bad signs of the hypocrisy of his repentance, and
that it came short even of Ahab\'s. 1. He made his application to Samuel
only, and seemed most solicitous to stand right in his opinion and to
gain his favour. He makes a little god of him, only to preserve his
reputation with the people, because they all knew Samuel to be a
prophet, and the man that had been the instrument of his preferment.
Thinking it would please Samuel, and be a sort of bribe to him, he puts
it into his confession: I have transgressed the commandment of the Lord
and thy word; as if he had been in God\'s stead, v. 24. David, though
convinced by the ministry of Nathan, yet, in his confession, has his eye
to God alone, not to Nathan. Ps. 51:4 Against thee only have I sinned.
But Saul, ignorantly enough, confesses his sin as a transgression of
Samuel\'s word; whereas his word was no other than a declaration of the
commandment of the Lord. He also applies to Samuel for forgiveness (v.
25): I pray thee, pardon my sin; as if any could forgive sin but God
only. Those wretchedly deceive themselves who, when they have fallen
into scandalous sin, think it enough to make their peace with the church
and their ministers, by the show and plausible profession of repentance,
without taking care to make their peace with God by the sincerity of it.
The most charitable construction we can put upon this of Saul is to
suppose that he looked upon Samuel as a sort of mediator between him and
God, and intended an address to God in his application to him. However,
it was very weak. 2. He excused his fault even in the confession of it,
and that is never the fashion of a true penitent (v. 24): I did it
because I feared the people, and obeyed their voice. We have reason
enough to think that it was purely his own doing and not the people\'s;
however, if they were forward to do it, it is plain, by what we have
read before, that he knew how to keep up his authority among them and
did not stand in any awe of them. So that the excuse was false and
frivolous; whatever he pretended, he did not really fear the people. But
it is common for sinners, in excusing their faults, to plead the
thoughts and workings of their own minds, because those are things
which, how groundless soever, no man can disprove; but they forget that
God searchest the heart. 3. All his care was to save his credit, and
preserve his interest in the people, lest they should revolt from him,
or at least despise him. Therefore he courts Samuel with so much
earnestness (v. 25) to turn again with him, and assist in a public
thanksgiving for the victory. Very importunate he was in this matter
when he laid hold on the skirt of his mantle to detain him (v. 27), not
that he cared for Samuel, but he feared that if Samuel forsook him the
people would do so too. Many seem zealously affected to good ministers
and good people only for the sake of their own interest and reputation,
while in heart they hate them. But his expression was very gross when he
said (v. 30), I have sinned, yet honour me, I pray thee, before my
people. Is this the language of a penitent? No, but the contrary: \"I
have sinned, shame me now, for to me belongs shame, and no man can
loathe me so much as I loathe myself.\" Yet how often do we meet with
the copies of this hypocrisy of Saul! It is very common for those who
are convicted of sin to show themselves very solicitous to be honoured
before the people. Whereas he that has lost the honour of an innocent
can pretend to no other than that of a penitent, and it is the honour of
a penitent to take shame to himself.

`II.` How little he got by these thin shows of repentance. What point did
he gain by them? 1. Samuel repeated the sentence passed upon him, so far
was he from giving any hopes of the repeal of it, v. 26, the same with
v. 23. He that covers his sins shall never prosper, Prov. 28:13. Samuel
refused to turn back with him, but turned about to go away, v. 27. As
the thing appeared to him upon the first view, he thought it altogether
unfit for him so far to countenance one whom God had rejected as to join
with him in giving thanks to God for a victory which was made to serve
rather Saul\'s covetousness than God\'s glory. Yet afterwards he did
turn again with him (v. 31), upon further thoughts, and probably by
divine direction, either to prevent a mutiny among the people or perhaps
not to do honour to Saul (for, though Saul worshipped the Lord, v. 31,
it is not said Samuel presided in that worship), but to do justice on
Agag, v. 32. 2. He illustrated the sentence by a sign, which Saul
himself, by his rudeness, gave occasion for. When Samuel was turning
from him he tore his clothes to detain him (v. 27), so loth was he to
part with the prophet; but Samuel put a construction upon this accident
which none but a prophet could do. He made it to signify the rending of
the kingdom from him (v. 28), and that, like this, was his own doing.
\"He hath rent it from thee, and given it to a neighbour better than
thou,\" namely, to David, who afterwards, upon occasion, cut off the
skirt of Saul\'s robe (1 Sa. 24:4), upon which Saul said (1 Sa. 24:20),
I know that thou shalt surely be king, perhaps remembering this sign,
the tearing of the skirt of Samuel\'s mantle. 3. He ratified it by a
solemn declaration of its being irreversible (v. 29): The Strength of
Israel will not lie. The Eternity or Victory of Israel, so some read it;
the holy One, so the Arabic; the most noble One, so the Syriac; the
triumphant King of Israel, so bishop Patrick. \"He is determined to
depose thee, and he will not change his purpose. He is not a man that
should repent.\" Men are fickle and alter their minds, feeble and cannot
effect their purposes; something happens which they could not foresee,
by which their measures are broken. But with God it is not so. God has
sometimes repented of the evil which he thought to have done, repentance
was hidden from Saul, and therefore hidden from God\'s eyes.

### Verses 32-35

Samuel, as a prophet, is here set over kings, Jer. 1:10.

`I.` He destroys king Agag, doubtless by such special direction from
heaven as none now can pretend to. He hewed Agag in pieces. Some think
he only ordered it to be done; or perhaps he did it with his own hands,
as a sacrifice to God\'s injured justice (v. 33), and sacrifices used to
be cut in pieces. Now observe in this,

`1.` How Agag\'s present vain hopes were frustrated: He came delicately,
in a stately manner, to show that he was a king, and therefore to be
treated with respect, or in a soft effeminate manner, as one never used
to hardship, that could not set the sole of his foot to the ground for
tenderness and delicacy (Deu. 28:56), to move compassion: and he said,
\"Surely, now that the heat of the battle is over, the bitterness of
death is past, v. 32. Having escaped the sword of Saul,\" that man of
war, he thought he was in no danger from Samuel, and old prophet, a man
of peace. Note, `(1.)` There is bitterness in death, it is terrible to
nature. Surely death is bitter, so divers versions read those words of
Agag; as the Septuagint read the former clause, He came trembling. Death
will dismay the stoutest heart. `(2.)` Many think the bitterness of death
is past when it is not so; they put that evil day far from them which is
very near. True believers may, through grace, say this, upon good
grounds, though death be not past, the bitterness of it is. O death!
where is thy sting?

`2.` How his former wicked practices were now punished. Samuel calls him
to account, not only for the sins of his ancestors, but his own sins:
Thy sword has made women childless, v. 33. He trod in the steps of his
ancestors\' cruelty, and those under him, it is likely, did the same;
justly therefore is all the righteous blood shed by Amalek required of
this generation, Mt. 23:36. Agag, that was delicate and luxurious
himself, was cruel and barbarous to others. It is commonly so: those who
are indulgent in their appetites are not less indulgent of their
passions. But blood will be reckoned for; even kings must account to the
King of kings for the guiltless blood they shed or cause to be shed. It
was that crime of king Manasseh which the Lord would not pardon, 2 Ki.
24:4. See Rev. 13:10.

`II.` He deserts king Saul, takes leave of him (v. 34), and never came
any more to see him (v. 35), to advise or assist him in any of his
affairs, because Saul did not desire his company nor would he be advised
by him. He looked upon him as rejected of God, and therefore he forsook
him. Though he might sometimes see him accidentally (as ch. 19:24), yet
he never came to see him out of kindness or respect. Yet he mourned for
Saul, thinking it a very lamentable thing that a man who stood so fair
for great things should ruin himself so foolishly. He mourned for the
bad state of the country, to which Saul was likely to have been so great
a blessing, but now would prove a curse and a plague. He mourned for his
everlasting state, having no hopes of bringing him to repentance. When
he wept for him, it is likely, he made supplication, but the Lord had
repented that he had made Saul king, and resolved to undo that work of
his, so that Samuel\'s prayers prevailed not for him. Observe, We must
mourn for the rejection of sinners, 1. Though we withdraw from them, and
dare not converse familiarly with them. Thus the prophet determines to
leave his people and go from them, and yet to weep day and night for
them, Jer. 9:1, 2. 2. Though they do not mourn for themselves. Saul
seems unconcerned at the tokens of God\'s displeasure which he lay
under, and yet Samuel mourns day and night for him. Jerusalem was secure
when Christ wept over it.
