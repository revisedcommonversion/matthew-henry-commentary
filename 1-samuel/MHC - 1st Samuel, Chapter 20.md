1st Samuel, Chapter 20
======================

Commentary
----------

David, having several times narrowly escaped Saul\'s fury, begins to
consider at last whether it may not be necessary for him to retire into
the country and to take up arms in his own defence. But he will not do
so daring a thing without consulting his faithful friend Jonathan; how
he did this, and what passed between them, we have an account in this
chapter, where we have as surprising instances of supernatural love as
we had in the chapter before of unnatural hatred. `I.` David complains to
Jonathan of his present distress, and engages him to be his friend (v.
1-8). `II.` Jonathan faithfully promises to get and give him intelligence
how his father stood affected to him, and renews the covenant of
friendship with him (v. 9-23). `III.` Jonathan, upon trial, finds, to his
grief, that his father was implacably enraged against David (v. 24-34).
IV. He gives David notice of this, according to the appointment between
them (v. 35-42).

### Verses 1-8

Here, `I.` David makes a representation to Jonathan of his present
troubles. While Saul lay bound by his trance at Naioth David escaped to
the court, and got to speak with Jonathan. And it was happy for him that
he had such a friend at court, when he had such an enemy on the throne.
If there be those that hate and despise us, let us not be disturbed at
that, for there are those also that love and respect us. God hath set
the one over against the other, and so must we. Jonathan was a friend
that loved at all times, loved David as well now in his distress, and
bade him as welcome into his arms, as he had done when he was in his
triumph (ch. 18:1), and he was a brother that was born for adversity,
Prov. 17:17. Now, 1. David appeals to Jonathan himself concerning his
innocency, and he needed not say much to him for the proof of it, only
he desired him that if he knew of any just offence he had given his
father he would tell him, that he might humble himself and beg his
pardon: What have I done? v. 1. 2. He endeavors to convince him that,
notwithstanding his innocency, Saul sought his life. Jonathan, from a
principal of filial respect to his father, was very loth to believe that
he designed or would ever do so wicked a thing, v. 2. He the rather
hoped so because he knew nothing of any such design, and he had usually
been made privy to all his counsels. Jonathan, as became a dutiful son,
endeavored to cover his father\'s shame, as far as was consistent with
justice and fidelity to David. Charity is not forward to think evil of
any, especially of a parent, 1 Co. 13:5. David therefore gives him the
assurance of an oath concerning his own danger, swears the peace upon
Saul, that he was in fear of his life by him: \"As the Lord liveth, than
which nothing more sure in itself, and as thy soul liveth, than which
nothing more certain to thee, whatever thou thinkest, there is but a
step between me and death,\" v. 3. And, as for Saul\'s concealing it
from Jonathan, it was easy to account for that; he knew the friendship
between him and David, and therefore, though in other things he advised
with him, yet not in that. None more fit than Jonathan to serve him in
every design that was just and honourable, but he knew him to be a man
of more virtue than to be his confidant in so base a design as the
murder of David.

`II.` Jonathan generously offers him his service (v. 4): Whatsoever thou
desirest, he needed not insert the proviso of lawful and honest (for he
knew David too well to think he would ask any thing that was otherwise),
I will even do it for thee. This is true friendship. Thus Christ
testifies his love to us: Ask, and it shall be done for you; and we must
testify ours to him by keeping his commandments.

`III.` David only desires him to satisfy himself, and then to satisfy him
whether Saul did really design his death or no. Perhaps David proposed
this more for Jonathan\'s conviction than his own, for he himself was
well satisfied. 1. The method of trial he proposed was very natural, and
would certainly discover how Saul stood affected to him. The two next
days Saul was to dine publicly, upon occasion of the solemnities of the
new moon, when extraordinary sacrifices were offered and feasts made
upon the sacrifices. Saul was rejected of God, and the Spirit of the
Lord had departed from him, yet he kept up his observance of the holy
feasts. There may be the remains of external devotion where there is
nothing but the ruins of real virtue. At these solemn feasts Saul had
either all his children to sit with him, and David had a seat as one of
them, or all his great officers, and David had a seat as one of them.
However it was, David resolved his seat should be empty (and that it
never used to be at a sacred feast) those two days (v. 5), and he would
abscond till the solemnity was over, and put it upon this issue: if Saul
admitted an excuse for his absence, and dispensed with it, he would
conclude he had changed his mind and was reconciled to him; but if he
resented it, and was put into a passion by it, it was easy to conclude
he designed him a mischief, since it was certain he did not love him so
well as to desire his presence for any other end than that he might have
an opportunity to do him a mischief, v. 7. 2. The excuse he desired
Jonathan to make for his absence, we have reason to think, was true,
that he was invited by his elder brother to Bethlehem, his own city, to
celebrate this new moon with his relations there, because, besides the
monthly solemnity in which they held communion with all Israel, they had
now a yearly sacrifice, and a holy feast upon it, for all the family, v.
6. They kept a day of thanksgiving in their family for the comforts they
enjoyed, and of prayer for the continuance of them. By this it appears
that the family David was of was a very religious family, a house that
had a church in it. 3. The arguments he used with Jonathan to persuade
him to do this kindness for him were very pressing, v. 8. `(1.)` That he
had entered into a league of friendship with him, and it was Jonathan\'s
own proposal: Thou hast brought thy servant into a covenant of the Lord
with thee. `(2.)` That he would by no means urge him to espouse his cause
if he was not sure that it was a righteous cause: \"If there be iniquity
in me, I am so far from desiring or expecting that the covenant between
us should bind thee to be a confederate with me in that iniquity that I
freely release thee from it, and wish that my hand may be first upon me:
Slay me thyself.\" No honest man will urge his friend to do a dishonest
thing for his sake.

### Verses 9-23

Here, `I.` Jonathan protests his fidelity to David in his distress.
Notwithstanding the strong confidence David had in Jonathan, yet,
because he might have some reason to fear that his father\'s influence,
and his own interest, should make him warp, or grow cool towards him,
Jonathan thought it requisite solemnly to renew the professions of his
friendship to him (v. 9): \"Far be it from thee to think that I suspect
thee of any crime for which I should either slay thee myself or deliver
thee to my father; no, if thou hast any jealousy of that, Come let us go
into the field (v. 11), and talk it over more fully.\" He did not
challenge him to the field to fight him for an affront, but to fix him
in his friendship. He faithfully promised him that he would let him know
how, upon trial, he found his father affected towards him, and would
make the matter neither better nor worse than it was. \"If there be good
towards thee, I will show it thee, that thou mayest be easy (v. 12), if
evil, I will send thee away, that thou mayest be safe\" (v. 13); and
thus he would help to deliver him from the evil if it were real and from
the fear of evil if it were but imaginary. For the confirmation of his
promise he appeals to God, 1. As a witness (v. 12): \"O Lord God of
Israel, thou knowest I mean sincerely, and think as I speak.\" The
strength of his passion made the manner of his speaking concise and
abrupt. 2. As a judge: \"The Lord do so and much more to Jonathan (v.
13), if I speak deceitfully, or break my word with my friend.\" He
expressed himself thus solemnly that David might be abundantly assured
of his sincerity. And thus God has confirmed his promises to us, that we
might have strong consolation, Heb. 6:17, 18. Jonathan adds to his
protestations his hearty prayers: \"The Lord be with thee, to protect
and prosper thee, as he has been formerly with my father, though now he
has withdrawn.\" Thus he imitates his belief that David would be in his
father\'s place, and his good wishes that he might prosper in it better
than his father now did.

`II.` He provides for the entail of the covenant of friendship with David
upon his posterity, v. 14-16. He engages David to be a friend to his
family when he was gone (v. 15): Thou shalt promise that thou wilt not
cut off thy kindness from my house for ever. This he spoke from a
natural affection he had to his children, whom he desired it might go
well with after his decease, and for whose future welfare he desired to
improve his present interest. It also intimates his firm belief of
David\'s advancement, and that it would be in the power of his hand to
do a kindness or unkindness to his seed; for, in process of time, the
Lord would cut off his enemies, Saul himself was not expected; then \"Do
not thou cut off thy kindness from my house, nor revenge my father\'s
wrongs upon my children.\" The house of David must likewise be bound to
the house of Jonathan from generation to generation; he made a covenant
(v. 16) with the house of David. Note, True friends cannot but covet to
transmit to theirs after them their mutual affections. Thy own friend,
and thy father\'s friend, forsake not. This kindness, 1. He calls the
kindness of the Lord, because it is such kindness as God shows to those
he takes into covenant with himself; for he is a God to them and to
their seed; they are beloved for the fathers\' sakes. 2. He secures it
by an imprecation (v. 16): The Lord require it at the hand of David\'s
seed (for of David himself he had no suspicion) if they prove so far
David\'s enemies as to deal wrongfully with the posterity of Jonathan,
David\'s friend. He feared lest David, or some of his, should hereafter
be tempted, for the clearing and confirming of their title to the
throne, to do by his seed as Abimelech had done by the sons of Gideon
(Jdg. 9:5), and this he would effectually prevent; but the reason given
(v. 17) why Jonathan was so earnest to have the friendship entailed is
purely generous, and has nothing of self in it; it was because he loved
him as he loved his own soul, and therefore desired that he and his
might be beloved by him. David, though now in disgrace at court and in
distress, was as amiable in the eyes of Jonathan as ever he had been,
and he loved him never the less for his father\'s hating him, so pure
were the principles on which his friendship was built. Having himself
sworn to David, he caused David to swear to him, and (as we read it) to
swear again, which David consented to (for he that bears an honest mind
does not startle at assurances), to swear by his love to him, which he
looked upon as a sacred thing. Jonathan\'s heart was so much upon it
that, when they parted this time, he concluded with a solemn appeal to
God: The Lord be between me and thee for ever (v. 23), that is, \"God
himself be judge between us and our families for ever, if on either side
this league of friendship be violated.\" It was in remembrance of this
covenant that David was kind to Mephibosheth, 2 Sa. 9:7; 21:7. It will
be a kindness to ourselves and ours to secure an interest in those whom
God favours and to make his friends ours.

`III.` He settles the method of intelligence, and by what signs and
tokens he would give him notice how his father stood affected towards
him. David would be missed the first day, or at least the second day, of
the new moon, and would be enquired after, v. 18. On the third day, by
which time he would have returned from Bethlehem, he must be at such a
place (v. 19), and Jonathan would come towards that place with his bow
and arrows to shoot for diversion (v. 20), would send his lad to fetch
his arrows, and, if they were shot short of the lad, David must take it
for a signal of safety, and not be afraid to show his head (v. 21); but,
if he shot beyond the lad, it was a signal of danger, and he must shift
for his safety, v. 22. This expedient he fixed lest he should not have
the opportunity, which yet it proved he had, of talking with David, and
making the report by word of mouth.

### Verses 24-34

Jonathan is here effectually convinced of that which he was so loth to
believe, that his father had an implacable enmity to David, and would
certainly be the death of him if it were in his power; and he had like
to have paid very dearly himself for the conviction.

`I.` David is missed from the feast on the first day, but nothing is said
of him. The king sat upon his seat, to feast upon the peace-offerings as
at other times (v. 25), and yet had his heart as full of envy and malice
against David as it could hold. He should first have been reconciled to
him, and then have come and offered his gift; but, instead of that, he
hoped, at this feast, to drink the blood of David. What an abomination
was that sacrifice which was brought with such a wicked mind as this!
Prov. 21:27. When the king came to take his seat Jonathan arose, in
reverence to him both as a father and as his sovereign; every one knew
his place, but David\'s was empty. It did not use to be so. None more
content than he in attending holy duties; nor had he been absent now but
that he must have come at the peril of his life; self-preservation
obliged him to withdraw. In imminent peril present opportunities may be
waived, nay, we ought not to throw ourselves into the mouth of danger.
Christ himself absconded often, till he knew that his hour had come. But
that day Saul took no notice that he missed David, but said within
himself, \"Surely he is not clean, v. 26. Some ceremonial pollution has
befallen him, which forbids him to eat of the holy things till he has
washed his clothes, and bathed his flesh in water, and been unclean
until the evening.\" Saul knew what conscience David made of the law,
and that he would rather keep away from the holy feast than come in his
uncleanness. Blessed be God, no uncleanness is now a restraint upon us,
but what we may by faith and repentance be washed from in the fountain
opened, Ps. 26:6.

`II.` He is enquired for the second day, v. 27. Saul asked Jonathan, who
he knew was his confidant, Wherefore cometh not the son of Jesse to
meat? He was his own son by marriage, but he calls him in disdain, the
son of Jesse. He asks for him as if he were not pleased that he should
be absent from a religious feast; and so it should be example to masters
of families to see to it that those under their charge be not absent
from the worship of God, either in public or in the family. It is a bad
thing for us, except in case of necessity, to omit an opportunity of
statedly attending on God in solemn ordinances. Thomas lost a sight of
Christ by being once absent from a meeting of the disciples. But that
which displeased Saul was that hereby he missed the opportunity he
expected of doing David a mischief.

`III.` Jonathan makes his excuse, v. 28, 29. 1. That he was absent upon a
good occasion, keeping the feast in another place, though not here, sent
for by his elder brother, who was now more respectful to him than he had
been (ch. 17:28), and that he had gone to pay his respects to his
relations, for the keeping up of brotherly love; and no master would
deny a servant liberty to do that in due time. He pleads, 2. That he did
not go without leave humbly asked and obtained from Jonathan, who, as
his superior officer, was proper to be applied to for it. Thus he
represents David as not wanting in any instance of respect and duty to
the government.

`IV.` Saul hereupon breaks out into a most extravagant passion, and rages
like a lion disappointed of his prey. David was out of his reach, but he
falls upon Jonathan for his sake (v. 30, 31), gives him base language,
not fit for a gentleman, a prince, to give to any man, especially his
own son, heir apparent to his crown, a son that served him, the greatest
stay and ornament of his family, before a great deal of company, at a
feast, when all should be in good humour, at a sacred feast, by which
all irregular passions should be mortified and subdued; yet he does in
effect call him, 1. A bastard: Thou son of the perverse rebellious
woman; that is, according to the foolish filthy language of men\'s
brutish passion now a day, \"Thou son of a whore.\" He tells him he was
born to the confusion of his mother, that is, he had given the world
cause to suspect that he was not the legitimate son of Saul, because he
loved him whom Saul hated and supported him who would be the destruction
of their family. 2. A traitor: Thou son of a perverse rebellion (so the
word is), that is, \"thou perverse rebel.\" At other times he reckoned
no counsellor or commander that he had more trusty and well-beloved than
Jonathan; yet now in this passion he represents him as dangerous to his
crown and life. 3. A fool: Thou hast chosen the son of Jesse for thy
friend to thy own confusion, for while he lives thou shalt never be
established. Jonathan indeed did wisely and well for himself and family
to secure an interest in David, whom Heaven had destined to the throne,
yet, for this, he is branded as most impolitic. It is good taking God\'s
people for our people and going with those that have him with them. It
will prove to our advantage at last, however for the present it may be
thought a disparagement, and a prejudice to our secular interest. It is
probable Saul knew that David was anointed to the kingdom by the same
hand that anointed him, and then not Jonathan, but himself, was the
fool, to think to defeat the counsels of God. Yet nothing will serve him
but David must die, and Jonathan must fetch him to execution. See how
ill Saul\'s passion looks, and let it warn us against the indulgence of
any thing like it in ourselves. Anger is madness, and he that hates his
brother is a murderer.

`V.` Jonathan is sorely grieved and put into disorder by his father\'s
barbarous passion, and the more because he had hoped better things, v.
2. He was troubled for his father, that he should be such a brute,
troubled for his friend, whom he knew to be a friend of God, that he
should be so basely abused; he was grieved for David (v. 34), and
troubled for himself too, because his father had done him shame, and,
though most unjustly, yet he must submit to it. One would pity Jonathan
to see how he was put, 1. Into the peril of sin. Much ado that wise and
good man had to keep his temper, upon such a provocation as this. His
father\'s reflections upon himself made no return to; it becomes
inferiors to bear with meekness and silence the contempts put upon them
in wrath and passion. When thou art the anvil lie thou still. But his
dooming David to die he could not bear: to that he replied with some
heat (v. 32), Wherefore shall he be slain? What has he done? Generous
spirits can much more easily bear to be abused themselves than to hear
their friends abused. 2. Into the peril of death. Saul was now so
outrageous that he threw his javelin at Jonathan, v. 33. He seemed to be
in great care (v. 31) than Jonathan should be established in his
kingdom, and yet now he himself aims at his life. What fools, what
savage beasts and worse does anger make men! How necessary it is to put
a hook in its nose and a bridle in its jaws! Jonathan was fully
satisfied that evil was determined against David, which put him out of
frame exceedingly: he rose from table, thinking it high time when his
life was struck at, and would eat no meat, for they were not to eat of
the holy things in their mourning. All the guests, we may suppose, were
discomposed, and the mirth of the feast was spoiled. He that is cruel
troubles his own flesh, Prov. 11:17.

### Verses 35-42

Here is, 1. Jonathan\'s faithful performance of his promise to give
David notice of the success of his dangerous experiment. He went at the
time and to the place appointed (v. 35), within sight of which he knew
David lay hid, sent his footboy to fetch his arrows, which he would
shoot at random (v. 36), and gave David the fatal signal by shooting an
arrow beyond the lad (v. 37): Is not the arrow beyond thee? That word
\[beyond\] David knew the meaning of better than the lad. Jonathan
dismissed the lad, who knew nothing of the matter, and, finding the
coast clear and no danger of a discovery, he presumed upon one minute\'s
personal conversation with David after he had bidden him flee for his
life. 2. The most sorrowful parting of these two friends, who, for aught
that appears, never came together again but once, and that was by
stealth in a wood, ch. 23:16. `(1.)` David addressed himself to Jonathan
with the reverence of a servant rather than the freedom of a friend: He
fell on his face to the ground, and bowed himself three times, as one
deeply sensible of his obligations to him for the good services he had
done him. `(2.)` They took leave of each other with the greatest affection
imaginable, with kisses and tears; they wept on each other\'s neck till
David exceeded, v. 41. The separation of two such faithful friends was
equally grievous to them both, but David\'s case was the more
deplorable; for, when Jonathan was returning to his family and friends,
David was leaving all his comforts, even those of God\'s sanctuary, and
therefore his grief exceeded Jonathan\'s, or perhaps it was because his
temper was more tender and his passions were stronger. `(3.)` They
referred themselves to the covenant of friendship that was between them,
both of them comforting themselves with this in this mournful
separation: \"We have sworn both of us in the name of the Lord, for
ourselves and our heirs, that we and they will be faithful and kind to
each other from generation to generation.\" Thus, while we are at home
in the body and absent from the Lord, this is our comfort, that he has
made with us an everlasting covenant.
