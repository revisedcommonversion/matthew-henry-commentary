1st Samuel, Chapter 19
======================

Commentary
----------

Immediately after David\'s marriage, which one would have hoped would
secure him Saul\'s affection, we find his troubles coming upon him
faster than ever and Saul\'s enmity to him the cause of all. His death
was vowed, and four fair escapes of his from the hurtful sword of Saul
we have an account of in this chapter: the first by the prudent
mediation of Jonathan (v. 1-7), the second by his own quickness (v.
8-10), the third by Michal\'s fidelity (v. 11-17), the fourth by
Samuel\'s protection, and a change, for the present, wrought upon Saul
(v. 18-24). Thus God has many ways of preserving his people. Providence
is never at a loss.

### Verses 1-7

Saul and Jonathan appear here in their different characters, with
reference to David.

`I.` Never was enemy so unreasonably cruel as Saul. He spoke to his son
and all his servants that they should kill David, v. 1. His projects to
take him off had failed, and therefore he proclaims him an out-law, and
charges all about him, upon their allegiance, to take the first
opportunity to kill David. It is strange that he was not ashamed thus to
avow his malice when he could give no reason for it, and that knowing
all his servants loved David (for so he had said himself, ch. 18:22), he
was not afraid of provoking them to rebel by this bloody order. Either
malice was not then so politic, or justice was not so corrupted as it
has been since, or else Saul would have had him indicted, and have
suborned witnesses to swear treason against him, and so have had him
taken off, as Naboth was, by colour of law. But there is least danger
from this undisguised malice. It was strange that he who knew how well
Jonathan loved him should expect him to kill him; but he thought that
because he was heir to the crown he must needs be as envious at David as
himself was. And Providence ordered it thus that he might befriend
David\'s safety.

`II.` Never was friend so surprisingly kind as Jonathan. A friend in need
is a friend indeed. Such a one Jonathan was to David. He not only
continued to delight much in him, though David\'s glory eclipsed his,
but bravely appeared for him now that the stream ran so strongly against
him.

`1.` He took care for his present security by letting him know his danger
(v. 2): \"Take heed to thyself, and keep out of harm\'s way.\" Jonathan
knew not but that some of the servants might be either so obsequious to
Saul or so envious at David as to put the orders in execution which Saul
had given, if they could light on David.

`2.` He took pains to pacify his father and reconcile him to David. The
next morning he ventured to commune with him concerning David (v. 3),
not that night, perhaps because he observed Saul to be drunk and not fit
to be spoken to, or because he hoped that, when he had slept upon it, he
would himself revoke the order, or because he could not have an
opportunity of speaking to him till morning.

`(1.)` His intercession for David was very prudent. It was managed with a
great deal of the meekness of wisdom; and he showed himself faithful to
his friends by speaking good of him, though he was in danger of
incurring his father\'s displeasure by it-a rare instance of valuable
friendship! He pleads, `[1.]` The good services David had done to the
public, and particularly to Saul: His work has been to thee-ward very
good, v. 4. Witness the relief he had given him against his distemper
with his harp, and his bold encounter with Goliath, that memorable
action, which did, in effect, save Saul\'s life and kingdom. He appeals
to himself concerning his: Thou thyself sawest it, and didst rejoice. In
that and other instances it appeared that David was a favourite of
heaven and a friend to Israel, as well as a good servant to Saul, for by
him the Lord wrought a great salvation for all Israel; so that to order
him to be slain was not only base ingratitude to so good a servant, but
a great affront to God and a great injury to the public. `[2.]` He
pleads his innocency. Though he had formerly done many good offices,
yet, if he had now been chargeable with any crimes, it would have been
another matter; but he has not sinned against thee (v. 1), his blood is
innocent (v. 5), and, if he be slain, it is without cause. And Jonathan
had therefore reason to protest against it because he could not entail
any thing upon his family more pernicious than the guilt of innocent
blood.

`(2.)` His intercession, being thus prudent, was prevalent. God inclined
the heart of Saul to hearken to the voice of Jonathan. Note, We must be
willing to hear reason, and to take all reproofs and good advice even
from our inferiors, parents from their own children. How forcible are
right words! Saul was, for the present, so far convinced of the
unreasonableness of his enmity to David that, `[1.]` He recalled the
bloody warrant for his execution (v. 6): As the Lord liveth, he shall
not be slain. Whether Saul swore here with due solemnity or no does not
appear; perhaps he did, and the matter was of such moment as to deserve
it and of such uncertainty as to need it. But at other times Saul swore
rashly and profanely, which made the sincerity of this oath justly
questionable; for it may be feared that those who can so far jest with
an oath as to make a by-word of it, and prostitute it to a trifle, have
not such a due sense of the obligation of it but that, to serve a turn,
they will prostitute it to a lie. Some suspect that Saul said and swore
this with a malicious design to bring David within his reach again,
intending to take the first opportunity to slay him. But, as bad as Saul
was, we can scarcely think so ill of him; and therefore we suppose that
he spoke as he thought for the present, but the convictions soon wore
off and his corruptions prevailed and triumphed over them. `[2.]` He
renewed the grant of his place at court. Jonathan brought him to Saul,
and he was in his presence as in times past (v. 7), hoping that now the
storm was over, and that his friend Jonathan would be instrumental to
keep his father always in this good mind.

### Verses 8-10

Here `I.` David continues his good services to his king and country.
Though Saul had requited him evil for good, and even his usefulness was
the very thing for which Saul envied him, yet he did not therefore
retire in sullenness and decline public service. Those that are ill paid
for doing good, yet must not be weary of well doing, remembering what a
bountiful benefactor our heavenly Father is, even to the froward and
unthankful. Notwithstanding the many affronts Saul had given to David,
yet we find him, 1. As bold as ever in using his sword for the service
of his country, v. 8. The war broke out again with the Philistines,
which gave David occasion again to signalize himself. It was a great
deal of bravery that he charged them; and he came off victorious,
slaying many and putting the rest to flight. 2. As cheerful as ever in
using his harp for the service of the prince. When Saul was disturbed
with his former fits of melancholy David played with his hand, v. 9. He
might have pleaded that this was a piece of service now below him; but a
humble man will think nothing below him by which he may do good. He
might have objected the danger he was in the last time he performed this
service for Saul, ch. 18:10. But he had learned to render good for evil,
and to trust God with his safety in the way of his duty. See how David
was affected when his enemy was sick (Ps. 35:13, 14), which perhaps
refers to Saul\'s sickness.

`II.` Saul continues his malice against David. He that but the other day
had sworn by his Maker that David should not be slain now endeavors to
slay him himself. So implacable, so incurable, is the enmity of the
serpent against that of the woman, so deceitful and desperately wicked
is the heart of man without the grace of God, Jer. 17:9. The fresh
honours David had won in this last war with the Philistines, instead of
extinguishing Saul\'s ill-will to him, and confirming his
reconciliation, revived his envy and exasperated him yet more. And, when
he indulged this wicked passion, no marvel that the evil spirit came
upon him (v. 9), for when we let the sun go down upon our wrath we give
place to the devil (Eph. 4:26, 27), we make room for him and invite him.
Discomposures of mind, though helped forward by the agency of Satan,
commonly owe their origin to men\'s own sins and follies. Saul\'s fear
and jealousy made him a torment to himself, so that he could not sit in
his house without a javelin in his hand, pretending it was for his
preservation, but designing it for David\'s destruction; for he
endeavored to nail him to the wall, running at him so violently that he
struck the javelin into the wall (v. 10), so strong was the devil in
him, so strong his own rage and passion. Perhaps he thought that, if he
killed David now, he would be excusable before God and man, as being non
compos mentis-not in his right mind, and that it would be imputed to his
distraction. But God cannot be deceived by pretences, whatever men may
be.

`III.` God continues his care of David and still watches over him for
good. Saul missed his blow. David was too quick for him and fled, and by
a kind providence escaped that night. To these preservations, among
others, David often refers in his Psalms, when he speaks of God\'s being
his shield and buckler, his rock and fortress, and delivering his soul
from death.

### Verses 11-17

Here is, `I.` Saul\'s further design of mischief to David. When David had
escaped the javelin, supposing he went straight to his own house, as
indeed he did, Saul sent some of his guards after him to lay wait at the
door of his house, and to assassinate him in the morning as soon as he
stirred out, v. 11. Josephus says the design was to seize him and to
hurry him before a court of justice that was ordered to condemn him and
put him to death as a traitor; but we are here told it was a shorter way
they were to take with him: they were ordered to slay him. Well might
David complain that his enemies were bloody men, as he did in the psalm
which he penned at this time, and upon this occasion (Ps. 59), when Saul
sent, and they watched the house to kill him. See v. 2, 3, and 7. He
complains that swords were in their lips.

`II.` David\'s wonderful deliverance out of this danger. Michal was the
instrument of it, whom Saul gave him to be a snare to him, but she
proved to be his protector and helper. Often is the devil out-shot with
his own bow. How Michal came to know the danger her husband was in does
not appear; perhaps she had notice sent her from court, or rather was
herself aware of the soldiers about the house, when they were going to
bed, though they kept so still and silent that they said, Who dost hear?
which David takes notice of, Ps. 59:7. She, knowing her father\'s great
indignation at David, soon suspected the design, and bestirred herself
for her husband\'s safety. 1. She got David out of the danger. She told
him how imminent the peril was (v. 11): To-morrow thou wilt be slain. As
Josephus paraphrases it, she told him that if the sun saw him there next
morning it would never see him more; and then put him in a way of
escape. David himself was better versed in the art of fighting than of
flying, and had it been lawful it would have been easy for him to have
cleared his house, by dint of sword, from those that haunted it; but
Michal let him down through a window (v. 12), all the doors being
guarded; and so he fled and escaped. And now it was that, either in his
own closet before he went or in the hiding-place to which he fled, he
penned that fifty-ninth Psalm, which shows that, in his fright and
hurry, his mind was composed, and, in this great danger, his faith was
strong and fixed on God; and, whereas the plot was to slay him in the
morning, he speaks there with the greatest assurance (v. 16), I will
sing aloud of thy mercy in the morning. 2. She practised a deception
upon Saul and those whom he employed to be the instruments of his
cruelty. When the doors of the house were opened in the morning, and
David did not appear, the messengers would search the house for him, and
did so. But Michal told them he was sick in bed (v. 14), and, if they
would not believe her, they might see, for (v. 13) she had put a wooden
image in the bed, and wrapped it up close and warm as if it had been
David asleep, not in a condition to be spoken to; the goats\' hair about
the image was to resemble David\'s hair, the better to impose upon them.
Michal can by no means be justified in telling a lie, and covering it
thus with a cheat. God\'s truth needed not her lie. But she intended
hereby to keep Saul in suspense for a while, that David might have some
time to secure himself, not doubting but those messengers would pursue
him if they found he had gone. The messengers had so much humanity as
not to offer him any disturbance when they heard he was sick; for to
those that are in this misery pity should be shown; but Saul, when he
heard it, gave positive orders that he should be brought to him sick or
well: Bring him to me in the bed, that I may slay him, v. 15. It was
base and barbarous thus to triumph over a sick man; and to vow the death
of one who for aught that he knew was dying by the hand of nature. So
earnestly did he thirst after his blood, and so greedy was his revenge,
that he could not be pleased to see him dead, unless he himself was the
death of him; though awhile ago he had said, Let not my hand be upon
him. Thus when men lay the reins on the neck of their passions they grow
more and more outrageous. When the messengers were sent again, the cheat
was discovered, v. 16. But by this time it was to be hoped that David
was safe, and therefore Michal was not then much concerned at the
discovery. Saul chid her for helping David to escape (v. 17): Why hast
thou deceived me so? What a base spirit was Saul of, to expect that,
because Michal was his daughter, she must therefore betray her own
husband to him unjustly. Ought she not to forsake and forget her father
and her father\'s house, to cleave to her husband? Those that themselves
will be held by no bonds of reason or religion are ready to think that
others should as easily break those bonds. In answer to Saul\'s chiding,
Michal is not so careful of her husband\'s reputation as she had been of
his person, when she makes this her excuse: He said, Let me go, why
should I kill thee? As her insinuating that she would have hindered his
flight was false (it was she that put him upon it and furthered it), so
it was an unjust unworthy reflection upon him to suggest that he
threatened to kill her if she would not let him go, and might confirm
Saul in his rage against him. David was far from being so barbarous a
man and so imperious a husband, so brutish in his resolves and so
haughty in his menaces, as she here represented him. But David suffered
both from friends and foes, and so did the son of David.

### Verses 18-24

Here is, `I.` David\'s place of refuge. Having got away in the night from
his own house, he fled not to Bethlehem to his relations, nor to any of
the cities of Israel that had caressed and cried him up, to make an
interest in them for his own preservation; but he ran straight to Samuel
and told him all that Saul had done to him, v. 18. 1. Because Samuel was
the man that had given him assurance of the crown, and his faith in that
assurance now beginning to fail, and he being ready to say in his haste
(or in his flight, as some read it, Ps. 116:11), All men are liars
(\"not only Saul that promised me my life, but Samuel himself that
promised me the throne\"), whither should he go but to Samuel, for such
encouragements, in this day of distress, as would support his faith? In
flying to Samuel he made God his refuge, trusting in the shadow of his
wings; where else can a good man think himself safe? 2. Because Samuel,
as a prophet, was best able to advise him what to do in this day of his
distress. In the psalm he penned the night before he had lifted up his
prayer to God, and now he takes the first opportunity of waiting upon
Samuel to receive direction and instruction from God. If we expect
answers of peace to our prayers, we must have our ears open to God\'s
word. 3. Because with Samuel there was a college of prophets with whom
he might join in praising God, and the pleasure of this exercise would
be the greatest relief imaginable to him in his present distress. He met
with little rest or satisfaction in Saul\'s court, and therefore went to
seek it in Samuel\'s church. And, doubtless, what little pleasure is to
be had in this world those have it that live a life of communion with
God; to this David retired in the time of trouble, Ps. 27:4-6.

`II.` David\'s protection in this place: He and Samuel went and dwelt (or
lodged) in Naioth, where the school of the prophets was, in Ramah, as in
a privileged place, for the Philistines themselves would not disturb
that meeting, ch. 10:10. But Saul, having notice of it by some of his
spies (v. 19), sent officers to seize David, v. 20. When they did not
bring him he sent more; when they returned not he sent the third time
(v. 21), and, hearing no tidings of these, he went himself, v. 22. So
impatient was he in his thirst after David\'s blood, so restless to
compass his design against him, that, though baffled by one providence
after another, he could not perceive that David was under the special
protection of Heaven. It was below the king to go himself on such an
errand as this; but persecutors will stoop to any thing, and stick at
nothing, to gratify their malice. Saul lays aside all public business to
hunt David. How was David delivered, now that he was just ready to fall
(like his own lamb formerly) into the mouth of the lions? Not as he
delivered his lamb, by slaying the lion, or, as Elijah was delivered, by
consuming the messengers with fire from heaven, but by turning the lions
for the present into lambs.

`1.` When the messengers came into the congregation where David was among
the prophets the Spirit of God came upon them, and they prophesied, that
is, they joined with the rest in praising God. Instead of seizing David,
they themselves were seized. And thus, `(1.)` God secured David; for
either they were put into such an ecstasy by the spirit of prophecy that
they could not think of any thing else, and so forgot their errand and
never minded David, or they were by it put, for the present, into so
good a frame that they could not entertain the thought of doing so bad a
thing. 2. He put an honour upon the sons of the prophets and the
communion of saints, and showed how he can, when he pleases, strike an
awe upon the worst of men, by the tokens of his presence in the
assemblies of the faithful, and force them to acknowledge that God is
with them of a truth, 1 Co. 14:24, 25. See also the benefit of religious
societies, and what good impressions may be made by them on minds that
seemed unapt to receive such impressions. And where may the influences
of the Spirit be expected but in the congregations of the saints? `(3.)`
He magnified his power over the spirits of men. He that made the heart
and tongue can manage both to serve his own purposes. Balaam prophesied
the happiness of Israel, whom he would have cursed; and some of the
Jewish writers think these messengers prophesied the advancement of
David to the throne of Israel.

`2.` Saul himself was likewise seized with the spirit of prophecy before
he came to the place. One would have thought that so bad a man as he was
in no danger of being turned into a prophet; yet, when God will take
this way of protecting David, even Saul had no sooner come (as bishop
Hall expresses it) within smell of the smoke of Naioth but he
prophesies, as his messengers did, v. 23. He stripped off his royal robe
and warlike habiliments, because they were either too fine or too heavy
for this service, and fell into a trance as it should seem, or into a
rapture, which continued all that day and night. The saints at Damascus
were delivered from the range of the New-Testament Saul by a change
wrought on his spirit, but of another nature from this. This was only
amazing, but that sanctifying-this for a day, that for ever. Note, Many
have great gifts and yet no grace, prophesy in Christ\'s name and yet
are disowned by him, Mt. 7:22, 23. Now the proverb recurs, Is Saul among
the prophets? See ch. 10:12. Then it was different from what it had
been, but now contrary. He is rejected of God, and actuated by an evil
spirit, and yet among the prophets.
