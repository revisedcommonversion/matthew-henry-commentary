> [[Introduction to Volume 4]{.underline}](#introduction-to-volume-4)
>
> [[Introduction to Isaiah]{.underline}](#introduction-to-isaiah)

Introduction to Volume 4
========================

Those books of scripture are all prophetical of which here, in weakness,
and in fear, and in much trembling, we have endeavoured a methodical
explication and a practical improvement. I call them prophetical because
so they are for the main, though we have some histories (here and there
brought in for the illustration of the prophecies) and a book of
Lamentations. Our Saviour often puts the Law and the Prophets for all
the Old Testament. The prophets, by waiving the ceremonial precepts, and
not insisting on them, but only on the weightier matters of the law,
plainly intimated the abolishing of that part of the law of Moses by the
gospel; and by their many predictions of Christ, and the kingdom of his
grace, they intimated the accomplishing the perfecting of that part of
the law of Moses in the gospel. Thus the prophets were the nexus-the
connecting bond between the law and the gospel, and are therefore fitly
placed between them.

These books, being prophetical, are, as such, divine, and of heavenly
origin and extraction. We have human laws, human histories, and human
poems, as well as divine ones, but we can have no human prophecies. Wise
and good men may make prudent conjectures concerning future events
(moral prognostications we call them); but it is essential to true
prophecy that it be of God. The learned Huetius (Demonstrat. Evang. pag.
15) lays this down for one of his axioms, Omnis prophetica facultas à
Deo est-The prophetic talent is entirely from God; and he proves it to
be the sense both of Jews and heathen that it is God\'s prerogative to
foresee things to come, and that whoever had such a power had it from
God. And therefore the Jews reckon all prophecy to be given by the
highest degree of inspiration, except that which was peculiar to Moses.
When our Saviour asked the chief priests whether John\'s baptism were
from heaven or of men, they durst not say Of men, because the people
counted him a prophet, and, if so, then not of men. The Hebrew name for
a prophet is nbyÕ-a speaker, preacher, or orator, a messenger, or
interpreter, that delivers God\'s messages to the children of men, as a
herald to proclaim war or an ambassador to treat of peace. But then it
must be remembered that he was formerly called rÕh or hä-.-åsä-.-åh,
that is, a seer (1 Sa. 9:9); for prophets, with the eyes of their minds,
first saw what they were to speak and then spoke what they had seen.

Prophecy, taken strictly, is the foretelling of things to come; and
there were those to whom God gave this power, not only that it might be
a sign for the confirming of the faith of the church concerning the
doctrine preached when the things foretold should be fulfilled, but for
warning, instruction, and comfort, in prospect of what they themselves
might not live to see accomplished, but which should be fulfilled in its
season: so predictions of things to come long after might be of present
use.

The learned Dr. Grew (Cosmol. sacra, lib. 4, cap. 6) describes prophecy
in this sense to be, \"A declaration of the divine prescience, looking
at any distance through a train of infinite causes, known and unknown to
us, upon a sure and certain effect.\" Hence he infers, \"That the being
of prophecies supposes the non-being of contingents; for, though there
are many things which seem to us to be contingents, yet, were they so
indeed, there could have been no prophecy; and there can be no
contingent seemingly so loose and independent but it is a link of some
chain.\" And Huetius gives this reason why none but God can foretel
things to come, Because every effect depends upon an infinite number of
preceding causes, all which, in their order, must be known to him that
foretels the effect, and therefore to God only, for he alone is
omniscient. So Tully argues: Qui teneat causas rerum futurarum, idem
necesse est omnia teneat quae futura sint; quod facere nemo nisi Deus
potest-He who knows the causes of future events must necessarily know
the events themselves; this is the prerogative of God alone (Cicero de
Divin. lib. 1). And therefore we find that by this the God of Israel
proves himself to be God, that by his prophets he foretold things to
come, which came to pass according to the prediction, Isa. 46:9, 10. And
by this he disproves the pretensions of the Pagan deities, that they
could not show the things that were to come to pass hereafter, Isa.
41:23. Tertullian proves the divine authority of the scripture from the
fulfilling of scripture-prophecies: Idoneum, opinor, testimonium
divinitatis, veritas divinationis-I conceive the accomplishment of
prophecy to be a satisfactory attestation from God (Apol. cap. 20). And,
besides the foretelling of things to come, the discovering of things
secret by revelation from God is a branch of prophecy, as Ahijah\'s
discovering Jeroboam\'s wife in disguise, and Elisha\'s telling Gehazi
what passed between him and Naaman. But (Du Pin, Hist. of the Canon.
lib. 1, cap. 2) prophecy, in scripture language, is taken more largely
for a declaration of such things to the children of men, either by word
or writing, as God has revealed to those that speak or write it, by
vision, dream, or inspiration, guiding their minds, their tongues, and
pens, by his Holy Spirit, and giving them not only ability, but
authority, to declare such things in his name, and to preface what they
say with, Thus saith the Lord. In this sense it is said, The prophecy of
scripture came not in old time by the will of man, as other pious moral
discourses might, but holy men spoke and wrote as they were moved by the
Holy Ghost, 2 Pt. 1:20, 21. The same Holy Spirit that moved upon the
face of the waters to produce the world moved upon the minds of the
prophets to produce the Bible.

Now I think it is worthy to be observed that all nations, having had
some sense of God and religion, have likewise had a notion of prophets
and prophecy, have had a veneration for them, and a desire and
expectation of acquaintance and communion with the gods they worshipped
in that way. Witness their oracles, their augurs, and the many arts of
divination they had in use among them in all the ages ad all the
countries of the world.

It is commonly urged as an argument against the atheists, to prove that
there is a God, That all nations of the world acknowledged some god or
other, some Being above them, to be worshipped and prayed to, to be
trusted in and praised; the most ignorant and barbarous nations could
not avoid the knowledge of it; the most learned and polite nations could
not avoid the belief of it. And this is a sufficient proof of the
general and unanimous consent of mankind to this truth, though far the
greatest part of men made to themselves gods which yet were no gods. Now
I think it may be urged with equal force against the Deists, for the
proof of a divine revelation, that all nations of the world had, and had
veneration for, that which they at least took to e a divine revelation,
and could not live without it, though in this also they became vain in
their imaginations, and their foolish heart was darkened. But, if there
were not a true deity and a true prophecy, there would never have been
pretended deities and counterfeit prophecies.

Lycurgus and Numa, those two great lawgivers of the Spartan and Roman
commonwealths, brought their people to an observance of their laws by
possessing them with a notion that they had them by divine revelation,
and so making it a point of religion to observe them. And those that
have been ever so little conversant with the Greek and Roman histories,
as well as with the more ancient ones of Chaldea and Egypt cannot but
remember what a profound deference their princes and great commanders,
and not their unthinking commonalty only, paid to the oracles and
prophets, and the prognostications of their soothsayers, which, in all
cases of importance, were consulted with abundance of gravity and
solemnity, and how often the resolutions of councils and the motions of
mighty armies turned upon them, though they appeared ever so groundless
and farfetched.

There is a full account given by that learned philosopher and physician
Caspar Peucer (De Praecipuis Divinationum Generibus, A. 1591) of the
many kinds of divination and prediction used among the Gentiles, by
which they took on them to tell the fortune both of states and
particular persons. They were all, he says, reduced by Plato to two
heads: Divinatio Mantikeµ, which was a kind of inspiration, or was
though to be so, the prophet or prophetess foretelling things to come by
an internal flatus or fury; such was the oracle of Apollo at Delphos,
and that of Jupiter Trophonius, which, with others like them, were
famous for many ages, during the prevalency of the kingdom of darkness,
but (as appears by some of the Pagan writers themselves) they were all
silenced and struck dumb, when the gospel (that truly divine oracle)
began to be preached to the nations. The other kind of divination was
that which he calls Oioµnistikeµ, which was a prognostication by signs,
according to rules of art, as by the flight of birds, the entrails of
beasts, by stars or meteors, and abundance of ominous accidents, with
which a foolish world was miserably imposed upon. A large account of
this matter we have also in the late learned dissertations of Anton. Van
Dale, to which I refer the reader (De Verâ ac Falsâ Prophetiâ, A. 1696).
But nothing of this kind made a greater noise in the Gentile world than
the oracles of the Sibyls and their prophecies. Their name signifies a
divine counsel: Sibyllae, qu. Siobulae, Sios, in the Aeolic dialect,
being put for Theos. Peucer says, \"Almost every nation had its Sibyls,
but those of Greece were most celebrated.\" They lived in several ages;
the most ancient is said to be the Sibylla Delphica, who lived before
the Trojan war, or about that time. The Sibylla Erythrea was the most
noted; she lived about the time of Alexander the Great. But it the
Sibylla Cumana of whom the story goes that she presented herself, and
nine books of oracles, to Tarquinius Superbus, which she offered to sell
him at so vast a rate that he refused to purchase them, upon which she
burnt three, and, upon his second refusal, three more, but made him give
the same rate for the remaining three, which were deposited with great
care in the Capitol. But, those being afterwards burnt accidentally with
the Capitol, a collection was made of other Sibylline oracles, and those
are they which Virgil refers to in his fourth Eclogue (Vid. Virg.
Aeneid. lib. 6). All the oracles of the Sibyls that are extant were put
together, and published, in Holland, not many years ago, by Seryatius
Gallaeus, in Greek and Latin, with large and learned notes, together
with all that could be met with of the metrical oracles that go under
the names of Jupiter, Apollo, Serapis, and others, by Joannes Opsopaeus.

The oracles of the Sibyls were appealed to by many of the fathers for
the confirmation of the Christian religion. Justin Martyr (Ad Graecos
Cohortat. juxta finem.) appeals with a great deal of assurance,
persuading the Greeks to give credit to that ancient Sibyl, whose works
were extant all the world over; and to their testimony, and that of
Hydaspis, he appeals concerning the general conflagration and the
torments of hell. Clemens Alexandrinus (Apol. 2. p. mihi. 66. l.) often
quotes the Sibyls\' verses with great respect; so does Lactantius
(Quaest. et Respons. p. 436); St. Austin (Aug. de Div. Dei, lib. 18,
cap. 23), De Civitate Dei, has the famous acrostic at large, said to be
one of the oracles of the Sibylla Erythrea, the first letters of the
verses making Ieµsous Christos Theou hyios Soµteµr-Jesus Christ the Son
of God the Saviour. Divers passages they produce out of those oracles
which expressly foretel the coming of the Messiah, his being born of a
virgin, his miracles, his sufferings, particularly his being buffeted,
spit upon, crowned with thorns, having vinegar and gall given him to
drink, etc. Whether these oracles were genuine and authentic or no has
been much controverted among the learned. Baronius and the popish
writers generally admit and applaud them, and build much upon them; so
do some protestant writers; Isaac Vossius has written a great deal to
support the reputation of them, and (as I find him quoted by Van Dale)
will needs have it that they were formerly a part of the canon of
scripture; and a learned prelate of our own nation, Bishop Montague,
pleads largely, and with great assurance, for their authority, and is of
opinion that some of them were divinely inspired. But many learned men
look upon it to be a pious fraud, as they call it, concluding that those
verses of the Sibyls which speak so very expressly of Christ and the
future state were forged by some Christians and imposed upon the
over-credulous. Huetius (Demonstrat. p. 748), though of the Romish
church, condemns both the ancient and more modern compositions of the
Sibyls, and refers his reader, for the proof of their vanity, to the
learned Blondel. Van Dale and Gallaeus look upon them to be a forgery.
And the truth is they speak so much more particularly and plainly
concerning our Saviour and the future state than any of the prophets of
the Old Testament do, that we must conclude St. Paul, who was the
apostle of the Gentiles, guilty not only of a very great omission (that
in all his preaching of the gospel to the Gentiles, and in all his
epistles to the Gentile churches, he never so much as mentions the
prophecies of the Sibyls, nor vouches their authority, as he does that
of the Old-Testament prophets, in his preaching and writing to the
Jews), but likewise of a very great mistake, in making it the particular
advantage which the Jews had above the Gentiles that to them were
committed the oracles of God (Rom. 3:1, 2), and that they were the
children of the prophets, while he speaks of the Gentiles as sitting in
darkness and being afar off. We cannot conceive that heathen women, and
those actuated by daemons, should speak more clearly and fully of the
Messiah than those holy men did who, we are sure, were moved by the Holy
Ghost, nor that the Gentiles should be entrusted with larger and earlier
discoveries of the great salvation than that people of whom, as
concerning the flesh, Christ was to come. But enough, if not more than
enough, of the pretenders to prophecy. It is a good remark which the
learned Gallaeus makes upon the great veneration which the Romans had
for the oracles of the Sibyls, for which he quotes Dionysius
Halicarnassaeus, Ouden oute Roµmaioi phylattousin, oute hosion kteµma
oute hieron, hoµs ta Sibylleia thesphata-The Romans preserve nothing
with such sacred care, nor do they hold any thing in such high
estimation, as the Sibylline oracles. Hi si pro vitreis suis thesauris
adeò decertarunt, quid nos pro genuinis nostris, à Deo inspiratis?-If
they had such a value for these counterfeits, how precious should the
true treasure of the divine oracles be to us! Of these we come next to
speak.

Prophecy, we are sure, was of equal date with the church; for faith
comes, not by thinking and seeing, as philosophy does, but by hearing,
by hearing the word of God, Rom. 10:17. In the antediluvian period Adam
received divine revelation in the promise of the Seed of the woman, and
no doubt communicated it in the name of the Lord, to his seed, and was
prophet, as well as priest, to his numerous family. Enoch was a prophet,
and foretold perhaps the deluge, certainly the last judgment, that of
the great day. Behold the Lord comes, Jude 14. When men began, as a
church, to call upon the name of the Lord (Gen. 4:26), or to call
themselves by his name, they were blessed with prophets, for the
prophecy came in old time (2 Pt. 1:21); it is venerable for its
antiquity. When God renewed his covenant of providence (and that a
figure of the covenant of grace) with Noah and his sons, we soon after
find Noah, as a prophet, foretelling, not only the servitude of Canaan,
but God\'s enlarging Japhet by Christ, and his dwelling in the tents of
Shem, Gen. 9:26, 27. And when, upon the general revolt of mankind to
idolatry (as, in the former period, upon the apostasy of Cain), God
distinguished a church for himself by the call of Abraham, and by his
covenant with him and his seed, he conferred upon him and the other
patriarchs the spirit of prophecy; for, when he reproved kings for their
sakes, he said, Touch not my anointed, who have received that unction
from the Holy One, and do my prophets no harm, Ps. 105:14, 15. And of
Abraham he said expressly, He is a prophet (Gen. 20:7); and it was with
a prophetic eye, as a seer, that Abraham saw Christ\'s day (Jn. 8:56),
saw it as so great a distance, and yet with so great an assurance
triumphed in it. And Stephen seems to speak of the first settling of a
correspondence between him and God, by which he was established to be a
prophet, when he says, The God of glory appeared to him (Acts 7:2),
appeared in glory. Jacob, upon his death-bed, as a prophet, told his
sons what should befal them in the last days (Gen. 49:1), and spoke very
particularly concerning the Messiah.

Hitherto was the infancy of the church, and with it of prophecy; it was
the dawning of that day; and that morning-light owed its rise to the Sun
of righteousness, though he rose not till long after, but it shone more
and more. During the bondage of Israel in Egypt, this, as other glories
of the church, was eclipsed; but, as the church made a considerable and
memorable advance in the deliverance of Israel out of Egypt and the
forming of them into a people, so did the Spirit of prophecy in Moses,
the illustrious instrument employed in that great service; and it was by
that Spirit that he performed that service; so it is said, Hos. 12:13,
By a prophet the Lord brought Israel out of Egypt, and by a prophet was
he preserved through the wilderness to Canaan, that is, by Moses as a
prophet. It appears, by what God said to Aaron, that there were then
other prophets among them, to whom God made known himself and his will
in dreams and visions (Num. 12:6), but to Moses he spoke in a peculiar
manner, mouth to mouth, even apparently, and not in dark speeches, Num.
12:8. Nay, such a plentiful effusion was there of the Spirit of prophecy
at that time (because Moses was such a prophet as was to be a type of
Christ the great prophet) that some of his Spirit was put upon seventy
elders of Israel at once, and they prophesied, Num. 11:25. What they
said was extraordinary, and not only under the direction of a prophetic
inspiration, but under the constraint of a prophetic impulse, as appears
by the case of Eldad and Meded.

When Moses, that great prophet, was laying down his office, he promised
Israel that the Lord God would raise them up a prophet of their brethren
like unto him, Deu. 18:15, 18. In these words, says the learned Bishop
Stillingfleet (Orig. Sacr. B. 2, c. 4-though, in their full and complete
sense, they relate to Christ, and to him they are more than once applied
in the New Testament), there is included a promise of an order of
prophets, which should succeed Moses in the Jewish church, and be the
logia zoµnta-the living oracles among them (Acts 7:38), by which they
might know the mind of God; for, in the next words, he lays down rules
for the trial of prophets, whether what they said was of God or no, and
it is observable that that promise comes in immediately upon an express
prohibition of the Pagan rites of divination and the consulting of
wizards and familiar spirits: \"You shall not need to do that\" (said
Moses), \"for, to your much better satisfaction, you shall have prophets
divinely inspired, by whom you may know from God himself both what to do
and what to expect.\" But as Jacob\'s dying prophecy concerning the
sceptre in Judah, and the lawgiver between his feet, did not begin to be
remarkably fulfilled till David\'s time, most of the Judges being of
other tribes, so Moses\'s promise of a succession of prophets began not
to receive its accomplishment till Samuel\'s time, a little before the
other promise began to emerge and operate; and it was an introduction to
the other, for it was by Samuel, as a prophet, that David was anointed
king, which was an intimation that the prophetical office of our
Redeemer should make way, both in the world and in the heart, for his
kingly office; and therefore when he was asked, Art thou a king? (Jn.
18:37) he answered, not evasively, but very pertinently, I came to bear
witness to the truth, and so to rule as a king purely by the power of
truth.

During the government of the Judges there was a pouring out of the
Spirit, but more as a Spirit of skill and courage for war than as a
Spirit of prophecy. Deborah is indeed called prophetess, because of her
extraordinary qualifications for judging Israel; but that is the only
mention of prophecy, that I remember, in all the book of Judges.
Extraordinary messages were sent by angels, as to Gideon and Manoah; and
it is expressly said that before the word of the Lord came to Samuel (1
Sa. 3:1) it was precious, it was very scarce, there was no open vision.
And it was therefore with more than ordinary solemnity that the word of
the Lord came first to Samuel; and by degrees notice and assurance were
given to all Israel that Samuel was established to be a prophet of the
Lord, 1 Sa. 3:20. In Samuel\'s time, and by him, the schools of the
prophets were erected, by which prophecy was dignified and provision
made for a succession of prophets; for it should seem that in those
colleges, hopeful young men were bred up in devotion, in a constant
attendance upon the instruction the prophets gave from God, and under a
strict discipline, as candidates, or probationers, for prophecy, who
were called the sons of the prophets; and their religious exercises of
prayer, conference, and psalmody especially, are called prophesyings;
and their praefect, or president, is called their father, 1 Sa. 10:12.
Out of these God ordinarily chose the prophets he sent; and yet not
always: Amos was no prophet nor prophet\'s son (Amon 7:14), had not his
education in the schools of the prophets, and yet was commissioned to go
on God\'s errands, and (which is observable) though he had not
academical education himself, yet he seems to speak of it with great
respect when he reckons it among the favours God had bestowed upon
Israel that he raised up of their sons for prophets and of their young
men for Nazarites, Amos 2:11.

It is worth noting that when the glory of the priesthood was eclipsed by
the iniquity of the house of Eli, the desolations of Shiloh, and the
obscurity of the ark, there was then a more plentiful effusion of the
Spirit of prophecy than had been before; a standing ministry of another
kind was thereby erected, and a succession of it kept up. And thus
afterwards, in the kingdom of the ten tribes, where there was no legal
priesthood at all, yet there were prophets and prophets; sons; in
Ahab\'s time we meet with a hundred of them, whom Obadiah his by fifty
in a cave, 1 Ki. 18:4. When the people of God, who desired to know his
mind, were deprived of one way of instruction, God furnished them with
another, and a less ceremonious one; for he left not himself without
witness, nor them without a guide. And when they had no temple or altar
that they could attend upon with any safety or satisfaction then had
private meetings at the prophets\' houses, to which the devout faithful
worshippers of God resorted (as we find the good Shunamite did, 2 Ki.
4:23), and where they kept their new-moons and their sabbaths,
comfortably, and to their edification.

David was himself a prophet; so St. Peter calls him (Acts 2:30); and,
though we read not of God\'s speaking to him by dreams and visions, yet
we are sure that the Spirit of the Lord spoke by him, and his word was
in his tongue (2 Sa. 23:2), and he had those about him that were seers,
that were his seers, as Gad and Iddo, that brought him messages from
God, and wrote the history of his times. And now the productions of the
Spirit of prophecy were translated into the service of the temple, not
only in the model of the house which the Lord made David understand in
writing by his hand upon him (1 Chr. 28:19), but in the worship
performed there; for there we find Asaph, Heman, and Jeduthun,
prophesying with harps and other musical instruments, according to the
order of the king, not to foretel things to come, but to give thanks and
to praise the Lord (1 Chr. 25:1-3); yet, in their psalms, they spoke
much of Christ and his kingdom, and the glory to be revealed.

In the succeeding reigns, both of Judah and Israel, we frequently meet
with prophets sent on particular errands to Rehoboam, Jeroboam, Asa, and
other kings, who, it is probable, instructed the people in the things of
God at other times, though it is not recorded. But, prophecy growing
into contempt with many, God revived the honour of it, and put a new
lustre upon it, in the power given to Elijah and Elisha to work
miracles, and the great things that God did by them for the confirming
of the people\'s faith in it, and the awakening of their regard to it, 2
Ki. 2:3, 4:1, 38; 5:22; 6:1. In their time, and by their agency, it
should seem, the schools of the prophets were revived, and we find sons
of the prophets, fellows of those sacred colleges, employed in carrying
messages to the great men, as to Ahab (1 Ki. 20:35), and to Jehu, 2 Ki.
9:1.

Hitherto, the prophets of the Lord delivered their messages by word of
mouth, only we read of one writing which came from Elijah the prophet to
Jehoram king of Israel, 2 Chr. 21:12. The histories of those times which
are left us were compiled by prophets, under a divine direction; and,
when the Old Testament is divided into the law and the Prophets, the
historical books are, for that reason, reckoned among the prophets. But,
in the later times of the kingdoms of Judah and Israel, some of the
prophets were divinely inspired to write their prophecies, or abstracts
of them, and to leave them upon record, for the benefit of after-ages,
that the children who should be born might praise the Lord for them,
and, by comparing the event with the prediction, might have their faith
confirmed. And, probably, those later prophets spoke more fully and
plainly of the Messiah and his kingdom than their predecessors had done,
and for that reason their prophecies were put in writing, not only for
the encouragement of the pious Jews that looked for the consolation of
Israel, but for the use of us Christians, upon whom the ends of the
world have come, as David\'s psalms had been for the same reason, that
the Old Testament and the New might mutually give light and lustre to
each other. Many other faithful prophets there were at the same time,
who spoke in God\'s name, who did not commit their prophecies to
writing, but were of those whom God sent, rising up betimes and sending
them, the contempt of whom, and of their messages, brought ruin without
remedy upon that sottish people, that knew not the day of their
visitation. In their captivity they had some prophets, some to show them
how long; and though it was not by a prophet, like Moses, that they were
brought out of Babylon, as they had been out of Egypt, but by Joshua the
high priest first, and afterwards by Ezra the scribe, to show that God
can do his work by ordinary means when he pleases, yet, soon after their
return, the Spirit of prophecy was poured out plentifully, and continued
(according to the Jews\' computation) forty years in the second temple,
but ceased in Malachi. Then (say the rabbin) the Holy Spirit was taken
from Israel, and they had the benefit only of the Bathkol-the daughter
of a voice, that is, a voice from heaven, which they look upon to be the
lowest degree of divine revelation. Now herein they are witnesses
against themselves for rejecting the true Messiah, for our Lord Jesus,
and he only was spoken to by a voice from heaven at his baptism, his
transfiguration, and his entrance on his sufferings.

In John the Baptist prophecy revived, and therefore in him the gospel is
said to begin, when the church had had no prophets for above 300 years.
We have not only the vox populi-the voice of the people to prove John a
prophet, for all the people counted him so, but vox Dei-the voice of God
too; for Christ calls him a prophet, Mt. 11:9, 10. He had an
extraordinary commission from God to call people to repentance, was
filled with the Holy Ghost from his mother\'s womb, and was therefore
called the prophet of the Highest, because he went before the face of
the Lord, to prepare his way (Lu. 1:15, 16); and though he did no
miracle, nor gave any sign or wonder, yet this proved him a true
prophet, that all he said of Christ was true, Jn. 10:41. Nay, and this
proved him more than a prophet, than any of the other prophets, that
whereas by other prophets Christ was discovered as at a great distance,
by him he was discovered as already come, and he was enabled to say,
Behold the Lamb of God. But after the ascension of our Lord Jesus there
was a more plentiful effusion of the Spirit of prophecy than ever
before; then was the promise fulfilled that God would pour out his
Spirit upon all flesh (and not as hitherto upon the Jews only), and
their sons and their daughters should prophesy, Acts 2:16, etc. The gift
of tongues was one new product of the Spirit of prophecy, and given for
a particular reason, that, the Jewish pale being taken down, all nations
might be brought into the church. These and other gifts of prophecy,
being for a sign, have long since ceased and laid aside, and we have no
encouragement to expect the revival of them; but, on the contrary, are
directed to call the scriptures the more sure word of prophecy, more
sure than voices from heaven; and to them we are directed to take heed,
to search them, and to hold them fast, 2 Pt. 1:19. All God\'s spiritual
Israel know that they are established to be the oracles of God (1 Sa.
3:20), and if any add to, or take from, the book of that prophecy, they
may read their doom in the close of it; God shall take blessings from
them, and add curses to them, Rev. 22:18, 19).

Now concerning the prophets of the Old Testament, whose writings are
before us, observe,

`I.` That they were all holy men. We are assured by the apostle that the
prophecy came in old time by holy men of God (and men of God they were
commonly called, because they were devoted to him), who spoke as they
were moved by the Holy Ghost. They were men, subject to like passions as
we are (so Elijah, one of the greatest of them, is said to have been,
Jam. 5:17); but they were holy men, men that in the temper of their
minds, and the tenour of their lives, were examples of serious piety.
Though there were many pretenders, that, without warrant, said Thus
saith the Lord, when he sent them not, and some that prophesied in
Christ\'s name, but he never knew them, and they indeed were workers of
iniquity (Mt. 7:22, 23), and though the cursing blaspheming lips of
Balaam and Caiaphas, even when they actually designed mischief, were
over-ruled to speak oracles, yet none were employed and commissioned to
speak as prophets but those that had received the Spirit of grace and
sanctification; for holiness becomes God\'s house. The Jewish doctors
universally agree in this rule, That the Spirit of prophecy never rests
upon any but a holy and wise man, and one whose passions are allayed
(see Mr. Smith on Prophecy), or, as others express it, a humble man and
a man of fortitude, that is, one that has power to keep his sensual
animal part in due subjection to religion and right reason. And some of
them (Gemara Schab. c. 2) give this rule, That the Spirit of prophecy
does not reside where there are either, on the one hand, grief and
melancholy, or, on the other hand, laughter and lightness of behaviour,
and impertinent idle talk: and it is commonly observed by them, both
from the musical instruments used in the schools of the prophets in
Samuel\'s time and from the instance of Elisha\'s calling for a minstrel
(2 Ki. 3:15), that the divine presence does not reside with sadness, but
with cheerfulness, and Elisha, they say, had not yet recovered himself
from the sorrow he conceived at parting with Elijah. They have also a
tradition (but I know no ground for it) that all the while Jacob mourned
for Joseph, the Shechinah, or Holy Spirit, withdrew from him. Yet I
believe that when David intimates that by his sin in the matter of Uriah
he had lost the right Spirit, and the free Spirit, Ps. 51:10, 12 (which
therefore he begs might be renewed in him and restored to him), it was
not because he was under grief, but because he was under guilt. And
therefore, in order to the return of that right and free Spirit, he
prays that God would create in him a clean heart.

`II.` That they had all a full assurance in themselves of their divine
mission; and (though they could not always prevail to satisfy others)
they were abundantly satisfied themselves that what they delivered as
from God, and in his name, was indeed from him; and with the same
assurance did the apostles speak of the word of life, as that which they
had heard, and seen, and looked on, and which their hands had handled, 1
Jn. 1:1. Nathan spoke from himself when he encouraged David to build the
temple, but afterwards knew he spoke from God when, in his name, he
forbade him to do it. God had various ways of making known to his
prophets the messages they were to deliver to his people; it should
seem, ordinarily, to have been by the ministry of angels. In the
Apocalypse Christ is expressly said to have signified by his angel to
his servant John, Rev. 1:1. It was sometimes done in a vision when the
prophet was awake, sometimes in a dream when the prophet was asleep, and
sometimes by a secret but strong impression upon the mind of the
prophet. But Maimonides has laid down, as a maxim, That all prophecy
makes itself known to the prophet that it is prophecy indeed; that is,
says another of the rabbin, By the vigour and liveliness of the
perception whereby he apprehends the thing propounded (which Jeremiah
intimates when he says, The word of the Lord was as a fire in my bones,
Jer. 20:9), and therefore they always spoke with great assurance,
knowing they should be justified, Isa. 1:7.

`III.` That in their prophesying, both in receiving their message from
God and in delivering it to the people, they always kept possession of
their own souls. Dan. 10:8. Though sometimes their bodily strength was
overpowered by the abundance of the revelations, and their eyes were
dazzled with the visionary light, as in the instances of Daniel and John
(Rev. 1:17), yet still their understanding remained with them, and the
free exercise of their reason. This is excellently well expressed by a
learned writer of our own (Smith on Prophecy, p. 190): \"The prophetical
Spirit, seating itself in the rational powers as well as in the
imagination, did never alienate the mind, but inform and enlighten it;
and those that were actuated by it always maintained a clearness and
consistency of reason, with strength and solidity of judgment. \"For\"
(says he afterwards-Pag. 266) \"God did not make use of idiots or fools
to reveal his will by, but such whose intellects were entire and
perfect; and he imprinted such a clear copy of his truth upon them as
that it became their own sense, being digested fully into their
understandings, so that they were able to deliver and represent it to
others as truly as any can paint forth his own thoughts.\" God\'s
messengers were speaking men, not speaking trumpets. The Fathers
frequently took notice of this difference between the prophets of the
Lord and the false prophets-that the pretenders to prophecy (who either
were actuated by an evil spirit or were under the force of a heated
imagination) underwent alienations of mind, and delivered what they had
to say in the utmost agitation and disorder, as the Pythian prophetess,
who delivered her infernal oracles with many antic gestures, tearing her
hair and foaming at the mouth. And by this rule they condemned the
Montanists, who pretended to prophecy, in the second century, that what
they said was in a way of ecstasy, not like rational men, but like men
in a frenzy. Chrysostom (in 1 Co. 12:1), having described the furious
violent motions of the pretenders to prophecy, adds, Ho de Propheµteµs
ouch houtoµs-A true prophet does not do so. Sed mente sobriâ, et
constanti animi staut, et intelligens quae profert, omnia pronunciat-He
understands what he utters, and utters it soberly and calmly. And
Jerome, in his preface to his Commentaries upon Nahum, observes that it
is called the book of the vision of Nahum. Non enim loquitur en
ekstasei, sed est liber intelligentis omnia quae loquitur-For he speaks
not in an ecstasy, but as one who understands every thing he says. And
again (Prolog. in Habac.), Non ut amens loquitur propheta, nec in morem
insanientium foeminarum dat sine mente sonum-The prophet speaks not as
an insane person, nor like women wrought into fury, does he utter sound
without sense.

`IV.` That they all aimed at one and the same thing, which was to bring
people to repent of their sins and to return to God and to do their duty
to him. This was the errand on which all God messengers were sent, to
beat down sin, and to revive and advance serious piety. The burden of
every son was, Turn you now every one from his evil way; amend your ways
and your doings, and execute judgment between a man and his neighbour,
Jer. 7:3, 5. See Zec. 7:8, 9; 8:16. The scope and design of all their
prophecies were to enforce the precepts and sanctions of the law of
Moses, the moral law, which is of universal and perpetual obligation.
Here is nothing of the ceremonial institutes, of the carnal ordinances
that were imposed only till the times of reformation, Heb. 9:10. Those
were now waxing old and ready to vanish away; but they make it their
business to press the great and weighty matters of the law, judgment,
mercy, and truth.

`V.` That they all bore witness to Jesus Christ and had an eye to him. God
raising up the horn of salvation for us, in the house of his servant
David, was consonant to, and in pursuance of, what he spoke by the mouth
of his holy prophets who have been since the world began, Lu. 1:69, 70.
They prophesied of the grace that should come to us, and it was the
Spirit of Christ in them, one and the same Spirit, that testified
beforehand the sufferings of Christ, and the glory that should follow, 1
Pt. 1:10, 11. Christ was then made known, and yet comparatively hid, in
the predictions of the prophets, as before in the types of the
ceremonial law. And the learned Huetius (Demonstrat. Evang. p. 737)
observes it as really admirable that so many persons, in different ages,
should conspire with one consent, as it were, to foretel, some one
particular and others another, concerning Christ, all which had, at
length, their full accomplishment in him. Ab ipsis mundi incunabulis,
per quatuor annorum millia, uno ore venturum Christum praedixerunt viri
complures, in ejusque ortu, vitâ, virtutibus, rebus gestis, morte, ac
totâ denique Oikonomia praemonstranda consenserunt-From the earliest
period of time, for 4000 years, a great number of men have predicted the
advent of Christ, and presented a harmonious statement of his birth,
life, character, actions, and death, and of that economy which he came
to establish.

`VI.` That these prophets were generally hated and abused in their
several generations by those that lived with them. Stephen challenges
his judges to produce an instance to the contrary: Which of the prophets
have not your fathers persecuted? Yea, and, as it should seem, for this
reason, because they showed before of the coming of the Just One, Acts
7:52. Some there were that trembled at the word of God in their mouths,
but by the most they were ridiculed and despised, and (as ministers are
now by profane people) made a jest of (Hos. 9:7); the prophet was the
fool in the play. Wherefore came this mad fellow unto thee? (2 Ki. 9:11)
said one of the captains concerning one of the sons of the prophets! The
Gentiles never treated their false prophets so ill as the Jews did their
true prophets, but, on the contrary, had them always in veneration. The
Jews\' mocking the messengers of the Lord, killing the prophets, and
stoning those that were sent unto them, was as amazing unaccountable an
instance of the enmity that is in the carnal mind against God as any
that can be produced. And this makes their rejection of Christ\'s gospel
the less strange, that the Spirit of prophecy, which, for many ages, was
so much the glory of Israel, in every age met with so much opposition,
and there were those that always resisted the Holy Ghost in the
prophets, and turned that glory into shame, Acts 7:51. But this was it
that was the measure-filling sin of Israel, that brought upon them both
their first destruction by the Chaldeans and their final ruin by the
Romans, 2 Chr. 36:16.

`VII.` That though men slighted these prophets, God owned them and put
honour upon them. As they were men of God, his immediate servants and
his messengers, so he always showed himself the Lord God of the holy
prophets (Rev. 22:6), stood by them and strengthened them, and by his
Spirit they were full of power; and those that slighted them, when they
had lost them, were made to know, to their confusion, that a prophet had
been among them. What was said of one of the primitive fathers of the
prophets was true of them all, The Lord was with them, and did let none
of their words fall to the ground, 1 Sa. 3:19. What they said by way of
warning and encouragement, for the enforcing of their calls to
repentance and reformation, was to be understood conditionally. When God
spoke by them either, on the one hand, to build and to plant, or, on the
other hand, to pluck up and pull down, the change of the people\'s way
might produce a change of God\'s way (Jer. 18:7-10); such was Jonah\'s
prophecy of Nineveh\'s ruin within forty days; or God might sometimes be
better than his word in granting a reprieve. But what they said by way
of prediction of a particular matter, and as a sign, did always come to
pass exactly as it was foretold; yea, and the general predictions,
sooner or later, took hold even of those that would fain have got clear
of them (Zec. 1:6); for this is that which God glories in, that he
confirms the word of his servants and performs the counsel of his
messengers, Isa. 44:26.

In the opening these prophecies I have endeavoured to give the genuine
sense of them, as far as I could reach it, by consulting the best
expositors, considering the scope and coherence, and comparing spiritual
things with spiritual, the spiritual things of the Old Testament with
those of the New, and especially by prayer to God for the guidance and
direction of the Spirit of truth. But, after all, thee are many things
here dark and hard to be understood, concerning the certain meaning of
which though I could not gain myself, much less expect to give my
reader, full satisfaction, Yet I have not, with the unlearned and
unstable, wrested them to the destruction of any, 2 Pt. 3:16. It is the
prerogative of the Lamb of God to take this book and to open all its
seals. I have likewise endeavoured to accommodate these prophecies to
the use and service of those who desire to faith and holiness. And we
shall find that whatever is given by inspiration of God is profitable (2
Tim. 3:16), though not all alike profitable, not all alike easy or
improvable; but, when the mystery of God shall be finished, we shall
see, what we are now bound to believe, that there is not one idle word
in all the prophecies of this book. What God has said, as well as what
he does, we know not now, but we shall know hereafter.

The pleasure I have had in studying and meditating upon those parts of
these prophecies which are plain and practical, and especially those
which are evangelical, has been an abundant balance to, and recompence
for, the harder tasks we have met with in other parts that are more
obscure. In many parts of this field the treasure must be dug for, as
that in the mines; but in other parts the surface is covered with rich
and precious products, with corn, and flocks, of which we may say, as
was said of Noah, These same have comforted us greatly concerning our
work and the toil of our hands, and have made it very pleasant and
delightful; God grant it may be no less so to the readers!

And now let me desire the assistance of my friends, in setting up my
Eben-Ezer here, in a thankful acknowledgment that hitherto the Lord has
helped me. I desire to praise God that he has spared my life to finish
the Old Testament, and has graciously given me some tokens of his
presence with me in carrying this work, though the more I reflect upon
myself the more unworthy I see myself of the honour of being thus
employed, and the more need I see of Christ and his merit and grace.
Remember me, O my God! for good, and spare me according to the multitude
of thy mercies. The Lord forgive what is mine, and accept what is his
own!

I purpose, if God continue my life and health, according to the measure
of the grace given to me, and in a constant and entire dependence upon
divine strength, to go through the New Testament in two volumes more. I
intimated in my preface to the first volume that I had drawn up some
expositions upon some parts of the New Testament; namely, The gospels of
St. Matthew and St. John; but they are so large that, to make them bear
some proportion to the rest, it is necessary that they be much
contracted, so that I shall be obliged to write them all over again, and
to make considerable alterations, and therefore I cannot expect they
should be published but as these hitherto have been, if God permit, a
volume every other year. I shall begin it now shortly, if the Lord will,
and apply myself to it as closely as I can; and I earnestly desire the
prayers of all that wish well to that undertaking that, if the Lord
spare me to go on with it, I may be enabled to do it well, and so as
that by it some may be led into the riches of the full assurance of
understanding in the mystery of God, even of the Father and of Christ,
Col. 2:2. And, if it shall please God to remove me by death before it be
finished, I trust I shall be able to say not only, Welcome his blessed
will, but, Welcome that blessed world, in which, though now we know in
part, and prophesy but in part, that knowledge which is perfect will
come, and that which is partial will be done away (1 Co. 13:8-10, 12),
in which all our mistakes will be rectified, all our doubts resolved,
all our deficiencies made up, all our endeavours in preaching,
catechising, and expounding, superseded and rendered useless, and all
our prayers swallowed up in everlasting praises,-in which prophecy, now
so much admired, shall fail, and tongues shall cease, and the knowledge
we have now shall vanish away, as the light of the morning-star does
when the sun has risen,-in which we shall no longer see through a glass
darkly, but face to face. In a believing, comfortable, well-grounded,
expectation of that true and perfect light, I desire to continue, living
and dying; in a humble and diligent preparation for it let me spend my
time, and in the full enjoyment of it Oh that I may spend a glorious
eternity!

Introduction to Isaiah
======================

Prophet is a title that sounds very great to those that understand it,
though, in the eye of the world, many of those that were dignified with
it appeared very mean. A prophet is one that has a great intimacy with
Heaven and a great interest there, and consequently a commanding
authority upon earth. Prophecy is put for all divine revelation (2 Pt.
1:20, 21), because that was most commonly by dreams, voices, or visions,
communicated to prophets first, and by them to the children of men, Num.
12:6. Once indeed God himself spoke to all the thousands of Israel from
the top of Mount Sinai; but the effect was so intolerably dreadful that
they entreated God would for the future speak to them as he had done
before, by men like themselves, whose terror should not make them
afraid, nor their hands be heavy upon them, Job 33:7. God approved the
motion (they have well said, says he, Deu. 5:27, 28), and the matter was
then settled by consent of parties, that we must never expect to hear
from God any more in that way, but by prophets, who received their
instructions immediately from God, with a charge to deliver them to his
church. Before the sacred canon of the Old Testament began to be written
there were prophets, who were instead of Bibles to the church. Our
Saviour seems to reckon Abel among the prophets, Mt. 23:31, 35. Enoch
was a prophet; and by him that was first in prediction which is to be
last in execution-the judgment of the great day. Jude 14, Behold, the
Lord comes with his holy myriads. Noah was a preacher of righteousness.
God said of Abraham, He is a prophet, Gen. 20:7. Jacob foretold things
to come, Gen. 49:1. Nay, all the patriarchs are called prophets. Ps.
105:15, Do my prophets no harm. Moses was, beyond all comparison, the
most illustrious of all the Old-Testament prophets, for with him the
Lord spoke face to face, Deu. 34:10. He was the first writing prophet,
and by his hand the first foundations of holy writ were laid. Even those
that were called to be his assistants in the government had the spirit
of prophecy, such a plentiful effusion was there of that spirit at that
time, Num. 11:25. But after the death of Moses, for some ages, the
Spirit of the Lord appeared and acted in the church of Israel more as a
martial spirit than as a spirit of prophecy, and inspired men more for
acting than speaking. I mean in the time of the judges. We find the
Spirit of the Lord coming upon Othniel, Gideon, Samson, and others, for
the service of their country, with their swords, not with their pens.
Messages were then sent from heaven by angels, as to Gideon and Manoah,
and to the people, Judges 2:1. In all the book of judges there is never
once mention of a prophet, only Deborah is called a prophetess. Then the
word of the Lord was precious; there was no open vision, 1 Sa. 3:1. They
had the law of Moses, recently written; let them study that. But in
Samuel prophecy revived, and in him a famous epocha, or period of the
church began, a time of great light in a constant uninterrupted
succession of prophets, till some time after the captivity, when the
canon of the Old Testament was completed in Malachi, and then prophecy
ceased for nearly 400 years, till the coming of the great prophet and
his forerunner. Some prophets were divinely inspired to write the
histories of the church. But they did not put their names to their
writings; they only referred for proof to the authentic records of those
times, which were known to be drawn up by prophets, as Gad, Iddo, etc.
David and others were prophets, to write sacred songs for the use of the
church. After them we often read of prophets sent on particular errands,
and raised up for special public services, among whom the most famous
were Elijah and Elisha in the kingdom of Israel. But none of these put
their prophecies in writing, nor have we any remains of them but some
fragments in the histories of their times; there was nothing of their
own writing (that I remember) but one epistle of Elijah\'s, 2 Chr.
21:12. But towards the latter end of the kingdoms of Judah and Israel,
it pleased God to direct his servants the prophets to write and publish
some of their sermons, or abstracts of them. The dates of many of their
prophecies are uncertain, but the earliest of them was in the days of
Uzziah king of Judah, and Jeroboam the second, his contemporary, king of
Israel, about 200 years before the captivity, and not long after Joash
had slain Zechariah the son of Jehoiada in the courts of the temple. If
they begin to murder the prophets, yet they shall not murder their
prophecies; these shall remain as witnesses against them. Hosea was the
first of the writing prophets; and Joel, Amos, and Obadiah, published
their prophecies about the same time. Isaiah began some time after, and
not long; but his prophecy is placed first, because it is the largest of
them all, and has most in it of him to whom all the prophets bore
witness; and indeed so much of Christ that he is justly styled the
Evangelical Prophet, and, by some of the ancients, a fifth Evangelist.
We shall have the general title of this book (v. 1) and therefore shall
here only observe some things,

`I.` Concerning the prophet himself. He was (if we may believe the
tradition of the Jews) of the royal family, his father being (they say)
brother to king Uzziah. He was certainly much at court, especially in
Hezekiah\'s time, as we find in his story, to which many think it is
owing that his style is more curious and polite than that of some other
of the prophets, and, in some places, exceedingly lofty and soaring. The
Spirit of God sometimes served his own purpose by the particular genius
of the prophet; for prophets were not speaking trumpets, through which
the Spirit spoke, but speaking men, by whom the Spirit spoke, making use
of their natural powers, in respect both of light and flame, and
advancing them above themselves.

`II.` Concerning the prophecy. It is transcendently excellent and useful;
it was so to the church of God then, serving for conviction of sin,
direction in duty, and consolation in trouble. Two great distresses of
the church are here referred to, and comfort prescribed in reference to
them, that by Sennacherib\'s invasion, which happened in his own time,
and that of the captivity in Babylon, which happened long after; and in
the supports and encouragements laid up for each of these times of need
we find abundance of the grace of the gospel. There are not so many
quotations in the gospels out of any, perhaps not out of all, the
prophecies of the Old Testament, as out of this; nor such express
testimonies concerning Christ, witness that of his being born of a
virgin (ch. 7) and that of his sufferings, ch. 53. The beginning of this
book abounds most with reproofs for sin and threatenings of judgment;
the latter end of it is full of wood words and comfortable words. This
method the Spirit of Christ took formerly in the prophets and does
still, first to convince and then to comfort; and those that would be
blessed with the comforts must submit to the convictions. Doubtless
Isaiah preached many sermons, and delivered many messages to the people,
which are not written in this book, as Christ did; and probably these
sermons were delivered more largely and fully than they are here
related, but so much is left on record as Infinite Wisdom thought fit to
convey to us on whom the ends of the world have come; and these
prophecies, as well as the histories of Christ, are written that we
might believe on the name of the Son of God, and that, believing, we
might have life through his name; for to us is the gospel here preached
as well as unto those that lived then, and more clearly. O that it may
be mixed with faith!
