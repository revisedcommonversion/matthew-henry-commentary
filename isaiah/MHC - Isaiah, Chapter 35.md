Isaiah, Chapter 35
==================

Commentary
----------

As after a prediction of God\'s judgments upon the world (ch. 24)
follows a promise of great mercy to be had in store for his church (ch.
25), so here after a black and dreadful scene of confusion in the
foregoing chapter we have, in this, a bright and pleasant one, which,
though it foretel the flourishing estate of Hezekiah\'s kingdom in the
latter part of his reign, yet surely looks as far beyond that as the
prophecy in the foregoing chapter does beyond the destruction of the
Edomites; both were typical, and it concerns us most to look at those
things which they were typical of, the kingdom of Christ and the kingdom
of heaven. When the world, which lies in wickedness, shall be laid in
ruins, and the Jewish church, which persisted in infidelity, shall
become a desolation, then the gospel church shall be set up and made to
flourish. `I.` The Gentiles shall be brought into it (v. 1, 2, 7). `II.` The
well-wishers to it, who were weak and timorous, shall be encouraged (v.
3, 4). `III.` Miracles shall be wrought both on the souls and on the
bodies of men (v. 5, 6). `IV.` The gospel church shall be conducted in the
way of holiness (v. 8, 9). `V.` It shall be brought at last to endless
joys (v. 10). Thus do we find more of Christ and heaven in this chapter
than one would have expected in the Old Testament.

### Verses 1-4

In these verses we have,

`I.` The desert land blooming. In the foregoing chapter we had a populous
and fruitful country turned into a horrid wilderness; here we have in
lieu of that, a wilderness turned into a good land. When the land of
Judah was freed from the Assyrian army, those parts of the country that
had been made as a wilderness by the ravages and outrages they committed
began to recover themselves, and to look pleasantly again, and to
blossom as the rose. When the Gentile nations, that had been long as a
wilderness, bringing forth no fruit to God, received the gospel, joy
came with it to them, Ps. 67:3, 4; 96:11, 12. When Christ was preached
in Samaria there was great joy in that city (Acts 8:8); those that sat
in darkness saw a great and joyful light, and then they blossomed, that
is, gave hopes of abundance of fruit; for that was it which the
preachers of the gospel aimed at (Jn. 15:16), to go and bring forth
fruit, Rom. 1:13; Col. 1:6. Though blossoms are not fruit, and often
miscarry and come to nothing, yet they are in order to fruit. Converting
grace makes the soul that was a wilderness to rejoice with joy and
singing, and to blossom abundantly. This flourishing desert shall have
all the glory of Lebanon given to it, which consisted in the strength
and stateliness of its cedars, together with the excellency of Carmel
and Sharon, which consisted in corn and cattle. Whatever is valuable in
any institution is brought into the gospel. All the beauty of the Jewish
church was admitted into the Christian church, and appeared in its
perfection, as the apostle shows at large in his epistle to the Hebrews.
Whatever was excellent an desirable in the Mosaic economy is translated
into the evangelical institutes.

`II.` The glory of God shining forth: They shall see the glory of the
Lord. God will manifest himself more than ever in his grace and love to
mankind (for that is his glory and excellency), and he shall give them
eyes to see it, and hearts to be duly affected with it. This is that
which will make the desert blossom. The more we see by faith of the
glory of the Lord and the excellency of our God the more joyful and the
more fruitful shall we be.

`III.` The feeble and faint-hearted encouraged, v. 3, 4. God\'s prophets
and ministers are in a special manner charged, by virtue of their
office, to strengthen the weak hands, to comfort those who could not yet
recover the fright they had been put into by the Assyrian army with an
assurance that God would now return in mercy to them. This is the design
of the gospel, 1. To strengthen those that are weak and to confirm
them-the weak hands, which are unable either to work or fight, and can
hardly be lifted up in prayer, and the feeble knees, which are unable
either to stand or walk and unfit for the race set before us. The gospel
furnishes us with strengthening considerations, and shows us where
strength is laid up for us. Among true Christians there are many that
have weak hands and feeble knees, that are yet but babes in Christ; but
it is our duty to strengthen our brethren (Lu. 22:32), not only to bear
with the weak, but to do what we can to confirm them, Rom. 15:1; 1 Th.
5:14. It is our duty also to strengthen ourselves, to lift up the hands
which hang down (Heb. 12:12), improving the strength God has given us,
and exerting it. 2. To animate those that are timorous and discouraged:
Say to those that are of a fearful heart, because of their own weakness
and the strength of their enemies, that are hasty (so the word is), that
are for betaking themselves to flight upon the first alarm, and giving
up the cause, that say, in their haste, \"We are cut off and undone\"
(Ps. 31:22), there is enough in the gospel to silence these fears; it
says to them, and let them say it to themselves and one to another, Be
strong, fear not. Fear is weakening; the more we strive against it the
stronger we are both for doing and suffering; and, for our encouragement
to strive, he that says to us, Be strong has laid help for us upon one
that is mighty.

`IV.` Assurance given of the approach of a Saviour: \"Your God will come
with vengeance. God will appear for you against your enemies, will
recompense both their injuries and your losses.\" The Messiah will come,
in the fulness of time, to take vengeance on the powers of darkness, to
spoil them, and make a show of them openly, to recompense those that
mourn in Zion with abundant comforts. He will come and save us. With the
hopes of this the Old-Testament saints strengthened their weak hands. He
will come again at the end of time, will come in flaming fire, to
recompense tribulation to those who have troubled his people, and, to
those who were troubled, rest, such a rest as will be not only a final
period to, but a full reward of, all their troubles, 2 Th. 1:6, 7. Those
whose hearts tremble for the ark of God, and who are under a concern for
his church in the world, may silence their fears with this, God will
take the work into his own hands. Your God will come, who pleads your
cause and owns your interest, even God himself, who is God alone.

### Verses 5-10

\"Then, when your God shall come, even Christ, to set up his kingdom in
the world, to which all the prophets bore witness, especially towards
the conclusion of their prophecies of the temporal deliverances of the
church, and this evangelical prophet especially-then look for great
things.\"

`I.` Wonders shall be wrought in the kingdoms both of nature and grace,
wonders of mercy wrought upon the children of men, sufficient to evince
that it is no less than a God that comes to us. 1. Wonders shall be
wrought on men\'s bodies (v. 5, 6): The eyes of the blind shall be
opened; this was often done by our Lord Jesus when he was here upon
earth, with a word\'s speaking, and one he gave sight to that was born
blind, Mt. 9:27; 12:22; 20:30; Jn. 9:6. By his power the ears of the
deaf also were unstopped, with one word. Ephphatha-Be opened, Mk. 7:34.
Many that were lame had the use of their limbs restored so perfectly
that they could not only go, but leap, and with so much joy to them that
they could not forbear leaping for joy, as that impotent man, Acts 3:8.
The dumb also were enabled to speak, and then no marvel that they were
disposed to sing for joy, Mt. 9:32, 33. These miracles Christ wrought to
prove that he was sent of God (Jn. 3:2), nay, working them by his own
power and in his own name, he proved that he was God, the same who at
first made man\'s mouth, the hearing ear, and the seeing eye. When he
would prove to John\'s disciples his divine mission he did it by
miracles of this kind, in which this scripture was fulfilled. 2.
Wonders, greater wonders, shall be wrought on men\'s souls. By the word
and Spirit of Christ those that were spiritually blind were enlightened
(Acts 26:18), those that were deaf to the calls of God were made to hear
them readily, so Lydia, whose heart the Lord opened, so that she
attended, Acts 16:14. Those that were impotent to every thing that is
good by divine grace are made, not only able for it, but active in it,
and run the way of God\'s commandments. Those also that were dumb, and
knew not how to speak of God or to God, having their understandings
opened to know him, shall thereby have their lips opened to show forth
his praise. The tongue of the dumb shall sing for joy, the joy of God\'s
salvation. Praise shall be perfected out of the mouth of babes and
sucklings.

`II.` The Spirit shall be poured out from on high. There shall be waters
and streams, rivers of living water; when our Saviour spoke of these as
the fulfilling of the scripture, and most probably of this scripture,
the evangelist tells us, He spoke of the Spirit (Jn. 7:38, 39), as does
also this prophet (ch. 32:15); so here (v. 6), in the wilderness, where
one would least expect it, shall waters break out. This was fulfilled
when the Holy Ghost fell upon the Gentiles that heard the word (Acts
10:44); then were the fountains of life opened, whence streams flowed,
that watered the earth abundantly. These waters are said to break out,
which denotes a pleasing surprise to the Gentile world, such as brought
them, as it were, into a new world. The blessed effect of this shall be
that the parched ground shall become a pool, v. 7. Those that laboured
and were heavily laden, under the burden of guilt, and were scorched
with the sense of divine wrath, found rest, and refreshment, and
abundant comforts in the gospel. In the thirsty land, where no water
was, nor ordinances (Ps. 63:1), there shall be springs of water, a
gospel ministry, and by that the administration of all gospel ordinances
in their purity and plenty, which are the river that makes glad the city
of our God, Ps. 46:4. In the habitation of dragons, who chose to dwell
in the parched scorched ground (ch. 34:9, 13), these waters shall flow,
and dispossess them, so that, where each lay shall be grass with reeds
and rushes, great plenty of useful productions. Thus it was when
Christian churches were planted, and flourished greatly, in the cities
of the Gentiles, which, for many ages, had been habitations of dragons,
or devils rather, as Babylon (Rev. 18:2); when the property of the
idols\' temples was altered, and they were converted to the service of
Christianity, then the habitations of dragons became fruitful fields.

`III.` The way of religion and godliness shall be laid open: it is here
called the way of holiness (v. 8) the way both of holy worship and a
holy conversation. Holiness is the rectitude of the human nature and
will, in conformity to the divine nature and will. The way of holiness
is that course of religious duties in which men ought to walk and press
forward, with an eye to the glory of God and their own felicity in the
enjoyment of him. \"When our God shall come to save us he shall chalk
out to us this way by his gospel, so as it had never been before
described.\" 1. It shall be an appointed way; not a way of sufferance,
but a highway, a way into which we are directed by a divine authority
and in which we are protected by a divine warrant. It is the King\'s
highway, the King of Kings\' highway, in which, though we may be
waylaid, we cannot be stopped. The way of holiness is the way of God\'s
commandments; it is (as highways usually are) the good old way, Jer.
6:16. 2. It shall be an appropriated way, the way in which God will
bring his own chosen to himself, but the unclean shall not pass over it,
either to defile it or to disturb those that walk in it. It is a way by
itself, distinguished from the way of the world, for it is a way of
separation from, and nonconformity to, this world. It shall be for those
whom the Lord has set apart for himself (Ps. 4:3), shall be reserved for
them: The redeemed shall walk there, and the satisfaction they take in
these ways of pleasantness shall be out of the reach of molestation from
an evil world. The unclean shall not pass over it, for it shall be a
fair way; those that walk in it are the undefiled in the way, who escape
the pollution that is in the world. 3. It shall be a straight way: The
wayfaring men, who choose to travel in it, though fools, of weak
capacity in other things, shall have such plain directions from the word
and Spirit of God in this way that they shall not err therein; not that
they shall be infallible even in their own conduct, or that they shall
in nothing mistake, but they shall not be guilty of any fatal
misconduct, shall not so miss their way but that they shall recover it
again, and get well to their journey\'s end. Those that are in the
narrow way, though some may fall into one path and others into another,
not all equally right, but all meeting at last in the same end, shall
yet never fall into the broad way again; the Spirit of truth shall lead
them into all truth that is necessary for them. Note, The way to heaven
is a plain way, and easy to hit. God has chosen the foolish things of
the world, and made them wise to salvation. Knowledge is easy to him
that understands. 4. It shall be a safe way: No lion shall be there, nor
any ravenous beast (v. 9), none to hurt or destroy. Those that keep
close to this way keep out of the reach of Satan the roaring lion, that
wicked one touches them not. Those that walk in the way of holiness may
proceed with a holy security and serenity of mind, knowing that nothing
can do them any real hurt; they shall be quiet from the fear of evil. It
was in Hezekiah\'s days, some time after the captivity of the ten
tribes, that God, being displeased with the colonies settled there, sent
lions among them, 2 Ki. 17:25. But Judah keeps her integrity, and
therefore no lions shall be there. Those that walk in the way of
holiness must separate themselves from the unclean and the ravenous,
must save themselves from an untoward generation; hoping that they
themselves are of the redeemed, let them walk with the redeemed who
shall walk there.

`IV.` The end of this way shall be everlasting joy, v. 10. This precious
promise of peace now will end shortly in endless joys and rest for the
soul. Here is good news for the citizens of Zion, rest to the weary: The
ransomed of the Lord, who therefore ought to follow him wherever he goes
(Rev. 14:4), shall return and come to Zion, 1. To serve and worship God
in the church militant: they shall deliver themselves out of Babylon
(Zec. 2:7), shall ask the way to Zion (Jer. 50:5), and shall find the
way ch. 52:12. God will open to them a door of escape out of their
captivity, and it shall be an effectual door, though there be many
adversaries. They shall join themselves to the gospel church, that Mount
Zion, that city of the living God, Heb. 12:22. They shall come with
songs of joy and praise for their deliverance out of Babylon, where they
wept upon every remembrance of Zion, Ps. 137:1. Those that by faith are
made citizens of the gospel Zion may go on their way rejoicing (Acts
8:39); they shall sing in the ways of the Lord, and be still praising
him. They rejoice in Christ Jesus, and the sorrows and signs of their
convictions are made to flee away by the power of divine consolations.
Those that mourn are blessed, for they shall be comforted. 2. To see and
enjoy God in the church triumphant; those that walk in the way of
holiness, under guidance of their Redeemer, shall come to Zion at last,
to the heavenly Zion, shall come in a body, shall all be presented
together, faultless, at the coming of Christ\'s glory with exceeding joy
(Jude 24; Rev. 7:17); they shall come with songs. When God\'s people
returned out of Babylon to Zion they came weeping (Jer. 50:4); but they
shall come to heaven singing a new song, which no man can learn, Rev.
14:3. When they shall enter into the joy of their Lord it shall be what
the joys of this world never could be everlasting joy, without mixture,
interruption, or period. It shall not only fill their hearts, to their
own perfect and perpetual satisfaction, but it shall be upon their
heads, as an ornament of grace and a crown of glory, as a garland worn
in token of victory. Their joy shall be visible, and no longer a secret
thing, as it is here in this world; it shall be proclaimed, to the glory
of God and their mutual encouragement. They shall then obtain the joy
and gladness which they could never expect on this side heaven; and
sorrow and sighing shall flee away for ever, as the shadows of the night
before the rising sun. Thus these prophecies, which relate to the
Assyrian invasion, conclude, for the support of the people of God under
that calamity, and to direct their joy, in their deliverance from it, to
something higher. Our joyful hopes and prospects of eternal life should
swallow up both all the sorrows and all the joys of this present time.
