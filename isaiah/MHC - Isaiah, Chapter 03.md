Isaiah, Chapter 3
=================

Commentary
----------

The prophet, in this chapter, goes on to foretel the desolations that
were coming upon Judah and Jerusalem for their sins, both that by the
Babylonians and that which completed their ruin by the Romans, with some
of the grounds of God\'s controversy with them. God threatens, `I.` To
deprive them of all the supports both of their life and of their
government (v. 1-3). `II.` To leave them to fall into confusion and
disorder (v. 4, 5, 12). `III.` To deny them the blessing of magistracy (v.
6-8). `IV.` To strip the daughters of Zion of their ornaments (v. 17-24).
`V.` To lay all waste by the sword of war (v. 25, 26). The sins that
provoked God to deal thus with them were, 1. Their defiance of God (v.
8). 2. Their impudence (v. 9). 3. The abuse of power to oppression and
tyranny (v. 12-15). 4. The pride of the daughters of Zion (v. 16). In
the midst of the chapter the prophet is directed how to address
particular persons. `(1.)` To assure good people that it should be well
with them, notwithstanding those general calamities (v. 10). `(2.)` To
assure wicked people that, however God might, in judgment, remember
mercy, yet it should go ill with them (v. 11). O that the nations of the
earth, at this day, would hearken to rebukes and warnings which this
chapter gives!

### Verses 1-8

The prophet, in the close of the foregoing chapter, had given a
necessary caution to all not to put confidence in man, or any creature;
he had also given a general reason for that caution, taken from the
frailty of human life and the vanity and weakness of human powers. Here
he gives a particular reason for it-God was now about to ruin all their
creature-confidences, so that they should meet with nothing but
disappointments in all their expectations from them (v. 1): The stay and
the staff shall be taken away, all their supports, of what kind soever,
all the things they trusted to and looked for help and relief from.
Their church and kingdom had now grown old and were going to decay, and
they were (after the manner of aged men, Zec. 8:4) leaning on a staff:
now God threatens to take away their staff, and then they must fall of
course, to take away the stays of both the city and the country, of
Jerusalem and of Judah, which are indeed stays to one another, and, if
one fail, the other feels from it. He that does this is the Lord, the
Lord of hosts-Adon, the Lord that is himself the stay or foundation; if
that stay depart, all other stays certainly break under us, for he is
the strength of them all. He that is the Lord, the ruler, that has
authority to do it, and the Lord of hosts, that has the ability to do
it, he shall take away the stay and the staff. St. Jerome refers this to
the sensible decay of the Jewish nation after they had crucified our
Saviour, Rom. 11:9, 10. I rather take it as a warning to all nations not
to provoke God; for if they make him their enemy, he can and will thus
make them miserable. Let us view the particulars.

`I.` Was their plenty a support to them? It is so to any people; bread is
the staff of life: but God can take away the whole stay of bread, and
the whole stay of water; and it is just with him to do so when fulness
of bread becomes an iniquity (Eze. 16:49), and that which was given to
be provision for the life is made provision for the lusts. He can take
away the bread and the water by withholding the rain, Deu. 28:23, 24.
Or, if he allow them, he can take away the stay of bread and the stay of
water by withholding his blessing, by which man lives, and not by bread
only, and which is the staff of bread (Mt. 4:4.), and then the bread is
not nourishing nor the water refreshing, Hag. 1:6. Christ is the bread
of life and the water of life; if he be our stay, we shall find that
this is a good part not to be taken away, Jn. 4:14; 6:27.

`II.` Was their army a support to them-their generals, and commanders,
and military men? These shall be taken away, either cut off by the sword
or so discouraged with the defeats they meet with that they shall throw
up their commissions and resolve to act no more; or they shall be
disabled by sickness, or dispirited, so as to be unfit for business; The
mighty men, and the man of war, and even the inferior officer, the
captain of fifty, shall be removed. It bodes ill with a people when
their valiant men are lost. Let not the strong man therefore glory in
his strength, nor any people trust too much to their mighty men; but let
the strong people glorify God and the city of the terrible nations fear
him, who can make them weak and despicable, ch. 25:3.

`III.` Were their ministers of state a support to them-their learned men,
their politicians, their clergy, their wits and virtuoso? These also
should be taken away-the judges, who were skilled in the laws, and
expert in administering justice,-the prophets, whom they used to consult
in difficult cases,-the prudent, who were celebrated as men of sense and
sagacity above all others and were assistants to the judges, the
diviners (so the word is), those who used unlawful arts, who, though
rotten stays, yet were stayed on, (but it may be taken, as we read it,
in a good sense),-the ancients, elders in age, in office,-the honourable
man, the gravity of whose aspect commands reverence and whose age and
experience make him fit to be a counsellor. Trade is one great support
to a nation, even manufactures and handicraft trades; and therefore,
when the whole stay is broken, the cunning artificer too shall be taken
away; and the last is the eloquent orator, the man skilful of speech,
who in some cases may do good service, though he be none of the prudent
or the ancient, by putting the sense of others in good language. Moses
cannot speak well, but Aaron can. God threatens to take these away, that
is, 1. To disable them for the service of their country, making judges
fools, taking away the speech of the trusty and the understanding of the
aged, Job 12:17, etc. Every creature is that to us which God makes it to
be; and we cannot be sure that those who have been serviceable to us
shall always be so. 2. To put an end to their days; for the reason why
princes are not to be trusted in is because their breath goeth forth,
Ps. 146:3, 4. Note, The removal of useful men by death, in the midst of
their usefulness, is a very threatening symptom to any people.

`IV.` Was their government a support to them? It ought to have been so;
it is the business of the sovereign to bear up the pillars of the land,
Ps. 75:3. But it is here threatened that this stay should fail them.
When the mighty men and the prudent are removed children shall be their
princes-children in age, who must be under tutors and governors, who
will be clashing with one another and making a prey of the young king
and his kingdom-children in understanding and disposition, childish men,
such as are babes in knowledge, no more fit to rule than a child in the
cradle. These shall rule over them, with all the folly, fickleness, and
frowardness, of a child. And woe unto thee, O land! when thy king is
such a one! Eccl. 10:16.

`V.` Was the union of the subjects among themselves, their good order and
the good understanding and correspondence that they kept with one
another, a stay to them? Where this is the case a people may do better
for it, though their princes be not such as they should be; but it is
here threatened that God would send an evil spirit among them too (as
Jdg. 9:23), which would make them, 1. Injurious and unneighbourly one
towards another (v. 5): \"The people shall be oppressed every one by his
neighbour,\" and their princes, being children, will take no care to
restrain the oppressors or relieve the oppressed, nor is it to any
purpose to appeal to them (which is a temptation to every man to be his
own avenger), and therefore they bite and devour one another and will
soon be consumed one of another. Then homo homini lupus-man becomes a
wolf to man; jusque datum sceleri-wickedness receives the stamp of law;
nec hospes ab hospite tutus-the guest and the host are in danger from
each other. 2. Insolent and disorderly towards their superiors. It is as
ill an omen to a people as can be when the rising generation among them
are generally untractable, rude, and ungovernable, when the child
behaves himself proudly against the ancient, whereas he should rise up
before the hoary head and honour the face of the old man, Lev. 19:32.
When young people are conceited and pert, and behave scornfully towards
their superiors, their conduct is not only a reproach to themselves, but
of ill consequence to the public; it slackens the reins of government
and weakens the hands that hold them. It is likewise ill with a people
when persons of honour cannot support their authority, but are affronted
by the base and beggarly, when judges are insulted and their powers set
at defiance by the mob. Those have a great deal to answer for who do
this.

`VI.` It is some stay, some support, to hope that, though matters may be
now ill-managed, yet other may be raised up, who may manage better? Yet
this expectation also shall be frustrated, for the case shall be so
desperate that no man of sense or substance will meddle with it.

`1.` The government shall go a begging, v. 6. Here, `(1.)` It is taken for
granted that there is no way of redressing all these grievances, and
bringing things into order again, but by good magistrates, who shall be
invested with power by common consent, and shall exert that power for
the good of the community. And it is probable that this was, in many
places, the true origin of government; men found it necessary to unite
in a subjection to one who was thought fit for such a trust, in order to
the welfare and safety of them all, being aware that they must either be
ruled or ruined. Here therefore is the original contract: \"Be thou our
ruler, and we will be subject to thee, and let this ruin be under thy
hand, to be repaired and restored, and then to be preserved and
established, and the interests of it advanced, ch. 58:12. Take care to
protect us by the sword of war from being injured from abroad, and by
the sword of justice from being injurious to another, and we will bear
faith and true allegiance to thee.\" `(2.)` The case is represented as
very deplorable, and things as having come to a sad pass; for, `[1.]`
Children being their princes, every man will think himself fit to
prescribe who shall be a magistrate, and will be for preferring his own
relations; whereas, if the princes were as they should be, it would be
left entirely to them to nominate the rulers, as it ought to be. `[2.]`
Men will find themselves under a necessity even of forcing power into
the hands of those that are thought to be fit for it: A man shall take
hold by violence of one to make him a ruler, perceiving him ready to
resist the motion: nay, he shall urge it upon his brother; whereas,
commonly, men are not willing that their equals should be their
superiors, witness the envy of Joseph\'s brethren. `[3.]` It will be
looked upon as ground sufficient for the preferring of a man to be a
ruler that he has clothing better than his neighbours-a very poor
qualification to recommend a man to a place of trust in the government.
It was a sign that the country was much impoverished when it was a rare
thing to find a man that had good clothes, or could afford to buy
himself an alderman\'s gown or a judge\'s robes; and it was proof enough
that the people were very unthinking when they had so much respect to a
man in gay clothing, with a gold ring (Jam. 2:2, 3), that, for the sake
thereof, they would make him their ruler. It would have been some sense
to have said, \"Thou hast wisdom, integrity, experience; be thou our
ruler.\" But it was a jest to say, Thou hast clothing; be thou our
ruler. A poor wise man, though in vile raiment, delivered a city, Eccl.
9:15. We may allude to this to show how desperate the case of fallen man
was when our Lord Jesus was pleased to become our brother, and, though
he was not courted, offered himself to be our ruler and Saviour, and to
take this ruin under his hand.

`2.` Those who are thus pressed to come into office will swear themselves
off, because, though they are taken to be men of some substance, yet
they know themselves unable to bear the charges of the office and to
answer the expectations of those that choose them (v. 7): He shall swear
(shall lift up the hand, the ancient ceremony used in taking the oath) I
will not be a healer; make not me a ruler. Note, Rulers must be healers,
and good rulers will be so; they must study to unite their subjects, and
not to widen the differences that are among them. Those only are fit for
government that are of a meek, quiet, healing, spirit. They must also
heal the wounds that are given to any of the interests of their people,
by suitable applications. But why will he not be a ruler? Because in my
house is neither bread nor clothing. `(1.)` If he said true, it was a sign
that men\'s estates were sadly ruined when even those who made the best
appearance really wanted necessaries-a common case, and a piteous one.
Some who, having lived fashionably, are willing to put the best side
outwards, are yet, if the truth were known, in great straits, and go
with heavy hearts for want of bread and clothing. `(2.)` If he did not
speak truth, it was a sign that men\'s consciences were sadly debauched,
when, to avoid the expense of an office, they would load themselves with
the guilt of perjury, and (which is the greatest madness in the world)
would damn their souls to save their money, Mt. 16:26. `(3.)` However it
was, it was a sign that the case of the nation was very bad when nobody
was willing to accept a place in the government of it, as despairing to
have either credit or profit by it, which are the two things aimed at in
men\'s common ambition of preferment.

`3.` The reason why God brought things to this sad pass, even among his
own people (which is given either by the prophet or by him that refused
to be a ruler); it was not for want of good will to his country, but
because he saw the case desperate and past relief, and it would be to no
purpose to attempt it (v. 8): Jerusalem is ruined and Judah is fallen;
and they may thank themselves. They have brought their destruction upon
their own heads, for their tongue and their doings are against the Lord;
in word and action they broke the law of God and therein designed an
affront to him; they wilfully intended to offend him, in contempt of his
authority and defiance of his justice. Their tongue was against the
Lord, for they contradicted his prophets; and their doings were no
better, for they acted as they talked. It was an aggravation of their
sin that God\'s eye was upon them, and that his glory was manifested
among them; but they provoked him to his face, as if the more they knew
of his glory the greater pride they took in slighting it, and turning it
into shame. And this, this, is it for which Jerusalem is ruined. Note,
The ruin both of persons and people is owing to their sins. If they did
not provoke God, he would do them no hurt, Jer. 25:6.

### Verses 9-15

Here God proceeds in his controversy with his people. Observe,

`I.` The ground of his controversy. It was for sin that God contended with
them; if they vex themselves, let them look a little further and they
will see that they must thank themselves: Woe unto their souls! For they
have rewarded evil unto themselves. Alas for their souls! (so it may be
read, in a way of lamentation), for they have procured evil to
themselves, v. 9. Note, The condition of sinners is woeful and very
deplorable. Note, also, It is the soul that is damaged and endangered by
sin. Sinners may prosper in their outward estates, and yet at the same
time there may be a woe to their souls. Note, further, Whatever evils
befals sinners it is of their own procuring, Jer. 2:19. That which is
here charged upon then is, 1. That the shame which should have
restrained them from their sins was quite thrown off and they had grown
impudent, v. 9. This hardens men against repentance, and ripens them for
ruin, as much as anything: The show of their countenance doth witness
against them that their minds are vain, and lewd, and malicious; their
eyes declare plainly that they cannot cease from sin, 2 Pt. 2:14. One
may look them in the face and guess at the desperate wickedness that
there is in their hearts: They declare their sin as Sodom, so impetuous,
so imperious, are their lusts, and so impatient of the least check, and
so perfectly are all the remaining sparks of virtue extinguished in
them. The Sodomites declared their sin, not only by the exceeding
greatness of it (Gen. 13:13), so that it cried to heaven (Gen. 18:20),
but by their shameless owning of that which was most shameful (Gen.
19:5); and thus Judah and Jerusalem did: they were so far from hiding it
that they gloried in it, in the bold attempts they made upon virtue, and
the victory they gained over their own convictions. They had a whore\'s
forehead (Jer. 3:3) and could not blush, Jer. 6:15. Note, Those that
have grown impudent in sin are ripe for ruin. Those that are past shame
(we say) are past grace, and then past hope. 2. That their guides, who
should direct them in the right way, put them out of the way (v. 12):
\"Those who lead thee (the princes, priests, and prophets) mislead thee;
they cause thee to err.\" Either they preached to them that which was
false and corrupt, or, if they preached that which was true and good,
they contradicted it by their practices, and the people would soon
follow a bad example than a good exhortation. Thus they destroyed the
ways of their paths, pulling down with one hand what they built up with
the other. Que te beatificant-Those that call thee blessed cause thee to
err; so some read it. Their priests applauded them, as if nothing were
amiss among them, cried Peace, peace, to them, as if they were in no
danger; and thus they caused them to go on in their errors. 3. That
their judges, who should have patronized and protected the oppressed,
were themselves the greatest oppressors, v. 14, 15. The elders of the
people, and the princes, who had learning and could not but know better
things, who had great estates and were not under the temptation of
necessity to encroach upon those about them, and who were men of honour
and should have scorned to do a base thing, yet they have eaten up the
vineyard. God\'s vineyard, which they were appointed to be the dressers
and keepers of, they burnt (so the word signifies); they did as ill by
it as its worst enemies could do, Ps. 80:16. Or the vineyards of the
poor they wrested out of their possession, as Jezebel did Naboth\'s, or
devoured the fruits of them, fed their lusts with that which should have
been the necessary food of indigent families; the spoil of the poor was
hoarded up in their houses; when God came to search for stolen goods
there he found it, and it was a witness against them. It was to be had,
and they might have made restitution, but would not. God reasons with
these great men (v. 15): \"What mean you, that you beat my people into
pieces? What cause have you for it? What good does it do you?\" Or,
\"What hurt have they done you? Do you think you had power given you for
such a purpose as this?\" Note, There is nothing more unaccountable, and
yet nothing which must more certainly be accounted for, than the
injuries and abuses that are done to God\'s people by their persecutors
and oppressors. \"You grind the faces of the poor; you put them to as
much pain and terror as if they were ground in a mill, and as certainly
reduce them to dust by one act of oppression after another.\" Or,
\"Their faces are bruised and crushed with the blows you have given
them; you have not only ruined their estates, but have given them
personal abuses.\" Our Lord Jesus was smitten on the face, Mt. 26:67.

`II.` The management of this controversy. 1. God himself is the
prosecutor (v. 13): The Lord stands up to plead, or he sets himself to
debate the matter, and he stands to judge the people, to judge for those
that were oppressed and abused; and he will enter into judgment with the
princes, v. 14. Note, The greatest of men cannot exempt or secure
themselves from the scrutiny and sentence of God\'s judgment, nor demur
to the jurisdiction of the court of heaven. 2. The indictment is proved
by the notorious evidence of the fact: \"Look upon the oppressors, and
the show of their countenance witnesses against them (v. 9); look upon
the oppressed, and you see how their faces are battered and abused,\" v.
15. 3. The controversy is already begun in the change of the ministry.
To punish those that had abused their power to bad purposes God sets
those over them that had not sense to use their power to any good
purposes: Children are their oppressors, and women rule over them (v.
12), men that have as weak judgments and strong passions as women and
children: this was their sin, that their rulers were such, and it became
a judgment upon them.

`III.` The distinction that shall be made between particular persons, in
the prosecution of this controversy (v. 10, 11): Say to the righteous,
It shall be well with thee. Woe to the wicked; it shall be ill with him.
He had said (v. 9), they have rewarded evil to themselves, in proof of
which he here shows that God will render to every man according to his
works. Had they been righteous, it would have been well with them; but,
if it be ill with them, it is because they are wicked and will be so.
Thus God stated the matter to Cain, to convince him that he had no
reason to be angry, Gen. 4:7. Or it may be taken thus: God is
threatening national judgments, which will ruin the public interests.
Now, 1. Some good people might fear that they should be involved in that
ruin, and therefore God bids the prophets comfort them against those
fears: \"Whatever becomes of the unrighteous nation, let the righteous
man know that he shall not be lost in the crowd of sinners; the Judge of
all the earth will not slay the righteous with the wicked (Gen. 18:25);
no, assure him, in God\'s name, that it shall be well with him. The
property of the trouble shall be altered to him, and he shall be hidden
in the day of the Lord\'s anger. He shall have divine supports and
comforts, which shall abound as afflictions abound, and so it shall be
well with him.\" When the whole stay of bread is taken away, yet in the
day of famine the righteous shall be satisfied; they shall eat the fruit
of their doings-they shall have the testimony of their consciences for
them that they kept themselves pure from the common iniquity, and
therefore the common calamity is not the same thing to them that it is
to others; they brought no fuel to the flame, and therefore are not
themselves fuel for it. 2. Some wicked people might hope that they
should escape that ruin, and therefore God bids the prophets shake their
vain hopes: \"Woe to the wicked; it shall be ill with him, v. 11. To him
the judgments shall have sting, and there shall be wormwood and gall in
the affliction and misery.\" There is a woe to wicked people, and,
though they may think to shelter themselves from public judgments, yet
it shall be ill with them; it will grow worse and worse with them if
they repent not, and the worst of all will be at last; for the reward of
their hands shall be given them, in the day when every man shall receive
according to the things done in the body.

### Verses 16-26

The prophet\'s business was to show all sorts of people what they had
contributed to the national guilt and what share they must expect in the
national judgments that were coming. Here he reproves and warns the
daughters of Zion, tells the ladies of their faults; and Moses, in the
law, having denounced God\'s wrath against the tender and delicate woman
(the prophets being a comment upon the law, Deu. 28:56), he here tells
them how they shall smart by the calamities that are coming upon them.
Observe,

`I.` The sin charged upon the daughters of Zion, v. 16. The prophet
expressly vouches God\'s authority for what he said. lest it should be
thought it was unbecoming in him to take notice of such things, and
should be resented by the ladies: The Lord saith it. \"Whether they will
hear, or whether they will forbear, let them know that God takes notice
of, and is much displeased with, the folly and vanity of proud women,
and his law takes cognizance even of their dress.\" Two things that here
stand indicted for-haughtiness and wantonness, directly contrary to that
modesty, shamefacedness, and sobriety, with which women ought to adorn
themselves, 1 Tim. 2:9. They discovered the disposition of their mind by
their gait and gesture, and the lightness of their carriage. They are
haughty, for they walk with stretched-forth necks, that they may seem
tall, or, as thinking nobody good enough to speak to them or to receive
a look or a smile from them. Their eyes are wanton, deceiving (so the
word is); with their amorous glances they draw men into their snares.
They affect a formal starched way of going, that people may look at
them, and admire them, and know they have been at the dancing-school,
and have learned the minuet-step. They go mincing, or nicely tripping,
not willing to set so much as the sole of their foot to the ground, for
tenderness and delicacy. They make a tinkling with their feet, having,
as some think, chains, or little bells, upon their shoes, that made a
noise: they go as if they were fettered (so some read it), like a horse
tramelled, that he may learn to pace. Thus Agag came delicately, 1 Sa.
15:32. Such a nice affected mien is not only a force upon that which is
natural, and ridiculous before men, men of sense; but as it is an
evidence of a vain mind, it is offensive to God. And two things
aggravated it here: 1. That these were the daughters of Zion, the holy
mountain, who should have behaved with the gravity that becomes women
professing godliness. 2. That it should seem, by the connexion, they
were the wives and daughters of the princes who spoiled and oppressed
the poor (v. 14, 15) that they might maintain the pride and luxury of
their families.

`II.` The punishments threatened for this sin; and they answer the sin as
face answers to face in a glass, v. 17, 18. 1. They walked with
stretched-forth necks, but God will smite with a scab the crown of their
head, which shall lower their crests, and make them ashamed to show
their heads, being obliged by it to cut off their hair. Note, Loathsome
diseases are often sent as the just punishment of pride, and are
sometimes the immediate effect of lewdness, the flesh and the body being
consumed by it. 2. They cared not what they laid out in furnishing
themselves with great variety of fine clothes; but God will reduce them
to such poverty and distress that they shall not have clothes sufficient
to cover their nakedness, but their uncomeliness shall be exposed
through their rags. 3. They were extremely fond and proud of their
ornaments; but God will strip them of those ornaments, when their houses
shall be plundered, their treasures rifled, and they themselves led into
captivity. The prophet here specifies many of the ornaments which they
used as particularly as if he had been the keeper of their wardrobe or
had attended them in their dressing-room. It is not at all material to
enquire what sort of ornaments these respectively were and whether the
translations rightly express the original words; perhaps 100 years hence
the names of some of the ornaments that are now in use in our own land
will be as little understood as some of those here mentioned now are.
Fashions alter, and so do the names of them; and yet the mention of them
is not in vain, but is designed to expose the folly of the daughters of
Zion; for, `(1.)` Many of these things, we may suppose, were very odd and
ridiculous, and, if they had not been in fashion, would have been hooted
at. They were fitter to be toys for children to play with than ornaments
for grown people to go to Mount Zion in. `(2.)` Those things that were
decent and convenient, as the linen, the hoods, and the veils, needed
not be provided in such abundance and variety. It is necessary to have
apparel and proper that all should have it according to their rank; but
what occasion was there for so many changeable suits of apparel (v. 22),
that they might not be seen two days together in the same suit? \"They
must have (as the homily against excess of apparel speaks) one gown for
the day, another for the night-one long, another short-one for the
working day, another for the holy-day-one of this colour, another of
that colour-one of cloth, another of silk or damask-one dress afore
dinner, another after-one of the Spanish fashion, another Turkey-and
never content with sufficient.\" All this, as it is an evidence of pride
and vain curiosity, so must needs spend a great deal in gratifying a
base lust that ought to be laid out in works of piety and charity; and
it is well if poor tenants be not racked, or poor creditors defrauded to
support it. `(3.)` The enumeration of these things intimates what care
they were in about them, how much their hearts were upon them, what an
exact account they kept of them, how nice and critical they were about
them, how insatiable their desire was of them, and how much of their
comfort was bound up in them. A maid could forget none of these
ornaments, though they were ever so many (Jer. 2:32), but they would
report them as readily, and talk of them with as much pleasure, as if
they had been things of the greatest moment. The prophet did not speak
of these things as in themselves sinful (they might lawfully be had and
used), but as things which they were proud of and should therefore be
deprived of.

`III.` They were very nice and curious about their clothes; but God would
make those bodies of theirs, which were at such expense to beautify and
make easy, a reproach and burden to them (v. 24): Instead of sweet smell
(those tablets, or boxes, of perfume, houses of the soul or breath, as
they are called, v. 20, margin) there shall be stink, garments grown
filthy with being long worn, or from some loathsome disease or plasters
for the cure of it. Instead of a rich embroidered girdle used to make
the clothes sit tight, there shall be a rent, a rending of the clothes
for grief, or old rotten clothes rent into rags. Instead of well-set
hair, curiously plaited and powdered, there shall be baldness, the hair
being plucked off or shaven, as was usual in times of great affliction
(ch. 15:2; Jer. 16:6), or in great servitude, Eze. 29:18. Instead of a
stomacher, or a scarf or sash, there shall be a girding of sackcloth, in
token of deep humiliation; and burning instead of beauty. Those that had
a good complexion, and were proud of it, when they are carried into
captivity shall be tanned and sun-burnt; and it is observed that the
best faces are soonest injured by the weather. From all this let us
learn, 1. Not to be nice and curious about our apparel, not to affect
that which is gay and costly, nor to be proud of it. 2. Not to be secure
in the enjoyment of any of the delights of sense, because we know not
how soon we may be stripped of them, nor what straits we may be reduced
to.

`IV.` They designed by these ornaments to charm the gentlemen, and win
their affections (Prov. 7:16, 17), but there shall be none to be charmed
by them (v. 25): Thy men shall fall by the sword, and the mighty in the
war, The fire shall consume them, and then the maidens shall not be
given in marriage; as it is, Ps. 78:63. When the sword comes with
commission the mighty commonly fall first by it, because they are most
forward to venture. And, when Zion\'s guards are cut off, no marvel that
Zion\'s gates lament and mourn (v. 26), the enemies having made
themselves masters of them; and the city itself, being desolate, being
emptied or swept, shall sit upon the ground like a disconsolate widow.
If sin be harboured within the walls, lamentation and mourning are near
the gates.
