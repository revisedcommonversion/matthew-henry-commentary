Isaiah, Chapter 58
==================

Commentary
----------

The prophet, in this chapter, has his commission and charge renewed to
reprove the sinners in Zion, particularly the hypocrites, to show them
their transgressions (v. 1). It is intended for admonition and warning
to all hypocrites, and is not to be confined to those of any one age.
Some refer it primarily to those at that time when Isaiah prophesied;
see 33:14; 29:13. Others to the captives in Babylon, the wicked among
them, to whom the prophet had declared there was no peace 57:21. Against
the terror of that word they thought to shelter themselves with their
external performances, particularly their fastings, which they kept up
in Babylon, and for some time after their return to their own land, Zec.
7:3, etc. The prophet therefore here shows them that their devotions
would not entitle them to peace while their conversations were not at
all of a piece with them. Others think it is principally intended
against the hypocrisy of the Jews, especially the Pharisees before and
in our Saviour\'s time: they boasted of their fastings, but Christ (as
the prophet here) showed them their transgressions (Mt. 23), much the
same with those they are here charged with. Observe, `I.` The plausible
profession of religion which they made (v. 2). `II.` The boasts they made
of that profession, and the blame they laid upon God for taking no more
notice of it (v. 3). `III.` The sins they are charged with, which spoiled
the acceptableness of their fasts (v. 4, 5). `IV.` Instructions given them
how to keep fasts aright (v. 6, 7). `V.` Precious promises made to those
who do so keep fasts (v. 8-12). `VI.` The like precious promises made to
those that sanctify sabbaths aright (v. 13, 14).

### Verses 1-2

When our Lord Jesus promised to send the Comforter he added, When he
shall come he shall convince (Jn. 16:7, 8); for conviction must prepare
for comfort, and must also separate between the precious and the vile,
and mark out those to whom comfort does not belong. God had appointed
this prophet to comfort his people (ch. 40:1); here he appoints him to
convince them, and show them their sins.

`I.` He must tell them how very bad they really were, v. 1. 1. He must
deal faithfully and plainly with them. \"Though they are called the
people of God and the house of Jacob, though they wear an honourable
title and character, by which they are interested in many glorious
privileges, yet do not flatter them, but show them their transgressions
and their sins, be particular in telling them their faults, what sins
are committed among them, which they do not know of, nay, what sins are
committed by them which they do not acknowledge to be sins; though in
some things they are reformed, let them know that in other things they
are still as bad as ever. Show them their transgressions and their sins,
that is, all their transgressions in their sins, their sins and all the
aggravations of them,\" Lev. 16:21. Note, `(1.)` God sees sin in his
people, in the house of Jacob, and is displeased with it. `(2.)` They are
often unapt and unwilling to see their own sins, and need to have them
shown them, and to be told, Thus and thus thou hast done. 2. He must be
vehement and in good earnest herein, must cry aloud, and not spare, not
spare them (not touch them with his reproofs as if he were afraid of
hurting them, but search the wound to the bottom, lay it bare to the
bone), not spare himself or his own pains, but cry as loud as he can;
though he spend his strength and waste his spirits, though he get their
ill-will by it and get himself into an ill name, yet he must not spare.
He must lift up his voice like a trumpet, to make those hear of their
faults that were apt to be deaf when admonition was addressed to them.
He must give his reproofs in the most powerful and pressing manner
possible, as one who desired to be heeded. The trumpet does not give an
uncertain sound, but, though loud and shrill, is intelligible; so must
his alarms be, giving them warning of the fatal consequences of sin,
Eze. 33:3.

`II.` He must acknowledge how very good they seemed to be,
notwithstanding (v. 2): Yet they seek me daily. When the prophet went
about to show them their transgressions they pleaded that they could see
no transgressions which they were guilty of; for they were diligent and
constant in attending on God\'s worship-and what more would he have of
them? Now,

`1.` He owns the matter of fact to be true. As far as hypocrites do that
which is good, they shall not be denied the praise of it; let them make
their best of it. It is owned that they have a form of godliness. `(1.)`
They go to church, and observe their hours of prayer: They seek me
daily; they are very constant in their devotions and never omit them nor
suffer any thing to put them by. `(2.)` They love to hear good preaching;
They delight to know my ways, as Herod, who heard John gladly, and the
stony ground, that received the seed of the word with joy; it is to them
as a lovely song, Eze. 33:32. `(3.)` They seem to take great pleasure in
the exercises of religion and to be in their element when they are at
their devotions: They delight in approaching to God, not for his sake to
whom they approach, but for the sake of some pleasing circumstance, the
company, or the festival. `(4.)` They are inquisitive concerning their
duty and seem desirous only to know it, making no question but that then
they should do it: They ask of me the ordinances of justice, the rules
of piety in the worship of God, the rules of equity in their dealings
with men, both which are ordinances of justice. `(5.)` They appear to the
eye of the world as if they made conscience of doing their duty: They
are as a nation that did righteousness and forsook not the ordinances of
their God; others took them for such, and they themselves pretended to
be such. Nothing lay open to view that was a contradiction to their
profession, but they seemed to be such as they should be. Note, Men may
go a great way towards heaven and yet come short; nay, may go to hell
with a good reputation. But,

`2.` He intimates that this was so far from being a cover or excuse for
their sin that really it was an aggravation of it: \"Show them their
sins which they go on in notwithstanding their knowledge of good and
evil, sin and duty, and the convictions of their consciences concerning
them.\"

### Verses 3-7

Here we have, `I.` The displeasure which these hypocrites conceived
against God for not accepting the services which they themselves had a
mighty opinion of (v. 3): Wherefore have we fasted, say they, and thou
seest not? Thus they went in the way of Cain, who was angry at God, and
resented it as a gross affront that his offering was not accepted.
Having gone about to put a cheat upon God by their external services,
here they go about to pick a quarrel with God for not being pleased with
their services, as if he had not done fairly or justly by them. Observe,
`1.` How they boast of themselves, and magnify their own performances:
\"We have fasted, and afflicted our souls; we have not only sought God
daily (v. 2), but have kept some certain times of more solemn
devotion.\" Some think this refers to the yearly fast (which was called
the day of atonement), others to their arbitrary occasional fasts. Note,
It is common for unhumbled hearts to be proud of their professions of
humiliation, as the Pharisee (Lu. 18:12), I fast twice in the week. 2.
What they expected from their performances. They thought God should take
great notice of them, and own himself a debtor to them for their
services. Note, It is a common thing for hypocrites, while they perform
the external services of religion, to promise themselves that acceptance
with God which he has promised only to the sincere; as if they must be
accepted of course, or for a compliment. 3. How heinously they take it
that God had not put some particular marks of his favour upon them, that
he had not immediately delivered them out of their troubles and advanced
them to honour and prosperity. They charge God with injustice and
partiality, and seem resolved to throw up their religion, and justify
themselves in doing so with this, that they had found no profit in
praying to God, Job 21:14, 15; Mal. 3:14. Note, Reigning hypocrisy often
breaks out in daring impiety and an open contempt and reproach of God
and religion for that which the hypocrisy itself must bear all the blame
of. Sinners reflect upon religion as a hard and melancholy service, and
on which there is nothing to be got by, when really it is owing to
themselves that it seems so to them, because they are not sincere in it.

`II.` The true reason assigned why God did not accept their fastings, nor
answer the prayers they made on their fast-days; it was because they did
not fast aright-to God, even to him, Zec. 7:5. They fasted indeed, but
they persisted in their sins, and did not, as the Ninevites, turn every
one from his evil way; but in the day of their fast, notwithstanding the
professed humiliations and covenants of that day, they went on to find
pleasure, that is, to do whatsoever seemed right in their own eyes,
lawful or unlawful, quicquid libet, licet-making their inclinations
their law; though they seemed to afflict their souls, they still
gratified their lusts as much as ever. 1. They were as covetous and
unmerciful as ever: \"You exact all your labours from your servants, and
will neither release them according to the law nor relax the rigour of
their servitude.\" This was their fault before the captivity, Jer. 34:8,
9. It was no less their fault after their captivity, notwithstanding all
their solemn fasts, Neh. 5:5. \"You exact all your dues, your debts\"
(so some read it); \"you are as rigorous and severe in extorting what
you demand from those that are poor as ever you were, though it was at
the close of the yearly fast that the release was proclaimed.\" 2. They
were contentious and spiteful (v. 4): Behold, you fast for strife and
debate. When they proclaimed a fast to deprecate God\'s judgments, they
pretended to search for those sins which provoked God to threaten them
with his judgments, and under that pretence perhaps particular persons
were falsely accused, as Naboth in the day of Jezebel\'s fast, 1 Ki.
21:12. Or the contending parties among them upon those occasions were
bitter and severe in their reflections one upon another, one side crying
out, \"It is owing to you,\" and the other, \"It is owing to you, that
our deliverance is not wrought.\" Thus, instead of judging themselves,
which is the proper work of a fast-day, they condemned one another. They
fasted for strife, with emulation which should make the most plausible
appearance on a fast-day and humour the matter best. Nor was it only
tongue-quarrels that were fomented in the times of their fasting, but
they came to blows too: You smite with the fist of wickedness. The cruel
task-masters beat their servants, and the creditors their insolvent
debtors, whom they delivered to the tormentors; they abused poor
innocents with wicked hands. Now while they thus continued in sin, in
those very sins which were directly contrary to the intention of a
fasting day, `(1.)` God would not allow them the use of such solemnities:
\"You shall not fast at all if you fast as you do this day, causing your
voice to be heard on high, in the heat of your clamours one against
another, or in your devotions, which you perform so as to make them to
be taken notice of for ostentation. Bring me no more of these empty,
noisy, vain oblations,\" ch. 1:13. Note, Those are justly forbidden the
honour of a profession of religion that will not submit to the power of
it. `(2.)` He would not accept of them in the use of them: \"You shall not
fast, that is, it shall not be looked upon as a fast, nor shall the
voice of your prayers on those days be heard on high in heaven.\" Note,
Those that fast and pray, and yet go on in their wicked ways, do but
mock God and deceive themselves.

`III.` Plain instructions given concerning the true nature of a religious
fast.

`1.` In general, a fast is intended, `(1.)` For the honouring and pleasing
of God. It must be such a performance as he has chosen (v. 5); it must
be an acceptable day to the Lord, in the duties of which we must study
to approve ourselves to him and obtain his favour, else it is not a
fast, else there is nothing done to any purpose. `(2.)` For the humbling
and abasing of ourselves. A fast is a day to afflict the soul; if it do
not express a genuine sorrow for sin, and do not promote a real
mortification of sin, it is not a fast; the law of the day of atonement
was that on that day they should afflict their souls, Lev. 16:29. That
must be done on a fast-day which is a real affliction to the soul, as
far as it is yet unregenerate and unsanctified, though a real pleasure
and advantage to the soul as far as it is itself.

`2.` It concerns us therefore to enquire, on a fast-day, what it is that
will be acceptable to God, and afflictive to our corrupt nature, and
tending to its mortification.

`(1.)` We are here told negatively what is not the fast that God has
chosen, and which does not amount to the afflicting of the soul. `[1.]`
It is not enough to look demure, to put on a grave and melancholy
aspect, to bow down the head like a bulrush that is withered and broken:
as the hypocrites, that were of a sad countenance, and disfigured their
faces, that they might appear unto men to fast, Mt. 6:16. Hanging down
the head did indeed well enough become the publican, whose heart was
truly humbled and broken for sin, and who therefore, in token of that,
would not so much as lift up his eyes to heaven (Lu. 18:13); but when it
was only mimicked, as here, it was justly ridiculed: it is but hanging
down the head like a bulrush, which nobody regards or takes any notice
of. As the hypocrite\'s humiliations are but like the hanging down of a
bulrush, so his elevations in his hopes are but like the flourishing of
a bulrush (Job 8:11, 12), which, while it is yet in its greenness,
withers before any other herb. `[2.]` It is not enough to do penance, to
mortify the body a little, while the body of sin is untouched. It is not
enough for a man to spread sackcloth and ashes under him, which may
indeed give him some uneasiness for the present, but will soon be
forgotten when he returns to stretch himself upon his beds of ivory,
Amos 6:4. Wilt thou call this a fast? No, it is but the shadow and
carcase of a fast. Wilt thou call this an acceptable day to the Lord?
No, it is so far from being so that the hypocrisy of it is an
abomination to him. Note, The shows of religion, though they show ever
so fair in the eye of the world, will not be accepted of God without the
substance of it.

`(2.)` We are here told positively what is the fast that God has chosen,
what that is which will recommend a fast-day to the divine acceptance,
and what is indeed afflicting the soul, that is, crushing and subduing
the corrupt nature. It is not afflicting the soul for a day (as some
read it, v. 5) that will serve; no, it must be the business of our whole
lives. It is here required, `[1.]` That we be just to those with whom we
have dealt hardly. The fast that God has chosen consists in reforming
our lives and undoing what we have done amiss (v. 6): To loose the bands
of wickedness, the bands which we have wickedly tied, and by which
others are bound out from their right or bound down under severe usage.
Those which perhaps were at first bands of justice, tying men to pay a
due debt, become, when the debt is exacted with rigour from those whom
Providence has reduced and emptied, bands of wickedness, and they must
be loosed, or they will bring us into bonds of guilt much more terrible.
It is to undo the heavy burden laid on the back of the poor servant,
under which he is ready to sink. It is to let the oppressed go free from
the oppression which makes his life bitter to him. \"Let the prisoner
for debt that has nothing to pay be discharged, let the vexatious action
be quashed, let the servant that is forcibly detained beyond the time of
his servitude be released, and thus break every yoke; not only let go
those that are wrongfully kept under the yoke, but break the yoke of
slavery itself, that it may not serve again another time nor any by made
again to serve under it.\" `[2.]` That we be charitable to those that
stand in need of charity, v. 7. The particulars in the former verse may
be taken as acts of charity, that we not only release those whom we have
unjustly oppressed-that is justice, but that we contribute to the rescue
and ransom of those that are oppressed by others, to the release of
captives and the payment of the debts of the poor; but those in this
verse are plainly acts of charity. This then is the fast that God has
chosen. First, To provide food for those that want it. This is put
first, as the most necessary, and which the poor can but a little while
live without. It is to break thy bread to the hungry. Observe, \"It must
be thy bread, that which is honestly got (not that which thou hast
robbed others of), the bread which thou thyself hast occasion for, the
bread of thy allowance.\" We must deny ourselves, that we may have to
give to him that needeth. \"Thy bread which thou hast spared from
thyself and thy family, on the fast-day, if that, or the value of it, be
not given to the poor, it is the miser\'s fast, which he makes a hand
of; it is fasting for the world, not for God. This is the true fast, to
break thy bread to the hungry, not only to give them that which is
already broken meat, but to break bread on purpose for them, to give
them loaves and not to put them off with scraps.\" Secondly, To provide
lodging for those that want it: It is to take care of the poor that are
cast out, that are forced from their dwelling, turned out of house and
harbour, are cast out as rebels (so some critics render it), that are
attainted, and whom therefore it is highly penal to protect. \"If they
suffer unjustly, make no difficulty of sheltering them; do not only find
out quarters for them and pay for their lodging elsewhere, but, which is
a greater act of kindness, bring them to thy own house, make them thy
own guests. Be not forgetful to entertain strangers: for though thou
mayest not, as some have done, thereby entertain angels, thou mayest
entertain Christ himself, who will recompense it in the resurrection of
the just. I was a stranger and you took me in.\" Thirdly, To provide
clothing for those that want it: \"When thou seest the naked, that thou
cover him, both to shelter him from the injuries of the weather and to
enable him to appear decently among his neighbours; give him clothes to
come to church in, and in these and other instances hide not thyself
from thy own flesh.\" Some understand it more strictly of a man\'s own
kindred and relations: \"If those of thy own house and family fall into
decay, thou art worse than an infidel if thou dost not provide for
them.\" 1 Tim. 5:8. Others understand it more generally; all that
partake of the human nature are to be looked upon as our own flesh, for
have we not all one Father? And for this reason we must not hide
ourselves from them, not contrive to be out of the way when a poor
petitioner enquires for us, not look another way when a moving object of
charity and compassion presents itself; let us remember that they are
flesh of our flesh and therefore we ought to sympathize with them, and
in doing good to them we really do good to our own flesh and spirit too
in the issue; for thus we lay up for ourselves a good foundation, a good
bond, for the time to come.

### Verses 8-12

Here are precious promises for those to feast freely and cheerfully upon
by faith who keep the fast that God has chosen; let them know that God
will make it up to them. Here is,

`I.` A further account of the duty to be done in order to our interest in
these promises (v. 9, 10); and here, as before, it is required that we
both do justly and love mercy, that we cease to do evil and learn to do
well. 1. We must abstain from all acts of violence and fraud. \"Those
must be taken away from the midst of thee, from the midst of thy person,
out of thy heart\" (so some); \"thou must not only refrain from the
practice of injury, but mortify in thee all inclination and disposition
towards it.\" Or from the midst of thy people. Those in authority must
not only not be oppressive themselves, but must do all they can to
prevent and restrain oppression in all within their jurisdiction. They
must not only break the yoke (v. 6), but take away the yoke, that those
who have been oppressed may never be re-enslaved (as they were Jer.
34:10, 11); they must likewise forbear threatening (Eph. 6:9) and take
away the putting forth of the finger, which seems to have been then, as
sometimes with us, a sign of displeasure and the indication of a purpose
to correct. Let not the finger be put forth to point at those that are
poor and in misery, and so to expose them to contempt; such expressions
of contumely as are provoking, and the products of ill-nature, ought to
be banished from all societies. And let them not speak vanity, flattery
or fraud, to one another, but let all conversation be governed by
sincerity. Perhaps that dissimulation which is the bane of friendship is
meant by the putting forth of the finger (as Prov. 6:13 by teaching with
the finger), or it is putting forth the finger with the ring on it,
which was the badge of authority, and which therefore they produced when
they spoke iniquity, that is, gave unrighteous sentences. 2. We must
abound in all acts of charity and beneficence. We must not only give
alms according as the necessities of the poor require, but, `(1.)` We must
give freely and cheerfully, and from a principle of charity. We must
draw out our soul to the hungry (v. 10), not only draw out the money and
reach forth the hand, but do this from the heart, heartily, and without
grudging, from a principle of compassion and with a tender affection to
such as we see to be in misery. Let the heart go along with the gift;
for God loves a cheerful giver, and so does a poor man too. When our
Lord Jesus healed and fed the multitude it was as having compassion on
them. `(2.)` We must give plentifully and largely, so as not to tantalize,
but to satisfy, the afflicted soul: \"Do not only feed the hungry, but
gratify the desire of the afflicted, and, if it lies in your power, make
them easy.\" What are we born for, and what have we our abilities of
body, mind, and estate for, but to do all the good we can in this world
with them? And the poor we have always with us.

`II.` Here is a full account of the blessings and benefits which attend
the performance of this duty. If a person, a family, a people, be thus
disposed to every thing that is good, let them know for their comfort
that they shall find God their bountiful rewarder and what they lay out
in works of charity shall be abundantly made up to them. 1. God will
surprise them with the return of mercy after great affliction, which
shall be as welcome as the light of the morning after a long and dark
night (v. 8): \"Then shall thy light break forth as the morning and (v.
10) thy light shall rise in obscurity. Though thou hast been long buried
alive thou shalt recover thy eminency; though long overwhelmed with
grief, thou shalt again look pleasant as the dawning day.\" Those that
are cheerful in doing good God will make cheerful in enjoying good; and
this also is a special gift of God, Eccl. 2:24. Those that have shown
mercy shall find mercy. Job, who in his prosperity had done a great deal
of good, had friends raised up for him by the Lord when he was reduced,
who helped him with their substance, so that his light rose in
obscurity. \"Not only thy light, which is sweet, but thy health too, or
the healing of the wounds thou hast long complained of, shall spring
forth speedily; all thy grievances shall be redressed, and thou shalt
renew thy youth and recover thy vigour.\" Those that have helped others
out of trouble will obtain help of God when it is their turn. 2. God
will put honour upon them. Good works shall be recompensed with a good
name; this is included in that light which rises out of obscurity.
Though a man\'s extraction be mean, his family obscure, and he has no
external advantages to gain him honour, yet, if he do good in his place,
that will procure him respect and veneration, and his darkness shall by
this means become as the noon-day, that is, he shall become very eminent
and shine brightly in his generation. See here what is the surest way
for a man to make himself illustrious; let him study to do good. He that
would be the greatest of all, and best-loved, let him by humility and
industry make himself a servant of all. \"Thy righteousness shall answer
for thee (as Jacob says, Gen. 30:33), that is, it shall silence
reproaches, nay, it shall bespeak thee more praises than thy humility
can be pleased with.\" He that has given to the poor, his righteousness
(that is, the honour of it) endures for ever, Ps. 112:9. 3. They shall
always be safe under the divine protection: \"Thy righteousness shall go
before thee as thy vanguard, to secure thee from enemies that charge
thee in the front, and the glory of the Lord shall be thy rearward, the
gathering host, to bring up those of thee that are weary and are left
behind, and to secure thee from the enemies, that, like Amalek, fall
upon thy rear.\" Observe, How good people are safe on all sides. Let
them look which way they will, behind them or before them; let them look
backward or forward; they see themselves safe, and find themselves easy
and quiet from the fear of evil. And observe what it is that is their
defence; it is their righteousness, and the glory of the Lord, that is,
as some suppose, Christ; for it is by him that we are justified, and God
is glorified. He it is that goes before us, and is the captain of our
salvation, as he is the Lord our righteousness; he it is that is our
rearward, on whom alone we can depend for safety when our sins pursue us
and are ready to take hold on us. Or, \"God himself in his providence
and grace shall both go before thee as thy guide to conduct thee, and
attend thee as thy rearward to protect thee, and this shall be the
reward of thy righteousness and so shall be for the glory of the Lord as
the rewarder of it.\" 4. God will be always nigh unto them, to hear
their prayers, v. 9. As, on the one hand, he that shuts his ears to the
cry of the poor shall himself cry and God will not hear him; so, on the
other hand, he that is liberal to the poor, his prayers shall come up
with his alms for a memorial before God, as Cornelius\'s did (Acts
10:4): \"Then shalt thou call, on thy fast-days, which ought to be days
of prayer, and the Lord shall answer, shall give thee the things thou
callest to him for; thou shalt cry when thou art in any distress or
sudden fright, and he shall say, Here I am.\" This is a very
condescending expression of God\'s readiness to hear prayer. When God
calls to us by his word it becomes us to say, Here we are; what saith
our Lord unto his servants? But that God should say to us, Behold me,
here I am, is strange. When we cry to him, as if he were at a distance,
he will let us know that he is near, even at our right hand, nearer than
we thought he was. It is I, be not afraid. When danger is near our
protector is nearer, a very present help. \"Here I am, ready to give you
what you want, and do for you what you desire; what have you to say to
me?\" God is attentive to the prayers of the upright, Ps. 130:2. No
sooner do they call to him than he answers, Ready, ready. Wherever they
are praying, God says, \"Here I am hearing; I am in the midst of you.\"
He is nigh unto them in all things, Deu. 4:7. 6. God will direct them in
all difficult and doubtful cases (v. 11): The Lord shall guide thee
continually. While we are here, in the wilderness of this world, we have
need of continual direction from heaven; for, if at any time we be left
to ourselves, we shall certainly miss our way; and therefore it is to
those who are good in God\'s sight that he gives the wisdom which in all
cases is profitable to direct, and he will be to them instead of eyes,
Eccl. 2:26. His providence will make their way plain to them, both what
is their duty and what will be most for their comfort. 6. God will give
them abundance of satisfaction in their own minds. As the world is a
wilderness in respect of wanderings, so that they need to be guided
continually, so also is it in respect of wants, which makes it necessary
that they should have continual supplies, as Israel in the wilderness
had not only the pillar of cloud to guide them continually, but manna
and water out of the rock to satisfy their souls in drought, in a dry
and thirsty land where no water is, Ps. 63:1. To a good man God gives
not only wisdom and knowledge, but joy; he is satisfied in himself with
the testimony of his conscience and the assurances of God\'s favour.
\"These will satisfy thy soul, will put gladness into thy heart, even in
the drought of affliction; these will make fat thy bones, and fill them
with marrow, will give thee that pleasure which will be a support to
thee as the bones to the body, that joy of the Lord which will be thy
strength. He shall give thy bones rest\" (so some read it), \"rest from
the pain and sickness which they have laboured under and been chastened
with;\" so it agrees with that promise made to the merciful. The Lord
will make all his bed in his sickness, Ps. 41:3. \"Thou shalt be like a
watered garden, so flourishing and fruitful in graces and comforts, and
like a spring of water, like a garden that has a spring of water in it,
whose waters fail not either in droughts or in frosts.\" The principle
of holy love in those that are good shall be a well of living water, Jn.
4:14. As a spring of water, though it is continually sending forth its
streams, is yet always full, so the charitable man abounds in good as he
abounds in doing good and is never the poorer for his liberality. He
that waters shall himself be watered. 7. They and their families shall
be public blessings. It is a good reward to those that are fruitful and
useful to be rendered more so, and especially to have those who descend
from them to be so too. This is here promised (v. 12): \"Those that now
are of thee, thy princes, and nobles, and great men, shall have such
authority and influence as they never had;\" or, \"Those that hereafter
shall be of thee, thy posterity, shall be serviceable to their
generation, as thou art to thine.\" It completes the satisfaction of a
good man, as to this world, to think that those that come after him
shall be doing good when he is gone. 1. They shall re-edify cities that
have been long in ruins, shall build the old waste places, which had
lain so long desolate that the rebuilding of them was quite despaired
of. This was fulfilled when the captives, after their return, repaired
the cities of Judah, and dwelt in them, and many of those in Israel too,
which had lain waste ever since the carrying away of the ten tribes. 2.
They shall carry on and finish that good work which was begun long
before, and shall be helped over the obstructions which had retarded the
progress of it: They shall raise up to the top that building the
foundation of which was laid long since and has been for many
generations in the rearing. This was fulfilled when the building of the
temple was revived after it had stood still for many years, Ezra 5:2.
Or, \"They shall raise up foundations which shall continue for many
generations yet to come;\" they shall do that good which shall be of
lasting consequence. 3. They shall have the blessing and praise of all
about them: \"Thou shalt be called (and it shall be to thy honour) the
repairer of the breach, the breach made by the enemy in the wall of a
besieged city, which whoso has the courage and dexterity to make up, or
make good, gains great applause.\" Happy are those who make up the
breach at which virtue is running out and judgments are breaking in.
\"Thou shalt be the restorer of paths, safe and quiet paths, not only to
travel in, but to dwell in, so safe and quiet that people shall make no
difficulty of building their houses by the road-side.\" The sum is that,
if they keep such fasts as God has chosen, he will settle them again in
their former peace and prosperity, and there shall be none to make them
afraid. See Zec. 7:5, 9; 8:3-5. It teaches us that those who do justly
and love mercy shall have the comfort thereof in this world.

### Verses 13-14

Great stress was always laid upon the due observance of the sabbath day,
and it was particularly required from the Jews when they were captives
in Babylon, because by keeping that day, in honour of the Creator, they
distinguished themselves from the worshippers of the gods that have not
made the heavens and the earth. See ch. 56:1, 2, where keeping the
sabbath is joined, as here, with keeping judgment and doing justice.
Some, indeed, understand this of the day of atonement, which they think
is the fast spoken of in the former part of the chapter, and which is
called a sabbath of rest, Lev. 23:32. But, as the fasts before spoken of
seem to be those that were occasional, so this sabbath is doubtless the
weekly sabbath, that great sign between God and his professing
people-his appointing it a sign of his favour to them and their
observing it a sign of their obedience to him. Now observe here,

`I.` How the sabbath is to be sanctified (v. 13); and, there remaining
still a sabbatism for the people of God, this law of the sabbath is
still binding to us on our Lord\'s day.

`1.` Nothing must be done that puts contempt upon the sabbath day, or
looks like having mean thoughts of it, when God has so highly dignified
it. We must turn away our foot from the sabbath, from trampling upon it,
as profane atheistical people do, from travelling on that day (so some);
we must turn away our foot from doing out pleasure on that holy day,
that is, from living at large, and taking a liberty to do what we please
on sabbath days, without the control and restraint of conscience, or
from indulging ourselves in the pleasures of sense, in which the modern
Jews wickedly place the sanctification of the sabbath, though it is as
great a profanation of it as any thing. On sabbath days we must not walk
in our own ways (that is, not follow our callings), not find our own
pleasure (that is, not follow our sports and recreations); nay, we must
not speak our own words, words that concern either our callings or our
pleasures; we must not allow ourselves a liberty of speech on that day
as on other days, for we must then mind God\'s ways, make religion the
business of the day; we must choose the things that please him; and
speak his words, speak of divine things as we sit in the house and walk
by the way. In all we say and do we must put a difference between this
day and other days.

`2.` Every thing must be done that puts an honour on the day and is
expressive of our high thoughts of it. We must call it a delight, not a
task and a burden; we must delight ourselves in it, in the restraints it
lays upon us and the services it obliges us to. We must be in our
element when we are worshipping God, and in communion with him. How
amiable are thy tabernacles, O Lord of hosts! We must not only count it
a delight, but call it so, must openly profess the complacency we take
in the day and the duties of it. We must call it so to God, in
thanksgiving for it and earnest desire of his grace to enable us to do
the work of the day in its day, because we delight in it. We must call
it so to others, to invite them to come and share in the pleasure of it;
and we must call it so to ourselves, that we may not entertain the least
thought of wishing the sabbath gone that we may sell corn. We must call
it the Lord\'s holy day, and honourable. We must call it holy, separated
from common use and devoted to God and to his service, must call it the
holy of the Lord, the day which he has sanctified to himself. Even in
Old-Testament times the sabbath was called the Lord\'s day, and
therefore it is fitly called so still, and for a further reason, because
it is the Lord Christ\'s day, Rev. 1:10. It is holy because it is the
Lord\'s day, and upon both accounts it is honourable. It is a beauty of
holiness that is upon it; it is ancient, and its antiquity is its
honour; and we must make it appear that we look upon it as honourable by
honouring God on that day. We put honour upon the day when we give
honour to him that instituted it, and to whose honour it is dedicated.

`II.` What the reward is of the sabbath-sanctification, v. 14. If we thus
remember the sabbath day to keep it holy,

`1.` We shall have the comfort of it; the work will be its own wages. If
we call the sabbath a delight, then shall we delight ourselves in the
Lord; he will more and more manifest himself to us as the delightful
subject of our thoughts and meditations and the delightful object of our
best affections. Note, The more pleasure we take in serving God the more
pleasure we shall find in it. If we go about duty with cheerfulness, we
shall go from it with satisfaction and shall have reason to say, \"It is
good to be here, good to draw near to God.\"

`2.` We shall have the honour of it: I will cause thee to ride upon the
high places of the earth, which denotes not only a great security (as
that, ch. 32:16, He shall dwell on high), but great dignity and
advancement. \"Thou shalt ride in state, shalt appear conspicuous, and
the eyes of all thy neighbours shall be upon thee.\" It was said of
Israel, when God led them triumphantly out of Egypt, that he made them
to ride on the high places of the earth, Deu. 32:12, 13. Those that
honour God and his sabbath he will thus honour. If God by his grace
enable us to live above the world, and so to manage it as not only not
to be hindered by it, but to be furthered and carried on by it in our
journey towards heaven, then he makes us to ride on the high places of
the earth.

`3.` We shall have the profit of it: I will feed thee with the heritage
of Jacob thy father, that is, with all the blessings of the covenant and
all the precious products of Canaan (which was a type of heaven), for
these were the heritage of Jacob. Observe, The heritage of believers is
what they shall not only be portioned with hereafter, but fed with now,
fed with the hopes of it, and not flattered, fed with the earnests and
foretastes of it; and those that are so fed have reason to say that they
are well fed. In order that we may depend upon it, it is added, \"The
mouth of the Lord has spoken it; you may take God\'s word for it, for he
cannot lie nor deceive; what his mouth has spoken his hand will give,
his hand will do, and not one iota or tittle of his good promise shall
fall to the ground.\" Blessed, therefore, thrice blessed, is he that
doeth this, and lays hold on it, that keeps the sabbath from polluting
it.
