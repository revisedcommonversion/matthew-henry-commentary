Isaiah, Chapter 21
==================

Commentary
----------

In this chapter we have a prophecy of sad times coming, and heavy
burdens, `I.` Upon Babylon, here called \"the desert of the sea,\" that it
should be destroyed by the Medes and Persians with a terrible
destruction, which yet God\'s people should have advantage by (v. 1-10).
`II.` Upon Dumah, or Idumea (v. 11, 12). `III.` Upon Arabia, or Kedar, the
desolation of which country was very near (v. 13-17). These and other
nations which the princes and people of Israel had so much to do with
the prophets of Israel could not but have something to say to. Foreign
affairs must be taken notice of as well as domestic ones, and news from
abroad enquired after as well as news at home.

### Verses 1-10

We had one burden of Babylon before (ch. 13); here we have another
prediction of its fall. God saw fit thus to possess his people with the
belief of this event by line upon line, because Babylon sometimes
pretended to be a friend to them (as ch. 39:1), and God would hereby
warn them not to trust to that friendship, and sometimes was really an
enemy to them, and God would hereby warn them not to be afraid of that
enmity. Babylon is marked for ruin; and all that believe God\'s prophets
can, through that glass, see it tottering, see it tumbling, even when
with an eye of sense they see it flourishing and sitting as a queen.
Babylon is here called the desert or plain of the sea; for it was a flat
country, and full of lakes, or loughs (as they call them in Ireland),
like little seas, and was abundantly watered with the many streams of
the river Euphrates. Babylon did but lately begin to be famous, Nineveh
having outshone it while the monarchy was in the Assyrian hands; but in
a little time it became the lady of kingdoms; and, before it arrived at
that pitch of eminency which it was at in Nebuchadnezzar\'s time, God by
this prophet plainly foretold its fall, again and again, that his people
might not be terrified at its rise, nor despair of relief in due time
when they were its prisoners, Job 5:3; Ps. 37:35, 36. Some think it is
here called a desert because, though it was now a populous city, it
should in time be made a desert. And therefore the destruction of
Babylon is so often prophesied of by this evangelical prophet, because
it was typical of the destruction of the man of sin, the great enemy of
the New-Testament church, which is foretold in the Revelation in many
expressions borrowed from these prophecies, which therefore must be
consulted and collated by those who would understand the prophecy of
that book. Here is,

`I.` The powerful irruption and descent which the Medes and Persians
should make upon Babylon (v. 1, 2): They will come from the desert, from
a terrible land. The northern parts of Media and Persia, where their
soldiers were mostly bred, was waste and mountainous, terrible to
strangers that were to pass through it and producing soldiers that were
very formidable. Elam (that is, Persia) is summoned to go up against
Babylon, and, in conjunction with the forces of Media, to besiege it.
When God has work of this kind to do he will find, though it be in a
desert, in a terrible land, proper instruments to be employed in it.
These forces come as whirlwinds from the south, so suddenly, so
strongly, so terribly, such a mighty noise shall they make, and throw
down every thing that stands in their way. As is usual in such a case,
some deserters will go over to them: The treacherous dealers will deal
treacherously. Historians tell us of Gadatas and Gobryas, two great
officers of the king of Babylon, that went over to Cyrus, and, being
well acquainted with all the avenues of the city, led a party directly
to the palace, where Belshazzar was slain. Thus with the help of the
treacherous dealers the spoilers spoiled. Some read it thus: There shall
be a deceiver of that deceiver, Babylon, and a spoiler of that spoiler,
or, which comes all to one, The treacherous dealer has found one that
deals treacherously, and the spoiler one that spoils, as it is
expounded, ch. 33:1. The Persians shall pay the Babylonians in their own
coin; those that by fraud and violence, cheating and plundering,
unrighteous wars and deceitful treaties, have made a prey of their
neighbours, shall meet with their match, and by the same methods shall
themselves be made a prey of.

`II.` The different impressions made hereby upon those concerned in
Babylon. 1. To the poor oppressed captives it would be welcome news; for
they had been told long ago that Babylon\'s destroyer would be their
deliverer, and therefore, \"when they hear that Elam and Media are
coming up to besiege Babylon, all their sighing will be made to cease;
they shall no longer mingle their tears with Euphrates\' streams, but
resume their harps, and smile when they remember Zion, which, before,
they wept at the thought of.\" For the sighing of the needy the God of
pity will arise in due time (Ps. 12:5); he will break the yoke from all
their neck, will remove the rod of the wicked from off their lot, and so
make their sighing to cease. 2. To the proud oppressors it would be a
grievous vision (v. 2), particularly to the king of Babylon for the time
being, and it should seem that he it is who is here brought in sadly
lamenting his inevitable fate (v. 3, 4): Therefore are my loins filled
with pain; pangs have taken hold upon me, etc., which was literally
fulfilled in Belshazzar, for that very night in which his city was
taken, and himself slain, upon the sight of a hand writing mystic
characters upon the wall his countenance was changed and his thoughts
troubled him, so that the joints of his loins were loosed and his knees
smote one against another, Dan. 5:6. And yet that was but the beginning
of sorrows. Daniel\'s deciphering the writing could not but increase his
terror, and the alarm which immediately followed of the executioners at
the door would be the completing of it. And those words, The night of my
pleasure has he turned into fear to me, plainly refer to that
aggravating circumstance of Belshazzar\'s fall that he was slain on that
night when he was in the height of his mirth and jollity, with his cups
and concubines about him and a thousand of his lords revelling with him;
that night of his pleasure, when he promised himself an undisturbed
unallayed enjoyment of the most exquisite gratifications of sense, with
a particular defiance of God and religion in the profanation of the
temple vessels, was the night that was turned into all this fear. Let
this give an effectual check to vain mirth and sensual pleasures, and
forbid us ever to lay the reins on the neck of them-that we know not
what heaviness the mirth may end in, nor how soon laughter may be turned
into mourning; but this we know that for all these things God shall
bring us into judgment; let us therefore mix trembling always with our
joys.

`III.` A representation of the posture in which Babylon should be found
when the enemy should surprise it-all in festival gaiety (v. 5):
\"Prepare the table with all manner of dainties. Set the guards; let
them watch in the watch-tower while we eat and drink securely and make
merry; and, if any alarm should be given, the princes shall arise and
anoint the shield, and be in readiness to give the enemy a warm
reception.\" Thus secure are they, and thus do they gird on the harness
with as much joy as if they were putting it off.

`IV.` A description of the alarm which should be given to Babylon upon
its being forced by Cyrus and Darius. The Lord, in vision, showed the
prophet the watchman set in his watch-tower, near the watch-tower, near
the palace, as is usual in times of danger; the king ordered those about
him to post a sentinel in the most advantageous place for discovery,
and, according to the duty of a watchman, let him declare what he sees,
v. 6. We read of watchmen thus set to receive intelligence in the story
of David (2 Sa. 18:24), and in the story of Jehu, 2 Ki. 9:17. This
watchman here discovered a chariot with a couple of horsemen attending
it, in which we may suppose the commander-in-chief to ride. He then saw
another chariot drawn by asses or mules, which were much in use among
the Persians, and a chariot drawn by camels, which were likewise much in
use among the Medes; so that (as Grotius thinks) these two chariots
signify the two nations combined against Babylon, or rather these
chariots come to bring tidings to the palace; compare Jer. 51:31, 32.
One post shall run to meet another, and one messenger to meet another,
to show the king of Babylon that his city is taken at one end while he
is revelling at the other end and knows nothing of the matter. The
watchman, seeing these chariots at some distance, hearkened diligently
with much heed, to receive the first tidings. And (v. 8) he cried, A
lion; this word, coming out of a watchman\'s mouth, no doubt gave them a
certain sound, and every body knew the meaning of it, though we do not
know it now. It is likely that it was intended to raise attention: he
that has an ear to hear, let him hear, as when a lion roars. Or he cried
as a lion, very loud and in good earnest, the occasion being very
urgent. And what has he to say? 1. He professes his constancy to the
post assigned him: \"I stand, my lord, continually upon the watch-tower,
and have never discovered any thing material till just now; all seemed
safe and quiet.\" Some make it to be a complaint of the people of God
that they had long expected the downfall of Babylon, according to the
prophecy, and it had not yet come; but withal a resolution to continue
waiting; as Hab. 2:1, I will stand upon my watch, and set me upon the
tower, to see what will be the issue of the present providences. 2. He
gives notice of the discoveries he had made (v. 9): Here comes a chariot
of men with a couple of horsemen, a vision representing the enemy\'s
entry into the city with all their force or the tidings brought to the
royal palace of it.

`V.` A certain account is at length given of the overthrow of Babylon. He
in the chariot answered and said (when he heard the watchman speak),
Babylon has fallen, has fallen; or God answered thus to the prophet
enquiring concerning the issue of these affairs: \"It has now come to
this, Babylon has surely and irrecoverably fallen. Babylon\'s business
is done now. All the graven images of her gods he has broken unto the
ground.\" Babylon was the mother of harlots (that is, of idolatry),
which was one of the grounds of God\'s quarrel with her; but her idols
should now be so far from protecting her that some of them should be
broken down to the ground, and others of them, that were worth carrying
way, should go into captivity, and be a burden to the beasts that
carried them, ch. 46:1, 2.

`VI.` Notice is given to the people of God, who were then captives in
Babylon, that this prophecy of the downfall of Babylon was particularly
intended for their comfort and encouragement, and they might depend upon
it that it should be accomplished in due season, v. 10. Observe,

`1.` The title the prophet gives them in God\'s name: O my threshing, and
the corn of my floor! The prophet calls them his, because they were his
countrymen, and such as he had a particular interest in and concern for;
but he speaks it as from God, and directs his speech to those that were
Israelites indeed, the faithful in the land. Note, `(1.)` The church is
God\'s floor, in which the most valuable fruits and products of this
earth are, as it were, gathered together and laid up. `(2.)` True
believers are the corn of God\'s floor. Hypocrites are but as the chaff
and straw, which take up a great deal of room, but are of small value,
with which the wheat is now mixed, but from which it shall be shortly
and for ever separated. `(3.)` The corn of God\'s floor must expect to be
threshed by afflictions and persecutions. God\'s Israel of old was
afflicted from her youth, often under the plougher\'s plough (Ps. 129:3)
and the thresher\'s flail. `(4.)` Even then God owns it for his threshing;
it is his still; nay, the threshing of it is by his appointment, and
under his restraint and direction. The threshers could have no power
against it but what was given them from above.

`2.` The assurance he gives them of the truth of what he had delivered to
them, which therefore they might build their hopes upon: That which I
have heard of the Lord of hosts, the God of Israel-that, and nothing
else, that, and no fiction or fancy of my own-have I declared unto you.
Note, In all events concerning the church, past, present, and to come,
we must have an eye to God both as the Lord of hosts and as the God of
Israel, who has power enough to do anything for his church and grace
enough to do everything that is for her good, and to the words of his
prophets, as words received from the Lord. As they dare not smother any
thing which he has entrusted them to declare, so they dare not declare
anything as from him which he has not made known to them, 1 Co. 11:23.

### Verses 11-12

This prophecy concerning Dumah is very short, and withal dark and hard
to be understood. Some think that Dumah is a part of Arabia, and that
the inhabitants descended from Dumah the sixth son of Ishmael, as those
of Kedar (v. 16, 17) from Ishmael\'s second son, Gen. 25:13, 14. Others,
because Mount Seir is here mentioned, by Dumah understand Idumea, the
country of the Edomites. Some of Israel\'s neighbours are certainly
meant, and their distress is foretold, not only for warning to them to
prepare them for it, but for warning to Israel not to depend upon them,
or any of the nations about them, for relief in a time of danger, but
upon God only. We must see all creature confidences failing us, and feel
them breaking under us, that we may not lay more weight upon them than
they will bear. But though the explication of this prophecy be
difficult, because we have no history in which we find the
accomplishment of it, yet the application will be easy. We have here,

`1.` A question put by an Edomite to the watchman. Some one or other
called out of Seir, somebody that was more concerned for the public
safety and welfare than the rest, who were generally careless and
secure. As the man of Macedonia, in a vision, desired Paul to come over
and help them (Acts 16:9), so this man of Mount Seir, in a vision,
desired the prophet to inform and instruct them. He calls not many; it
is well there are any, that all are not alike unconcerned about the
things that belong to the public peace. Some out of Seir ask advice of
God\'s prophets, and are willing to be taught, when many of God\'s
Israel heed nothing. The question is serious: What of the night? It is
put to a proper person, the watchman, whose office it is to answer such
enquiries. He repeats the question, as one in care, as one in earnest,
and desirous to have an answer. Note, `(1.)` God\'s prophets and ministers
are appointed to be watchmen, and we are to look upon them as such. They
are as watchmen in the city in a time of peace, to see that all be safe,
to knock at every door by personal enquiries (\"Is it locked? Is the
fire safe?\"), to direct those that are at a loss, and check those that
are disorderly, Cant. 3:3; 5:7. They are as watchmen in the camp in time
of war, Eze. 33:7. They are to take notice of the motions of the enemy
and to give notice of them, to make discoveries and then give warning;
and in this they must deny themselves. `(2.)` It is our duty to enquire of
the watchmen, especially to ask again and again, What of the night? for
watchmen wake when other sleep. `[1.]` What time of the night? After a
long sleep in sin and security, is it not time to rise, high time to
awake out of sleep? Rom. 13:11. We have a great deal of work to do, a
long journey to go; is it not time to be stirring? \"Watchman, what
o\'clock is it? After a long dark night is there any hope of the day
dawning?\" `[2.]` What tidings of the night? What from the night? (so
some); \"what vision has the prophet had to-night? We are ready to
receive it.\" Or, rather, \"What occurs to night? What weather is it?
What news?\" We must expect an alarm, and never be secure. The day of
the Lord will come as a thief in the night; we must prepare to receive
the alarm, and resolve to keep our ground, and then take the first hint
of danger, and to our arms presently, to our spiritual weapons.

`2.` The watchman\'s answer to this question. The watchman was neither
asleep nor dumb; though it was a man of Mount Seir that called to him,
he was ready to give him an answer: The morning comes. He answers, `(1.)`
By way of prediction: \"There comes first a morning of light, and peace,
and opportunity; you will enjoy one day of comfort more; but afterwards
comes a night of trouble and calamity.\" Note, In the course of God\'s
providence it is usual that morning and night are counterchanged and
succeed each other. Is it night? Yet the morning comes, and the
day-spring knows his place, Ps. 30:5. Is it day? Yet the night comes
also. If there be a morning of youth and health, there will come a night
of sickness and old age; if a morning of prosperity in the family, in
the public, yet we must look for changes. But God usually gives a
morning of opportunity before he sends a night of calamity, that his own
people may be prepared for the storm and others left inexcusable. `(2.)`
By way of excitement: If you will enquire, enquire. Note, It is our
wisdom to improve the present morning in preparation for the night that
is coming after it. \"Enquire, return, come. Be inquisitive, be
penitent, be willing and obedient.\" The manner of expression is very
observable, for we are put to our choice what we will do: \"If you will
enquire, enquire; if not, it is at your peril; you cannot say but you
have a fair offer made you.\" We are also urged to be at a point: \"If
you will, say so, and do not stand pausing; what you will do do quickly,
for it is no time to trifle.\" Those that return and come to God will
find they have a great deal of work to do and but a little time to do it
in, and therefore they have need to be busy.

### Verses 13-17

Arabia was a large country, that lay eastward and southward of the land
of Canaan. Much of it was possessed by the posterity of Abraham. The
Dedanim, here mentioned (v. 13), descended from Dedan, Abraham\'s son by
Keturah; the inhabitants of Tema and Kedar descended from Ishmael, Gen.
25:3, 13, 15. The Arabians generally lived in tents, and kept cattle,
were a hardy people, inured to labour; probably the Jews depended upon
them as a sort of a wall between them and the more warlike eastern
nations; and therefore, to alarm them, they shall hear the burden of
Arabia, and see it sinking under its own burden.

`I.` A destroying army shall be brought upon them, with a sword, with a
drawn sword, with a bow ready bent, and with all the grievousness of
war, v. 15. It is probable that the king of Assyria, in some of the
marches of his formidable and victorious army, took Arabia in his way,
and, meeting with little resistance, made an easy prey of them. The
consideration of the grievousness of war should make us thankful for the
blessings of peace.

`II.` The poor country people will hereby be forced to flee for shelter
wherever they can find a place; so that the travelling companies of
Dedanium, which used to keep the high roads with their caravans, shall
be obliged to quit them and lodge in the forest in Arabia (v. 13), and
shall not have the wonted convenience of their own tents, poor and
weather-beaten as they are.

`III.` They shall stand in need of refreshment, being ready to perish for
want of it, in their flight from the invading army: \"O you inhabitants
of the land of Tema!\" (who probably were next neighbours to the
companies of Dedanim) \"bring you water\" (so the margin reads it) \"to
him that is thirsty, and prevent with your bread those that flee, for
they are objects of your compassion; they do not wander for wandering
sake, nor are they reduced to straits by any extravagance of their own,
but they flee from the sword.\" Tema was a country where water was
sometimes a scarce commodity (as we find, Job 6:19), and we may conclude
it would be in a particular manner acceptable to these poor distressed
refugees. Let us learn hence. 1. To look for distress ourselves. We know
not what straits we may be brought into before we die. Those that live
in cities may be forced to lodge in forests; and those may know the want
of necessary food who now eat bread to the full. Our mountain stands not
so strong but that it may be moved, rises not so high but that it may be
scaled. These Arabians would the better bear these calamities because in
their way of living they had used themselves to hardships. 2. To look
with compassion upon those that are in distress, and with all
cheerfulness to relieve them, not knowing how soon their case may be
ours: \"Bring water to those that are thirsty, and not only give bread
to those that need and ask it, but prevent those with it that have need;
give it to them unasked.\" Those that do so shall find it remembered to
their praise, as (according to our reading) it is here remembered to the
praise of the land of Tema that they did bring water to the thirsty and
relieved even those that were on the falling side.

`IV.` All that which is the glory of Kedar shall vanish away and fail.
Did they glory in their numerous herds and flocks? They shall all be
driven away by the enemy. It seems they were famous about other nations
for the use of the bow in battle; but their archers, instead of foiling
the enemy, shall fall themselves; and the residue of their number, when
they are reduced to a small number, shall be diminished (v. 17); their
mighty able-bodied men, and men of spirit too, shall become very few;
for they, being most forward in the defence of their country, were most
exposed, and fell first, either by the enemies\' sword or into the
enemies\' hand. Note, Neither the skill of archers (though they be ever
so good marksmen) nor the courage of mighty men can protect a people
from the judgments of God, when they come with commission; they rather
expose the undertakers. That is poor glory which will thus quickly come
to nothing.

`V.` All this shall be done in a little time: \"Within one year according
to the years of a hireling (within one year precisely reckoned) this
judgment shall come upon Kedar.\" If this fixing of the time be of no
great use to us now (because we find not either when the prophecy was
delivered or when it was accomplished), yet it might be of great use to
the Arabians then, to awaken them to repentance, that, like the men of
Nineveh, they might prevent the judgment when they were thus told it was
just at the door. Or, when it begins to be fulfilled, the business shall
be done, be begun and ended in one year\'s time. God, when he please,
can do a great work in a little time.

`VI.` It is all ratified by the truth of God (v. 16); \"Thus hath the
Lord said to me; you may take my word for it that it is his word;\" and
we may be sure no word of his shall fall to the ground. And again (v.
17): The Lord God of Israel hath spoken it, as the God of Israel, in
pursuance of his gracious designs concerning them; and we may be sure
the strength of Israel will not lie.
