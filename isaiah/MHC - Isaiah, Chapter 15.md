Isaiah, Chapter 15
==================

Commentary
----------

This chapter, and that which follows it, are the burden of Moab-a
prophecy of some great desolation that was coming upon that country,
which bordered upon this land of Israel, and had often been injurious
and vexatious to it, though the Moabites were descended from Lot,
Abraham\'s kinsman and companion, and though the Israelites, by the
appointment of God, had spared them when they might both easily and
justly have cut them off with their neighbours. In this chapter we have,
`I.` Great lamentation made by the Moabites, and by the prophet himself
for them (v. 1-5). `II.` The great calamities which should occasion that
lamentation and justify it (v. 6-9).

### Verses 1-5

The country of Moab was of small extent, but very fruitful. It bordered
upon the lot of Reuben on the other side Jordan and upon the Dead Sea.
Naomi went to sojourn there when there was a famine in Canaan. This is
the country which (it is here foretold) should be wasted and grievously
harassed, not quite ruined, for we find another prophecy of its ruin
(Jer. 48), which was accomplished by Nebuchadnezzar. This prophecy here
was to be fulfilled within three years (ch. 16:14), and therefore was
fulfilled in the devastations made of that country by the army of the
Assyrians, which for many years ravaged those parts, enriching
themselves with spoil and plunder. It was done either by the army of
Shalmaneser, about the time of the taking of Samaria, in the fourth year
of Hezekiah (as is most probable), or by the army of Sennacherib, which,
ten years after, invaded Judah. We cannot suppose that the prophet went
among the Moabites to preach to them this sermon; but he delivered it to
his own people, 1. To show them that, though judgment begins at the
house of God, it shall not end there,-that there is a providence which
governs the world and all the nations of it,-and that to the God of
Israel the worshippers of false gods were accountable, and liable to his
judgments. 2. To give them a proof of God\'s care of them and jealousy
for them, and to convince them that God was an enemy to their enemies,
for such the Moabites had often been. 3. That the accomplishment of this
prophecy now shortly (within three years) might be a confirmation of the
prophet\'s mission and of the truth of all his other prophecies, and
might encourage the faithful to depend upon them.

Now concerning Moab it is here foretold,

`I.` That their chief cities should be surprised and taken in a night by
the enemy, probably because the inhabitants, as the men of Laish,
indulged themselves in ease and luxury, and dwelt securely (v. 1):
Therefore there shall be great grief, because in the night Air of Moab
is laid waste and Kir of Moab, the two principal cities of that kingdom.
In the night that they were taken, or sacked, Moab was cut off. The
seizing of them laid the whole country open, and made all the wealth of
it an easy prey to the victorious army. Note, 1. Great changes and very
dismal ones may be made in a very little time. Here are two cities lost
in a night, though that is the time of quietness. Let us therefore lie
down as those that know not what a night may bring forth. 2. As the
country feeds the cities, so the cities protect the country, and neither
can say to the other, I have no need of thee.

`II.` That the Moabites, being hereby put into the utmost consternation
imaginable, should have recourse to their idols for relief, and pour out
their tears before them (v. 2): He (that is, Moab, especially the king
of Moab) has gone up to Bajith (or rather to the house or temple of
Chemosh), and Dibon, the inhabitants of Dibon, have gone up to the high
places, where they worshipped their idols, there to make their
complaints. Note, It becomes a people in distress to seek to their God;
and shall not we then thus walk in the name of the Lord our God, and
call upon him in the time of trouble, before whom we shall not shed such
useless profitless tears as they did before their gods?

`III.` That there should be the voice of universal grief all the country
over. It is described here elegantly and very affectingly. Moab shall be
a vale of tears-a little map of this world, v. 2. The Moabites shall
lament the loss of Nebo and Medeba, two considerable cities, which, it
is likely, were plundered and burnt. They shall tear their hair for
grief to such a degree that on all their heads shall be baldness, and
they shall cut off their beards, according to the customary expressions
of mourning in those times and countries. When they go abroad they shall
be so far from coveting to appear handsome that in the streets they
shall gird themselves with sackcloth (v. 3), and perhaps being forced to
use that poor clothing, the enemy having stripped them, and rifled their
houses, and left them no other clothing. When they come home, instead of
applying themselves to their business, they shall go up to the tops of
their houses which were flat-roofed, and there they shall weep
abundantly, nay, they shall howl, in crying to their gods. Those that
cry not to God with their hearts do but howl upon their beds, Hos. 7:14;
Amos 8:3. They shall come down with weeping (so the margin reads it);
they shall come down from their high places and the tops of their houses
weeping as much as they did when they went up. Prayer to the true God is
heart\'s ease (1 Sa. 1:18), but prayers to false gods are not. Divers
places are here named that should be full of lamentation (v. 4), and it
is but a poor relief to have so many fellow-sufferers, fellow-mourners;
to a public spirit it is rather an aggravation socios habuisse
doloris-to have associates in woe.

`IV.` That the courage of their militia should fail them. Though they
were bred soldiers, and were well armed, yet they shall cry out and
shriek for fear, and every one of them shall have his life become
grievous to him, though it is characteristic of a military life to
delight in danger, v. 4. See how easily God can dispirit the stoutest of
men, and deprive a nation of benefit by those whom it most depended upon
for strength and defence. The Moabites shall generally be so overwhelmed
with grief that life itself shall be a burden to them. God can easily
make weary of life those that are fondest of it.

`V.` That the outcry for these calamities should propagate grief to all
the adjacent parts, v. 5. 1. The prophet himself has very sensible
impressions made upon his spirit by the prediction of it: \"My heart
shall cry out for Moab; though they are enemies to Israel, they are our
fellow-creatures, of the same rank with us, and therefore it should
grieve us to see them in such distress, the rather because we know not
how soon it may be our own turn to drink of the same cup of trembling.\"
Note, It becomes God\'s ministers to be of a tender spirit, not to
desire the woeful day, but to be like their master, who wept over
Jerusalem even when he gave her up to ruin, like their God, who desires
not the death of sinners. 2. All the neighbouring cities shall echo to
the lamentations of Moab. The fugitives, who are making the best of
their way to shift for their own safety, shall carry the cry to Zoar,
the city to which their ancestor Lot fled for shelter from Sodom\'s
flames and which was spared for his sake. They shall make as great a
noise with their cry as a heifer of three years old does when she goes
lowing for her calf, as 1 Sa. 6:12. They shall go up the hill of Luhith
(as David went up the ascent of Mount Olivet, many a weary step and all
in tears, 2 Sa. 15:30), and in the way of Horonaim (a dual termination),
the way that leads to the two Beth-horons, the upper and the nether,
which we read of, Jos. 16:3, 5. Thither the cry shall be carried, there
it shall be raised, even at that great distance: A cry of destruction;
that shall be the cry, like, \"Fire, fire! we are all undone.\" Grief is
catching, so is fear, and justly, for trouble is spreading and when it
begins who knows where it will end?

### Verses 6-9

Here the prophet further describes the woeful and piteous lamentations
that should be heard throughout all the country of Moab when it should
become a prey to the Assyrian army. \"By this time the cry has gone
round about all the borders of Moab,\" v. 8. Every corner of the country
has received the alarm, and is in the utmost confusion upon it. It has
reached to Eglaim, a city at one end of the country, and to Beer-elim, a
city as far the other way. Where sin has been general, and all flesh
have corrupted their way, what can be expected but a general desolation?
Two things are here spoken of as causes of this lamentation:-

`I.` The waters of Nimrim are desolate (v. 6), that is, the country is
plundered and impoverished, and all the wealth and substance of it swept
away by the victorious army. Famine is usually the sad effect of war.
Look into the fields that were well watered, the fruitful meadows that
yielded delightful prospects and more delightful products, and there all
is eaten up, or carried off by the enemy\'s foragers, and the remainder
trodden to dirt by their horses. If an army encamp upon green fields,
their greenness is soon gone. Look into the houses, and they are
stripped too (v. 7): The abundance of wealth that they had gotten with a
great deal of art and industry, and that which they had laid up with a
great deal of care and confidence, shall they carry away to the brook of
the willows. Either the owners shall carry it thither to hide it or the
enemies shall carry it thither to pack it up and send it home, by water
perhaps, to their own country. Note, 1. Those that are eager to get
abundance of this world, and solicitous to lay up what they have gotten,
little consider what may become of it and in how short a time it may be
all taken from them. Great abundance, by tempting the robbers, exposes
the owners; and those who depend upon it to protect them often find it
does but betray them. 2. In times of distress great riches are often
great burdens, and do but increase the owner\'s care or the enemies\'
strength. Cantabit vacuus coram latrone viator-The penniless traveller
will exult, when accosted by a robber, in having nothing about him.

`II.` The waters of Dimon are turned into blood (v. 9), that is, the
inhabitants of the country are slain in great numbers, so that the
waters adjoining to the cities, whether rivers or pools, are discoloured
with human gore, inhumanly shed like water. Dimon signifies bloody; the
place shall answer to its name. Perhaps it was that place in the country
of Moab where the waters seemed to the Moabites as blood (2 Ki. 3:22,
23), which occasioned their overthrow. But now, says God, I will bring
more upon Dimon, more blood than was shed, or thought to be seen, at
that time. I will bring additions upon Dimon (so the word is),
additional plagues; I have yet more judgments in reserve for them. For
all this, God\'s anger is not turned away. When he judges he will
overcome; and to the roll of curses shall be added many like words, Jer.
36:32. See here what is the yet more evil to be brought upon Dimon, upon
Moab, which is now to be made a land of blood. Some flee, and make their
escape, others sit still, and are overlooked, and are as a remnant of
the land; but upon both God will bring lions, beasts of prey (which are
reckoned one of God\'s four judgments, Eze. 14:21), and these shall
glean up those that have escaped the sword of the enemy. Those that
continue impenitent in sin, when they are preserved from one judgment,
are but reserved for another.
