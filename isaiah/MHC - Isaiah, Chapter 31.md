Isaiah, Chapter 31
==================

Commentary
----------

This chapter is an abridgment of the foregoing chapter; the heads of it
are much the same. Here is, `I.` A woe to those who, when the Assyrian
army invaded them, trusted to the Egyptians, and not to God, for succour
(v. 1-3). `II.` Assurance given of the care God would take of Jerusalem in
that time of danger and distress (v. 4, 5). `III.` A call to repentance
and reformation (v. 6, 7). `IV.` A prediction of the fall of the Assyrian
army, and the fright which the Assyrian king should thereby be put into
(v. 8, 9).

### Verses 1-5

This is the last of four chapters together that begin with woe; and they
are all woes to the sinners that were found among the professing people
of God, to the drunkards of Ephraim (ch. 28:1), to Ariel (ch. 29:1), to
the rebellious children (ch. 30:1), and here to those that go down to
Egypt for help; for men\'s relation to the church will not secure them
from divine woes if they live in contempt of divine laws. Observe,

`I.` What the sin was that is here reproved, v. 1. 1. Idolizing the
Egyptians, and making court to them, as if happy were the people that
had the Egyptians for their friends and allies. They go down to Egypt
for help in every exigence, as if the worshippers of false gods had a
better interest in heaven and were more likely to have success of earth
than the servants of the living and true God. That which invited them to
Egypt was that the Egyptians had many chariots to accommodate them with,
and horses and horsemen that were strong; and, if they could get a good
body of forces thence into their service, they would think themselves
able to deal with the king of Assyria and his numerous army. Their kings
were forbidden to multiply horses and chariots, and were told of the
folly of trusting to them (Ps. 20:7); but they think themselves wiser
than their Bible. 2. Slighting the God of Israel: They look not to the
Holy One of Israel, as if he were not worth taking notice of in this
distress. They advise not with him, seek not his favour, nor are in any
care to make him their friend.

`II.` The gross absurdity and folly of this sin. 1. They neglected one
whom, if they would not hope in him, they had reason to fear. They do
not seek the Lord, nor make their application to him, yet he also is
wise, v. 2. They are solicitous to get the Egyptians into an alliance
with them, because they have the reputation of a politic people; and is
not God wise too? and would not infinite wisdom, engaged on their side,
stand them in more stead than all the policies of Egypt? They are at the
pains of going down to Egypt, a tedious journey, when they might have
had better advice, and better help, by looking up to heaven, and would
not. But, if they will not court God\'s wisdom to act for them, they
shall find it act against them. He is wise, too wise for them to outwit,
and he will bring evil upon those who thus affront him. He will not call
back his words as men do (because they are fickle and foolish), but he
will arise against the house of the evil-doers, this cabal of them that
go down to Egypt; God will appear to their confusion, according to the
word that he has spoken, and will oppose the help they think to bring in
from the workers of iniquity. Some think the Egyptians made it one
condition of their coming into an alliance with him that they should
worship the gods of Egypt, and they consented to it, and therefore they
are both called evil-doers and workers of iniquity. 2. They trusted to
those who were unable to help them and would soon appear to be so, v. 3.
Let them know that the Egyptians, whom they depend so much upon, are men
and not God. As it is good for men to know themselves to be but men (Ps.
9:20), so it is good for us to consider that those we love and trust to
are but men. They therefore can do nothing without God, nothing against
him, nothing in comparison with him. They are men, and therefore fickle
and foolish, mutable and mortal, here to day and gone to morrow; they
are men, and therefore let us not make gods of them, by making them our
hope and confidence, and expecting that in them which is to be found in
God only; they are not God, they cannot do that for us which God can do,
and will, if we trust in him. Let us not then neglect him, to seek to
them; let us not forsake the rock of ages for broken reeds, nor the
fountain of living waters for broken cisterns. The Egyptians indeed have
horses that are very strong; but they are flesh, and not spirit, and
therefore, strong as they are, they may be wearied with a long march,
and become unserviceable, or be wounded and slain in battle, and leave
their riders to be ridden over. Every one knows this, that the Egyptians
are not God and their horses are not spirit; but those that seek to them
for help do not consider it, else they would not put such confidence in
them. Sinners may be convicted of folly by the plainest and most
self-evident truths, which they cannot deny, but will not believe. 3.
They would certainly be ruined with the Egyptians they trusted in, v. 3.
When the Lord does but stretch out his hand how easily, how effectually,
will he make them ashamed of their confidence in Egypt, and the
Egyptians ashamed of the encouragement they gave them to trust in them;
for he that helps and he that is helped shall fall together, and their
mutual alliance shall prove their joint ruin. The Egyptians were shortly
to be reckoned with, as appears by the burden of Egypt (ch. 19), and
then those who fled to them for shelter and succour should fall with
them; for there is no escaping the judgments of God. Evil pursues
sinners, and it is just with God to make that creature a scourge to us
which we make an idol of. 4. They took God\'s work out of his hands.
They pretended a great deal of care to preserve Jerusalem, in advising
to an alliance with Egypt; and, when others would not fall in with their
measures, they pleaded self preservation, and went to Egypt themselves.
Now the prophet here tells them that Jerusalem should be preserved
without aid from Egypt and that those who tarried there should be safe
when those who fled to Egypt should be ruined. Jerusalem was under
God\'s protection, and therefore there was no occasion to put it under
the protection of Egypt. But a practical distrust of God\'s
all-sufficiency is at the bottom of all our sinful departures from him
to the creature. The prophet tells them he had it from God\'s own mouth:
Thus hath the Lord spoken to me. They might depend upon it, `(1.)` That
God would appear against Jerusalem\'s enemies with the boldness of a
lion over his prey, v. 4. When the lion comes out to seize his prey a
multitude of shepherds come out against him; for it becomes neighbours
to help one another when persons or goods are in danger. These shepherds
dare not come near the lion; all they can do is to make a noise, and
with that they think to frighten him off. But does he regard it? No: he
will not be afraid of their voice, nor abase himself so far as to be in
the least moved by it either to quit his prey or to make any more haste
than otherwise he would do in seizing it. Thus will the Lord of hosts
come down to fight for Mount Zion, with such an unshaken undaunted
resolution not to be moved by any opposition; and he will as easily and
irresistibly destroy the Assyrian army as a lion tears a lamb in pieces.
Whoever appear against God, they are but like a multitude of poor simple
shepherds shouting at a lion, who scorns to take notice of them or so
much as to alter his pace for them. Surely those that have such a
protector need not go to Egypt for help. `(2.)` That God would appear for
Jerusalem\'s friends with the tenderness of a bird over her young, v. 5.
God was ready to gather Jerusalem, as a hen gathers her brood under her
wings (Mt. 23:37); but those that trusted to the Egyptians would not be
gathered. As birds flying to their nests with all possible speed, when
they see them attacked, and fluttering about their nests with all
possible concern, hovering over their young ones to protect them and
drive away the assailants, with such compassion and affection will the
Lord of hosts defend Jerusalem. As an eagle stirs up her young when they
are in danger, takes them and bears them on her wings, so the Lord led
Israel out of Egypt (Deu. 32:11, 12); and he has now the same tender
concern for them that he had then, so that they need not flee into Egypt
again for shelter. Defending, he will deliver it; he will so defend it
as to secure the continuance of its safety, not defend it for a while
and abandon it at last, but defend it so that it shall not fall into the
enemies\' hand. I will defend this city to save it, ch. 37:35. Passing
over he will preserve it; the word for passing over is used in this
sense only here and Ex. 12:12, 23, 27, concerning the destroying
angel\'s passing over the houses of the Israelites when he slew all the
first-born of the Egyptians, to which story this passage refers. The
Assyrian army was to be routed by a destroying angel, who should pass
over Jerusalem, though that deserved to be destroyed, and draw his sword
only against the besiegers. They shall be slain by the pestilence, but
none of the besieged shall take the infection. Thus he will again pass
over the houses of his people and secure them.

### Verses 6-9

This explains the foregoing promise of the deliverance of Jerusalem; she
shall be fitted for deliverance, and then it shall be wrought for her;
for in that method God delivers.

`I.` Jerusalem shall be reformed, and so she shall be delivered from her
enemies within her walls, v. 6, 7. Here is, 1. A gracious call to
repentance. This was the Lord\'s voice crying in the city, the voice of
the rod, the voice of the sword, and the voice of the prophets
interpreting the judgment: \"Turn you, O turn you now, from your evil
ways, unto God, return to your allegiance to him from whom the children
of Israel have deeply revolted, from whom you, O children of Israel!
have revolted.\" He reminds them of their birth and parentage, that they
were children of Israel, and therefore under the highest obligations
imaginable to the God of Israel, as an aggravation of their revolt from
him and as an encouragement to them to return to him. \"They have been
backsliding children, yet children; therefore let them return, and their
backslidings shall be healed. They have deeply revolted, with great
address as they supposed (the revolters are profound, Hos. 5:2); but the
issue will prove that they have revolted dangerously. The stain of their
sins has gone deeply into their nature, not to be easily got out, like
the blackness of the Ethiopian. They have deeply corrupted themselves
(Hos. 9:9); they have sunk deep into misery, and cannot easily recover
themselves; therefore you have need to hasten your return to God.\" 2. A
gracious promise of the good success of this call (v. 7): In that day
every man shall cast away his idols, in obedience to Hezekiah\'s orders,
which, till they were alarmed by the Assyrian invasion, many refused to
do. That is a happy fright which frightens us from our sins. `(1.)` It
shall be a general reformation: every man shall cast away his own idols,
shall begin with them before he undertakes to demolish other people\'s
idols, which there will be no need of when every man reforms himself.
`(2.)` It shall be a thorough reformation; for they shall part with their
idolatry, their beloved sin, with their idols of silver and gold, their
idols that they are most fond of. Many make an idol of their silver and
gold, and by the love of that idol are drawn to revolt from God; but
those that turn to God cast that away out of their hearts and will be
ready to part with it when God calls. `(3.)` It shall be a reformation
upon a right principle, a principle of piety, not of politics. They
shall cast away their idols, because they have been unto them for a sin,
an occasion of sin; therefore they will have nothing to do with them,
though they had been the work of their own hands, and upon that account
they had a particular fondness for them. Sin is the work of our own
hands, but in working it we have been working our own ruin, and
therefore we must cast it away; and those are strangely wedded to it who
will not be prevailed upon to cast it away when they see that otherwise
they themselves will be castaways. Some make this to be only a
prediction that those who trust in idols, when they find they stand them
in no stead, will cast them away in indignation. But it agrees so
exactly with ch. 30:22 that I rather take it as a promise of a sincere
reformation.

`II.` Jerusalem\'s besiegers shall be routed, and so she shall be
delivered from the enemies about her walls. The former makes way for
this. If a people return to God, they may leave it to him to plead their
cause against their enemies. When they have cast away their idols, then
shall the Assyrian fall, v. 8, 9. 1. The army of the Assyrians shall be
laid dead upon the spot by the sword, not of a mighty man, nor of a mean
man, not of any man at all, either Israelite or Egyptian, not forcibly
by the sword of a mighty man nor surreptitiously by the sword of a mean
man, but by the sword of an angel, who strikes more strongly than a
mighty man and yet more secretly than a mean man, by the sword of the
Lord, and his power and wrath in the hand of the angel. Thus the young
men of the army shall melt, and be discomfited, and become tributaries
to death. When God has work to do against the enemies of his church we
expect it must be done by mighty men and mean men, officers and common
soldiers; whereas God can, if he please, do it without either. He needs
not armies of men who has legions of angels at command, Mt. 26:53. 2.
The king of Assyria shall flee for the same, shall flee from that
invisible sword, hoping to get out of the reach of it; and he shall make
the best of his way to his own dominions, shall pass over to some
strong-hold of his own, for fear lest the Jews should pursue him now
that his army was routed. Sennacherib had been very confident that he
should make himself master of Jerusalem, and in the most insolent manner
had set both God and Hezekiah at defiance; yet now he is made to tremble
for fear of both. God can strike a terror into the proudest of men, and
make the stoutest heart to tremble. See Job 18:11; 20:24. His princes
that accompany him shall be afraid of the ensign, shall be in a
continual fright at the remembrance of the ensign in the air, which
perhaps the destroying angel displayed before he gave the fatal bow. Or
they shall be afraid of every ensign they see, suspecting it is a party
of the Jews pursuing them. The banner that God displays for the
encouragement of his people (Ps. 60:4) will be a terror to his and their
enemies. Thus he cuts off the spirit of princes and is terrible to the
kings of the earth. But who will do this? It is the Lord, whose fire is
in Zion and his furnace in Jerusalem. `(1.)` Whose residence is there, and
who there keeps house, as a man does where his fire and his oven are. It
is the city of the great King, and let not the Assyrians think to turn
him out of the possession of his own house. `(2.)` Who is there a
consuming fire to all his enemies and will make them as a fiery oven in
the day of his wrath, Ps. 21:9. He is himself a wall of fire round about
Jerusalem, so that whoever assaults her does so at his peril, Zec. 2:5;
Rev. 11:5. `(3.)` Who has his altar there, on which the holy fire is
continually kept burning and sacrifices are daily offered to his honour,
and with which he is well pleased; and therefore he will defend this
city, especially having an eye to the great sacrifice which was there
also to be offered, of which all the sacrifices were types. If we keep
up the fire of holy love and devotion in our hearts and houses, we may
depend upon God to be a protection to us and them.
