Introduction to Micah
=====================

We shall have some account of this prophet in the first verse of the
book of his prophecy; and therefore shall here only observe that, being
contemporary with the prophet Isaiah (only that he began to prophesy a
little after him), there is a near resemblance between that prophet\'s
prophecy and this; and there is a prediction of the advancement and
establishment of the gospel-church, which both of them have, almost in
the same words, that out of the mouth of two such witnesses so great a
word might be established. Compare Isa. 2:2, 3, with Mic. 4:1, 2.
Isaiah\'s prophecy is said to be concerning Judah and Jerusalem, but
Micah\'s concerning Samaria and Jerusalem; for, though this prophecy be
dated only by the reigns of the kings of Judah, yet it refers to the
kingdom of Israel, the approaching ruin of which, in the captivity of
the ten tribes, he plainly foretels and sadly laments. What we find here
in writing was but an abstract of the sermons he preached during the
reigns of three kings. The scope of the whole is, `I.` To convince sinners
of their sins, by setting them in order before them, charging both
Israel and Judah with idolatry, covetousness, oppression, contempt of
the word of God, and their rulers especially, both in church and state,
with the abuse of their power; and also by showing them the judgments of
God ready to break in upon them for their sins. `II.` To comfort God\'s
people with promises of mercy and deliverance, especially with an
assurance of the coming of the Messiah and of the grace of the gospel
through him. It is remarkable concerning this prophecy, and confirms its
authority, that we find two quotations out of it made publicly upon very
solemn occasions, and both referring to very great events. 1. One is a
prediction of the destruction of Jerusalem (3:12), which we find quoted
in the Old Testament, by the elders of the land (Jer. 26:17, 18), in
justification of Jeremiah, when he foretold the judgments of God coming
upon Jerusalem, and to stay the proceedings of the court against him.
\"Micah (say they) foretold that Zion should be ploughed as a field, and
Hezekiah did not put him to death; why then should we punish Jeremiah
for saying the same?\" 2. Another is a prediction of the birth of Christ
(5:2) which we find quoted in the New Testament, by the chief priests
and scribes of the people, in answer to Herod\'s enquiry, where Christ
should be born (Mt. 2:5, 6); for still we find that to him bear all the
prophets witness.
