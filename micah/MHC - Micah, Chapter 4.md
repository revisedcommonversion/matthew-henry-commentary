Micah, Chapter 4
================

Commentary
----------

Comparing this chapter with the close of the foregoing chapter, the
comfortable promises here with the terrible threatenings there, we may,
with the apostle, \"behold the goodness and severity of God,\" (Rom.
11:22), towards the Jewish church which fell, severity when Zion was
ploughed as a field, but towards the Christian church, which was built
upon the ruins of it, goodness, great goodness; for it is here promised,
`I.` That it shall be advanced and enlarged by the accession of the
nations to it (v. 1, 2). `II.` That it shall be protected in tranquility
and peace (v. 3, 4). `III.` That it shall be kept close, and constant, and
faithful to God (v. 5). `IV.` That under Christ\'s government, all its
grievances shall be redressed (v. 6, 7). `V.` That it shall have an ample
and flourishing dominion (v. 8). `VI.` That its troubles shall be brought
to a happy issue at length (v. 9, 10). `VII.` That its enemies shall be
disquieted, nay, that they shall be destroyed in and by their attempts
against it (v. 11-13).

### Verses 1-7

It is a very comfortable but with which this chapter begins, and very
reviving to those who lay the interests of God\'s church near their
heart and are concerned for the welfare of it. When we sometimes see the
corruptions of the church, especially of church-rulers, princes,
priests, and prophets, seeking their own things and not the things of
God, and when we soon after see the desolations of the church, Zion for
their sakes ploughed as a field, we are ready to fear that it will one
day perish between both, that the name of Israel shall be no more in
remembrance; we are ready to give up all for gone, and to conclude the
church will have neither root not branch upon earth. But let not our
faith fail in this matter; out of the ashes of the church another
phoenix shall arise. In the last words of the foregoing chapter we left
the mountain of the house as desolate and waste as the high places of
the forest; and is it possible that such a wilderness should ever become
a fruitful field again? Yes, the first words of this chapter bring in
the mountain of the Lord\'s house as much dignified by being frequented
as ever it had been disgraced by being deserted. Though Zion be ploughed
as a field, yet God has not cast off his people, but by the fall of the
Jews salvation has come to the Gentiles, so that it proves to be the
riches of the world, Rom. 11:11, 12. This is the mystery which God by
the prophet here shows us, and he says the very same in the first three
verses of this chapter which another prophet said by the word of the
Lord at the same time (Isa. 2:2-4), that out of the mouth of these two
witnesses these promises might be established; and very precious
promises they are, relating to the gospel-church, which have been in
part accomplished, and will be yet more and more, for he is faithful
that has promised.

`I.` That there shall be a church for God set up in the world, after the
defection and destruction of the Jewish church, and this in the last
days; that is, as some of the rabbin themselves acknowledge, in the days
of the Messiah. The people of God shall be incorporated by a new
charter, a new spiritual way of worship shall be enacted, and a new
institution of offices to attend it; better privileges shall be granted
by this new charter, and better provision made for enlarging and
establishing the kingdom of God among men than had been made by the
Old-Testament constitution: The mountain of the house of the Lord shall
again appear firm ground for God\'s faithful worshippers to stand, and
go, and build upon, in their attendance on him, v. 1. And it shall be a
centre of unity to them; a church shall be set up in the world, to which
the Lord will be daily adding such as shall be saved.

`II.` That this church shall be firmly founded and well-built: It shall
be established in the top of the mountains; Christ himself will build it
upon a rock; it shall be an impregnable fort upon an immovable
foundation, so that the gates of hell shall neither overthrow the one
nor undermine the other (Mt. 16:18); its foundations are still in the
holy mountains (Ps. 87:1), the everlasting mountains, which cannot,
which shall not, be removed. It shall be established, not as the temple,
upon one mountain, but upon many; for the foundations of the church, as
they are sure, so they are large.

`III.` That it shall be highly advanced, and become eminent and
conspicuous: It shall be exalted above the hills, observed with wonder
for its growing greatness from small beginnings. The kingdom of Christ
shall shine with greater lustre than ever any of the kingdoms of the
earth did. It shall be as a city on a hill, which cannot be hid, Mt.
5:14. The glory of this latter house is greater than that of the former,
Hag. 2:9. See 2 Co. 3:7, 8, etc.

`IV.` That there shall be a great accession of converts to it and
succession of converts in it. People shall flow unto it as the waters of
a river are continually flowing; there shall be a constant stream of
believers flowing in from all parts into the church, as the people of
the Jews flowed into the temple, while it was standing, to worship
there. Then many tribes came to the mountain of the house, to enquire of
God\'s temple; but in gospel-times many nations shall flow into the
church, shall fly like a cloud and as the doves to their windows.
Ministers shall be sent forth to disciple all nations, and they shall
not labour in vain; for, multitudes being wrought upon to believe the
gospel and embrace the Christian religion, they shall excite and
encourage one another, and shall say, \"Come, and let us go up to the
mountain of the Lord now raised among us, even to the house of the God
of Jacob, the spiritual temple which we need not travel far to, for it
is brought to our doors and set up in the midst of us.\" Thus shall
people be made willing in the day of his power (Ps. 110:3), and shall do
what they can to make others willing, as Andrew invited Peter, and
Philip Nathanael, to be acquainted with Christ. They shall call the
people to the mountain (Deu. 33:19), for there is in Christ enough for
all, enough for each. Now observe what it is, 1. Which these converts
expect to find in the house of the God of Jacob. They come thither for
instruction: \"He will teach us of his ways, what is the way in which he
would have us to walk with him and in which we may depend upon him to
meet us graciously.\" Note, Where we come to worship God we come to be
taught of him. 2. Which they engage to do when they are thus taught of
God: We will walk in his paths. Note, Those may comfortably expect that
God will teach them who are firmly resolved by his grace to do as they
are taught.

`V.` That, in order to this, a new revelation shall be published to the
world, on which the church shall be founded, and by which multitudes
shall be brought into it: For the law shall go forth of Zion, and the
word of the Lord from Jerusalem. The gospel is here called the word of
the Lord, for the Lord gave the word, and great was the company of those
that published it, Ps. 68:11. It was of a divine original, a divine
authority; it began to be spoken by the Lord Christ himself, Heb. 2:3.
And it is a law, a law of faith; we are under the law to Christ. This
was to go forth from Jerusalem, from Zion, the metropolis of the
Old-Testament dispensation, where the temple, and altars, and oracles
were, and whither the Jews went to worship from all parts; thence the
gospel must take rise, to show the connexion between the Old Testament
and the New, that the gospel is not set up in opposition to the law, but
is an explication and illustration of it, and a branch growing out of
its roots. It was in Jerusalem that Christ preached and wrought
miracles; there he died, rose again, and ascended; there the Spirit was
poured out; and those that were to preach repentance and remission of
sins to all nations were ordered to begin at Jerusalem, so that thence
flowed the streams that were to water the desert world.

`VI.` That a convincing power should go along with the gospel of Christ,
in all places where it should be preached (v. 3): He shall judge among
many people. Messiah, the lawgiver (v. 2.), is here the judge, for to
him the Father committed all judgment, and for judgment he came into
this world; his word, the word of his gospel, that was to go forth from
Jerusalem, was the golden sceptre by which he shall rule and judge when
he sits as king on the holy hill of Zion, Ps. 2:6. By it he shall rebuke
strong nations afar off; for the Spirit working with the word shall
reprove the world, Jn. 16:8. It is promised to the Son of David that he
shall judge among the heathen (Ps. 110:6), which he does when in the
chariot of his everlasting gospel he goes forth, and goes on, conquering
and to conquer.

`VII.` That a disposition to mutual peace and love shall be the happy
effect of the setting up of the kingdom of the Messiah: They shall beat
their swords into plough-shares; that is, angry passionate men, that
have been fierce and furious, shall be wonderfully sweetened, and made
mild and meek, Tit. 3:2, 3. Those who, before their conversion, did
injuries, and would bear none, after their conversion can bear injuries,
but will do none. As far as the gospel prevails it makes men peaceable,
for such is the wisdom from above; it is gentle and easy to be
entreated; and if nations were but leavened by it, there would be
universal peace. When Christ was born there was universal peace in the
Roman empire; those that were first brought into the gospel church were
all of one heart and of one soul (Acts 4:32); and it was observed of the
primitive Christians how well they loved one another. In heaven this
will have its full accomplishment. It is promised, 1. That none shall be
quarrelsome. The art of war, instead of being improved (which some
reckon the glory of a kingdom), shall be forgotten and laid aside as
useless. They shall not learn war any more as they have done, for they
shall have no need to defend themselves nor any inclination to offend
their neighbours. Nation shall no longer lift up sword against nation;
not that the gospel will make men cowards, but it will make men
peaceable. 2. That all shall be quiet, both from evil and from the fear
of evil (v. 4): They shall sit safely, and none shall disturb them; they
shall sit securely, and shall not disturb themselves, every man under
his vine and under his fig-tree, enjoying the fruit of them, and needing
no other shelter than the leaves of them. None shall make them afraid;
not only there shall be nothing that is likely to frighten them, but
they shall not be disposed to fear. under the dominion of Christ, as
that of Solomon, there shall be abundance of peace. Though his followers
have trouble in the world, in him they enjoy great tranquillity. If this
seems unlikely, yet we may depend upon it, for the mouth of the Lord has
spoken it, and no word of his shall fall to the ground; what he has
spoken by his word he will do by his providence and grace. He that is
the Lord of hosts will be the God of peace; and those may well be easy
whom the Lord of hosts, of all hosts, undertakes the protection of.

`VIII.` That the churches shall be constant in their duty, and so shall
make a good use of their tranquillity and shall not provoke the Lord to
deprive them of it, v. 5. When the churches have rest they shall be
edified, and confirmed, and comforted, and shall resolve to be as firm
to their God as other nations are to theirs, though they be no gods.
Where we find the foregoing promises, Isa. 2:2, etc. it follows (v. 5),
O house of Jacob! come ye, and let us walk in the light of the Lord; and
here, We will walk in the name of the Lord our God. Note, Peace is a
blessing indeed when it strengthens our resolutions to cleave to the
Lord. Observe, 1. How constant other nations were to their gods: All
people will walk every one in the name of his god, will own their god
and cleave to him, will worship their god and serve him, will depend
upon him and put confidence in him. Whatever men make a god of they will
make use of, and take his name along with them in all their actions and
affairs. The mariners, in a storm, cried every man to his god, Jonah
1:5. And no instance could be found of a nation\'s changing its gods,
Jer. 2:11: If the hosts of heaven were their gods, they loved them, and
served them, and walked after them, Jer. 8:2. 2. How constant God\'s
people now resolve to be to him: \"We will walk in the name of the Lord
our God, will acknowledge him in all our ways, and govern ourselves by a
continual regard to him, doing nothing but what we have warrant from him
for, and openly professing our relation to him.\" Observe, Their
resolution is peremptory; it is not a thing that needs be disputed: \"We
will walk in the name of the Lord our God.\" It is just and reasonable:
He is our God. And it is a resolution for a perpetuity: \"We will do it
for ever and ever, and will never leave him. He will be ours for ever,
and therefore so we will be his, and never repent our choice.\"

`IX.` That notwithstanding the dispersions, distress, and infirmities of
the church, it shall be formed and established, and made very
considerable, v. 6, 7. 1. The state of the church had been low, and
weak, and very helpless, in the latter times of the Old Testament,
partly through the corruptions of the Jewish nation, and partly through
the oppressions under which they groaned. They were like a flock of
sheep that were maimed, worried, and scattered, Eze. 34:16; Jer. 50:6.
17. The good people among them, and in other places, that were well
inclined, were dispersed, were very infirm, and in a manner lost and
cast far off. 2. It is promised that all these grievances shall be
redressed and the distemper healed. Christ will come himself (Mt.
15:24), and send his apostles to the lost sheep of the house of Israel,
Mt. 10:6. From among the Jews that halted, or that for want of strength,
could not go upright, God gathered a remnant (v. 7), that remnant
according to the election of grace which is spoken of in Rom. 11:7,
which embraced the gospel of Christ. And from among the Gentiles that
were cast far off (so the Gentiles are described to be, Eph. 2:13, Acts
2:39) he raised a strong nation; greater numbers of them were brought
into the church than of the Jews, Gal. 4:27. And such a strong nation
the gospel-church is that the gates of hell shall never be able to
prevail against it. The church of Christ is more numerous than any other
nation, and strong in the Lord and in the power of his might.

`X.` That the Messiah shall be the king of this kingdom, shall protect and
govern it, and order all the affairs of it for the best, and this to the
end of time. The Lord Jesus shall reign over them in Mount Zion by his
word and Spirit in his ordinances, and this henceforth and for ever, for
of the increase of his government and peace there shall be no end.

### Verses 8-13

These verses relate to Zion and Jerusalem, here called the tower of the
flock or the tower of Edor; we read of such a place (Gen. 35:21) near
Bethlehem; and some conjecture it is the same place where the shepherds
were keeping their flocks when the angels brought them tidings of the
birth of Christ, and some think Bethlehem itself is here spoken of, as
ch. 5:2. Some think it is a tower at that gate of Jerusalem which is
called the sheep-gate (Neh. 3:32), and conjecture that through that gate
Christ rode in triumph into Jerusalem. However, it seems to be put for
Jerusalem itself, or for Zion the tower of David. All the sheep of
Israel flocked thither three times a year; it was the stronghold (Ophel,
which is also a name of a place in Jerusalem, Neh. 3:27), or castle, of
the daughter of Zion. Now here,

`I.` We have a promise of the glories of the spiritual Jerusalem, the
gospel-church, which is; the tower of the flock, that one fold in which
all the sheep of Christ are protected under one Shepherd: \"Unto thee
shall it come; that which thou hast long wanted and wished for, even the
first dominion, a dignity and power equal to that of David and Solomon,
by whom Jerusalem was first raised, that kingdom shall again come to the
daughter of Jerusalem, which it was deprived of at the captivity. It
shall make as great a figure and shine with as much lustre among the
nations, and have as much influence upon them, as ever it had; this is
the first or chief dominion.\" Now this had by no means its
accomplishment in Zerubbabel; his was nothing like the first dominion
either in respect of splendour and sovereignty at home or the extent of
power abroad; and therefore it must refer to the kingdom of the Messiah
(and to that the Chaldee-paraphrase refers it) and had its
accomplishment when God gave to our Lord Jesus the throne of his father
David (Lu. 1:32), set him king upon the holy hill of Zion and gave him
the heathen for his inheritance (Ps. 2:6), made him, his first-born,
higher than the kings of the earth, Ps. 89:27; Dan. 7:14. David, in
spirit, called him Lord, and (as Dr. Pocock observes) he witnessed of
himself, and his witness was true, that he was greater than Solomon,
none of their dominions being like his for extent and duration. The
common people welcomed Christ into Jerusalem with hosannas to the son of
David, to show that it was the first dominion that came to the daughter
of Zion; and the evangelist applies it to the promise of Zion\'s king
coming to her, Mt. 21:5; Zec. 9:9. Some give this sense of the words: To
Zion, and Jerusalem that tower of the flock, to the nation of the Jews,
came the first dominion; that is, there the kingdom of Christ was first
set up, the gospel of the kingdom was first preached (Lu. 24:47), there
Christ was first called king of the Jews.

`II.` This is illustrated by a prediction of the calamities of the
literal Jerusalem, to which some favour and relief should be granted, as
a type and figure of what God would do for the gospel-Jerusalem in the
last days, notwithstanding its distresses. We have here,

`1.` Jerusalem put in pain by the providences of God. \"She cries out
aloud, that all her neighbours may take notice of her griefs, because
there is no king in her, none of that honour and power she used to have.
Instead of ruling the nations, as she did when she sat a queen, she is
ruled by them, and has become a captive. Her counsellors have perished;
she is no longer at her own disposal, but is given up to the will of her
enemies, and is governed by their counsellors. Pangs have taken her.\"
`(1.)` She is carried captive to Babylon, and there is in pangs of grief.
\"She goes forth out of the city, and is constrained to dwell in the
field, exposed to all manner of inconveniences; she goes even to
Babylon, and there wears out seventy tedious years in a miserable
captivity, all that while in pain, as a woman in travail, waiting to be
delivered, and thinking the time very long.\" `(2.)` When she is delivered
out of Babylon, and redeemed from the hand of her enemies there, yet
still she is in pangs of fear; the end of one trouble is but the
beginning of another; for now also, when Jerusalem is in the rebuilding,
many nations are gathered against her, v. 11. They were so in Ezra\'s
and Nehemiah\'s time, and did all they could to obstruct the building of
the temple and the wall. They were so in the time of the Maccabees; they
said, Let her be defiled; let her be looked upon as a place polluted
with sin, and be forsaken and abandoned both of God and man; let her
holy places be profaned and all her honours laid in the dust; let our
eye look upon Zion, and please itself with the sight of its ruins, as it
is said of Edom (Obad. 12, Thou shouldst not have looked upon the day of
thy brother); let our eyes see our desire upon Zion, the day we have
long wished for. When they hear the enemies thus combine against them,
and insult over them, no wonder that they are in pain, and cry aloud.
Without are fightings, within are fears.

`2.` Jerusalem made easy by the promises of God: \"Why dost thou cry out
aloud? Let thy griefs and fears be silenced; indulge not thyself in
them, for, though things are bad with thee, they shall end well; thy
pangs are great, but they are like those of a woman in travail (v. 9),
that labours to bring forth (v. 10), the issue of which will be good at
last.\" Jerusalem\'s pangs are not as dying agonies, but as travailing
throes, which after a while will be forgotten, for joy that a child is
born into the world. Let the literal Jerusalem comfort herself with
this, that, whatever straits she may be reduced to, she shall continue
until the coming of the Messiah, for there his kingdom must be first set
up, and she shall not be destroyed while that blessing is in her; and
when at length she is ploughed as a field, and become heaps (as is
threatened, ch. 3:12), yet her privileges shall be resigned to the
spiritual Jerusalem, and in that the promises made to her shall be
fulfilled. Let Jerusalem be easy then, for, `(1.)` Her captivity in
Babylon shall have an end, a happy end (v. 10): There shalt thou be
delivered, and the Lord shall redeem thee from the hand of thy enemies
there. This was done by Cyrus, who acted therein as God\'s servant; and
that deliverance was typical of our redemption by Jesus Christ, and the
release from our spiritual bondage which is proclaimed in the
everlasting gospel, that acceptable year of the Lord, in which Christ
himself preached liberty to the captives, and the opening of the prison
to those that were bound, Lu. 4:18, 19. `(2.)` The designs of her enemies
against her afterwards shall be baffled, nay, they shall turn upon
themselves, v. 12, 13. They promise themselves a day of it, but it shall
prove God\'s day. They are gathered against Zion, to destroy it, but it
shall prove to their own destruction, which Israel and Israel\'s God
shall have the glory of. `[1.]` Their coming together against Zion shall
be the occasion of their ruin. They associate themselves, and gird
themselves, that they may break Jerusalem in pieces, but it will prove
that they shall be broken in pieces, Isa. 8:9. They know not the
thoughts of the Lord. When they are gathering together, and Providence
favours them in it, they little think what God is designing by it, nor
do they understand his counsel; they know what they aim at in coming
together, but they know not what God aims at in bringing them together;
they aim at Zion\'s ruin, but God aims at theirs. Note, When men are
made use of as instruments of Providence in accomplishing its purposes
it is very common for them to intend one thing and for God to intend
quite the contrary. The king of Assyria is to be a rod in God\'s hand
for the correction of his people, in order to their reformation; howbeit
he means not so, nor does his heart think so, Isa. 10:7. And thus it is
here; the nations are gathered against Zion, as soldiers into the field,
but God gathers them as sheaves into the floor, to be beaten to pieces;
and they could not have been so easily, so effectually, destroyed, if
they had not gathered together against Zion. Note, The designs of
enemies for the ruin of the church often prove ruining to themselves;
and thereby they prepare themselves for destruction and put themselves
in the way of it; they are snared in the work of their own hands. `[2.]`
Zion shall have the honour of being victorious over them, v. 13. When
they are gathered as sheaves into the floor, to be trodden down, as the
corn then was by the oxen, then, \"Arise, and thresh, O daughter of
Zion! instead of fearing them, and fleeing from them, boldly set upon
them, and take the opportunity Providence favours thee with of trampling
upon them. Plead not thy own weakness, and that thou art not a match for
so many confederated enemies; God will make thy horn iron, to push them
down, and thy hoofs brass, to tread upon them when they are down; and
thus thou shalt beat in pieces many people, that have long been beating
thee in pieces.\" Thus, when God pleases, the daughter of Babylon is
made a threshing floor (it is time to thresh her, Jer. 51:33), and the
worm Jacob is made a threshing instrument, with which God will thresh
the mountains, and make them as chaff, Isa. 41:14, 15. How strangely,
how happily, are the tables turned, since Jacob was the threshing-floor
and Babylon the threshing instrument! Isa. 21:10. Note, When God has
conquering work for his people to do he will furnish them with strength
and ability for it, will make the horn iron and the hoofs brass; and,
when he does so, they must exert the power he gives them, and execute
the commission; even the daughter of Zion must arise, and thresh. `[3.]`
The glory of the victory shall redound to God. Zion shall thresh these
sheaves in the floor, but the corn threshed out shall be a meat-offering
at God\'s altar: I will consecrate their gain unto the Lord (that is, I
will have it consecrated) and their substance unto the Lord of the whole
earth. The spoils gained by Zion\'s victory shall be brought into the
sanctuary, and devoted to God, either in part, as those of Midian (Num.
31:28), or in whole, as those of Jericho, Jos. 6:17. God is Jehovah, the
fountain of being; he is the Lord of the whole earth, the fountain of
power; and therefore he needs not any of our gain or substance, but may
challenge and demand it all if he please; and with ourselves we must
devote all we have to his honour, to be employed as he directs. Thus far
all we have must have holiness to the Lord written upon it, all our gain
and substance must be consecrated to the Lord of the whole earth, Isa.
23:18. And extraordinary successes call for extraordinary
acknowledgments, whether they be of spoils in war or gains in trade. It
is God that gives us power to get wealth, which way soever it is
honestly got, and therefore he must be honoured with what we get. Some
make all this to point at the defeat of Sennacherib when he besieged
Jerusalem, others to the destruction of Babylon, others to the successes
of the Maccabees; but the learned Dr. Pocock and others think it had its
full accomplishment in the spiritual victories obtained by the gospel of
Christ over the powers of darkness that fought against it. The nations
thought to ruin Christianity in its infancy, but it was victorious over
them; those that persisted in their enmity were broken to pieces (Mt.
21:44), particularly the Jewish nation; but multitudes by divine grace
were gained to the church, and they and their substance were consecrated
to the Lord Jesus, the Lord of the whole earth.
