Matthew, Chapter 17
===================

Commentary
----------

In this chapter we have, `I.` Christ in his pomp and glory transfigured
(v. 1-13). `II.` Christ in his power and grace, casting the devil out of a
child (v. 14-21). And, `III.` Christ in his poverty and great humiliation,
`1.` Foretelling his own sufferings (v. 22, 23). 2. Paying tribute (v.
24-27). So that here is Christ, the Brightness of his Father\'s glory,
by himself purging our sins, paying our debts, and destroying for us him
that had the power of death, that is, the devil. Thus were the several
indications of Christ\'s gracious intentions admirable interwoven.

### Verses 1-13

We have here the story of Christ\'s transfiguration; he had said that
the Son of man should shortly come in his kingdom, with which promise
all the three evangelists industriously connect this story; as if
Christ\'s transfiguration were intended for a specimen and an earnest of
the kingdom of Christ, and of that light and love of his, which therein
appears to his select and sanctified ones. Peter speaks of this as the
power and coming of our Lord Jesus (2 Pt. 1:16); because it was an
emanation of his power, and a previous notice of his coming, which was
fitly introduced by such prefaces.

When Christ was here in his humiliation, though his state, in the main,
was a state of abasement and afflictions, there were some glimpses of
his glory intermixed, that he himself might be the more encouraged in
his sufferings, and others the less offended. His birth, his baptism,
his temptation, and his death, were the most remarkable instances of his
humiliation; and these were each of them attended with some signal
points of glory, and the smiles of heaven. But the series of his public
ministry being a continued humiliation, here, just in the midst of that,
comes in this discovery of his glory. As, now that he is in heaven, he
has his condescensions, so, when he was on earth, he had his
advancements.

Now concerning Christ\'s transfiguration, observe,

`I.` The circumstances of it, which are here noted, v. 1.

`1.` The time; six days after he had the solemn conference with his
disciples, ch. 16:21. St Luke saith, It was about eight days after, six
whole days intervening, and this the eighth day, that day seven-night.
Nothing is recorded to be said or done by our Lord Jesus for six days
before his transfiguration; thus, before some great appearances, there
was silence in heaven for the space of half an hour, Rev. 8:1. Then when
Christ seems to be doing nothing for his church, expect, ere long,
something more than ordinary.

`2.` The place; it was on top of a high mountain apart. Christ chose a
mountain, `(1.)` As a secret place. He went apart; for though a city upon
a hill can hardly be hid, two or three persons upon a hill can hardly be
found; therefore their private oratories were commonly on mountains.
Christ chose a retired place to be transfigured in, because his
appearing publicly in his glory was not agreeable to his present state;
and thus he would show his humility, and teach us that privacy much
befriends our communion with God. Those that would maintain intercourse
with Heaven, must frequently withdraw from the converse and business of
this world; and they will find themselves never less alone than when
alone, for the Father is with them. `(2.)` Though a sublime place,
elevated above things below. Note, Those that would have a transforming
fellowship with God, must not only retire, but ascend; lift up their
hearts, and seek things above. The call is, Come up hither, Rev. 4:1.

`3.` The witnesses of it. He took with him Peter and James and John. `(1.)`
He took three, a competent number to testify what they should see; for
out of the mouth of two or three witnesses shall every word be
established. Christ makes his appearances certain enough, but not too
common; not to all the people, but to witnesses (Acts 10:41), that they
might be blessed, who have not seen, and yet have believed. `(2.)` He took
these three because they were the chief of his disciples, the first
three of the worthies of the Son of David; probably they excelled in
gifts and graces; they were Christ\'s favourites, singled out to be the
witnesses of his retirements. They were present when he raised the
damsel to life, Mk. 5:37. They were afterward to be the witnesses of his
agony, and this was to prepare them for that. Note, A sight of Christ\'s
glory, while we are here in this world, is a good preparative for our
sufferings with him, as these are preparatives for the sight of his
glory in the other world. Paul, who had abundance of trouble, had
abundance of revelations.

`II.` The manner of it (v. 2); He was transfigured before them. The
substance of his body remained the same, but the accidents and
appearances of it were greatly altered; he was not turned into a spirit,
but his body, which had appeared in weakness and dishonour, now appeared
in power and glory. He was transfigured, metamorphoµtheµ-he was
metamorphosed. The profane poets amused and abused the world with idle
extravagant stories of metamorphoses, especially the metamorphoses of
their gods, such as were disparaging and diminishing to them, equally
false and ridiculous; to these some think Peter has an eye, when, being
about to mention this transfiguration of Christ, he saith, We have not
followed cunningly devised fables when we made it known unto you, 2 Pt.
1:16. Christ was both God and man; but, in the days of his flesh, he
took on him the form of a servant-morpheµn doulou, Phil. 2:7. He drew a
veil over the glory of his godhead; but now, in his transfiguration, he
put by that veil, appeared en morpheµ theou-in the form of God (Phil.
2:6), and gave his disciples a glimpse of his glory, which could not but
change his form.

The great truth which we declare, is, that God is light (1 Jn. 1:5),
dwells in the light (1 Tim. 6:16), covers himself with light, Ps. 104:2.
And therefore when Christ would appear in the form of God, he appeared
in light, the most glorious of all visible beings, the first-born of the
creation, and most nearly resembling the eternal Parent. Christ is the
Light; while he was in the world, he shined in darkness, and therefore
the world knew him not (Jn. 1:5, 10); but, at this time, that Light
shined out of the darkness.

Now his transfiguration appeared in two things:

`1.` His face did shine as the sun. The face is the principal part of the
body, by which we are known; therefore such a brightness was put on
Christ\'s face, that face which afterward he hid not from shame and
spitting. It shone as the sun when he goes forth in his strength, so
clear, so bright; for he is the Sun of righteousness, the Light of the
world. The face of Moses shone but as the moon, with a borrowed
reflected light, but Christ\'s shone as the sun, with an innate inherent
light, which was the more sensibly glorious, because it suddenly broke
out, as it were, from behind a black cloud.

`2.` His raiment was white as the light. All his body was altered, as his
face was; so that beams of light, darting from every part through his
clothes, made them white and glittering. The shining of the face of
Moses was so weak, that it could easily be concealed by a thin veil; but
such was the glory of Christ\'s body, that his clothes were enlightened
by it.

`III.` The companions of it. He will come, at last, with ten thousands of
his saints; and, as a specimen of that, there now appeared unto them
Moses and Elias talking with him, v. 3. Observe, 1. There were glorified
saints attending him, that, when there were three to bear record on
earth, Peter, James, and John, there might be some to bear record from
heaven too. Thus here was a lively resemblance of Christ\'s kingdom,
which is made up of saints in heaven and saints on earth, and to which
belong the spirits of just men made perfect. We see here, that they who
are fallen asleep in Christ are not perished, but exist in a separate
state, and shall be forthcoming when there is occasion. 2. These two
were Moses and Elias, men very eminent in their day. They had both
fasted forty days and forty nights, as Christ did, and wrought other
miracles, and were both remarkable at their going out of the world as
well as in their living in the world. Elias was carried to heaven in a
fiery chariot, and died not. The body of Moses was never found, possibly
it was preserved from corruption, and reserved for this appearance. The
Jews had great respect for the memory of Moses and Elias, and therefore
they came to witness of him, they came to carry tidings concerning him
to the upper world. In them the law and the prophets honoured Christ,
and bore testimony to him. Moses and Elias appeared to the disciples;
they saw them, and heard them talk, and, either by their discourse or by
information from Christ, they knew them to be Moses and Elias; glorified
saints shall know one another in heaven. They talked with Christ. Note,
Christ has communion with the blessed, and will be no stranger to any of
the members of that glorified corporation. Christ was now to be sealed
in his prophetic office, and therefore these two great prophets were
fittest to attend him, as transferring all their honour and interest to
him; for in these last days God speaks to us by his Son, Heb. 1:1.

`IV.` The great pleasure and satisfaction that the disciples took in the
sight of Christ\'s glory. Peter, as usual, spoke or the rest; Lord, it
is good for us to be here. Peter here expresses,

`1.` The delight they had in this converse; Lord, it is good to be here.
Though upon a high mountain, which we may suppose rough and unpleasant,
bleak and cold, yet it is good to be here. He speaks the sense of his
fellow-disciples; It is good not only for me, but for us. He did not
covet to monopolize this favour, but gladly takes them in. He saith this
to Christ. Pious and devout affections love to pour out themselves
before the Lord Jesus. The soul that loves Christ, and loves to be with
him, loves to go and tell him so; Lord, it is good for us to be here.
This intimates a thankful acknowledgment of his kindness in admitting
them to this favour. Note, Communion with Christ is the delight of
Christians. All the disciples of the Lord Jesus reckon it is good for
them to be with him in the holy mount. It is good to be here where
Christ is, and whither he brings us along with him by his appointment;
it is good to be here, retired and alone with Christ; to be here, where
we may behold the beauty of the Lord Jesus, Ps. 27:4. It is pleasant to
hear Christ compare notes with Moses and the prophets, to see how all
the institutions of the law, and all the predictions of the prophets,
pointed at Christ, and were fulfilled in him.

`2.` The desire they had of the continuance of it; Let us make here three
tabernacles. There was in this, as in many other of Peter\'s sayings, a
mixture of weakness and of goodwill, more zeal than discretion.

`(1.)` Here was a zeal for this converse with heavenly things, a laudable
complacency in the sight they had of Christ\'s glory. Note, Those that
by faith behold the beauty of the Lord in his house, cannot but desire
to dwell there all the days of their life. It is good having a nail in
God\'s holy place (Ezra 9:8), a constant abode; to be in holy ordinances
as a man at home, not as a wayfaring man. Peter thought this mountain
was a fine spot of ground to build upon, and he was for making
tabernacles there; as Moses in the wilderness made a tabernacle for the
Shechinah, or divine glory.

It argued great respect for his Master and the heavenly guests, with
some commendable forgetfulness of himself and his fellow-disciples, that
he would have tabernacles for Christ, and Moses, and Elias, but none for
himself. He would be content to lie in the open air, on the cold ground,
in such good company; if his Master have but where to lay his head, no
matter whether he himself has or no.

`(2.)` Yet in this zeal he betrayed a great deal of weakness and
ignorance. What need had Moses and Elias of tabernacles? They belonged
to that blessed world, where they hunger no more, nor doth the sun light
upon them. Christ had lately foretold his sufferings, and bidden his
disciples expect the like; Peter forgets this, or, to prevent it, will
needs be building tabernacles in the mount of glory, out of the way of
trouble. Still he harps upon, Master, spare thyself, though he had been
so lately checked for it. Note, There is a proneness in good men to
expect the crown without the cross. Peter was for laying hold of this as
the prize, though he had not yet fought his fight, nor finished his
course, as those other disciples, ch. 20:21. We are out in our aim, if
we look for a heaven here upon earth. It is not for strangers and
pilgrims (such as we are in our best circumstances in this world), to
talk of building, or to expect a continuing city.

Yet it is some excuse for the incongruity of Peter\'s proposal, not only
that he knew not what he said (Lu. 9:33), but also that he submitted the
proposal to the wisdom of Christ; If thou wilt, let us make tabernacles.
Note, Whatever tabernacles we propose to make to ourselves in this
world, we must always remember to ask Christ\'s leave.

Now to this which Peter said, there was no reply made; the disappearing
of the glory would soon answer it. They that promise themselves great
things on earth will soon be undeceived by their own experience.

`V.` The glorious testimony which God the Father gave to our Lord Jesus,
in which he received from him honour and glory (2 Pt. 1:17), when there
came this voice from the excellent glory. This was like proclaiming the
titles of honour or the royal style of a prince, when, at his
coronation, he appears in his robes of state; and be it known, to the
comfort of mankind, the royal style of Christ is taken from his
mediation. Thus, in vision, he appeared with a rainbow, the seal of the
covenant, about his throne (Rev. 4:3); for it is his glory to be our
Redeemer.

Now concerning this testimony from heaven to Christ, observe.

`1.` How it came, and in what manner it was introduced.

`(1.)` There was a cloud. We find often in the Old Testament, that a cloud
was the visible token of God\'s presence; he came down upon mount Sinai
in a cloud (Ex. 19:9), and so to Moses, Ex. 34:5; Num. 11:25. He took
possession of the tabernacle in a cloud, and afterwards of the temple;
where Christ was in his glory, the temple was, and there God showed
himself present. We know not the balancing of the clouds, but we know
that much of the intercourse and communication between heaven and earth
is maintained by them. By the clouds vapours ascend, and rains descend;
therefore God is said to make the clouds his chariots; so he did here
when he descended upon this mount.

`(2.)` It was a bright cloud. Under the law it was commonly a thick and
dark cloud that God made the token of his presence; he came down upon
mount Sinai in a thick cloud (Ex. 19:16), and said he would dwell in
thick darkness; see 1 Ki. 8:12. But we are now come, not to the mount
that was covered with thick blackness and darkness (Heb. 12:18), but to
the mount that is crowned with a bright cloud. Both the Old-Testament
and the New-Testament dispensation had tokens of God\'s presence; but
that was a dispensation of darkness, and terror, and bondage, this of
light, love, and liberty.

`(3.)` It overshadowed them. This cloud was intended to break the force of
that great light which otherwise would have overcome the disciples, and
have been intolerable; it was like the veil which Moses put upon his
face when it shone. God, in manifesting himself to his people, considers
their frame. This cloud was to their eyes as parables to their
understandings, to convey spiritual things by things sensible, as they
were able to bear them.

`(4.)` There came a voice out of the cloud, and it was the voice of God,
who now, as of old, spake in the cloudy pillar, Ps. 99:7. Here was no
thunder, or lightning, or voice of a trumpet, as there was when the law
was given by Moses, but only a voice, a still small voice, and that not
ushered in with a strong wind, or an earthquake, or fire, as when God
spake to Elias, 1 Ki. 19:11, 12. Moses then and Elias were witnesses,
that in these last days God hath spoken to us by his Son, in another way
than he spoke formerly to them. This voice came from the excellent glory
(2 Pt. 1:17), the glory which excelleth, in comparison of which the
former had no glory; though the excellent glory was clouded, yet thence
came a voice, for faith comes by hearing.

`2.` What this testimony from heaven was; This is my beloved Son, hear ye
him. Here we have,

`(1.)` The great gospel mystery revealed; This is my beloved Son, in whom
I am well pleased. This was the very same that was spoken from heaven at
his baptism (ch. 3:17); and it was the best news that ever came from
heaven to earth since man sinned. It is to the same purport with that
great doctrine (2 Co. 5:19), That God was in Christ, reconciling the
world unto himself. Moses and Elias were great men, and favourites of
Heaven, yet they were but servants, and servants that God was not always
well pleased in; for Moses spoke unadvisedly, and Elias was a man
subject to passions; but Christ is a Son, and in him God was always well
pleased. Moses and Elias were sometimes instruments of reconciliation
between God and Israel; Moses was a great intercessor, and Elias a great
reformer; but in Christ God is reconciling the world; his intercession
is more prevalent than that of Moses, and his reformation more effectual
than that of Elias.

This repetition of the same voice that came from heaven at his baptism
was no vain repetition; but, like the doubling of Pharaoh\'s dream, was
to show the thing was established. What God hath thus spoken once, yea
twice, no doubt he will stand to, and he expects we should take notice
of it. It was spoken at his baptism, because then he was entering upon
his temptation, and his public ministry; and now it was repeated,
because he was entering upon his sufferings, which are to be dated from
hence; for now, and not before, he began to foretel them, and
immediately after his transfiguration it is said (Lu. 9:51), that the
time was come that he should be received up; this therefore was then
repeated, to arm him against the terror, and his disciples against the
offence, of the cross. When sufferings begin to abound, consolations are
given in more abundantly, 2 Co. 1:5.

`(2.)` The great gospel duty required, and it is the condition of our
benefit by Christ; Hear ye him. God is well pleased with none in Christ
but those that hear him. It is not enough to give him the hearing (what
will that avail us?) but we must hear him and believe him, as the great
Prophet and Teacher; hear him, and be ruled by him, as the great Prince
and Lawgiver; hear him, and heed him. Whoever would know the mind of
God, must hearken to Jesus Christ; for by him God has in these last days
spoken to us. This voice from heaven has made all the sayings of Christ
as authentic as if they had been thus spoken out of a cloud. God does
here, as it were, turn us over to Christ for all the revelations of his
mind; and it refers to that prediction concerning the Prophet God would
raise up like unto Moses (Deu. 18:18); him shall ye hear.

Christ now appeared in glory; and the more we see of Christ\'s glory,
the more cause we shall see to hearken to him: but the disciples were
gazing on that glory of his which they saw; they are therefore bid not
to look at him, but to hear him. Their sight of his glory was soon
intercepted by the cloud, but their business was to hear him. We walk by
faith, which comes by hearing, not by sight, 2 Co. 5:7.

Moses and Elias were now with him; the law and the prophets; hitherto it
was said, Hear them, Lu. 16:29. The disciples were ready to equal them
with Christ, when they must have tabernacles for them as well as for
him. They had been talking with Christ, and probably the disciples were
very desirous to know what they said, and to hear something more from
them; No, saith God, hear him, and that is enough; him, and not Moses
and Elias, who were present, and whose silence gave consent to this
voice; they had nothing to say to the contrary; whatever interest they
had in the world as prophets, they were willing to see it all
transferred to Christ, that in all things he might have the
pre-eminence. Be not troubled that Moses and Elias make so short a stay
with you; hear Christ, and you will not want them.

`IV.` The fright which the disciples were put into by this voice, and the
encouragement Christ gave them.

`1.` The disciples fell on their faces, and were sore afraid. The
greatness of the light, and the surprise of it, might have a natural
influence upon them, to dispirit them. But that was not all, ever since
man sinned, and heard God\'s voice in the garden, extraordinary
appearances of God have ever been terrible to man, who, knowing he has
no reason to expect any good, has been afraid to hear any thing
immediately from God. Note, even then when fair weather comes out of the
secret place, yet with God is terrible majesty, Job 37:22. See what
dreadful work the voice of the Lord makes, Ps. 29:4. It is well for us
that God speaks to us by men like ourselves, whose terror shall not make
us afraid.

`2.` Christ graciously raised them up with abundance of tenderness. Note,
The glories and advancements of our Lord Jesus do not at all lessen his
regard to, and concern for, his people that are compassed about with
infirmity. It is comfortable to think, that now, in his exalted state,
he has a compassion for, and condescends to, the meanest true believer.
Observe here, `(1.)`. What he did; he came, and touched them. His
approaches banished their fears; and when they apprehended that they
were apprehended of Christ, there needed no more to make them easy.
Christ laid his right hand upon John is a like case, and upon Daniel,
Rev. 1:17; Dan. 8:18; 10:18. Christ\'s touches were often healing, and
here they were strengthening and comforting. `(2.)` What he said; Arise,
and be not afraid. Note, Though a fear of reverence in our converse with
Heaven is pleasing to Christ, yet a fear of amazement is not so, but
must be striven against. Christ said, Arise. Note, It is Christ by his
word, and the power of his grace going along with it, that raises up
good men from their dejections, and silences their fears; and none but
Christ can do it; Arise, be not afraid. Note, causeless fears would soon
vanish, if we would not yield to them, and lie down under them, but get
up, and do what we can against them. considering what they had seen and
heard, they had more reason to rejoice than to fear, and yet, it seems,
they needed this caution. Note, Through the infirmity of the flesh, we
often frighten ourselves with that wherewith we should encourage
ourselves. Observe, After they had an express command from heaven to
hear Christ, the first word they had from him was, Be not afraid, hear
that. Note, Christ\'s errand into the world was to give comfort to good
people, that, being delivered out of the hands of their enemies, they
might serve God without fear, Lu. 1:74, 75.

`VII.` The disappearing of the vision (v. 8); They lift up themselves,
and then lift up their eyes, and saw no man, save Jesus only. Moses and
Elias were gone, the rays of Christ\'s glory were laid aside, or veiled
again. They hoped this had been the day of Christ\'s entrance into his
kingdom, and his public appearance in that external splendour which they
dreamed of; but see how they are disappointed. Note, It is not wisdom to
raise our expectations high in this world, for the most valuable of our
glories and joys here are vanishing, even those of near communion with
God are so, not a continual feast, but a running banquet. If sometimes
we are favoured with special manifestations of divine grace, glimpses
and pledges of future glory, yet they are withdrawn presently; two
heavens are too much for those to expect that never deserve one. Now
they saw no man, save Jesus only. Note, Christ will tarry with us when
Moses and Elias are gone. The prophets do not live for ever (Zec. 1:5),
and we see the period of our ministers\' conversation; but Jesus Christ
is the same yesterday, to-day, and forever, Heb. 13:7, 8.

`VIII.` The discourse between Christ and his disciples as they came down
from the mountain, v. 9-13.

Observe, 1. They came down from the mountain. Note, We must come down
from the holy mountains, where we have communion with God, and
complacency in that communion, and of which we are saying. It is good to
be here; even there we have no continuing city. Blessed be God, there is
a mountain of glory and joy before us, whence we shall never come down.
But observe, When the disciples came down, Jesus came with them. Note,
When we return to the world again after an ordinance, it must be our
care to take Christ with us, and then it may be our comfort that he is
with us.

`2.` As they came down, they talked of Christ. Note, When we are
returning from holy ordinance, it is good to entertain ourselves and one
another with discourse suitable to the work we have been about. That
communication which is good to the use of edifying is then in a special
manner seasonable; as, on the contrary, that which is corrupt, is worse
then than at another time.

Here is, `(1.)` The charge that Christ gave the disciples to keep the
vision very private for the present (v. 9); Tell it to no man till the
Son of man is risen. If they had proclaimed it, the credibility of it
would have been shocked by his sufferings, which were now hastening on.
But let the publication of it be adjourned till after his resurrection,
and then that and his subsequent glory will be a great confirmation of
it. Note, Christ observed a method in the manifestation of himself; he
would have his works put together, mutually to explain and illustrate
each other, that they might appear in their full strength and convincing
evidence. Everything is beautiful in its season. Christ\'s resurrection
was properly the beginning of the gospel state and kingdom, to which all
before was but preparatory and by way of preface; and therefore, though
this was transacted before, it must not be produced as evidence till
then (and then it appears to have been much insisted on by 2 Pt.
1:16-18), when the religion it was designed for the confirmation of was
brought to its full consistence and maturity. Christ\'s time is the best
and fittest for the manifesting of himself and must be attended to by
us.

`(2.)` An objection which the disciples made against something Christ had
said (v. 10); \"Why then say the scribes that Elias must first come? If
Elias make so short a stay, and is gone so suddenly, and we must say
nothing of him; why have we been taught out of the law to expect his
public appearance in the world immediately before the setting up of the
Messiah\'s kingdom? Must the coming of Elias be a secret, which
everybody looks for?\" or thus; \"If the resurrection of the Messiah,
and with it the beginning of his kingdom, be at hand, what becomes of
that glorious preface and introduction to it, which we expect in the
coming of Elias?\" The scribes, who were the public expositors of the
law, said this according to the scripture (Mal. 4:5); Behold I send you
Elijah the prophet. The disciples spoke the common language of the Jews,
who made that the saying of the scribes which was the saying of the
scripture, whereas of that which ministers speak to us according to the
word of God, we should say, \"God speaks to us, not the ministers;\" for
we must not receive it as the word of men, 1 Th. 2:13. Observe, When the
disciples could not reconcile what Christ said with what they had heard
out of the Old Testament, they desired him to explain it to them. Note,
When we are puzzled with scripture difficulties, we must apply ourselves
to Christ by prayer for his Spirit to open our understandings and to
lead us into all truth.

`(3.)` The solving of this objection. Ask, and it shall be given, ask
instruction, and it shall be given.

`[1.]` Christ allows the prediction (v. 11); \"Elias truly shall first
come, and restore all things; so far you are in the right.\" Christ did
not come to alter or invalidate any thing foretold in the Old Testament.
Note, Corrupt and mistaken glosses may be sufficiently rejected and
exploded, without diminishing or derogating from the authority or
dignity of the sacred text. New-Testament prophecies are true and good,
and are to be received and improved, though some hot foolish men may
have misinterpreted them and drawn wrong inferences from them. He shall
come, and restore all things; not restore them to their former state
(John Baptist went not about to do that), but he shall accomplish all
things (so it may be read), all things that were written of him, all the
predictions of the coming of Elias. John Baptist came to restore things
spiritually, to revive the decays of religion, to turn the hearts of the
fathers to the children; which means the same with this, he shall
restore all things. John preached repentance, and that restores all
things.

`[2.]` He asserts the accomplishment. The scribes say true, that Elias
is come, v. 12. Note, God\'s promises are often fulfilled, and men
perceive it not, but enquire, Where is the promise? when it is already
performed. Elias is come, and they knew him not; they knew him not to be
the Elias promised, the forerunner of the Messiah. The scribes busied
themselves in criticizing upon the scripture, but understood not by the
signs of the times the fulfilling of the scripture. Note, It is easier
to explain the word of God than to apply it and make a right use of it.
But it is no wonder that the morning star was not observed, when he who
is the Sun itself, was in the world, and the world knew him not.

Because they knew him not, they have done to him whatsoever they listed;
if they had known, they would not have crucified Christ, or beheaded
John, 1 Co. 2:8. They ridiculed John, persecuted him, and at last put
him to death; which was Herod\'s doing, but is here charged upon the
whole generation of unbelieving Jews, and particularly the scribes, who,
though they could not prosecute John themselves, were pleased with what
Herod did. He adds, Likewise also shall the Son of man suffer of them.
Marvel not that Elias should be abused and killed by those who
pretended, with a great deal of reverence, to expect him, when the
Messiah himself will be in like manner treated. Note, The sufferings of
Christ took off the strangeness of all other sufferings (Jn. 15:18);
when they had imbrued their hands in the blood of John Baptist, they
were ready to do the like to Christ. Note, As men deal with Christ\'s
servants, so they would deal with him himself; and they that are drunk
with the blood of the martyrs still cry, Give, give, Acts 12:1-3.

`(4.)` The disciples\' satisfaction in Christ\'s reply to their objection
(v. 13); They understood that he spake unto them of John the Baptist. He
did not name John, but gives them such a description of him as would put
them in mind of what he had said to them formerly concerning him; This
is Elias. This is a profitable way of teaching; it engages the
learners\' own thoughts, and makes them, if not their own teachers, yet
their own remembrancers; and thus knowledge becomes easy to him that
understands. When we diligently use the means of knowledge, how
strangely are mists scattered and mistakes rectified!

### Verses 14-21

We have here the miraculous cure of a child that was lunatic and vexed
with a devil. Observe,

`I.` A melancholy representation of the case of this child, made to Christ
by the afflicted father. This was immediately upon his coming down from
the mountain where he was transfigured. Note, Christ\'s glories do not
make him unmindful of us and of our wants and miseries. Christ, when he
came down from the mount, where had conversation with Moses and Elias,
did not take state upon him, but was as easy of access, as ready to poor
beggars, and as familiar with the multitude, as ever he used to be. This
poor man\'s address was very importunate; he came kneeling to Christ.
Note, Sense of misery will bring people to their knees. Those who see
their need of Christ will be earnest, will be in good earnest, in their
applications to him; and he delights to be thus wrestled with.

Two things the father of the child complains of.

`1.` The distress of his child (v. 15); Lord have mercy on my son. The
affliction of the children cannot but affect the tender parents, for
they are pieces of themselves. And the case of afflicted children should
be presented to God by faithful and fervent prayer. This child\'s
distemper, probably, disabled him to pray for himself. Note, Parents are
doubly concerned to pray for their children, not only that are weak and
cannot, but much more that are wicked and will not, pray for themselves.
Now, `(1.)`. The nature of this child\'s disease was very sad; He was
lunatic and sore vexed. A lunatic is properly one whose distemper lies
in the brain, and returns with the change of the moon. The devil, by the
divine permission, either caused this distemper, or at least concurred
with it, to heighten and aggravate it. The child had the
falling-sickness, and the hand of Satan was in it; by it he tormented
then, and made it much more grievous than ordinarily it is. Those whom
Satan got possession of, he afflicted by those diseases of the body
which do most affect the mind; for it is the soul that he aims to do
mischief to. The father, in his complaint, saith, He is lunatic, taking
notice of the effect; but Christ, in the cure, rebuked the devil, and so
struck at the cause. Thus he doth in spiritual cures. `(2.)` The effects
of the disease were very deplorable; He oft falls into the fire, and
into the water. If the force of the disease made him to fall, the malice
of the devil made him to fall into the fire or water; so mischievous is
he where he gains possession and power in any soul. He seeks to devour,
1 Pt. 5:8.

`2.` The disappointment of his expectation from the disciples (v. 16); I
brought him to thy disciples, and they could not cured him. Christ gave
his disciples power to cast out devils (ch. 10:1, 8), and therein they
were successful (Lu. 10:17); yet at this time they failed in the
operation, though there were nine of them together, and before a great
multitude. Christ permitted this, `(1.)` To keep them humble, and to show
their dependence upon him, that without him they could do nothing. `(2.)`
To glorify himself and his own power. It is for the honour of Christ to
come in with help at a dead-lift, when other helpers cannot help.
Elisha\'s staff in Gehazi\'s hand will not raise the child: he must come
himself. Note, There are some special favours which Christ reserves the
bestowment of to himself; and sometimes he keeps the cistern empty; that
he may bring us to himself, the Fountain. But the failures of
instruments shall not hinder the operations of his grace, which will
work, if not by them, yet without them.

`II.` The rebukes that Christ gave to the people first, and then to the
devil.

`1.` He chid those about him (v. 17); O faithless and perverse
generation! This is not spoken to the disciples, but to the people, and
perhaps especially to the scribes, who are mentioned in Mk. 9:14, and
who, as it should seem, insulted over the disciples, because they had
now met with a case that was too hard for them. Christ himself could not
do many mighty works among a people in whom unbelief reigned. It was
here owing to the faithlessness of this generation, that they could not
obtain those blessings from God, which otherwise they might have had; as
it was owing to the weakness of the disciples\' faith, that they could
not do those works for God, which otherwise they might have done. They
were faithless and perverse. Note, Those that are faithless will be
perverse; and perverseness is sin in its worst colours. Faith is
compliance with God, unbelief is opposition and contradiction to God.
Israel of old was perverse, because faithless (Ps. 95:9), forward, for
in them is no faith, Deu. 32:20.

Two things he upbraids them with. `(1.)` His presence with them so long;
\"How long shall I be with you? Will you always need my bodily presence,
and never come to such maturity as to be fit to be left, the people to
the conduct of the disciples, and the disciples to the conduct of the
Spirit and of their commission? Must the child be always carried, and
will it never learn to go alone?\" `(2.)` His patience with them so long;
How long shall I suffer you? Note, `[1.]` The faithlessness and
perverseness of those who enjoy the means of grace are a great grief to
the Lord Jesus. Thus did he suffer the manners of Israel of old, Acts
13:18. `[2.]` The longer Christ has borne with a perverse and faithless
people, the more he is displeased with their perverseness and unbelief;
and he is God, and not man, else he would not suffer so long, nor bear
so much, as he doth.

`2.` He cured the child, and set him to-rights again. He called, Bring
him hither to me. Though the people were perverse, and Christ was
provoked, yet care was taken of the child. Note, Though Christ may be
angry, he is never unkind, nor doth he, in the greatest of his
displeasure, shut up the bowels of his compassion from the miserable;
Bring him to me. Note, When all other helps and succours fail, we are
welcome to Christ, and may be confident in him and in his power and
goodness.

See here an emblem of Christ\'s undertaking as our Redeemer.

`(1.)` He breaks the power of Satan (v. 18); Jesus rebuked the devil, as
one having authority, who could back with force his word of command.
Note, Christ\'s victories over Satan are obtained by the power of his
word, the sword that comes out of his mouth, Rev. 19:21. Satan cannot
stand before the rebukes of Christ, though his possession has been ever
so long. It is comfortable to those who are wrestling with
principalities and powers, that Christ hath spoiled them, Colos. 2:15.
The lion of the tribe of Judah will be too hard for the roaring lion
that seeks to devour.

`(2.)` He redresses the grievances of the children of men; The child was
cured from that very hour. It was an immediate cure, and a perfect one.
This is an encouragement to parents to bring their children to Christ,
whose souls are under Satan\'s power; he is able to heal them, and as
willing as he is able. Not only bring them to Christ by prayer, but
bring them to the word of Christ, the ordinary means by which Satan\'s
strongholds are demolished in the soul. Christ\'s rebukes, brought home
to the heart, will ruin Satan\'s power there.

`III.` Christ\'s discourse with his disciples hereupon.

`1.` They ask the reason why they could not cast out the devil at this
time (v. 19); They came to Jesus apart. Note, Ministers, who are to deal
for Christ in public, have need to keep up a private communion with him,
that they may in secret, where no eye sees, bewail their weakness and
straitness, their follies and infirmities, in their public performances,
and enquire into the cause of them. We should make use of the liberty of
access we have to Jesus apart, where we may be free and particular with
him. Such questions as the disciples put to Christ, we should put to
ourselves, in communing with our own hearts upon our beds; Why were we
so dull and careless at such a time? Why came we so much short in such a
duty? That which is amiss may, when found out, be amended.

`2.` Christ gives them two reasons why they failed.

`(1.)` It was because of their unbelief, v. 20. When he spake to the
father of the child and to the people, he charged it upon their
unbelief; when he spake to his disciples, he charged it upon theirs; for
the truth was, there were faults on both sides; but we are more
concerned to hear of our own faults than of other people\'s, and to
impute what is amiss to ourselves than to others. When the preaching of
the word seems not to be so successful as sometimes it has been, the
people are apt to lay all the fault upon the ministers, and the
ministers upon the people; whereas, it is more becoming for each to own
his own faultiness, and to say, \"It is owing to me.\" Ministers, in
reproving, must learn thus to give to each his portion of the word; and
to take people off from judging others, by teaching all to judge
themselves; It is because of your unbelief. Though they had faith, yet
that faith was weak and ineffectual. Note, `[1.]` As far as faith falls
short of its due strength, vigour, and activity, it may truly be said,
\"There is unbelief.\" Many are chargeable with unbelief, who yet are
not to be called unbelievers. `[2.]` It is because of our unbelief, that
we bring so little to pass in religion, and so often miscarry, and come
short, in that which is good.

Our Lord Jesus takes this occasion to show them the power of faith, that
they might not be defective in that, another time, as they were now; If
ye have faith as a grain of mustard-seed, ye shall do wonders, v. 20.
Some make the comparison to refer to the quality of the mustard-seed,
which is, when bruised, sharp and penetrating; \"If you have an active
growing faith, not dead, flat, or insipid, you will not be baffled
thus.\" But it rather refers to the quantity; \"If you had but a grain
of true faith, though so little that it were like that which is the
least of all seeds, you would do wonders.\" Faith in general is a firm
assent to, a compliance with, and a confidence in, all divine
revelation. The faith here required, is that which had for its object
that particular revelation by which Christ gave his disciples power to
work miracles in his name, for the confirmation of the doctrine they
preached. It was a faith in this revelation that they were defective in;
either doubting the validity of their commission, or fearing that it
expired with their first mission, and was not to continue when they were
returning to their Master; or that it was some way or other forfeited or
withdrawn. Perhaps their Master\'s absence with the three chief of his
disciples, with a charge to the rest not to follow them, might occasion
some doubts concerning their power, or rather the power of the Lord with
them, to do this; however, there were not, at present, such a strong
actual dependence upon, and confidence in, the promise of Christ\'s
presence with them, as there should have been. It is good for us to be
diffident of ourselves and of our own strength; but it is displeasing to
Christ, when we distrust any power derived from him or granted by him.

If ye have ever so little of this faith in sincerity, if ye truly rely
upon the powers committed to you, ye shall say to this mountain, Remove.
This is a proverbial expression, denoting that which follows, and no
more, Nothing shall be impossible to you. They had a full commission,
among other things, to cast out devils without exception; but, this
devil being more than ordinarily malicious and inveterate, they
distrusted the power they had received, and so failed. To convince them
of this, Christ shows them what they might have done. Note, An active
faith can remove mountains, not of itself, but in the virtue of a divine
power engaged by a divine promise, both which faith fastens upon.

`(2.)` Because there was something in the kind of the malady, which
rendered the cure more than ordinarily difficult (v. 21); \"This kind
goes not out but by prayer and fasting. This possession, which works by
a falling-sickness, or this kind of devils that are thus furious, is not
cast out ordinarily but by great acts of devotion, and wherein ye were
defective.\" Note, `[1.]` Though the adversaries we wrestle, be all
principalities and powers, yet some are stronger than others, and their
power more hardly broken. `[2.]` The extraordinary power of Satan must
not discourage our faith, but quicken us to a greater intenseness in the
acting of it, and more earnestness in praying to God for the increase of
it; so some understand it here; \"This kind of faith (which removeth
mountains) doth not proceed, is not obtained, from God, nor is it
carried up to its full growth, nor drawn out into act and exercise, but
by earnest prayer.\" `[3.]` Fasting and prayer are proper means for the
bringing down of Satan\'s power against us, and the fetching in of
divine power to our assistance. Fasting is of use to put an edge upon
prayer; it is an evidence and instance of humiliation which is necessary
in prayer, and is a means of mortifying some corrupt habits, and of
disposing the body to serve the soul in prayer. When the devil\'s
interest in the soul is confirmed by the temper and constitution of the
body, fasting must be joined with prayer, to keep under the body.

### Verses 22-23

Christ here foretels his own sufferings; he began to do it before (ch.
16:21); and, finding that it was to his disciples a hard saying, he saw
it necessary to repeat it. There are some things which God speaketh
once, yea twice, and yet man perceiveth it not. Observe here,

`1.` What he foretold concerning himself-that he should be betrayed and
killed. He perfectly knew, before, all things that should come to him,
and yet undertook the work of our redemption, which greatly commends his
love; nay, his clear foresight of them was a kind of ante-passion, had
not his love to man made all easy to him.

`(1.)` He tells them that he should be betrayed into the hands of men. He
shall be delivered up (so it might be read and understood of his
Father\'s delivering him up by his determined counsel and
fore-knowledge, Acts 2:23; Rom. 8:32); but as we render it, it refers to
Judas\'s betraying him into the hands of the priests, and their
betraying him into the hands of the Romans. He was betrayed into the
hands of men; men to whom he was allied by nature, and from whom
therefore he might expect pity and tenderness; men whom he had
undertaken to save, and from whom therefore he might expect honour and
gratitude; yet these are his persecutors and murderers.

`(2.)` That they should kill him; nothing less than that would satisfy
their rage; it was his blood, his precious blood, that they thirsted
after. This is the heir, come, let us kill him. Nothing less would
satisfy God\'s justice, and answer his undertaking; if he be a Sacrifice
of atonement, he must be killed; without blood no remission.

`(3.)` That he shall be raised again the third day. Still, when he spoke
of his death, he gave a hint of his resurrection, the joy set before
him, in the prospect of which he endured the cross, and despised the
shame. This was an encouragement, not only to him, but to his disciples;
for if he rise the third day, his absence from them will not be long,
and his return to them will be glorious.

`2.` How the disciples received this; They were exceedingly sorry. Herein
appeared their love to their Master\'s person, but with all their
ignorance and mistake concerning his undertaking. Peter indeed durst not
say anything against it, as he had done before (ch. 16:22), having then
been severely chidden for it; but he, and the rest of them, greatly
lamented it, as it would be their own loss, their Master\'s grief, and
the sin and ruin of them that did it.

### Verses 24-27

We have here an account of Christ\'s paying tribute.

`I.` Observe how it was demanded, v. 24. Christ was now at Capernaum, his
headquarters, where he mostly resided; he did not keep from thence, to
decline being called upon for his dues, but rather came thither, to be
ready to pay them.

`1.` The tribute demanded was not any civil payment to the Roman powers,
that was strictly exacted by the publicans, but the church-duties, the
half shekel, about fifteen pence, which were required from every person
or the service of the temple, and the defraying of the expenses of the
worship there; it is called a ransom for the soul, Ex. 30:12, etc. This
was not so strictly exacted now as sometimes it had been, especially not
in Galilee.

`2.` The demand was very modest; the collectors stood in such awe of
Christ, because of his mighty works, that they durst not speak to him
about it, but applied themselves to Peter, whose house was in Capernaum,
and probably in his house Christ lodged; he therefore was fittest to be
spoken to as the housekeeper, and they presumed he knew his Master\'s
mind. Their question is, Doth not your master pay tribute? Some think
that they sought an occasion against him, designing, if he refused, to
represent him as disaffected to the temple-service, and his followers as
lawless people, that would pay neither toll, tribute, nor custom, Ezra
4:13. It should rather seem, they asked this with respect, intimating,
that if he had any privilege to exempt him from this payment, they would
not insist upon it.

Peter presently his word for his Master; \"Yes, certainly; my Master
pays tribute; it is his principle and practice; you need not fear moving
it to him.\" `(1.)` He was made under the law (Gal. 4:4); therefore under
this law he was paid for at forty days old (Lu. 2:22), and now he paid
for himself, as one who, in his state of humiliation, had taken upon him
the form of a servant, Phil. 2:7, 8. `(2.)` He was made sin for us, and
was sent forth in the likeness of sinful flesh, Rom. 8:3. Now this tax
paid to the temple is called an atonement for the soul, Ex. 30:15.
Christ, that in every thing he might appear in the likeness of sinners,
paid it though he had no sin to atone for. `(3.)` Thus it became him to
fulfil all righteousness, ch. 3:15. He did this to set an example,
`[1.]` Of rendering to all their due, tribute to whom tribute is due,
Rom. 13:7. The kingdom of Christ not being of this world, the favourites
and officers of it are so far from having a power granted them, as such,
to tax other people\'s purses, that theirs are made liable to the powers
that are. `[2.]` Of contributing to the support of the public worship of
God in the places where we are. If we reap spiritual things, it is fit
that we should return carnal things. The temple was now made a den of
thieves, and the temple-worship a pretence for the opposition which the
chief priests gave to Christ and his doctrine; and yet Christ paid this
tribute. Note, Church-duties, legally imposed, are to be paid,
notwithstanding church-corruptions. We must take care not to use our
liberty as a cloak of covetousness or maliciousness, 1 Pt. 2:16. If
Christ pay tribute, who can pretend an exemption?

`II.` How it was disputed (v. 25), not with the collectors themselves,
lest they should be irritated, but with Peter, that he might be
satisfied in the reason why Christ paid tribute, and might not mistake
about it. He brought the collectors into the house; but Christ
anticipated him, to give him a proof of his omniscience, and that no
thought can be withholden from him. The disciples of Christ are never
attacked without his knowledge.

Now, 1. He appeals to the way of the kings of the earth, which is, to
take tribute of strangers, of the subjects of their kingdom, or
foreigners that deal with them, but not of their own children that are
of their families; there is such a community of goods between parents
and children, and a joint-interest in what they have, that it would be
absurd for the parents to levy taxes upon the children, or demand
anything from them; it is like one hand taxing the other.

`2.` He applies this to himself; Then are the children free. Christ is
the Son of God, and Heir of all things; the temple is his temple (Mal.
3:1), his Father\'s house (Jn. 2:16), in it he is faithful as a Son in
his own house (Heb. 3:6), and therefore not obliged to pay this tax for
the service of the temple. Thus Christ asserts his right, lest his
paying this tribute should be misimproved to the weakening of his title
as the Son of God, and the King of Israel, and should have looked like a
disowning of it himself. These immunities of the children are to be
extended no further than our Lord Jesus himself. God\'s children are
freed by grace and adoption from the slavery of sin and Satan, but not
from their subjection to civil magistrates in civil things; here the law
of Christ is express; Let every soul (sanctified souls not excepted) be
subject to the higher powers. Render to Caesar the things that are
Caesar\'s.

`III.` How it was paid, notwithstanding, v. 27.

`1.` For what reason Christ waived his privilege, and paid this tribute,
though he was entitled to an exemption-Lest we should offend them. Few
knew, as Peter did, that he was the Son of God; and it would have been a
diminution to the honour of that great truth, which was yet a secret, to
advance it now, to serve such a purpose as this. Therefore Christ drops
that argument, and considers, that if he should refuse this payment, it
would increase people\'s prejudice against him and his doctrine, and
alienate their affections from him, and therefore he resolves to pay it.
Note, Christian prudence and humility teach us, in many cases, to recede
from our right, rather than give offence by insisting upon it. We must
never decline our duty for fear of giving offence (Christ\'s preaching
and miracles offended them, yet he went on with him, ch. 15:12, 13,
better offend men than God); but we must sometimes deny ourselves in
that which is our secular interest, rather than give offence; as Paul, 1
Co. 8:13; Rom. 14:13.

`2.` What course he took for the payment of this tax; he furnished
himself with money for it out of the mouth of a fish (v. 27), wherein
appears,

`(1.)` The poverty of Christ; he had not fifteen pence at command to pay
his tax with, though he cured so many that were diseased; it seems, he
did all gratis; for our sakes he became poor, 2 Co. 8:9. In his ordinary
expenses, he lived upon alms (Lu. 8:3), and in extraordinary ones, he
lived upon miracles. He did not order Judas to pay this out of the bag
which he carried; that was for subsistence, and he would not order that
for his particular use, which was intended for the benefit of the
community.

`(2.)` The power of Christ, in fetching money out of a fish\'s mouth for
this purpose. Whether his omnipotence put it there, or his omniscience
knew that it was there, it comes all to one; it was an evidence of his
divinity, and that he is Lord of hosts. Those creatures that are most
remote from man are at the command of Christ, even the fishes of the sea
are under his feet (Ps. 8:5); and to evidence his dominion in this lower
world, and to accommodate himself to his present state of humiliation,
he chose to take it out of a fish\'s mouth, when he could have taken it
out of an angel\'s hand. Now observe,

`[1.]` Peter must catch the fish by angling. Even in miracles he would
use means to encourage industry and endeavour. Peter has something to
do, and it is in the way of his own calling too; to teach us diligence
in the employment we are called to, and called in. Do we expect that
Christ should give to us? Let us be ready to work for him.

`[2.]` The fish came up, with money in the mouth of it, which represents
to us the reward of obedience in obedience. What work we do at Christ\'s
command brings its own pay along with it: In keeping God\'s commands, as
well as after keeping them, there is great reward, Ps. 19:11. Peter was
made a fisher of men, and those that he caught thus, came up; where the
heart is opened to entertain Christ\'s word, the hand is open to
encourage his ministers.

`[3.]` The piece of money was just enough to pay the tax for Christ and
Peter. Thou shalt find a stater, the value of a Jewish shekel, which
would pay the poll-tax for two, for it was half a shekel, Ex. 30:13.
Christ could as easily have commanded a bag of money as a piece of
money; but he would teach us not to covet superfluities, but, having
enough for our present occasions, therewith to be content, and not to
distrust God, though we live but from hand to mouth. Christ made the
fish his cash-keeper; and why may not we make God\'s providence our
storehouse and treasury? If we have a competency for today, let
to-morrow take thought for the things of itself. Christ paid for himself
and Peter, because it is probable that here he only was assessed, and of
him it was at this time demanded; perhaps the rest had paid already, or
were to pay elsewhere. The papists make a great mystery of Christ\'s
paying for Peter, as if this made him the head and representative of the
whole church; whereas the payment of tribute for him was rather a sign
of subjection than of superiority. His pretended successors pay no
tribute, but exact it. Peter fished for this money, and therefore part
of it went for his use. Those that are workers together with Christ in
winning souls shall shine with him. Give it for thee and me. What Christ
paid for himself was looked upon as a debt; what he paid for Peter was a
courtesy to him. Note, it is a desirable thing, if God so please, to
have wherewithal of this world\'s goods, not only to be just, but to be
kind; not only to be charitable to the poor, but obliging to our
friends. What is a great estate good for, but that it enables a man to
do so much the more good?

Lastly, Observe, The evangelist records here the orders Christ gave to
Peter, the warrant; the effect is not particularly mentioned, but taken
for granted, and justly; for, with Christ, saying and doing are the same
thing.
