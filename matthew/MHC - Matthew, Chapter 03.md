Matthew, Chapter 3
==================

Commentary
----------

At the start of this chapter, concerning the baptism of John, begins the
gospel (Mk. 1:1); what went before is but preface or introduction; this
is \"the beginning of the gospel of Jesus Christ.\" And Peter observes
the same date, Acts 1:22, beginning from the baptism of John, for then
Christ began first to appear in him, and then to appear to him, and by
him to the world. Here is, `I.` The glorious rising of the
morning-star-John the Baptist (v. 1). 1. The doctrine he preached (v.
2). 2. The fulfilling of the scripture in him (v. 3). 3. His manner of
life (v. 4). 4. The resort of multitudes to him, and their submission to
his baptism (v. 5, 6). 5. His sermon that he preached to the Pharisees
and Sadducees, wherein he endeavours to bring them to repentance (v.
7-10), and so to bring them to Christ (v. 11, 12). `II.` The more glorious
shining forth of the Sun of righteousness, immediately after: where we
have, 1. The honour done by him to the baptism of John (v. 13-15). 2.
The honour done to him by the descent of the Spirit upon him, and a
voice from heaven (v. 16, 17).

### Verses 1-6

We have here an account of the preaching and baptism of John, which were
the dawning of the gospel-day. Observe,

`I.` The time when he appeared. In those days (v. 1), or, after those
days, long after what was recorded in the foregoing chapter, which left
the child Jesus in his infancy. In those days, in the time appointed of
the Father for the beginning of the gospel, when the fulness of time was
come, which was often thus spoken of in the Old Testament, In those
days. Now the last of Daniel\'s weeks began, or rather, the latter half
of the week, when the Messiah was to confirm the covenant with many,
Dan. 9:27. Christ\'s appearances are all in their season. Glorious
things were spoken both of John and Jesus, at and before their births,
which would have given occasion to expect some extraordinary appearances
of a divine presence and power with them when they were very young; but
it is quite otherwise. Except Christ\'s disputing with the doctors at
twelve years old, nothing appears remarkable concerning either of them,
till they were about thirty years old. Nothing is recorded of their
childhood and youth, but the greatest part of their life is tempus,
adeµlon-wrapt up in darkness and obscurity: these children differ little
in outward appearance from other children, as the heir, while he is
under age, differs nothing from a servant, though he be lord of all. And
this was to show, 1. That even when God is acting as the God of Israel,
the Saviour, yet verily he is a God that hideth himself (Isa. 45:15).
The Lord is in this place and I knew it not, Gen. 28:16. Our beloved
stands behind the wall long before he looks forth at the windows, Cant.
2:9. 2. That our faith must principally have an eye to Christ in his
office and undertaking, for there is the display of his power; but in
his person is the hiding of his power. All this while, Christ was
god-man; yet we are not told what he said or did, till he appeared as a
prophet; and then, Hear ye him. 3. That young men, though well
qualified, should not be forward to put forth themselves in public
service, but be humble, and modest, and self-diffident, swift to hear,
and slow to speak.

Matthew says nothing of the conception and birth of John the Baptist,
which is largely related by St. Luke, but finds him at full age, as if
dropt from the clouds to preach in the wilderness. For above three
hundred years the church had been without prophets; those lights had
been long put out, that he might be the more desired, who was to be the
great prophet. After Malachi there was no prophet, nor any pretender to
prophecy, till John the Baptist, to whom therefore the prophet Malachi
points more directly than any of the Old Testament prophets had done
(Mal. 3:1); I send my messenger.

`II.` The place where he appeared first. In the wilderness of Judea. It
was not an uninhabited desert, but a part of the country not so thickly
peopled, nor so much enclosed into fields and vineyards, as other parts
were; it was such a wilderness as had six cities and their villages in
it, which are named, Jos. 15:61, 62. In these cities and villages John
preached, for thereabouts he had hitherto lived, being born hard by, in
Hebron; the scenes of his action began there, where he had long spent
his time in contemplation; and even when he showed himself to Israel, he
showed how well he loved retirement, as far as would consist with his
business. The word of the Lord found John here in a wilderness. Note, No
place is so remote as to shut us out from the visits of divine grace;
nay, commonly the sweetest intercourse the saints have with Heaven, is
when they are withdrawn furthest from the noise of this world. It was in
this wilderness of Judah that David penned the 63rd Psalm, which speaks
so much of the sweet communion he then had with God, Hos. 2:14. In a
wilderness the law was given; and as the Old Testament, so the New
Testament Israel was first found in the desert land, and there God led
him about and instructed him, Deu. 32:10. John Baptist was a priest of
the order of Aaron, yet we find him preaching in a wilderness, and never
officiating in the temple; but Christ, who was not a son of Aaron, is
yet often found in the temple, and sitting there as one having
authority; so it was foretold, Mal. 3:1. The Lord whom ye seek shall
suddenly come to his temple; not the messenger that was to prepare his
way. This intimated that the priesthood of Christ was to thrust out that
of Aaron, and drive it into a wilderness.

The beginning of the gospel in a wilderness, speaks comfort to the
deserts of the Gentile world. Now must the prophecies be fulfilled, I
will plant in the wilderness the cedar, Isa. 41:18, 19. The wilderness
shall be a fruitful field, Isa. 32:15. And the desert shall rejoice,
Isa. 35:1, 2. The Septuagint reads, the deserts of Jordan, the very
wilderness in which John preached. In the Romish church there are those
who call themselves hermits, and pretend to follow John; but when they
say of Christ, Behold, he is in the desert, go not forth, ch. 24:26.
There was a seducer that led his followers into the wilderness, Acts
21:38.

`III.` His preaching. This he made his business. He came, not fighting,
nor disputing, but preaching (v. 1); for by the foolishness of
preaching, Christ\'s kingdom must be set up.

`1.` The doctrine he preached was that of repentance (v. 2); Repent ye.
He preached this in Judea, among those that were called Jews, and made a
profession of religion; for even they needed repentance. He preached it,
not in Jerusalem, but in the wilderness of Judea, among the plain
country people; for even those who think themselves most out of the way
of temptation, and furthest from the vanities and vices of the town,
cannot wash their hands in innocency, but must do it in repentance. John
Baptist\'s business was to call men to repent of their sins;
Metanoeite-Bethink yourselves; \"Admit a second thought, to correct the
errors of the first-an afterthought. Consider your ways, change your
minds; you have thought amiss; think again, and think aright.\" Note,
True penitents have other thoughts of God and Christ, and sin and
holiness, and this world and the other, than they have had, and stand
otherwise affected toward them. The change of the mind produces a change
of the way. Those who are truly sorry for what they have done amiss,
will be careful to do so no more. This repentance is a necessary duty,
in obedience to the command of God (Acts 17:30); and a necessary
preparative and qualification for the comforts of the gospel of Christ.
If the heart of man had continued upright and unstained, divine
consolations might have been received without this painful operation
preceding; but, being sinful, it must be first pained before it can be
laid at ease, must labour before it can be at rest. The sore must be
searched, or it cannot be cured. I wound and I heal.

`2.` The argument he used to enforce this call was, For the kingdom of
heaven is at hand. The prophets of the Old Testament called people to
repent, for the obtaining and securing of temporal national mercies, and
for the preventing and removing of temporal national judgments: but now,
though the duty pressed is the same, the reason is new, and purely
evangelical. Men are now considered in their personal capacity, and not
so much as then in a social and political one. Now repent, for the
kingdom of heaven is at hand; the gospel dispensation of the covenant of
grace, the opening of the kingdom of heaven to all believers, by the
death and resurrection of Jesus Christ. It is a kingdom of which Christ
is the Sovereign, and we must be the willing, loyal subjects of it. It
is a kingdom of heaven, not of this world, a spiritual kingdom: its
original from heaven, its tendency to heaven. John preached this as at
hand; then it was at the door; to us it is come, by the pouring out of
the Spirit, and the full exhibition of the riches of gospel-grace. Now,
`(1.)` This is a great inducement to us to repent. There is nothing like
the consideration of divine grace to break the heart, both for sin and
from sin. That is evangelical repentance, that flows from a sight of
Christ, from a sense of his love, and the hopes of pardon and
forgiveness through him. Kindness is conquering; abused kindness,
humbling and melting. What a wretch was I to sin against such grace,
against the law and love of such a kingdom! `(2.)` It is a great
encouragement to us to repent; \"Repent, for your sins shall be pardoned
upon your repentance. Return to God in a way of duty, and he will,
through Christ, return to you in a way of mercy.\" The proclamation of
pardon discovers, and fetches in, the malefactor who before fled and
absconded. Thus we are drawn to it with the cords of man, and the bands
of love.

`IV.` The prophecy that was fulfilled in him, v. 3. This is he that was
spoken of in the beginning of that part of the prophecy of Esaias, which
is mostly evangelical, and which points at gospel-times and
gospel-grace; see Isa. 40:3, 4. John is here spoken of,

`1.` As the voice of one crying in the wilderness. John owned it himself
(Jn. 1:23); I am the voice, and that is all, God is the Speaker, who
makes known his mind by John, as a man does by his voice. The word of
God must be received as such (1 Th. 2:13); what else is Paul, and what
is Apollos, but the voice! John is called the voice, phoµneµ
booµntos-the voice of one crying aloud, which is startling and
awakening. Christ is called the Word, which, being distinct and
articulate, is more instructive. John as the voice, roused men, and then
Christ, as the Word, taught them; as we find, Rev. 14:2. The voice of
many waters, and of a great thunder, made way for the melodious voice of
harpers and the new song, v. 3. Some observe that, as Samson\'s mother
must drink no strong drink, yet he was designed to be a strong man; so
John Baptist\'s father was struck dumb, and yet he was designed to be
the voice of one crying. When the crier\'s voice is begotten of a dumb
father, it shows the excellency of the power to be of God, and not of
man.

`2.` As one whose business it was to prepare the way of the Lord, and to
make his paths straight; so it was said of him before he was born, that
he should make ready a people prepared for the Lord (Lu. 1:17), as
Christ\'s harbinger and forerunner: he was such a one as intimated the
nature of Christ\'s kingdom, for he came not in the gaudy dress of a
herald at arms, but in the homely one of a hermit. Officers were sent
before great men to clear the way; so John prepares the way of the Lord.
`(1.)` He himself did so among the men of that generation. In the Jewish
church and nation, at that time, all was out of course; there was a
great decay of piety, the vitals of religion were corrupted and eaten
out by the traditions and injunctions of the elders. The Scribes and
Pharisees, that is, the greatest hypocrites in the world, had the key of
knowledge, and the key of government, at their girdle. The people were,
generally, extremely proud of their privileges, confident of
justification by their own righteousness, insensible of sin; and, though
now under the most humbling providences, being lately made a province of
the Roman Empire, yet they were unhumbled; they were much in the same
temper as they were in Malachi\'s time, insolent and haughty, and ready
to contradict the word of God: now John was sent to level these
mountains, to take down their high opinion of themselves, and to show
them their sins, that the doctrine of Christ might be the more
acceptable and effectual. `(2.)` His doctrine of repentance and
humiliation is still as necessary as it was then to prepare the way of
the Lord. Note, There is a great deal to be done, to make way for Christ
into a soul, to bow the heart for the reception of the Son of David (2
Sa. 19:14); and nothing is more needful, in order to this, than the
discovery of sin, and a conviction of the insufficiency of our own
righteousness. That which lets will let, until it be taken out of the
way; prejudices must be removed, high thoughts brought down, and
captivated to the obedience of Christ. Gates of brass must be broken,
and bars of iron cut asunder, ere the everlasting doors be opened for
the King of glory to come in. The way of sin and Satan is a crooked way;
to prepare a way for Christ, the paths must be made straight, Heb.
12:13.

`V.` The garb in which he appeared, the figure he made, and the manner of
his life, v. 4. They, who expected the Messiah as a temporal prince,
would think that his forerunner must come in great pomp and splendour,
that his equipage should be very magnificent and gay; but it proves
quite contrary; he shall be great in the sight of the Lord, but mean in
the eyes of the world; and, as Christ himself, having no form or
comeliness; to intimate betimes, that the glory of Christ\'s kingdom was
to be spiritual, and the subjects of it such as ordinarily were either
found by it, or made by it, poor and despised, who derived their
honours, pleasures, and riches, from another world.

`1.` His dress was plain. This same John had his raiment of camel\'s
hair, and a leathern girdle about his loins; he did not go in long
clothing, as the scribes, or soft clothing, as the courtiers, but in the
clothing of a country husbandman; for he lived in a country place, and
suited his habit to his habitation. Note, It is good for us to
accommodate ourselves to the place and condition which God, in his
providence, has put us in. John appeared in this dress, `(1.)` To show
that, like Jacob, he was a plain man, and mortified to this world, and
the delights and gaieties of it. Behold an Israelite indeed! Those that
are lowly in heart should show it by a holy negligence and indifference
in their attire; and not make the putting on of apparel their adorning,
nor value others by their attire. `(2.)` To show that he was a prophet,
for prophets wore rough garments, as mortified men (Zec. 13:4); and,
especially, to show that he was the Elias promised; for particular
notice is taken of Elias, that he was a hairy man (which, some think, is
meant of the hairy garments he wore), and that he was girt with a girdle
of leather about his loins, 2 Ki. 1:8. John Baptist appears no way
inferior to him in mortification; this therefore is that Elias that was
to come. `(3.)` To show that he was a man of resolution; his girdle was
not fine, such as were then commonly worn, but it was strong, it was a
leathern girdle; and blessed is that servant, whom his Lord, when he
comes, finds with his loins girt, Lu. 12:35; 1 Pt. 1:13.

`2.` His diet was plain; his meat was locusts and wild honey; not as if
he never ate any thing else; but these he frequently fed upon, and made
many meals of them, when he retired into solitary places, and continued
long there for contemplation. Locusts were a sort of flying insect, very
good for food, and allowed as clean (Lev. 11:22); they required little
dressing, and were light, and easy of digestion, whence it is reckoned
among the infirmities of old age, that the grasshopper, or locust, is
then a burden to the stomach, Eccl. 12:5. Wild honey was that which
Canaan flowed with, 1 Sa. 14:26. Either it was gathered immediately, as
it fell in the dew, or rather, as it was found in the hollows of trees
and rocks, where bees built, that were not, like those in hives, under
the care and inspection of men. This intimates that he ate sparingly, a
little served his turn; a man would be long ere he filled his belly with
locusts and wild honey: John Baptist came neither eating nor drinking
(ch. 11:18)-not with the curiosity, formality, and familiarity that
other people do. He was so entirely taken up with spiritual things, that
he could seldom find time for a set meal. Now, `(1.)` This agreed with the
doctrine he preached of repentance, and fruits meet for repentance.
Note, Those whose business it is to call others to mourn for sin, and to
mortify it, ought themselves to live a serious life, a life of
self-denial, mortification, and contempt of the world. John Baptist thus
showed the deep sense he had of the badness of the time and place he
lived in, which made the preaching of repentance needful; every day was
a fast-day with him. `(2.)` This agreed with his office as Christ\'s
forerunner; by this practice he showed that he knew what the kingdom of
heaven was, and had experienced the powers of it. Note, Those that are
acquainted with divine and spiritual pleasures, cannot but look upon all
the delights and ornaments of sense with a holy indifference; they know
better things. By giving others this example he made way for Christ.
Note, A conviction of the vanity of the world, and everything in it, is
the best preparative for the entertainment of the kingdom of heaven in
the heart. Blessed are the poor in spirit.

`VI.` The people who attended upon him, and flocked after him (v. 5);
Then went out to him Jerusalem, and all Judea. Great multitudes came to
him from the city, and from all parts of the country; some of all sorts,
men and women, young and old, rich and poor, Pharisees and publicans;
they went out to him, as soon as they heard his preaching the kingdom of
heaven, that they might hear what they heard so much of. Now, 1. This
was a great honour put upon John, that so many attended him, and with so
much respect. Note, Frequently those have most real honour done them,
who least court the shadow of it. Those who live a mortified life, who
are humble and self-denying, and dead to the world, command respect; and
men have a secret value and reverence for them, more than they would
imagine. 2. This gave John a great opportunity of doing good, and was an
evidence that God was with him. Now people began to crowd and press into
the kingdom of heaven (Lu. 16:16); and a blessed sight it was, to see
the dew of the youth dropping from the womb of the gospel-morning (Ps.
110:3), to see the net cast where there were so many fish. 3. This was
an evidence, that it was now a time of great expectation; it was
generally thought that the kingdom of God would presently appear (Lu.
19:11), and therefore, when John showed himself to Israel, lived and
preached at this rate, so very different from the Scribes and Pharisees,
they were ready to say of him, that he was the Christ (Lu. 3:15); and
this occasioned such a confluence of people about him. 4. Those who
would have the benefit of John\'s ministry must go out to him in the
wilderness, sharing in his reproach. Note, They who truly desire the
sincere milk of the word, it if be not brought to them, will seek out
for it: and they who would learn the doctrine of repentance must go out
from the hurry of this world, and be still. 5. It appears by the issue,
that of the many who came to John\'s Baptism, there were but few that
adhered to it; witness the cold reception Christ had in Judea, and about
Jerusalem. Note, There may be a multitude of forward hearers, where
there are but a few true believers. Curiosity, and affectation of
novelty and variety, may bring many to attend upon good preaching, and
to be affected with it for a while, who yet are never subject to the
power of it, Eze. 33:31, 32.

`VII.` The rite, or ceremony, by which he admitted disciples, v. 6. Those
who received his doctrine, and submitted to his discipline, were
baptized of him in Jordan, thereby professing their repentance, and
their belief that the kingdom of the Messiah was at hand. 1. They
testified their repentance by confessing their sins; a general
confession, it is probable, they made to John that they were sinners,
that they were polluted by sin, and needed cleansing; but to God they
made a confession of particular sins, for he is the party offended. The
Jews had been taught to justify themselves; but John teaches them to
accuse themselves, and not to rest, as they used to do, in the general
confession of sin made for all Israel, once a year, upon the day of
atonement; but to make a particular acknowledgment, every one, of the
plague of his own heart. Note, A penitent confession of sin is required
in order to peace and pardon; and those only are ready to receive Jesus
Christ as their Righteousness, who are brought with sorrow and shame to
their own guilt, 1 Jn. 1:9. 2. The benefits of the kingdom of heaven,
now at hand, were thereupon sealed to them by baptism. He washed them
with water, in token of this-that from all their iniquities God would
cleanse them. It was usual with the Jews to baptize those whom they
admitted proselytes to their religion, especially those who were only
Proselytes of the gate, and were not circumcised, as the Proselytes of
righteousness were. Some think it was likewise a custom for persons of
eminent religion, who set up for leaders, by baptism to admit pupils and
disciples. Christ\'s question concerning John\'s Baptism, Was it from
heaven, or of men? implied, that there were baptisms of men, who
pretended not to a divine mission; with this usage John complied, but
his was from heaven, and was distinguished from all others by this
character, It was the baptism of repentance, Acts 19:4. All Israel were
baptized unto Moses, 1 Co. 10:2. The ceremonial law consisted in divers
washings or baptisms (Heb. 9:10); but John\'s baptism refers to the
remedial law, the law of repentance and faith. He is said to baptize
them in Jordan, that river which was famous for Israel\'s passage
through it, and Naaman\'s cure; yet it is probable that John did not
baptize in that river at first, but that afterward, when the people who
came to his baptism were numerous, he removed Jordan. By baptism he
obliged them to live a holy life, according to the profession they took
upon themselves. Note, Confession of sin must always be accompanied with
holy resolutions, in the strength of divine grace, not to return to it
again.

### Verses 7-12

The doctrine John preached was that of repentance, in consideration of
the kingdom of heaven being at hand; now here we have the use of that
doctrine. Application is the life of preaching, so it was of John\'s
preaching.

Observe, 1. To whom he applied it; to the Pharisees and Sadducees that
came to his baptism, v. 7. To others he thought it enough to say,
Repent, for the kingdom of heaven is at hand; but when he saw these
Pharisees and Sadducees come about him, he found it necessary to explain
himself, and deal more closely. These were two of the three noted sects
among the Jews at that time, the third was that of the Essenes, whom we
never read of in the gospels, for they affected retirement, and declined
busying themselves in public affairs. The Pharisees were zealots for the
ceremonies, for the power of the church, and the traditions of the
elders; the Sadducees ran into the other extreme, and were little better
than deists, denying the existence of spirits and a future state. It was
strange that they came to John\'s baptism, but their curiosity brought
them to be hearers; and some of them, it is probable, submitted to be
baptized, but it is certain that the generality of them did not; for
Christ says (Lu. 7:29, 30), that when the publicans justified God, and
were baptized of John, the Pharisees and lawyers rejected the counsel of
God against themselves, being not baptized of him. Note, Many come to
ordinances, who come not under the power of them. Now to them John here
addresses himself with all faithfulness, and what he said to them, he
said to the multitude (Lu. 3:7), for they were all concerned in what he
said. 2. What the application was. It is plain and home, and directed to
their consciences; he speaks as one that came not to preach before them,
but to preach to them. Though his education was private, he was not
bashful when he appeared in public, nor did he fear the face of man, for
he was full of the Holy Ghost, and of power.

`I.` Here is a word of conviction and awakening. He begins harshly, calls
them not Rabbi, gives them not the titles, much less the applauses, they
had been used to. 1. The title he gives them is, O generation of vipers.
Christ gave them the same title; ch. 12:34; 23:33. They were as vipers;
though specious, yet venomous and poisonous, and full of malice and
enmity to every thing that was good; they were a viperous brood, the
seed and offspring of such as had been of the same spirit; it was bred
in the bone with them. They gloried in it, that they were the seed of
Abraham; but John showed them that they were the serpent\'s seed
(compare Gen. 3:15); of their father the Devil, Jn. 8:44. They were a
viperous gang, they were all alike; though enemies to one another, yet
confederate in mischief. Note, A wicked generation is a generation of
vipers, and they ought to be told so; it becomes the ministers of Christ
to be bold in showing sinners their true character. 2. The alarm he
gives them is, Who has warned you to flee from the wrath to come? This
intimates that they were in danger of the wrath to come; and that their
case was so nearly desperate, and their hearts so hardened in sin (the
Pharisees by their parade of religion, and the Sadducees by their
arguments against religion), that it was next to a miracle to effect
anything hopeful among them. \"What brings you hither? Who thought of
seeing you here? What fright have you been put into, that you enquire
after the kingdom of heaven?\" Note, `(1.)` There is a wrath to come;
besides present wrath, the vials of which are poured out now, there is
future wrath, the stores of which are treasured up for hereafter. `(2.)`
It is the great concern of every one of us to flee from this wrath. `(3.)`
It is wonderful mercy that we are fairly warned to flee from this wrath;
think-Who has warned us? God has warned us, who delights not in our
ruin; he warns by the written word, by ministers, by conscience. `(4.)`
These warnings sometime startle those who seemed to have been very much
hardened in their security and good opinion of themselves.

`II.` Here is a word of exhortation and direction (v. 8); \"Bring forth
therefore fruits meet for repentance. Therefore, because you are warned
to flee from the wrath to come, let the terrors of the Lord persuade you
to a holy life.\" Or, \"Therefore, because you profess repentance, and
attend upon the doctrine and baptism of repentance, evidence that you
are true penitents.\" Repentance is seated in the heart. There it is as
a root; but in vain do we pretend to have it there, if we do not bring
forth the fruits of it in a universal reformation, forsaking all sin,
and cleaving to that which is good; these are fruits, axious teµs
metanoias-worthy of repentance. Note, Those are not worthy the name of
penitents, or their privileges, who say they are sorry for their sins,
and yet persist in them. They that profess repentance, as all that are
baptized do, must be and act as becomes penitents, and never do any
thing unbecoming a penitent sinner. It becomes penitents to be humble
and low in their own eyes, to be thankful for the least mercy, patient
under the greatest affliction, to be watchful against all appearances of
sin, and approaches towards it, to abound in every duty, and to be
charitable in judging others.

`III.` Here is a word of caution, not to trust to their external
privileges, so as with them to shift off these calls to repentance (v.
9); Think not to say within yourselves, We have Abraham to our father.
Note, There is a great deal which carnal hearts are apt to say within
themselves, to put by the convincing, commanding power of the word of
God, which ministers should labour to meet with and anticipate; vain
thoughts which lodge within those who are called to wash their hearts,
Jer. 4:14. Meµ doxeµte-Pretend not, presume not, to say within
yourselves; be not of the opinion that this will save you; harbour not
such a conceit. \"Please not yourselves with saying this\" (so some
read); \"rock not yourselves asleep with this, nor flatter yourselves
into a fool\'s paradise.\" Note, God takes notice of what we say within
ourselves, which we dare not speak out, and is acquainted with all the
false rests of the soul, and the fallacies with which it deludes itself,
but which it will not discover, lest it should be undeceived. Many hide
the lie that ruins them, in their right hand, and roll it under their
tongue, because they are ashamed to own it; they keep in the Devil\'s
interest, by keeping the Devil\'s counsel. Now John shows them,

`1.` What their pretense was; \"We have Abraham to our father; we are not
sinners of the Gentiles; it is fit indeed that they should be called to
repent; but we are Jews, a holy nation, a peculiar people, what is this
to us?\" Note, The word does us no good, when we will not take it as it
is spoken to us, and belonging to us. \"Think not that because you are
the seed of Abraham, therefore,\" `(1.)` \"You need not repent, you have
nothing to repent of; your relation to Abraham, and your interest in the
covenant made with him, denominate you so holy, that there is no
occasion for you to change your mind or way.\" `(2.)` \"That therefore you
shall fare well enough, though you do not repent. Think not that this
will bring you off in the judgment, and secure you from the wrath to
come; that God will connive at your impenitence, because you are
Abraham\'s seed.\" Note, It is vain presumption to think that our having
good relations will save us, though we be not good ourselves. What
though we be descended from pious ancestors; have been blessed with a
religious education; have our lot cast in families where the fear of God
is uppermost; and have good friends to advise us, and pray for us; what
will all this avail us, if we do not repent, and live a life of
repentance? We have Abraham to our father, and therefore are entitled to
the privileges of the covenant made with him; being his seed, we are
sons of the church, the temple of the Lord, Jer. 7:4. Note, Multitudes,
by resting in the honours and advantages of their visible
church-membership, take up short of heaven.

`2.` How foolish and groundless this pretence was; they thought that
being the seed of Abraham, they were the only people God had in the
world, and therefore that, if they were cut off, he would be at a loss
for a church; but John shows them the folly of this conceit; I say unto
you (whatever you say within yourselves), that God is able of these
stones to raise up children unto Abraham. He was now baptizing in Jordan
at Bethabara (Jn. 1:28), the house of passage, where the children of
Israel passed over; and there were the twelve stones, one for each
tribe, which Joshua set up for a memorial, Jos. 4:20. It is not unlikely
that he pointed to those stones, which God could raise to be, more than
in representation, the twelve tribes of Israel. Or perhaps he refers to
Isa. 51:1, where Abraham is called the rock out of which they were hewn.
That God who raised Isaac out of such a rock, can, if there be an
occasion, do as much again, for with him nothing is impossible. Some
think he pointed to those heathen soldiers that were present, telling
the Jews that God would raise up a church for himself among the
Gentiles, and entail the blessing of Abraham upon them. Thus when our
first parents fell, God could have left them to perish, and out of
stones have raised up another Adam and another Eve. Or, take it thus,
\"Stones themselves shall be owned as Abraham\'s seed, rather than such
hard, dry, barren sinners as you are.\" Note, As it is lowering to the
confidence of the sinners in Zion, so it is encouraging to the hopes of
the sons of Zion, that, whatever comes of the present generation, God
will never want a church in the world; if the Jews fall off, the
Gentiles shall be grafted in, ch. 21:43; Rom. 11:12, etc.

`IV.` Here is a word of terror to the careless and secure Pharisees and
Sadducees, and other Jews, that knew not the signs of the times, nor the
day of their visitation, v. 10. \"Now look about you, now that the
kingdom of God is at hand, and be made sensible.\"

`1.` How strict and short your trial is; Now the axe is carried before
you, now it is laid to the root of the tree, now you are upon your good
behavior, and are to be so but a while; now you are marked for ruin, and
cannot avoid it but by a speedy and sincere repentance. Now you must
expect that God will make quicker work with you by his judgments than he
did formerly, and that they will begin at the house of God: \"where God
allows more means, he allows less time.\" Behold, I come quickly. Now
they were put upon their last trial; now or never.

`2.` \"How sore and severe your doom will be, if you do not improve
this.\" It is now declared with the axe at the root, to show that God is
in earnest in the declaration, that every tree, however high in gifts
and honours, however green in external professions and performances, if
it bring not forth good fruit, the fruits meet for repentance, is hewn
down, disowned as a tree in God\'s vineyard, unworthy to have room
there, and is cast into the fire of God\'s wrath-the fittest place for
barren trees: what else are they good for? If not fit for fruit, they
are fit for fuel. Probably this refers to the destruction of Jerusalem
by the Romans, which was not, as other judgments had been, like the
lopping off of the branches, or cutting down of the body of the tree,
leaving the root to bud again, but it would be the total, final, and
irrecoverable extirpation of that people, in which all those should
perish that continued impenitent. Now God would make a full end, wrath
was coming on them to the utmost.

`V.` A word of instruction concerning Jesus Christ, in whom all John\'s
preaching centered. Christ\'s ministers preach, not themselves, but him.
Here is,

`1.` The dignity and pre-eminence of Christ above John. See how meanly he
speaks of himself, that he might magnify Christ (v. 11); \"I indeed
baptize you with water, that is the utmost I can do.\" Note, Sacraments
derive not their efficacy from those who administer them; they can only
apply the sign; it is Christ\'s prerogative to give the thing signified,
1 Co. 3:6; 2 Ki. 4:31. But he that comes after me is mightier than `I.`
Though John had much power, for he came in the spirit and power of
Elias, Christ has more; though John was truly great, great in the sight
of the Lord (not a greater was born of woman), yet he thinks himself
unworthy to be in the meanest place of attendance upon Christ, whose
shoes I am not worthy to bear. He sees, `(1.)` How mighty Christ is, in
comparison with him. Note, It is a great comfort to the faithful
ministers, to think that Jesus Christ is mightier than they, can do that
for them, and that by them, which they cannot do; his strength is
perfected in their weakness. `(2.)` How mean he is in comparison with
Christ, not worthy to carry his shoes after him! Note, Those whom God
puts honour upon, are thereby made very humble and low in their own
eyes; willing to be abased, so that Christ may be magnified; to be any
thing, to be nothing, so that Christ may be all.

`2.` The design and intention of Christ\'s appearing, which they were now
speedily to expect. When it was prophesied that John should be sent as
Christ\'s forerunner (Mal. 3:1, 2), it immediately follows, The Lord,
whom ye seek, shall suddenly come, and shall sit as a refiner, v. 3. And
after the coming of Elijah, the day comes that shall burn as an oven
(Mal. 4:1), to which the Baptist seems here to refer. Christ will come
to make a distinction,

`(1.)` By the powerful working of his grace; He shall baptize you, that
is, some of you, with the Holy Ghost and with fire. Note, `[1.]` It is
Christ\'s prerogative to baptize with the Holy Ghost. This he did in the
extraordinary gifts of the Spirit conferred upon the apostles, to which
Christ himself applies these words of John, Acts 1:5. This he does in
the graces and comforts of the Spirit given to them that ask him, Lu.
11:13; Jn. 7:38, 39; See Acts 11:16. `[2.]` They who are baptized with
the Holy Ghost are baptized as with fire; the seven spirits of God
appear as seven lamps of fire, Rev. 4:5. Is fire enlightening? So the
Spirit is a Spirit of illumination. Is it warming? And do not their
hearts burn within them? Is it consuming? And does not the Spirit of
judgment, as a Spirit of burning, consume the dross of their
corruptions? Does fire make all it seizes like itself? And does it move
upwards? So does the Spirit make the soul holy like itself, and its
tendency is heaven-ward. Christ says I am come to send fire, Lu. 12:49.

`(2.)` By the final determinations of his judgment (v. 12); Whose fan is
in his hand. His ability to distinguish, as the eternal wisdom of the
Father, who sees all by a true light, and his authority to distinguish,
as the Person to whom all judgment is committed, is the fan that is in
his hand, Jer. 15:7. Now he sits as a Refiner. Observe here `[1.]` The
visible church is Christ\'s floor; O my threshing, and the corn of my
floor, Isa. 21:10. The temple, a type of church, was built upon a
threshing-floor. `[2.]` In this floor there is a mixture of wheat and
chaff. True believers are as wheat, substantial, useful, and valuable;
hypocrites are as chaff, light, and empty, useless and worthless, and
carried about with every wind; these are now mixed, good and bad, under
the same external profession; and in the same visible communion. `[3.]`
There is a day coming when the floor shall be purged, and the wheat and
chaff shall be separated. Something of this kind is often done in this
world, when God calls his people out of Babylon, Rev. 18:4. But it is
the day of the last judgment that will be the great winnowing,
distinguishing day, which will infallibly determine concerning doctrines
and works (1 Co. 3:13), and concerning persons (ch. 25:32, 33), when
saints and sinners shall be parted for ever. `[4.]` Heaven is the garner
into which Jesus Christ will shortly gather all his wheat, and not a
grain of it shall be lost: he will gather them as the ripe fruits were
gathered in. Death\'s scythe is made use of to gather them to their
people. In heaven the saints are brought together, and no longer
scattered; they are safe, and no longer exposed; separated from corrupt
neighbours without, and corrupt affections within, and there is no chaff
among them. They are not only gathered into the barn (ch. 13:30), but
into the garner, where they are thoroughly purified. `[5.]` Hell is the
unquenchable fire, which will burn up the chaff, which will certainly be
the portion and punishment, and everlasting destruction, of hypocrites
and unbelievers. So that here are life and death, good and evil, set
before us; according as we now are in the field, we shall be then in the
floor.

### Verses 13-17

Our Lord Jesus, from his childhood till now, when he was almost thirty
years of age, had lain hid in Galilee, as it were, buried alive; but
now, after a long and dark night, behold, the Sun of righteousness rises
in glory. The fulness of time was come that Christ should enter upon his
prophetical office; and he chooses to do it, not at Jerusalem (though it
is probable that he went thither at the three yearly feasts, as others
did), but there where John was baptizing; for to him resorted those who
waited for the consolation of Israel, to whom alone he would be welcome.
John the Baptist was six months older than our Saviour, and it is
supposed that he began to preach and baptize about six months before
Christ appeared; so long he was employed in preparing his way, in the
region round about Jordan; and more was done towards it in these six
months than had been done in several ages before. Christ\'s coming from
Galilee to Jordan, to be baptized, teaches us not the shrink from pain
and toil, that we may have an opportunity of drawing nigh to God in
ordinance. We should be willing to go far, rather than come short of
communion with God. Those who will find must seek.

Now in this story of Christ\'s baptism we may observe,

`I.` How hardly John was persuaded to admit of it, v. 14, 15. It was an
instance of Christ\'s great humility, that he would offer himself to be
baptized of John; that he who knew no sin would submit to the baptism of
repentance. Note, As soon as ever Christ began to preach, he preached
humility, preached it by his example, preached it to all, especially the
young ministers. Christ was designed for the highest honours, yet in his
first step he thus abases himself. Note, Those who would rise high must
begin low. Before honour is humility. It was a great piece of respect
done to John, for Christ thus to come to him; and it was a return for
the service he did him, in giving notice of his approach. Note, Those
that honour God he will honour. Now here we have,

`1.` The objection that John made against baptizing Jesus, v. 14. John
forbade him, as Peter did, when Christ went about to wash his feet, Jn.
13:6, 8. Note, Christ\'s gracious condescensions are so surprising, as
to appear at first incredible to the strongest believers; so deep and
mysterious, that even they who know his mind well cannot soon find out
the meaning of them, but, by reason of darkness, start objections
against the will of Christ. John\'s modesty thinks this an honour too
great for him to receive, and he expresses himself to Christ, just as
his mother had done to Christ\'s mother (Lu. 1:43); Whence is this to
me, that the mother of my Lord should come to me? John had now obtained
a great name, and was universally respected: yet see how humble he is
still! Note, God has further honours in reserve for those whose spirits
continue low when their reputation rises.

`(1.)` John thinks it necessary that he should be baptized of Christ; I
have need to be baptized of thee with the baptism of the Holy Ghost, as
of fire, for that was Christ\'s baptism, v. 11. `[1.]` Though John was
filled with the Holy Ghost from the womb (Lu. 1:15), yet he acknowledges
he had need to be baptized with that baptism. Note, They who have much
of the Spirit of God, yet, while here, in this imperfect state, see that
they have need of more, and need to apply themselves to Christ for more.
`[2.]` John has need to be baptized, though he was the greatest that
ever was born of woman; yet, being born of a woman, he is polluted, as
others of Adam\'s seed are, and owns he had need of cleansing. Note, The
purest souls are most sensible of their own remaining impurity, and seek
most earnestly for spiritual washing. `[3.]` He has need to be baptized
of Christ, who can do that for us, which no one else can, and which must
be done for us, or we are undone. Note, The best and holiest of men have
need of Christ, and the better they are, the more they see of that need.
`[4.]` This was said before the multitude, who had a great veneration
for John, and were ready to embrace him for the Messiah; yet he publicly
owns that he had need to be baptized of Christ. Note, It is no
disparagement to the greatest of men, to confess that they are undone
without Christ and his grace. `[5.]` John was Christ\'s forerunner, and
yet owns that he had need to be baptized of him. Note, Even they who
were born before Christ in time depended on him, received from him, and
had an eye to him. `[6.]` While John was dealing with others about their
souls, observe how feelingly he speaks of the case of his own soul, I
have need to be baptized of thee. Note, Ministers, who preach to others,
and baptize others, are concerned to look to it that they preach to
themselves, and be themselves baptized with the Holy Ghost. Take heed to
thyself first; save thyself, 1 Tim. 4:16.

`(2.)` He therefore thinks it very preposterous and absurd, that Christ
should be baptized by him; Comest thou to me? Does the holy Jesus, that
is separated from sinners, come to be baptized by a sinner, as a sinner,
and among sinners? How can this be? Or what account can we give of it?
Note, Christ\'s coming to us may well be wondered at.

`2.` The overruling of this objection (v. 15); Jesus said, Suffer it to
be so now. Christ accepted his humility, but not his refusal; he will
have the thing done; and it is fit that Christ should take his own
method, though we do not understand it, nor can give a reason for it.
See,

`(1.)` How Christ insisted upon it; It must be so now. He does not deny
that John had need to be baptized of him, yet he will now be baptized of
John. Aphes arti-Let it be yet so; suffer it to be so now. Note, Every
thing is beautiful in its season. But why now? Why yet? `[1.]` Christ is
now in a state of humiliation: he has emptied himself, and made himself
of no reputation. He is not only found in fashion as a man, but is made
in the likeness of sinful flesh, and therefore now let him be baptized
of John; as if he needed to be washed, though perfectly pure; and thus
he was made sin for us, though he knew no sin. `[2.]` John\'s baptism is
now in reputation, it is that by which God is now doing his work; that
is the present dispensation, and therefore Jesus will now be baptized
with water; but his baptizing with the Holy Ghost is reserved for
hereafter, many days hence, Acts 1:5. John\'s baptism has now its day,
and therefore honour must now be put upon that, and they who attend upon
it must be encouraged. Note, They who are of greatest attainments in
gifts and graces, should yet, in their place, bear their testimony to
instituted ordinances, by a humble and diligent attendance on them, that
they may give a good example to others. What we see God owns, and while
we see he does so, we must own. John was now increasing, and therefore
it must be thus yet; shortly he will decrease, and then it will be
otherwise. `[3.]` It must be so now, because now is the time for
Christ\'s appearing in public, and this will be a fair opportunity for
it, See Jn. 1:31-34. Thus he must be made manifest to Israel, and be
signalized by wonders from heaven, in that act of his own, which was
most condescending and self-abasing.

`(2.)` The reason he gives for it; Thus it becomes us to fulfil all
righteousness. Note, `[1.]` There was a propriety in every thing that
Christ did for us; it was all graceful (Heb. 2:10; 7:26); and we must
study to do not only that which behoves us, but that which becomes us;
not only that which is indispensably necessary, but that which is
lovely, and of good report. `[2.]` Our Lord Jesus looked upon it as a
thing well becoming him, to fulfil all righteousness, that is (as Dr.
Whitby explains it), to own every divine institution, and to show his
readiness to comply with all God\'s righteous precepts. Thus it becomes
him to justify God, and approve his wisdom, in sending John to prepare
his way by the baptism of repentance. Thus it becomes us to countenance
and encourage every thing that is good, by pattern as well as precept.
Christ often mentioned John and his baptism with honour, which that he
might do the better, he was himself baptized. Thus Jesus began first to
do, and then to teach; and his ministers must take the same method. Thus
Christ filled up the righteousness of the ceremonial law, which
consisted in divers washings; thus he recommended the gospel-ordinance
of baptism to his church, put honour upon it, and showed what virtue he
designed to put into it. It became Christ to submit to John\'s washing
with water, because it was a divine appointment; but it became him to
oppose the Pharisees\' washing with water, because it was a human
invention and imposition; and he justified his disciples in refusing to
comply with it.

With the will of Christ, and this reason for it, John was entirely
satisfied, and then he suffered him. The same modesty which made him at
first decline the honour Christ offered him, now made him do the service
Christ enjoined him. Note, No pretence of humility must make us decline
our duty.

`II.` How solemnly Heaven was pleased to grace the baptism of Christ with
a special display of glory (v. 16, 17); Jesus when he was baptized, went
up straightway out of the water. Others that were baptized staid to
confess their sins (v. 6); but Christ, having no sins to confess, went
up immediately out of the water; so we read it, but not right: for it is
apo tou hydatos-from the water; from the brink of the river, to which he
went down to be washed with water, that is, to have his head or face
washed (Jn. 13:9); for here is no mention of the putting off, or putting
on, of his clothes, which circumstance would not have omitted, if he had
been baptized naked. He went up straightway, as one that entered upon
his work with the utmost cheerfulness and resolution; he would lose no
time. How was he straitened till it was accomplished!

Now, when he was coming up out of the water, and all the company had
their eye upon him,

`1.` Lo! the heavens were opened unto him, so as to discover something
above and beyond the starry firmament, at least, to him. This was, `(1.)`
To encourage him to go on in his undertaking, with the prospect of the
glory and joy that were set before him. Heaven is opened to receive him,
when he has finished the work he is now entering upon. `(2.)` To encourage
us to receive him, and submit to him. Note, In and through Jesus Christ,
the heavens are opened to the children of men. Sin shut up heaven, put a
stop to all friendly intercourse between God and man; but now Christ has
opened the kingdom of heaven to all believers. Divine light and love are
darted down upon the children of men, and we have boldness to enter into
the holiest. We have receipts of mercy from God, we make returns of duty
to God, and all by Jesus Christ, who is the ladder that had its foot on
earth and its top in heaven, by whom alone it is that we have any
comfortable correspondence with God, or any hope of getting to heaven at
last. The heavens were opened when Christ was baptized, to teach us,
that when we duly attend on God\'s ordinances, we may expect communion
with him, and communications from him.

`2.` He saw the Spirit of God descending like a dove, or as a dove, and
coming or lighting upon him. Christ saw it (Mk. 1:10), and John saw it
(Jn. 1:33, 34), and it is probable that all the standers-by saw it; for
this was intended to be his public inauguration. Observe,

`(1.)` He saw the Spirit of God descended, and lighted on him. In the
beginning of the old world, the Spirit of God moved upon the face of the
waters (Gen. 1:2), hovered as a bird upon the nest. So here, in the
beginning of this new world, Christ, as God, needed not to receive the
Holy Ghost, but it was foretold that the Spirit of the Lord should rest
upon him (Isa. 11:2; 61:1), and here he did so; for, `[1.]` He was to be
a Prophet; and prophets always spoke by the Spirit of God, who came upon
them. Christ was to execute the prophetic office, not by his divine
nature (says Dr. Whitby), but by the afflatus of the Holy Spirit. `[2.]`
He was to be the Head of the church; and the Spirit descended upon him,
by him to be derived to all believers, in his gifts, graces, and
comforts. The ointment on the head ran down to the skirts; Christ
received gifts for men, that he might give gifts to men.

`(2.)` He descended on him like a dove; whether it was a real, living
dove, or, as was usual in visions, the representation or similitude of a
dove, is uncertain. If there must be a bodily shape (Lu. 3:22), it must
not be that of a man, for the being seen in fashion as a man was
peculiar to the second person: none therefore was more fit than the
shape of one of the fowls of heaven (heaven being now opened), and of
all fowl none was so significant as the dove. `[1.]` The Spirit of
Christ is a dove-like spirit; not like a silly dove, without heart (Hos.
7:11), but like an innocent dove, without gall. The Spirit descended,
not in the shape of an eagle, which is, though a royal bird, yet a bird
of prey, but in the shape of a dove, than which no creature is more
harmless and inoffensive. Such was the Spirit of Christ: He shall not
strive, nor cry; such must Christians be, harmless as doves. The dove is
remarkable for her eyes; we find that both the eyes of Christ (Cant.
5:12), and the eyes of the church (Cant. 1:15; 4:1), are compared to
doves\' eyes, for they have the same spirit. The dove mourns much (Isa.
38:14). Christ wept oft; and penitent souls are compared to doves of the
valleys. `[2.]` The dove was the only fowl that was offered in sacrifice
(Lev. 1:14), and Christ by the Spirit, the eternal Spirit, offered
himself without spot to God. `[3.]` The tidings of the decrease of
Noah\'s flood were brought by a dove, with an olive-leaf in her mouth;
fitly therefore are the glad tidings of peace with God brought by the
Spirit as a dove. It speaks God\'s good will towards men; that his
thoughts towards us are thoughts of good, and not evil. By the voice of
the turtle heard in our land (Cant. 2:12), the Chaldee paraphrase
understands, the voice of the Holy Spirit. That God is in Christ
reconciling the world unto himself, is a joyful message, which comes to
us upon the wing, the wings of a dove.

`3.` To explain and complete this solemnity, there came a voice from
heaven, which, we have reason to think, was heard by all that were
present. The Holy Spirit manifested himself in the likeness of a dove,
but God the Father by a voice; for when the law was given they saw no
manner of similitude, only they heard a voice (Deu. 4:12); and so this
gospel came, and gospel indeed it is, the best news that ever came from
heaven to earth; for it speaks plainly and fully God\'s favour to
Christ, and us in him.

`(1.)` See here how God owns our Lord Jesus; This is my beloved Son.
Observe, `[1.]` The relation he stood in to him; He is my Son. Jesus
Christ is the Son of God, by eternal generation, as he was begotten of
the Father before all the worlds (Col. 1:15; Heb. 1:3); and by
supernatural conception; he was therefore called the Son of God, because
he was conceived by the power of the Holy Ghost (Lu. 1:35); yet this is
not all; he is the Son of God by special designation to the work and
office of the world\'s Redeemer. He was sanctified and sealed, and sent
upon that errand, brought up with the Father for it (Prov. 8:30),
appointed to it; I will make him my First-born, Ps. 89:27. `[2.]` The
affection the Father had for him; He is my beloved Son; his dear Son,
the Son of his love (Col. 1:13); he has lain in his bosom from all
eternity (Jn. 1:18), had been always his delight (Prov. 8:30), but
particularly as Mediator, and in undertaking the work of man\'s
salvation, he was his beloved Son. He is my Elect, in whom my soul
delights. See Isa. 42:1. Because he consented to the covenant of
redemption, and delighted to do that will of God, therefore the Father
loved him. Jn. 10:17; 3:35. Behold, then, behold, and wonder, what
manner of love the Father has bestowed on us, that he should deliver up
him that was the Son of his love, to suffer and die for those that were
the generation of his wrath; nay, and that he therefore loved him,
because he laid down his life for the sheep! Now know we that he loved
us, seeing he has not withheld his Son, his only Son, his Isaac whom he
loved, but gave him to be a sacrifice for our sin.

`(2.)` See here how ready he is to own us in him: He is my beloved Son,
not only with whom, but in whom, I am well pleased. He is pleased with
all that are in him, and are united to him by faith. Hitherto God had
been displeased with the children of men, but now his anger is turned
away, and he has made us accepted in the Beloved, Eph. 50:6. Let all the
world take notice, that this is the Peace-maker, the Days-man, who has
laid his hand upon us both, and that there is no coming to God as a
Father, but by him as Mediator, Jn. 14:6. In him our spiritual
sacrifices are acceptable, for his the Altar that sanctifies every gift,
1 Pt. 2:5. Out of Christ, God is a consuming Fire, but, in Christ, a
reconciled Father. This is the sum of the whole gospel; it is a faithful
saying, and worthy of all acceptation, that God has declared, by a voice
from heaven, that Jesus Christ is his beloved Son, in whom he is well
pleased, with which we must by faith cheerfully concur, and say, that he
is our beloved Saviour, in whom we are well pleased.
