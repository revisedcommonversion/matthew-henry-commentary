Matthew, Chapter 5
==================

Commentary
----------

This chapter, and the two that follow it, are a sermon; a famous sermon;
the sermon upon the mount. It is the longest and fullest continued
discourse of our Saviour that we have upon record in all the gospels. It
is a practical discourse; there is not much of the credenda of
Christianity in it-the things to be believed, but it is wholly taken up
with the agenda-the things to be done; these Christ began with in his
preaching; for if any man will do his will, he shall know of the
doctrine, whether it be of God. The circumstances of the sermon being
accounted for (v. 1, 2), the sermon itself follows, the scope of which
is, not to fill our heads with notions, but to guide and regulate our
practice. `I.` He proposes blessedness as the end, and gives us the
character of those who are entitled to blessedness (very different from
the sentiments of a vain world), in eight beatitudes, which may justly
be called paradoxes (v. 3-12). `II.` He prescribes duty as the way, and
gives us standing rules of that duty. He directs his disciples, 1. To
understand what they are-the salt of the earth, and the lights of the
world (v. 13-16). 2. To understand what they have to do-they are to be
governed by the moral law. Here is, `(1.)` A general ratification of the
law, and a recommendation of it to us, as our rule (v. 17-20). `(2.)` A
particular rectification of divers mistakes; or, rather, a reformation
of divers wilful, gross corruptions, which the scribes and Pharisees had
introduced in their exposition of the law; and an authentic explication
of divers branches which most needed to be explained and vindicated (v.
20). Particularly, here is an explication, `[1.]` Of the sixth
commandment, which forbids murder (v. 21-26). `[2.]` Of the seventh
commandment, against adultery (v. 27-32). `[3.]` Of the third
commandment (v. 33-37). `[4.]` Of the law of retaliation (v. 38-42).
`[5.]` Of the law of brotherly love (v. 43-48). And the scope of the
whole is, to show that the law is spiritual.

### Verses 1-2

We have here a general account of this sermon.

`I.` The Preacher was our Lord Jesus, the Prince of preachers, the great
Prophet of his church, who came into the world, to be the Light of the
world. The prophets and John had done virtuously in preaching, but
Christ excelled them all. He is the eternal Wisdom, that lay in the
bosom of the Father, before all worlds, and perfectly knew his will (Jn.
1:18); and he is the eternal Word, by whom he has in these last days
spoken to us. The many miraculous cures wrought by Christ in Galilee,
which we read of in the close of the foregoing chapter, were intended to
make way for this sermon, and to dispose people to receive instructions
from one in whom there appeared so much of a divine power and goodness;
and, probably, this sermon was the summary, or rehearsal, of what he had
preached up and down in the synagogues of Galilee. His text was, Repent,
for the kingdom of heaven is at hand. This is a sermon on the former
part of that text, showing what it is to repent; it is to reform, both
in judgment and practice; and here he tells us wherein, in answer to
that question (Mal. 3:7), Wherein shall we return? He afterward preached
upon the latter part of the text, when, in divers parables, he showed
what the kingdom of heaven is like, ch. 13.

`II.` The place was a mountain in Galilee. As in other things, so in
this, our Lord Jesus was but ill accommodated; he had no convenient
place to preach in, any more than to lay his head on. While the scribes
and Pharisees had Moses\' chair to sit in, with all possible ease,
honour, and state, and there corrupted the law; our Lord Jesus, the
great Teacher of truth, is driven out to the desert, and finds no better
a pulpit than a mountain can afford; and not one of the holy mountains
neither, not one of the mountains of Zion, but a common mountain; by
which Christ would intimate that there is no such distinguishing
holiness of places now, under the gospel, as there was under the law;
but that it is the will of God that men should pray and preach every
where, any where, provided it be decent and convenient. Christ preached
this sermon, which was an exposition of the law, upon a mountain,
because upon a mountain the law was given; and this was also a solemn
promulgation of the Christian law. But observe the difference: when the
law was given, the Lord came down upon the mountain; now the Lord went
up: then, he spoke in thunder and lightning; now, in a still small
voice: then the people were ordered to keep their distance; now they are
invited to draw near: a blessed change! If God\'s grace and goodness are
(as they certainly are) his glory, then the glory of the gospel is the
glory that excels, for grace and truth came by Jesus Christ, 2 Co. 3:7;
Heb. 12:18, etc. It was foretold of Zebulun and Issachar, two of the
tribes of Galilee (Deu. 33:19), that they shall call the people to the
mountain; to this mountain we are called, to learn to offer the
sacrifices of righteousness. Now was this the mountain of the Lord,
where he taught us his ways, Isa. 2:2, 3; Mic. 4:1, 2.

`III.` The auditors were his disciples, who came unto him; came at his
call, as appears by comparing Mk. 3:13, Lu. 6:13. To them he directed
his speech, because they followed him for love and learning, while
others attended him only for cures. He taught them, because they were
willing to be taught (the meek will he teach his way); because they
would understand what he taught, which to others was foolishness; and
because they were to teach others; and it was therefore requisite that
they should have a clear and distinct knowledge of these things
themselves. The duties prescribed in this sermon were to be
conscientiously performed by all those that would enter into that
kingdom of heaven which they were sent to set up, with hope to have the
benefit of it. But though this discourse was directed to the disciples,
it was in the hearing of the multitude; for it is said (ch. 7:28), The
people were astonished. No bounds were set about this mountain, to keep
the people off, as were about mount Sinai (Ex. 19:12); for, through
Christ, we have access to God, not only to speak to him, but to hear
from him. Nay, he had an eye to the multitude, in preaching this sermon.
When the fame of his miracles had brought a vast crowd together, he took
the opportunity of so great a confluence of people, to instruct them.
Note, It is an encouragement to a faithful minister to cast the net of
the gospel where there are a great many fishes, in hope that some will
be caught. The sight of a multitude puts life into a preacher, which yet
must arise from a desire of their profit, not his own praise.

`IV.` The solemnity of his sermon is intimated in that word, when he was
set. Christ preached many times occasionally, and by interlocutory
discourses; but this was a set sermon, kathisantos autou, when he had
placed himself so as to be best heard. He sat down as a Judge or
Lawgiver. It intimates with what sedateness and composure of mind the
things of God should be spoken and heard. He sat, that the scriptures
might be fulfilled (Mal. 3:3), He shall sit as a refiner, to purge away
the dross, the corrupt doctrines of the sons of Levi. He sat as in the
throne, judging right (Ps. 9:4); for the word he spoke shall judge us.
That phrase, He opened his mouth, is only a Hebrew periphrasis of
speaking, as Job 3:1. Yet some think it intimates the solemnity of this
discourse; the congregation being large, he raised his voice, and spoke
louder than usual. He had spoken long by his servants the prophets, and
opened their mouths (Eze. 3:27; 24:27; 33:22); but now he opened his
own, and spoke with freedom, as one having authority. One of the
ancients has this remark upon it; Christ taught much without opening his
mouth. that is, by his holy and exemplary life; nay, he taught, when,
being led as a lamb to the slaughter, he opened not his mouth, but now
he opened his mouth, and taught, that the scriptures might be fulfilled,
Prov. 8:1, 2, 6. Doth not wisdom cry-cry on the top of high places? And
the opening of her lips shall be right things. He taught them, according
to the promise (Isa. 54:13), All thy children shall be taught of the
Lord; for this purpose he had the tongue of the learned (Isa. 50:4), and
the Spirit of the Lord, Isa. 61:1. He taught them, what was the evil
they should abhor, and what was the good they should abide and abound
in; for Christianity is not a matter of speculation, but is designed to
regulate the temper of our minds and the tenour of our conversations;
gospel-time is a time of reformation (Heb. 9:10); and by the gospel we
must be reformed, must be made good, must be made better. The truth, as
it is in Jesus, is the truth which is according to godliness, Tit. 1:1.

### Verses 3-12

Christ begins his sermon with blessings, for he came into the world to
bless us (Acts 3:26), as the great High Priest of our profession; as the
blessed Melchizedec; as He in whom all the families of the earth should
be blessed, Gen. 12:3. He came not only to purchase blessings for us,
but to pour out and pronounce blessings on us; and here he does it as
one having authority, as one that can command the blessing, even life
for evermore, and that is the blessing here again and again promised to
the good; his pronouncing them happy makes them so; for those whom he
blesses, are blessed indeed. The Old Testament ended with a curse (Mal.
4:6), the gospel begins with a blessing; for hereunto are we called,
that we should inherit the blessing. Each of the blessings Christ here
pronounces has a double intention: 1. To show who they are that are to
be accounted truly happy, and what their characters are. 2. What that is
wherein true happiness consists, in the promises made to persons of
certain characters, the performance of which will make them happy. Now,

`1.` This is designed to rectify the ruinous mistakes of a blind and
carnal world. Blessedness is the thing which men pretend to pursue; Who
will make us to see good? Ps. 4:6. But most mistake the end, and form a
wrong notion of happiness; and then no wonder that they miss the way;
they choose their own delusions, and court a shadow. The general opinion
is, Blessed are they that are rich, and great, and honourable in the
world; they spend their days in mirth, and their years in pleasure; they
eat the fat, and drink the sweet, and carry all before them with a high
hand, and have every sheaf bowing to their sheaf; happy the people that
is in such a case; and their designs, aims, and purposes are
accordingly; they bless the covetous (Ps. 10:3); they will be rich. Now
our Lord Jesus comes to correct this fundamental error, to advance a new
hypothesis, and to give us quite another notion of blessedness and
blessed people, which, however paradoxical it may appear to those who
are prejudiced, yet is in itself, and appears to be to all who are
savingly enlightened, a rule and doctrine of eternal truth and
certainty, by which we must shortly be judged. If this, therefore, be
the beginning of Christ\'s doctrine, the beginning of a Christian\'s
practice must be to take his measures of happiness from those maxims,
and to direct his pursuits accordingly.

`2.` It is designed to remove the discouragements of the weak and poor
who receive the gospel, by assuring them that his gospel did not make
those only happy that were eminent in gifts, graces, comforts, and
usefulness; but that even the least in the kingdom of heaven, whose
heart was upright with God, was happy in the honours and privileges of
that kingdom.

`3.` It is designed to invite souls to Christ, and to make way for his
law into their hearts. Christ\'s pronouncing these blessings, not at the
end of his sermon, to dismiss the people, but at the beginning of it, to
prepare them for what he had further to say to them, may remind us of
mount Gerizim and mount Ebal, on which the blessings and cursings of the
law were read, Deu. 27:12, etc. There the curses are expressed, and the
blessings only implied; here the blessings are expressed, and the curses
implied: in both, life and death are set before us; but the law appeared
more as a ministration of death, to deter us from sin; the gospel as a
dispensation of life, to allure us to Christ, in whom alone all good is
to be had. And those who had seen the gracious cures wrought by his hand
(ch. 4:23, 24), and now heard the gracious words proceeding out of his
mouth, would say that he was all of a piece, made up of love and
sweetness.

`4.` It is designed to settle and sum up the articles of agreement
between God and man. The scope of the divine revelation is to let us
know what God expects from us, and what we may then expect from him; and
no where is this more fully set forth in a few words than here, nor with
a more exact reference to each other; and this is that gospel which we
are required to believe; for what is faith but a conformity to these
characters, and a dependence upon these promises? The way to happiness
is here opened, and made a highway (Isa. 35:8); and this coming from the
mouth of Jesus Christ, it is intimated that from him, and by him, we are
to receive both the seed and the fruit, both the grace required, and the
glory promised. Nothing passes between God and fallen man, but through
his hand. Some of the wiser heathen had notions of blessedness different
from the rest of mankind, and looking toward this of our Saviour.
Seneca, undertaking to describe a blessed man, makes it out, that it is
only an honest, good man that is to be so called: De Vitâ Beatâ. cap. 4.
Cui nullum bonum malumque sit, nisi bonus malusque animus-Quem nec
extollant fortuita, nec frangant-Cui vera voluptas erit voluptatum
comtemplio-Cui unum bonum honestas, unum malum turpitudo.-In whose
estimation nothing is good or evil, but a good or evil heart-Whom no
occurrences elate or deject-Whose true pleasure consists in a contempt
of pleasure-To whom the only good is virtue, and the only evil vice.

Our Saviour here gives us eight characters of blessed people; which
represent to us the principal graces of a Christian. On each of them a
present blessing is pronounced; Blessed are they; and to each a future
blessing is promised, which is variously expressed, so as to suit the
nature of the grace or duty recommended.

Do we ask then who are happy? It is answered,

`I.` The poor in spirit are happy, v. 3. There is a poor-spiritedness that
is so far from making men blessed that it is a sin and a snare-cowardice
and base fear, and a willing subjection to the lusts of men. But this
poverty of spirit is a gracious disposition of soul, by which we are
emptied of self, in order to our being filled with Jesus Christ. To be
poor in spirit is, 1. To be contentedly poor, willing to be emptied of
worldly wealth, if God orders that to be our lot; to bring our mind to
our condition, when it is a low condition. Many are poor in the world,
but high in spirit, poor and proud, murmuring and complaining, and
blaming their lot, but we must accommodate ourselves to our poverty,
must know how to be abased, Phil. 4:12. Acknowledging the wisdom of God
in appointing us to poverty, we must be easy in it, patiently bear the
inconveniences of it, be thankful for what we have, and make the best of
that which is. It is to sit loose to all worldly wealth, and not set our
hearts upon it, but cheerfully to bear losses and disappointments which
may befal us in the most prosperous state. It is not, in pride or
pretence, to make ourselves poor, by throwing away what God has given
us, especially as those in the church of Rome, who vow poverty, and yet
engross the wealth of the nations; but if we be rich in the world we
must be poor in spirit, that is, we must condescend to the poor and
sympathize with them, as being touched with the feeling of their
infirmities; we must expect and prepare for poverty; must not
inordinately fear or shun it, but must bid it welcome, especially when
it comes upon us for keeping a good conscience, Heb. 10:34. Job was poor
in spirit, when he blessed God in taking away, as well as giving. 2. It
is to be humble and lowly in our own eyes. To be poor in spirit, is to
think meanly of ourselves, of what we are, and have, and do; the poor
are often taken in the Old Testament for the humble and self-denying, as
opposed to those that are at ease, and the proud; it is to be as little
children in our opinion of ourselves, weak, foolish, and insignificant,
ch. 18:4; 19:14. Laodicea was poor in spirituals, wretchedly and
miserably poor, and yet rich in spirit, so well increased with goods, as
to have need of nothing, Rev. 3:17. On the other hand, Paul was rich in
spirituals, excelling most in gifts and graces, and yet poor in spirit,
the least of the apostles, less than the least of all saints, and
nothing in his own account. It is to look with a holy contempt upon
ourselves, to value others and undervalue ourselves in comparison of
them. It is to be willing to make ourselves cheap, and mean, and little,
to do good; to become all things to all men. It is to acknowledge that
God is great, and we are mean; that he is holy and we are sinful; that
he is all and we are nothing, less than nothing, worse than nothing; and
to humble ourselves before him, and under his mighty hand. 3. It is to
come off from all confidence in our own righteousness and strength, that
we may depend only upon the merit of Christ for our justification, and
the spirit and grace of Christ for our sanctification. That broken and
contrite spirit with which the publican cried for mercy to a poor
sinner, is that poverty of spirit. We must call ourselves poor, because
always in want of God\'s grace, always begging at God\'s door, always
hanging on in his house.

Now, `(1.)` This poverty in spirit is put first among the Christian
graces. The philosophers did not reckon humility among their moral
virtues, but Christ puts it first. Self-denial is the first lesson to be
learned in his school, and poverty of spirit entitled to the first
beatitude. The foundation of all other graces is laid in humility. Those
who would build high must begin low; and it is an excellent preparative
for the entrance of gospel-grace into the soul; it fits the soil to
receive the seed. Those who are weary and heavy laden, are the poor in
spirit, and they shall find rest with Christ.

`(2.)` They are blessed. Now they are so, in this world. God looks
graciously upon them. They are his little ones, and have their angels.
To them he gives more grace; they live the most comfortable lives, and
are easy to themselves and all about them, and nothing comes amiss to
them; while high spirits are always uneasy.

`(3.)` Theirs is the kingdom of heaven. The kingdom of grace is composed
of such; they only are fit to be members of Christ\'s church, which is
called the congregation of the poor (Ps. 74:19); the kingdom of glory is
prepared for them. Those who thus humble themselves, and comply with God
when he humbles them, shall be thus exalted. The great, high spirits go
away with the glory of the kingdoms of the earth; but the humble, mild,
and yielding souls obtain the glory of the kingdom of heaven. We are
ready to think concerning those who are rich, and do good with their
riches, that, no doubt, theirs is the kingdom of heaven; for they can
thus lay up in store a good security for the time to come; but what
shall the poor do, who have not wherewithal to do good? Why, the same
happiness is promised to those who are contentedly poor, as to those who
are usefully rich. If I am not able to spend cheerfully for his sake, if
I can but want cheerfully for his sake, even that shall be recompensed.
And do not we serve a good master then?

`II.` They that mourn are happy (v. 4); Blessed are they that mourn. This
is another strange blessing, and fitly follows the former. The poor are
accustomed to mourn, the graciously poor mourn graciously. We are apt to
think, Blessed are the merry; but Christ, who was himself a great
mourner, says, Blessed are the mourners. There is a sinful mourning,
which is an enemy to blessedness-the sorrow of the world; despairing
melancholy upon a spiritual account, and disconsolate grief upon a
temporal account. There is a natural mourning, which may prove a friend
to blessedness, by the grace of God working with it, and sanctifying the
afflictions to us, for which we mourn. But there is a gracious mourning,
which qualifies for blessedness, an habitual seriousness, the mind
mortified to mirth, and an actual sorrow. 1. A penitential mourning for
our own sins; this is godly sorrow, a sorrow according to God; sorrow
for sin, with an eye to Christ, Zec. 12:10. Those are God\'s mourners,
who live a life of repentance, who lament the corruption of their
nature, and their many actual transgressions, and God\'s withdrawings
from them; and who, out of regard to God\'s honour, mourn also for the
sins of others, and sigh and cry for their abominations, Eze. 9:4. 2. A
sympathizing mourning for the afflictions of others; the mourning of
those who weep with them that weep, are sorrowful for the solemn
assemblies, for the desolations of Zion (Zep. 3:18; Ps. 137:1),
especially who look with compassion on perishing souls, and weep over
them, as Christ over Jerusalem.

Now these gracious mourners, `(1.)` Are blessed. As in vain and sinful
laughter the heart is sorrowful, so in gracious mourning the heart has a
serious joy, a secret satisfaction, which a stranger does not
intermeddle with. They are blessed, for they are like the Lord Jesus,
who was a man of sorrows, and of whom we never read that he laughed, but
often that he wept. The are armed against the many temptations that
attend vain mirth, and are prepared for the comforts of a sealed pardon
and a settled peace. `(2.)` They shall be comforted. Though perhaps they
are not immediately comforted, yet plentiful provision is made for their
comfort; light is sown for them; and in heaven, it is certain, they
shall be comforted, as Lazarus, Lu. 16:25. Note, The happiness of heaven
consists in being perfectly and eternally comforted, and in the wiping
away of all tears from their eyes. It is the joy of our Lord; a fulness
of joy and pleasures for evermore; which will be doubly sweet to those
who have been prepared for them by this godly sorrow. Heaven will be a
heaven indeed to those who go mourning thither; it will be a harvest of
joy, the return of a seed-time of tears (Ps. 126:5, 6); a mountain of
joy, to which our way lies through a vale of tears. See Isa. 66:10.

`III.` The meek are happy (v. 5); Blessed are the meek. The meek are
those who quietly submit themselves to God, to his word and to his rod,
who follow his directions, and comply with his designs, and are gentle
towards all men (Tit. 3:2); who can bear provocation without being
inflamed by it; are either silent, or return a soft answer; and who can
show their displeasure when there is occasion for it, without being
transported into any indecencies; who can be cool when others are hot;
and in their patience keep possession of their own souls, when they can
scarcely keep possession of any thing else. They are the meek, who are
rarely and hardly provoked, but quickly and easily pacified; and who
would rather forgive twenty injuries than revenge one, having the rule
of their own spirits.

These meek ones are here represented as happy, even in this world. 1.
They are blessed, for they are like the blessed Jesus, in that wherein
particularly they are to learn of him, ch. 11:29. They are like the
blessed God himself, who is Lord of his anger, and in whom fury is not.
They are blessed, for they have the most comfortable, undisturbed
enjoyment of themselves, their friends, their God; they are fit for any
relation, and condition, any company; fit to live, and fit to die. 2.
They shall inherit the earth; it is quoted from Ps. 37:11, and it is
almost the only express temporal promise in all the New Testament. Not
that they shall always have much of the earth, much less that they shall
be put off with that only; but this branch of godliness has, in a
special manner, the promise of life that now is. Meekness, however
ridiculed and run down, has a real tendency to promote our health,
wealth, comfort, and safety, even in this world. The meek and quiet are
observed to live the most easy lives, compared with the froward and
turbulent. Or, They shall inherit the land (so it may be read), the land
of Canaan, a type of heaven. So that all the blessedness of heaven
above, and all the blessings of earth beneath, are the portion of the
meek.

`IV.` They that hunger and thirst after righteousness are happy, v. 6.
Some understand this as a further instance of our outward poverty, and a
low condition in this world, which not only exposes men to injury and
wrong, but makes it in vain for them to seek to have justice done to
them; they hunger and thirst after it, but such is the power on the side
of their oppressors, that they cannot have it; they desire only that
which is just and equal, but it is denied them by those that neither
fear God nor regard men. This is a melancholy case! Yet, blessed are
they, if they suffer these hardships for and with a good conscience; let
them hope in God, who will see justice done, right take place, and will
deliver the poor from their oppressors, Ps. 103:6. Those who contentedly
bear oppression, and quietly refer themselves to God to plead their
cause, shall in due time be satisfied, abundantly satisfied, in the
wisdom and kindness which shall be manifested in his appearances for
them. But it is certainly to be understood spiritually, of such a desire
as, being terminated on such an object, is gracious, and the work of
God\'s grace in the soul, and qualifies for the gifts of the divine
favour. 1. Righteousness is here put for all spiritual blessings. See
Ps. 24:5; ch. 6:33. They are purchased for us by the righteousness of
Christ; conveyed and secured by the imputation of that righteousness to
us; and confirmed by the faithfulness of God. To have Christ made of God
to us righteousness, and to be made the righteousness of God in him; to
have the whole man renewed in righteousness, so as to become a new man,
and to bear the image of God; to have an interest in Christ and the
promises-this is righteousness. 2. These we must hunger and thirst
after. We must truly and really desire them, as one who is hungry and
thirsty desires meat and drink, who cannot be satisfied with any thing
but meat and drink, and will be satisfied with them, though other things
be wanting. Our desires of spiritual blessings must be earnest and
importunate; \"Give me these, or else I die; every thing else is dross
and chaff, unsatisfying; give me these, and I have enough, though I had
nothing else.\" Hunger and thirst are appetites that return frequently,
and call for fresh satisfactions; so these holy desires rest not in any
thing attained, but are carried out toward renewed pardons, and daily
fresh supplies of grace. The quickened soul calls for constant meals of
righteousness, grace to do the work of every day in its day, as duly as
the living body calls for food. Those who hunger and thirst will labour
for supplies; so we must not only desire spiritual blessings, but take
pains for them in the use of the appointed means. Dr. Hammond, in his
practical Catechism, distinguishes between hunger and thirst. Hunger is
a desire of food to sustain, such as sanctifying righteousness. Thirst
is the desire of drink to refresh, such as justifying righteousness, and
the sense of our pardon.

Those who hunger and thirst after spiritual blessings, are blessed in
those desires, and shall be filled with those blessings. `(1.)` They are
blessed in those desires. Though all desires of grace are not grace
(feigned, faint desires are not), yet such a desire as this is; it is an
evidence of something good, and an earnest of something better. It is a
desire of God\'s own raising, and he will not forsake the work of his
own hands. Something or other the soul will be hungering and thirsting
after; therefore they are blessed who fasten upon the right object,
which is satisfying, and not deceiving; and do not pant after the dust
of the earth, Amos 2:7; Isa. 55:2. `(2.)` They shall be filled with those
blessings. God will give them what they desire to complete their
satisfaction. It is God only who can fill a soul, whose grace and favour
are adequate to its just desires; and he will fill those with grace for
grace, who, in a sense of their own emptiness, have recourse to his
fulness. He fills the hungry (Lu. 1:53), satiates them, Jer. 31:25. The
happiness of heaven will certainly fill the soul; their righteousness
shall be complete, the favour of God and his image, both in their full
perfection.

`V.` The merciful are happy, v. 7. This, like the rest, is a paradox; for
the merciful are not taken to be the wisest, nor are likely to be the
richest; yet Christ pronounces them blessed. Those are the merciful, who
are piously and charitably inclined to pity, help, and succour persons
in misery. A man may be truly merciful, who has not wherewithal to be
bountiful or liberal; and then God accepts the willing mind. We must not
only bear our own afflictions patiently, but we must, by Christian
sympathy, partake of the afflictions of our brethren; pity must be shown
(Job 6:14), and bowels of mercy put on (Col. 3:12); and, being put on,
they must put forth themselves in contributing all we can for the
assistance of those who are any way in misery. We must have compassion
on the souls of others, and help them; pity the ignorant, and instruct
them; the careless, and warn them; those who are in a state of sin, and
snatch them as brands out of the burning. We must have compassion on
those who are melancholy and in sorrow, and comfort them (Job 16:5); on
those whom we have advantage against, and not be rigorous and severe
with them; on those who are in want, and supply them; which if we refuse
to do, whatever we pretend, we shut up the bowels of our compassion,
James 2:15, 16; 1 Jn. 3:17. Draw out they soul by dealing thy bread to
the hungry, Isa. 58:7, 10. Nay, a good man is merciful to his beast.

Now as to the merciful. 1. They are blessed; so it was said in the Old
Testament; Blessed is he that considers the poor, Ps. 41:1. Herein they
resemble God, whose goodness is his glory; in being merciful as he is
merciful, we are, in our measure, perfect as he is perfect. It is an
evidence of love to God; it will be a satisfaction to ourselves, to be
any way instrumental for the benefit of others. One of the purest and
most refined delights in this world, is that of doing good. In this
word, Blessed are the merciful, is included that saying of Christ, which
otherwise we find not in the gospels, It is more blessed to give than to
receive, Acts 20:35. 2. They shall obtain mercy; mercy with men, when
they need it; he that watereth, shall be watered also himself (we know
not how soon we may stand in need of kindness, and therefore should be
kind); but especially mercy with God, for with the merciful he will show
himself merciful, Ps. 18:25. The most merciful and charitable cannot
pretend to merit, but must fly to mercy. The merciful shall find with
God sparing mercy (ch. 6:14), supplying mercy (Prov. 19:17), sustaining
mercy (Ps. 41:2), mercy in that day (2 Tim. 1:18); may, they shall
inherit the kingdom prepared for them (ch. 25:34, 35); whereas they
shall have judgment without mercy (which can be nothing short of
hell-fire) who have shown no mercy.

`VI.` The pure in heart are happy (v. 8); Blessed are the poor in heart,
for they shall see God. This is the most comprehensive of all the
beatitudes; here holiness and happiness ar fully described and put
together.

`1.` Here is the most comprehensive character of the blessed: they are
pure in heart. Note, True religion consists in heart-purity. Those who
are inwardly pure, show themselves to be under the power of pure and
undefiled religion. True Christianity lies in the heart, in the purity
of heart; the washing of that from wickedness, Jer. 4:14. We must lift
up to God, not only clean hands, but a pure heart, Ps. 24:4, 5; 1 Tim.
1:5. The heart must be pure, in opposition to mixture-an honest heart
that aims well; and pure, in opposition to pollution and defilement; as
wine unmixed, as water unmuddied. The heart must be kept pure from
fleshly lusts, all unchaste thoughts and desires; and from worldly
lusts; covetousness is called filthy lucre; from all filthiness of flesh
and spirit, all that which come out of the heart, and defiles the man.
The heart must be purified by faith, and entire for God; must be
presented and preserved a chaste virgin to Christ. Create in me such a
clean heart, O God!

`2.` Here is the most comprehensive comfort of the blessed; They shall
see God. Note, `(1.)` It is the perfection of the soul\'s happiness to see
God; seeing him, as we may by faith in our present state, is a heaven
upon earth; and seeing him as we shall in the future state, in the
heaven of heaven. To see him as he is, face to face, and no longer
through a glass darkly; to see him as ours, and to see him and enjoy
him; to see him and be like him, and be satisfied with that likeness
(Ps. 17:15); and to see him for ever, and never lose the sight of him;
this is heaven\'s happiness. `(2.)` The happiness of seeing God is
promised to those, and those only, who are pure in heart. None but the
pure are capable of seeing God, nor would it be a felicity to the
impure. What pleasure could an unsanctified soul take in the vision of a
holy God? As he cannot endure to look upon their iniquity, so they
cannot endure to look upon his purity; nor shall any unclean thing enter
into the new Jerusalem; but all that are pure in heart, all that are
truly sanctified, have desires wrought in them, which nothing but the
sight of God will sanctify; and divine grace will not leave those
desires unsatisfied.

`VII.` The peace-makers are happy, v. 9. The wisdom that is from above is
first pure, and then peaceable; the blessed ones are pure toward God,
and peaceable toward men; for with reference to both, conscience must be
kept void of offence. The peace-makers are those who have, 1. A
peaceable disposition: as, to make a lie, is to be given and addicted to
lying, so, to make peace, is to have a strong and hearty affection to
peace. I am for peace, Ps. 120:7. It is to love, and desire, and delight
in peace; to be put in it as in our element, and to study to be quiet.
2. A peaceable conversation; industriously, as far as we can, to
preserve the peace that it be not broken, and to recover it when it is
broken; to hearken to proposals of peace ourselves, and to be ready to
make them to others; where distance is among brethren and neighbours, to
do all we can to accommodate it, and to be repairers of the breaches.
The making of peace is sometimes a thankless office, and it is the lot
of him who parts a fray, to have blows on both sides; yet it is a good
office, and we must be forward to it. Some think that this is intended
especially as a lesson for ministers, who should do all they can to
reconcile those who are at variance, and to promote Christian love among
those under their charge.

Now, `(1.)` Such persons are blessed; for they have the satisfaction of
enjoying themselves, by keeping the peace, and of being truly
serviceable to others, by disposing them to peace. They are working
together with Christ, who came into the world to slay all enmities, and
to proclaim peace on earth. `(2.)` They shall be called the children of
God; it will be an evidence to themselves that they are so; God will own
them as such, and herein they will resemble him. He is the God of peace;
the Son of God is the Prince of peace; the Spirit of adoption is a
Spirit of peace. Since God has declared himself reconcilable to us all,
he will not own those for his children who are implacable in their
enmity to one another; for if the peacemakers are blessed, woe to the
peace-breakers! Now by this it appears, that Christ never intended to
have his religion propagated by fire and sword, or penal laws, or to
acknowledge bigotry, or intemperate zeal, as the mark of his disciples.
The children of this world love to fish in troubled waters, but the
children of God are the peace-makers, the quiet in the land.

`VIII.` Those who are persecuted for righteousness\' sake, are happy.
This is the greatest paradox of all, and peculiar to Christianity; and
therefore it is put last, and more largely insisted upon than any of the
rest, v. 10-12. This beatitude, like Pharaoh\'s dream, is doubled,
because hardly credited, and yet the thing is certain; and in the latter
part there is change of the person, \"Blessed are ye-ye my disciples,
and immediate followers. This is that which you, who excel in virtue,
are more immediately concerned in; for you must reckon upon hardships
and troubles more than other men.\" Observe here,

`1.` The case of suffering saints described; and it is a hard case, and a
very piteous one.

`(1.)` They are persecuted, hunted, pursued, run down, as noxious beasts
are, that are sought for to be destroyed; as if a Christian did caput
gerere lupinum-bear a wolf\'s head, as an outlaw is said to do-any one
that finds him may slay him; they are abandoned as the offscouring of
all things; fined, imprisoned, banished, stripped of their estates,
excluded from all places of profit and trust, scourged, racked,
tortured, always delivered to death, and accounted as sheep for the
slaughter. This has been the effect of the enmity of the serpent\'s seed
against the holy seed, ever since the time of righteous Abel. It was so
in Old-Testament times, as we find, Heb. 11:35, etc. Christ has told us
that it would much more be so with the Christian church, and we are not
to think it strange, 1 Jn. 3:13. He has left us an example.

`(2.)` The are reviled, and have all manner of evil said against them
falsely. Nicknames, and names of reproach, are fastened upon them, upon
particular persons, and upon the generation of the righteous in the
gross, to render them odious; sometimes to make them formidable, that
they may be powerfully assailed; things are laid to their charge that
they knew not, Ps. 35:11; Jer. 20:18; Acts 17:6, 7. Those who have had
no power in their hands to do them any other mischief, could yet do
this; and those who have had power to persecute, had found it necessary
to do this too, to justify themselves in their barbarous usage of them;
they could not have baited them, if they had not dressed them in
bear-skins; nor have given them the worst of treatment, if they had not
first represented them as the worst of men. They will revile you, and
persecute you. Note, Reviling the saints is persecuting them, and will
be found so shortly, when hard speeches must be accounted for (Jude 15),
and cruel mockings, Heb. 11:36. They will say all manner of evil of you
falsely; sometimes before the seat of judgment, as witnesses; sometimes
in the seat of the scornful, with hypocritical mockers at feasts; they
are the song of the drunkards; sometimes to face their faces, as Shimei
cursed David; sometimes behind their backs, as the enemies of Jeremiah
did. Note, There is no evil so black and horrid, which, at one time or
other, has not been said, falsely, of Christ\'s disciples and followers.

`(3.)` All this is for righteousness\' sake (v. 10); for my sake, v. 11.
If for righteousness\' sake, then for Christ\'s sake, for he is nearly
interested in the work of righteousness. Enemies to righteousness are
enemies to Christ. This precludes those from the blessedness who suffer
justly, and are evil spoken of truly for their real crimes; let such be
ashamed and confounded, it is part of their punishment; it is not the
suffering, but the cause, that makes the martyr. Those suffer for
righteousness\' sake, who suffer because they will not sin against their
consciences, and who suffer for doing that which is good. Whatever
pretence persecutors have, it is the power of godliness that they have
an enmity to; it is really Christ and his righteousness that are
maligned, hated, and persecuted; For thy sake I have borne reproach, Ps.
69:9; Rom. 8:36.

`2.` The comforts of suffering saints laid down.

`(1.)` They are blessed; for they now, in their life-time, receive their
evil things (Lu. 16:25), and receive them upon a good account. They are
blessed; for it is an honour to them (Acts 5:41); it is an opportunity
of glorifying Christ, of doing good, and of experiencing special
comforts and visits of grace and tokens of his presence, 2 Co. 1:5; Dan.
3:25; Rom. 8:29.

`(2.)` They shall be recompensed; Theirs is the kingdom of heaven. They
have at present a sure title to it, and sweet foretastes of it; and
shall ere long be in possession of it. Though there be nothing in those
sufferings than can, in strictness, merit of God (for the sins of the
best deserve the worst), yet this is here promised as a reward (v. 12);
Great is your reward in heaven: so great, as far to transcend the
service. It is in heaven, future, and out of sight; but well secured,
out of the reach of chance, fraud, and violence. Note, God will provide
that those who lose for him, though it be life itself, shall not lose by
him in the end. Heaven, at last, will be an abundant recompence for all
the difficulties we meet with in our way. This is that which has borne
up the suffering saints in all ages-this joy set before them.

`(3.)` \"So persecuted they the prophets that were before you, v. 12. They
were before you in excellency, above what you are yet arrived at; they
were before you in time, that they might be examples to you of suffering
affliction and of patience, James 5:10. They were in like manner
persecuted and abused; and can you expect to go to heaven in a way by
yourself? Was not Isaiah mocked for his line upon line? Elisha for his
bald head? Were not all the prophets thus treated? Therefore marvel not
at it as a strange thing, murmur not at it as a hard thing; it is a
comfort to see the way of suffering a beaten road, and an honour to
follow such leaders. That grace which was sufficient for them, to carry
them through their sufferings, shall not be deficient to you. Those who
are your enemies are the seed and successors of them who of old mocked
the messengers of the Lord,\" 2 Chr. 36:16; ch. 23:31; Acts 7:52.

### Verses 13-16

Christ had lately called his disciples, and told them that they should
be fishers of men; here he tells them further what he designed them to
be- the salt of the earth, and lights of the world, that they might be
indeed what it was expected they should be.

`I.` Ye are the salt of the earth. This would encourage and support them
under their sufferings, that, though they should be treated with
contempt, yet they should really be blessings to the world, and the more
so for their suffering thus. The prophets, who went before them, were
the salt of the land of Canaan; but the apostles were the salt of the
whole earth, for they must go into all the world to preach the gospel.
It was a discouragement to them that they were so few and so weak. What
could they do in so large a province as the whole earth? Nothing, if
they were to work by force of arms and dint of sword; but, being to work
silent as salt, one handful of that salt would diffuse its savour far
and wide; would go a great way, and work insensibly and irresistibly as
leaven, ch. 13:33. The doctrine of the gospel is as salt; it is
penetrating, quick, and powerful (Heb. 4:12); it reaches the heart Acts
2:37. It is cleansing, it is relishing, and preserves from putrefaction.
We read of the savour of the knowledge of Christ (2 Co. 2:14); for all
other learning is insipid without that. An everlasting covenant is
called a covenant of salt (Num. 18:19); and the gospel is an everlasting
gospel. Salt was required in all the sacrifices (Lev. 2:13), in
Ezekiel\'s mystical temple, Eze. 43:24. Now Christ\'s disciples having
themselves learned the doctrine of the gospel, and being employed to
teach it to others, were as salt. Note, Christians, and especially
ministers, are the salt of the earth.

`1.` If they be as they should be they are as good salt, white, and
small, and broken into many grains, but very useful and necessary. Pliny
says, Sine sale, vita humana non potest degere-Without salt human life
cannot be sustained. See in this, `(1.)` What they are to be in
themselves-seasoned with the gospel, with the salt of grace; thoughts
and affections, words and actions, all seasoned with grace, Col. 4:6.
Have salt in yourselves, else you cannot diffuse it among others, Mk.
9:50. `(2.)` What they are to be to others; they must not only be good but
do good, must insinuate themselves into the minds of the people, not to
serve any secular interest of their own, but that they might transform
them into the taste and relish of the gospel. `(3.)` What great blessings
they are to the world. Mankind, lying in ignorance and wickedness, were
a vast heap of unsavoury stuff, ready to putrefy; but Christ sent forth
his disciples, by their lives and doctrines, to season it with knowledge
and grace, and so to render it acceptable to God, to the angels, and to
all that relish divine things. `(4.)` How they must expect to be disposed
of. They must not be laid on a heap, must not continue always together
at Jerusalem, but must be scattered as salt upon the meat, here a grain
and there a grain; as the Levites were dispersed in Israel, that,
wherever they live, they may communicate their savour. Some have
observed, that whereas it is foolishly called an ill omen to have the
salt fall towards us, it is really an ill omen to have the salt fall
from us.

`2.` If they be not, they are as salt that has lost its savour. If you,
who should season others, are yourselves unsavoury, void of spiritual
life, relish, and vigour; if a Christian be so, especially if a minister
be so, his condition is very sad; for, `(1.)` He is irrecoverable:
Wherewith shall it be salted? Salt is a remedy for unsavoury meat, but
there is no remedy for unsavoury salt. Christianity will give a man a
relish; but if a man can take up and continue the profession of it, and
yet remain flat and foolish, and graceless and insipid, no other
doctrine, no other means, can be applied, to make him savoury. If
Christianity do not do it, nothing will. `(2.)` He is unprofitable: It is
thenceforth good for nothing; what use can it be put to, in which it
will not do more hurt than good? As a man without reason, so is a
Christian without grace. A wicked man is the worst of creatures; a
wicked Christian is the worst of men; and a wicked minister is the worst
of Christians. `(3.)` He is doomed to ruin and rejection; He shall be cast
out-expelled the church and the communion of the faithful, to which he
is a blot and a burden; and he shall be trodden under foot of men. Let
God be glorified in the shame and rejection of those by whom he has been
reproached, and who have made themselves fit for nothing but to be
trampled upon.

`II.` Ye are the light of the world, v. 14. This also bespeaks them
useful, as the former (Sole et sale nihil utilius-Nothing more useful
than the sun and salt), but more glorious. All Christians are light in
the Lord (Eph. 5:8), and must shine as lights (Phil. 2:15), but
ministers in a special manner. Christ call himself the Light of the
world (Jn. 8:12), and they are workers together with him, and have some
of his honour put upon them. Truly the light is sweet, it is welcome;
the light of the first day of the world was so, when it shone out of
darkness; so is the morning light of every day; so is the gospel, and
those that spread it, to all sensible people. The world sat in darkness,
Christ raised up his disciples to shine in it; and, that they may do so,
from him they borrow and derive their light.

This similitude is here explained in two things:

`1.` As the lights of the world, they are illustrious and conspicuous,
and have many eyes upon them. A city that is set on a hill cannot be
hid. The disciples of Christ, especially those who are forward and
zealous in his service, become remarkable, and are taken notice of as
beacons. They are for signs (Isa. 7:18), men wondered at (Zec. 3:8); all
their neighbours have any eye upon them. Some admire them, commend them,
rejoice in them, and study to imitate them; others envy them, hate them,
censure them, and study to blast them. They are concerned therefore to
walk circumspectly, because of their observers; they are as spectacles
to the world, and must take heed of every thing that looks ill, because
they are so much looked at. The disciples of Christ were obscure men
before he called them, but the character he put upon them dignified
them, and as preachers of the gospel they made a figure; and though they
were reproached for it by some, they were respected for it by others,
advanced to thrones, and made judges (Lu. 22:30); for Christ will honour
those that honour him.

`2.` As the lights of the world, they are intended to illuminate and give
light to others (v. 15), and therefore, `(1.)` They shall be set up as
lights. Christ has lighted these candles, they shall not be put under a
bushel, not confined always, as they are now, to the cities of Galilee,
or the lost sheep of the house of Israel, but they shall be sent into
all the world. The churches are the candlesticks, the golden
candlesticks, in which these lights are placed, that they light may be
diffused; and the gospel is so strong a light, and carries with it so
much of its own evidence, that, like a city on a hill, it cannot be hid,
it cannot but appear to be from God, to all those who do not wilfully
shut their eyes against it. It will give light to all that are in the
house, to all that will draw near to it, and come where it is. Those to
whom it does not give light, must thank themselves; they will not be in
the house with it; will not make a diligent and impartial enquiry into
it, but are prejudiced against it. `(2.)` They must shine as lights,
`[1.]` By their good preaching. The knowledge they have, they must
communicate for the good of others; not put it under a bushel, but
spread it. The talent must not be buried in a napkin, but traded with.
The disciples of Christ must not muffle themselves up in privacy and
obscurity, under pretence of contemplation, modesty, or
self-preservation, but, as they have received the gift, must minister
the same, Lu. 12:3. `[2.]` By their good living. They must be burning
and shining lights (Jn. 5:35); must evidence, in their whole
conversation, that they are indeed followers of Christ, James 3:13. They
must be to others for instruction, direction, quickening, and comfort,
Job 29:11.

See here, First, How our light must shine-by doing such good works as
men may see, and may approve of; such works as are of good report among
them that are without, and as will therefore give them cause to think
well of Christianity. We must do good works that may be seen to the
edification of others, but not that they may be seen to our own
ostentation; we are bid to pray in secret, and what lies between God and
our souls, must be kept to ourselves; but that which is of itself open
and obvious to the sight of men, we must study to make congruous to our
profession, and praiseworthy, Phil. 4:8. Those about us must not only
hear our good words, but see our good works; that they may be convinced
that religion is more than a bare name, and that we do not only make a
profession of it, but abide under the power of it.

Secondly, For what end our light must shine-\"That those who see your
good works may be brought, not to glorify you (which was the things the
Pharisees aimed at, and it spoiled all their performances), but to
glorify your Father which is in heaven.\" Note, The glory of God is the
great thing we must aim at in every thing we do in religion, 1 Pt. 4:11.
In this centre the lines of all our actions must meet. We must not only
endeavor to glorify God ourselves, but we must do all we can to bring
others to glorify him. The sight of our good works will do this, by
furnishing them, 1. With matter for praise. \"Let them see your good
works, that they may see the power of God\'s grace in you, and may thank
him for it, and give him the glory of it, who has given such power unto
men.\" 2. With motives of piety. \"Let them see your good works, that
they may be convinced of the truth and excellency of the Christian
religion, may be provoked by a holy emulation to imitate your good
works, and so may glorify God.\" Note, The holy, regular, and exemplary
conversation of the saints, may do much towards the conversion of
sinners; those who are unacquainted with religion, may hereby be brought
to know what it is. Examples teach. And those who are prejudiced against
it, may hereby by brought in love with it, and thus there is a winning
virtue in a godly conversation.

### Verses 17-20

Those to whom Christ preached, and for whose use he gave these
instructions to his disciples, were such as in their religion had an
eye, 1. To the scriptures of the Old Testament as their rule, and
therein Christ here shows them they were in the right: 2. To the scribes
and the Pharisees as their example, and therein Christ here shows them
they were in the wrong; for,

`I.` The rule which Christ came to establish exactly agreed with the
scriptures of the Old Testament, here called the law and the prophets.
The prophets were commentators upon the law, and both together made up
that rule of faith and practice which Christ found upon the throne in
the Jewish church, and here he keeps it on the throne.

`1.` He protests against the thought of cancelling and weakening the Old
Testament; Think not that I am come to destroy the law and the prophets.
`(1.)` \"Let not the pious Jews, who have an affection for the law and the
prophets, fear that I come to destroy them.\" Let them be not prejudiced
against Christ and his doctrine, from a jealousy that this kingdom he
came to set up, would derogate from the honour of the scriptures, which
they had embraced as coming from God, and of which they had experienced
the power and purity; no, let them be satisfied that Christ has no ill
design upon the law and the prophets. \"Let not the profane Jews, who
have a disaffection to the law and the prophets, and are weary of that
yoke, hope that I am come to destroy them.\" Let not carnal libertines
imagine that the Messiah is come to discharge them from the obligation
of divine precepts and yet to secure to them divine promises, to make
the happy and yet to give them leave to live as they list. Christ
commands nothing now which was forbidden either by the law of nature or
the moral law, nor forbids any thing which those laws had enjoined; it
is a great mistake to think he does, and he here takes care to rectify
the mistake; I am not come to destroy. The Saviour of souls is the
destroyer of nothing but the works of the devil, of nothing that comes
from God, much less of those excellent dictates which we have from Moses
and the prophets. No, he came to fulfil them. That is, `[1.]` To obey
the commands of the law, for he was made under the law, Gal. 4:4. He in
all respects yielded obedience to the law, honoured his parents,
sanctified the sabbath, prayed, gave alms, and did that which never any
one else did, obeyed perfectly, and never broke the law in any thing.
`[2.]` To make good the promises of the law, and the predictions of the
prophets, which did all bear witness to him. The covenant of grace is,
for substance, the same now that it was then, and Christ the Mediator of
it. `[3.]` To answer the types of the law; thus (as bishop Tillotson
expresses it), he did not make void, but make good, the ceremonial law,
and manifested himself to be the Substance of all those shadows. `[4.]`
To fill up the defects of it, and so to complete and perfect it. Thus
the word pleµroµsai properly signifies. If we consider the law as a
vessel that had some water in it before, he did not come to pour out the
water, but to fill the vessel up to the brim; or, as a picture that is
first rough-drawn, displays some outlines only of the piece intended,
which are afterwards filled up; so Christ made an improvement of the law
and the prophets by his additions and explications. `[5.]` To carry on
the same design; the Christian institutes are so far from thwarting and
contradicting that which was the main design of the Jewish religion,
that they promote it to the highest degree. The gospel is the time of
reformation (Heb. 9:10), not the repeal of the law, but the amendment of
it, and, consequently, its establishment.

`2.` He asserts the perpetuity of it; that not only he designed not the
abrogation of it, but that it never should be abrogated (v. 18);
\"Verily I say unto you, I, the Amen, the faithful Witness, solemnly
declare it, that till heaven and earth pass, when time shall be no more,
and the unchangeable state of recompences shall supersede all laws, one
jot, or one tittle, the least and most minute circumstance, shall in no
wise pass from the law till all be fulfilled;\" for what is it that God
is doing in all the operations both of providence and grace, but
fulfilling the scripture? Heaven and earth shall come together, and all
the fulness thereof be wrapped up in ruin and confusion, rather than any
word of God shall fall to the ground, or be in vain. The word of the
Lord endures for ever, both that of the law, and that of the gospel.
Observe, The care of God concerning his law extends itself even to those
things that seem to be of least account in it, the iotas and the
tittles; for whatever belongs to God, and bears his stamp, be it ever so
little, shall be preserved. The laws of men are conscious to themselves
of so much imperfection, that they allow it for a maxim, Apices juris
non sunt jura-The extreme points of the law are not the law, but God
will stand by and maintain every iota and every tittle of his law.

`3.` He gives it in charge to his disciples, carefully to preserve the
law, and shows them the danger of the neglect and contempt of it (v.
19); Whosoever therefore shall break one of the least commandments of
the law of Moses, much more any of the greater, as the Pharisees did,
who neglected the weightier matters of the law, and shall teach men so
as they did, who made void the commandment of God with their traditions
(ch. 15:3), he shall be called the least in the kingdom of heaven.
Though the Pharisees be cried up for such teachers as should be, they
shall not be employed as teachers in Christ\'s kingdom; but whosoever
shall do and teach them, as Christ\'s disciples would, and thereby prove
themselves better friends to the Old Testament than the Pharisees were,
they, though despised by men, shall be called great in the kingdom of
heaven. Note, `(1.)` Among the commands of God there are some less than
others; none absolutely little, but comparatively so. The Jews reckon
the least of the commandments of the law to be that of the bird\'s nest
(Deu. 22:6, 7); yet even that had a significance and an intention very
great and considerable. `(2.)` It is a dangerous thing, in doctrine or
practice, to disannul the least of God\'s commands; to break them, that
is, to go about either to contract the extent, or weaken the obligation
of them; whoever does so, will find it is at his peril. Thus to vacate
any of the ten commandments, is too bold a stroke for the jealous God to
pass by. it is something more than transgressing the law, it is making
void the law, Ps. 119:126. `(3.)` That the further such corruptions as
they spread, the worse they are. It is impudence enough to break the
command, but is a greater degree of it to teach men so. This plainly
refers to those who at this time sat in Moses\' seat, and by their
comments corrupted and perverted the text. Opinions that tend to the
destruction of serious godliness and the vitals of religion, by corrupt
glosses on the scripture, are bad when they are held, but worse when
they are propagated and taught, as the word of God. He that does so,
shall be called least in the kingdom of heaven, in the kingdom of glory;
he shall never come thither, but be eternally excluded; or, rather, in
the kingdom of the gospel-church. He is so far from deserving the
dignity of a teacher in it, that he shall not so much as be accounted a
member of it. The prophet that teaches these lies shall be the tail in
that kingdom (Isa. 9:15); when truth shall appear in its own evidence,
such corrupt teachers, though cried up as the Pharisees, shall be of no
account with the wise and good. Nothing makes ministers more
contemptible and base than corrupting the law, Mal. 2:8, 11. Those who
extenuate and encourage sin, and discountenance and put contempt upon
strictness in religion and serious devotion, are the dregs of the
church. But, on the other hand, Those are truly honourable, and of great
account in the church of Christ, who lay out themselves by their life
and doctrine to promote the purity and strictness of practical religion;
who both do and teach that which is good; for those who do not as they
teach, pull down with one hand what they build up with the other, and
give themselves the lie, and tempt men to think that all religion is a
delusion; but those who speak from experience, who live up to what they
preach, are truly great; they honour God, and God will honour them (1
Sa. 2:30), and hereafter they shall shine as the stars in the kingdom of
our Father.

`II.` The righteousness which Christ came to establish by this rule, must
exceed that of the scribes and Pharisees, v. 20. This was strange
doctrine to those who looked upon the scribes and Pharisees as having
arrived at the highest pitch of religion. The scribes were the most
noted teachers of the law, and the Pharisees the most celebrated
professors of it, and they both sat in Moses\' chair (ch. 23:2), and had
such a reputation among the people, that they were looked upon as
super-conformable to the law, and people did not think themselves
obliged to be as good as they; it was therefore a great surprise to
them, to hear that they must be better than they, or they should not go
to heaven; and therefore Christ here avers it with solemnity; I say unto
you, It is so. The scribes and Pharisees were enemies to Christ and his
doctrine, and were great oppressors; and yet it must be owned, that
there was something commendable in them. They were much in fasting and
prayer, and giving of alms; they were punctual in observing the
ceremonial appointments, and made it their business to teach others;
they had such an interest in the people that they ought, if but two men
went to heaven, one would be a Pharisee; and yet our Lord Jesus here
tells his disciples, that the religion he came to establish, did not
only exclude the badness, but excel the goodness, of the scribes and
Pharisees. We must do more than they, and better than they, or we shall
come short of heaven. They were partial in the law, and laid most stress
upon the ritual part of it; but we must be universal, and not think it
enough to give the priest his tithe, but must give God our hearts. They
minded only the outside, but we must make conscience of inside
godliness. They aimed at the praise and applause of men, but we must aim
at acceptance with God: they were proud of what they did in religion,
and trusted to it as a righteousness; but we, when we have done all,
must deny ourselves, and say, We are unprofitable servants, and trust
only to the righteousness of Christ; and thus we may go beyond the
scribes and Pharisees.

### Verses 21-26

Christ having laid down these principles, that Moses and the prophets
were still to be their rulers, but that the scribes and Pharisees were
to be no longer their rulers, proceeds to expound the law in some
particular instances, and to vindicate it from the corrupt glosses which
those expositors had put upon it. He adds not any thing new, only limits
and restrains some permissions which had been abused: and as to the
precepts, shows the breadth, strictness, and spiritual nature of them,
adding such explanatory statutes as made them more clear, and tended
much toward the perfecting of our obedience to them. In these verses, he
explains the law of the sixth commandment, according to the true intent
and full extent of it.

`I.` Here is the command itself laid down (v. 12); We have heard it, and
remember it; he speaks to them who know the law, who had Moses read to
them in their synagogues every sabbath-day; you have heard that it was
said by them, or rather as it is in the margin, to them of old time, to
your forefathers the Jews, Thou shalt not kill. Note, The laws of God
are not novel, upstart laws, but were delivered to them of old time;
they are ancient laws, but of that nature as never to be antiquated nor
grow obsolete. The moral law agrees with the law of nature, and the
eternal rules and reasons of good and evil, that is, the rectitude of
the eternal Mind. Killing is here forbidden, killing ourselves, killing
any other, directly or indirectly, or being any way accessory to it. The
law of God, the God of life, is a hedge of protection about our lives.
It was one of the precepts of Noah, Gen. 9:5, 6.

`II.` The exposition of this command which the Jewish teachers contended
themselves with; their comment upon it was, Whosoever shall kill, shall
be in danger of the judgment. This was all they had to say upon it, that
wilful murderers were liable to the sword of justice, and casual ones to
the judgment of the city of refuge. The courts of judgment sat in the
gate of their principal cities; the judges, ordinarily, were in number
twenty-three; these tried, condemned, and executed murderers; so that
whoever killed, was in danger of their judgment. Now this gloss of
theirs upon this commandment was faulty, for it intimated, 1. That the
law of the sixth commandment was only external, and forbade no more than
the act of murder, and laid to restraint upon the inward lusts, from
which wars and fightings come. This was indeed the proµton pseudos-the
fundamental error of the Jewish teachers, that the divine law prohibited
only the sinful act, not the sinful thought; they were disposed haerere
in cortice-to rest in the letter of the law, and they never enquired
into the spiritual meaning of it. Paul, while a Pharisee, did not, till,
by the key of the tenth commandment, divine grace let him into the
knowledge of the spiritual nature of all the rest, Rom. 7:7, 14. 2.
Another mistake of theirs was, that this law was merely political and
municipal, given for them, and intended as a directory for their courts,
and no more; as if they only were the people, and the wisdom of the law
must die with them.

`III.` The exposition which Christ gave of this commandment; and we are
sure that according to his exposition of it we must be judged hereafter,
and therefore ought to be ruled now. The commandment is exceeding broad,
and not to be limited by the will of the flesh, or the will of men.

`1.` Christ tells them that rash anger is heart-murder (v. 22); Whosoever
is angry with his brother without a cause, breaks the sixth commandment.
By our brother here, we are to understand any person, though ever so
much our inferior, as a child, a servant, for we are all made of one
blood. Anger is a natural passion; there are cases in which it is lawful
and laudable; but it is then sinful, when we are angry without cause.
The word is eikeµ, which signifies, sine causâ, sine effectu, et sine
modo-without cause, without any good effect, without moderation; so that
the anger is then sinful, `(1.)` When it is without any just provocation
given; either for no cause, or no good cause, or no great and
proportionable cause; when we are angry at children or servants for that
which could not be helped, which was only a piece of forgetfulness or
mistake, that we ourselves might easily have been guilty of, and for
which we should not have been angry at ourselves; when we are angry upon
groundless surmises, or for trivial affronts not worth speaking of. `(2.)`
When it is without any good end aimed at, merely to show our authority,
to gratify a brutish passion, to let people know our resentments, and
excite ourselves to revenge, then it is in vain, it is to do hurt;
whereas if we are at any time angry, it should be to awaken the offender
to repentance, and prevent his doing so again; to clear ourselves (2 Co.
7:11), and to give warning to others. `(3.)` When it exceeds due bounds;
when we are hardy and headstrong in our anger, violent and vehement,
outrageous and mischievous, and when we seek the hurt of those we are
displeased at. This is a breach of the sixth commandment, for he that is
thus angry, would kill if he could and durst; he has taken the first
step toward it; Cain\'s killing his brother began in anger; he is a
murderer in the account of God, who knows his heart, whence murder
proceeds, ch. 15:19.

`2.` He tells them, that given opprobrious language to our brother is
tongue-murder, calling him, Raca, and, Thou fool. When this is done with
mildness and for a good end, to convince others of their vanity and
folly, it is not sinful. Thus James says, O vain man; and Paul, Thou
fool; and Christ himself, O fools, and slow of heart. But when it
proceeds from anger and malice within, it is the smoke of that fire
which is kindled from hell, and falls under the same character. `(1.)`
Raca is a scornful word, and comes from pride, \"Thou empty fellow;\" it
is the language of that which Solomon calls proud wrath (Prov. 21:24),
which tramples upon our brother-disdains to set him even with the dogs
of our flock. This people who knoweth not the law, is cursed, is such
language, Jn. 7:49. `(2.)` Thou fool, is a spiteful word, and comes from
hatred; looking upon him, not only as mean and not to be honoured, but
as vile and not to be loved; \"Thou wicked man, thou reprobate.\" The
former speaks a man without sense, this (in scripture language) speaks a
man without grace; the more the reproach touches his spiritual
condition, the worse it is; the former is a haughty taunting of our
brother, this is a malicious censuring and condemning of him, as
abandoned of God. Now this is a breach of the sixth commandment;
malicious slanders and censures are poison under the tongue, that kills
secretly and slowly; bitter words are as arrows that would suddenly (Ps.
64:3), or as a sword in the bones. The good name of our neighbour, which
is better than life, is thereby stabbed and murdered; and it is an
evidence of such an ill-will to our neighbour as would strike at his
life, if it were in our power.

`3.` He tells them, that how light soever they made of these sins, they
would certainly be reckoned for; he that is angry with is brother shall
be in danger of the judgment and anger of God; he that calls him Raca,
shall be in danger of the council, of being punished by the Sanhedrim
for reviling an Israelite; but whosoever saith, Thou fool, thou profane
person, thou child of hell, shall be in danger of hell-fire, to which he
condemns his brother; so the learned Dr. Whitby. Some think, in allusion
to the penalties used in the several courts of judgment among the Jews,
Christ shows that the sin of rash anger exposes men to lower or higher
punishments, according to the degrees of its proceeding. The Jews had
three capital punishments, each worse than the other; beheading, which
was inflicted by the judgment; stoning, by the council or chief
Sanhedrim; and burning in the valley of the son of Hinnom, which was
used only in extraordinary cases: it signifies, therefore, that rash
anger and reproachful language are damning sins; but some are more
sinful than others, and accordingly there is a greater damnation, and a
sorer punishment reserved for them: Christ would thus show which sin was
most sinful, by showing which it was the punishment whereof was most
dreadful.

`IV.` From all this it is here inferred, that we ought carefully to
preserve Christian love and peace with our brethren, and that if at any
time a breach happens, we should labour for a reconciliation, by
confessing our fault, humbling ourselves to our brother, begging his
pardon, and making restitution, or offering satisfaction for wrong done
in word or deed, according as the nature of the thing is; and that we
should do this quickly for two reasons:

`1.` Because, till this be done, we are utterly unfit for communion with
God in holy ordinances, v. 23, 24. The case supposed is, \"That thy
brother have somewhat against thee,\" that thou has injured and offended
him, either really or in his apprehension; if thou are the party
offended, there needs not this delay; if thou have aught against thy
brother, make short work of it; no more is to be done but to forgive him
(Mk. 11:25), and forgive the injury; but if the quarrel began on thy
side, and the fault was either at first or afterwards thine, so that thy
brother has a controversy with thee, go and be reconciled to him before
thou offer thy gift at the altar, before thou approach solemnly to God
in the gospel-services of prayer and praise, hearing the word or the
sacraments. Note, `(1.)` When we are addressing ourselves to any religious
exercises, it is good for us to take that occasion of serious reflection
and self-examination: there are many things to be remembered, when we
bring our gift to the altar, and this among the rest, whether our
brother hath aught against us; then, if ever, we are disposed to be
serious, and therefore should then call ourselves to an account. `(2.)`
Religious exercises are not acceptable to God, if they are performed
when we are in wrath; envy, malice, and uncharitableness, are sins so
displeasing to God, that nothing pleases him which comes from a heart
wherein they are predominant, 1 Tim. 2:8. Prayers made in wrath are
written in gall, Isa. 1:15; 58:4. `(3.)` Love or charity is so much better
than all burnt-offerings and sacrifice, that God will have
reconciliation made with an offended brother before the gift be offered;
he is content to stay for the gift, rather than have it offered while we
are under guilt and engaged in a quarrel. `(4.)` Though we are unfitted
for communion with God, by a continual quarrel with a brother, yet that
can be no excuse for the omission or neglect of our duty: \"Leave there
thy gift before the altar, lest otherwise, when thou has gone away, thou
be tempted not to come again.\" Many give this as a reason why they do
not come to church or to the communion, because they are at variance
with some neighbour; and whose fault is that? One sin will never excuse
another, but will rather double the guilt. Want of charity cannot
justify the want of piety. The difficulty is easily got over; those who
have wronged us, we must forgive; and those whom we have wronged, we
must make satisfaction to, or at least make a tender of it, and desire a
renewal of the friendship, so that if reconciliation be not made, it may
not be our fault; and then come, come and welcome, come and offer thy
gift, and it shall be accepted. Therefore we must not let the sun go
down upon our wrath any day, because we must go to prayer before we go
to sleep; much less let the sun rise upon our wrath on a sabbath-day,
because it is a day of prayer.

`2.` Because, till this be done, we lie exposed to much danger, v. 25,
26. It is at our peril if we do not labour after an agreement, and that
quickly, upon two accounts:

`(1.)` Upon a temporal account. If the offence we have done to our
brother, in his body, goods, or reputation, be such as will bear action,
in which he may recover considerable damages, it is our wisdom, and it
is our duty to our family, to prevent that by a humble submission and a
just and peaceable satisfaction; lest otherwise he recover it by law,
and put us to the extremity of a prison. In such a case it is better to
compound and make the best terms we can, than to stand it out; for it is
in vain to contend with the law, and there is danger of our being
crushed by it. Many ruin their estates by an obstinate persisting in the
offences they have given, which would soon have been pacified by a
little yielding at first. Solomon\'s advice in case of suretyship is,
Go, humble thyself, and so secure and deliver thyself, Prov. 6:1-5. It
is good to agree, for the law is costly. Though we must be merciful to
those we have advantage against, yet we must be just to those that have
advantage against us, as far as we are able. \"Agree, and compound with
thine adversary quickly, lest he be exasperated by thy stubbornness, and
provoked to insist upon the utmost demand, and will not make thee the
abatement which at first he would have made.\" A prison is an
uncomfortable place to those who are brought to it by their own pride
and prodigality, their own wilfulness and folly.

`(2.)` Upon a spiritual account. \"Go, and be reconciled to thy brother,
be just to him, be friendly with him, because while the quarrel
continues, as thou art unfit to bring thy gift to the altar, unfit to
come to the table of the Lord, so thou art unfit to die: if thou persist
in this sin, there is danger lest thou be suddenly snatched away by the
wrath of God, whose judgment thou canst not escape nor except against;
and if that iniquity be laid to thy charge, thou art undone for ever.\"
Hell is a prison for all that live and die in malice and
uncharitableness, for all that are contentious (Rom. 2:8), and out of
that prison there is no rescue, no redemption, no escape, to eternity.

This is very applicable to the great business of our reconciliation to
God through Christ; Agree with him quickly, whilst thou art in the way.
Note, `[1.]` The great God is an Adversary to all sinners, antidikos-a
law-adversary; he has a controversy with them, an action against them.
`[2.]` It is our concern to agree with him, to acquaint ourselves with
him, that we may be at peace, Job 22:21; 2 Co. 5:20. `[3.]` It is our
wisdom to do this quickly, while we are in the way. While we are alive,
we are in the way; after death, it will be too late to do it; therefore
give not sleep to thine eyes till it be done. `[4.]` They who continue
in a state of enmity to God, are continually exposed to the arrests of
his justice, and the most dreadful instances of his wrath. Christ is the
Judge, to whom impenitent sinners will be delivered; for all judgment is
committed to the Son; he that was rejected as a Saviour, cannot be
escaped as a Judge, Rev. 6:16, 17. It is a fearful thing to be thus
turned over to the Lord Jesus, when the Lamb shall become the Lion.
Angels are the officers to whom Christ will deliver them (ch. 13:41,
42); devils are so too, having the power of death as executioners to all
unbelievers, Heb. 2:14. Hell is the prison, into which those will be
cast that continue in a state of enmity to God, 2 Pt. 2:4. `[5.]` Damned
sinners must remain in it to eternity; they shall not depart till they
have paid the uttermost farthing, and that will not be to the utmost
ages of eternity: divine justice will be for ever in the satisfying, but
never satisfied.

### Verses 27-32

We have here an exposition of the seventh commandment, given us by the
same hand that made the law, and therefore was fittest to be the
interpreter of it: it is the law against uncleanness, which fitly
follows upon the former; that laid a restraint upon sinful passions,
this upon sinful appetites, both which ought always to be under the
government of reason and conscience, and if indulged, are equally
pernicious.

`I.` The command is here laid down (v. 27), Thou shalt not commit
adultery; which includes a prohibition of all other acts of uncleanness,
and the desire of them: but the Pharisees, in their expositions of this
command, made it to extend no further than the act of adultery,
suggesting, that if the iniquity was only regarded in the heart, and
went no further, God could not hear it, would not regard it (Ps. 66:18),
and therefore they thought it enough to be able to say that they were no
adulterers, Lu. 18:11.

`II.` It is here explained in the strictness of it, in three things,
which would seem new and strange to those who had been always governed
by the tradition of the elders, and took all for oracular that they
taught.

`1.` We are here taught, that there is such a thing as heart-adultery,
adulterous thoughts and dispositions, which never proceed to the act of
adultery or fornication; and perhaps the defilement which these give to
the soul, that is here so clearly asserted, was not only included in the
seventh commandment, but was signified and intended in many of those
ceremonial pollutions under the law, for which they were to wash their
clothes, and bathe their flesh in water. Whosoever looketh on a woman
(not only another man\'s wife, as some would have it, but any woman), to
lust after her, has committed adultery with her in his heart, v. 28.
This command forbids not only the acts of fornication and adultery, but,
`(1.)` All appetites to them, all lusting after the forbidden object; this
is the beginning of the sin, lust conceiving (James 1:15); it is a bad
step towards the sin; and where the lust is dwelt upon and approved, and
the wanton desire is rolled under the tongue as a sweet morsel, it is
the commission of sin, as far as the heart can do it; there wants
nothing but convenient opportunity for the sin itself. Adultera mens
est-The mind is debauched. Ovid. Lust is conscience baffled or biassed:
biassed, if it say nothing against the sin; baffled, if it prevail not
in what is says. `(2.)` All approaches toward them; feeding the eye with
the sight of the forbidden fruit; not only looking for that end, that I
may lust; but looking till I do lust, or looking to gratify the lust,
where further satisfaction cannot be obtained. The eye is both the inlet
and outlet of a great deal of wickedness of this kind, witness Joseph\'s
mistress (Gen. 39:7), Samson (Jdg. 16:1), David, 2 Sa. 11:2. We read the
eyes full of adultery, that cannot cease from sin, 2 Pt. 2:14. What need
have we, therefore, with holy Job, to make a covenant with our eyes, to
make this bargain with them that they should have the pleasure of
beholding the light of the sun and the works of God, provided they would
never fasten or dwell upon any thing that might occasion impure
imaginations or desires; and under this penalty, that if they did, they
must smart for it in penitential tears! Job 31:1. What have we the
covering of the eyes for, but to restrain corrupt glances, and to keep
out of their defiling impressions? This forbids also the using of any
other of our senses to stir up lust. If ensnaring looks are forbidden
fruit, much more unclean discourses, and wanton dalliances, the fuel and
bellows of this hellish fire. These precepts are hedges about the law of
heart-purity, v. 8. And if looking be lust, they who dress and deck, and
expose themselves, with design to be looked at and lusted after (like
Jezebel, that painted her face and tired her head, and looked out at the
window) are no less guilty. Men sin, but devils tempt to sin.

`2.` That such looks and such dalliances are so very dangerous and
destructive to the soul, that it is better to lose the eye and the hand
that thus offend then to give way to the sin, and perish eternally in
it. This lesson is here taught us, v. 29, 30. Corrupt nature would soon
object against the prohibition of heart-adultery, that it is impossible
to governed by it; \"It is a hard saying, who can bear it? Flesh and
blood cannot but look with pleasure upon a beautiful woman; and it is
impossible to forbear lusting after and dallying with such an object.\"
Such pretences as these will scarcely be overcome by reason, and
therefore must be argued against with the terrors of the Lord, and so
they are here argued against.

`(1.)` It is a severe operation that is here prescribed for the preventing
of these fleshly lusts. If thy right eye offend thee, or cause thee to
offend, by wanton glances, or wanton gazings, upon forbidden objects; if
thy right hand offend thee, or cause thee to offend, by wanton
dalliances; and if it were indeed impossible, as is pretended, to govern
the eye and the hand, and they have been so accustomed to these wicked
practices, that they will not be withheld from them; if there be no
other way to restrain them (which, blessed be God, through his grace,
there is), it were better for us to pluck out the eye, and cut off the
hand, though the right eye, and right hand, the more honourable and
useful, than to indulge them in sin to the ruin of the soul. And if this
must be submitted to, at the thought of which nature startles, much more
must we resolve to keep under the body, and to bring it into subjection;
to live a life of mortification and self-denial; to keep a constant
watch over our own hearts, and to suppress the first rising of lust and
corruption there; to avoid the occasions of sin, to resist the
beginnings of it, and to decline the company of those who will be a
snare to us, though ever so pleasing; to keep out of harm\'s way, and
abridge ourselves in the use of lawful things, when we find them
temptations to us; and to seek unto God for his grace, and depend upon
that grace daily, and so to walk in the Spirit, as that we may not
fulfil the lusts of the flesh; and this will be as effectual as cutting
off a right hand or pulling out a right eye; and perhaps as much against
the grain to flesh and blood; it is the destruction of the old man.

`(2.)` It is a startling argument that is made use of to enforce this
prescription (v. 29), and it is repeated in the same words (v. 30),
because we are loth to hear such rough things; Isa. 30:10. It is
profitable for thee that one of thy members should perish, though it be
an eye or a hand, which can be worse spared, and not that thy whole body
should be cast into hell. Note, `[1.]` It is not unbecoming a minister
of the gospel to preach of hell and damnation; nay, he must do it, for
Christ himself did it; and we are unfaithful to our trust, if we give
not warning of the wrath to come. `[2.]` There ar some sins from which
we need to be saved with fear, particularly fleshly lusts, which are
such natural brute beasts as cannot be checked, but by being frightened;
cannot be kept from a forbidden tree, but by cherubim, with a flaming
sword. `[3.]` When we are tempted to think it hard to deny ourselves,
and to crucify fleshly lusts, we ought to consider how much harder it
will be to lie for ever in the lake that burns with fire and brimstone;
those do not know or do not believe what hell is, that will rather
venture their eternal ruin in those flames, than deny themselves the
gratification of a base and brutish lust. `[4.]` In hell there will be
torments for the body; the whole body will be cast into hell, and there
will be torment in every part of it; so that if we have a care of our
own bodies, we shall possess them in sanctification and honour, and not
in the lusts of uncleanness. `[5.]` Even those duties that are most
unpleasant to flesh and blood, are profitable for us; and our Master
requires nothing from us but what he knows to be for our advantage.

`3.` That men\'s divorcing of their wives upon dislike, or for any other
cause except adultery, however tolerated and practised among the Jews,
was a violation of the seventh commandment, as it opened a door to
adultery, v. 31, 32. Here observe,

`(1.)` How the matter now stood with reference to divorce. It hath been
said (he does not say as before, It hath been said by them of old time,
because this was not a precept, as those were, though the Pharisees were
willing so to understand it, ch. 19:7, but only a permission),
\"Whosoever shall put away his wife, let him give her a bill of divorce;
let him not think to do it by word of mouth, when he is in a passion;
but let him do it deliberately, by a legal instrument in writing,
attested by witnesses; if he will dissolve the matrimonial bond, let him
do it solemnly.\" Thus the law had prevented rash and hasty divorces;
and perhaps at first, when writing was not so common among the Jews,
that made divorces rare things; but in process of time it became very
common, and this direction of how to do it, when there was just cause
for it, was construed into a permission of it for any cause, ch. 19:3.

`(2.)` How this matter was rectified and amended by our Saviour. He
reduced the ordinance of marriage to its primitive institution: They two
shall be one flesh, not to be easily separated, and therefore divorce is
not to be allowed, except in case of adultery, which breaks the marriage
covenant; but he that puts away his wife upon any other pretence,
causeth her to commit adultery, and him also that shall marry her when
she is thus divorced. Note, Those who lead others into temptation to
sin, or leave them in it, or expose them to it, make themselves guilty
of their sin, and will be accountable for it. This is one way of being
partaker with adulterers Ps. 50:18.

### Verses 33-37

We have here an exposition of the third commandment, which we are the
more concerned right to understand, because it is particularly said,
that God will not hold him guiltless, however he may hold himself, who
breaks this commandment, by taking the name of the Lord in vain. Now as
to this command,

`I.` It is agreed on all hands that it forbids perjury, forswearing, and
the violation of oaths and vows, v. 33. This was said to them of old
time, and is the true intent and meaning of the third commandment. Thou
shalt not use, or take up, the name of God (as we do by an oath) in
vain, or unto vanity, or a lie. He hath not lift up his soul unto
vanity, is expounded in the next words, nor sworn deceitfully, Ps. 24:4.
Perjury is a sin condemned by the light of nature, as a complication of
impiety toward God and injustice toward man, and as rendering a man
highly obnoxious to the divine wrath, which was always judged to follow
so infallibly upon that sin, that the forms of swearing were commonly
turned into execrations or imprecations; as that, God do so to me, and
more also; and with us, So help me God; wishing I may never have any
help from God, if I swear falsely. Thus, by the consent of nations, have
men cursed themselves, not doubting but that God would curse them, if
they lied against the truth then, when they solemnly called God to
witness to it.

It is added, from some other scriptures, but shalt perform unto the Lord
thine oaths (Num. 30:2); which may be meant, either, 1. Of those
promises to which God is a party, vows made to God; these must be
punctually paid (Eccl. 5:4, 5): or, 2. Of those promises made to our
brethren, to which God was a Witness, he being appealed to concerning
our sincerity; these must be performed to the Lord, with an eye to him,
and for his sake: for to him, by ratifying the promises with an oath, we
have made ourselves debtors; and if we break a promise so ratified, we
have not lied unto men only, but unto God.

`II.` It is here added, that the commandment does not only forbid false
swearing, but all rash, unnecessary swearing: Swear not at all, v. 34;
Compare Jam. 5:12. Not that all swearing is sinful; so far from that, if
rightly done, it is a part of religious worship, and we in it give unto
God the glory due to his name. See Deu. 6:13; 10:20; Isa. 45:23; Jer.
4:2. We find Paul confirming what he said by such solemnities (2 Co.
1:23), when there was a necessity for it. In swearing, we pawn the truth
of something known, to confirm the truth of something doubtful or
unknown; we appeal to a greater knowledge, to a higher court, and
imprecate the vengeance of a righteous Judge, if we swear deceitfully.

Now the mind of Christ in this matter is,

`1.` That we must not swear at all, but when we are duly called to it,
and justice or charity to our brother, or respect to the commonwealth,
make it necessary for the end of strife (Heb. 6:16), of which necessity
the civil magistrate is ordinarily to be the judge. We may be sworn, but
we must now swear; we may be adjured, and so obliged to it, but we must
not thrust ourselves upon it for our own worldly advantage.

`2.` That we must not swear lightly and irreverently, in common
discourse: it is a very great sin to make a ludicrous appeal to the
glorious Majesty of heaven, which, being a sacred thing, ought always to
be very serious: it is a gross profanation of God\'s holy name, and of
one of the holy things which the children of Israel sanctify to the
Lord: it is a sin that has no cloak, no excuse for it, and therefore a
sign of a graceless heart, in which enmity to God reigns: Thine enemies
take thy name in vain.

`3.` That we must in a special manner avoid promissory oaths, of which
Christ more particularly speaks here, for they are oaths that are to be
performed. The influence of an affirmative oath immediately ceases, when
we have faithfully discovered the truth, and the whole truth; but a
promissory oath binds so long, and may be so many ways broken, by the
surprise as well as strength of a temptation, that it is not to be used
but upon great necessity: the frequent requiring and using of oaths, is
a reflection upon Christians, who should be of such acknowledged
fidelity, as that their sober words should be as sacred as their solemn
oaths.

`4.` That we must not swear by any other creature. It should seem there
were some, who, in civility (as they thought) to the name of God, would
not make use of that in swearing, but would swear by heaven or earth,
etc. This Christ forbids here (v. 34) and shows that there is nothing we
can swear by, but it is some way or other related to God, who is the
Fountain of all beings, and therefore that it is as dangerous to swear
by them, as it is to swear by God himself: it is the verity of the
creature that is laid at stake; now that cannot be an instrument of
testimony, but as it has regard to God, who is the summum verum-the
chief Truth. As for instance,

`(1.)` Swear not by the heaven; \"As sure as there is a heaven, this is
true;\" for it is God\'s throne, where he resides, and in a particular
manner manifests his glory, as a Prince upon his throne: this being the
inseparable dignity of the upper world, you cannot swear by heaven, but
you swear by God himself.

`(2.)` Nor by the earth, for it is his footstool. He governs the motions
of this lower world; as he rules in heaven, so he rules over the earth;
and though under his feet, yet it is also under his eye and care, and
stands in relation to him as his, Ps. 24:1. The earth is the Lord\'s; so
that in swearing by it, you swear by its Owner.

`(3.)` Neither by Jerusalem, a place for which the Jews had such a
veneration, that they could not speak of any thing more sacred to swear
by; but beside the common reference Jerusalem has to God, as part of the
earth, it is in special relation to him, for it is the city of the great
King (Ps. 48:2), the city of God (Ps. 46:4), he is therefore interested
in it, and in every oath taken by it.

`(4.)` \"Neither shalt thou swear by the head; though it be near thee, and
an essential part of thee, yet it is more God\'s than thine; for he made
it, and formed all the springs and powers of it; whereas thou thyself
canst not, from any natural intrinsic influence, change the colour of
one hair, so as to make it white or black; so that thou canst not swear
by thy head, but thou swearest by him who is the Life of thy head, and
the Lifter up of it.\" Ps. 3:3.

`5.` That therefore in all our communications we must content ourselves
with, Yea, yea, and nay, nay, v. 37. In ordinary discourse, if we affirm
a thing, let us only say, Yea, it is so; and, if need be, to evidence
our assurance of a thing, we may double it, and say, Yea, yea, indeed it
is so: Verily, verily, was our Saviour\'s yea, yea. So if we deny a
thing, let is suffice to say, No; or if it be requisite, to repeat the
denial, and say, No, no; and if our fidelity be known, that will suffice
to gain us credit; and if it be questioned, to back what we say with
swearing and cursing, is but to render it more suspicious. They who can
swallow a profane oath, will not strain at a lie. It is a pity that
this, which Christ puts in the mouths of all his disciples, should be
fastened, as a name of reproach, upon a sect faulty enough other ways,
when (as Dr. Hammond says) we are not forbidden any more than yea and
nay, but are in a manner directed to the use of that.

The reason is observable; For whatsoever is more than these cometh of
evil, though it do not amount to the iniquity of an oath. It comes ek
tou Diabolou; so an ancient copy has it: it comes from the Devil, the
evil one; it comes from the corruption of men\'s nature, from passion
and vehemence; from a reigning vanity in the mind, and a contempt of
sacred things: it comes from that deceitfulness which is in men, All men
are liars; therefore men use these protestations, because they are
distrustful one of another, and think they cannot be believed without
them. Note, Christians should, for the credit of their religion, avoid
not only that which is in itself evil, but that which cometh of evil,
and has the appearance of it. That may be suspected as a bad thing,
which comes from a bad cause. An oath is physic, which supposes a
disease.

### Verses 38-42

In these verses the law of retaliation is expounded, and in a manner
repealed. Observe,

`I.` What the Old-Testament permission was, in case of injury; and here
the expression is only, Ye have heard that is has been said; not, as
before, concerning the commands of the decalogue, that it has been said
by, or to, them of old time. It was a command, that every one should of
necessity require such satisfaction; but they might lawfully insist upon
it, if they pleased; an eye for an eye, and a tooth for a tooth. This we
find, Ex. 21:24; Lev. 24:20; Deu. 19:21; in all which places it is
appointed to be done by the magistrate, who bears not the sword in vain,
but is the minister of God, an avenger to execute wrath, Rom. 13:4. It
was a direction to the judges of the Jewish nation what punishment to
inflict in case of maims, for terror to such as would do mischief on the
one hand, and for a restraint to such as have mischief done to them on
the other hand, that they may not insist on a greater punishment than is
proper: it is not a life for an eye, nor a limb for a tooth, but observe
a proportion; and it is intimated (Num. 35:31), that the forfeiture in
this case might be redeemed with money; for when it is provided that no
ransom shall be taken for the life of a murderer, it is supposed that
for maims a pecuniary satisfaction was allowed.

But some of the Jewish teachers, who were not the most compassionate men
in the world, insisted upon it as necessary that such revenge should be
taken, even by private persons themselves, and that there was no room
left for remission, or the acceptance of satisfaction. Even now, when
they were under the government of the Roman magistrates, and
consequently the judicial law fell to the ground of course, yet they
were still zealous for any thing that looked harsh and severe.

Now, so far this is in force with us, as a direction to magistrates, to
use the sword of justice according to the good and wholesome laws of the
land, for the terror of evil-doers, and the vindication of the
oppressed. That judge neither feared God nor regarded man, who would not
avenge the poor widow of her adversary, Lu. 18:2, 3. And it is in force
as a rule to lawgivers, to provide accordingly, and wisely to apportion
punishments to crimes, for the restraint of rapine and violence, and the
protection of innocency.

`II.` What the New-Testament precept is, as to the complainant himself,
his duty is, to forgive the injury as done to himself, and no further to
insist upon the punishment of it than is necessary to the public good:
and this precept is consonant to the meekness of Christ, and the
gentleness of his yoke.

Two things Christ teaches us here:

`1.` We must not be revengeful (v. 39); I say unto you, that ye resist
not evil;-the evil person that is injurious to you. The resisting of any
ill attempt upon us, is here as generally and expressly forbidden, as
the resisting of the higher powers is (Rom. 13:2); and yet this does not
repeal the law of self-preservation, and the care we are to take of our
families; we may avoid evil, and may resist it, so far as is necessary
to our own security; but we must not render evil for evil, must not bear
a grudge, nor avenge ourselves, nor study to be even with those that
have treated us unkindly, but we must go beyond them by forgiving them,
Prov. 20:22; 24:29; 25:21, 22; Rom. 12:7. The law of retaliation must be
made consistent with the law of love: nor, if any have injured us, is
our recompence in our own hands, but in the hands of God, to whose wrath
we must give place; and sometimes in the hands of his viceregents, where
it is necessary for the preservation of the public peace; but it will
not justify us in hurting our brother to say that he began, for it is
the second blow that makes the quarrel; and when we were injured, we had
an opportunity not to justify our injuring him, but to show ourselves
the true disciples of Christ, by forgiving him.

Three things our Saviour specifies, to show that Christians must
patiently yield to those who bear hard upon them, rather than contend;
and these include others.

`(1.)` A blow on the cheek, which is an injury to me in my body;
\"Whosoever shall smite thee on thy right cheek,\" which is not only a
hurt, but an affront and indignity (2 Co. 11:20), if a man in anger or
scorn thus abuse thee, \"turn to him the other cheek;\" that is,
\"instead of avenging that injury, prepare for another, and bear it
patiently: give not the rude man as good as he brings; do not challenge
him, nor enter an action against him; if it be necessary to the public
peace that he be bound to his good behaviour, leave that to the
magistrate; but for thine own part, it will ordinarily be the wisest
course to pass it by, and take no further notice of it: there are no
bones broken, no great harm done, forgive it and forget it; and if proud
fools think the worse of thee, and laugh at thee for it, all wise men
will value and honour thee for it, as a follower of the blessed Jesus,
who, though he was the Judge of Israel, did not smite those who smote
him on the cheek,\" Micah 5:1. Though this may perhaps, with some base
spirits, expose us to the like affront another time, and so it is, in
effect, to turn the other cheek, yet let not that disturb us, but let us
trust God and his providence to protect us in the way of our duty.
Perhaps, the forgiving of one injury may prevent another, when the
avenging of it would but draw on another; some will be overcome by
submission, who by resistance would but be the more exasperated, Prov.
25:22. However, our recompence is in Christ\'s hands, who will reward us
with eternal glory for the shame we thus patiently endure; and though it
be not directly inflicted, it if be quietly borne for conscience\' sake,
and in conformity to Christ\'s example, it shall be put upon the score
of suffering for Christ.

`(2.)` The loss of a coat, which is a wrong to me in my estate (v. 40); If
any man will sue thee at the law, and take away thy coat. It is a hard
case. Note, It is common for legal processes to be made use of for the
doing of greatest injuries. Though judges be just and circumspect, yet
it is possible for bad men who make no conscience of oaths and
forgeries, by course of law to force off the coat from a man\'s back.
Marvel not at the matter (Eccl. 5:8), but, in such a case, rather than
go to the law by way of revenge, rather than exhibit a cross bill, or
stand out to the utmost, in defence of that which is thy undoubted
right, let him even take thy cloak also. If the matter be small, which
we may lose without an considerable damage to our families, it is good
to submit to it for peace\' sake. \"It will not cost thee so much to buy
another cloak, as it will cost thee by course of law to recover that;
and therefore unless thou canst get it again by fair means, it is better
to let him take it.\"

`(3.)` The going a mile by constraint, which is a wrong to me in my
liberty (v. 41); \"Whosoever shall compel thee to go a mile, to run an
errand for him, or to wait upon him, grudge not at it, but go with him
two miles rather than fall out with him:\" say not, \"I would do it, if
I were not compelled to it, but I hate to be forced;\" rather say,
\"Therefore I will do it, for otherwise there will be a quarrel;\" and
it is better to serve him, than to serve thy own lusts of pride and
revenge. Some give this sense of it: The Jews taught that the disciples
of the wise, and the students of the law, were not to be pressed, as
others might, by the king\'s officers, to travel upon the public
service; but Christ will not have his disciples to insist upon this
privilege, but to comply rather than offend the government. The sum of
all is, that Christians must not be litigious; small injuries must be
submitted to, and no notice taken of them; and if the injury be such as
requires us to seek reparation, it must be for a good end, and without
thought of revenge: though we must not invite injuries, yet we must meet
them cheerfully in the way of duty, and make the best of them. If any
say, Flesh and blood cannot pass by such an affront, let them remember,
that flesh and blood shall not inherit the kingdom of God.

`2.` We must be charitable and beneficent (v. 42); must not only do no
hurt to our neighbours, but labour to do them all the good we can. `(1.)`
We must be ready to give; \"Give to him that asketh thee. If thou has an
ability, look upon the request of the poor as giving thee an opportunity
for the duty of almsgiving.\" When a real object of charity presents
itself, we should give at the first word: Give a portion to seven, and
also to eight; yet the affairs of our charity must be guided with
discretion (Ps. 112:5), lest we give that to the idle and unworthy,
which should be given to those that are necessitous, and deserve well.
What God says to us, we should be ready to say to our poor brethren,
Ask, and it shall be given you. `(2.)` We must be ready to lend. This is
sometimes as great a piece of charity as giving; as it not only relieves
the present exigency, but obliges the borrower to providence, industry,
and honesty; and therefore, \"From him that would borrow of thee
something to live on, or something to trade on, turn not thou away: shun
not those that thou knowest have such a request to make of thee, nor
contrive excuses to shake them off.\" Be easy of access to him that
would borrow: though he be bashful, and have not confidence to make
known his case and beg the favour, yet thou knowest both his need and
his desire, and therefore offer him the kindness. Exorabor antequam
rogor; honestis precibus occuram-I will be prevailed on before I am
entreated; I will anticipate the becoming petition. Seneca, De Vitâ
Beatâ. It becomes us to be thus forward in acts of kindness, for before
we call, God hears us, and prevents us with the blessings of his
goodness.

### Verses 43-48

We have here, lastly, an exposition of that great fundamental law of the
second table, Thou shalt love thy neighbour, which was the fulfilling of
the law.

`I.` See here how this law was corrupted by the comments of the Jewish
teachers, v. 43. God said, Thou shalt love thy neighbour; and by
neighbour they understood those only of their own country, nation, and
religion; and those only that they were pleased to look upon as their
friends: yet this was not the worst; from this command, Thou shalt love
thy neighbour, they were willing to infer what God never designed; Thou
shalt hate thine enemy; and they looked upon whom they pleased as their
enemies, thus making void the great command of God by their traditions,
though there were express laws to the contrary, Ex. 23:4, 5; Deu. 23:7.
Thou shalt not abhor an Edomite, nor an Egyptian, though these nations
had been as much enemies to Israel as any whatsoever. It was true, God
appointed them to destroy the seven devoted nations of Canaan, and not
to make leagues with them; but there was a particular reason for it-to
make room for Israel, and that they might not be snares to them; but it
was very ill-natured from hence to infer, that they must hate all their
enemies; yet the moral philosophy of the heathen then allowed this. It
is Cicero\'s rule, Nemini nocere nisi prius lacessitum injuriâ-To injure
no one, unless previously injured. De Offic. See how willing corrupt
passions are to fetch countenance from the word of God, and to take
occasion by the commandment to justify themselves.

`II.` See how it is cleared by the command of the Lord Jesus, who teaches
us another lesson: \"But I say unto you, I, who come to be the great
Peace-Maker, the general Reconciler, who loved you when you were
strangers and enemies, I say, Love your enemies,\" v. 44. Though men are
ever so bad themselves, and carry it ever so basely towards us, yet that
does not discharge us from the great debt we owe them, of love to our
kind, love to our kin. We cannot but find ourselves very prone to wish
the hurt, or at least very coldly to desire the good, of those that hate
us, and have been abusive to us; but that which is at the bottom hereof
is a root of bitterness, which must be plucked up, and a remnant of
corrupt nature which grace must conquer. Note, it is the great duty of
Christians to love their enemies; we cannot have complacency in one that
is openly wicked and profane, nor put a confidence in one that we know
to be deceitful; nor are we to love all alike; but we must pay respect
to the human nature, and so far honour all men: we must take notice,
with pleasure, of that even in our enemies which is amiable and
commendable; ingenuousness, good temper, learning, and moral virtue,
kindness to others, profession of religion, etc., and love that, though
they are our enemies. We must have a compassion for them, and a good
will toward them. We are here told,

`1.` That we must speak well of them: Bless them that curse you. When we
speak to them, we must answer their revilings with courteous and
friendly words, and not render railing for railing; behind their backs
we must commend that in them which is commendable, and when we have said
all the good we can of them, not be forward to say any thing more. See 1
Pt. 3:9. They, in whose tongues is the law of kindness, can give good
words to those who give bad words to them.

`2.` That we must do well to them: \"Do good to them that hate you, and
that will be a better proof of love than good words. Be ready to do them
all the real kindness that you can, and glad of an opportunity to do it,
in their bodies, estates, names, families; and especially to do good to
their souls.\" It was said of Archbishop Cranmer, that the way to make
him a friend was to do him an ill turn; so many did he serve who had
disobliged him.

`3.` We must pray for them: Pray for them that despitefully use you, and
persecute you. Note, `(1.)` It is no new thing for the most excellent
saints to be hated, and cursed, and persecuted, and despitefully used,
by wicked people; Christ himself was so treated. `(2.)` That when at any
time we meet with such usage, we have an opportunity of showing our
conformity both to the precept and to the example of Christ, by praying
for them who thus abuse us. If we cannot otherwise testify our love to
them, yet this way we may without ostentation, and it is such a way as
surely we durst not dissemble in. We must pray that God will forgive
them, that they may never fare the worse for any thing they have done
against us, and that he would make them to be at peace with us; and this
is one way of making them so. Plutarch, in his Laconic Apophthegms, has
this of Aristo; when one commended Cleomenes\'s saying, who, being asked
what a good king should do, replied, Tous men philous euergetein, tous
de echthrous kakoµs poiein-Good turns to his friends, and evil to his
enemies; he said, How much better is it tous men philous euergetein,
tous de echthrous philous poiein-to do good to our friends, and make
friends of our enemies. This is heaping coals of fire on their heads.

Two reasons are here given to enforce this command (which sounds so
harsh) of loving our enemies. We must do it,

`[1.]` That we may be like God our Father; \"that ye may be, may approve
yourselves to be, the children of your Father which is in heaven.\" Can
we write a better copy? It is a copy in which love to the worst of
enemies is reconciled to, and consistent with, infinite purity and
holiness. God maketh his sun to rise, and sendeth rain, on the just and
the unjust, v. 45. Note, First, Sunshine and rain are great blessings to
the world, and they come from God. It is his sun that shines, and the
rain is sent by him. They do not come of course, or by chance, but from
God. Secondly, Common mercies must be valued as instances and proofs of
the goodness of God, who in them shows himself a bountiful Benefactor to
the world of mankind, who would be very miserable without these favours,
and are utterly unworthy of the least of them. Thirdly, These gifts of
common providence are dispensed indifferently to good and evil, just and
unjust; so that we cannot know love and hatred by what is before us, but
by what is within us; not by the shining of the sun on our heads, but by
the rising of the Sun of Righteousness in our hearts. Fourthly, The
worst of men partake of the comforts of this life in common with others,
though they abuse them, and fight against God with his own weapons;
which is an amazing instance of God\'s patience and bounty. It was but
once that God forbade his sun to shine on the Egyptians, when the
Israelites had light in their dwellings; God could make such a
distinction every day. Fifthly, The gifts of God\'s bounty to wicked men
that are in rebellion against him, teach us to do good to those that
hate us; especially considering, that though there is in us a carnal
mind which is enmity to God, yet we share in his bounty. Sixthly, Those
only will be accepted as the children of God, who study to resemble him,
particularly in his goodness.

`[2.]` That we may herein do more than others, v. 46, 47. First,
Publicans love their friends. Nature inclines them to it; interest
directs them to it. To do good to them who do good to us, is a common
piece of humanity, which even those whom the Jews hated and despised
could give as good proofs as of the best of them. The publicans were men
of no good fame, yet they were grateful to such as had helped them to
their places, and courteous to those they had a dependence upon; and
shall we be no better than they? In doing this we serve ourselves and
consult our own advantage; and what reward can we expect for that,
unless a regard to God, and a sense of duty, carrying us further than
our natural inclination and worldly interest? Secondly, We must
therefore love our enemies, that we may exceed them. If we must go
beyond scribes and Pharisees, much more beyond publicans. Note,
Christianity is something more than humanity. It is a serious question,
and which we should frequently put to ourselves, \"What do we more than
others? What excelling thing do we do? We know more than others; we talk
more of the things of God than others; we profess, and have promised,
more than others; God has done more for us, and therefore justly expects
more from us than from others; the glory of God is more concerned in us
than in others; but what do we more than others? Wherein do we live
above the rate of the children of this world? Are we not carnal, and do
we not walk as men, below the character of Christians? In this
especially we must do more than others, that while every one will render
good for good, we must render good for evil; and this will speak a
nobler principle, and is consonant to a higher rule, than the most of
men act by. Others salute their brethren, they embrace those of their
own party, and way, and opinion; but we must not so confine our respect,
but love our enemies, otherwise what reward have we? We cannot expect
the reward of Christians, if we rise no higher than the virtue of
publicans.\" Note, Those who promise themselves a reward above others
must study to do more than others.

Lastly, Our Saviour concludes this subject with this exhortation (v.
48), Be ye therefore perfect, as your Father which is in heaven is
perfect. Which may be understood, 1. In general, including all those
things wherein we must be followers of God as dear children. Note, It is
the duty of Christians to desire, and aim at, and press toward a
perfection in grace and holiness, Phil. 3:12-14. And therein we must
study to conform ourselves to the example of our heavenly Father, 1 Pt.
1:15, 16. Or, 2. In this particular before mentioned, of doing good to
our enemies; see Lu. 6:36. It is God\'s perfection to forgive injuries
and to entertain strangers, and to do good to the evil and unthankful,
and it will be ours to be like him. We that owe so much, that owe our
all, to the divine bounty, ought to copy it out as well as we can.
