> [[Introduction to Volume 5]{.underline}](#introduction-to-volume-5)
>
> [[Introduction to Matthew]{.underline}](#introduction-to-matthew)

Introduction to Volume 5
========================

(It may be proper to apprise the reader that the volume to which this
preface was originally prefixed included the Acts of the Apostles, which
in the present edition will commence the second volume, in order to
secure a more equal division of the New Testament-the commentary on the
remaining books being less extended than the author contemplated.-Ed.)

The one half of our undertaking upon the New Testament is now, by the
assistance of divine grace, finished, and presented to the reader, who,
it is hoped, the Lord working with it, may hereby be somewhat helped in
understanding and improving the sacred history of Christ and his
apostles, and in making it, as it certainly is, the best exposition of
our creed, in which these inspired writers are summed up, as is
intimated by that evangelist who calls his gospel A Declaration of those
things which are most surely believed among us, Lu. 1:1. And, as there
is no part of scripture in the belief of which it concerns more to be
established, so there is none with which the generality of Christians
are more conversant, or which they speak of more frequently. It is
therefore our duty, by constant pains in meditation and prayer, to come
to an intimate acquaintance with the true intent and meaning of these
narratives, what our concern is in them, and what we are to build upon
them and draw from them; that we may not rest in such a knowledge of
them as that which we had when in our childhood we were taught to read
English out of the translation and Greek out of the originals of these
books. We ought to know them as the physician does his dispensatory, the
lawyer his books of reports, and the sailor his chart and compass; that
is, to know how to make use of them in that to which we apply ourselves
as our business in this world, which is to serve God here and enjoy him
hereafter, and both in Christ the Mediator.

The great designs of the Christian institutes (of which these books are
the fountains and foundations) were, to reduce the children of men to
the fear and love of God, as the commanding active principle of their
observance of him, and obedience to him,-to show them the way of their
reconciliation to him and acceptance with him, and to bring them under
obligations to Jesus Christ as Mediator, and thereby to engage them to
all instances of devotion towards God and justice and charity towards
all men, in conformity to the example of Christ, in obedience to his
law, and in pursuance of his great intentions. What therefore I have
endeavoured here has been with this view, to make these writings
serviceable to the faith, holiness, and comfort of good Christians.

Now that these writings, thus made use of to serve these great and noble
designs, may have their due influence upon us, it concerns us to be well
established in our belief of their divine origin. And here we have to do
with two sorts of people. Some embrace the Old Testament, but set that
up in opposition to the New, pleading that, if that be right, this is
wrong; and these are the Jews. Others, though they live in a Christian
nation, and by baptism wear the Christian name, yet, under pretence of
freedom of thought, despise Christianity, and consequently reject the
New Testament, and therefore the Old of course. I confess it is strange
that any now who receive the Old Testament should reject the New, since,
besides all the particular proofs of the divine authority of the New
Testament, there is such an admirable harmony between it and the Old. It
agrees with the Old in all the main intentions of it, refers to it,
builds upon it, shows the accomplishment of its types and prophecies,
and thereby is the perfection and crown of it. Nay, if it be not true,
the Old Testament must be false, and all the glorious promises which
shine so brightly in it, and the performance of which was limited within
certain periods of time, must be a great delusion, which we are sure
they are not, and therefore must embrace the New Testament to support
the reputation of the Old.

Those things in the Old Testament which the New Testament lays aside are
the peculiarity of the Jewish nation and the observances of the
ceremonial law, both which certainly were of divine appointment; and yet
the New Testament does not at all clash with the Old; for,

`1.` They were always designed to be laid aside in the fulness of time.
No other is to be expected than that the morning-star should disappear
when the sun rises; and the latter parts of the Old Testament often
speak of the laying aside of those things, and of the calling in of the
Gentiles.

`2.` They were very honourable laid aside, and rather exchanged for that
which was more noble and excellent, more divine and heavenly. The Jewish
church was swallowed up in the Christian, the mosaic ritual in
evangelical institutions. So that the New Testament is no more the
undoing of the Old than the sending of a youth to the university is the
undoing of his education in the grammar-school.

`3.` Providence soon determined this controversy (which is the only thing
that seemed a controversy between the Old Testament and the New) by the
destruction of Jerusalem, the desolations of the temple, the dissolution
of the temple-service, and the total dispersion of all the remains of
the Jewish nation, with a judicial defeat of all the attempts to
incorporate it again, now for above 1600 years; and this according to
the express predictions of Christ, a little before his death. And, as
Christ would not have the doctrine of his being the Messiah much
insisted on till the great conclusive proof of it was given by his
resurrection from the dead, so the repeal of the ceremonial law, as to
the Jews, was not much insisted on, but their keeping up the observation
of it was connived at, till the great conclusive proof of its repeal was
given by the destruction of Jerusalem, which made the observation of it
for ever impracticable. And the manifest tokens of divine wrath which
the Jews, considered as a people, even notwithstanding the prosperity of
particular persons among them, continue under to this day, is a proof,
not only of the truth of Christ\'s predictions concerning them, but that
they lie under a greater guilt than that of idolatry (for which they lay
under a desolation of 70 years), and this can be no other than
crucifying Christ, and rejecting his gospel.

Thus evident it is that, in our expounding of the New Testament, we are
not undoing what we did in expounding the Old; so far from it that we
may appeal to the law and the prophets for the confirmation of the great
truth which the gospels are written to prove-That our Lord Jesus is the
Messiah promised to the fathers, who should come, and we are to look for
no other. For though his appearing did not answer the expectation of the
carnal Jews, who looked for a Messiah in external pomp and power, yet it
exactly answered all the types, prophecies, and promises, of the Old
Testament, which all had their accomplishment in him; and even his
ignominious sufferings, which are the greatest stumbling-block to the
Jews, were foretold concerning the Messiah; so that if he had not
submitted to them we had failed in our proof; so fat it is from being
weakened by them. Bishop Kidder\'s Demonstration of the Christian\'s
Messiah has abundantly made out this truth, and answered the cavils (for
such they are, rather than arguments) of the Jews against it, above any
in our language.

But we live in an age when Christianity and the New Testament are more
virulently and daringly attacked by some within their own bowels than by
those upon their borders. Never were Moses and his writings so arraigned
and ridiculed by any Jews, or Mahomet and his Alcoran by any Mussulmans,
as Christ and his gospel by men that are baptized and called Christians;
and this, not under colour of any other divine revelation, but in
contempt and defiance of all divine revelation; and not by way of
complaint that they meet with that which shocks their faith, and which,
through their own weakness, they cannot get over, and therefore desire
to be instructed in, and helped in the understanding of, and the
reconciling of them to the truth which they have received, but by way of
resolute opposition, as if they looked upon it as their enemy, and were
resolved by all means possible to be the ruin of it, though they cannot
say what evil it has done to the world or to them. If the pretence of it
has transported many in the church of Rome into such corruptions of
worship and cruelties of government as are indeed the scandal of human
nature, yet, instead of being thereby prejudiced against pure
Christianity, they should the rather appear more vigorously in defence
of it, when they see so excellent an institution as this is in itself so
basely abused and misrepresented. They pretend to a liberty of thought
in their opposition to Christianity, and would be distinguished by the
name of free-thinkers. I will not here go about to produce the arguments
which, to all that are not wilfully ignorant and prejudiced against the
truth, are sufficient to prove the divine origin and authority of the
doctrine of Christ. The learned find much satisfaction in reading the
apologies of the ancients for the Christian religion, when it was
struggling with the polytheism and idolatry of the Gentiles. Justin
Martyr and Tertullian, Lactantius and Minutius Felix, wrote admirable in
defence of Christianity, when it was further sealed by the blood of the
martyrs. But its patrons and advocates in the present day have another
sort of enemies to deal with. The antiquity of the pagan theology, its
universal prevalence, the edicts of princes, and the traditions and
usages of the country, are not now objected to Christianity; but I know
not what imaginary freedom of thought, and an unheard-of privilege of
human nature, are assumed, not to be bound by any divine revelation
whatsoever. Now it is easy to make out,

`1.` That those who would be thought thus to maintain a liberty of
thinking as one of the privileges of human nature, and in defence of
which they will take up arms against God himself, do not themselves
think freely, nor give others leave to do so. In some of them a resolute
indulgence of themselves in those vicious courses which they know the
gospel if they admit it will make very uneasy to them, and a secret
enmity to a holy heavenly mind and life, forbid them all free thought;
for so strong a prejudice have their lusts and passions laid them under
against the laws of Christ that they find themselves under a necessity
of opposing the truths of Christ, upon which these laws are founded.
Perit judicium, quando res transit in affectum-The judgment is overcome,
when the decision is referred to the affections. Right or wrong,
Christ\'s bonds must be broken, and his cords cast from them; and
therefore, how evident soever the premises be, the conclusion must be
denied, if it tend to fasten these bands and cords upon them; and where
is the freedom of thought then? While they promise themselves liberty,
they themselves are the servants of corruption; for of whom a man is
overcome of the same is he brought into bondage. In others of them, a
reigning pride and affectation of singularity, and a spirit of
contradiction, those lusts of the mind, which are as impetuous and
imperious as any of the lusts of the flesh and of the world, forbid a
freedom of thinking, and enslave the soul in all its enquiries after
religion. Those can no more think freely who resolve they will think by
themselves than those can who resolve to think with their neighbours.
Nor will they give others liberty to think freely; for it is not by
reason and argument that they go about to convince us, but by jest and
banter, and exposing Christianity and its serious professors to
contempt. Now, considering how natural it is to most men to be jealous
for their reputation, this is as great an imposition as can possibly be;
and the unthinking are as much kept from free-thinking by the fear of
being ridiculed in the club of those who set up for oracles in reason as
by the fear of being cursed, excommunicated, and anathematized, by the
counsel of those who set up for oracles in religion. And where is the
free-thinking then?

`2.` That those who will allow themselves a true liberty of thinking, and
will think seriously, cannot but embrace all Christ\'s sayings, as
faithful, and well worthy of all acceptation. Let the corrupt bias of
the carnal heart towards the world, and the flesh, and self (the most
presumptuous idol of the three) be taken away, and let the doctrine of
Christ be proposed first in its true colours, as Christ and his apostles
have given it to us, and in its true light, with all its proper
evidence, intrinsic and extrinsic; and then let the capable soul freely
use its rational powers and faculties, and by the operation of the
Spirit of grace, who alone works faith in all that believe, even the
high thought, when once it becomes a free thought, freed from the
bondage of sin and corruption, will, by a pleasing and happy power, be
captivated, and brought into obedience to Christ; and, when he thus
makes it free, it will be free indeed. Let any one who will give himself
leave to think impartially, and be at the pains to think closely, read
Mr. Baxter\'s Reasons for the Christian Religion, and he will find both
that it goes to the bottom, and lays the foundation deep and firm, and
also that it brings forth the top-stone in a believer\'s consent to God
in Christ, to the satisfaction of any that are truly concerned about
their souls and another world. The proofs of the truths of the gospel
have been excellently well methodized, and enforced likewise, by bishop
Stillingfleet, in his Origines Sacrae; by Grotius, in his book of the
Truth of the Christian Religion; by Dr. Whitby, in his General Preface
to his Commentary on the New Testament; and of late by Mr. Ditton, very
argumentatively, in his discourse concerning the Resurrection of Jesus
Christ; and many others have herein done worthily. And I will not
believe any man who rejects the New Testament and the Christian religion
to have thought freely upon the subject, unless he has, with humility,
seriousness, and prayer to God for direction, deliberately read these or
the like books, which, it is certain, were written both with liberty and
clearness of thought.

For my own part, if my thoughts were worth any one\'s notice, I do
declare I have thought of this great concern with all the liberty that a
reasonable soul can pretend to, or desire; and the result is that the
more I think, and the more freely I think, the more fully I am satisfied
that the Christian religion is the true religion, and that which, if I
submit my soul sincerely to it, I may venture my soul confidently upon.
For when I think freely,

`1.` I cannot but think that the God who made man a reasonable creature
by his power has a right to rule him by his law, and to oblige him to
keep his inferior faculties of appetite and passion, together with the
capacities of thought and speech, in due subjection to the superior
powers of reason and conscience. And, when I look into my own heart, I
cannot but think that it was this which my Maker designed in the order
and frame of my soul, and that herein he intended to support his own
dominion in me.

`2.` I cannot but think that my happiness is bound up in the favour of
God, and that his favour will, or will not, be towards me, according as
I do, or do not, comply with the laws and ends of my creation,-that I am
accountable to this God, and that from him my judgment proceeds, not
only fr this world, but for my everlasting state.

`3.` I cannot but think that my nature is very unlike what the nature of
man was as it came out of the Creator\'s hands,-that it is degenerated
from its primitive purity and rectitude. I find in myself a natural
aversion to my duty, and to spiritual and divine exercises, and a
propensity to that which is evil, such an inclination towards the world
and the flesh as amounts to a propensity to backslide from the living
God.

`4.` I cannot but think that I am therefore, by nature, thrown out of the
favour of God; for though I think he is a gracious and merciful God, yet
I think he is also a just and holy God, and that I am become, by sin,
both odious to his holiness and obnoxious to his justice. I should not
think freely, but very partially, if I should think otherwise. I think I
am guilty before God, have sinned, and come short of glorifying him, and
of being glorified with him.

`5.` I cannot but think that, without some special discovery of God\'s
will concerning me, and good-will to me, I cannot possibly recover his
favour, be reconciled to him, or be so far restored to my primitive
rectitude as to be capable of serving my Creator, and answering the ends
of my creation, and becoming fit for another world; for the bounties of
Providence to me, in common with the inferior creatures, cannot serve
either as assurances that God is reconciled tome or means to reconcile
me to God.

`6.` I cannot but think that the way of salvation, both from the guilt
and from the power of sin, by Jesus Christ, and his mediation between
God and man, as it is revealed by the New Testament, is admirable will
fitted to all the exigencies of my case, to restore me both to the
favour of God and to the government and enjoyment of myself. Here I see
a proper method for the removing of the guilt of sin (that I may not die
by the sentence of the law) by the all-sufficient merit and
righteousness of the Son of God in our nature, and for the breaking of
the power of sin (that I may not die by my own disease) by the
all-sufficient influence and operation of the Spirit of God upon our
nature. Every malady has herein its remedy, every grievance is hereby
redressed, and in such a way as advances the honour of all the divine
attributes and is suited and accommodated to human nature.

`7.` I cannot but think that what I find in myself of natural religion
does evidently bear testimony to the Christian religion; for all that
truth which is discovered to me by the light of nature is confirmed, and
more clearly discovered, by the gospel; the very same thing which the
light of nature gives me a confused sight of (like the sight of men as
trees walking) the New Testament gives me a clear and distinct sight of.
All that good which is pressed upon me by the law of nature is more
fully discovered to me, and I find myself much more strongly bound to it
by the gospel of Christ, the engagements it lays upon me to my duty, and
the encouragements and assistances it gives me in my duty. And this is
further confirming to me that there, just there, where natural light
leaves me at a loss, and unsatisfied-tells me that hitherto it can carry
me, but no further-the gospel takes me up, helps me out, and gives me
all the satisfaction I can desire, and that is especially in the great
business of the satisfying of God\'s justice for the sin of man. My own
conscience asks, Wherewith shall I come before the Lord, and bow myself
before the most high God? Will he be pleased with thousands of rams? But
I am still at a loss; I cannot frame a righteousness from any thing I
am, or have, in myself, or from any thing I can do for God or present to
God, wherein I dare appear before him; but the gospel comes, and tells
me that Jesus Christ had made his soul an offering for sin, and God has
declared himself well-pleased with all believers in him; and this makes
me easy.

`8.` I cannot but think that the proofs by which God has attested the
truth of the gospel are the most proper that could be given in a case of
this nature-that the power and authority of the Redeemer in the kingdom
of grace should be exemplified to the world, not by the highest degree
of the pomp and authority of the kings of the earth, as the Jews
expected, but by the evidences of his dominion in the kingdom of nature,
which is a much greater dignity and authority than any of the kings of
the earth ever pretended to, and is no less than divine. And his
miracles being generally wrought upon men, not only upon their bodies,
as they were mostly when Christ was here upon earth, but, which is more,
upon their minds, as they were mostly after the pouring out of the
Spirit in the gift of tongues and other supernatural endowments, were
the most proper confirmations possible of the truth of the gospel, which
was designed for the making of men holy and happy.

`9.` I cannot but think that the methods taken for the propagation of
this gospel, and the wonderful success of those methods, which are
purely spiritual and heavenly, and destitute of all secular advantages
and supports, plainly show that it was of God, for God was with it; and
it could never have spread as it did, in the face of so much opposition,
if it had not been accompanied with a power from on high. And the
preservation of Christianity in the world to this day, notwithstanding
the difficulties it has struggles with, is to me a standing miracle for
the proof of it.

`10.` I cannot but think that the gospel of Christ has had some influence
upon my soul, has had such a command over me, and been such a comfort to
me, as is a demonstration to myself, though it cannot be so to another,
that it is of God. I have tasted in it that the Lord is gracious; and
the most subtle disputant cannot convince one who has tasted honey that
it is not sweet.

And now I appeal to him who knows the thoughts and intents of the heart
that in all this I think freely (if it be possible for a man to know
that he does so), and not under the power of any bias. Whether we have
reason to think that those who, without any colour of reason, not only
usurp, but monopolize, the character of free-thinkers, do so, let those
judge who easily observe that they do not speak sincerely, but
industriously dissemble their notions; and one instance I cannot but
notice of their unfair dealing with their readers-that when, for the
diminishing of the authority of the New Testament, they urge the various
readings of the original, and quote an acknowledgment of Mr. Gregory of
Christ-church, in his preface to his Works, That no profane author
whatsoever, etc., and yet suppress what immediately follows, as the
sense of that learned man upon it, That this is an invincible reason for
the scriptures\' part, etc.

But while we are thus maintaining the divine origin and authority of the
New Testament, as it has been received through all the ages of the
church, we find our cause not only attacked by the enemies we speak of,
but in effect betrayed by one who makes our New Testament almost double
to what it really is, adding to the Constitutions of the Apostles,
collected by Clement, together with the Apostolical Canons, and making
those to be of equal authority with the writings of the evangelists, and
preferable to the Epistles. By enlarging the lines of defence thus,
without either cause or precedent, he gives great advantage to the
invaders. Those Constitutions of the Apostles have many things in them
very good, and may be of use, as other human compositions; but to
pretend that they wee composed, as they profess to be, by the twelve
apostles in concert at Jerusalem, I Peter saying this, I Andrew saying
that, etc., is the greatest imposition that can be practised upon the
credulity of the simple.

`1.` It is certain there were a great many spurious writings which, in
the early days of the church, went under the names of the apostles and
apostolical men; so that it has always been complained of as impossible
to find out any thing but the canon of scripture that could with any
assurance be attributed to them. Baronius himself acknowledges it, Cum
apostolorum nomine tam facta quam dicta reperiantur esse supposititia;
nec sic quid de illis à veris sincerisque spriptoribus narratum sit
integrum et incorruptum remanserit, in desperationem planè quandam
animum dejicunt posse unquam assequi quod verum certumque
subsistat-Since so many of the acts and sayings ascribed to the apostles
are found to be spurious, and even the narrations of faithful writers
respecting them are not free from corruption, we must despair of ever
being able to arrive at any absolute certainty about them.-Ad An.
Christ. 44, sect. 42, etc. There were Acts under the names of Andrew the
apostle, Philip, Peter, Thomas; a Gospel under the names of Thaddeus,
another of Barnabas, another of Bartholomew; a book concerning the
infancy of our Saviour, another concerning his nativity, and many the
like, which we all rejected as forgeries.

`2.` These Constitutions and Canons, among the rest, were condemned in
the primitive church as apocryphal, and therefore justly rejected;
because, though otherwise good, they pretended to be what really they
were not, dictated by the twelve apostles themselves, as received from
Christ. If Jesus Christ gave them such instructions, and they gave them
in such a solemn manner to the church, as is pretended, it is
unaccountable that there is not the least notice taken of any such thing
done or designed in the Gospels, the Acts, or any of the Epistles.

Those who have judged the most favourable of these Canons and
Constitutions have concluded that they were complied by some officious
persons under the name of Clement, towards the end of the second
century, above 150 years after Christ\'s ascension, out of the common
practice of the churches; that is, that which the compilers were most
acquainted with, or had respect for; when at the same time we have
reason to think that the far greater number of Christian churches which
by that time were planted had Constitutions of their own, which, if they
had had the happiness to be transmitted to posterity, would have
recommended themselves as well as these, or better. But, as the
legislators of old put a reputation upon their laws by pretending to
have received them from some deity or other, so church-governors studied
to gain reputation to their sees by placing some apostolical man or
other at the head of their catalogue of bishops (see bishop
Stillingfleet\'s Irenicum, p. 302), and reputation to their Canons and
Constitutions by fathering them upon the apostles. But how can it be
imagined that the apostles should be all together at Jerusalem, to
compose this book of Canons with so much solemnity, when we know that
their commission was to go into all the world, and to preach the gospel
to every creature? Accordingly, Eusebius tells us that Thomas went into
Parthia, Andrew into Scythia, John into the lesser Asia; and we have
reason to think that after their dispersion they never came together
again, any more than the planters of the nations did after the Most High
had separated the sons of Adam.

I think that any one who will compare these Constitutions with the
writings which we are sure were given by inspiration of God will easily
discern a vast difference in the style and spirit. What is the chaff to
the wheat? \"Where are ministers, in the style of the true apostles,
called priests, high priests? Where do we find in the apostolical age,
that age of suffering, of the placing of the bishop in his throne? Or of
readers, singers, and porters, in the church?\" (Edit. Joan. Clerici, p.
245.)

I fear the collector and compiler of those Constitutions, under the name
of Clement, was conscious to himself of his honesty in it, in that he
would not have them published before all, because of the mysteries
contained in them; nor were they known or published till the middle of
the fourth century, when the forgery could not be so well disproved. I
cannot see any mysteries in them, that they should be concealed, if they
had been genuine; but I am sure that Christ bids his apostles publish
the mysteries of the kingdom of God upon the house-tops. And St. Paul,
though there are mysteries in his epistles much more sublime than any of
these Constitutions, charges that they should be read to all the holy
brethren. Nay, these Constitutions are so wholly in a manner taken up
either with moral precepts, or rules of practice in the church, that if
they had been what they pretend they had been most fit to be published
before all. And though the Apocalypse is so full of mysteries, yet a
blessing is pronounced upon the readers and hearers of that prophecy. We
must therefore conclude that, whenever they were written, by declining
the light they owned themselves to be apocryphal, that is, hidden or
concealed; that they durst not mingle themselves with what was given by
divine inspiration; to allude to what is said of the ministers (Acts
5:13), Of the rest durst no man join himself to the apostles, for the
people magnified them. So that even by their own confession they were
not delivered to the churches with the other writings, when the
New-Testament canon was solemnly sealed up with that dreadful sentence
passed on those that add unto these things.

And as we have thus had attempts made of late upon the purity and
sufficiency of our New Testament, by additions to it, so we have
likewise had from another quarter a great contempt put upon it by the
papal power. The occasion was this:-One Father Quesnel, a French papist,
but a Jansenist, nearly thirty years ago, published the New Testament in
French, in several small volumes, with Moral Reflections on every verse,
to render the reading of it more profitable, and meditation upon it more
easy. It was much esteemed in France, for the sake of the piety and
devotion which appeared in it, and it had several impressions. The
Jesuits were much disgusted, and solicited the pope for the condemnation
of it, though the author of it was a papist, and many things in it
countenanced popish superstition. After much struggling about it in the
court of Rome a bull was at length obtained, at the request of the
French king, from the present pope Clement 11 bearing date September 8,
1713, by which the said book, with what title or in what language soever
it is printed, is prohibited and condemned; both the New Testament
itself, because in many things varying from the vulgar Latin, and the
Annotations, as containing divers propositions (above a hundred are
enumerated) scandalous and pernicious, injurious to the church and its
customs, impious, blasphemous, savouring of heresy. And the propositions
are such as these-\"That the grace of our Lord Jesus Christ is the
effectual principle of all manner of good, is necessary for every good
action; for without it nothing is done, nay nothing can be done\"-\"That
it is a sovereign grace, and is an operation of the almighty hand of
God\"-\"That, when God accompanies his word with the internal power of
his grace, it operates in the soul the obedience which it
demands\"-\"That faith is the first grace, and the fountain of all
others\"-\"That it is in vain for us to call God our Father, if we do
not cry to him with a spirit of love\"-\"That there is no God, nor
religion, where there is no charity\"-\"That the catholic church
comprehends the angels and all the elect and just men of the earth of
all ages\"-\"That it had the Word incarnate for its head, and all the
saints for its members\"-\"That it is profitable and necessary at all
times, in all places, and for all sorts of persons, to know the holy
Scriptures\"-\"That the holy obscurity of the word of God is no reason
for the laity not reading it\"-\"That the Lord\'s day ought to be
sanctified by reading books of piety, especially the holy
scriptures\"-And \"that to forbid Christians from reading the scriptures
is to prohibit the use of the light to the children of light.\" Many
such positions as these, which the spirit of every good Christian cannot
but relish as true and good, are condemned by the pope\'s bull as
impious and blasphemous. And this bull, though strenuously opposed by a
great number of the bishops in France, who were well affected to the
notions of father Quesnel, was yet received and confirmed by the French
king\'s letters patent, bearing date at Versailles, February 14, 1714,
which forbid all manner of persons, upon pain of exemplary punishment,
so much as to keep any of those books in their houses; and adjudge any
that should hereafter write in defence of the propositions condemned by
the pope as disturbers of the peace. It was registered the day
following, February 15, by the Parliament of Paris, but with divers
provisos and limitations.

By this is appears that popery is still the same thing that ever it was,
an enemy to the knowledge of the scriptures, and to the honour of divine
grace. What reason have we to bless God that we have liberty to read the
scriptures, and have helps to understand and improve them, which we are
concerned diligently to make a good use of, that we may not provoke God
to give us up into the hands of those powers that would use us in like
manner!

I am willing to hope that those to whom the reading of the Exposition of
the Old Testament was pleasant will find this yet more pleasant; for
this is that part of scripture which does most plainly testify of
Christ, and in which that gospel grace which appears unto all men,
bringing salvation, shines most clearly. This is the New-Testament milk
for babes, the rest is strong meat for strong men. By these, therefore,
let us be nourished and strengthened that we my be pressing on towards
perfection; and that, having laid the foundation in the history of our
blessed Saviour\'s life, death, and resurrection, and the first
preaching of his gospel, we may build upon it by an acquaintance with
the mysteries of godliness, to which we shall be further introduced in
the Epistles.

I desire I may be read with a candid, and not a critical, eye. I pretend
not to gratify the curious; the summit of my ambition is to assist those
who are truly serious in searching the scriptures daily. I am sure the
work is designed, and hope it is calculated, to promote piety towards
God and charity towards our brethren, and that there is not only
something in it which may edify, but nothing which may justly offend any
good Christian.

If any receive spiritual benefit by my poor endeavours, it will be
comfort to me, but let God have all the glory, and that free grace of
his which has employed one that is utterly unworthy of such an honour,
and enabled one thus far to go on in it who is utterly insufficient for
such a service.

Having obtained help of God, I continue hitherto in it, and humbly
depend upon the same good hand of my God to carry me on it that which
remains, to gird my loins with needful strength and to make my way
perfect; and for this I humbly desire the prayers of my friends. One
volume more, I hope, will include what is yet to be done; and I will
both go about it, and go on with it, as God shall enable me, withall
convenient speed; but it is that part of the scripture which, of all
others, requires the most care and pains in expounding it. But I trust
that as the day so shall the strength be.

Introduction to Matthew
=======================

We have now before us, `I.` The New Testament of our Lord and Savior Jesus
Christ; so this second part of the holy Bible is entitled: The new
covenant; so it might as well be rendered; the word signifies both. But,
when it is (as here) spoken of as Christ\'s act and deed, it is most
properly rendered a testament, for he is the testator, and it becomes of
force by his death (Heb. 9:16, 17); nor is there, as in covenants, a
previous treaty between the parties, but what is granted, though an
estate upon condition, is owing to the will, the free-will, the
good-will, of the Testator. All the grace contained in this book is
owing to Jesus Christ as our Lord and Saviour; and, unless we consent to
him as our Lord, we cannot expect any benefit by him as our Saviour.
This is called a new testament, to distinguish it from that which was
given by Moses, and was not antiquated; and to signify that it should be
always new, and should never wax old, and grow out of date. These books
contain, not only a full discovery of that grace which has appeared to
all men, bringing salvation, but a legal instrument by which it is
conveyed to, and settled upon, all believers. How carefully do we
preserve, and with what attention and pleasure do we read, the last will
and testament of a friend, who has therein left us a fair estate, and,
with it, high expressions of his love to us! How precious then should
this testament of our blessed Saviour be to us, which secures to us all
his unsearchable riches! It is his testament; for though, as is usual,
it was written by others (we have nothing upon record that was of
Christ\'s own writing), yet he dictated it; and the night before he
died, in the institution of his supper, he signed, sealed, and published
it, in the presence of twelve witnesses. For, though these books were
not written for some years after, for the benefit of posterity, in
perpetuam rei memoriam-as a perpetual memorial, yet the New Testament of
our Lord Jesus was settled, confirmed, and declared, from the time of
his death, as a nuncupative will, with which these records exactly
agree. The things which St. Luke wrote were things which were most
surely believed, and therefore well known, before he wrote them; but,
when they were written, the oral tradition was superseded and set aside,
and these writings were the repository of that New Testament. This is
intimated by the title which is prefixed to many Greek Copies, Teµs
kaineµs Diatheµkeµs Hapanta-The whole of the New Testament, or all the
things of it. In it is declared the whole counsel of God concerning our
salvation, Acts 20:27. As the law of the Lord is perfect, so is the
gospel of Christ, and nothing is to be added to it. We have it all, and
are to look for no more.

`II.` We have before us The Four Gospels. Gospel signifies good news, or
glad tidings; and this history of Christ\'s coming into the world to
save sinners is, without doubt, the best news that ever came from heaven
to earth; the angel gave it this title (Lu. 2:10), Euangelizomai hymin-I
bring you good tidings; I bring the gospel to you. And the prophet
foretold it, Isa. 52:7; 61:1. It is there foretold that in the days of
the messiah good tidings should be preached. Gospel is an old Saxon
word; it is God\'s spell or word; and God is so called because he is
good, Deus optimus-God most excellent, and therefore it may be a good
spell, or word. If we take spell in its more proper signification for a
charm (carmen), and take that in a good sense, for what is moving and
affecting, which is apt lenire dolorem-to calm the spirits, or to raise
them in admiration or love, as that which is very amiable we call
charming, it is applicable to the gospel; for in it the charmer charmeth
wisely, though to deaf adders, Ps. 58:4, 5. Nor (one would think) can
any charms be so powerful as those of the beauty and love of our
Redeemer. The whole New Testament is the gospel. St. Paul calls it his
gospel, because he was one of the preachers of it. Oh that we may each
of us make it ours by our cordial acceptance of it and subjection to it!
But the four books which contain the history of the Redeemer we commonly
call the four gospels, and the inspired penmen of them evangelists, or
gospel-writers; not, however, very properly, because that title belongs
to a particular order of ministers, that were assistants to the apostles
(Eph. 4:11): He gave some apostles, and some evangelists. It was
requisite that the doctrine of Christ should be interwoven with, and
founded upon, the narrative of his birth, life, miracles, death, and
resurrection; for then it appears in its clearest and strongest light.
As in nature, so in grace, the most happy discoveries are those which
take rise from the certain representations of matters of fact. Natural
history is the best philosophy; and so is the sacred history, both of
the Old and New Testament, the most proper and grateful vehicle of
sacred truth. These four gospels were early and constantly received by
the primitive church, and read in Christian assemblies, as appears by
the writings of Justin Martyr and Irenaeus, who lived little more than a
hundred years after the ascension of Christ; they declared that neither
more nor fewer than four were received by the church. A Harmony of these
four evangelists was compiled by Tatian about that time, which he
called, To dia tessaroµn-The Gospel out of the four. In the third and
fourth centuries there were gospels forged by divers sects, and
published, one under the name of St. Peter, another of St. Thomas,
another of St. Philip, etc. But they were never owned by the church, nor
was any credit given to them, as the learned Dr. Whitby shows. And he
gives this good reason why we should adhere to these written records,
because, whatever the pretences of tradition may be, it is not
sufficient to preserve things with any certainty, as appears by
experience. For, whereas Christ said and did many memorable things,
which were not written (Jn. 20:30; 21:25), tradition has not preserved
any one of them to us, but all is lost except what was written; that
therefore is what we must abide by; and blessed by God that we have it
to abide by; it is the sure word of history.

`III.` We have before us the Gospel according to St. Matthew. The penman
was by birth a Jew, by calling a publican, till Christ commanded his
attendance, and then he left the receipt of custom, to follow him, and
was one of those that accompanied him all the time that the Lord Jesus
went in and out, beginning from the baptism of John unto the day that he
was taken up, Acts 1:21, 22. He was therefore a competent witness of
what he has here recorded. He is said to have written this history about
eight years after Christ\'s ascension. Many of the ancients say that he
wrote it in the Hebrew or Syriac language; but the tradition is
sufficiently disproved by Dr. Whitby. Doubtless, it was written in
Greek, as the other parts of the New Testament were; not in that
language which was peculiar to the Jews, whose church and state were
near a period, but in that which was common to the world, and in which
the knowledge of Christ would be most effectually transmitted to the
nations of the earth; yet it is probable that there might be an edition
of it in Hebrew, published by St. Matthew himself, at the same time that
he wrote it in Greek; the former for the Jews, the latter for the
Gentiles, when he left Judea, to preach among the Gentiles. Let us bless
God that we have it, and have it in a language we understand.
