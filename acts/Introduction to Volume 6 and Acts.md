> [[Introduction to Volume 6]{.underline}](#introduction-to-volume-6)
>
> [[Introduction to Acts]{.underline}](#introduction-to-acts)

Introduction to Volume 6
========================

After much expectation, and many enquiries, the last volume of the late
reverend Mr. Henry\'s Exposition now appears in the world. The common
disadvantages that attend posthumous productions will doubtless be
discerned in this; but we hope, though there are diversities of gifts,
there will be found to be the same spirit. Some of the relations and
hearers of that excellent person have been at the pains of transcribing
the notes they took in short-hand of this part of the holy scripture,
when expounded by him in his family or in the congregation; they have
furnished us with very good materials for the finishing of this great
work, and we doubt not but that the ministers who have been concerned in
it have made that use of those assistances which may entitle this
composure to the honour of Mr. Henry\'s name; and, if so, they can very
willingly conceal their own.

The New Testament may be very properly divided into two parts, the one
historical the other epistolary. It is the exposition of the latter we
now recommend, and shall offer some thoughts on the epistolary way of
writing in general, and then proceed to observe the divine authority of
these epistles, together with the style, matter, method, and design of
them, leaving what might be said concerning the several inspired penmen
to the prefaces appertaining to the particular epistles.

As to the epistolary way of writing, it may be sufficient to observe
that it has usually three properties:-It may in some things be more
difficult to be understood, but then it is very profitable, and very
pleasant; these will be found to be the properties of these sacred
letters. We shall meet with things not easy to be understood, especially
in some parts of them, where we cannot so well discover the particular
occasions on which they were written or the questions or matters of fact
to which they refer; but this is abundantly compensated by the profit
which will accrue to those that read them with due attention. They will
find the strongest reasoning, the most moving expostulations, and warm
and pressing exhortations, mixed with seasonable cautions and reproofs,
which are all admirably fitted to impress the mind with suitable
sentiments and affections. And how much solid pleasure and delight must
this afford to persons of a serious and religious spirit, especially
when they wisely and faithfully apply to themselves what they find to
suit their case! Thus they will appear to be as truly written to them as
if their names were superscribed on them. It is natural for us to be
very much pleased in perusing a wise and kind letter, full of
instruction and comfort, sent to us by an absent friend: how then should
we prize this part of holy scripture, when we consider herein that our
God and Saviour has written these letters to us, in which we have the
great things of his law and gospel, the things that belong to our peace!
By these means not only the holy apostles, being dead, yet speak, but
the Lord of the prophets and apostles continues to speak and write to
us; and while we read them with proper affections, and follow them with
suitable petitions and thanksgivings, a blessed correspondence and
intercourse will be kept up between heaven and us, while we are yet
sojourners in the earth.

But it is the divine inspiration and authority of these epistles we are
especially concerned to know; and it is of the last importance that in
this our minds be fully established. And we have strong and clear
evidence that these epistles were written by the apostles of our Lord
Jesus, and that they (like the prophets of the Old Testament) spoke and
wrote as they were moved by the Holy Ghost. These epistles have in all
ages of the church been received by Christians as a part of those holy
scriptures that are given by inspiration of God, and are profitable for
doctrine, for reproof, for correction, and for instruction in
righteousness, and are able to make us wise to salvation through faith
which is in Jesus Christ; they are part of that perpetual universal rule
of faith and life which contains doctrines and revelations we are bound
to believe with a divine faith, as coming from the God of truth, and
duties to be practised by us in obedience to the will of God,
acknowledging that the things written therein are the commandments of
God, 1 Co. 14:37. And, for the same reasons that lead us to acknowledge
the other parts of the Bible to be the word of God, we must own these to
be so too. If there is good reason (as indeed there is) to believe that
the books of Moses were written by inspiration of God, there is the same
reason to believe that the writings of the prophets were also from God,
because the law and the prophets speak the same things, and such things
as none but the Holy Ghost could teach; and, if we must with a divine
faith believe the Old Testament to be a revelation from God, we cannot
with any good reason question the divine authority of the New, when we
consider how exactly the histories of the one agree with the prophecies
of the other, and how the dark types and shadows of the law are
illustrated and accomplished in the gospel. Nor can any person who
pretends to believe the divine authority of the historical part of the
New Testament, containing the Gospels and the Acts, with good reason
question the equal authority of the epistolary part; for the
subject-matter of all these epistles, as well as of the sermons of the
apostles, is the word of God (Rom. 10:17; 1 Th. 2:13; Col. 1:25), and
the gospel of God (Rom. 15:16; 2 Co. 11:7), and the gospel of Christ, 2
Co. 2:12. We are built upon the foundation of the apostles and prophets,
Jesus Christ himself being the chief corner-stone; and, as Moses wrote
of Christ, so did all the prophets, for the Spirit of Christ in them did
testify of him. And the apostles confirmed what Christ himself began to
teach, God also bearing them witness with signs, and wonders, and divers
miracles, and gifts of the Holy Ghost, according to his will, Heb. 2:3,
4. The manifestation of God in the flesh, and the things he began both
to do and teach until the day in which he was taken up, together with
his sufferings unto death, and his resurrection (which things are
declared to us, and are firmly to be believed, and strictly regarded by
us), do give us an ample account of the way of life and salvation by
Jesus Christ; but still it was the will of our blessed Lord that his
apostles should not only publish his gospel to all the world, but also
that, after his resurrection, they should declare some things more
plainly concerning him than he thought fit to do while he was here on
earth, for which end he promised to send his Holy Spirit to teach them
all things, to bring all things to their remembrance which he had spoken
unto them, Jn. 14:26. For he told them (Jn. 16:12, 13), I have many
things to say unto you, but you cannot bear them now; but when he, the
Spirit of truth, is come, he shall lead you into all truth, and shall
show you things to come. Accordingly we find there was a wonderful
effusion of the Holy Spirit upon the apostles (who in these epistles are
called the servants, ambassadors, and ministers of Christ, and stewards
of the mysteries of God), under whose infallible guidance they preached
the gospel, and declared the whole counsel of God, and that with amazing
courage and success, Satan every where falling down before them like
lightning from heaven. That in preaching the gospel they were under the
influence of the infallible Spirit is undeniable, from the miraculous
gifts and powers they received for their work, particularly that gift of
tongues so necessary for the publication of the gospel throughout the
world to nations of different languages; nor must we omit that mighty
power that accompanied the word preached, bringing multitudes to the
obedience of faith, notwithstanding all opposition from earth and hell,
and the potent lusts in the hearts of those who were turned from idols
to serve the living God, and to wait for his Son from heaven, whom he
raised from the dead, even Jesus, that delivered us from the wrath to
come. Now that they were under the same mighty influence in writing
these epistles as in preaching cannot be denied. Such infallible
assistance seems to be as needful at least to direct their writing as
their preaching, considering that these epistles were written to keep in
memory those things that had been delivered by word of mouth (2 Pt.
1:15), and to rectify the mistakes that might arise about some
expressions that had been used in preaching (2 Th. 2:2), and were to
remain as a standing rule and record to which believers were to appeal,
for defending the truth and discovering error, and a proper means to
transmit the truths of the gospel to posterity, even to the end of time.
Besides, the writers of these epistles have declared that what they
wrote was from God: now they must know whether they had the special
assistance of the divine Spirit or no, in their writing as well as
preaching; and they in all things appear to have been men of such
probity that they would not dare to say they had the Spirit of God when
they had it not, or if they so much as doubted whether they had it or
not; yea, they are careful, when they speak their own private opinion,
or only under some common influence, to tell the world that not the
Lord, but they, spoke those things, but that in the rest it was not they
but the Lord, 1 Co. 7:10, 12, etc. And the apostle Paul makes the
acknowledgment of this their inspiration to be a test to try those that
pretended to be prophets or spiritual: Let them (says he) acknowledge
that the things I write unto you are the commandments of the Lord, 1 Co.
14:37. And the apostle Peter gives this as the reason of his writing,
that those he wrote to might after his decease have those things always
in remembrance (2 Pt. 1:15), which afterwards he calls the commandment
of the apostles of the Lord (ch. 3:1, 2), and so of the Lord himself.
And the apostles John declareth (1 Jn. 4:6), We are of God; he that
knoweth God heareth us; he that is not of God heareth not us; by this we
know the Spirit of truth, and the spirit of error.

As to the style of these epistles, though it be necessary we should
believe a divine influence superintending the several writers of them,
yet it is not easy to explain the manner of it, nor to determine whether
and in what particulars the words they wrote were dictated to them by
the Holy Spirit, as mere amanuenses, or how far their own memories, and
reasoning faculties, and other natural or acquired endowments, were
employed under the inspection of the Spirit. We must believe that these
holy men spoke and wrote as they were moved by the Holy Ghost, that he
put them on and assisted them in this work. It is very probable that
sometimes he not only suggested the very thoughts in their minds, but
put words into their mouths, and always infallibly guided them into all
truth, both when they expounded the scriptures of the Old Testament and
when they gave rules for our faith and practice in the gospel church
state. And yet perhaps it may be allowed, without any diminution to the
authority of these epistles, that the penmen of them made some use of
their own reasoning powers and different endowments in their manner of
writing, as well as of their different sorts of chirography; and that by
this we are to account for that difference of style which has been
observed between the writings of Paul, who was brought up at the feet of
Gamaliel, and those of Peter and John, who were fishermen. The like
difference may be discerned between the style of the prophet Isaiah, who
was educated in a court, and that of Amos, who was one of the herdsmen
of Tekoa. However, the best way to understand these scriptures aright is
not to criticise too nicely upon the words and phrases, but to attend
carefully to the drift and design of these inspired writers in them.

The subject-matter of these epistles is entirely conformable to the rest
of the scriptures. In them we find frequent reference to some passages
of the Old Testament, and explanations of them: in the epistle to the
Hebrews we have the best exposition of the Levitical law. Indeed the New
Testament refers to, and in a manner builds upon, the Old, showing the
accomplishment of all the ancient promises and prophecies concerning the
Messiah, and explains all the antiquated types and shadows of the good
things that were then to come. But, besides these references to the
preceding part of holy writ, in some of these epistles there are
contained prophecies, either wholly new or at least more largely and
plainly revealed, as that in the Revelation concerning the rise, reign,
and fall of antichrist, of which great apostasy we have some account in
2 Th. 2:3, 4, and in 1 Tim. 4:1-3. And in these epistles we have several
of the great doctrines of the gospel more fully discussed than
elsewhere, particularly the doctrine of original sin, of the sin that
dwells in the regenerate, and of justification by the righteousness of
Christ, of the abolishing of the Jewish rites and ceremonies, of the
true nature and design of the seals of the new covenant, the obligations
they bring us under, and their perpetual use in the Christian church.

The general method of these epistles is such as best serves the end or
design of them, which is indeed the end of the whole scripture-practical
godliness, out of a principle of divine love, a good conscience, and
faith unfeigned. Accordingly most of the epistles begin with the great
doctrines of the gospel, the articles of the Christian faith, which,
when received, work by love, purify the conscience, and produce
evangelical obedience; and, after these principles have been laid down,
practical conclusions are drawn and urged from them. In taking this
method there is a regard paid to the nature and faculties of the soul of
man (where the understanding is to lead the way, the will, affections,
and executive powers, to follow after), and to the nature of religion in
general, which is a reasonable service. We are not to be determined by
superstitious fancies, nor by blind passions, but by a sound judgment
and good understanding in the mind and will of God. By this we are
taught how necessary it is that faith and practice, truth and holiness,
be joined together, that the performance of moral duties will never be
acceptable to God, nor available to our own salvation, without the
belief of the truth, since those who make shipwreck of the faith seldom
maintain a good conscience, and the most solemn profession of the faith
will never save those that hold the truth in unrighteousness.

The particular occasions upon which these epistles were written do not
so evidently appear in them all as in some. The first to the Corinthians
seems to have taken its rise from the unhappy divisions that so early
rose in the churches of Christ, through the emulation of the ministers
and personal affections of the people; but it does not confine itself to
that subject. That to the Galatians seems directed chiefly against those
judaizing teachers that went about to draw the Gentile converts away
from the simplicity of the gospel in doctrine and worship. The epistle
to the Hebrews is manifestly calculated to wean the converted Jews from
those Mosaical rites and ceremonies for which they retained too great a
fondness, and to reconcile them to the abolition of that economy. Those
epistles that are directed to particular persons more evidently carry
their design in them, which he that runs may read. But this is certain,
none of these epistles are of private interpretation. Most of the psalms
and of the prophecies of the Old Testament were penned or pronounced on
particular occasions, and yet they are of standing and universal use,
and very instructive even to us upon whom the ends of the world have
come. And so are those epistles that seem to have been most limited in
the rise and occasion of them. There will always be need enough to warn
Christians against uncharitable divisions, against corrupting the faith
and worship of the gospel; and, whenever the case is the same, these
epistles are as certainly directed to such churches and persons as if
they had been inscribed to them.

These general observations, we suppose, may be sufficient to introduce
the reader into the book itself; let us now take a short view of the
whole work, of which this posthumous piece is the conclusion. It is now
about fourteen years since the first part of this exposition of the
Bible was made public. In five years\' time the Old Testament was
finished in four volumes. The first volume of the New Testament was
longer in hand; for though the ever-memorable author was always fully
employed in the ordinary work of his ministry, yet those last years of
his life, in which he drew up the exposition upon the historical part of
the New Testament, were less at his own command than any other had been.
His removal to Hackney, his almost continual preaching from day to day,
his journeys to Chester, and the necessity of more frequent visits to
his friends in and about London, together with a gradual sensible decay
of health, will more than excuse the three years\' time that passed
before that was finished. And under such difficulties none but a man of
his holy zeal, unwearied industry, and great sagacity, could have gone
through such a service in that space of time. He lived not to see that
volume published, though left by him ready for the press. The church of
God was suddenly deprived of one of the most useful ministers of the
age. We have been gathering up the fragments of those feasts with which
he used to entertain his family and friends, in his delightful work of
opening the scriptures. What remains is that we recommend the whole of
this work to the acceptance and blessing of our God and Saviour, to
whose honour and interest it was from the first directed and devoted. We
need not be very solicitous about the acceptance it may meet with in the
world: what has been before published has been received and read with
great pleasure and advantage by the most serious experienced Christians
in Great Britain and Ireland; and the many loud calls there have been
for the publishing of this supplement, and reprinting the whole, leave
us no room to doubt but that it will meet with a hearty welcome. Though
it must be acknowledged that we live in an age which by feeding upon
ashes and the wind, has very much lost the relish of every thing that is
spiritual and evangelical, yet we persuade ourselves there will still be
found many who, by reason of use, have, their senses exercised to
discern both good and evil. Those that may think the expository notes
too long, especially for family worship, may easily relieve themselves,
either by reading a less part of the chapter at one time, or by
abridging the annotations, and perusing the rest when they have more
leisure; for, though it must be owned they are somewhat copious, yet we
are persuaded that those who peruse them seriously will find nothing in
them superfluous or impertinent; and, if any where some things in the
comment do not seem to flow so naturally and necessarily from the text,
we believe when they are well considered and compared it will appear
they come under the analogy and general reason of the subject, and truly
belong to it. If there be any that think this exposition of the Bible is
too plain and familiar, that it wants the beauties of oratory and the
strength of criticism, we only wish that they will read it over again
with due attention, and we are pretty confident they will find the style
natural, clear, and comprehensive; and we think they will hardly be able
to produce one valuable criticism out of the most learned commentators
but they will have it in this exposition, though couched in plain terms,
and not brought in as of a critical nature. No man was more happy than
Mr. Henry in that useful talent of making dark things plain, while too
many, that value themselves upon their criticising faculty, affect
rather to make plain things dark.

But we leave this great and good work to speak for itself, and doubt not
but it will grow in its use and esteem, and will, through the blessing
of God, help to revive and promote family religion and scriptural
knowledge, and support the credit of scripture commentaries, though
couched in human expressions. These have been always accounted the great
treasures of the church, and when done with judgment, have been so far
from lessening the authority of the Bible that they have greatly
promoted its honour and usefulness.

The following are the ministers by whom the Exposition on the Epistolary
writings, and the Revelation, was completed, as given by J. B. Williams,
Esq., LL.D.,F.S.A., in his Memoirs of the Life, Character, and Writings,
of the Rev. Matthew Henry, 8vo. p. 308.

Romans\...\...\...\...\...\...\.....Mr. \[afterwards Dr.\] John Evans.

1 Corinthians\...\...\...\...\....Mr. Simon Browne.

2 Corinthians\...\...\...\...\....Mr. Daniel Mayo.

Galatians\...\...\...\...\...\.....Mr. Joshua Bayes.

Ephesians\...\...\...\...\...\.....Mr. Samuel Rosewell.

Philippians and Colossians\...Mr. \[afterwards Dr.\] William Harris.

1, 2 Thessalonians\...\...\.....Mr. Daniel Mayo.

1, 2 Timothy\...\...\...\...\.....Mr. Benjamin Andrews Atkinson.

Titus and Philemon\...\...\.....Mr. Jeremiah Smith.

Hebrews\...\...\...\...\...\...\....Mr. William Tong.

James\...\...\...\...\...\...\...\...Dr. S. Wright.

1 Peter\...\...\...\...\...\...\....Mr. Zec. Merrill.

2 Peter\...\...\...\...\...\...\....Mr. Joseph Hill.

1, 2, and 3 John\...\...\...\....Mr. John Reynolds, of Shrewsbury.

Jude\...\...\...\...\...\...\...\....Mr. John Billingsley.

Revelation\...\...\...\...\...\....Mr. William Tong.

Introduction to Acts
====================

We have with an abundant satisfaction seen the foundation of our holy
religion laid in the history of our blessed Saviour, its great author,
which was related and left upon record by four several inspired writers,
who all agree in this sacred truth, and the incontestable proofs of it,
that Jesus is the Christ, the Son of the living God. Upon this rock the
Christian church is built. How it began to be built upon this rock comes
next to be related in this book which we have now before us, and of this
we have the testimony only of one witness; for the matters of fact
concerning Christ were much more necessary to be fully related and
attested than those concerning the apostles. Had Infinite Wisdom seen
fit, we might have had as many books of the Acts of the Apostles as we
have gospels, nay, as we might have had gospels: but, for fear of
over-burdening the world (Jn. 21:25), we have sufficient to answer the
end, if we will but make use of it. The history of this book (which was
always received as a part of the sacred canon) may be considered.

`I.` As looking back to the preceding gospels, giving light to them, and
greatly assisting our faith in them. The promises there made we here
find made good, particularly the great promises of the descent of the
Holy Ghost, and his wonderful operations, both on the apostles (whom
here in a few days we find quite other men than what the gospels left
them; no longer weak-headed and weak-hearted, but able to say that which
then they were not able to bear (Jn. 16:12) as bold as lions to face
those hardships at the thought of which they then trembled as lambs),
and also with the apostles, making the word mighty to the pulling down
of Satan\'s strong holds, which had been before comparatively preached
in vain. The commission there granted to the apostles we here find
executed, and the powers there lodged in them we here find exerted in
miracles wrought on the bodies of people-miracles of mercy, restoring
sick bodies to health and dead bodies to life-miracles of judgment,
striking rebels blind or dead; and much greater miracles wrought on the
minds of people, in conferring spiritual gifts upon them, both of
understanding and utterance; and this in pursuance of Christ\'s
purposes, and in performance of his promises, which we had in the
gospels. The proofs of Christ\'s resurrection with which the gospels
closed are here abundantly corroborated, not only by the constant and
undaunted testimony of those that conversed with him after he arose (who
had all deserted him, and one of them denied him, and would not
otherwise have been rallied again but by his resurrection, but must have
been irretrievably dispersed, and yet by that were enabled to own him
more resolutely than ever, in defiance of bonds and deaths), but by the
working of the Spirit with that testimony for the conversion of
multitudes to the faith of Christ, according to the word of Christ, that
his resurrection, the sign of the prophet Jonas, which was reserved to
the last, should be the most convincing proof of his divine mission.
Christ had told his disciples that they should be his witnesses, and
this book brings them in witnessing for him,-that they should be fishers
of men, and here we have them enclosing multitudes in the
gospel-net,-that they should be the lights of the world, and here we
have the world enlightened by them; but that day-spring from on high the
first appearing of which we there discerned we here find shining more
and more. The corn of wheat, which there fell to the ground, here
springs up and bears much fruit; the grain of mustard-seed there is here
a great tree; and the kingdom of heaven, which was then at hand, is here
set up. Christ\'s predictions of the virulent persecutions which the
preachers of the gospel should be afflicted with (though one could not
have imagined that a doctrine so well worthy of all acceptation should
meet with so much opposition) we here find abundantly fulfilled, and
also the assurances he gave them of extraordinary supports and comforts
under their sufferings. Thus, as the latter part of the history of the
Old Testament verifies the promises made to the fathers of the former
part (as appears by that famous and solemn acknowledgment of Solomon\'s,
which runs like a receipt in full, 1 Ki. 8:56, There has not failed one
word of all his good promises which he promised by the hand of Moses his
servant), so this latter part of the history of the New Testament
exactly answers to the world of Christ in the former part of it: and
thus they mutually confirm and illustrate each other.

`II.` As looking forward to the following epistles, which are an
explication of the gospels, which open the mysteries of Christ\'s death
and resurrection, the history of which we had in the gospels. This book
introduces them and is a key to them, as the history of David is to
David\'s psalms. We are members of the Christian church, that tabernacle
of God among men, and it is our honour and privilege that we are so. Now
this book gives us an account of the framing and rearing of that
tabernacle. The four gospels showed us how the foundation of that house
was laid; this shows us how the superstructure began to be raised, 1.
Among the Jews and Samaritans, which we have an account of in the former
part of this book. 2. Among the Gentiles, which we have an account of in
the latter part: from thence, and downward to our own day, we find the
Christian church subsisting in a visible profession of faith in Christ,
as the Son of God and Saviour of the world, made by his baptized
disciples, incorporated into religious societies, statedly meeting in
religious assemblies, attending on the apostles\' doctrine, and joining
in prayers and the breaking of bread, under the guidance and presidency
of men that gave themselves to prayer and the ministry of the word, and
in a spiritual communion with all in every place that do likewise. Such
a body as this thee is now in the world, which we belong to: and, to our
great satisfaction and honour, in this book we find the rise and origin
of it, vastly different from the Jewish church, and erected upon its
ruins; but undeniably appearing to be of God, and not of man. With what
confidence and comfort may we proceed in, and adhere to, our Christian
profession, as far as we find it agrees with this pattern in the mount,
to which we ought religiously to conform and confine ourselves!

Two things more are to be observed concerning this book:- `(1.)` The
penman of it. It was written by Luke, who wrote the third of the four
gospels, which bears his name; and who (as the learned Dr. Whitby shows)
was, very probably, one of the seventy disciples, whose commission (Lu.
10:1, etc.) was little inferior to that of the twelve apostles. This
Luke was very much a companion of Paul in his services and sufferings.
Only Luke is with me, 2 Tim. 4:11. We may know by his style in the
latter part of this book when and where he was with him, for then he
writes, We did so and so, as ch. 16:10; 20:6; and thenceforward to the
end of the book. He was with Paul in his dangerous voyage to Rome, when
he was carried thither a prisoner, was with him when from his prison
there he wrote his epistles to the Colossians and Philemon, in both
which he is named. And it should seem that St. Luke wrote this history
when he was with St. Paul at Rome, during his imprisonment there, and
was assistant to him; for the history concludes with St. Paul\'s
preaching there in his own hired house. `(2.)` The title of it: The Acts
of the Apostles; of the holy Apostles, so the Greek copies generally
read it, and so they are called, Rev. 18:20, Rejoice over her you holy
apostles. One copy inscribes it, The Acts of the Apostles by Luke the
Evangelist. `[1.]` It is the history of the apostles; yet there is in it
the history of Stephen, Barnabas, and some other apostolical men, who,
though not of the twelve, were endued with the same Spirit, and employed
in the same work; and, of those that were apostles, it is the history of
Peter and Paul only that is here recorded (and Paul was now of the
twelve), Peter the apostle of the circumcision, and Paul the apostles of
the Gentiles, Gal. 2:7. But this suffices as a specimen of what the rest
did in other places, pursuant to their commission, for there were none
of them idle; and as we are to think what is related in the gospels
concerning Christ sufficient, because Infinite Wisdom thought so, the
same we are to think here concerning what is related of the apostles and
their labours; for what more is told us from tradition of the labours
and sufferings of the apostles, and the churches they planted, is
altogether doubtful and uncertain, and what I think we cannot build upon
with any satisfaction at all. This is gold, silver, and precious stones,
built upon the foundation: that is wood, hay, and stubble. `[2.]` It is
called their acts, or doings; Gesta apostolorum; so some. Praxeis-their
practices of the lessons their Master had taught them. The apostles
where active men; and though the wonders they did were by the word, yet
they are fitly called their acts; they spoke, or rather the Spirit by
them spoke, and it was done. The history is filled with their sermons
and their sufferings; yet so much did they labour in their preaching,
and so voluntarily did they expose themselves to sufferings, and such
were their achievements by both, that they may very well be called their
acts.
