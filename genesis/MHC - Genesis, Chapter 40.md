Genesis, Chapter 40
===================

Commentary
----------

In this chapter things are working, though slowly, towards Joseph\'s
advancement. `I.` Two of Pharaoh\'s servants are committed to prison, and
there to Joseph\'s care, and so become witnesses of his extraordinary
conduct (v. 1-4). `II.` They dreamed each of them a dream, which Joseph
interpreted (v. 5-19), and the event verified the interpretation (v.
20-22), and so they became witnesses of his extraordinary skill. `III.`
Joseph recommends his case to one of them, whose preferment he foresaw
(v. 14, 15), but in vain (v. 23).

### Verses 1-4

We should not have had this story of Pharaoh\'s butler and baker
recorded in scripture if it had not been serviceable to Joseph\'s
preferment. The world stands for the sake of the church, and is governed
for its good. Observe, 1. Two of the great officers of Pharaoh\'s court,
having offended the king, are committed to prison. Note, High places are
slippery places; nothing more uncertain than the favour of princes.
Those that make God\'s favour their happiness, and his service their
business, will find him a better Master than Pharaoh was, and not so
extreme to mark what they do amiss. Many conjectures there are
concerning the offence of these servants of Pharaoh; some make it no
less than an attempt to take away his life, others no more than the
casual lighting of a fly into his cup and a little sand into his bread.
Whatever it was, Providence by this means brought them into the prison
where Joseph was. 2. The captain of the guard himself, who was Potiphar,
charged Joseph with them (v. 4), which intimates that he began now to be
reconciled to him, and perhaps to be convinced of his innocence, though
he durst not release him for fear of disobliging his wife. John Baptist
must lose his head, to please Herodias.

### Verses 5-19

Observe, `I.` The special providence of God, which filled the heads of
these two prisoners with unusual dreams, such as made extraordinary
impressions upon them, and carried with them evidences of a divine
origin, both in one night. Note, God has immediate access to the spirits
of men, which he can make serviceable to his own purposes whenever he
pleases, quite beyond the intention of those concerned. To him all
hearts are open, and anciently he spoke not only to his own people, but
to others, in dreams, Job 33:15. Things to come were thus foretold, but
very obscurely.

`II.` The impression which was made upon these prisoners by their dreams
(v. 6): They were sad. It was not the prison that made them sad (they
were pretty well used to that, and perhaps lived jovially there), but
the dream. Note, God has more ways than one to sadden the spirits of
those that are to be made sad. Those sinners that are hardy enough under
outward troubles, and will not yield to them, yet God can find out a way
to punish; he can take off their wheels, by wounding their spirits, and
laying loads upon them.

`III.` Joseph\'s great tenderness and compassion towards them. He
enquired with concern, Wherefore look you so sadly to-day? v. 7. Joseph
was their keeper, and in that office he was mild. Note, It becomes us to
take cognizance of the sorrows even of those that are under our check.
Joseph was their companion in tribulation, he was now a prisoner with
them, and had been a dreamer too. Note, Communion in sufferings helps to
work compassion towards those that do suffer. Let us learn hence, 1. To
concern ourselves in the sorrows and troubles of others, and to enquire
into the reason of the sadness of our brethren\'s countenances; we
should be often considering the tears of the oppressed, Eccl. 4:1. It is
some relief to those that are in trouble to be taken notice of. 2. To
enquire into the causes of our own sorrow, \"Wherefore do I look so
sadly? Is there a reason? Is it a good reason? Is there not a reason for
comfort sufficient to balance it, whatever it is? Why art thou cast
down, O my soul?\"

`IV.` The dreams themselves, and the interpretation of them. That which
troubled these prisoners was that being confined they could not have
recourse to the diviners of Egypt who pretended to interpret dreams:
There is no interpreter here in the prison, v. 8. Note, There are
interpreters which those that are in prison and sorrow should wish to
have with them, to instruct them in the meaning and design of Providence
(Elihu alludes to such, when he says, If there be an interpreter, one
among a thousand, to show unto man his uprightness, Job 33:23, 24),
interpreters to guide their consciences, not to satisfy their curiosity.
Joseph hereupon directed them which way to look: Do not interpretations
belong to God? He means the God whom he worshipped, to the knowledge of
whom he endeavours hereby to lead them. Note, It is God\'s prerogative
to foretel things to come, Isa. 46:10. He must therefore have the praise
of all the gifts of foresight which men have, ordinary or extraordinary.
Joseph premises a caveat against his own praise, and is careful to
transmit the glory to God, as Daniel, ch. 2:30. Joseph suggests, \"If
interpretations belong to God, he is a free agent, and may communicate
the power to whom he pleases, and therefore tell me your dreams.\" Now,
`1.` The chief butler\'s dream was a happy presage of his enlargement, and
re-advancement, within three days; and so Joseph explained it to him, v.
12, 13. Probably it had been usual with him to press the full-ripe
grapes immediately into Pharaoh\'s cup, the simplicity of that age not
being acquainted with the modern arts of making the wine fine. Observe,
Joseph foretold the chief butler\'s deliverance, but he did not foresee
his own. He had long before dreamt of his own honour, and the obeisance
which his brethren should do to him, with the remembrance of which he
must now support himself, without any new or fresh discoveries. The
visions that are for the comfort of God\'s saints are for a great while
to come, and relate to things that are very far off, while the
foresights of others, like this recorded there, look but three days
before them. 2. The chief baker\'s dream portended his ignominious
death, v. 18, 19. The happy interpretation of the other\'s dream
encouraged him to relate his. Thus hypocrites, when they hear good
things promised to good Christians, would put in for a share, though
they have no part nor lot in the matter. It was not Joseph\'s fault that
he brought him no better tidings. Ministers are but interpreters, they
cannot make the thing otherwise than it is; if therefore they deal
faithfully, and their message prove unpleasing, it is not their fault.
Bad dreams cannot expect a good interpretation.

`V.` The improvement Joseph made of this opportunity to get a friend at
court, v. 14, 15. He modestly bespoke the favour of the chief butler,
whose preferment he foretold: But think of me when it shall be well with
thee. Though the respect paid to Joseph made the prison as easy to him
as a prison could be, yet none can blame him for being desirous of
liberty. See here, 1. What a modest representation he makes of his own
case, v. 15. He does not reflect upon his brethren that sold him; he
only says, I was stolen out of the land of the Hebrews, that is,
unjustly sent thence, no matter where the fault was. Nor does he reflect
on the wrong done him in this imprisonment by his mistress that was his
prosecutrix, and his master that was his judge; but mildly avers his own
innocence: Here have I done nothing that they should put me into the
dungeon. Note, When we are called to vindicate ourselves we should
carefully avoid, as much as may be, speaking ill of others. Let us be
content to prove ourselves innocent, and not be fond of upbraiding
others with their guilt. 2. What a modest request he makes to the chief
butler: \"Only, think of me. Pray do me a kindness, if it lie in your
way.\" And his particular petition is, Bring me out of this house. He
does not say, \"Bring me into Pharaoh\'s house, get me a place at
court.\" No, he begs for enlargement, not preferment. Note, Providence
sometimes designs the greatest honours for those that least covet or
expect them.

### Verses 20-23

Here is, 1. The verifying of Joseph\'s interpretation of the dreams, on
the very day prefixed. The chief butler and baker were both advanced,
one to his office, the other to the gallows, and both at the three
days\' end. Note, Very great changes, both for the better and for the
worse, often happen in a very little time, so sudden are the revolutions
of the wheel of nature. The occasion of giving judgement severally upon
their case was the solemnizing of Pharaoh\'s birth-day, on which, all
his servants being obliged by custom to attend him, these two came to be
enquired after, and the cause of their commitment looked into. The
solemnizing of the birth-day of princes has been an ancient piece of
respect done them; and if it be not abused, as Jeroboam\'s was (Hos.
7:5), and Herod\'s (Mk. 6:21), is a usage innocent enough: and we may
all profitably take notice of our birth-days, with thankfulness for the
mercies of our birth, sorrow for the sinfulness of it, and an
expectation of the day of our death as better than the day of our birth.
On Pharaoh\'s birth-day he lifted up the head of these two prisoners,
that is, arraigned and tried them (when Naboth was tried he was set on
high among the people, 1 Ki. 21:9), and he restored the chief butler,
and hanged the chief baker. If the butler was innocent and the baker
guilty, we must own the equity of Providence in clearing up the
innocency of the innocent, and making the sin of the guilty to find him
out. If both were either equally innocent or equally guilty, it is an
instance of the arbitrariness of such great princes as pride themselves
in that power which Nebuchadnezzar set up for (Dan. 5:19, whom he would
he slew, and whom he would he kept alive), forgetting that there is a
higher than they, to whom they are accountable. 2. The disappointing of
Joseph\'s expectation from the chief butler: He remembered not Joseph,
but forgot him, v. 23. `(1.)` See here an instance of base ingratitude;
Joseph had deserved well at his hands, had ministered to him,
sympathized with him, helped him to a favourable interpretation of his
dream, had recommended himself to him as an extraordinary person upon
all accounts; and yet he forgot him. We must not think it strange if in
this world we have hatred shown us for our love, and slights for our
respects. `(2.)` See how apt those that are themselves at ease are to
forget others in distress. Perhaps it is in allusion to this story that
the prophet speaks of those that drink wine in bowls, and are not
grieved for the affliction of Joseph, Amos 6:6. Let us learn hence to
cease from man. Joseph perhaps depended too much upon his interest in
the chief butler, and promised himself too much from him; he learned by
his disappointment to trust in God only. We cannot expect too little
from man nor too much from God.

Some observe the resemblance between Joseph and Christ in this story.
Joseph\'s fellow-sufferers were like the two thieves that were crucified
with Christ-the one saved, the other condemned. (It is Dr. Lightfoot\'s
remark, from Mr. Broughton.) One of these, when Joseph said to him,
Remember me when it shall be well with thee, forget him; but one of
those, when he said to Christ, Remember me when thou comest into thy
kingdom, was not forgotten. We justly blame the chief butler\'s
ingratitude to Joseph, yet we conduct ourselves much more disingenuously
towards the Lord Jesus. Joseph had but foretold the chief butler\'s
enlargement, but Christ wrought out ours, mediated with the King of
kings for us; yet we forget him, though often reminded of him, though we
have promised never to forget him: thus ill do we requite him, like
foolish people and unwise.
