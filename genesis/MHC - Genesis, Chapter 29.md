Genesis, Chapter 29
===================

Commentary
----------

This chapter gives us an account of God\'s providences concerning Jacob,
pursuant to the promises made to him in the foregoing chapter. `I.` How he
was brought in safety to his journey\'s end, and directed to his
relations there, who bade him welcome (v. 1-14). `II.` How he was
comfortably disposed of in marriage (v. 15-30). `III.` How his family was
built up in the birth of four sons (v. 31-35). The affairs of princes
and mighty nations that were then in being are not recorded in the book
of God, but are left to be buried in oblivion; while these small
domestic concerns of holy Jacob are particularly recorded with their
minute circumstances, that they may be in everlasting remembrance. For
\"the memory of the just is blessed.\"

### Verses 1-8

All the stages Israel\'s march to Canaan are distinctly noticed, but no
particular journal is kept of Jacob\'s expedition further than Beth-el;
no, he had no more such happy nights as he had at Beth-el, no more such
visions of the Almighty. That was intended for a feast; he must not
expect it to be his daily bread. But, 1. We are here told how cheerfully
he proceeded in his journey after the sweet communion he had with God at
Beth-el: Then Jacob lifted up his feet; so the margin reads it, v. 1.
Then he went on with cheerfulness and alacrity, not burdened with his
cares, nor cramped with his fears, being assured of God\'s gracious
presence with him. Note, After the visions we have had of God, and the
vows we have made to him in solemn ordinances, we should run the way of
his commandments with enlarged hearts, Heb. 12:1. 2. How happily he
arrived at his journey\'s end. Providence brought him to the very field
where his uncle\'s flocks were to be watered, and there he met with
Rachel, who was to be his wife. Observe, `(1.)` The divine Providence is
to be acknowledged in all the little circumstances which concur to make
a journey, or other undertaking, comfortable and successful. If, when we
are at a loss, we meet seasonably with those that can direct us-if we
meet with a disaster, and those are at hand that will help us-we must
not say that it was by chance, nor that fortune therein favoured us, but
that it was by Providence, and that God therein favoured us. Our ways
are ways of pleasantness, if we continually acknowledge God in them.
`(2.)` Those that have flocks must look well to them, and be diligent to
know their state, Prov. 27:23. What is here said of the constant care of
the shepherds concerning their sheep (v. 2, 3, 7, 8) may serve to
illustrate the tender concern which our Lord Jesus, the great Shepherd
of the sheep, has for his flock, the church; for he is the good
Shepherd, that knows his sheep, and is known of them, Jn. 10:14. The
stone at the well\'s mouth, which is so often mentioned here, was either
to secure their property in it (for water was scarce, it was not there
usus communis aquarum-for every one\'s use), or it was to save the well
from receiving damage from the heat of the sun, or from any spiteful
hand, or to prevent the lambs of the flock from being drowned in it.
`(3.)` Separate interests should not take us from joint and mutual help;
when all the shepherds came together with their flocks, then, like
loving neighbours, at watering-time, they watered their flocks together.
`(4.)` It becomes us to speak civilly and respectfully to strangers.
Though Jacob was no courtier, but a plain man, dwelling in tents, and a
stranger to compliment, yet he addresses himself very obligingly to the
people he met with, and calls them his brethren, v. 4. The law of
kindness in the tongue has a commanding power, Prov. 31:26. Some think
he calls them brethren because they were of the same trade, shepherds
like him. Though he was now upon his preferment, he was not ashamed of
his occupation. `(5.)` Those that show respect have usually respect shown
to them. As Jacob was civil to these strangers, so he found them civil
to him. When he undertook to teach them how to despatch their business
(v. 7), they did not bid him meddle with his own concerns and let them
alone; but, though he was a stranger, they gave him the reason of their
delay, v. 8. Those that are neighbourly and friendly shall have
neighbourly and friendly usage.

### Verses 9-14

Here we see, 1. Rachel\'s humility and industry: She kept her father\'s
sheep (v. 9), that is, she took the care of them, having servants under
her that were employed about them. Rachel\'s name signifies a sheep.
Note, Honest useful labour is that which nobody needs be ashamed of, nor
ought it to be a hindrance to any one\'s preferment. 2. Jacob\'s
tenderness and affection. When he understood that this was his kinswoman
(probably he had heard of her name before), knowing what his errand was
into that country, we may suppose it struck his mind immediately that
his must be his wife. Being already smitten with her ingenuous comely
face (though it was probably sun-burnt, and she was in the homely dress
of a shepherdess), he is wonderfully officious, and anxious to serve her
(v. 10), and addresses himself to her with tears of joy and kisses of
love, v. 11. She runs with all haste to tell her father; for she will by
no means entertain her kinsman\'s address without her father\'s
knowledge and approbation, v. 12. These mutual respects, at their first
interview, were good presages of their being a happy couple. 3.
Providence made that which seemed contingent and fortuitous to give
speedy satisfaction to Jacob\'s mind, as soon as ever he came to the
place which he was bound for. Abraham\'s servant, when he came upon a
similar errand, met with similar encouragement. Thus God guides his
people with his eye, Ps. 32:8. It is a groundless conceit which some of
the Jewish writers have, that Jacob, when he kissed Rachel, wept because
he had been set upon in his journey by Eliphaz the eldest son of Esau,
at the command of his father, and robbed of all his money and jewels,
which his mother had given him when she sent him away. It was plain that
it was his passion for Rachel, and the surprise of this happy meeting,
that drew these tears from his eyes. 4. Laban, though none of the
best-humoured men, bade him welcome, was satisfied in the account he
gave of himself, and of the reason of his coming in such poor
circumstances. While we avoid the extreme, on the one hand, of being
foolishly credulous, we must take heed of falling into the other
extreme, of being uncharitably jealous and suspicious. Laban owned him
for his kinsman: Thou art my bone and my flesh, v. 14. Note, Those are
hard-hearted indeed that are unkind to their relations, and that hide
themselves from their own flesh, Isa. 58:7.

### Verses 15-30

Here is, `I.` The fair contract made between Laban and Jacob, during the
month that Jacob spent there as a guest, v. 14. It seems he was not
idle, nor did he spend his time in sport and pastime; but like a man of
business, though he had no stock of his own, he applied himself to serve
his uncle, as he had begun (v. 10) when he watered his flock. Note,
Wherever we are, it is good to be employing ourselves in some useful
business, which will turn to a good account to ourselves or others.
Laban, it seems, was so taken with Jacob\'s ingenuity and industry about
his flocks that he was desirous he should continue with him, and very
fairly reasons thus: \"Because thou art my brother, shouldst thou
therefore serve me for nought? v. 15. No, what reason for that?\" If
Jacob be so respectful to his uncle as to give him his service without
demanding any consideration for it, yet Laban will not be so unjust to
his nephew as to take advantage either of his necessity or of his
good-nature. Note, Inferior relations must not be imposed upon; if it be
their duty to serve us, it is our duty to reward them. Now Jacob had a
fair opportunity to make known to Laban the affection he had for his
daughter Rachel; and, having no worldly goods in his hand with which to
endow her, he promises him seven years\' service, upon condition that,
at the end of the seven years, he would bestow her upon him for his
wife. It appears by computation that Jacob was now seventy-seven years
old when he bound himself apprentice for a wife, and for a wife he kept
sheep, Hos. 12:12. His posterity are there reminded of it long
afterwards, as an instance of the meanness of their origin: probably
Rachel was young, and scarcely marriageable, when Jacob first came,
which made him the more willing to stay for her till his seven years\'
service had expired.

`II.` Jacob\'s honest performance of his part of the bargain, v. 20. He
served seven years for Rachel. If Rachel still continued to keep her
father\'s sheep (as she did, v. 9), his innocent and religious
conversation with her, while they kept the flocks, could not but
increase their mutual acquaintance and affection (Solomon\'s song of
love is a pastoral); if she now left it off, his easing her of that care
was very obliging. Jacob honestly served out his seven years, and did
not forfeit his indentures, though he was old; nay, he served them
cheerfully: They seemed to him but a few days, for the love he had to
her, as if it were more his desire to earn her than to have her. Note,
Love makes long and hard services short and easy; hence we read of the
labour of love, Heb. 6:10. If we know how to value the happiness of
heaven, the sufferings of this present time will be as nothing to us in
comparison of it. An age of work will be but as a few days to those that
love God and long for Christ\'s appearing.

`III.` The base cheat which Laban put upon him when he was out of his
time: he put Leah into his arms instead of Rachel, v. 23. This was
Laban\'s sin; he wronged both Jacob and Rachel, whose affections,
doubtless, were engaged to each other, and, if (as some say) Leah was
herein no better than an adulteress, it was no small wrong to her too.
But it was Jacob\'s affliction, a damp to the mirth of the
marriage-feast, when in the morning behold it was Leah, v. 25. It is
easy to observe here how Jacob was paid in his own coin. He had cheated
his own father when he pretended to be Esau, and now his father-in-law
cheated him. Herein, how unrighteous soever Laban was, the Lord was
righteous; as Judges 1:7. Even the righteous, if they take a false step,
are sometimes thus recompensed on the earth. Many that are not, like
Jacob, disappointed in the person, soon find themselves, as much to
their grief, disappointed in the character. The choice of that relation
therefore, on both sides, ought to be made with good advice and
consideration, that, if there should be a disappointment, it may not be
aggravated by a consciousness of mismanagement.

`IV.` The excuse and atonement Laban made for the cheat. 1. The excuse
was frivolous: It must not be so done in our country, v. 26. We have
reason to think there was no such custom of his country as he pretends;
only he banters Jacob with it, and laughs at his mistake. Note, Those
that can do wickedly and then think to turn it off with a jest, though
they may deceive themselves and others, will find at last that God is
not mocked. But if there had been such a custom, and he had resolved to
observe it, he should have told Jacob so when he undertook to serve him
for his younger daughter. Note, As saith the proverb of the ancients,
Wickedness proceeds from the wicked, 1 Sa. 24:13. Those that deal with
treacherous men must expect to be dealt treacherously with 2. His
compounding the matter did but make bad worse: We will give thee this
also, v. 27. Hereby he drew Jacob into the sin, and snare, and disquiet,
of multiplying wives, which remains a blot in his escutcheon, and will
be so to the end of the world. Honest Jacob did not design it, but to
have kept as true to Rachel as his father had done to Rebekah. He that
had lived without a wife to the eighty-fourth year of his age could then
have been very well content with one; but Laban, to dispose of his two
daughters without portions, and to get seven years\' service more out of
Jacob, thus imposes upon him, and draws him into such a strait by his
fraud, that (the matter not being yet settled, as it was afterwards by
the divine law, Lev. 18:18, and more fully since by our Saviour, Mt.
19:5) he had some colourable reasons for marrying them both. He could
not refuse Rachel, for he had espoused her; still less could he refuse
Leah, for he had married her; and therefore Jacob must be content, and
take two talents, 2 Kings v. 23. Note, One sin is commonly the inlet of
another. Those that go in by one door of wickedness seldom find their
way out but by another. The polygamy of the patriarchs was, in some
measure, excusable in them, because, though there was a reason against
it as ancient as Adam\'s marriage (Mal. 2:15), yet there was no express
command against it; it was in them a sin of ignorance. It was not he
product of any sinful lust, but for the building up of the church, which
was the good that Providence brought out of it; but it will by no means
justify the like practice now, when God\'s will is plainly made known,
that one man and one woman only must be joined together, 1 Co. 7:2. The
having of many wives suits well enough with the carnal sensual spirit of
the Mahomedan imposture, which allows it; but we have not so learned
Christ. Dr. Lightfoot makes Leah and Rachel to be figures of the two
churches, the Jews under the law and the Gentiles under the gospel: the
younger the more beautiful, and more in the thoughts of Christ when he
came in the form of a servant; but he other, like Leah, first embraced:
yet in this the allegory does not hold, that the Gentiles, the younger,
were more fruitful, Gal. 4:27.

### Verses 31-35

We have here the birth of four of Jacob\'s sons, all by Leah. Observe,
`1.` That Leah, who was less beloved, was blessed with children, when
Rachel was denied that blessing, v. 31. See how Providence, in
dispensing its gifts, observes a proportion, to keep the balance even,
setting crosses and comforts one over-against another, that none may be
either too much elevated or too much depressed. Rachel wants children,
but she is blessed with her husband\'s love; Leah wants that, but she is
fruitful. Thus it was between Elkana\'s two wives (1 Sa. 1:5); for the
Lord is wise and righteous. When the Lord saw that Leah was hated, that
is, loved less than Rachel, in which sense it is required that we hate
father and mother, in comparison with Christ (Lu. 14:26), then the Lord
granted her a child, which was a rebuke to Jacob, for making so great a
difference between those that he was equally related to,-a check to
Rachel, who perhaps insulted over her sister upon that account,-and a
comfort to Leah, that she might not be overwhelmed with the contempt put
upon her: thus God giveth abundant honour to that which lacked, 1 Co.
12:24. 2. The names she gave her children were expressive of her
respectful regards both to God and to her husband. `(1.)` She appears very
ambitious of her husband\'s love: she reckoned the want of it her
affliction (v. 32); not upbraiding him with it as his fault, nor
reproaching him for it, and so making herself uneasy to him, but laying
it to heart as her grief, which yet she had reason to bear with the more
patience because she herself was consenting to the fraud by which she
became his wife; and we may well bear that trouble with patience which
we bring upon ourselves by our own sin and folly. She promised herself
that the children she bore him would gain her the interest she desired
in his affections. She called her first-born Reuben (see a son), with
this pleasant thought, Now will my husband love me; and her third son
Levi (joined), with this expectation, Now will my husband by joined unto
me, v. 34. Mutual affection is both the duty and comfort of that
relation; and yoke-fellows should study to recommend themselves to each
other, 1 Co. 7:33, 34. `(2.)` She thankfully acknowledges the kind
providence of God in it: The Lord hath looked upon my affliction, v. 32.
\"The Lord hath heard, that is, taken notice of it, that I was hated
(for our afflictions, as they are before God\'s eyes, so they have a cry
in his ears), he has therefore given me this son.\" Note, Whatever we
have that contributes either to our support and comfort under our
afflictions or to our deliverance from them, God must be owned in it,
especially his pity and tender mercy. Her fourth she called Judah
(praise), saying, Now will I praise the Lord, v. 35. And this was he of
whom, as concerning the flesh, Christ came. Note, `[1.]` Whatever is the
matter of our rejoicing ought to be the matter of our thanksgiving.
Fresh favours should quicken us to praise God for former favours. Now
will I praise the Lord more and better than I have done. `[2.]` All our
praises must centre in Christ, both as the matter of them and as the
Mediator of them. He descended from him whose name was praise, for he is
our praise. Is Christ formed in my heart? Now will I praise the Lord.
