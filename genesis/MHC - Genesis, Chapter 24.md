Genesis, Chapter 24
===================

Commentary
----------

Marriages and funerals are the changes of families, and the common news
among the inhabitants of the villages. In the foregoing chapter we had
Abraham burying his wife, here we have him marrying his son. These
stories concerning his family, with their minute circumstances, are
largely related, while the histories of the kingdoms of the world then
in being, with their revolutions, are buried in silence; for the Lord
knows those that are his. The subjoining of Isaac\'s marriage to
Sarah\'s funeral (with a particular reference to it, v. 67) shows us
that as \"one generation passes away another generation comes;\" and
thus the entail both of the human nature, and of the covenant, is
preserved. Here is, `I.` Abraham\'s care about the marrying of his son,
and the charge he gave to his servant about it (v. 1-9). `II.` His
servant\'s journey into Abraham\'s country, to seek a wife for his young
master among his own relations (v. 10-14). `III.` The kind providence
which brought him acquainted with Rebekah, whose father was Isaac\'s
cousin-german (v. 15-28). `IV.` The treaty of marriage with her relations
(v. 29-49). `V.` Their consent obtained (v. 50-60). `VI.` The happy meeting
and marriage between Isaac and Rebekah (v. 61, etc.).

### Verses 1-9

Three things we may observe here concerning Abraham:-

`I.` The care he took of a good son, to get him married, well married. It
was high time to think of it now, for Isaac was about forty years old,
and it had been customary with his ancestors to marry at thirty, or
sooner, ch. 11:14, 18, 22, 24. Abraham believed the promise of the
building up of his family, and therefore did not make haste; not more
haste than good speed. Two considerations moved him to think of it now
(v. 1):-1. That he himself was likely to leave the world quickly, for he
was old, and well-stricken in age, and it would be a satisfaction to him
to see his son settled before he died; and, 2. That he had a good estate
to leave behind him, for the Lord had blessed him in all things; and the
blessing of the Lord makes rich. See how much religion and piety
befriend outward prosperity. Now Abraham\'s pious care concerning his
son was, `(1.)` That he should not marry a daughter of Canaan, but one of
his kindred. He saw that the Canaanites were degenerating into great
wickedness, and knew by revelation that they were designed for ruin, and
therefore he would not marry his son among them, lest they should be
either a snare to his soul, or at least a blot to his name. `(2.)` That
yet he should not leave the land of Canaan, to go himself among his
kindred, not even for the purpose of choosing a wife, lest he should be
tempted to settle there. This caution is given v. 6, and repeated, v. 8.
\"Bring not my son thither again, whatever comes of it. Let him rather
want a wife than expose himself to that temptation.\" Note, Parents in
disposing of their children, should carefully consult the welfare of
their souls, and their furtherance in the way to heaven. Those who
through grace have escaped the corruption that is in the world through
lust, and have brought up their children accordingly, should take heed
of doing any thing by which they may be again entangled therein and
overcome, 2 Pt. 2:20. Beware that you bring them not thither again, Heb.
11:15.

`II.` The charge he gave to a good servant, probably Eliezer of Damascus,
one of whose conduct, fidelity, and affection to him and his family, he
had had long experience. He trusted him with this great affair, and not
Isaac himself, because he would not have Isaac go at all into that
country, but marry there by proxy; and no proxy so fit as this steward
of his house. This matter is settled between the master and the servant
with a great deal of care and solemnity. 1. The servant must be bound by
an oath to do his utmost to get a wife for Isaac from among his
relations, v. 2-4. Abraham swears him to it, both for his own
satisfaction and for the engagement of his servant to all possible care
and diligence in this matter. Thus God swears his servants to their
work, that, having sworn, they may perform it. Honour is here done to
the eternal God; for he it is that is sworn by, to whom alone these
appeals ought to be made. And some think honour is done to the covenant
of circumcision by the ceremony here used of putting his hand under his
thigh. Note, Swearing being an ordinance not peculiar to the church, but
common to mankind, is to be performed by such signs as are the
appointments and common usages of our country, for binding the person
sworn. 2. He must be clear of this oath if, when he had done his utmost,
he could not prevail. This proviso the servant prudently inserted (v.
5), putting the case that the woman would not follow him; and Abraham
allowed the exception, v. 8. Note, Oaths are to be taken with great
caution, and the matter sworn to should be rightly understood and
limited, because it is a snare to devour that which is holy, and, after
vows, to make the enquiry which should have been made before.

`III.` The confidence he put in a good God, who, he doubts not, will give
his servant success in this undertaking, v. 7. He remembers that God had
wonderfully brought him out of the land of his nativity, by the
effectual call of his grace; and therefore doubts not but he will
succeed him in his care not to bring his son thither again. He remembers
also the promise God had made and confirmed to him that he would give
Canaan to his seed, and thence infers that God would own him in his
endeavours to match his son, not among those devoted nations, but to one
that was fit to be the mother of such a seed. \"Fear not therefore; he
shall send his angel before thee to make thy way prosperous.\" Note, 1.
Those that carefully keep in the way of duty, and govern themselves by
the principles of their religion in their designs and undertakings, have
good reason to expect prosperity and success in them. God will cause
that to issue in our comfort in which we sincerely aim at his glory. 2.
God\'s promises, and our own experiences, are sufficient to encourage
our dependence upon God, and our expectations from him, in all the
affairs of this life. 3. God\'s angels are ministering spirits, sent
forth, not only for the protection, but for the guidance, of the heirs
of promise, Heb. 1:14. \"He shall send his angel before thee, and then
thou wilt speed well.\"

### Verses 10-28

Abraham\'s servant now begins to make a figure in this story; and,
though he is not named, yet much is here recorded to his honour, and for
an example to all servants, who shall be honoured if, by faithfully
serving God and their masters, they adorn the doctrine of Christ
(compare Prov. 27:18 with Titus 2:10); for there is no respect of
persons with God, Col. 3:24, 25. A good servant that makes conscience of
the duty of his place, and does it in the fear of God, though he make
not a figure in the world nor have praise of men, yet shall be owned and
accepted of God and have praise of him. Observe here,

`I.` How faithful Abraham\'s servant approved himself to his master.
Having received his charge, he with all expedition set out on his
journey, with an equipage suitable to the object of his negotiation (v.
10), and he had all the goods of his master, that is, a schedule or
particular account of them, in his hand, to show to those with whom he
was to treat; for, from first to last, he consulted his master\'s
honour. Isaac being a type of Christ, some make this fetching of a wife
for him to signify the espousing of the church by the agency of his
servants the ministers. The church is the bride, the Lamb\'s wife, Rev.
21:9. Christ is the bridegroom, and ministers are the friends of the
bridegroom (Jn. 3:29), whose work it is to persuade souls to consent to
him, 2 Co. 11:2. The spouse of Christ must not be of the Canaanites, but
of his own kindred, born again from above. Ministers, like Abraham\'s
servant, must lay out themselves with the utmost wisdom and care to
serve their master\'s interest herein.

`II.` How devoutly he acknowledged God in this affair, like one of that
happy household which Abraham had commanded to keep the way of the Lord,
etc., ch. 18:19. He arrived early in the evening (after many days\'
journeying) at the place of his destination, and reposed himself by a
well of water, to consider how he might manage his business for the
best. And,

`1.` He acknowledges God by a particular prayer (v. 12-14), wherein, `(1.)`
He petitions for prosperity and good success in this affair: Send me
good speed, this day. Note, We have leave to be particular in
recommending our affairs to the conduct and care of the divine
Providence. Those that would have good speed must pray for it. This day,
in this affair; thus we must, in all our ways, acknowledge God, Prov.
3:6. And, if we thus look up to God in every undertaking which we are in
care about, we shall have the comfort of having done our duty, whatever
the issue be. `(2.)` He pleads God\'s covenant with his master Abraham: O
God of my master Abraham, show kindness to him. Note, As the children of
good parents, so the servants of good masters, have peculiar
encouragement in the prayers they offer to God for prosperity and
success. `(3.)` He proposes a sign (v. 14), not by it to limit God, nor
with a design to proceed no further if he were not gratified in it; but
it is a prayer, `[1.]` That God would provide a good wife for his young
master, and this was a good prayer. He knew that a prudent wife is from
the Lord (Prov. 19:14), and therefore that for this he will be enquired
of. He desires that his master\'s wife might be humble and industrious
woman, bred up to care and labour, and willing to put her hand to any
work that was to be done; and that she might be of a courteous
disposition, and charitable to strangers. When he came to seek a wife
for his master, he did not go to the playhouse or the park, and pray
that he might meet one there, but to the well of water, expecting to
find one there well employed. `[2.]` That he would please to make his
way, in this matter, plain and clear before him, by the concurrence of
minute circumstances in his favour. Note, First, It is the comfort, as
well as the belief, of a good man, that God\'s providence extends itself
to the smallest occurrences and admirably serves its own purposes by
them. Our times are in God\'s hand; not only events themselves, but the
times of them. Secondly, It is our wisdom, in all our affairs, to follow
Providence, and folly to force it. Thirdly, It is very desirable, and
that which we may lawfully pray for, while in the general we set God\'s
will before us as our rule, that he will, by hints of providence, direct
us in the way of our duty, and give us indications what his mind it.
Thus he guides his people with his eye (Ps. 32:8), and leads them in a
plain path, Ps. 27:11.

`2.` God owns him by a particular providence. He decreed the thing, and
it was established to him, Job 22:28. According to his faith, so was it
unto him. The answer to this prayer was, `(1.)` Speedy-before he had made
an end of speaking (v. 15), as it is written (Isa. 65:24), While they
are yet speaking, I will hear. Though we are backward to pray, God is
forward to hear prayer. `(2.)` Satisfactory: the first that came to draw
water was, and did, in every thing, according to his own heart. `[1.]`
She was so well qualified that in all respects she answered the
characters he wished for in the woman that was to be his master\'s wife,
handsome and healthful, humble and industrious, very courteous and
obliging to a stranger, and having all the marks of a good disposition.
When she came to the well (v. 16), she went down and filled her pitcher,
and came up to go home with it. She did not stand to gaze upon the
strange man and his camels, but minded her business, and would not have
been diverted from it but by an opportunity of doing good. She did not
curiously nor confidently enter into discourse with him, but modestly
answered him, with all the decorum that became her sex. What a
degenerate age do we live in, in which appear all the instances of
pride, luxury, and laziness, the reverse of Rebekah\'s character, whose
daughters few are! Those instances of goodness which were then in honour
are now in contempt. `[2.]` Providence so ordered it that she did that
which exactly answered to his sign, and was wonderfully the counterpart
of his proposal: she not only gave him drink, but, which was more than
could have been expected, she offered her services to give his camels
drink, which was the very sign he proposed. Note, First, God, in his
providence, does sometimes wonderfully own the prayer of faith, and
gratify the innocent desires of his praying people, even in little
things, that he may show the extent of his care, and may encourage them
at all times to seek to him and trust in him; yet we must take heed of
being over-bold in prescribing to God, lest the event should weaken our
faith rather than strengthen it. Secondly, It is good to take all
opportunities of showing a humble, courteous, charitable, disposition,
because, some time or other, it may turn more to our honour and benefit
than we think of; some hereby have entertained angels, and Rebekah
hereby, quite beyond her expectation at this time, was brought into the
line of Christ and the covenant. Thirdly, There may be a great deal of
obliging kindness in that which costs but little: our Saviour has
promised a reward for a cup of cold water, Mt. 10:42. Fourthly, The
concurrence of providences and their minute circumstances, for the
furtherance of our success in any business, ought to be particularly
observed, with wonder and thankfulness, to the glory of God: The man
wondered, v. 21. We have been wanting to ourselves, both in duty and in
comfort, by neglecting to observe Providence. `[3.]` Upon enquiry he
found, to his great satisfaction, that she was a near relation to his
master, and that the family she was of was considerable, and able to
give him entertainment, v. 23-25. Note, Providence sometimes wonderfully
directs those that by faith and prayer seek direction from heaven in the
choice of suitable yoke-fellows: happy marriages those are likely to be
that are made in the fear of God; and these, we are sure, are made in
heaven.

`3.` He acknowledges God in a particular thanksgiving. He first paid his
respects to Rebekah, in gratitude for her civility (v. 22), obliging her
with such ornaments and attire as a maid, especially a bride, cannot
forget (Jer. 2:32), which yet, we should think, ill suited the pitcher
of water; but the ear-rings and bracelets she sometimes wore did not
make her think herself above the labours of a virtuous woman (Prov.
31:13), who works willingly with her hands; nor the services of a child,
who, while under age, differs nothing from a servant, Gal. 9:1. Having
done this, he turns his wonder (v. 21) into worshipping: Blessed be the
Lord God of my master Abraham, v. 26, 27. Observe here, `(1.)` He had
prayed for good speed (v. 12), and now that he had sped well he gives
thanks. Note, What we win by prayer we must wear with praise; for
mercies in answer to prayer lay us under particular obligations. `(2.)` He
had as yet but a comfortable prospect of mercy, and was not certain what
the issue might prove; yet he gives thanks. Note, When God\'s favours
are coming towards us we must meet them with our praises. `(3.)` He
blesses God for success when he was negotiating for his master. Note, We
should be thankful for our friend\'s mercies as for our own. `(4.)` He
gives thanks that, being in the way, at a loss what course to steer, the
Lord had led him. Note, In doubtful cases, it is very comfortable to see
God leading us, as he led Israel in the wilderness by the pillar of
cloud and fire. `(5.)` He thinks himself very happy, and owns God in it,
that he was led to the house of his master\'s brethren, those of them
that had come out of Ur of the Chaldees, though they had not come to
Canaan, but remained in Haran. They were not idolaters, but worshippers
of the true God, and inclinable to the religion of Abraham\'s family.
Note, God is to be acknowledged in providing suitable yoke-fellows,
especially such as are agreeable in religion. `(6.)` He acknowledges that
God, herein, had not left his master destitute of his mercy and truth.
God had promised to build up Abraham\'s family, yet it seemed destitute
of the benefit of that promise; but now Providence is working towards
the accomplishing of it. Note, `[1.]` God\'s faithful ones, how
destitute soever they may be of worldly comforts, shall never be left
destitute of God\'s mercy and truth; for God\'s mercy is an
inexhaustible fountain, and his truth an inviolable foundation. `[2.]`
It adds much to the comfort of any blessing to see in it the continuance
of God\'s mercy and truth.

### Verses 29-53

We have here the making up of the marriage between Isaac and Rebekah. It
is related very largely and particularly, even to the minute
circumstances, which, we should think, might have been spared, while
other things of great moment and mystery (as the story of Melchizedek)
are related in few words. Thus God conceals that which is curious from
the wise and prudent, reveals to babes that which is common and level to
their capacity (Mt. 11:25), and rules and saves the world by the
foolishness of preaching, 1 Co. 1:21. Thus also we are directed to take
notice of God\'s providence in the little common occurrences of human
life, and in them also to exercise our own prudence and other graces;
for the scripture was not intended for the use of philosophers and
statesmen only, but to make us all wise and virtuous in the conduct of
ourselves and families. Here is,

`I.` The very kind reception given to Abraham\'s servant by Rebekah\'s
relations. Her brother Laban went to invite and conduct him in, but not
till he saw the ear-rings and the bracelets upon his sister\'s hands, v.
30. \"O,\" thinks Laban, \"here is a man that there is something to be
got by, a man that is rich and generous; we will be sure to bid him
welcome!\" We know so much of Laban\'s character, by the following
story, as to think that he would not have been so free of his
entertainment if he had not hoped to be well paid for it, as he was, v.
53. Note, A man\'s gift maketh room for him (Prov. 18:16), which way
soever it turneth, it prospereth, Prov. 17:8. 1. The invitation was
kind: Come in, thou blessed of the Lord, v. 31. They saw he was rich,
and therefore pronounced him blessed of the Lord; or, perhaps, because
they heard from Rebekah (v. 28) or the gracious words which proceeded
out of his mouth, they concluded him a good man, and therefore blessed
of the Lord. Note, Those that are blessed of God should be welcome to
us. It is good owning those whom God owns. 2. The entertainment was
kind, v. 32, 33. Both the house and stable were well furnished, and
Abraham\'s servant was invited to the free use of both. Particular care
was taken of the camels; for a good man regardeth the life of his beast,
Prov. 12:10. If the ox knows his owner to serve him, the owner should
know his ox to provide for him that which is fitting for him.

`II.` The full account which he gave them of his errand, and the court he
made to them for their consent respecting Rebekah. Observe,

`1.` How intent he was upon his business; though he had come off a
journey, and come to a good house, he would not eat, till he had told
his errand, v. 33. Note, The doing of our work, and the fulfilling of
our trusts, either for God or man, should be preferred by us before our
necessary food: it was our Saviour\'s meat and drink, Jn. 4:34.

`2.` How ingenious he was in the management of it; he approved himself,
in this matter, both a prudent man and a man of integrity, faithful to
his master by whom he was trusted, and just to those with whom he now
treated.

`(1.)` He gives a short account of the state of his master\'s family, v.
34-36. He was welcome before, but we may suppose him doubly welcome when
he said, I am Abraham\'s servant. Abraham\'s name, no doubt, was well
known among them and respected, and we might suppose them not altogether
ignorant of his state, for Abraham knew theirs, ch. 22:20-24. Two things
he suggests, to recommend his proposal:-`[1.]` That his master Abraham,
through the blessing of God, had a very good estate; and, `[2.]` That he
had settled it all upon Isaac, for whom he was now a suitor.

`(2.)` He tells them the charge his master had given him, to fetch a wife
for his son from among his kindred, with the reason of it, v. 37, 38.
Thus he insinuates a pleasing hint, that, though Abraham had removed to
a country at so great a distance, yet he still retained the remembrance
of his relations that he had left behind, and a respect for them. The
highest degrees of divine affection must not divest us of natural
affection. He likewise obviates an objection, That, if Isaac were
deserving, he needed not send so far off for a wife: why did he not
marry nearer home? \"For a good reason,\" says he; \"my master\'s son
must not match with a Canaanite.\" He further recommends his proposal,
`[1.]` From the faith his master had that it would succeed, v. 40.
Abraham took encouragement from the testimony of his conscience that he
walked before God in a regular course of holy living, and thence
inferred that God would prosper him; probably he refers to that covenant
which God had made with him (ch. 17:1), I am God, all-sufficient, walk
before me. Therefore, says he the God before whom I walk will send his
angel. Note, While we make conscience of our part of the covenant, we
may take the comfort of God\'s part of it; and we should learn to apply
general promises of particular cases, as there is occasion. `[2.]` From
the care he himself had taken to preserve their liberty of giving or
refusing their consent, as they should see cause, without incurring the
guilt of perjury (v. 39-41), which showed him, in general, to be a
cautious man, and particularly careful that their consent might not be
forced, but be either free or not at all.

`(3.)` He relates to them the wonderful concurrence of providences, to
countenance and further the proposal, plainly showing the finger of God
in it. `[1.]` He tells them how he had prayed for direction by a sign,
v. 42-44. Note, It is good dealing with those who be prayer take God
along with them in their dealings. `[2.]` How God had answered his
prayer in the very letter of it. Though he did but speak in his heart (v
45), which perhaps he mentions, lest it should be suspected that Rebekah
had overheard his prayer and designedly humoured it. \"No,\" says he,
\"I spoke it in my heart, so that none heard it but God, to whom thought
are word, and from him the answer came,\" v. 46, 47. `[3.]` How he had
immediately acknowledged God\'s goodness to him therein, leading him, as
he here expresses it, in the right way. Note, God\'s way is always the
right way (Ps. 107:7), and those are well led whom he leads.

`(4.)` He fairly refers the matter to their consideration, and waits their
decision (v. 49): \"If you will deal kindly and truly with my master,
well and good: if you will be sincerely kind, you will accept the
proposal, and I have what I came for; if not, do not hold me in
suspense.\" Note, Those who deal fairly have reason to expect fair
dealing.

`(5.)` They freely and cheerfully close with the proposal upon a very good
principle (v. 50): \"The thing proceedeth from the Lord, Providence
smiles upon it, and we have nothing to say against it.\" They do not
object distance of place, Abraham\'s forsaking them, or his having no
land in possession, but person estate only: they do not question the
truth of what this man said; but, `[1.]` They trust much to his
integrity. It were well if honesty did so universally prevail among men
that it might be as much an act of prudence as it is of good nature to
take a man\'s word. `[2.]` They trust more to God\'s providence, and
therefore by silence give consent, because it appears to be directed and
disposed by Infinite Wisdom. Note, A marriage is then likely to be
comfortable when it appears to proceed from the Lord.

`(6.)` Abraham\'s servant makes a thankful acknowledgment of the good
success he had met with, `[1.]` To God: He worshipped the Lord, v. 52.
Observe, First, As his good success went on, he went on to bless God.
Those that pray without ceasing should in every thing give thanks, and
own God in every step of mercy. Secondly, God sent his angel before him,
and so gave him success, v. 7, 40. But when he has the desired success,
he worships God, not the angel. Whatever benefit we have by the
ministration of angels, all the glory must be given to the Lord of the
angels, Rev. 22:9. `[2.]` He pays his respects to the family also, and
particularly to the bride, v. 53. He presented her, and her mother, and
brother, with many precious things, both to give a real proof of his
master\'s riches and generosity and in gratitude for their civility to
him, and further to ingratiate himself with them.

### Verses 54-61

Rebekah is here taking leave of her father\'s house; and 1. Abraham\'s
servant presses for a dismission. Though he and his company were very
welcome, and very cheerful there, yet he said, Send me away (v. 54), and
again, v. 56. He knew his master would expect him home with some
impatience; he had business to do at home which wanted him, and
therefore, as one that preferred his work before his pleasure, he was
for hastening home. Note, Lingering and loitering no way become a wise
and good man; when we have despatched our business abroad we must not
delay our return to our business at home, nor be longer from it than
needs must; for as a bird that wanders from her nest so is he that
wanders from his place, Prov. 27:8. 2. Rebekah\'s relations, from
natural affection and according to the usual expression of kindness in
that case, solicit for her stay some time among them, v. 55. They could
not think of parting with her on a sudden, especially as she was about
the remove so far off and it was not likely that they would ever see one
another again: Let her stay a few days, at least ten, which makes it as
reasonable a request as the reading in the margin seems to make it
unreasonable, a year, or at least ten months. They had consented to the
marriage, and yet were loth to part with her. Note, It is an instance of
the vanity of this world that there is nothing in it so agreeable but it
has its alloy. Nulla est sincera voluptas-There is no unmingled
pleasure. They were pleased that they had matched a daughter of their
family so well, and yet, when it came to the last, it was with great
reluctance that they sent her away. 3. Rebekah herself determined the
matter. To her they appealed, as it was fit they should (v. 57): Call
the damsel (who had retired to her apartment with a modest silence) and
enquire at her mouth. Note, As children ought not to marry without their
parents\' consent, so parents ought not to marry them without their own.
Before the matter is resolved on, \"Ask at the damsel\'s mouth;\" she is
a party principally concerned, and therefore ought to be principally
consulted. Rebekah consented, not only to go, but to go immediately: I
will go, v. 58. We may hope that the notice she had taken of the
servant\'s piety and devotion gave her such an idea of the prevalence of
religion and godliness in the family she was to go to made her desirous
to hasten thither, and willing to forget her own people and her
father\'s house, where religion had not so much the ascendant. 4.
Hereupon she is sent away with Abraham\'s servant; not, we may suppose,
the very next day after, but very quickly: her friends see that she has
a good heart on it, and so they dismiss her, `(1.)` With suitable
attendants-her nurse (v. 59), her damsels, v. 61. It seems, then, that
when she went to the well for water it was not because she had not
servants at command, but because she took a pleasure in works of humble
industry. Now that she was going among strangers, it was fit she should
take those with her with whom she was acquainted. Here is nothing said
of her portion. Her personal merits were a portion in her, she needed
none with her, nor did that ever come into the treaty of marriage. `(2.)`
With hearty good wishes: They blessed Rebekah, v. 60. Note, When our
relations are entering into a new condition, we ought by prayer to
recommend them to the blessing and grace of God. Now that she was going
to be a wife, they prayed that she might be a mother both of a numerous
and of a victorious progeny. Perhaps Abraham\'s servant had told them of
the promise God had lately made to his master, which it is likely,
Abraham acquainted his household with, that God would multiply his seed
as the stars of heaven, and that they should possess the gate of their
enemies (ch. 22:17), to which promise they had an eye in this blessing,
Be thou the mother of that seed.

### Verses 62-67

Isaac and Rebekah are, at length, happily brought together. Observe,

`I.` Isaac was well employed when he met Rebekah: He went out to meditate,
or pray, in the field, at the even-tide, v. 62, 63. Some think he
expected the return of his servants about this time, and went out on
purpose to meet them. But, it should seem, he went out on another
errand, to take the advantage of a silent evening and a solitary field
for meditation and prayer, those divine exercises by which we converse
with God and our own hearts. Note, 1. Holy souls love retirement. It
will do us good to be often left alone, walking alone and sitting alone;
and, if we have the art of improving solitude, we shall find we are
never less alone than when alone. 2. Meditation and prayer ought to be
both our business and our delight when we are alone; while we have a
God, a Christ, and a heaven, to acquaint ourselves with, and to secure
our interest in, we need not want matter either for meditation or
prayer, which, if they go together, will mutually befriend each other.
3. Our walks in the field are then truly pleasant when in them we apply
ourselves to meditation and prayer. We there have a free and open
prospect of the heavens above us and the earth around us, and the host
and riches of both, by the view of which we should be led to the
contemplation of the Maker and owner of all. 4. The exercises of
devotion should be the refreshment and entertainment of the evening, to
relieve us from the fatigue occasioned by the care and business of the
day, and to prepare us for the repose and sleep of the night. 5.
Merciful providences are then doubly comfortable when they find us well
employed and in the way of our duty. Some think Isaac was now praying
for good success in this affair that was depending, and meditating upon
that which was proper to encourage his hope in God concerning it; and
now, when he sets himself, as it were, upon his watch-tower, to see what
God would answer him, as the prophet (Hab. 2:1), he sees the camels
coming. Sometimes God sends in the mercy prayed for immediately, Acts
12:12.

`II.` Rebekah behaved herself very becomingly, when she met Isaac:
understanding who he was, she alighted off her camel (v. 64), andtook a
veil, and covered herself (v. 65), in token of humility, modesty, and
subjection. She did not reproach Isaac for not coming himself to fetch
her, or, at least, to meet her a day\'s journey or two, did not complain
of the tediousness of her journey, or the difficulty of leaving her
relations, to come into a strange place; but, having seen Providence
going before her in the affair, she accommodates herself with
cheerfulness to her new relation. Those that by faith are espoused to
Christ, and would be presented as chaste virgins to him, must, in
conformity to his example, humble themselves, as Rebekah, who alighted
when she saw Isaac on foot, and must put themselves into subjection to
him who is their head (Eph. 5:24), as Rebekah, signifying it by the veil
she put on, 1 Co. 11:10.

`III.` They were brought together (probably after some further
acquaintance), to their mutual comfort, v. 67. Observe here, 1. What an
affectionate son he was to his mother: it was about three years since
her death, and yet he was not, till now, comforted concerning it; the
wound which that affliction gave to his tender spirit bled so long, and
was never healed till God brought him into this new relation. Thus
crosses and comforts are balances to each other (Eccl. 7:14), and help
to keep the scale even. 2. What an affectionate husband he was to his
wife. Note, Those that have approved themselves well in one relation, it
may be hoped, will do so in another: She became his wife, and he loved
her; there was all the reason in the world why he should, for so ought
men to love their wives even an themselves. The duty of the relation is
then done, and the comfort of the relation is then enjoyed, when mutual
love governs; for there the Lord commands the blessing.
