Genesis, Chapter 21
===================

Commentary
----------

In this chapter we have, `I.` Isaac, the child of promise born into
Abraham\'s family (v. 1-8). `II.` Ishmael, the son of the bondwoman, cast
out of it (v. 9-21). `III.` Abraham\'s league with his neighbour Abimelech
(v. 22-32). `IV.` His devotion to his God (v. 33-34).

### Verses 1-8

Long-looked-for comes at last. The vision concerning the promised seed
is for an appointed time, and now, at the end, it speaks, and does not
lie; few under the Old Testament were brought into the world with such
expectation as Isaac was, not for the sake of any great person eminence
at which he was to arrive, but because he was to be, in this very thin,
a type of Christ, that seed which the holy God had so long promised and
holy men so long expected. In this account of the first days of Isaac we
may observe,

`I.` The fulfilling of God\'s promise in the conception and birth of
Isaac, v. 1, 2. Note, God\'s providences look best and brightest when
they are compared with his word, and when we observe how God, in them
all, acts as he has said, as he has spoken. 1. Isaac was born according
to the promise. The Lord visited Sarah in mercy, as he had said. Note,
No word of God shall fall to the ground; for he is faithful that has
promised, and God\'s faithfulness is the stay and support of his
people\'s faith. He was born at the set time of which God had spoken, v.
2. Note, God is always punctual to his time; though his promised mercies
come not at the time we set, they will certainly come at the time he
sets, and that is the best time., 2. He was born by virtue of the
promise: Sarah by faith received strength to conceive Heb. 11:11. God
therefore by promise gave that strength. It was not by the power of
common providence, but by the power of a special promise, that Isaac was
born. A sentence of death was, as it were, passed upon the second
causes: Abraham was old, and Sarah old, and both as good as dead; and
then the word of God took place. Note, True believers, by virtue of
God\'s promises, are enabled to do that which is above the power of
human nature, for by them they partake of a divine nature, 2 Pt. 1:4.

`II.` Abraham\'s obedience to God\'s precept concerning Isaac.

`1.` He named him, as God commanded him, v. 3. God directed him to a name
for a memorial, Isaac, laughter; and Abraham, whose office it was, gave
him that name, though he might have designed him some other name of a
more pompous signification. Note, It is fit that the luxuriancy of human
invention should always yield to the sovereignty and plainness of divine
institution; yet there was good reason for the name, for, `(1.)` When
Abraham received the promise of him he laughed for joy, ch. 17:17. Note,
When the sun of comfort has risen upon the soul it is good to remember
how welcome the dawning of the day was, and with what exultation we
embraced the promise. `(2.)` When Sarah received the promise she laughed
with distrust and diffidence. Note, When God gives us the mercies we
began to despair of we ought to remember with sorrow and shame our
sinful distrusts of God\'s power and promise, when we were in pursuit of
them. `(3.)` Isaac was himself, afterwards, laughed at by Ishmael (v. 9),
and perhaps his name bade him expect it. Note, God\'s favourites are
often the world\'s laughing-stocks. `(4.)` The promise which he was not
only the son, but the heir of, was to be the joy of all the saints in
all ages, and that which would fill their mouths with laughter.

`2.` He circumcised him, v. 4. The covenant being established with him,
the seal of the covenant was administered to him; and though a bloody
ordinance, and he a darling, yet it must not be omitted, no, nor
deferred beyond the eighth day. God had kept time in performing the
promise, and therefore Abraham must keep time in obeying the precept.

`III.` The impressions which this mercy made upon Sarah.

`1.` It filled her with joy (v. 6): \"God has made me to laugh; he has
given me both cause to rejoice and a heart to rejoice.\" Thus the mother
of our Lord, Lu. 1:46, 47. Note, `(1.)` God bestows mercies upon his
people to encourage their joy in his work and service; and, whatever is
the matter of our joy, God must be acknowledged as the author of it,
unless it be the laughter of the fool. `(2.)` When mercies have been long
deferred they are the more welcome when they come. `(3.)` It adds to the
comfort of any mercy to have our friends rejoice with us in it: All that
hear will laugh with me; for laughing is catching. See Lu. 1:58. Others
would rejoice in this instance of God\'s power and goodness, and be
encouraged to trust in him. See Ps. 119:74.

`2.` It filled her with wonder, v. 7. Observe here, `(1.)` What it was she
thought so wonderful: That Sarah should give children suck, that she
should, not only bear a child, but be so strong and hearty at the age as
to give it suck. Note, Mothers, if they be able, ought to be nurses to
their own children. Sarah was a person of quality, was aged; nursing
might be thought prejudicial of herself, or to the child, or to both;
she had choice of nurses, no doubt, in her own family: and yet she would
do her duty in this matter; and her daughters the good wives are while
they thus do well, 1 Pt. 3:5, 6. See Lam. 4:3. `(2.)` How she expressed
her wonder: \"Who would have said it? The thing was so highly
improbable, so near to impossible, that if any one but God had said it
we could not have believed it.\" Note, God\'s favours to his
covenant-people are such as surpass both their own and others\' thoughts
and expectations. Who could imagine that God should do so much for those
that deserve so little, nay, for those that deserve so ill? See Eph.
3:20; 2 Sa. 7:18, 19. Who would have said that God should send his Son
to die for us, his Spirit to sanctify us, his angels to attend us? Who
would have said that such great sins should be pardoned, such mean
services accepted, and such worthless worms taken into covenant and
communion with the great and holy God?

`IV.` A short account of Isaac\'s infancy: The child grew, v. 8. Special
notice is taken of this, though a thing of course, to intimate that the
children of the promise are growing children. See Lu. 1:80; 2:40. Those
that are born of God shall increase of God, Col. 2:19. He grew so as not
always to need milk, but was able to bear strong meat, and then he was
weaned. See Heb. 5:13, 14. And then it was that Abraham made a great
feast for his friends and neighbours, in thankfulness to God for his
mercy to him. He made this feast, not on the day that Isaac was born,
that would have been too great a disturbance to Sarah; nor on the day
that he was circumcised, that would have been too great a diversion from
the ordinance; but on the day that he was weaned, because God\'s
blessing upon the nursing of children, and the preservation of them
throughout the perils of the infant age, are signal instances of the
care and tenderness of the divine providence, which ought to be
acknowledged, to its praise. See Ps. 22:9, 10; Hos. 11:1.

### Verses 9-13

The casting out of Ishmael is here considered of, and resolved on.

`I.` Ishmael himself gave the occasion by some affronts he gave to Isaac
his little brother, some think on the day that Abraham made the feast
for joy that Isaac was safely weaned, which the Jews say was not till he
was three years old, others say five. Sarah herself was an eye-witness
of the abuse: she saw the son of the Egyptian mocking (v. 9), mocking
Isaac, no doubt, for it is said, with reference to this (Gal. 4:29),
that he that was born after the flesh persecuted him that was born after
the Spirit. Ishmael is here called the son of the Egyptian, because, as
some think, the 400 years\' affliction of the seed of Abraham by the
Egyptians began now, and was to be dated hence, ch. 15:13. She saw him
playing with Isaac, so the Septuagint, and, in play, mocking him.
Ishmael was fourteen years older than Isaac; and, when children are
together, the elder should be careful and tender of the younger: but it
argued a very base and sordid disposition in Ishmael to be abusive to a
child that was no way a match for him. Note, 1. God takes notice of what
children say and do in their play, and will reckon with them if they say
or do amiss, though their parents do not. 2. Mocking is a great sin, and
very provoking to God. 3. There is a rooted remaining enmity in the seed
of the serpent against the seed of the woman. The children of promise
must expect to be mocked. This is persecution, which those that will
live godly must count upon. 4. None are rejected and cast out from God
but those who have first deserved it. Ishmael is continued in Abraham\'s
family till he becomes a disturbance, grief, and scandal to it.

`II.` Sarah made the motion: Cast out this bond-woman, v. 10. This seems
to be spoken in some heat, yet it is quoted (Gal. 4:30) as if it had
been spoken by a spirit of prophecy; and it is the sentence passed on
all hypocrites and carnal people, though they have a place and a name in
the visible church. All that are born after the flesh and not born
again, that rest in the law and reject the gospel promise, shall
certainly be cast out. It is made to point particularly at the rejection
of the unbelieving Jews, who, though they were the seed of Abraham, yet,
because they submitted not to the gospel covenant, were unchurched and
disfranchised: and that which, above any thing, provoked God to cast
them off was their mocking and persecuting the gospel church, God\'s
Isaac, in its infancy, 1 Th. 2:16, Note, There are many who are
familiarly conversant with the children of God in this world, and yet
shall not partake with them in the inheritance of sons. Ishmael might be
Isaac\'s play-fellow and school-fellow, yet not his fellow-heir.

`III.` Abraham was averse to it: The thing was very grievous in
Abraham\'s sight, v. 11. 1. It grieved him that Ishmael had given such a
provocation. Note, Children ought to consider that the more their
parents love them the more they are grieved at their misconduct, and
particularly at their quarrels among themselves. 2. It grieved him that
Sarah insisted upon such a punishment. \"Might it not suffice to correct
him? would nothing less serve than to expel him?\" Note, Even the
needful extremities which must be used with wicked and incorrigible
children are very grievous to tender parents, who cannot thus afflict
willingly.

`IV.` God determined it, v. 12, 13. We may well suppose Abraham to be
greatly agitated about this matter, loth to displease Sarah, and yet
loth to expel Ishmael; in this difficulty God tells him what his will
is, and then he is satisfied. Note, A good man desires no more in
doubtful cases than to know his duty, and what God would have him do;
and, when he is clear in this, he is, or should be, easy. To make
Abraham so, God sets this matter before him in a true light, and shows
him, 1. That the casting out of Ishmael was necessary to the
establishment of Isaac in the rights and privileges of the covenant: In
Isaac shall thy seed be called. Both Christ and the church must descend
from Abraham through the loins of Isaac; this is the entail of the
promise upon Isaac, and is quoted by the apostle (Rom. 9:7) to show that
not all who come from Abraham\'s loins were the heirs of Abraham\'s
covenant. Isaac, the promised son, must be the father of the promised
seed; therefore, \"Away with Ishmael, send him far enough, lest he
corrupt the manners or attempt to invade the rights of Isaac.\" It will
be his security to have his rival banished. The covenant seed of Abraham
must be a peculiar people, a people by themselves, from the very first,
distinguished, not mingled with those that were out of covenant; for
this reason Ishmael must be separated. Abraham was called alone, and so
must Isaac be. See Isa. 51:2. It is probable that Sarah little thought
of this (Jn. 11:51), but God took what she said, and turned it into an
oracle, as afterwards, ch. 27:10. 2. That the casting out of Ishmael
should not be his ruin, v. 13. He shall be a nation, because he is thy
seed. We are not sure that it was his eternal ruin. It is presumption to
say that all those who are left out of the external dispensation from
all his mercies: those may be saved who are not thus honoured. However,
we are sure it was not his temporal ruin. Though he was chased out of
the church, he was not chased out of the world. I will make him a
nation. Note, `(1.)` Nations are of God\'s making: he founds them, he
forms them, he fixes them. `(2.)` Many are full of the blessings of God\'s
providence that are strangers to the blessings of his covenant. `(3.)` The
children of this world often fare the better, as to outward things, for
their relation to the children of God.

### Verses 14-21

Here is, `I.` The casting out of the bond-woman, and her son from the
family of Abraham, v. 14. Abraham\'s obedience to the divine command in
this matter was speedy-early in the morning, we may suppose immediately
after he had, in the night\'s visions, received orders to do this. It
was also submissive; it was contrary to his judgment, at least to his
own inclination, to do it; yet as soon as he perceives that it is the
mind of God he makes no objections, but silently does as he is bidden,
as one trained up to an implicit obedience. In sending them away without
any attendants, on foot, and slenderly provided for, it is probable that
he observed the directions given him. If Hagar and Ishmael had conducted
themselves well in Abraham\'s family, they might have continued there;
but they threw themselves out by their own pride and insolence, which
were thus justly chastised. Note, By abusing our privileges we forfeit
them. Those that know not when they are well off, in such a desirable
place as Abraham\'s family, deserve to be cashiered, and to be made to
know the worth of mercies by the want of them.

`II.` Their wandering in the wilderness, missing their way to the place
Abraham designed them for a settlement.

`1.` They were reduced to great distress there. Their provisions were
spent, and Ishmael was sick. He that used to be full fed in Abraham\'s
house, where he waxed fat and kicked, now fainted and sunk, when he was
brought to short allowance. Hagar is in tears, and sufficiently
mortified. Now she wishes for the crumbs she had wasted and made light
of at her master\'s table. Like one under the power of the spirit of
bondage, she despairs of relief, counts upon nothing but the death of
the child (v. 15, 16), though God had told her, before he was born, that
he should live to be a man, a great man. We are apt to forget former
promises, when present providences seem to contradict them; for we live
by sense.

`2.` In this distress, God graciously appeared for their relief: he heard
the voice of the lad, v. 17. We read not of a word he said; but his
sighs, and groans, and calamitous state, cried aloud in the ears of
mercy. An angel was sent to comfort Hagar, and it was not the first time
that she had met with God\'s comforts in a wilderness; she had
thankfully acknowledged the former kind visit which God made his in such
a case (ch. 16:13), and therefore God now visited her again with
seasonable succours. `(1.)` The angel assures her of the cognizance God
took of her distress: God has heard the voice of the lad where he is,
though he is in a wilderness (for, wherever we are, there is a way open
heaven-ward); therefore lift up the lad, and hold him in thy hand, v.
18. Note, God\'s readiness to help us when we are in trouble must not
slacken, but quicken, our endeavours to help ourselves. `(2.)` He repeats
the promise concerning her son, that he should be a great nation, as a
reason why she should bestir herself to help him. Note, It should engage
our care and pains about children and young people to consider that we
know not what God has designed them for, nor what great use Providence
may make of them. `(3.)` He directs her to a present supply (v. 19): He
opened her eyes (which were swollen and almost blinded with weeping),
and then she saw a well of water. Note, Many that have reason enough to
be comforted go mourning from day to day, because they do not see the
reason they have for comfort. There is a well of water by them in the
covenant of grace, but they are not aware of it; they have not the
benefit of it, till the same God that opened their eyes to see their
wound opens them to see their remedy, Jn. 16:6, 7. Now the apostle tells
us that those things concerning Hagar and Ishmael are alleµgoroumena
(Gal. 4:24), they are to be allegorized; this then will serve to
illustrate the folly, `[1.]` Of those who, like the unbelieving Jews,
seek for righteousness by the law and the carnal ordinances of it, and
not by the promise made in Christ, thereby running themselves into a
wilderness of want and despair. Their comforts are soon exhausted, and
if God save them not by his special prerogative, and by a miracle of
mercy open their eyes and undeceive them, they are undone. `[2.]` Of
those who seek for satisfaction and happiness in the world and the
things of it. Those that forsake the comforts of the covenant and
communion with God, and choose their portion in this earth, take up with
a bottle of water, poor and slender provision, and that soon spent; they
wander endlessly in pursuit of satisfaction, and, at length, sit down
short of it.

`III.` The settlement of Ishmael, at last, in the wilderness of Paran (v.
20, 21), a wild place, fittest for a wild man; and such a one he was,
ch. 16. 12. Those that are born after the flesh take up with the
wilderness of this world, while the children of the promise aim at the
heavenly Canaan, and cannot be at rest till they are there. Observe, 1.
He had some tokens of God\'s presence: God was with the lad; his outward
prosperity was owing to this. 2. By trade he was an archer, which
intimates that craft was his excellency and sport his business: rejected
Esau was a cunning hunter. 3. He matched among his mother\'s relations;
she took him a wife out of Egypt: as great an archer as he was, he did
not think he could take his aim well, in the business of marriage, if he
proceeded without his mother\'s advice and consent.

### Verses 22-32

We have here an account of the treaty between Abimelech and Abraham, in
which appears the accomplishment of that promise (ch. 12:2) that God
would make his name great. His friendship is valued, is courted, though
a stranger, though a tenant at will to the Canaanites and Perizzites.

`I.` The league is proposed by Abimelech, and Phichol his prime-minister
of state and general of his army.

`1.` The inducement to it was God\'s favour to Abraham (v. 22): \"God is
with thee in all that thou doest, and we cannot but take notice of it.\"
Note, `(1.)` God in his providence sometimes shows his people such tokens
for good that their neighbours cannot but take notice of it, Ps. 86:17.
Their affairs do so visibly prosper, and they have such remarkable
success in their undertakings, that a confession is extorted from all
about them of God\'s presence with them. `(2.)` It is good being in favour
with those that are in favour with God, and having an interest in those
that have an interest in heaven, Zec. 8:23. We will go with you, for we
have heard that God is with you. We do well for ourselves if we have
fellowship with those that have fellowship with God, 1 Jn. 1:3.

`2.` The tenour of it was, in general, that there should be a firm and
constant friendship between the two families, which should not upon any
account be violated. This bond of friendship must be strengthened by the
bond of an oath, in which the true God was appealed to, both as a
witness of their sincerity and an avenger in case either side were
treacherous, v. 23. Observe, `(1.)` He desires the entail of this league
upon his posterity and the extension of it to his people. He would have
his son, and his son\'s son, and his land likewise, to have the benefit
of it. Good men should secure an alliance and communion with the
favourites of Heaven, not for themselves only, but for theirs also. `(2.)`
He reminds Abraham of the fair treatment he had found among them:
According to the kindness I have done unto thee. As those that have
received kindness must return it, so those that have shown kindness may
expect it.

`II.` It is consented to by Abraham, with a particular clause inserted
about a well. In Abraham\'s part of this transaction observe,

`1.` He was ready to enter into this league with Abimelech, finding him
to be a man of honour and conscience, and that had the fear of God
before his eyes: I will swear, v. 24. Note, `(1.)` Religion does not make
men morose and unconversable; I am sure it ought not. We must not, under
colour of shunning bad company, be sour to all company, and jealous of
every body. `(2.)` An honest mind does not startle at giving assurances:
if Abraham say that he will be true to Abimelech, he is not afraid to
swear it; an oath is for confirmation.

`2.` He prudently settled the matter concerning a well, about which
Abimelech\'s servants had quarrelled with him. Wells of water, it seems,
were choice goods in that country: thanks be to God, that they are not
so scarce in ours. `(1.)` Abraham mildly told Abimelech of it, v. 25.
Note, If our brother trespass against us, we must, with the meekness of
wisdom, tell him his fault, that the matter may be fairly accommodated
and an end made of it, Mt. 18:15. `(2.)` He acquiesced in Abimelech\'s
justification of himself in this matter: I wot not who has done this
thing, v. 26. Many are suspected of injustice and unkindness that are
perfectly innocent, and we ought to be glad when they clear themselves.
The faults of servants must not be imputed to their masters, unless they
know of them and justify them; and no more can be expected from an
honest man than that he be ready to do right as soon as he knows that he
has done wrong. `(3.)` He took care to have his title to the well cleared
and confirmed, to prevent any disputes or quarrels for the future, v.
30. It is justice, as well as wisdom, to do thus, in perptuam rei
memoriam-that the circumstance may be perpetually remembered.

`3.` He made a very handsome present to Abimelech, v. 27. It was not any
thing curious or fine that he presented to him, but that which was
valuable and useful-sheep and oxen, in gratitude for Abimelech\'s
kindness to him, and in token of hearty friendship between them. The
interchanging of kind offices is the improving of love: that which is
mine is my friend\'s.

`4.` He ratified the covenant by an oath, and registered it by giving a
new name to the place (v. 31), Beer-sheba, the well of the oath, in
remembrance of the covenant they swore to, that they might be ever
mindful of it; or the well of seven, in remembrance of the seven lambs
given to Abimelech, as a consideration for his confirming Abraham\'s
title to that well. Note, Bargains made must be remembered, that we may
make them good, and may not break our word through oversight.

### Verses 33-34

Observe, 1. Abraham, having got into a good neighbourhood, knew when he
was well off, and continued a great while there. There he planted a
grove for a shade to his tent, or perhaps an orchard of fruit-trees; and
there, though we cannot say he settled, for God would have him, while he
lived, to be a stranger and a pilgrim, yet he sojourned many days, as
many as would consist with his character, as Abraham the Hebrew, or
passenger. 2. There he made, not only a constant practice, but an open
profession, of his religion: There he called on the name of the Lord,
the everlasting God, probably in the grove he planted, which was his
oratory or house of prayer. Christ prayed in a garden, on a mountain.
`(1.)` Abraham kept up public worship, to which, probably, his neighbours
resorted, that they might join with him. Note, Good men should not only
retain their goodness wherever they go, but do all they can to propagate
it, and make others good. `(2.)` In calling on the Lord, we must eye him
as the everlasting God, the God of the world, so some. Though God had
made himself known to Abraham as his God in particular, and in covenant
with him, yet he forgets not to give glory to him as the Lord of all:
The everlasting God, who was, before all worlds, and will be, when time
and days shall be no more. See Isa. 40:28.
