Genesis, Chapter 18
===================

Commentary
----------

We have an account in this chapter of another interview between God and
Abraham, probably within a few days after the former, as the reward of
his cheerful obedience to the law of circumcision. Here is, `I.` The kind
visit which God made him, and the kind entertainment which he gave to
that visit (v. 1-8). `II.` The matters discoursed of between them. 1. The
purposes of God\'s love concerning Sarah (v. 9-15). 2. The purposes of
God\'s wrath concerning Sodom. `(1.)` The discovery God made to Abraham of
his design to destroy Sodom (v. 16-22). `(2.)` The intercession Abraham
made for Sodom (v. 23, etc.).

### Verses 1-8

The appearance of God to Abraham seems to have had in it more of freedom
and familiarity, and less of grandeur and majesty, than those we have
hitherto read of; and therefore more resembles that great visit which,
in the fullness of time, the Son of God was to make to the world, when
the Word would be flesh, and appear as one of us. Observe here,

`I.` How Abraham expected strangers, and how richly his expectations were
answered (v. 1): He sat in the tent-door, in the heat of the day; not so
much to repose or divert himself as to seek an opportunity of doing
good, by giving entertainment to strangers and travellers, there being
perhaps no inns to accommodate them. Note, 1. We are likely to have the
most comfort of those good works to which we are most free and forward.
2. God graciously visits those in whom he has first raised the
expectation of him, and manifests himself to those that wait for him.
When Abraham was thus sitting, he saw three men coming towards him.
These three men were three spiritual heavenly beings, now assuming human
bodies, that they might be visible to Abraham, and conversable with him.
Some think that they were all created angels, others that one of them
was the Son of God, the angel of the covenant, whom Abraham
distinguished from the rest (v. 3), and who is called Jehovah, v. 13.
The apostle improves this for the encouragement of hospitality, Heb.
13:2. Those that have been forward to entertain strangers have
entertained angels, to their unspeakable honour and satisfaction. Where,
upon a prudent and impartial judgment, we see no cause to suspect ill,
charity teaches us to hope well and to show kindness accordingly. It is
better to feed five drones, or wasps, than to starve one bee.

`II.` How Abraham entertained those strangers, and how kindly his
entertainment was accepted. The Holy Ghost takes particular notice of
the very free and affectionate welcome Abraham gave to the strangers. 1.
He was very complaisant and respectful to them. Forgetting his age and
gravity, he ran to meet them in the most obliging manner, and with all
due courtesy bowed himself towards the ground, though as yet he knew
nothing of them but that they appeared graceful respectable men. Note,
Religion does not destroy, but improve, good manners, and teaches us to
honour all men. Decent civility is a great ornament to piety. 2. He was
very earnest and importunate for their stay, and took it as a great
favour, v. 3, 4. Note, `(1.)` It becomes those whom God has blessed with
plenty to be liberal and open-hearted in their entertainments, according
to their ability, and (not in compliment, but cordially) to bid their
friends welcome. We should take a pleasure in showing kindness to any;
for both God and man love a cheerful giver. Who would eat the bread of
him that has an evil eye? Prov. 23:6, 7. `(2.)` Those that would have
communion with God must earnestly desire it and pray for it. God is a
guest worth entertaining. 3. His entertainment, though it was very free,
was yet plain and homely, and there was nothing in it of the gaiety and
niceness of our times. His dining-room was an arbour under a tree; no
rich table-linen, no side-board set with plate. His feast was a joint or
two of veal, and some cakes baked on the hearth, and both hastily
dressed up. Here were no dainties, no varieties, no forced-meats, no
sweet-meats, but good, plain, wholesome food, though Abraham was very
rich and his guests were very honourable. Note, We ought not to be
curious in our diet. Let us be thankful for food convenient, though it
be homely and common; and not be desirous of dainties, for they are
deceitful meat to those that love them and set their hearts upon them.
4. He and his wife were both of them very attentive and busy, in
accommodating their guests with the best they had. Sarah herself is cook
and baker; Abraham runs to fetch the calf, brings out the milk and
butter, and thinks it not below him to wait at table, that he might show
how heartily welcome his guests were. Note, `(1.)` Those that have real
merit need not take state upon them, nor are their prudent
condescensions any disparagement to them. `(2.)` Hearty friendship will
stoop to any thing but sin. Christ himself has taught us to wash one
another\'s feet, in humble love. Those that thus abase themselves shall
be exalted. Here Abraham\'s faith showed itself in good works; and so
must ours, else it is dead, Jam. 2:21, 26. The father of the faithful
was famous for charity, and generosity, and good house-keeping; and we
must learn of him to do good and to communicate. Job did not eat his
morsel alone, Job 31:17.

### Verses 9-15

These heavenly guests (being sent to confirm the promise lately made to
Abraham, that he should have a son by Sarah), while they are receiving
Abraham\'s kind entertainment, they return his kindness. He receives
angels, and has angels\' rewards, a gracious message from heaven, Mt.
10:41.

`I.` Care is taken that Sarah should be within hearing. She must conceive
by faith, and therefore the promise must be made to her, Heb. 11:11. It
was the modest usage of that time that the women did not sit at meat
with men, at least not with strangers, but confined themselves to their
own apartments; therefore Sarah is here out of sight: but she must not
be out of hearing. The angels enquire (v. 9), Where is Sarah thy wife?
By naming her, they gave intimation enough to Abraham that, though they
seemed strangers, yet they very well knew him and his family. By
enquiring after her, they showed a friendly kind concern for the family
and relations of one whom they found respectful to them. It is a piece
of common civility, which ought to proceed from a principle of Christian
love, and then it is sanctified. And, by speaking of her (she
over-hearing it), they drew her to listen to what was further to be
said. Where is Sarah thy wife? say the angels. \"Behold in the tent,\"
says Abraham. \"Where should she be else? There she is in her place, as
she uses to be, and is now within call.\" Note, 1. The daughters of
Sarah must learn of her to be chaste, keepers at home, Tit. 2:5. There
is nothing got by gadding. 2. Those are most likely to receive comfort
from God and his promises that are in their place and in the way of
their duty, Lu. 2:8.

`II.` The promise is then renewed and ratified, that she should have a
son (v. 10): \"I will certainly return unto thee, and visit thee next
time with the performance, as now I do with the promise.\" God will
return to those that bid him welcome, that entertain his visits: \"I
will return thy kindness, Sarah thy wife shall have a son;\" it is
repeated again, v. 14. Thus the promises of the Messiah were often
repeated in the Old Testament, for the strengthening of the faith of
God\'s people. We are slow of heart to believe, and therefore have need
of line upon line to the same purport. This is that word of promise
which the apostle quotes (Rom. 9:9) as that by the virtue of which Isaac
was born. Note, 1. The same blessings which others have from common
providence believers have from the promise, which makes them very sweet
and very sure. 2. The spiritual seed of Abraham owe their life, and joy,
and hope, and all, to the promise. They are born by the word of God, 1
Pt. 1:23.

`III.` Sarah thinks this too good news to be true, and therefore cannot
as yet find in her heart to believe it: Sarah laughed within herself, v.
12. It was not a pleasing laughter of faith, like Abraham\'s (ch.
17:17), but it was a laughter of doubting and mistrust. Note, The same
thing may be done from very different principles, of which God only, who
knows the heart, can judge. The great objection which Sarah could not
get over was her age: \"I am waxed old, and past childbearing in the
course of nature, especially having been hitherto barren, and (which
magnifies the difficulty) my lord is old also.\" Observe here, 1. Sarah
calls Abraham her lord; it was the only good word in this saying, and
the Holy Ghost takes notice of it to her honour, and recommends it to
the imitation of all Christian wives. 1 Pt. 3:6, Sarah obeyed Abraham,
calling him lord, in token of respect and subjection. Thus must the wife
reverence her husband, Eph. 5:33. And thus must we be apt to take notice
of what is spoken decently and well, to the honour of those that speak
it, though it may be mixed with that which is amiss, over which we
should cast a mantle of love. 2. Human improbability often sets up in
contradiction to the divine promise. The objections of sense are very
apt to stumble and puzzle the weak faith even of true believers. It is
hard to cleave to the first Cause, when second causes frown. 3. Even
where there is true faith, yet there are often sore conflicts with
unbelief, Sarah could say, Lord, I believe (Heb. 11:11), and yet must
say, Lord, help my unbelief.

`IV.` The angel reproves the indecent expressions of her distrust, v. 13,
14. Observe, 1. Though Sarah was now most kindly and generously
entertaining these angels, yet, when she did amiss, they reproved her
for it, as Christ reproved Martha in her own house, Lu. 10:40, 41. If
our friends be kind to us, we must not therefore be so unkind to them as
to suffer sin upon them. 2. God gave this reproof to Sarah by Abraham
her husband. To him he said, Why did Sarah laugh? perhaps because he had
not told her of the promise which had been given him some time before to
this purport, and which, if he had communicated it to her with its
ratifications, would have prevented her from being so surprised now. Or
Abraham was told of it that he might tell her of it. Mutual reproof,
when there is occasion for it, is one of the duties of the conjugal
relation. 3. The reproof itself is plain, and backed with a good reason:
Wherefore did Sarah laugh? Note, It is good to enquire into the reason
of our laughter, that it may not be the laughter of the fool, Eccl. 7:6.
\"Wherefore did I laugh?\" Again, Our unbelief and distrust are a great
offence to the God of heaven. He justly takes it ill to have the
objections of sense set up in contradiction to his promise, as Lu. 1:18.
4. Here is a question asked which is enough to answer all the cavils of
flesh and blood: Is any thing too hard for the Lord? (Heb. too
wonderful), that is, `(1.)` Is any thing so secret as to escape his
cognizance? No, not Sarah\'s laughing, though it was only within
herself. Or, `(2.)` Is any thing so difficult as to exceed his power? No,
not the giving of a child to Sarah in her old age.

`V.` Sarah foolishly endeavours to conceal her fault (v. 15): She denied,
saying, I did not laugh, thinking nobody could contradict her: she told
this lie, because she was afraid; but it was in vain to attempt
concealing it from an all-seeing eye; she was told, to her shame, Thou
didst laugh. Now, 1. There seems to be in Sarah a retraction of her
distrust. Now she perceived, by laying circumstances together, that it
was a divine promise which had been made concerning her, she renounced
all doubting distrustful thoughts about it. But, 2. There was withal a
sinful attempt to cover a sin with a lie. It is a shame to do amiss, but
a greater shame to deny it; for thereby we add iniquity to our iniquity.
Fear of a rebuke often betrays us into this snare. See Isa. 57:11, Whom
hast thou feared, that thou hast lied? But we deceive ourselves if we
think to impose upon God; he can and will bring truth to light, to our
shame. He that covers his sin cannot prosper, for the day is coming
which will discover it.

### Verses 16-22

The messengers from heaven had now despatched one part of their
business, which was an errand of grace to Abraham and Sarah, and which
they delivered first; but now they have before them work of another
nature. Sodom is to be destroyed, and they must do it, ch. 19:13. Note,
As with the Lord there is mercy, so he is the God to whom vengeance
belongs. Pursuant to their commission, we here find, 1. That they looked
towards Sodom (v. 16); they set their faces against it in wrath, as God
is said to look unto the host of the Egyptians, Ex. 14:24. Note, Though
God has long seemed to connive at sinners, from which they have inferred
that the Lord does not see, does not regard, yet, when the day of his
wrath comes, he will look towards them. 2. That they went towards Sodom
(v. 22), and accordingly we find two of them at Sodom, ch. 19:1. Whether
the third was the Lord, before whom Abraham yet stood, and to whom he
drew near (v. 23), as most think, or whether the third left them before
they came to Sodom, and the Lord before whom Abraham stood was the
shechinah, or that appearance of the divine glory which Abraham had
formerly seen and conversed with, is uncertain. However, we have here,

`I.` The honour Abraham did to his guests: He went with them to bring them
on the way, as one that was loth to part with such good company, and was
desirous to pay his utmost respects to them. This is a piece of civility
proper to be shown to our friends; but it must be done as the apostle
directs (3 Jn. 6), after a godly sort.

`II.` The honour they did to him; for those that honour God he will
honour. God communicated to Abraham his purpose to destroy Sodom, and
not only so, but entered into a free conference with him about it.
Having taken him, more closely than before, into covenant with himself
(ch. 17), he here admits him into more intimate communion with himself
than ever, as the man of his counsel. Observe here,

`1.` God\'s friendly thoughts concerning Abraham, v. 17-19, where we have
his resolution to make known to Abraham his purpose concerning Sodom,
with the reasons of it. If Abraham had not brought them on their way,
perhaps he would not have been thus favoured; but he that loves to walk
with wise men shall be wise, Prov. 13:20. See how God is pleased to
argue with himself: Shall I hide from Abraham (or, as some read it, Am I
concealing from Abraham) that thing which I do? \"Can I go about such a
thing, and not tell Abraham?\" Thus does God, in his counsels, express
himself, after the manner of men, with deliberation. But why must
Abraham be of the cabinet-council? The Jews suggest that because God had
granted the land of Canaan to Abraham and his seed therefore he would
not destroy those cities which were a part of that land, without his
knowledge and consent. But God here gives two other reasons:-

`(1.)` Abraham must know, for he is a friend and a favourite, and one that
God has a particular kindness for and great things in store for. He is
to become a great nation; and not only so, but in the Messiah, who is to
come from his loins, All nations of the earth shall be blessed. Note,
The secret of the Lord is with those that fear him, Ps. 25:14; Prov.
3:32. Those who by faith live a life of communion with God cannot but
know more of his mind than other people, though not with a prophetical,
yet with a prudential practical knowledge. They have a better insight
than others into what is present (Hos. 14:9; Ps. 107:43), and a better
foresight of what is to come, at least so much as suffices for their
guidance and for their comfort.

`(2.)` Abraham must know, for he will teach his household: I know Abraham
very well, that he will command his children and his household after
him, v. 19. Consider this, `[1.]` As a very bright part of Abraham\'s
character and example. He not only prayed with his family, but he taught
them as a man of knowledge, nay, he commanded them as a man in
authority, and was prophet and king, as well as priest, in his own
house. Observe, First, God having made the covenant with him and his
seed, and his household being circumcised pursuant to that, he was very
careful to teach and rule them well. Those that expect family blessings
must make conscience of family duty. If our children be the Lord\'s,
they must be nursed for him; if they wear his livery, they must be
trained up in his work. Secondly, Abraham took care not only of his
children, but of his household; his servants were catechized servants.
Masters of families should instruct and inspect the manners of all under
their roof. The poorest servants have precious souls that must be looked
after. Thirdly, Abraham made it his care and business to promote
practical religion in his family. He did not fill their heads with
matters of nice speculation, or doubtful disputation; but he taught them
to keep the way of the Lord, and to do judgment and justice, that is, to
be serious and devout in the worship of God and to be honest in their
dealings with all men. Fourthly, Abraham, herein, had an eye to
posterity, and was in care not only that his household with him, but
that his household after him, should keep the way of the Lord, that
religion might flourish in his family when he was in his grave. Fifthly,
His doing this was the fulfilling of the conditions of the promises
which God had made him. Those only can expect the benefit of the
promises that make conscience of their duty. `[2.]` As the reason why
God would make known to him his purpose concerning Sodom, because he was
communicative of his knowledge, and improved it for the benefit of those
that were under his charge. Note, To him that hath shall be given, Mt.
13:12; 25:29. Those that make a good use of their knowledge shall know
more.

`2.` God\'s friendly talk with Abraham, in which he makes known to him
purpose concerning Sodom, and allows him a liberty of application to him
about the matter. `(1.)` He tells him of the evidence there was against
Sodom: The cry of Sodom is great, v. 20. Note, Some sins, and the sins
of some sinners, cry aloud to heaven for vengeance. The iniquity of
Sodom was crying iniquity, that is, it was so very provoking that it
even urged God to punish. `(2.)` The enquiry he would make upon this
evidence: I will go down now and see, v. 21. Not as if there were any
thing concerning which God is in doubt, or in the dark; but he is
pleased thus to express himself after the manner of men, `[1.]` To show
the incontestable equity of all his judicial proceedings. Men are apt to
suggest that his way is not equal; but let them know that his judgments
are the result of an eternal counsel, and are never rash or sudden
resolves. He never punishes upon report, or common fame, or the
information of others, but upon his own certain and infallible
knowledge. `[2.]` To give example to magistrates, and those in
authority, with the utmost care and diligence to enquire into the merits
of a cause, before they give judgment upon it. `[3.]` Perhaps the decree
is here spoken of as not yet peremptory, that room and encouragement
might be given to Abraham to make intercession for them. Thus God looked
if there were any to intercede, Isa. 59:16.

### Verses 23-33

Communion with God is kept up by the word and by prayer. In the word God
speaks to us; in prayer we speak to him. God had revealed to Abraham his
purposes concerning Sodom; now from this Abraham takes occasion to speak
to God on Sodom\'s behalf. Note, God\'s word then does us good when it
furnishes us with matter for prayer and excites us to it. When God has
spoken to us, we must consider what we have to say to him upon it.
Observe,

`I.` The solemnity of Abraham\'s address to God on this occasion: Abraham
drew near, v. 23. The expression intimates, 1. A holy concern: He
engaged his heart to approach to God, Jer. 30:21. \"Shall Sodom be
destroyed, and I not speak one good word for it?\" 2. A holy confidence:
He drew near with an assurance of faith, drew near as a prince, Job
31:37. Note, When we address ourselves to the duty of prayer, we ought
to remember that we are drawing near to God, that we may be filled with
a reverence of him, Lev. 10:3.

`II.` The general scope of this prayer. It is the first solemn prayer we
have upon record in the Bible; and it is a prayer for the sparing of
Sodom. Abraham, no doubt, greatly abhorred the wickedness of the
Sodomites; he would not have lived among them, as Lot did, if they would
have given him the best estate in their country; and yet he prayed
earnestly for them. Note, Though sin is to be hated, sinners are to be
pitied and prayed for. God delights not in their death, nor should we
desire, but deprecate, the woeful day. 1. He begins with a prayer that
the righteous among them might be spared, and not involved in the common
calamity, having an eye particularly to just Lot, whose disingenuous
carriage towards him he had long since forgiven and forgotten, witness
his friendly zeal to rescue him before by his sword and now by his
prayers. 2. He improves this into a petition that all might be spared
for the sake of the righteous that were among them, God himself
countenancing this request, and in effect putting him upon it by his
answer to his first address, v. 26. Note, We must pray, not only for
ourselves, but for others also; for we are members of the same body, at
least of the same body of mankind. All we are brethren.

`III.` The particular graces eminent in this prayer.

`1.` Here is great faith; and it is the prayer of faith that is the
prevailing prayer. His faith pleads with God, orders the cause, and
fills his mouth with arguments. He acts faith especially upon the
righteousness of God, and is very confident.

`(1.)` That God will not destroy the righteous with the wicked, v. 23. No,
that be far from thee, v. 25. We must never entertain any thought that
derogates from the honour of God\'s righteousness. See Rom. 3:5, 6.
Note, `[1.]` The righteous are mingled with the wicked in this world.
Among the best there are, commonly, some bad, and among the worst some
good: even in Sodom, one Lot. `[2.]` Though the righteous be among the
wicked, yet the righteous God will not, certainly he will not, destroy
the righteous with the wicked. Though in this world they may be involved
in the same common calamities, yet in the great day a distinction with
be made.

`(2.)` That the righteous shall not be as the wicked, v. 25. Though they
may suffer with them, yet they do not suffer like them. Common
calamities are quite another thing to the righteous than what they are
to the wicked, Isa. 27:7.

`(3.)` That the Judge of all the earth will do right; undoubtedly he will,
because he is the Judge of all the earth; it is the apostle\'s argument,
Rom. 3:5, 6. Note, `[1.]` God is the Judge of all the earth; he gives
charge to all, takes cognizance of all, and will pass sentence upon all.
`[2.]` That God Almighty never did nor ever will do any wrong to any of
the creatures, either by withholding that which is right or by exacting
more than is right, Job 34:10, 11.

`2.` Here is great humility.

`(1.)` A deep sense of his own unworthiness (v. 27): Behold now, I have
taken upon me to speak unto the Lord, who am but dust and ashes; and
again, v. 31. He speaks as one amazed at his own boldness, and the
liberty God graciously allowed him, considering God\'s greatness-he is
the Lord; and his own meanness-but dust and ashes. Note, `[1.]` The
greatest of men, the most considerable and deserving, are but dust and
ashes, mean and vile before God, despicable, frail, and dying. `[2.]`
Whenever we draw near to God, it becomes us reverently to acknowledge
the vast distance that there is between us and God. He is the Lord of
glory, we are worms of the earth. `[3.]` The access we have to the
throne of grace, and the freedom of speech allowed us, are just matter
of humble wonder, 2 Sa. 7:18.

`(2.)` An awful dread of God\'s displeasure: O let not the Lord be angry
(v. 30), and again, v. 32. Note, `[1.]` The importunity which believers
use in their addresses to God is such that, if they were dealing with a
man like themselves, they could not but fear that he would be angry with
them. But he with whom we have to do is God and not man; and, whoever he
may seem, is not really angry with the prayers of the upright (Ps.
80:4), for they are his delight (Prov. 15:8), and he is pleased when he
is wrestled with. `[2.]` That even when we receive special tokens of the
divine favour we ought to be jealous over ourselves, lest we make
ourselves obnoxious to the divine displeasure; and therefore we must
bring the Mediator with us in the arms of our faith, to atone for the
iniquity of our holy things.

`3.` Here is great charity. `(1.)` A charitable opinion of Sodom\'s
character: as bad as it was, he thought there were several good people
in it. It becomes us to hope the best of the worst places. Of the two it
is better to err in that extreme. `(2.)` A charitable desire of Sodom\'s
welfare: he used all his interest at the throne of grace for mercy for
them. We never find him thus earnest in pleading with God for himself
and his family, as here for Sodom.

`4.` Here are great boldness and believing confidence. `(1.)` He took the
liberty to pitch upon a certain number of righteous ones which he
supposed might be in Sodom. Suppose there be fifty, v. 24. `(2.)` He
advanced upon God\'s concessions, again and again. As God granted much,
he still begged more, with the hope of gaining his point. `(3.)` He
brought the terms as low as he could for shame (having prevailed for
mercy if there were but ten righteous ones in five cities), and perhaps
so low that he concluded they would have been spared.

`IV.` The success of the prayer. He that thus wrestled prevailed
wonderfully; as a prince he had power with God: it was but ask and have.
`1.` God\'s general good-will appears in this, that he consented to spare
the wicked for the sake of the righteous. See how swift God is to show
mercy; he even seeks a reason for it. See what great blessings good
people are to any place, and how little those befriend themselves that
hate and persecute them. 2. His particular favour to Abraham appeared in
this, that he did not leave off granting till Abraham left off asking.
Such is the power of prayer. Why then did Abraham leave off asking, when
he had prevailed so far as to get the place spared it there were but ten
righteous in it? Either, `(1.)` Because he owned that it deserved to be
destroyed if there were not so many; as the dresser of the vineyard, who
consented that the barren tree should be cut down if one year\'s trial
more did not make it fruitful, Lu. 13:9. Or, `(2.)` Because God restrained
his spirit from asking any further. When God has determined the ruin of
a place, he forbids it to be prayed for, Jer. 7:16; 11:14; 14:11.

`V.` Here is the breaking up of the conference, v. 33. 1. The Lord went
his way. The visions of God must not be constant in this world, where it
is by faith only that we are to set God before us. God did not go away
till Abraham had said all he had to say; for he is never weary of
hearing prayer, Isa. 59:1. 2. Abraham returned unto his place, not
puffed up with the honour done him, nor by these extraordinary
interviews taken off from the ordinary course of duty. He returned to
his place to observe what that event would be; and it proved that his
prayer was heard, and yet Sodom was not spared, because there were not
ten righteous in it. We cannot expect too little from man nor too much
from God.
