Introduction to John
====================

It is not material to enquire when and where this gospel was written; we
are sure that it was given by inspiration of God to John, the brother of
James, one of the twelve apostles, distinguished by the honourable
character of that disciple whom Jesus loved, one of the first three of
the worthies of the Son of David, whom he took to be the witnesses of
his retirements, particularly of his transfiguration and his agony. The
ancients tell us that John lived longest of all the twelve apostles, and
was the only one of them that died a natural death, all the rest
suffering martyrdom; and some of them say that he wrote this gospel at
Ephesus, at the request of the ministers of the several churches of
Asia, in opposition to the heresy of Corinthus and the Ebionites, who
held that our Lord was a mere man. It seems most probable that he wrote
it before his banishment into the isle of Patmos, for there he wrote his
Apocalypse, the close of which seems designed for the closing up of the
canon of scripture; and, if so, this gospel was not written after. I
cannot therefore give credit to those later fathers, who say that he
wrote it in his banishment, or after his return from it, many years
after the destruction of Jerusalem; when he was ninety years old, saith
one of them; when he was a hundred, saith another of them. However, it
is clear that he wrote last of the four evangelists, and, comparing his
gospel with theirs, we may observe, 1. That he relates what they had
omitted; he brings up the rear, and his gospel is as the rearward or
gathering host; it gleans up what they has passed by. Thus there was a
later collection of Solomon\'s wise sayings (Prov. 25:1), and yet far
short of what he delivered, 1 Ki. 4:32. 2. That he gives us more of the
mystery of that of which the other evangelists gave us only the history.
It was necessary that the matters of fact should be first settled, which
was done in their declarations of those things which Jesus began both to
do and teach, Lu. 1:1; Acts 1:1. But, this being done out of the mouth
of two or three witnesses, John goes on to perfection (Heb. 6:1), not
laying again the foundation, but building upon it, leading us more
within the veil. Some of the ancients observe that the other evangelists
wrote more of the ta soµmatika-the bodily things of Christ; but John
writes of the ta pneumatika-the spiritual things of the gospel, the life
and soul of it; therefore some have called this gospel the key of the
evangelists. Here is it that a door is opened in heaven, and the first
voice we hear is, Come up hither, come up higher. Some of the ancients,
that supposed the four living creatures in John\'s vision to represent
the for evangelists, make John himself to be the flying eagle, so high
does he soar, and so clearly does he see into divine and heavenly
things.
