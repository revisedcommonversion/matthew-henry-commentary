2nd Samuel, Chapter 9
=====================

Commentary
----------

The only thing recorded in this chapter is the kindness David showed to
Jonathan\'s seed for his sake. `I.` The kind enquiry he made after the
remains of the house of Saul, and his discovery of Mephibosheth (v.
1-4). `II.` The kind reception he gave to Mephibosheth, when he was
brought to him (v. 5-8). `III.` The kind provision he made for him and his
(v. 9-13).

### Verses 1-8

Here is, `I.` David\'s enquiry after the remains of the ruined house of
Saul, v. 1. This was a great while after his accession to the throne,
for it should seem that Mephibosheth, who was but five years old when
Saul died, had now a son born, v. 12. David had too long forgotten his
obligations to Jonathan, but now, at length, they are brought to his
mind. It is good sometimes to bethink ourselves whether there be any
promises or engagements that we have neglected to make good; better do
it late than never. The compendium which Paul gives us of the life of
David is this (Acts 13:36), that he served his generation according to
the will of God, that is, he was a man that made it his business to do
good; witness this instance, where we may observe,

`1.` That he sought an opportunity to do good. He might perhaps have
satisfied his conscience with the performance of his promise to Jonathan
if he had been only ready, upon request or application made to him by
any of his seed, to help and succour them. But he does more, he enquires
of those about him first (v. 1), and, when he met with a person that was
likely to inform him, asked him particularly, Is there any yet left of
the house of Saul, that I may show him kindness? v. 3. \"Is there any,
not only to whom I may do justice (Num. 5:8), but to whom I may show
kindness?\" Note, Good men should seek opportunities of doing good. The
liberal deviseth liberal things, Isa. 32:8. For, the most proper objects
of our kindness and charity are such as will not be frequently met with
without enquiry. The most necessitous are the least clamorous.

`2.` Those he enquired after were the remains of the house of Saul, to
whom he would show kindness for Jonathan\'s sake: Is there any left of
the house of Saul? Saul had a very numerous family (1 Chr. 8:33), enough
to replenish a country, and was yet so emptied that none of it appeared;
but it was a matter of enquiry, Is there any left? See how the
providence of God can empty full families; see how the sin of man will
do it. Saul\'s was a bloody house, no marvel it was thus reduced, ch.
21:1. But, though God visited the iniquity of the father upon the
children, David would not. \"Is there any left that I can show kindness
to, not for Saul\'s own sake, but for Jonathan\'s?\" `(1.)` Saul was
David\'s sworn enemy, and yet he would show kindness to his house with
all his heart and was forward to do it. He does not say, \"Is there any
left of the house of Saul, that I may find some way to take them off,
and prevent their giving disturbance to me or my successor?\" It was
against Abimelech\'s mind that any one was left of the house of Gideon
(Jdg. 9:5), and against Athaliah\'s mind that any one was left of the
seed royal, 2 Chr. 22:10, 11. Those were usurped governments. David\'s
needed no such vile supports. He was desirous to show kindness to the
house of Saul, not only because he trusted in God and feared not what
they could do unto him, but because he was of a charitable disposition
and forgave what they had done to him. Note, We must evince the
sincerity of our forgiving those that have been any way unjust or
injurious to us by being ready, as we have opportunity, to show kindness
both to them and theirs. We must not only not avenge ourselves upon
them, but we must love them, and do them good (Mt. 5:44), and not be
backward to do any office of love and good-will to those that have done
us many an injury. 1 Pt. 3:9,-but, contrari-wise, blessing. This is the
way to overcome evil, and to find mercy for ourselves and ours, when we
or they need it. `(2.)` Jonathan was David\'s sworn friend, and therefore
he would show kindness to his house. This teaches us, `[1.]` To be
mindful of our covenant. The kindness we have promised we must
conscientiously perform, though it should not be claimed. God is
faithful to us; let us not be unfaithful to one another. `[2.]` To be
mindful of our friendships, our old friendships. Note, Kindness to our
friends, even to them and theirs, is one of the laws of our holy
religion. He that has friends must show himself friendly, Prov. 18:24.
If Providence has raised us, and our friends and their families are
brought low, yet we must not forget former acquaintance, but rather look
upon that as giving us so much the fairer opportunity of being kind to
them: then our friends have most need of us and we are in the best
capacity to help them. Though there be not a solemn league of friendship
tying us to this constancy of love, yet there is a sacred law of
friendship no less obliging, that to him that is in misery pity should
be shown by his friend, Job 6:14. A brother is born for adversity.
Friendship obliges us to take cognizance of the families and surviving
relations of those we have loved, who, when they left us, left behind
them their bodies, their names, and their posterity, to be kind to.

`3.` The kindness he promised to show them he calls the kindness of God;
not only great kindness, but, `(1.)` Kindness in pursuance of the covenant
that was between him and Jonathan, to which God was a witness. See 1 Sa.
20:42. `(2.)` Kindness after God\'s example; for we must be merciful as he
is. He spares those whom he has advantage against, and so must we.
Jonathan\'s request to David was (1 Sa. 20:14, 15), \"Show me the
kindness of the Lord, that I die not, and the same to my seed.\" The
kindness of God is some greater instance of kindness than one can
ordinarily expect from men. `(3.)` It is kindness done after a godly sort,
and with an eye to God, and his honour and favour.

`II.` Information given him concerning Mephibosheth, the son of Jonathan.
Ziba was an old retainer to Saul\'s family, and knew the state of it. He
was sent for and examined, and informed the king that Jonathan\'s son
was living, but lame (how he came to be so we read before, ch. 4:4), and
that he lived in obscurity, probably among his mother\'s relations in
Lo-debar in Gilead, on the other side Jordan, where he was forgotten, as
a dead man out of mind, but bore this obscurity the more easily because
he could remember little of the honour he fell from.

`III.` The bringing of him to court. The king sent (Ziba, it is likely)
to bring him up to Jerusalem with all convenient speed, v. 5. Thus he
eased Machir of his trouble, and perhaps recompensed him for what he had
laid out on Mephibosheth\'s account. This Machir appears to have been a
very generous free-hearted man, and to have entertained Mephibosheth,
not out of any disaffection to David or his government, but in
compassion to the reduced son of a prince, for afterwards we find him
kind to David himself when he fled from Absalom. He is named (ch. 17:27)
among those that furnished the king with what he wanted at Mahanaim,
though David, when he sent for Mephibosheth from him, little thought
that the time would come when he himself would gladly be beholden to
him: and perhaps Machir was then the more ready to help David in
recompence for his kindness to Mephibosheth. Therefore we should be
forward to give, because we know not but we ourselves may some time be
in want, Eccl. 11:2. And he that watereth shall be watered also himself,
Prov. 11:25. Now,

`1.` Mephibosheth presented himself to David with all the respect that
was due to his character. Lame as he was, he fell on his face, and did
homage, v. 6. David had thus made his honours to Mephibosheth\'s father,
Jonathan, when he was next to the throne (1 Sa. 20:41, he bowed himself
to him three times), and now Mephibosheth, in like manner, addresses
him, when affairs are so completely reversed. Those who, when they are
in inferior relations, show respect, shall, when they come to be
advanced, have respect shown to them.

`2.` David received him with all the kindness that could be. `(1.)` He
spoke to him as one surprised, but pleased to see him. \"Mephibosheth!
Why, is there such a man living?\" He remembered his name, for it is
probable that he was born about the time of the intimacy between him and
Jonathan. `(2.)` He bade him not be afraid: Fear not, v. 7. It is probable
that the sight of David put him into some confusion, to free him from
which he assures him that he sent for him, not out of any jealousy he
had of him, nor with any bad design upon him, but to show him kindness.
Great men should not take a pleasure in the timorous approaches of their
inferiors (for the great God does not), but should encourage them. `(3.)`
He gives him, by grant from the crown, all the land of Saul his father,
that is, his paternal estate, which was forfeited by Ishbosheth\'s
rebellion and added to his own revenue. This was a real favour, and more
than giving him a kind word. True friendship will be generous. `(4.)`
Though he had thus given him a good estate, sufficient to maintain him,
yet for Jonathan\'s sake (whom perhaps he saw some resemblance of in
Mephibosheth\'s face), he will take him to be a constant guest at his
own table, where he will not only be comfortably fed, but have company
and attendance suitable to his birth and quality. Though Mephibosheth
was lame and unsightly, and does not appear to have had any great
fitness for business, yet, for his good father\'s sake, David took him
to be one of his family.

`3.` Mephibosheth accepts this kindness with great humility and
self-abasement. He was not one of those that take every favour as a
debt, and think every thing too little that their friends do for them;
but, on the contrary, speaks as one amazed at the grants David made him
(v. 8): What is thy servant, that thou shouldst look upon such a dead
dog as I am? How does he vilify himself! Though the son of a prince, and
the grandson of a king, yet his family being under guilt and wrath, and
himself poor and lame, he calls himself a dead dog before David. Note,
It is good to have the heart humble under humbling providences. If, when
divine Providence brings our condition down, divine grace brings our
spirits down with it, we shall be easy. And those who thus humble
themselves shall be exalted. How does he magnify David\'s kindness! It
would have been easy to lessen it if he had been so disposed. Had David
restored him his father\'s estate? It was but giving him his own. Did he
take him to his table? This was policy, that he might have an eye upon
him. But Mephibosheth considered all that David said and did as very
kind, and himself as less than the least of all his favours. See 1 Sa.
18:18.

### Verses 9-13

The matter is here settled concerning Mephibosheth. 1. This grant of his
father\'s estate is confirmed to him, and Ziba called to be a witness to
it (v. 9); and, it should seem, Saul had a very good estate, for his
father was a mighty man of substance (1 Sa. 9:1), and he had fields and
vineyards to bestow, 1 Sa. 22:7. Be it ever so much, Mephibosheth is now
master of it all. 2. The management of the estate is committed to Ziba,
who knew what it was and how to make the most of it, in whom, having
been his father\'s servant, he might confide, and who, having a numerous
family of sons and servants, had hands sufficient to be employed about
it, v. 10. Thus Mephibosheth is made very easy, having a good estate
without care, and is in a fair way of being very rich, having much
coming in and little occasion to spend, himself being kept at David\'s
table. Yet he must have food to eat besides his own bread, provisions
for his son and servants; and Ziba\'s sons and servants would come in
for their share of his revenue, for which reason perhaps their number is
here mentioned, fifteen sons and twenty servants, who would require
nearly all there was; for as goods are increased those are increased
that eat them, and what good has the owner thereof save the beholding of
them with his eyes? Eccl. 5:11. All that dwelt in the house of Ziba were
servants to Mephibosheth (v. 12), that is, they all lived upon him, and
made a prey of his estate, under pretence of waiting on him and doing
him service. The Jews have a saying, \"He that multiplies servants
multiplies thieves.\" Ziba is now pleased, for he loves wealth, and will
have abundance. \"As the king has commanded, so will thy servant do, v.
11. Let me alone with the estate: and as for Mephibosheth\" (they seem
to be Ziba\'s words), \"if the king please, he need not trouble the
court, he shall eat at my table, and be as well treated as one of the
king\'s sons.\" But David will have him at his own table, and
Mephibosheth is as well pleased with his post as Ziba with his. How
unfaithful Ziba was to him we shall find afterwards, ch. 16:3. Now
because David was a type of Christ, his Lord and son, his root and
offspring, let his kindness to Mephibosheth serve to illustrate the
kindness and love of God our Saviour towards fallen man, which yet he
was under no obligation to, as David was to Jonathan. Man was convicted
of rebellion against God, and, like Saul\'s house, under a sentence of
rejection from him, was not only brought low and impoverished, but lame
and impotent, made so by the fall. The Son of God enquires after this
degenerate race, that enquired not after him, comes to seek and save
them. To those of them that humble themselves before him, and commit
themselves to him, he restores the forfeited inheritance, he entitles
them to a better paradise than that which Adam lost, and takes them into
communion with himself, sets them with his children at his table, and
feasts them with the dainties of heaven. Lord, what is man, that thou
shouldst thus magnify him!
