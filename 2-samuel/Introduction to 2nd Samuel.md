Introduction to 2nd Samuel
==========================

This book is the history of the reign of king David. We had in the
foregoing book an account of his designation to the government, and his
struggles with Saul, which ended at length in the death of his
persecutor. This book begins with his accession to the throne, and is
entirely taken up with the affairs of the government during the forty
years he reigned, and therefore is entitled by the Septuagint. The Third
Book of the Kings. It gives us an account of David\'s triumphs and his
troubles. `I.` His triumphs over the house of Saul (ch. 1-4), over the
Jebusites and Philistines (ch. 5), at the bringing up of the ark (ch. 6
and 7), over the neighbouring nations that opposed him (ch. 8-10); and
so far the history is agreeable to what we might expect from David\'s
character and the choice made of him. But his cloud has a dark side. `II.`
We have his troubles, the causes of them, his sin in the matter of Uriah
(ch. 11 and 12), the troubles themselves from the sin of Amnon (ch. 13),
the rebellion of Absalom (ch. 14-19) and of Sheba (ch. 20), and the
plague in Israel for his numbering the people (ch. 24), besides the
famine of the Gibeonites (ch. 21). His son we have (ch. 22), and his
words and worthies (ch. 23). Many things in his history are very
instructive; but for the hero who is the subject of it, though in many
instances he appears here very great, and very good, and very much the
favourite of heaven, yet it must be confessed that his honour shines
brighter in his Psalms than in his Annals.
