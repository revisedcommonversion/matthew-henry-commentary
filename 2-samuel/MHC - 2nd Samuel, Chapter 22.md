2nd Samuel, Chapter 22
======================

Commentary
----------

This chapter is a psalm, a psalm of praise; we find it afterwards
inserted among David\'s psalms (Ps. 18) with some little variation. We
have it here as it was first composed for his own closed and his own
harp; but there we have it as it was afterwards delivered to the chief
musician for the service of the church, a second edition with some
amendments; for, though it was calculated primarily for David\'s case,
yet it might indifferently serve the devotion of others, in giving
thanks for their deliverances; or it was intended that his people should
thus join with him in his thanksgivings, because, being a public person,
his deliverances were to be accounted public blessings and called for
public acknowledgments. The inspired historian, having largely related
David\'s deliverances in this and the foregoing book, and one
particularly in the close of the foregoing chapter, thought fit to
record this sacred poem as a memorial of all that had been before
related. Some think that David penned this psalm when he was old, upon a
general review of the mercies of his life and the many wonderful
preservations God had blessed him with, from first to last. We should in
our praises, look as far back as we can, and not suffer time to wear out
the sense of God\'s favours. Others think that he penned it when he was
young, upon occasion of some of his first deliverances, and kept it by
him for his use afterwards, and that, upon every new deliverance, his
practice was to sing this song. But the book of Psalms shows that he
varied as there was occasion, and confined not himself to one form. Here
is, `I.` The title of the psalm (v. 1). `II.` The psalm itself, in which,
with a very warm devotion and very great fluency and copiousness of
expression, 1. He gives glory to God. 2. He takes comfort in him; and he
finds matter for both, `(1.)` In the experiences he had of God\'s former
favours. `(2.)` In the expectations he had of his further favours. These
are intermixed throughout the whole psalm.

### Verse 1

Observe here, `I.` That it has often been the lot of God\'s people to have
many enemies, and to be in imminent danger of falling into their hands.
David was a man after God\'s heart, but not after men\'s heart: many
were those that hated him, and sought his ruin; Saul is particularly
named, either, 1. As distinguished from his enemies of the heathen
nations. Saul hated David, but David did not hate Saul, and therefore
would not reckon him among his enemies; or, rather, 2. As the chief of
his enemies, who was more malicious and powerful than any of them. Let
not those whom God loves marvel if the world hate them.

`II.` Those that trust God in the way of duty shall find him a present
help to them in their greatest dangers. David did so. God delivered him
out of the hand of Saul. He takes special notice of this. Remarkable
preservations should be mentioned in our praises with a particular
emphasis. He delivered him also out of the hand of all his enemies, one
after another, sometimes in one way, sometimes in another; and David,
from his own experience, has assured us that, though many are the
troubles of the righteous, yet the Lord delivers them out of them all,
Ps. 34:19. We shall never be delivered from all our enemies till we get
to heaven; and to that heavenly kingdom God will preserve all that are
his, 2 Tim. 4:18.

`III.` Those that have received many signal mercies from God ought to
give him the glory of them. Every new mercy in our hand should put a new
song into our mouth, even praises to our God. Where there is a grateful
heart, out of the abundance of that the mouth will speak. David spoke,
not only to himself, for his own pleasure, not merely to those about
him, for their instruction, but to the Lord, for his honour, the words
of this song. Then we sing with grace when we sing to the Lord. In
distress he cried with his voice (Ps. 142:1), therefore with his voice
he gave thanks. Thanksgiving to God is the sweetest vocal music.

`IV.` We ought to be speedy in our thankful returns to God: In the day
that God delivered him he sang this song. While the mercy is fresh, and
our devout affections are most excited by it, let the thank-offering be
brought, that it may be kindled with the fire of those affections.

### Verses 2-51

Let us observe, in this song of praise,

`I.` How David adores God, and gives him the glory of his infinite
perfections. There is none like him, nor any to be compared with him (v.
32): Who is God, save the Lord? All others that are adored as deities
are counterfeits and pretenders. None is to be relied on but he. Who is
a rock, save our God? They are dead, but the Lord liveth, v. 47. They
disappoint their worshippers when they most need them. But as for God
his way is perfect, v. 31. Men begin in kindness, but end not-promise,
but perform not; but God will finish his work, and his word is tried,
and what we may trust.

`II.` How he triumphs in the interest he has in this God, and his
relation to him, which he lays down as the foundation of all the
benefits he has received from him: He is my God; as such he cries to him
(v. 7), and cleaves to him (v. 22); \"and, if my God, then my rock\" (v.
2), that is, \"my strength and my power (v. 33), the rock under which I
take shelter (he who is to me as the shadow of a great rock in a weary
land), the rock on which I build my hope,\" v. 3. Whatever is my
strength and support, it is the God of my rock that makes it so; nay, he
is the God of the rock of my salvation (v. 47): my saving strength is in
him and from him. David often hid himself in a rock (1 Sa. 24:2), but
God was his chief hiding-place. \"He is my fortress, in which I am safe
and think myself so-my high tower, or stronghold, in which I am out of
the reach of real evils-the tower of salvation (v. 51), which can never
be sealed nor battered, nor undermined. Salvation itself saves me. Am I
in distress? he is my deliverer-struck at, shot at? he is my
shield-pursued? he is my refuge-oppressed? he is my saviour, that
rescues me out of the hand of those that seek my ruin. Nay, he is the
horn of my salvation, by which I am strongly protected, and my enemies
are strongly pushed.\" Christ is spoken of as the horn of salvation in
the house of David, Lu. 1:69. \"Am I burdened, and ready to sink? The
Lord is my stay (v. 19), by whom I am supported. Am I in the dark,
benighted, at a loss? Thou art my lamp, O Lord! to show me my way, and
thou wilt dispel my darkness,\" v. 29. If we sincerely take the Lord for
our God, all this, and much more, he will be to us, all we need and can
desire.

`III.` What improvement he makes of his interest in God. If he be mine,
`1.` In him will I trust (v. 3), that is, \"I will resign myself to his
direction, and then depend upon his power, and wisdom, and goodness, to
conduct me well.\" 2. On him I will call (v. 4), for he is worthy to be
praised. What we have found in God that is worthy to be praised should
engage us to pray to him and give glory to him. 3. To him will I give
thanks (v. 50), and that publicly. When he was among the heathen he
would neither be afraid nor ashamed to own his obligations to the God of
Israel.

`IV.` The full and large account he keeps for himself, and gives to
others, of the great and kind things God had done for him. This takes up
most of the song. He gives God the glory both of his deliverances and of
his successes, showing both the perils he was delivered from and the
power he was advanced to.

`1.` He magnifies the great salvations God had wrought for him. God
sometimes brings his people into very great difficulties and dangers,
that he may have the honour of saving them and they the comfort of being
saved by him. He owns, Thou hast saved me from violence (v. 3), from my
enemies (v. 4), from my strong enemy, meaning Saul, who, if God had not
succoured him, would have been too hard for him, v. 18. Thou hast given
me the shield of thy salvation, v. 36. To magnify the salvation, he
observes,

`(1.)` That the danger was very great and threatening out of which he was
delivered. Men rose up against him (v. 40, 49) that hated him (v. 41), a
violent man (v. 49) namely, Saul, who was malicious in his designs
against him and vigorous in his pursuit. This is expressed figuratively,
v. 5, 6. He was surrounded with death on every side, threatened to be
overwhelmed, and saw no way of escape. So violently did the waves of
death beat upon him, so strongly did the cords and snares of death hold
him, that he could not help himself, any more than a man in the grave
can. The floods of Belial, the wicked one, and his wicked instruments,
made him afraid; he trembled to see not only earth, but death and hell,
in arms against him.

`(2.)` That his deliverance was an answer to prayer, v. 7. He has here
left us a good example, when we are in distress, to cry unto God with
importunity, as children in a fright cry to their parents; and great
encouragement to do so, in that he found God ready to answer prayer out
of his temple in heaven, where he is continually served and adored.

`(3.)` That God appeared in a singular and extraordinary manner for him
and against his enemies. The expressions are borrowed from the descent
of the divine Majesty upon Mount Sinai, v. 8, 9, etc. We do not find
that in any of David\'s battles God fought for him with thunder (as in
Samuel\'s time), or with hail (as in Joshua\'s time), or with the stars
in their courses (as in Deborah\'s time); but these lofty metaphors are
used, `[1.]` To set forth the glory of God, which was manifested in his
deliverance. God\'s wisdom and power, his goodness and faithfulness, his
justice and holiness, and his sovereign dominion over all the creatures
and all the counsels of men, which appeared in favour of David, were as
clear and bright a discovery of God\'s glory to an eye of faith as such
miraculous interpositions would have been to an eye of sense. `[2.]` To
set forth God\'s displeasure against his enemies, God so espoused his
cause that he showed himself an enemy to all his enemies; his anger is
set forth by a smoke out of his nostrils, and fire out of his mouth (v.
9), coals kindled (v. 13), arrows, v. 15. Who knows the power and terror
of his wrath? `[3.]` To set forth the extraordinary confusion which his
enemies were put into, and the consternation that seized them; as if the
earth had trembled and the foundations of the world had been discovered,
v. 8, 16. Who can stand before God when he is angry? `[4.]` To show how
ready God was to help him: He rode upon a cherub and did fly, v. 11. God
hastened to his succour, and came to him with seasonable relief, though
he had seemed at a distance; yet he was a God hiding himself (Isa.
14:15), for he made darkness his pavilion (v. 12), for the amazement of
his enemies and the protection of his own people.

`(4.)` That God manifested his particular favour and kindness to him in
these deliverances (v. 20): He delivered me, because he delighted in me.
The deliverance came not from common providence, but covenant-love; he
was herein treated as a favourite: so he perceived by the communications
of divine grace and comfort to his soul with these deliverances, and the
communion he had with God in them. Herein he was a type of Christ, whom
God upheld because he delighted in him, Isa. 42:1, 2.

`2.` He magnifies the great successes God had crowned him with. He had
not only preserved but prospered him. He was blessed, `(1.)` With liberty
and enlargement. He was brought into a large place (v. 20), where he had
room to thrive, and his steps were enlarged under him, so that he had
room to stir (v. 37), being no longer straitened and confined. `(2.)` With
military skill, and strength, and swiftness. Though he was bred up to
the crook, he was well instructed in the arts of war and qualified for
the toils and perils of it. God, having called him to fight his battles,
qualified him for the service. He made him very ingenious (He teacheth
my hands to war, v. 35. And this ingenuity was as good as strength, for
it follows, \"so that a bow of steel is broken by my arms,\" not so much
by main force as by dexterity), and very vigorous and valiant. (Thou
hast girded me with strength to battle, v. 40. He gives God the glory of
all his courage and ability for service), and very expeditious: He
maketh my feet swift like hinds feet (v. 34), which is of great
advantage both in charging and retreating. `(3.)` With victory over his
enemies, not only Saul and Absalom, but the Philistines, Moabites,
Ammonites, Syrians, and other neighbouring nations, whom he subdued and
made tributaries to Israel. His wonderful victories are here described,
v. 38-43. They were speedy victories (I turned not again till I had
consumed them, v. 38) and complete victories. The enemies of Israel were
wounded, destroyed, consumed, fell under his feet, trampled upon, and
disabled to rise, and their necks lay at his mercy. They cried both to
earth and heaven for help, but in vain. There was none to save, none
that durst appear for them. God answered them, not for they were not on
his side, nor did they cry unto him till they were brought to the last
extremity. Being thus abandoned, they became an easy prey to David\'s
righteous and victorious sword, so that he beat them as small as the
dust of the earth, which is scattered by the wind and trodden on by
every foot. `(4.)` With advancement to honour and power. To this he was
anointed before his troubles began, and at length, post tot discrimina
rerum-after all his dangers and disasters, he gained his point. God made
his way perfect (v. 33), gave him success in all his undertakings, set
him upon his high places (v. 34), denoting both safety and dignity.
God\'s gentleness, his grace and tender mercy, made him great (v. 36),
gave him great wealth, and great authority, and a name like that of the
great men of the earth. He was kept to be the head of the heathen (v.
44); his signal preservations evinced that he was designed and reserved
for something great-to rule over all Israel, notwithstanding the
strivings of the people, and so that those whom he had not known should
serve him, many of the nations that lay remote. Thus he was lifted up on
high, as high as the throne, above those that rose up against him, v.
49.

`V.` The comfortable reflections he makes upon his own integrity, which
God, by those wonderful deliverances, had graciously owned and witnessed
to, v. 21-25. He means especially his integrity with reference to Saul
and Ishbosheth, Absalom and Sheba, and those who either opposed his
coming to the crown or endeavoured to dethrone him. They falsely accused
him and misrepresented him, but he had the testimony of this conscience
for him that he was not an ambitious aspiring man, a false and bloody
man, as they called him,-that he had never taken any indirect unlawful
courses to secure or raise himself, but in his whole conduct had kept in
the way of his duty,-and that in the whole course of his conversation he
had, for the main, made religion his business, so that he could take
God\'s favours to him as the rewards of his righteousness, not of debt,
but of grace. God had recompensed him, though not for his righteousness,
as if that had merited any thing at the hand of God, yet according to
his righteousness, which he was well pleased with, and had an eye to.
His conscience witnessed for him, 1. That he had made the word of God
his rule, and had kept to it, v. 23. Wherever he was, God\'s judgments
were before him as his guide; whithersoever he went, he took his
religion along with him, and though he was forced to depart from his
country, and sent, as it were, to serve other gods, yet as for God\'s
statutes, he did not depart from them, but kept the way of the Lord and
walked in it. 2. That he had carefully avoided the bye-paths of sin. He
had not wickedly departed from his God. He could not say but that he had
taken some false steps, but he had not deserted God, nor forsaken his
way. Sins of infirmity he could not acquit himself from, but the grace
of God had kept him from presumptuous sins. Though he had sometimes
weakly departed from his God. By this it appeared that he was upright
before God, or to God (in his sight, and with an eye to him), that he
kept himself from his own iniquity, not only from that particular sin of
killing Saul when it was in the power of his hand to do it, but, in
general, he was afraid of sin and watchful against it, and made
conscience of what he said and did. The matter of Uriah is an exception
(1 Ki. 15:5), like that in Hezekiah\'s character, 2 Chr. 32:31. Note, A
careful abstaining from our own iniquity is one of the best evidences of
our own integrity; and the testimony of our conscience for us that we
have done so will be such a rejoicing as will not only lessen the griefs
of an afflicted state, but increase the comforts of a prosperous state.
David reflected with more comfort upon his victories over his own
iniquity than upon his conquest of Goliath and all the hosts of the
uncircumcised Philistines; and the witness of his own heart to his
uprightness was sweeter though more silent music than theirs that sang,
David has slain his ten thousands. If a great man be a good man, his
goodness will be much more his satisfaction than his greatness. Let
favour be shown to the upright and his uprightness will sweeten it, will
double it.

`VI.` The comfortable prospects he has of God\'s further favour. As he
looks back, so he looks forward, with pleasure, and assures himself of
the kindness God has in store for all the saints, for himself, and also
for his seed.

`1.` For all good people, v. 26-28. As God had dealt with him according
to his uprightness, so he will with all others. He takes occasion here
to lay down the established rules of God\'s procedure with the children
of men:-

`(1.)` That he will do good to those that are upright in their hearts. As
we are found towards God, he will be found towards us. `[1.]` God\'s
mercy and grace will be the joy of those that are merciful and gracious.
Even the merciful need mercy; and they shall obtain it. `[2.]` God\'s
uprightness, his justice and faithfulness, will be the joy of those that
are upright, just, and faithful, both towards God and man. `[3.]` God\'s
purity and holiness will be the joy of those that are pure and holy, who
therefore give thanks at the remembrance thereof. And, if any of these
good people be afflicted people, he will save them, either out of their
afflictions or by and after them. On the other hand,

`(2.)` That those who turn aside to crooked ways he will lead forth with
the workers of iniquity, as he says in another psalm. With the froward
he will wrestle; and those with whom God wrestles are sure to be foiled.
Woe unto him that strives with his Maker! God will walk contrary to
those that walk contrary to him and be displeased with those that are
displeased with him. As for the haughty, his eyes are upon them, marking
them out, as it were, to be brought down; for he resists the proud.

`2.` For himself. He foresaw that his conquests and kingdom would be yet
further enlarged, v. 45, 46. Even the sons of the stranger, that would
hear the report of his victories and the tokens of God\'s presence with
him, would be possessed with a fear of him, would be forced to submit to
him, though feignedly, and would be obedient to him. The successes which
he had had he looked upon as earnests of more and means of more. Who
durst oppose him by whom so many had been overcome? Thus the Son of
David goes on conquering and to conquer, Rev. 6:2. His gospel, which has
been victorious, shall be so more and more.

`3.` For his seed: He showeth mercy to his Messiah (v. 51), not only to
David himself, but to that seed of his for evermore. David was himself
anointed of God, not a usurper, but duly called to the government and
qualified for it; therefore he doubted not but God would show mercy to
him, that mercy which he had promised not to take from him nor from his
posterity (ch. 7:15, 16); on that promise he depends, with an eye to
Christ, who alone is his seed for evermore, whose throne and kingdom
still continue, and will to the end, whereas the seed and lineage of
David are long since extinct. See Ps. 89:28, 29. Thus all his joys and
all his hopes terminate, as ours should, in the great Redeemer.
