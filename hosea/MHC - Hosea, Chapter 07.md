Hosea, Chapter 7
================

Commentary
----------

In this chapter we have, `I.` A general charge drawn up against Israel for
those high crimes and misdemeanors by which they had obstructed the
course of God\'s favours to them (v. 1, 2). `II.` A particular accusation,
`1.` Of the court-the king, princes, and judges (v. 3-7). 2. Of the
country. Ephraim is here charged with conforming to the nations (v. 8),
senselessness and stupidity under the judgments of God (v. 9-11),
ingratitude to God for his mercies (v. 13), incorrigibleness under his
judgments (v. 14), contempt of God (v. 15), and hypocrisy in their
pretences to return to him (v. 16). They are also threatened with a
severe chastisement, which shall humble them (v. 12), and, if that
prevail not, then with an utter destruction (v. 13), particularly their
princes (v. 16).

### Verses 1-7

Some take away the last words of the foregoing chapter, and make them
the beginning of this: \"When I returned, or would have returned, the
captivity of my people, when I was about to come towards them in ways of
mercy, even when I would have healed Israel, then the iniquity of
Ephraim (the country and common people) was discovered, and the
wickedness of Samaria, the court and the chief city.\" Now, in these
verses, we may observe,

`I.` A general idea given of the present state of Israel, v. 1, 2. See how
the case now stood with them.

`1.` God graciously designed to do well for them: I would have healed
Israel. Israel were sick and wounded; their disease was dangerous and
malignant, and likely to be fatal, Isa. 1:6. But God offered to be their
physician, to undertake the cure, and there was balm in Gilead
sufficient to recover the health of the daughter of his people; their
case was bad, but it was not desperate, nay, it was hopeful, when God
would have healed Israel. `(1.)` He would have reformed them, would have
separated between them and their sins, would have purged out the
corruptions that were among them, by his laws and prophets. `(2.)` He
would have delivered them out of their troubles, and restored to them
their peace and prosperity. Several healing attempts were made, and
their declining state seemed sometimes to be in a hopeful way of
recovery; but their own folly put them back again. Note, If sinful
miserable souls be not healed and helped, but perish in their sin and
misery, they cannot lay the blame on God, for he both could and would
have healed them; he offered to take the ruin under his hand. And there
are some special seasons when God manifests his readiness to heal a
distempered church and nation, now and then a hopeful crisis, which, if
carefully watched and improved, might, even when the case is very bad,
turn the scale for life and health.

`2.` They stood in their own light and put a bar in their own door. When
God would have healed them, when they bade fair for reformation and
peace, then their iniquity was discovered and their wickedness, which
stopped that current of God\'s favours, and undid all again. `(1.)` Then,
when their case came to be examined and enquired into, in order to their
cure, that wickedness which had been concealed and palliated was found
out; not that it was ever hid from God, but he speaks after the manner
of men; as a surgeon, when he probes a wound in order to the cure of it
and finds that it touches the vitals and is incurable, goes no further
in his endeavour to cure it, so, when God came down to see the case of
Israel (as the expression is, Gen. 18:21), with kind intentions towards
them, he found their wickedness so very flagrant, and them so hardened
in it, so impudent and impenitent, that he could not in honour show them
the favour he designed them. Note, Sinners are not healed because they
would not be healed. Christ would have gathered them, and they would
not. `(2.)` Then, when some endeavours were used to reform and reclaim
them, that wickedness which had been restrained and kept under broke
out; and from God\'s steps towards the healing of them they took
occasion to be so much the more provoking. When endeavours were used to
reform them vice grew more impetuous, more outrageous, and swelled so
much the higher, as a stream when it is damned up. When they began to
prosper they grew more proud, wanton, and secure, and so stopped the
progress of their cure. Note, It is sin that turns away good things from
us when they are coming towards us; and it is the folly and ruin of
multitudes that, when God would do well for them, they do ill for
themselves. And what was it that did them this mischief? In one word,
they commit falsehood; they worship idols (so some), defraud one another
(so others), or, rather, they dissemble with God in their professions of
repentance and regard to him. They say that they are desirous to be
healed by him, and, in order to that, willing to be ruled by him; but
they lie unto him with their mouth and flatter him with their tongue.

`3.` A practical disbelief of God\'s omniscience and government was at
the bottom of all their wickedness (v. 2): \"They consider not in their
hearts, they never say it to their own hearts, never think of this, that
I remember all their wickedness.\" As if God could not see it, though he
is all eye, or did not heed it, though his name is Jealous, or had
forgotten it, though he is an eternal mind that can never be unmindful,
or would not reckon for it, though he is the Judge of heaven and earth.
This is the sinner\'s atheism; as good say that there is no God as say
that he is either ignorant or forgetful, that there is none that judges
in the earth as that he remembers not the things he is to give judgment
upon. It is a high affront they put upon God; it is a damning cheat they
put upon themselves; they say, The Lord shall not see, Ps. 94:7. They
cannot but know that God remembers all their works; they have been told
it many a time; nay, if you ask them, they cannot but own it, and yet
they do not consider it; they do not think of it when they should, and
with application to themselves and their own works, else they would not,
they durst not, do as they do. But the time will come when those who
thus deceive themselves shall be undeceived: \"Now their own doings have
beset them about, that is, they have come at length to such a pitch of
wickedness that their sins appear on every side of them; all their
neighbours see how bad they are, and can they think that God does not
see it?\" Or, rather, \"The punishment of their doings besets them
about; they are surrounded and embarrassed with troubles, so that they
cannot get out, by which it appears that the sins they smart for are
before my face, not only that I have seen them, but that I am displeased
at them;\" for, till God by pardoning our sins has cast them behind his
back, they are still before his face. Note, Sooner or later, God will
convince those who do not now consider it that he remembers all their
works.

`4.` God had begun to contend with them by his judgments, in earnest of
what was further coming: The thief comes in, and the troop of robbers
spoils without. Some take this as an instance of their wickedness, that
they robbed and spoiled one another. Nec hospes ab hospite tutus-The
host and the guest stand in fear of each other. It seems rather to be a
punishment of their sin; they were infested with secret thieves among
themselves, that robbed their houses and shops and picked their pockets,
and troops of robbers, foreign invaders, that with open violence spoiled
abroad; so far was Israel from being healed that they had fresh wounds
given them daily by robbers and spoilers; and all this the effect of
sin, all to punish them for robbing God, Isa. 42:24; Mal. 3:8, 11.

`II.` A particular account of the sins of the court, of the king and
princes, and those about them, and the tokens of God\'s displeasure that
they were under for them.

`1.` Their king and princes were pleased with the wickedness and
profaneness of their subjects, who were emboldened thereby to be so much
them ore wicked (v. 3): They make the king and princes glad with their
wickedness. It pleased them to see the people conform to their wicked
laws and examples, in the worship of their idols, and other instances of
impiety and immorality, and to hear them flatter and applaud them in
their wicked ways. When Herod saw that his wickedness pleased the people
he proceeded further in it, much more will the people do so when they
see that it pleases the prince, Acts 12:3. Particularly, they made them
glad with their lies, with the lying praises with which they crowned the
favourites of the prince and the lying calumnies and censures with which
they blackened those whom they knew the princes had a dislike to. Those
who show themselves pleased with slanders and ill-natured stories shall
never want those about them who will fill their ears with such stories.
Prov. 29:12, If a ruler hearken to lies, all his servants are wicked,
and will make him glad with their lies.

`2.` Drunkenness and revelling abound much at the court, v. 5. The day of
our king was a merry day with them, either his birth-day or his
inauguration-day, of which it is probable that they had an anniversary
observation, or perhaps it was some holiday of his appointing, which was
therefore called his day; on that day the princes met to drink the
king\'s health, and got him among them, to be merry, and made him sick
with bottles of wine. It should seem the king did not ordinarily drink
to excess, but he was not upon a high day brought to it by the artifices
of the princes, tempted by the goodness of the wine, the gaiety of the
company, or the healths they urged; and so little was he used to it that
it made him sick; and it is justly charged as a crime, as crimen laesae
majestatis-treason, upon those who thus imposed upon him and made him
sick; nor would it serve for an excuse that it was the day of their
king, but was rather an aggravation of the crime, that, whey they
pretended to do him honour, they dishonoured him to the highest degree.
If it is a great affront and injury to a common person to make him
drunk, and there is a woe to those that do it (Hab. 2:15), much more to
a crowned head; for the greater any man\'s dignity is the greater
disgrace it is to him to be drunk. It is not for kings, O Lemuel! it is
not for kings, to drink wine, Prov. 31:4, 5. See what a prejudice the
sin of drunkenness is to a man, to a king. `(1.)` In his health; it made
him sick. It is a force upon nature; and strange it is by what charms
men, otherwise rational enough, can be drawn to that which besides the
offence it gives to God, and the damage it does to their spiritual and
eternal welfare, is a present disorder and distemper to their own
bodies. `(2.)` In his honour; for, when he was thus intoxicated, he
stretched out his hand with scorners; then he that was entrusted with
the government of a kingdom lost the government of himself, and so far
forgot, `[1.]` The dignity of a king that he made himself familiar with
players and buffoons, and those whose company was a scandal. `[2.]` The
duty of a king that he joined in confederacy with atheists, and the
profane scoffers at religion, whom he ought to have silenced and put to
shame; he sat in the seat of the scornful, of those that had arrived at
the highest pitch of impiety; he struck in with them, said as they said,
did as they did, and exerted his power, and stretched forth the hand of
his government, in concurrence with them. Goodness and good men are
often made the song of the drunkards (Ps. 69:12; 35:16); but woe unto
thee, O land! when thy king is such a child as to stretch forth his hand
with those that make them so, Eccl. 10:16.

`3.` Adultery and uncleanness prevailed much among the courtiers. This is
spoken of v. 4, 6, 7, and the charge of drunkenness comes in in the
midst of this article; for wine is oil to the fire of lust, Prov. 23:33.
Those that are inflamed with fleshly lusts, that are adulterers (v. 4),
are here again and again compared to an oven heated by the baker (v. 4):
They have made ready their heart like an oven (v. 6); they are all hot
as an oven, v. 7. Note, `[1.]` An unclean heart is like an oven heated;
and the unclean lusts and affections of it are as the fuel that makes it
hot. It is an inward fire, it keeps the heat within itself; so
adulterers and fornicators secretly burn in lust, as the expression is,
Rom. 1:27. The heat of the oven is an intense heat, especially as it is
here described; he that heats it stirs up the fire, and ceases not from
raising it up, till the bread is ready to be put in, being kneaded and
leavened, all which only signifies that they are like an oven when it is
at the hottest; nay, when it is too hot for the baker (so the learned
Dr. Pocock), when it is hotter than he would have it, so that the raiser
up of the fire ceases as long as while the dough that is kneaded is in
the fermenting, that the heat may abate a little. Thus fiery hot are the
lusts of an unclean heart. `(2.)` The unclean wait for an opportunity to
compass their wicked desires; having made ready their heart like an
oven, they lie in wait to catch their prey. The eye of the adulterer
waits for the twilight, Job 24:15. Their baker sleeps all the night, but
in the morning it burns as a flaming fire. As the baker, having kindled
a fire in his oven and laid sufficient fuel to it, goes to bed, and
sleeps all night, and in the morning finds his oven well heated, and
ready for his purpose, so these wicked people, when they have laid some
wicked plot, and formed a design for the gratifying of some covetous,
ambitious, revengeful, or unclean lusts, have their hearts so fully set
in them to do evil that, though they may stifle them for a while, yet
the fire of corrupt affections is still glowing within, and, as soon as
ever there is an opportunity for it, their purposes which they have
compassed and imagined break out into overt acts, as a fire flames out
when it has vent given it. Thus they are all hot as an oven. Note, Lust
in the heart is like fire in an oven, puts it into a heat; but the day
is coming when those who thus make themselves like a fiery oven with
their own vile affections, if that fire be not extinguished by divine
grace, shall be made as a fiery oven by divine wrath (Ps. 21:9), when
the day comes that shall burn as an oven, Mal. 4:1.

`4.` They resist the proper methods of reformation and redress: They have
devoured their judges, those few good judges that were among them, that
would have put out these fires with which they were heated; they fell
foul upon them, and would not suffer them to do justice, but were ready
to stone them, and perhaps did so; or, as some think, they provoked God
to deprive them of the blessing of magistracy and to leave all in
confusion: All their kings have fallen one after another, and their
families with them, which could not but put the kingdom into confusion,
crumble it into contending parties, and occasion a great deal of
bloodshed. There are heart-burnings among them; they are hot as an oven
with rage and malice at one another, and this occasions the devouring of
their judges, the falling of their kings. For the transgressions of a
land many are the princes thereof, Prov. 28:2. But in the midst of all
this trouble and disorder there is none among them that calls unto God,
that sees his hand stretched out against them in these judgments, and
deprecates the strokes of it, none, or next to none, that stir up
themselves to take hold on God, Isa. 64:7. Note, Those are not only
heated with sin, but hardened in sin, that continue to live without
prayer even when they are in trouble and distress.

### Verses 8-16

Having seen how vicious and corrupt the court was, we now come to
enquire how it is with the country, and we find that to be no better;
and no marvel if the distemper that has so seized the head affect the
whole body, so that there is no soundness in it; the iniquity of Ephraim
is discovered, as well as the sin of Samaria, of the people as well as
the princes, of which here are divers instances.

`I.` They were not peculiar and entire for God, as they should have been,
v. 8. 1. They did not distinguish themselves from the heathen, as God
had distinguished them: Ephraim, he has mingled himself among the
people, has associated with them, and conformed himself to them, and has
in a manner confounded himself with them and lost his character among
them. God had said, The people shall dwell alone; but they mingled
themselves with the heathen and learned their works, Ps. 16:35. They
went up and down among the heathen, to beg help of one of them against
another (so some); whereas, if they had kept close to God, they would
not have needed the help of any of them. 2. They were not entirely
devoted to God: Ephraim is a cake not turned, and so is burnt on one
side and dough on the other side, but good for nothing on either side.
As in Ahab\'s time, so now, they halted between God and Baal; sometimes
they seemed zealous for God, but at other times as hot for Baal. Note,
It is sad to think how many, who, after a sort, profess religion, are
made up of contraries and inconsistencies, as a cake not turned, a
constant self-contradiction, and always in one extreme or the other.

`II.` They were strangely insensible of the judgments of God, which they
were under, and which threatened their ruin, v. 9. Observe, 1. The
condition they were in. God was not to them, in his judgments, as a moth
and as rottenness; they were silently and slowly drawing towards the
ruin of their state partly by the encroachments of foreigners upon them:
Strangers have devoured his strength, and eaten him up; they have wasted
his wealth and treasure, lessened his numbers, and consumed the fruits
of the earth. Some devoured them by open wars (as 2 Ki. 13:7, when the
king of Syria made them like the dust by threshing), others by
pretending treaties of peace and amity, in which they extorted abundance
of wealth from them, and made them pay dearly for that which did them no
good, but which afterwards they paid more dearly for, as 2 Ki. 16:9.
This Ephraim got by mingling with the heathen, and suffering them to
mingle with him; they devoured that which he rested upon and supported
himself with. Note, Those that make not God their strength (Ps. 52:7)
make that their strength which will soon be devoured by strangers. They
were thus reduced partly by their own mal-administrations among
themselves: Yea, gray hairs are here and there upon him (are sprinkled
upon him, so the word is), that is, the sad symptoms of a decaying
declining state, which is waxing old and ready to vanish away, and the
effects of trouble and vexation. Cura facit canos-Care turns gray. The
almond-tree does not as yet flourish, but it begins to turn colour,
which speaks aloud to him that the evil days are coming, and the years
of which he shall say, I have no pleasure in them, Eccl. 12:1, 5. 2.
Their regardlessness of these warnings: He knows it not; he is not aware
of the hand of God gone out against him; it is lifted up, but he will
not see, Isa. 26:11. He does not know how near his ruin is, and takes no
care to prevent it. Note, Stupidity under less judgments is a presage of
greater coming.

`III.` They went on frowardly in their wicked ways, and were not
reclaimed by the rebukes they were under (v. 10): The pride of Israel
still testifies to his face, as it had done before (ch. 5:5); under
humbling providences their hearts were still unhumbled, their lusts
unmortified; and it is through the pride of their countenance that they
will not seek after God (Ps. 10:4); they do not return to the Lord their
God by repentance and reformation, nor do they seek him by faith and
prayer for all this; though they suffer for going astray from him,
though it can never be well with them till they come back to him, and
though they have in vain sought to others for relief, yet they think not
of applying to God.

`IV.` They were infatuated in their counsels, and took very wrong methods
when they were in distress (v. 11, 12): Ephraim is like a silly dove
without heart. To be harmless as a dove, without gall, and not to hurt
or injure others, is commendable; but to be sottish as a dove, without
heart, that knows not how to defend herself and provide for her own
safety, is a shame.

`1.` The silliness of this dove is, `(1.)` That she laments not the loss of
her young that are taken from her, but will make her nest again in the
same place; so they have their people carried away by the enemy, and are
not affected with it, but continue their dealings with those that deal
barbarously with them. `(2.)` That she is easily enticed by the bait into
the net, and has no heart, no understanding, to discern her danger, as
many other fowls do, Prov. 1:17. She hastes to the snare, and knows not
that it is for her life (Prov. 7:23); so they were drawn into leagues
with neighbouring nations that were their ruin. `(3.)` That, when she is
frightened, she has not courage to stay in the dove-house, where she is
safe, and under the careful protection of her owner, but flutters and
hovers, seeking shelter first in one place, then in another, and thereby
exposes herself so much the more; so this people, when they were in
distress, sought not to God, did not fly like the doves to their windows
where they might have been secured from all the birds of prey that
struck at them, but threw themselves out of God\'s protection, and then
called to Egypt to help them, and went in all haste to Assyria, to seek
for that aid in vain which they might, by repentance and prayer, have
found nearer home, in their God. Note, It is a silly senseless thing for
those who have a God in heaven to trust to creatures for the refuge and
relief which are to be had in him only; and those that do so are a
people of no understanding, they are without heart. Now,

`2.` See what becomes of this silly dove (v. 12): When they shall go to
Egypt and Assyria, I will spread my net upon them. Note, Those that will
not abide by the mercy of God must expect to be pursued by the justice
of God. Here, `(1.)` They are ensnared: \"I will spread my net upon them,
bring them into straits, that they may see their folly and think of
returning.\" Note, It is common for those that go away from God to find
snares where they expected shelters. `(2.)` They are humbled; they soar
upward, proud of their foreign alliances and confiding in them; but I
will bring them down, let them fly ever so high, as the fowls of heaven,
that are shot flying. Note, God can and will bring those down that exalt
themselves as the eagle, Obad. 3, 4. `(3.)` They are made to smart for
their folly: I will chastise them. Note, The disappointments we meet
with in the creature, when we put a confidence in it, are a necessary
chastisement, or discipline, that we may learn to be wiser another time.
`(4.)` In all this the scripture is fulfilled. It is as their congregation
has heard; they have been many a time told by the word of God, read, and
preached, and sung, in their religious assemblies, that \"vain is the
help of man, that in the son of man there is no help; they have heard
both from the law and from the prophets what judgments God would bring
upon them for their wickedness; and as they have heard now they shall
see, they shall feel.\" Note, It concerns us to take notice of the word
of God which we hear from time to time in the congregation, and to be
governed by it, for we must shortly be judged by it; and it will justify
God in the condemnation of sinners, and aggravate it to them, that they
have had plain public warning given them of it; it is what their
congregation has heard many a time, but they would not take warning.
\"Son, remember thou wast told what would come of it; and now thou seest
they were not vain words.\" See Zec. 1:6.

`V.` They revolted from God and rebelled against him, notwithstanding the
various methods he took to retain them in their allegiance, v. 13-15.
Here observe,

`1.` How kindly and tenderly God had dealt with them, as a gracious
sovereign towards a people dear unto him, and whose prosperity he had
much at heart. He had redeemed them (v. 13), brought them, at first, out
of the land of Egypt, and, since, delivered them out of many a distress.
He had bound and strengthened their arms, v. 15. When their power was
weakened, like an arm broken or out of joint, God set it again, and
bound it, as a surgeon does a broken bone, to make it knit. God had
given Israel victories over the Syrians (2 Ki. 13:16, 17), had restored
their coast (2 Ki. 14:25, 26), had girded them with strength for battle.
\"Though I have chastened them\" (so the margin reads it), \"sometimes
corrected them for their faults and thereby taught them, at other times
strengthened their arms and relieved them, though I have used both fair
means and foul to work upon them, it was all to no purpose; they were
mercy-proof and judgment-proof.\"

`2.` How impudent their conduct had been towards him notwithstanding,
which is described here for the conviction and humiliation of all those
who have gone on in any way of wickedness, that they may see how
exceedingly sinful their sin is, how heinous, how the God of heaven
interprets it, how he resents it. `(1.)` He had courted them to him, and
taken them into covenant with himself; but they fled from him, as if he
had been their dangerous enemy who had always approved himself their
faithful friend. They wandered from him, as the silly dove from her
nest, for those who forsake God will find no rest nor settlement in the
creature, but wander endlessly. They fled from God when they forsook the
worship of him, and ran away from his service, and withdrew themselves
from their allegiance to him. `(2.)` He had given them his laws, which
were all holy, just, and good, by which he designed to keep them in the
right way; but they transgressed against him; they sinned with a high
hand and a stiff neck, wilfully and presumptuously (so the words
signifies); they broke through the fence of the divine law, and therein
thwarted the design of the divine love. `(3.)` He had made known his
truths to them, and given them all possible proofs of the sincerity of
his good-will to them; and yet they spoke lies against him. They set up
false gods in competition with him; they denied his providence and
power; thus they belied the Lord, Jer. 5:12. They rejected his messages
sent them by his prophets, and said that they should have peace, though
they went on in sin, directly against what he said. In their
hypocritical professions of religion, shows of devotion, and promises of
amendment, they lied to the Lord, which he took as lying against him.
`(4.)` He was their rightful Lord and King, and had always ruled in Jacob
with equity, and for the public good; and yet they rebelled against him,
v. 14. They not only went off from him, but took up arms against him,
would have deposed him if they could and set up another. `(5.)` He
designed well for them, but they imagined mischief against him, v. 15.
Sin is a mischievous thin; it is mischief against God, for it is treason
against his crown and dignity; not that the sinners can do any thing to
hurt their Creator (as one of the ancients observes on these words), but
what they can they do; and it is so much the worse when it is not done
by surprise, or through inadvertency, but designedly and with
contrivance. The Jews have a saying, which Dr. Pocock quotes here, The
thoughts of transgression are worse than the transgression. The
designing of mischief is doing it, in God\'s account. Compassing and
imagining the death of the king is treason by our law. Those that
imagine an evil thing, though it prove a vain thing (Ps. 2:1), will be
reckoned with for the imagination.

`3.` How they shall be punished for this (v. 13): Woe unto them! for they
have fled from me. Note, Those who flee from God have woes sent after
them, and are, without doubt, in a woeful case. The wrath of God is
revealed from heaven against them; the word of God says, Woe to them!
And observe what follows immediately, Destruction unto them! Note, The
woes of God\'s word have real effects; destruction makes them good. The
judgments of his hand shall verify the judgments of his mouth. Those
whom he curses, and pronounces woeful, they are cursed, they are woeful
indeed.

`VI.` Their shows of devotion and reformation were but shows, and in them
they did but mock God.

`1.` They pretended devotion, but it was not sincere, v. 14. When the
hand of God had gone forth against them they made some sort of
application to him. When he slew them, then they sought him. Lord, in
trouble have they visited thee. But it was all in hypocrisy. `(1.)` When
they were under personal troubles, and called upon God in secret, they
were not sincere in that: They have not cried unto me with their heart,
when they howled upon their beds. When they were chastened with pain
upon their beds, and the multitude of their bones with strong pains,
perhaps ill of the wounds they received in war, they cried, and groaned,
and complained in the forms of devotion, and, it may be, they used many
good words, proper enough for the circumstances they were in; they
cried, God help us, and, Lord, look upon us. But they did not cry with
their heart, and therefore God reckons it as no crying to him. Moses is
said to cry unto God when he spoke not a word, only his heart prayed
with faith and fervency, Ex. 14:15. These made a great noise, and said a
great deal, and yet did not cry to God, because their hearts were not
right with him, not subjected to his will, devoted to his honour, nor
employed in his service. To pray is to lift up the soul to God, this is
the essence of prayer. If this be not done, words, though ever so well
chosen, are but wind; but, if it be, it is an acceptable prayer, though
the groanings cannot be uttered. Note, Those do not pray to God at all
that do not pray in the spirit. Nay, God is so far from approving their
prayer and accepting it that he calls it howling. Some think it
intimates the noisiness of their prayers (they cried to God as they used
to cry to Baal, when they thought he must be awakened), or the brutish
violent passions which they vented in their prayers; they snarled at the
stone, and howled under the whip, but regarded not the hand. Or it
denotes that their hypocritical prayers were so far from being pleasing
to God that they were offensive to him; he was angry at their prayers.
The songs of the temple shall be howlings, Amos 8:3. God will be so far
from pitying them that he will justly laugh at their calamity, who have
so often laughed at his authority. `(2.)` When they were under public
troubles, and met together to implore God\'s favour, in that also they
were hypocritical; they assembled themselves, for fashion-sake, because
it was usual to call a solemn assembly in times of general mourning,
Zep. 2:1. But it was only to pray for corn and wine that they came
together, which were the things they wanted, and feared being deprived
of by the want of rain, the judgment they now laboured under. They did
not pray for the favour or grace of God, that God would give them
repentance, pardon their sins, and turn away his wrath, but only that he
would not take away from them their corn and wine. Note, Carnal hearts,
in their prayers to God, covet temporal mercies only, and dread and
deprecate no other but temporal judgments, for they have no sense of any
other.

`2.` They pretended reformation, but neither was that sincere, v. 16.
Here is, `(1.)` The sin of Israel: They return, that is, they make as if
they would return; they pretend to repent and amend their doings, but
they make nothing of it; they do not come home to God nor return to
their allegiance, whereas God says (Jer. 4:1), If thou wilt return, O
Israel! return to me; do not only turn towards me, but return to me.
This dissimulation of theirs makes them like a deceitful bow, which
looks as if it were fit for business, and is bent and drawn accordingly,
but, when strength comes to be laid to it, either the bow or the string
breaks, and the arrow, instead of flying to the mark, drops at the
archer\'s foot. Such were their essays towards repentance and
reformation. `(2.)` The sin of the princes of Israel. That which is
charged upon them is the rage of their tongue, quarrelling with God and
his providence and with all about them when they are crossed. Princes
think they may say what they will, and that it is their prerogative to
huff and bluster, to curse and rail, and to call names at their
pleasure, but let them know there is a God above them that will call
them to an account for the rage of their tongues and make their own
tongues to fall upon them. `(3.)` The punishment of Israel and their
princes for their sin. As for the princes, they shall fall by the sword
either of their enemies or of their own people, some by one and some by
the other; and this shall be their derision, this is that for which they
shall be derided in the land of Egypt, when they flee to the Egyptians
for succour, v. 11. Their sin and punishment shall make them a
laughing-stock to all about them. Note, Those that are treacherous and
deceitful in their dealings with God, and passionate and outrageous in
their conduct towards men, will justly be made a derision to their
neighbours, for they make themselves ridiculous.
