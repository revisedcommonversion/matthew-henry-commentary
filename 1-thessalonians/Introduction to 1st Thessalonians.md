Introduction to 1st Thessalonians
=================================

Thessalonica was formerly the metropolis of Macedonia; it is now called
Salonichi, and is the best peopled, and one of the best towns for
commerce, in the Levant. The apostle Paul, being diverted from his
design of going into the provinces of Asia, properly so called, and
directed after an extraordinary manner to preach the gospel in Macedonia
(Acts 16:9, 10), in obedience to the call of God went from Troas to
Samothracia, thence to Neapolis, and thence to Philippi, where he had
good success in his ministry, but met with hard usage, being cast into
prison with Silas his companion in travel and labour, from which being
wonderfully delivered, they comforted the brethren there, and departed.
Passing through Amphipolis and Apollonia, they came to Thessalonica,
where the apostle planted a church that consisted of some believing Jews
and many converted Gentiles, Acts 17:1-4. But a tumult being raised in
the city by the unbelieving Jews, and the lewd and baser sort of the
inhabitants, Paul and Silas, for their safety, were sent away by night
unto Berea, and afterwards Paul was conducted to Athens, leaving Silas
and Timotheus behind him, but sent directions that they should come to
him with all speed. When they came, Timotheus was sent to Thessalonica,
to enquire after their welfare and to establish them in the faith (1 Th.
3:2), and, returning to Paul while he tarried at Athens, was sent again,
together with Silas, to visit the churches in Macedonia. So that Paul,
being left at Athens alone (1 Th. 3:1), departed thence to Corinth,
where he continued a year and a half, in which time Silas and Timotheus
returned to him from Macedonia (Acts 18:5), and then he wrote this
epistle to the church of Christ at Thessalonica, which, though it is
placed after the other epistles of this apostle, is supposed to be first
in time of all Paul\'s epistles, and to be written about A.D. 51. The
main scope of it is to express the thankfulness of this apostle for the
good success his preaching had among them, to establish them in the
faith, and persuade them to a holy conversation.
