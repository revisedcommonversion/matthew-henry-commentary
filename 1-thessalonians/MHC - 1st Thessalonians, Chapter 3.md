1st Thessalonians, Chapter 3
============================

Commentary
----------

In this chapter the apostle gives further evidence of his love to the
Thessalonians, reminding them of his sending Timothy to them, with the
mention of his design therein and his inducements so to do (v. 1-5). He
acquaints them also with his great satisfaction at the return of
Timothy, with good tidings concerning them (v. 6-10). And concludes with
fervent prayer for them (v. 11 to the end).

### Verses 1-5

In these words the apostle gives an account of his sending Timothy to
the Thessalonians. Though he was hindered from going to them himself,
yet his love was such that he could not forbear sending Timothy to them.
Though Timothy was very useful to him, and he could not well spare him,
yet Paul was content, for their good, to be left alone at Athens. Note,
Those ministers do not duly value the establishment and welfare of their
people who cannot deny themselves in many things for that end. Observe,

`I.` The character he gives of Timothy (v. 2): We sent Timotheus, our
brother. Elsewhere he calls him his son; here he calls him brother.
Timothy was Paul\'s junior in age, his inferior in gifts and graces, and
of a lower rank in the ministry: for Paul was an apostle, and Timothy
but an evangelist; yet Paul calls him brother. This was an instance of
the apostle\'s humility, and showed his desire to put honour upon
Timothy and to recommend him to the esteem of the churches. He calls him
also a minister of God. Note, Ministers of the gospel of Christ are
ministers of God, to promote the kingdom of God among men. He calls him
also his fellow-labourer in the gospel of Christ. Note, Ministers of the
gospel must look upon themselves as labourers in the Lord\'s vineyard;
they have an honourable office and hard work, yet a good work. This is a
true saying, If any man desire the office of a bishop, he desires a good
work, 1 Tim. 3:1. And ministers should look upon one another, and
strengthen one another\'s hands, not strive and contend one with another
(which will hinder their work), but strive together to carry on the
great work they are engaged in, namely, to preach and publish the gospel
of Christ, and to persuade people to embrace and entertain it and live
suitably thereto.

`II.` The end and design why Paul sent Timothy: To establish you and to
comfort you concerning your faith, v. 2. Paul had converted them to the
Christian faith, and now he was desirous that they might be confirmed
and comforted, that they might confirmed in the choice they had made of
the Christian religion, and comforted in the profession and practice of
it. Note, The more we are comforted, the more we shall be confirmed,
because, when we find pleasure in the ways of God, we shall thereby be
engaged to continue and persevere therein. The apostle\'s design was to
establish and comfort the Thessalonians concerning their
faith,-concerning the object of their faith, namely, the truths of the
gospel, and particularly that Jesus Christ was the Saviour of the world,
and so wise and good, so powerful and faithful, that they might rely
upon him,-concerning the recompence of faith, which was more than
sufficient to balance all their losses and reward all their labours.

`III.` The motive inducing Paul to send Timothy for this end, namely, a
godly fear or jealousy, lest they should be moved from the faith of
Christ, v. 3. He was desirous that no man, no one among them, should be
moved or shaken in mind, that they should not apostatize or waver in the
faith. And yet,

`1.` He apprehended there was danger, and feared the consequence.

`(1.)` There was danger, `[1.]` By reason of affliction and persecution
for the sake of the gospel, v. 3. These Thessalonians could not but
perceive what afflictions the apostles and preachers of the gospel met
with, and this might possibly stumble them; and also those who made
profession of the gospel were persecuted, and without doubt these
Thessalonians themselves were afflicted. `[2.]` By reason of the
tempter\'s subtlety and malice. The apostle was afraid lest by any means
the tempter had tempted them, v. 5. The devil is a subtle and unwearied
tempter, who seeks an opportunity to beguile and destroy us, and takes
all advantages against us, both in a time of prosperity and adversity;
and he has often been successful in his attacks upon persons under
afflictions. He has often prejudiced the minds of men against religion
on account of the sufferings its professors are exposed to. We have
reason therefore to be jealous over ourselves and others, lest we be
ensnared by him.

`(2.)` The consequence the apostle feared was lest his labour should be in
vain. And thus it would have been, if the tempter had tempted them, and
prevailed against them, to move them from the faith. They would have
lost what they had wrought, and the apostle would have lost what he
laboured for. Note, It is the devil\'s design to hinder the good fruit
and effect of the preaching of the gospel. If he cannot hinder ministers
from labouring in the word and doctrine, he will, if he be able, hinder
them of the success of their labours. Note also, Faithful ministers are
much concerned about the success of their labours. No one would
willingly labour in vain; and ministers are loth to spend their
strength, and pains, and time, for nought.

`2.` To prevent this danger, with its bad consequence, the apostle tells
them what care he took in sending Timothy, `(1.)` To put them in mind of
what he had told them before concerning suffering tribulation (v. 4), he
says (v. 3), We are appointed thereunto, that is, unto afflictions. So
is the will and purpose of God that through many afflictions we must
enter into his kingdom. Their troubles and persecutions did not come by
chance, not merely from the wrath and malice of the enemies of religion,
but by the appointment of God. The event only came to pass according as
God had determined, and they knew he had told them before it would be;
so that they should not think it strange, and, being fore-warned, they
should be fore-armed. Note, The apostles were so far from flattering
people with an expectation of worldly prosperity in religion that, on
the contrary, they told them plainly they must count upon trouble in the
flesh. And herein they followed the example of their great Master, the
author or our faith. Besides, it might prove a confirmation of their
faith, when they perceived that it only happened to them as was
predicted before. `(2.)` To know their faith, that so he might inform the
apostles whether they remained stedfast under all their sufferings,
whether their faith failed or not, because, if their faith did not fail,
they would be able to stand their ground against the tempter and all his
temptations: their faith would be a shield, to defend them against all
the fiery darts of the wicked, Eph. 6:16.

### Verses 6-10

Here we have Paul\'s great satisfaction upon the return of Timothy with
good tidings from the Thessalonians, in which we may observe,

`I.` The good report Timothy made concerning them, v. 6. Without question,
he was a willing messenger of these good tidings. Concerning their
faith, that is, concerning their stedfastness in the faith, that they
were not shaken in mind, nor turned aside form the profession of the
gospel. Their love also continued; their love to the gospel, and the
ministers of the gospel. For they had a good and a kind remembrance of
the apostles, and that constantly, or always. The names of the apostles
were very dear to them, and the thoughts of them, and what they
themselves had received from them, were very precious, insomuch that
they desired greatly to see them again, and receive some spiritual gift
from them; and there was no love lost, for the apostle was as desirous
to see them. It is happy where there is such mutual love between
minister and people. This tends to promote religion, and the success of
the gospel. The world hates them, and therefore they should love one
another.

`II.` The great comfort and satisfaction the apostle had in this good
report concerning them (v. 7, 8): Therefore, brethren, we were comforted
in all our affliction and distress. The apostle thought this good news
of them was sufficient to balance all the troubles he met with. It was
easy to him to bear affliction, or persecution, or fightings from
without, when he found the good success of his ministry and the
constancy of the converts he had made to Christianity; and his distress
of mind on account of his fears within, lest he had laboured in vain,
was now in a good measure over, when he understood their faith and the
perseverance of it. This put new life and spirit into the apostle and
made him vigorous and active in the work of the Lord. Thus he was not
only comforted, but greatly rejoiced also: Now we live, if you stand
fast in the Lord, v. 8. It would have been a killing thing to the
apostles if the professors of religion had been unsteady, or proved
apostates; whereas nothing was more encouraging than their constancy.

`III.` The effects of this were thankfulness and prayer to God on their
behalf. Observe, 1. How thankful the apostle was, v. 9. He was full of
joy, and full of praise and thanksgiving. When we are most cheerful we
should be most thankful. What we rejoice in we should give thanks for.
This is to rejoice before our God, to spiritualize our joy. Paul speaks
as if he could not tell how to express his thankfulness to God, or his
joy and rejoicing for their sakes. But he was careful God should not
lose the glory of the comfort he received in the welfare of his friends.
His heart was enlarged with love to them and with thanksgiving to God.
He was willing to express the one and the other as well as he could. As
to thankfulness to God, this especially is very imperfect in the present
state; but, when we come to heaven, we shall do this work better than
now we can. 2. He prayed for them night and day (v. 10), evening and
morning, or very frequently, in the midst of the business of the day or
slumber of the night lifting up his heart to God in prayer. Thus we
should pray always. And Paul\'s prayer was fervent prayer. He prayed
exceedingly, and was earnest in his supplication. Note, When we are most
thankful we should always give ourselves to prayer; and those we give
thanks for have yet need to be prayed for. Those whom we most rejoice
in, and who are our greatest comforts, must be our constant care, while
in this world of temptation and imperfection. There was something still
lacking in their faith; Paul desired that this might be perfected, and
to see their face in order thereunto. Note, `(1.)` The best of men have
something wanting in their faith, if not as to the matter of it, there
being some mysteries or doctrines not sufficiently known or believed by
them, yet as to the clearness and certainty of their faith, there being
some remaining darkness and doubtings, or at least as to the effects and
operations of it, these being not so conspicuous and perfect as they
should be. And, `(2.)` The ministry of the word and ordinances is helpful,
and to be desired and used for the perfecting of that which is lacking
in our faith.

### Verses 11-13

In these words we have the earnest prayer of the apostle. He desired to
be instrumental in the further benefit of the Thessalonians; and the
only way to be so while at a distance was by prayer for them, together
with his writing or sending to them. He desired that their faith might
be perfected, which he could not be the proper cause or author of; for
he pretended not to dominion over their faith, nor to have the donation
of it, and he therefore concludes with prayer for them. Observe,

`I.` Whom he prays to, namely, God and Christ. Prayer is a part of
religious worship, and all religious worship is due unto God only.
Prayer is here made to God, even the Father and our Father; and also to
Christ, even our Lord Jesus Christ. Therefore Jesus Christ our Lord is
God, even as God our Father is God. Prayer is to be offered to God as
our Father. So Christ taught his disciples to pray; and so the Spirit of
adoption prompts them to pray, to cry, Abba Father. Prayer is not only
to be offered in the name of Christ, but offered up to Christ himself,
as our Lord and our Saviour.

`II.` What he prays for, with respect to himself and his
fellow-labourers, and on behalf of the Thessalonians.

`1.` He prays that himself and fellow-labourers might have a prosperous
journey to them by the will of God, that their way might be directed to
them, v. 11. The taking of a journey to this or that place, one would
think, is a thing depending so much on a man\'s own will, and lies so
much in his own power, that Paul needed not by prayer to go to God about
it. But the apostle knew that in God we live, and move, and have our
being, that we depend upon God in all our motions and actions, as well
as for the continuance of life and being, that divine Providence orders
all our affairs and that it is owing thereto if we prosper therein, that
God our Father directs and orders his children whither they shall go and
what they shall do, that our Lord Jesus Christ in a particular manner
directs the motions of his faithful ministers, those stars which he
holds in his right hand. Let us acknowledge God in all our ways, and he
will direct our paths.

`2.` He prays for the prosperity of the Thessalonians. Whether he should
have an opportunity of coming to them or not, yet he earnestly prayed
for the prosperity of their souls. And there are two things he desired
for them, which we should desire for ourselves and friends:-`(1.)` That
they might increase and abound in love (v. 12), in love to one another
and in love to all men. Note, Mutual love is required of all Christians,
and not only that they love one another, but that they also have a
charitable disposition of mind and due concern for the welfare of all
men. Love is of God, and is the fulfilling of the gospel as well as of
the law. Timothy brought good tidings of their faith, yet something was
lacking therein; and of their charity, yet the apostle prays that this
might increase and abound. Note, We have reason to desire to grow in
every grace, and have need of the Spirit\'s influence in order to growth
in grace; and the way to obtain this is by prayer. We are beholden to
God not only for the stock put into our hands at first, but for the
improvement of it also. And to our prayer we must add endeavour. To
excite this in the Thessalonians the apostle again mentions his love,
his abounding love, towards them. The more we are beloved, the more
affectionate we should be. `(2.)` That they might be established
unblamable in holiness, v. 13. This spiritual benefit is mentioned as an
effect of increasing and abounding love: To the end that he (the Lord)
may establish your hearts. Note, The more we grow and abound in grace,
and particularly in the grace of love, the more we are established and
confirmed in it. Note also, Holiness is required of all those who would
go to heaven, and therein we must be unblamable; that is, we must act in
every thing so that we may not in the least contradict the profession we
make of holiness. Our desire should be to have our hearts established in
holiness before God, and be preserved safe, to the coming of the Lord
Jesus Christ; and that we may be unblamable before God, even the Father,
now, and be presented blameless before the throne of his glory, when the
Lord Jesus shall come with all his saints. Note, `[1.]` The Lord Jesus
will certainly come, and come in his glory. `[2.]` When he comes, his
saints will come with him: They shall appear with him in glory. `[3.]`
Then the excellency as well as the necessity of holiness will appear,
because without this no hearts shall be established at that day, nor
shall any one be unblamable, or avoid everlasting condemnation.
