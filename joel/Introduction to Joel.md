Introduction to Joel
====================

We are altogether uncertain concerning the time when this prophet
prophesied; it is probable that it was about the same time Amos
prophesied, not for the reason that the rabbin give, \"Because Amos
begins his prophecy with that wherewith Joel concludes his, The Lord
shall roar out of Zion,\" but for the reason Dr. Lightfoot gives,
\"Because he speaks of the same judgments of locusts, and drought, and
fire, that Amos laments, which is an intimation that they appeared about
the same time, Amos in Israel and Joel in Judah. Hosea and Obadiah
prophesied about the same time; and it appears that Amos prophesied in
the says of Jeroboam, the second king of Israel, Amos 7:10. God sent a
variety of prophets, that they might strengthen the hands one of
another, and that out of the mouth of two or three witnesses every word
might be established. In this prophecy, `I.` The desolations made by hosts
of noxious insects is described, ch. 1 and part of ch. 2. `II.` The people
are hereupon called to repentance, ch. 2. `III.` Promises are made of the
return of mercy upon their repentance (ch. 2), and promises of the
pouring out of the Spirit in the latter days. `IV.` The cause of God\'s
people is pleaded against their enemies, whom God would in due time
reckon with (ch. 3); and glorious things are spoken of the
gospel-Jerusalem and of the prosperity and perpetuity of it.
