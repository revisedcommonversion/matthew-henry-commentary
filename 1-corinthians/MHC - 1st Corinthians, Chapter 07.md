1st Corinthians, Chapter 7
==========================

Commentary
----------

In this chapter the apostle answers some cases proposed to him by the
Corinthians about marriage. He, `I.` Shows them that marriage was
appointed as a remedy against fornication, and therefore that persons
had better marry than burn (v. 1-9). `II.` He gives direction to those who
are married to continue together, though they might have an unbelieving
relative, unless the unbeliever would part, in which case a Christian
would not be in bondage (v. 10-16). `III.` He shows them that becoming
Christians does not change their external state; and therefore advises
everyone to continue, in the general, in that state in which he was
called (v. 17-24). `IV.` He advises them, by reason of the present
distress, to keep themselves unmarried; hints the shortness of time, and
how they should improve it, so as to grow dead and indifferent to the
comforts of the world; and shows them how worldly cares hinder their
devotions, and distract them in the service of God (v. 25-35). `V.` He
directs them in the disposal of their virgins (v. 36-38). `VI.` And closes
the chapter with advice to widows how to dispose of themselves in that
state (v. 39, 40).

### Verses 1-9

The apostle comes now, as a faithful and skilful casuist, to answer some
cases of conscience which the Corinthians had proposed to him. Those
were things whereof they wrote to him, v. 1. As the lips of ministers
should keep knowledge, so the people should ask the law at their mouths.
The apostle was as ready to resolve as they were to propose their
doubts. In the former chapter, he warns them to avoid fornication; here
he gives some directions about marriage, the remedy God had appointed
for it. He tells them in general,

`I.` That it was good, in that juncture of time at least, to abstain from
marriage altogether: It is good for a man not to touch a woman (not to
take her to wife), by good here not understanding what is so conformable
to the mind and will of God as if to do otherwise were sin, an extreme
into which many of the ancients have run in favour of celibacy and
virginity. Should the apostle be understood in this sense, he would
contradict much of the rest of his discourse. But it is good, that is,
either abstracting from circumstances there are many things in which the
state of celibacy has the advantage above the marriage state; or else at
this juncture, by reason of the distress of the Christian church, it
would be a convenience for Christians to keep themselves single,
provided they have the gift of continency, and at the same time can keep
themselves chaste. The expression also may carry in it an intimation
that Christians must avoid all occasions of this sin, and flee all
fleshly lusts, and incentives to them; must neither look on nor touch a
woman, so as to provoke lustful inclinations. Yet,

`II.` He informs them that marriage, and the comforts and satisfactions
of that state, are by divine wisdom prescribed for preventing
fornication (v. 2), Porneias-Fornications, all sorts of lawless lust. To
avoid these, Let every man, says he, have his own wife, and every woman
her own husband; that is, marry, and confine themselves to their own
mates. And, when they are married, let each render the other due
benevolence (v. 3), consider the disposition and exigency of each other,
and render conjugal duty, which is owing to each other. For, as the
apostle argues (v. 4), in the married state neither person has power
over his own body, but has delivered it into the power of the other, the
wife hers into the power of the husband, the husband his into the power
of the wife. Note, Polygamy, or the marriage of more persons than one,
as well as adultery, must be a breach of marriage-covenants, and a
violation of the partner\'s rights. And therefore they should not
defraud one another of the use of their bodies, nor any other of the
comforts of the conjugal state, appointed of God for keeping the vessel
in sanctification and honour, and preventing the lusts of uncleanness,
except it be with mutual consent (v. 5) and for a time only, while they
employ themselves in some extraordinary duties of religion, or give
themselves to fasting and prayer. Note, Seasons of deep humiliation
require abstinence from lawful pleasures. But this separation between
husband and wife must not be for a continuance, lest they expose
themselves to Satan\'s temptations, by reason of their incontinence, or
inability to contain. Note, Persons expose themselves to great danger by
attempting to perform what is above their strength, and at the same time
not bound upon them by any law of God. If they abstain from lawful
enjoyments, they may be ensnared into unlawful ones. The remedies God
hath provided against sinful inclinations are certainly best.

`III.` The apostle limits what he had said about every man\'s having his
own wife, etc. (v. 2): I speak this by permission, not of command. He
did not lay it as an injunction upon every man to marry without
exception. Any man might marry. No law of God prohibited the thing. But,
on the other hand, not law bound a man to marry so that he sinned if he
did not; I mean, unless his circumstances required it for preventing the
lust of uncleanness. It was a thing in which men, by the laws of God,
were in a great measure left at liberty. And therefore Paul did not bind
every man to marry, though every man had an allowance. No, he could wish
all men were as himself (v. 7), that is, single, and capable of living
continently in that state. There were several conveniences in it, which
at that season, if not at others, made it more eligible in itself. Note,
It is a mark of true goodness to wish all men as happy as ourselves. But
it did not answer the intentions of divine Providence as well for all
men to have as much command of this appetite as Paul had. It was a gift
vouchsafed to such persons as Infinite Wisdom thought proper: Every one
hath his proper gift of God, one after this manner and another after
that. Natural constitutions vary; and, where there may not be much
difference in the constitution, different degrees of grace are
vouchsafed, which may give some a greater victory over natural
inclination than others. Note, The gifts of God, both in nature and
grace, are variously distributed. Some have them after this manner and
some after that. Paul could wish all men were as himself, but all men
cannot receive such a saying, save those to whom it is given, Mt. 19:11.

`IV.` He sums up his sense on this head (v. 9, 10): I say therefore to
the unmarried and widows, to those in a state of virginity or widowhood,
It is good for them if they abide even as `I.` There are many
conveniences, and especially at this juncture, in a single state, to
render it preferable to a married one. It is convenient therefore that
the unmarried abide as I, which plainly implies that Paul was at that
time unmarried. But, if they cannot contain, let them marry; for it is
better to marry than to burn. This is God\'s remedy for lust. The fire
may be quenched by the means he has appointed. And marriage, with all
its inconveniences, is much better than to burn with impure and lustful
desires. Marriage is honourable in all; but it is a duty in those who
cannot contain nor conquer those inclinations.

### Verses 10-16

In this paragraph the apostle gives them direction in a case which must
be very frequent in that age of the world, especially among the Jewish
converts; I mean whether they were to live with heathen relatives in a
married state. Moses\'s law permitted divorce; and there was a famous
instance in the Jewish state, when the people were obliged to put away
their idolatrous wives, Ezra 10:3. This might move a scruple in many
minds, whether converts to Christianity were not bound to put away or
desert their mates, continuing infidels. Concerning this matter the
apostle here gives direction. And,

`I.` In general, he tells them that marriage, by Christ\'s command, is for
life; and therefore those who are married must not think of separation.
The wife must not depart from the husband (v. 10), nor the husband put
away his wife, v. 11. This I command, says the apostle; yet not I, but
the Lord. Not that he commanded anything of his own head, or upon his
own authority. Whatever he commanded was the Lord\'s command, dictated
by his Spirit and enjoined by his authority. But his meaning is that the
Lord himself, with his own mouth, had forbidden such separations, Mt.
5:32; 19:9; Mk. 10:11; Lu. 16:18. Note, Man and wife cannot separate at
pleasure, nor dissolve, when they will, their matrimonial bonds and
relation. They must not separate for any other cause than what Christ
allows. And therefore the apostle advises that if any woman had been
separated, either by a voluntary act of her own or by an act of her
husband, she should continue unmarried, and seek reconciliation with her
husband, that they might cohabit again. Note, Husbands and wives should
not quarrel at all, or should be quickly reconciled. They are bound to
each other for life. The divine law allows of no separation. They cannot
throw off the burden, and therefore should set their shoulders to it,
and endeavour to make it as light to each other as they can.

`II.` He brings the general advice home to the case of such as had an
unbelieving mate (v. 12): But to the rest speak I, not the Lord; that
is, the Lord had not so expressly spoken to this case as to the former
divorce. It does not mean that the apostle spoke without authority from
the Lord, or decided this case by his own wisdom, without the
inspiration of the Holy Ghost. He closes this subject with a declaration
to the contrary (v. 40), I think also that I have the Spirit of God.
But, having thus prefaced his advice, we may attend,

`1.` To the advice itself, which is that if an unbelieving husband or
wife were pleased to dwell with a Christian relative, the other should
not separate. The husband should not put away an unbelieving wife, nor
the wife leave an unbelieving husband, v. 12, 13. The Christian calling
did not dissolve the marriage covenant, but bind it the faster, by
bringing it back to the original institution, limiting it to two
persons, and binding them together for life. The believer is not by
faith in Christ loosed from matrimonial bonds to an unbeliever, but is
at once bound and made apt to be a better relative. But, though a
believing wife or husband should not separate from an unbelieving mate,
yet if the unbelieving relative desert the believer, and no means can
reconcile to a cohabitation, in such a case a brother or sister is not
in bondage (v. 15), not tied up to the unreasonable humour, and bound
servilely to follow or cleave to the malicious deserter, or not bound to
live unmarried after all proper means for reconciliation have been
tried, at least of the deserter contract another marriage or be guilty
of adultery, which was a very easy supposition, because a very common
instance among the heathen inhabitants of Corinth. In such a case the
deserted person must be free to marry again, and it is granted on all
hands. And some think that such a malicious desertion is as much a
dissolution of the marriage-covenant as death itself. For how is it
possible that the two shall be one flesh when the one is maliciously
bent to part from or put away the other? Indeed, the deserter seems
still bound by the matrimonial contract; and therefore the apostle says
(v. 11), If the woman depart from her husband upon the account of his
infidelity, let her remain unmarried. But the deserted party seems to be
left more at liberty (I mean supposing all the proper means have been
used to reclaim the deserter, and other circumstances make it necessary)
to marry another person. It does not seem reasonable that they should be
still bound, when it is rendered impossible to perform conjugal duties
or enjoy conjugal comforts, through the mere fault of their mate: in
such a case marriage would be a state of servitude indeed. But, whatever
liberty be indulged Christians in such a case as this, they are not
allowed, for the mere infidelity of a husband or wife, to separate; but,
if the unbeliever be willing, they should continue in the relation, and
cohabit as those who are thus related. This is the apostle\'s general
direction.

`2.` We have here the reasons of this advice. `(1.)` Because the relation
or state is sanctified by the holiness of either party: For the
unbelieving husband is sanctified by the wife, and the unbelieving wife
by the husband (v. 14), or hath been sanctified. The relation itself,
and the conjugal use of each other, are sanctified to the believer. To
the pure all things are pure, Tit. 1:15. Marriage is a divine
institution; it is a compact for life, by God\'s appointment. Had
converse and congress with unbelievers in that relation defiled the
believer, or rendered him or her offensive to God, the ends of marriage
would have been defeated, and the comforts of it in a manner destroyed,
in the circumstances in which Christians then were. But the apostle
tells them that, though they were yoked with unbelievers, yet, if they
themselves were holy, marriage was to them a holy state, and marriage
comforts, even with an unbelieving relative, were sanctified enjoyments.
It was no more displeasing to God for them to continue to live as they
did before, with their unbelieving or heathen relation, than if they had
become converts together. If one of the relatives had become holy,
nothing of the duties or lawful comforts of the married state could
defile them, and render them displeasing to God, though the other were a
heathen. He is sanctified for the wife\'s sake. She is sanctified for
the husband\'s sake. Both are one flesh. He is to be reputed clean who
is one flesh with her that is holy, and vice versâ: Else were your
children unclean, but now are they holy (v. 14), that is, they would be
heathen, out of the pale of the church and covenant of God. They would
not be of the holy seed (as the Jews are called, Isa. 6:13), but common
and unclean, in the same sense as heathens in general were styled in the
apostle\'s vision, Acts 10:28. This way of speaking is according to the
dialect of the Jews, among whom a child begotten by parents yet
heathens, was said to be begotten out of holiness; and a child begotten
by parents made proselytes was said to be begotten intra
sanctitatem-within the holy enclosure. Thus Christians are called
commonly saints; such they are by profession, separated to be a peculiar
people of God, and as such distinguished from the world; and therefore
the children born to Christians, though married to unbelievers, are not
to be reckoned as part of the world, but of the church, a holy, not a
common and unclean seed. \"Continue therefore to live even with
unbelieving relatives; for, if you are holy, the relation is so, the
state is so, you may make a holy use even of an unbelieving relative, in
conjugal duties, and your seed will be holy too.\" What a comfort is
this, where both relatives are believers! `(2.)` Another reason is that
God hath called Christians to peace, v. 15. The Christian religion
obliges us to act peaceably in all relations, natural and civil. We are
bound, as much as in us lies, to live peaceably with all men (Rom.
12:18), and therefore surely to promote the peace and comfort of our
nearest relatives, those with whom we are one flesh, nay, though they
should be infidels. Note, It should be the labour and study of those who
are married to make each other as easy and happy as possible. `(3.)` A
third reason is that it is possible for the believing relative to be an
instrument of the other\'s salvation (v. 16): What knowest thou, O wife,
whether thou shalt save thy husband? Note, It is the plain duty of those
in so near a relation to seek the salvation of those to whom they are
related. \"Do not separate. There is other duty now called for. The
conjugal relation calls for the most close and endeared affection; it is
a contract for life. And should a Christian desert a mate, when an
opportunity offers to give the most glorious proof of love? Stay, and
labour heartily for the conversion of thy relative. Endeavour to save a
soul. Who knows but this may be the event? It is not impossible. And,
though there be no great probability, saving a soul is so good and
glorious a service that the bare possibility should put one on exerting
one\'s self.\" Note, Mere possibility of success should be a sufficient
motive with us to use our diligent endeavours for saving the souls of
our relations. \"What know I but I may save his soul? should move me to
attempt it.\"

### Verses 17-24

Here the apostle takes occasion to advise them to continue in the state
and condition in which Christianity found them, and in which they became
converts to it. And here,

`I.` He lays down this rule in general-as God hath distributed to every
one. Note, Our states and circumstances in this world are distributions
of divine Providence. This fixes the bounds of men\'s habitations, and
orders their steps. God setteth up and pulleth down. And again, As the
Lord hath called every one, so let him walk. Whatever his circumstances
or condition was when he was converted to Christianity, let him abide
therein, and suit his conversation to it. The rules of Christianity
reach every condition. And in every state a man may live so as to be a
credit to it. Note, It is the duty of every Christian to suit his
behaviour to his condition and the rules of religion, to be content with
his lot, and conduct himself in his rank and place as becomes a
Christian. The apostle adds that this was a general rule, to be observed
at all times and in all places; So ordain I in all churches.

`II.` He specifies particular cases; as, 1. That of circumcision. Is any
man called being circumcised? Let him not be uncircumcised. Is any man
called being uncircumcised? Let him not be circumcised. It matters not
whether a man be a Jew or Gentile, within the covenant of peculiarity
made with Abraham or without it. He who is converted, being a Jew, has
no need to give himself uneasiness upon that head, and wish himself
uncircumcised. Nor, is he who is converted from Gentilism under an
obligation to be circumcised: nor should he be concerned because he
wants that mark of distinction which did heretofore belong to the people
of God. For, as the apostle goes on, circumcision is nothing, and
uncircumcision is nothing, but keeping the commandments of God, v. 19.
In point of acceptance with God, it is neither here nor there whether
men be circumcised or not. Note, It is practical religion, sincere
obedience to the commands of God, on which the gospel lays stress.
External observances without internal piety are as nothing. Therefore
let every man abide in the calling (the state) wherein he was called, v.
20. 2. That of servitude and freedom. It was common in that age of the
world for many to be in a state of slavery, bought and sold for money,
and so the property of those who purchased them. \"Now,\" says the
apostle, \"art thou called being a servant? Care not for it. Be not
over-solicitous about it. It is not inconsistent with thy duty,
profession, or hopes, as a Christian. Yet, if thou mayest be made free,
use it rather,\" v. 21. There are many conveniences in a state of
freedom above that of servitude: a man has more power over himself, and
more command of his time, and is not under the control of another lord;
and therefore liberty is the more eligible state. But men\'s outward
condition does neither hinder nor promote their acceptance with God. For
he that is called being a servant is the Lord\'s freed-man-apeleutheros,
as he that is called being free is the Lord\'s servant. Though he be not
discharged from his master\'s service, he is freed from the dominion and
vassalage of sin. Though he be not enslaved to Christ, yet he is bound
to yield himself up wholly to his pleasure and service; and yet that
service is perfect freedom. Note, Our comfort and happiness depend on
what we are to Christ, not what we are in the world. The goodness of our
outward condition does not discharge us from the duties of Christianity,
nor the badness of it debar us from Christian privileges. He who is a
slave may yet be a Christian freeman; he who is a freeman may yet be
Christ\'s servant. He is bought with a price, and should not therefore
be the servant of man. Not that he must quit the service of his master,
or not take all proper measures to please him (this were to contradict
the whole scope of the apostle\'s discourse); but he must not be so the
servant of men but that Christ\'s will must be obeyed, and regarded,
more than his master\'s. He has paid a much dearer price for him, and
has a much fuller property in him. He is to be served and obeyed without
limitation or reserve. Note, The servants of Christ should be at the
absolute command of no other master besides himself, should serve no
man, any further than is consistent with their duty to him. No man can
serve two masters. Though some understand this passage of persons being
bought out of slavery by the bounty and charity of fellow-Christians;
and read the passage thus, Have you been redeemed out of slavery with a
price? Do not again become enslaved; just as before he had advised that,
if in slavery they had any prospect of being made free, they should
choose it rather. This meaning the words will bear, but the other seems
the more natural. See ch. 6:20.

`III.` He sums up his advice: Let every man wherein he is called abide
therein with God, v. 24. This is to be understood of the state wherein a
man is converted to Christianity. No man should make his faith or
religion an argument to break through any natural or civil obligations.
He should quietly and comfortably abide in the condition in which he is;
and this he may well do, when he may abide therein with God. Note, The
special presence and favour of God are not limited to any outward
condition or performance. He may enjoy it who is circumcised; and so may
he who is uncircumcised. He who is bound may have it as well as he who
is free. In this respect there is neither Greek nor Jew, circumcision
nor uncircumcision, barbarian nor Scythian, bond nor free, Col. 3:11.
The favour of God is not bound.

### Verses 25-35

The apostle here resumes his discourse, and gives directions to virgins
how to act, concerning which we may take notice,

`I.` Of the manner wherein he introduces them: \"Now concerning virgins I
have no commandment of the Lord, v. 25. I have no express and universal
law delivered by the Lord himself concerning celibacy; but I give my
judgment, as one who hath obtained mercy of the Lord to be faithful,\"
namely, in the apostleship. He acted faithfully, and therefore his
direction was to be regarded as a rule of Christ: for he gave judgment
as one who was a faithful apostle of Christ. Though Christ had before
delivered no universal law about that matter, he now gives direction by
an inspired apostle, one who had obtained mercy of the Lord to be
faithful. Note, Faithfulness in the ministry is owing to the grace and
mercy of Christ. It is what Paul was ready to acknowledge upon all
occasions: I laboured more abundantly than they all; yet not I, but the
grace of God which was with me, ch. 15:10. And it is a great mercy which
those obtain from God who prove faithful in the ministry of his word,
either ordinary or extraordinary.

`II.` The determination he gives, which, considering the present
distress, was that a state of celibacy was preferable: It is good for a
man so to be, that is, to be single. I suppose, says the apostle, or it
is my opinion. It is worded with modesty, but delivered,
notwithstanding, with apostolic authority. It is not the mere opinion of
a private man, but the very determination of the Spirit of God in an
apostle, though it be thus spoken. And it was thus delivered to give it
the more weight. Those that were prejudiced against the apostle might
have rejected this advice had it been given with a mere authoritative
air. Note, Ministers do not lose their authority by prudent
condescensions. They must become all things to all men, that they may do
them the more good. This is good, says he, for the present distress.
Christians, at the first planting of their religion, were grievously
persecuted. Their enemies were very bitter against them, and treated
them very cruelly. They were continually liable to be tossed and hurried
by persecution. This being the then state of things, he did not think it
so advisable for Christians that were single to change conditions. The
married state would bring more care and cumber along with it (v. 33,
34), and would therefore make persecution more terrible, and render them
less able to bear it. Note, Christians, in regulating their conduct,
should not barely consider what is lawful in itself, but what may be
expedient for them.

`III.` Notwithstanding he thus determines, he is very careful to satisfy
them that he does not condemn marriage in the gross, nor declare it
unlawful. And therefore, though he says, \"If thou art loosed from a
wife (in a single state, whether bachelor or widower, virgin or widow)
do not seek a wife, do not hastily change conditions;\" yet he adds,
\"If thou art bound to a wife, do not seek to be loosed. It is thy duty
to continue in the married relation, and do the duties of it.\" And
though such, if they were called to suffer persecution, would find
peculiar difficulties in it; yet, to avoid these difficulties, they must
not cast off nor break through the bonds of duty. Duty must be done, and
God trusted with events. But to neglect duty is the way to put ourselves
out of the divine protection. He adds therefore, If thou marry thou hast
not sinned; or if a virgin marry she hath not sinned: but such shall
have trouble in the flesh. Marrying is not in itself a sin, but marrying
at that time was likely to bring inconvenience upon them, and add to the
calamities of the times; and therefore he thought it advisable and
expedient that such as could contain should refrain from it; but adds
that he would not lay celibacy on them as a yoke, nor, by seeming to
urge it too far, draw them into any snare; and therefore says, But I
spare you. Note, How opposite in this are the papist casuists to the
apostle Paul! They forbid many to marry, and entangle them with vows of
celibacy, whether they can bear the yoke or no.

`IV.` He takes this occasion to give general rules to all Christians to
carry themselves with a holy indifferency towards the world, and
everything in it. 1. As to relations: Those that had wives must be as
though they had none; that is, they must not set their hearts too much
on the comforts of the relation; they must be as though they had none.
They know not how soon they shall have none. This advice must be carried
into every other relation. Those that have children should be as though
they had none. Those that are their comfort now may prove their greatest
cross. And soon may the flower of all comforts be cut down. 2. As to
afflictions: Those that weep must be as though they wept not; that is,
we must not be dejected too much with any of our afflictions, nor
indulge ourselves in the sorrow of the world, but keep up a holy joy in
God in the midst of all our troubles, so that even in sorrow the heart
may be joyful, and the end of our grief may be gladness. Weeping may
endure for a night, but joy will come in the morning. If we can but get
to heaven at last, all tears shall be wiped from our eyes; and the
prospect of it now should make us moderate our sorrows and refrain our
tears. 3. As to worldly enjoyments: Those that rejoice should be as
though they rejoiced not; that is, they should not take too great a
complacency in any of their comforts. They must be moderate in their
mirth, and sit loose to the enjoyments they most value. Here is not
their rest, nor are these things their portion; and therefore their
hearts should not be set on them, nor should they place their solace or
satisfaction in them. 4. As to worldly traffic and employment: Those
that buy must be as though they possessed not. Those that prosper in
trade, increase in wealth, and purchase estates, should hold these
possessions as though they held them not. It is but setting their hearts
on that which is not (Prov. 23:5) to do otherwise. Buying and possessing
should not too much engage our minds. They hinder many people altogether
from minding the better part. Purchasing land and trying oxen kept the
guests invited from the wedding-supper, Lu. 14:18, 19. And, when they do
not altogether hinder men from minding their chief business, they do
very much divert them from a close pursuit. Those are most likely to run
so as to obtain the prize who ease their minds of all foreign cares and
cumbrances. 5. As to all worldly concerns: Those that use this world as
not abusing it, v. 31. The world may be used, but must not be abused. It
is abused when it is not used to those purposes for which it is given,
to honour God and do good to men-when, instead of being oil to the
wheels of our obedience, it is made fuel to lust-when, instead of being
a servant, it is made our master, our idol, and has that room in our
affections which should be reserved for God. And there is great danger
of abusing it in all these respects, if our hearts are too much set upon
it. We must keep the world as much as may be out of our hearts, that we
may not abuse it when we have it in our hands.

`V.` He enforces these advices with two reasons:-1. The time is short, v.
29. We have but little time to continue in this world; but a short
season for possessing and enjoying worldly things; kairos synestalmenos.
It is contracted, reduced to a narrow compass. It will soon be gone. It
is just ready to be wrapped up in eternity. Therefore do not set your
hearts on worldly enjoyments. Do not be overwhelmed with worldly cares
and troubles. Possess what you must shortly leave without suffering
yourselves to be possessed by it. Why should your hearts be much set on
what you must quickly resign? 2. The fashion of this world passeth away
(v. 31), scheµma-the habit, figure, appearance, of the world, passeth
away. It is daily changing countenance. It is in a continual flux. It is
not so much a world as the appearance of one. All is show, nothing solid
in it; and it is transient show too, and will quickly be gone. How
proper and powerful an argument is this to enforce the former advice!
How irrational is it to be affected with the images, the fading and
transient images, of a dream! Surely man walketh in a vain show (Ps.
39:6), in an image, amidst the faint and vanishing appearances of
things. And should he be deeply affected, or grievously afflicted, with
such a scene?

`VI.` He presses his general advice by warning them against the
embarrassment of worldly cares: But I would have you without
carefulness, v. 32. Indeed to be careless is a fault; a wise concern
about worldly interests is a duty; but to be careful, full of care, to
have an anxious and perplexing care about them, is a sin. All that care
which disquiets the mind, and distracts it in the worship of God, is
evil; for God must be attended upon without distraction, v. 35. The
whole mind should be engaged when God is worshipped. The work ceases
while it diverts to any thing else, or is hurried and drawn hither and
thither by foreign affairs and concerns. Those who are engaged in divine
worship should attend to this very thing, should make it their whole
business. But how is this possible when the mind is swallowed up of the
cares of this life? Note, It is the wisdom of a Christian so to order
his outward affairs, and choose such a condition in life, as to be
without distracting cares, that he may attend upon the Lord with a mind
at leisure and disengaged. This is the general maxim by which the
apostle would have Christians govern themselves. In the application of
it Christian prudence must direct. That condition of life is best for
every man which is best for his soul, and keeps him most clear of the
cares and snares of the world. By this maxim the apostle solves the case
put to him by the Corinthians, whether it were advisable to marry? To
this he says, That, by reason of the present distress, and it may be in
general, at that time, when Christians were married to infidels, and
perhaps under a necessity of being so, if married at all: I say, in
these circumstances, to continue unmarried would be the way to free
themselves from any cares and incumbrances, and allow them more vacation
for the service of God. Ordinarily, the less care we have about the
world the more freedom we have for the service of God. Now the married
state at that time (if not at all times) did bring most worldly care
along with it. He that is married careth for the things of the world,
that he may please his wife, v. 33. And she that is married careth for
the things of the world, how she may please her husband. But the
unmarried man and woman mind the things of the Lord, that they may
please the Lord, and be holy both in body and spirit, v. 32, 34. Not but
the married person may be holy both in body and spirit too. Celibacy is
not in itself a state of greater purity and sanctity than marriage; but
the unmarried would be able to make religion more their business at that
juncture, because they would have less distraction from worldly cares.
Marriage is that condition of life that brings care along with it,
though sometimes it brings more than at others. It is the constant care
of those in that relation to please each other; though this is more
difficult to do at some reasons, and in some cases, than in others. At
that season, therefore, the apostle advises that those who were single
should abstain from marriage, if they were under no necessity to change
conditions. And, where the same reason is plain at other times, the rule
is as fit to be observed. And the very same rule must determine persons
for marriage where there is the same reason, that is, if in the
unmarried state persons are likely to be more distracted in the service
of God than if they were married, which is a case supposable in many
respects. This is the general rule, which every one\'s discretion must
apply to his own particular case; and by it should he endeavour to
determine, whether it be for marriage or against. That condition of life
should be chosen by the Christian in which it is most likely he will
have the best helps, and the fewest hindrances, in the service of God
and the affairs of his own salvation.

### Verses 36-38

In this passage the apostle is commonly supposed to give advice about
the disposal of children in marriage, upon the principle of his former
determination. In this view the general meaning is plain. It was in that
age, and those parts of the world, and especially among the Jews,
reckoned a disgrace for a woman to remain unmarried past a certain
number of years: it gave a suspicion of somewhat that was not for her
reputation. \"Now,\" says the apostle, \"if any man thinks he behaves
unhandsomely towards his daughter, and that it is not for her credit to
remain unmarried, when she is of full age, and that upon this principle
it is needful to dispose of her in marriage, he may use his pleasure. It
is no sin in him to dispose of her to a suitable mate. But if a man has
determined in himself to keep her a virgin, and stands to this
determination, and is under no necessity to dispose of her in marriage,
but is at liberty, with her consent, to pursue his purpose, he does well
in keeping her a virgin. In short, he that gives her in marriage does
well; but he that keeps her single, if she can be easy and innocent in
such a state, does what is better; that is, more convenient for her in
the present state of things, if not at all times and seasons.\" Note, 1.
Children should be at the disposal of their parents, and not dispose of
themselves in marriage. Yet, 2. Parents should consult their children\'s
inclinations, both to marriage in general and to the person in
particular, and not reckon they have uncontrollable power to do with
them, and dictate to them, as they please. 3. It is our duty not only to
consider what is lawful, but in many cases, at least, what is fit to be
done, before we do it.

But I think the apostle is here continuing his former discourse, and
advising unmarried persons, who are at their own disposal, what to do,
the man\'s virgin being meant of his virginity. Teµrein teµn heautou
parthenon seems to be rather meant of preserving his own virginity than
keeping his daughter a virgin, though it be altogether uncommon to use
the word in this sense. Several other reasons may be seen in Locke and
Whitby, by those who will consult them. And it was a common matter of
reproach among Jews and civilized heathens, for a man to continue single
beyond such a term of years, though all did not agree in limiting the
single life to the same term. The general meaning of the apostle is the
same, that it was no sin to marry, if a man thought there was a
necessity upon, to avoid popular reproach, much less to avoid the
hurrying fervours of lust. But he that was in his own power, stood firm
in his purpose, and found himself under no necessity to marry, would, at
that season, and in the circumstances of Christians at that time, at
least, make a choice every way most for his own conveniency, ease, and
advantage, as to his spiritual concerns. And it is highly expedient, if
not a duty, for Christians to be guided by such a consideration.

### Verses 39-40

The whole is here closed up with advice to widows: As long as the
husband liveth the wife is bound by the law, confined to one husband,
and bound to continue and cohabit with him. Note, The marriage-contract
is for life; death only can annul the bond. But, the husband being dead,
she is at liberty to marry whom she will. There is no limitation by
God\'s law to be married only for such a number of times. It is certain,
from this passage, that second marriages are not unlawful; for then the
widow could not be at liberty to marry whom she pleased, nor to marry a
second time at all. But the apostle asserts she has such a liberty, when
her husband is dead, only with a limitation that she marry in the Lord.
In our choice of relations, and change of conditions, we should always
have an eye to God. Note, Marriages are likely to have God\'s blessing
only when they are made in the Lord, when persons are guided by the fear
of God, and the laws of God, and act in dependence on the providence of
God, in the change and choice of a mate-when they can look up to God,
and sincerely seek his direction, and humbly hope for his blessing upon
their conduct. But she is happier, says the apostle, if she so abide
(that is, continue a widow) in my judgment; and I think I have the
Spirit of God, v. 40. At this juncture, at least, if not ordinarily, it
will be much more for the peace and quiet of such, and give them less
hindrance in the service of God, to continue unmarried. And this, he
tells them, was by inspiration of the Spirit. \"Whatever your false
apostles may think of me, I think, and have reason to know, that I have
the Spirit of God.\" Note, Change of condition in marriage is so
important a matter that it ought not to be made but upon due
deliberation, after careful consideration of circumstances, and upon
very probable grounds, at least, that it will be a change to advantage
in our spiritual concerns.
