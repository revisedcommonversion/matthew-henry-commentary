Exodus, Chapter 26
==================

Commentary
----------

Moses here receives instructions, `I.` Concerning the inner curtains of
the tent or tabernacle, and the coupling of those curtains (v. 1-6). `II.`
Concerning the outer curtains which were of goats\' hair, to strengthen
the former (v. 7-13). `III.` Concerning the case or cover which was to
secure it from the weather (v. 14). `IV.` Concerning the boards which were
to be reared up to support the curtains, with their bars and sockets (v.
15-30). `V.` The partition between the holy place and the most holy (v.
31-35). `VI.` The veil for the door (v. 36, 37). These particulars, thus
largely recorded, seem of little use to us now; yet, having been of
great use to Moses and Israel, and God having thought fit to preserve
down to us the remembrance of them, we ought not to overlook them. Even
the antiquity renders this account venerable.

### Verses 1-6

`I.` The house must be a tabernacle or tent, such as soldiers now use in
the camp, which was both a mean dwelling and a movable one; and yet the
ark of God had not better, till Solomon built the temple 480 years after
this, 1 Ki. 6:1. God manifested his presence among them thus in a
tabernacle, 1. In compliance with their present condition in the
wilderness, that they might have him with them wherever they went. Note,
God suits the tokens of his favour, and the gifts of his grace, to his
people\'s wants and necessities, according as they are, accommodating
his mercy to their state, prosperous or adverse, settled or unsettled.
When thou passest through the waters, I will be with thee, Isa. 43:2. 2.
That it might represent the state of God\'s church in this world, it is
a tabernacle-state, Ps. 15:1. We have here no continuing city; being
strangers in this world, and travellers towards a better, we shall never
be fixed till we come to heaven. Church-privileges are movable goods,
from one place to another; the gospel is not tied to any place; the
candlestick is in a tent, and may easily be taken away, Rev. 2:5. If we
make much of the tabernacle, and improve the privilege of it, wherever
we go it will accompany us; but, if we neglect and disgrace it, wherever
we stay it will forsake us. What hath my beloved to do in my house? Jer.
11:15.

`II.` The curtains of the tabernacle must correspond to a divine pattern.
`1.` They were to be very rich, the best of the kind, fine twined linen;
and colours very pleasing, blue, and purple, and scarlet. 2. They were
to be embroidered with cherubim (v. 1), to intimate that the angels of
God pitch their tents round about the church, Ps. 34:7. As there were
cherubim over the mercy-seat, so there were round the tabernacle; for we
find the angels compassing, not only the throne, but the elders; see
Rev. 5:11. 3. There were to be two hangings, five breadths in each,
sewed together, and the two hangings coupled together with golden
clasps, or tacks, so that it might be all one tabernacle, v. 6. Thus the
churches of Christ and the saints, though they are many, are yet one,
being fitly joined together in holy love, and by the unity of the
Spirit, so growing into one holy temple in the Lord, Eph. 2:21, 22;
4:16. This tabernacle was very straight and narrow; but, at the
preaching of the gospel, the church is bidden to enlarge the place of
her tent, and to stretch forth her curtains, Isa. 54:2.

### Verses 7-14

Moses is here ordered to make a double covering for the tabernacle, that
it might not rain in, and that the beauty of those fine curtains might
not be damaged. 1. There was to be a covering of hair camlet curtains,
which were somewhat larger every way than the inner curtains, because
they were to enclose them, and probably were stretched out at some
little distance from them, v. 7, etc. These were coupled together with
brass clasps. The stuff being less valuable, the tacks were so; but the
brass tacks would answer the intention as effectually as the golden
ones. The bonds of unity may be as strong between curtains of goats\'
hair as between those of purple and scarlet. 2. Over this there was to
be another covering, and that a double one (v. 14), one of rams\' skins
dyed red, probably dressed with the wool on; another of badgers\' skins,
so we translate it, but it should rather seem to have been some strong
sort of leather (but very fine), for we read of the best sort of shoes
being made of it, Eze. 16:10. Now observe here, `(1.)` That the outside of
the tabernacle was coarse and rough, the beauty of it was in the inner
curtains. Those in whom God dwells must labour to be better than they
seem to be. Hypocrites put the best side outwards, like whited
sepulchres; but the king\'s daughter is all glorious within (Ps. 45:13);
in the eye of the world black as the tents of Kedar, but, in the eye of
God, comely as the curtains of Solomon, Cant. 1:5. Let our adorning be
that of the hidden man of the heart, which God values, 1 Pt. 3:4. `(2.)`
That where God places his glory he will create a defence upon it; even
upon the habitations of the righteous there shall be a covert, Isa. 6:5,
6. The protection of Providence shall always be upon the beauty of
holiness. God\'s tent will be a pavilion, Ps. 27:5.

### Verses 15-30

Very particular directions are here given about the boards of the
tabernacle, which were to bear up the curtains, as the stakes of a tent
which had need to be strong, Isa. 54:2. These boards had tenons which
fell into the mortises that were made for them in silver bases. God took
care to have every thing strong, as well as fine, in his tabernacle.
Curtains without boards would have been shaken by every wind; but it is
a good thing to have the heart established with grace, which is as the
boards to support the curtains of profession, which otherwise will not
hold out long. The boards were coupled together with gold rings at top
and bottom (v. 24), and kept firm with bars that ran through golden
staples in every board (v. 26), and the boards and bars were all richly
gilded, v. 29. Thus every thing in the tabernacle was very splendid,
agreeable to that infant state of the church, when such things were
proper enough to please children, to possess the minds of the
worshippers with a reverence of the divine glory, and to affect them
with the greatness of that prince who said, Here will I dwell; in
allusion to this the new Jerusalem is said to be of pure gold, Rev.
21:18. But the builders of the gospel church said, Silver and gold have
we none; and yet the glory of their building far exceeded that of the
tabernacle, 2 Co. 3:10, 11. How much better is wisdom than gold! No
orders are given here about the floor of the tabernacle; probably that
also was boarded; for we cannot think that within all these fine
curtains they trod upon the cold or wet ground; if it was so left, it
may remind us of ch. 20:24, An altar of earth shalt thou make unto me.

### Verses 31-37

Two veils are here ordered to be made, 1. One for a partition between
the holy place and the most holy, which not only forbade any to enter,
but forbade them so much as to look into the holiest of all, v. 31, 33.
Under that dispensation, divine grace was veiled, but now we behold it
with open face, 2 Co. 3:18. The apostle tells us (Heb. 9:8, 9) what was
the meaning of this veil; it intimated that the ceremonial law could not
make the comers thereunto perfect, nor would the observance of it bring
men to heaven; the way into the holiest of all was not made manifest
while the first tabernacle was standing; life and immortality lay
concealed till they were brought to light by the gospel, which was
therefore signified by the rending of this veil at the death of Christ,
Mt. 27:51. We have not boldness to enter into the holiest, in all acts
of devotion, by the blood of Jesus, yet such as obliges us to a holy
reverence and a humble sense of our distance. 2. Another veil was for
the outer door of the tabernacle, v. 36, 37. Through this first veil the
priests went in every day to minister in the holy place, but not the
people, Heb. 9:6. This veil, which was all the defence the tabernacle
had against thieves and robbers, might easily be broken through, for it
could be neither locked nor barred, and the abundance of wealth in the
tabernacle, one would think, might be a temptation; but by leaving it
thus exposed, `(1.)` The priests and Levites would be so much the more
obliged to keep a strict watch upon it, and, `(2.)` God would show his
care of his church on earth, though it is weak and defenceless, and
continually exposed. A curtain shall be (if God please to make it so) as
strong a defence to his house as gates of brass and bars of iron.
