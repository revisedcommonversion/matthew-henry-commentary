Introduction to Exodus
======================

Moses (the servant of the Lord in writing for him as well as in acting
for him-with the pen of God as well as with the rod of God in his hand)
having, in the first book of his history, preserved and transmitted the
records of the church, while it existed in private families, comes, in
this second book, to give us an account of its growth into a great
nation; and, as the former furnishes us with the best economics, so this
with the best politics. The beginning of the former book shows us how
God formed the world for himself; the beginning of this shows us how he
formed Israel for himself, and both show forth his praise, Isa. 43:21.
There we have the creation of the world in history, here the redemption
of the world in type. The Greek translators called this book Exodus
(which signifies a departure or going out) because it begins with the
story of the going out of the children of Israel from Egypt. Some allude
to the names of this and the foregoing book, and observe that
immediately after Genesis, which signifies the beginning or original,
follows Exodus, which signifies a departure; for a time to be born is
immediately succeeded by a time to die. No sooner have we made our
entrance into the world than we must think of making our exit, and going
out of the world. When we begin to live we begin to die. The forming of
Israel into a people was a new creation. As the earth was, in the
beginning, first fetched from under water, and then beautified and
replenished, so Israel was first by an almighty power made to emerge out
of Egyptian slavery, and then enriched with God\'s law and tabernacle.
This book gives us, `I.` The accomplishment of the promises made before to
Abraham (ch. 1-19), and then, `II.` The establishment of the ordinances
which were afterwards observed by Israel (ch. 20-40). Moses, in this
book, begins, like Caesar, to write his own Commentaries; nay, a
greater, a far greater, than Caesar is here. But henceforward the penman
is himself the hero, and gives us the history of those things of which
he was himself an eye and ear-witness, et quorum pars magna fuit-and in
which he bore a conspicuous part. There are more types of Christ in this
book than perhaps in any other book of the Old Testament; for Moses
wrote of him, Jn. 5:46. The way of man\'s reconciliation to God, and
coming into covenant and communion with him by a Mediator, is here
variously represented; and it is of great use to us for the illustration
of the New Testament, now that we have that to assist us in the
explication of the Old.
