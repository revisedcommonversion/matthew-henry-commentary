Exodus, Chapter 35
==================

Commentary
----------

What should have been said and done upon Moses\' coming down the first
time from the mount, if the golden calf had not broken the measures and
put all into disorder, now at last, when with great difficulty
reconciliation was made, begins to be said and done; and that great
affair of the setting up of God\'s worship is put into its former
channel again, and goes on now without interruption. `I.` Moses gives
Israel those instructions, received from God, which required immediate
observance. 1. Concerning the sabbath (v. 1-3). 2. Concerning the
contribution that was to be made for the erecting of the tabernacle (v.
4-9). 3. Concerning the framing of the tabernacle and the utensils of it
(v. 10-19). `II.` The people bring in their contributions (v. 20-29). `III.`
The head-workmen are nominated (v. 30, etc.).

### Verses 1-19

It was said in general (ch. 34:32), Moses gave them in commandment all
that the Lord has spoken with him. But, the erecting and furnishing of
the tabernacle being the work to which they were now immediately to
apply themselves, there is particular mention of the orders given
concerning it.

`I.` All the congregation is summoned to attend (v. 1); that is, the heads
and rulers of the congregation, the representatives of the several
tribes, who must receive instructions from Moses as he had received them
from the Lord, and must communicate them to the people. Thus John, being
commanded to write to the seven churches what had been revealed to him,
writes it to the angels, or ministers, of the churches.

`II.` Moses gave them in charge all that (and that only) which God had
commanded him; thus he approved himself faithful both to God and Israel,
between whom he was a messenger or mediator. If he had added, altered,
or diminished, he would have been false to both. But, both sides having
reposed a trust in him, he was true to the trust; yet he was faithful as
a servant only, but Christ as a Son, Heb. 3:5, 6.

`III.` He begins with the law of the sabbath, because that was much
insisted on in the instructions he had received (v. 21, 3): Six days
shall work be done, work for the tabernacle, the work of the day that
was now to be done in its day; and they had little else to do here in
the wilderness, where they had neither husbandry nor merchandise,
neither food to get nor clothes to make: but on the seventh day you must
not strike a stroke, no, not at the tabernacle-work; the honour of the
sabbath was above that of the sanctuary, more ancient and more lasting;
that must be to you a holy day, devoted to God, and not be spent in
common business. It is a sabbath of rest. It is a sabbath of sabbaths
(so some read it), more honourable and excellent than any of the other
feasts, and should survive them all. A sabbath of sabbatism, so others
read it, being typical of that sabbatism or rest, both spiritual and
eternal, which remains for the people of God, Heb. 4:9. It is a sabbath
of rest, that is, in which a rest from all worldly labour must be very
carefully and strictly observed. It is a sabbath and a little sabbath,
so some of the Jews would have it read; not only observing the whole day
as a sabbath, but an hour before the beginning of it, and an hour after
the ending of it, which they throw in over and above out of their own
time, and call a little sabbath, to show how glad they are of the
approach of the sabbath and how loth to part with it. It is a sabbath of
rest, but it is rest to the Lord, to whose honour it must be devoted. A
penalty is here annexed to the breach of it: Whosoever doeth work
therein shall be put to death. Also a particular prohibition of kindling
fires on the sabbath day for any servile work, as smith\'s work, or
plumbers, etc.

`IV.` He orders preparation to be made for the setting up of the
tabernacle. Two things were to be done:-

`1.` All that were able must contribute: Take you from among you an
offering, v. 5. The tabernacle was to be dedicated to the honour of God,
and used in his service; and therefore what was brought for the setting
up and furnishing of that was an offering to the Lord. Our goodness
extends not to God, but what is laid out for the support of his kingdom
and interest among men he is pleased to accept as an offering to
himself; and he requires such acknowledgements of our receiving our all
from him and such instances of our dedicating our all to him. The rule
is, Whosoever is of a willing heart let him bring. It was not to be a
tax imposed upon them, but a benevolence or voluntary contribution, to
intimate to us, `(1.)` That God has not made our yoke heavy. He is a
prince that does not burden his subjects with taxes, nor make them to
serve with an offering, but draws with the cords of a man, and leaves it
to ourselves to judge what is right; his is a government that there is
no cause to complain of, for he does not rule with rigour. `(2.)` That God
loves a cheerful giver, and is best pleased with the free-will offering.
Those services are acceptable to him that come from the willing heart of
a willing people, Ps. 110:3.

`2.` All that were skilful must work: Every wise-hearted among you shall
come, and make, v. 10. See how God dispenses his gifts variously; and,
as every man hath received the gift, so he must minister, 1 Pt. 4:10.
Those that were rich must bring in materials to work on; those that were
ingenious must serve the tabernacle with their ingenuity; as they needed
one another, so the tabernacle needed them both, 1 Co. 12:7-21. The work
was likely to go on when some helped with their purses, others with
their hands, and both with a willing heart. Moses, as he had told them
what must be given (v. 5-9), so he gives them the general heads of what
must be made (v. 11-19), that, seeing how much work was before them,
they might apply themselves to it the more vigorously, and every hand
might be busy; and it gave them such an idea of the fabric designed that
they could not but long to see it finished.

### Verses 20-29

Moses having made known to them the will of God, they went home and
immediately put in practice what they had heard, v. 20. O that every
congregation would thus depart from the hearing of the word of God, with
a full resolution to be doers of the same! Observe here,

`I.` The offerings that were brought for the service of the tabernacle (v.
21, etc.), concerning which many things may be noted. 1. It is intimated
that they brought their offerings immediately; they departed to their
tents immediately to fetch their offering, and did not desire time to
consider of it, lest their zeal should be cooled by delays. What duty
God convinces us of, and calls us to, we should set about speedily. No
season will be more convenient than the present season. 2. It is said
that their spirits made them willing (v. 21), and their hearts, v. 29.
What they did they did cheerfully, and from a good principle. They were
willing, and it was not any external inducement that made them so, but
their spirits. It was from a principle of love to God and his service, a
desire of his presence with them in his ordinances, gratitude for the
great things he had done for them, faith in his promise of what he would
further do (or, at least, from the present consideration of these
things), that they were willing to offer. What we give and do for God is
then acceptable when it comes from a good principle in the heart and
spirit. 3. When it is said that as many as were willing-hearted brought
their offerings (v. 22), it should seem as if there were some who were
not, who loved their gold better than their God, and would not part with
it, no, not for the service of the tabernacle. Such there are, who will
be called Israelites, and yet will not be moved by the equity of the
thing, God\'s expectations from them, and the good examples of those
about them, to part with any thing for the interests of God\'s kingdom:
they are for the true religion, provided it be cheap and will cost them
nothing. 4. The offerings were of divers kinds, according as they had;
those that had gold and precious stones brought them, not thinking any
thing too good and too rich to part with for the honour of God. Those
that had not precious stones to bring brought goats\' hair, and rams\'
skins. If we cannot do as much as others for God, we must not therefore
sit still and do nothing: if the meaner offerings which are according to
our ability gain us not such a reputation among men, yet they shall not
fail of acceptance with God, who requires according to what a man hath,
and not according to what he hath not, 2 Co. 8:12; 2 Ki. 5:23. Two mites
from a pauper were more pleasing than so many talents from a Dives. God
has an eye to the heart of the giver more than to the value of the gift.
5. Many of the things they offered were their ornaments, bracelets and
rings, and tablets or lockets (v. 22); and even the women parted with
these. Can a maid forget her ornaments? Thus far they forgot them that
they preferred the beautifying of the sanctuary before their own
adorning. Let this teach us, in general, to part with that for God, when
he calls for it, which is very dear to us, which we value, and value
ourselves by; and particularly to lay aside our ornaments, and deny
ourselves in them, when either they occasion offence to others or feed
our own pride. If we think those gospel rules concerning our clothing
too strict (1 Tim. 2:9, 10; 1 Pt. 3:3, 4), I fear we should scarcely
have done as these Israelites did. If they thought their ornaments well
bestowed upon the tabernacle, shall not we think the want of ornaments
well made up by the graces of the Spirit? Prov. 1:9. 6. These rich
things that they offered, we may suppose, were mostly the spoils of the
Egyptians; for the Israelites in Egypt were kept poor, till they
borrowed at parting. And we may suppose the rulers had better things (v.
27), because, having more influence among the Egyptians, they borrowed
larger sums. Who would have thought that ever the wealth of Egypt should
have been so well employed? but thus God has often made the earth to
help the woman, Rev. 12:16. It was by a special providence and promise
of God that the Israelites got all that spoil, and therefore it was
highly fit that they should devote a part of it to the service of that
God to whom they owed it all. Let every man give according as God hath
prospered him, 1 Co. 16:2. Extraordinary successes should be
acknowledged by extraordinary offerings. Apply it to human learning,
arts and sciences, which are borrowed, as it were, from the Egyptians.
Those that are enriched with these must devote them to the service of
God and his tabernacle: they may be used as helps to understand the
scriptures, as ornaments or handmaids to divinity. But then great care
must be taken that Egypt\'s gods mingle not with Egypt\'s gold. Moses,
though learned in all the learning of the Egyptians, did not therefore
pretend, in the least instance, to correct the pattern shown him in the
mount. The furnishing of the tabernacle with the riches of Egypt was
perhaps a good omen to the Gentiles, who, in the fulness of time, should
be brought into the gospel tabernacle, and their silver and their gold
with them (Isa. 60:9), and it should be said, Blessed be Egypt my
people, Isa. 19:25. 7. We may suppose that the remembrance of the
offerings made for the golden calf made them the more forward in these
offerings. Those that had then parted with their ear-rings would not
testify their repentance by giving the rest of their jewels to the
service of God: godly sorrow worketh such a revenge, 2 Co. 7:11. And
those that had kept themselves pure from that idolatry yet argued with
themselves, \"Were they so forward in contributing to an idol, and shall
we be backward or sneaking in our offerings to the Lord?\" Thus some
good was brought even out of that evil.

`II.` The work that was done for the service of the tabernacle (v. 25):
The women did spin with their hands. Some spun fine work, of blue and
purple; others coarse work, of goats\' hair, and yet theirs also is said
to be done in wisdom, v. 26. As it is not only rich gifts, so it is not
only fine work that God accepts. Notice is here taken of the good
women\'s work for God, as well as of Bezaleel\'s and Aholiab\'s. The
meanest hand for the honour of God, shall have an honourable recompence.
Mary\'s anointing of Christ\'s head shall be told for a memorial (Mt.
26:13); and a record is kept of the women that laboured in the gospel
tabernacle (Phil. 4:3), and were helpers to Paul in Christ Jesus, Rom.
16:3. It is part of the character of the virtuous woman that she layeth
her hands to the spindle, Prov. 31:19. This employment was here turned
to a pious use, as it may be still (though we have no hangings to make
for the tabernacle) by the imitation of the charity of Dorcas, who made
coats and garments for poor widows, Acts 9:39. Even those that are not
in a capacity to give in charity may yet work in charity; and thus the
poor may relieve the poor, and those that have nothing but their limbs
and senses may be very charitable in the labour of love.

### Verses 30-35

Here is the divine appointment of the master-workmen, that there might
be no strife for the office, and that all who were employed in the work
might take direction from, and give account to, these general
inspectors; for God is the God of order and not of confusion. Observe,
`1.` Those whom God called by name to this service he filled with the
Spirit of God, to qualify them for it, v. 30, 31. Skill in secular
employments is God\'s gift, and comes from above, Jam. 1:17. From him
the faculty is, and the improvement of it. To his honour therefore all
knowledge must be devoted, and we must study how to serve him with it.
The work was extraordinary which Bezaleel was designed for, and
therefore he was qualified in an extraordinary manner for it; thus when
the apostles were appointed to be master-builders in setting up the
gospel tabernacle they were filled with the Spirit of God in wisdom and
understanding. 2. The were appointed, not only to devise, but to work
(v. 32), to work all manner of work, v. 35. Those of eminent gifts, that
are capable of directing others, must not thing that these will excuse
them in idleness. Many are ingenious enough in cutting out work for
other people, and can tell what this man and that man should do, but the
burdens they ind on others they themselves will not touch with one of
their fingers. These will fall under the character of slothful servants.
3. They were not only to devise and work themselves, but they were to
teach others, v. 34. Not only had Bezaleel power to command, but he was
to take pains to instruct. Those that rule should teach; and those to
whom God had given knowledge should be willing to communicate it for the
benefit of others, not coveting to monopolize it.
