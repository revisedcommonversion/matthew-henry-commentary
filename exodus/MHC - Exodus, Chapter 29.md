Exodus, Chapter 29
==================

Commentary
----------

Particular orders are given in this chapter, `I.` Concerning the
consecration of the priests, and the sanctification of the altar (v.
1-37). `II.` Concerning the daily sacrifice (v. 38-41). To which gracious
promises are annexed that God would own and bless them in all their
services (v. 42, etc.).

### Verses 1-37

Here is, `I.` The law concerning the consecration of Aaron and his sons to
the priest\'s office, which was to be done with a great deal of ceremony
and solemnity, that they themselves might be duly affected with the
greatness of the work to which they were called, and that the people
also might learn to magnify the office and none might dare to invade it.

`1.` The ceremonies wherewith it was to be done were very fully and
particularly appointed, because nothing of this kind had been done
before, and because it was to be a statute for ever that the high priest
should be thus inaugurated. Now,

`(1.)` The work to be done was the consecrating of the persons whom God
had chosen to be priests, by which they devoted and gave up themselves
to the service of God and God declared his acceptance of them; and the
people were made to know that they glorified not themselves to be made
priests, but were called of God, Heb. 5:4, 5. They were thus
distinguished from common men, sequestered from common services, and set
apart for God and an immediate attendance on him. Note, All that are to
be employed for God are to be sanctified to him. The person must first
be accepted, and then the performance. The Hebrew phrase for
consecrating is filling the hand (v. 9): Thou shalt fill the hand of
Aaron and his sons, and the ram of consecration is the ram of fillings,
v. 22, 26. The consecrating of them was the perfecting of them; Christ
is said to be perfect or consecrated for evermore, Heb. 7:28. Probably
the phrase here is borrowed from the putting of the sacrifice into their
hand, to be waved before the Lord, v. 24. But it intimates, `[1.]` That
ministers have their hands full; they have no time to trifle, so great,
so copious, so constant is their work. `[2.]` That they must have their
hands filled. Of necessity they must have something to offer, and they
cannot find it in themselves, it must be given them from above. They
cannot fill the people\'s hearts unless God fill their hands; to him
therefore they must go, and receive from his fulness.

`(2.)` The person to do it was Moses, by God\'s appointment. Though he was
ordained for men, yet the people were not to consecrate him; Moses the
servant of the Lord, and his agent herein, must do it. By God\'s special
appointment he now did the priest\'s work, and therefore that which was
the priest\'s part of the sacrifice was here ordered to be his, v. 26.

`(3.)` The place was at the door of the tabernacle of meeting, v. 4. God
was pleased to dwell in the tabernacle, the people attending in the
courts, so that the door between the court and the tabernacle was the
fittest place for those to be consecrated in who were to mediate between
God and man, and to stand between both, and lay their hands (as it were)
upon both. They were consecrated at the door, for they were to be
door-keepers.

`(4.)` It was done with many ceremonies.

`[1.]` They were to be washed (v. 4), signifying that those must be
clean who bear the vessels of the Lord, Isa. 52:11. Those that would
perfect holiness must cleanse themselves from all filthiness of flesh
and spirit, 2 Co. 7:1; Isa. 1:16-18. They were now washed all over; but
afterwards, when they went in to minister, they washed only their hands
and feet (ch. 30:19); for he that is washed needs no more, Jn. 13:10.

`[2.]` They were to be clothed with the holy garments (v. 5, 6, 8, 9),
to signify that it was not sufficient for them to put away the
pollutions of sin, but they must put on the graces of the Spirit, be
clothed with righteousness, Ps. 132:9. They must be girded, as men
prepared and strengthened for their work; and they must be robed and
crowned, as men that counted their work and office their true honour.

`[3.]` The high priest was to be anointed with the holy anointing oil
(v. 7), that the church might be filled and delighted with the sweet
savour of his administrations (for ointment and perfume rejoice the
heart), and in token of the pouring out of the Spirit upon him, to
qualify him for his work. Brotherly love is compared to this oil with
which Aaron was anointed, Ps. 133:2. The inferior priests are said to be
anointed (ch. 30:30), not on their heads, as the high priest (Lev.
21:10), the oil was only mingled with the blood that was sprinkled upon
their garments.

`[4.]` Sacrifices were to be offered for them. The covenant of
priesthood, as all other covenants, must be made by sacrifice.

First, There must be a sin-offering, to make atonement for them, v.
10-14. The law made those priests that had infirmity, and therefore they
must first offer for their own sin, before they could make atonement for
the people, Heb. 7:27, 28. They were to put their hand on the head of
their sacrifice (v. 10), confessing that they deserved to die for their
own sin, and desiring that the killing of the beast might expiate their
guilt, and be accepted as a vicarious satisfaction. It was used as other
sin-offerings were; only, whereas the flesh of other sin-offerings was
eaten by the priests (Lev. 10:18), in token of the priest\'s taking away
the sin of the people, this was appointed to be all burnt without the
camp (v. 14), to signify the imperfection of the legal dispensation (as
the learned bishop Patrick notes); for the sins of the priests
themselves could not be taken away by those sacrifices, but they must
expect a better high priest and a better sacrifice.

Secondly, There must be a burnt-offering, a ram wholly burnt, to the
honour of God, in token of the dedication of themselves wholly to God
and to his service, as living sacrifices, kindled with the fire and
ascending in the flame of holy love, v. 15-18. The sin-offering must
first be offered and then the burnt-offering; for, till guilt be
removed, no acceptable service can be performed, Isa. 6:7.

Thirdly, There must be a peace-offering; it is called the ram of
consecration, because there was more in this peculiar to the occasion
than in the other two. In the burnt-offering God had the glory of their
priesthood, in this they had the comfort of it; and, in token of a
mutual covenant between God and them, 1. The blood of the sacrifice was
divided between God and them (v. 20, 21); part of the blood was
sprinkled upon the altar round about, and part put upon them, upon their
bodies (v. 20), and upon their garments, v. 21. Thus the benefit of the
expiation made by the sacrifice was applied and assured to them, and
their whole selves from head to foot sanctified to the service of God.
The blood was put upon the extreme parts of the body, to signify that it
was all, as it were, enclosed and taken in for God, the tip of the ear
and the great toe not excepted. We reckon that the blood and oil
sprinkled upon garments spot and stain them; yet the holy oil, and the
blood of the sacrifice, sprinkled upon their garments, must be looked
upon as the greatest adorning imaginable to them, for they signified the
blood of Christ, and the graces of the Spirit, which constitute and
complete the beauty of holiness, and recommend us to God; we read of
robes made white with the blood of the Lamb. 2. The flesh of the
sacrifice, with the meat-offering annexed to it, was likewise divided
between God and them, that (to speak with reverence) God and they might
feast together, in token of friendship and fellowship. `(1.)` Part of it
was to be first waved before the Lord, and then burnt upon the altar;
part of the flesh (v. 22), part of the bread, for bread and flesh must
go together (v. 23); these were first put into the hands of Aaron to be
waved to and fro, in token of their being offered to God (who, though
unseen, yet compasses us round on every side), and then they were to be
burnt upon the altar (v. 24, 25), for the altar was to devour God\'s
part of the sacrifice. Thus God admitted Aaron and his sons to be his
servants, and wait at his table, taking the mat of his altar from their
hands. Here, in a parenthesis, as it were, comes in the law concerning
the priests\' part of the peace-offerings afterwards, the breast and
shoulder, which were now divided; Moses had the breast, and the shoulder
was burnt on the altar with God\'s part, v. 26-28. `(2.)` The other part,
both of the flesh of the ram and of the bread, Aaron and his sons were
to eat at the door of the tabernacle (v. 31-33), to signify that he
called them not only servants but friends, Jn. 15:15. He supped with
them, and they with him. Their eating of the things wherewith the
atonement was made signified their receiving the atonement, as the
expression is (Rom. 5:11), their thankful acceptance of the benefit of
it, and their joyful communion with God thereupon, which was the true
intent and meaning of a feast upon a sacrifice. If any of it was left,
it must be burnt, that it might not be in any danger of putrefying, and
to show that it was an extraordinary peace-offering.

`2.` The time that was to be spent in this consecration: Seven days shalt
thou consecrate them, v. 35. Though all the ceremonies were performed on
the first day, yet, `(1.)` They were not to look upon their consecration
as completed till the seven days\' end, which put a solemnity upon their
admission, and a distance between this and their former state, and
obliged them to enter upon their work with a pause, giving them time to
consider the weight and seriousness of it. This was to be observed in
after-ages, v. 30. He that was to succeed Aaron in the high-priesthood
must put on the holy garments seven days together, in token of a
deliberate and gradual advance into his office, and that one sabbath
might pass over him in his consecration. `(2.)` Every day of the seven, in
this first consecration, a bullock was to be offered for a sin-offering
(v. 36), which was to intimate to them, `[1.]` That it was of very great
concern to them to get their sins pardoned, and that though atonement
was made, and they had the comfort of it, yet they must still keep up a
penitent sense of sin and often repeat the confession of it. `[2.]` That
those sacrifices which were thus offered day by day to make atonement
could not make the comers thereunto perfect, for then they would have
ceased to be offered, as the apostle argues, Heb. 10:1, 2. They must
therefore expect the bringing in of a better hope.

`3.` This consecration of the priests was a shadow of good things to
come. `(1.)` Our Lord Jesus is the great high-priest of our profession,
called of God to be so, consecrated for evermore, anointed with the
Spirit above his fellows (whence he is called Messiah, the Christ),
clothed with the holy garments, even with glory and beauty, sanctified
by his own blood, not that of bullocks and rams (Heb. 9:12), made
perfect, or consecrated, through sufferings, Heb. 2:10. Thus in him this
was a perpetual statute, v. 9. `(2.)` All believers are spiritual priests,
to offer spiritual sacrifices (1 Pt. 2:5), washed in the blood of
Christ, and so made to our God priests, Rev. 1:5, 6. They also are
clothed with the beauty of holiness, and have received the anointing, 1
Jn. 2:27. Their hands are filled with work, to which they must
continually attend; and it is through Christ, the great sacrifice, that
they are dedicated to this service. His blood sprinkled upon the
conscience purges it from dead works, that they may, as priests, serve
the living God. The Spirit of God (as Ainsworth notes) is called the
finger of God (Lu. 11:20, compared with Mt. 12:28), and by him the merit
of Christ is effectually applied to our souls, as here Moses with his
finger was to put the blood upon Aaron. It is likewise intimated that
gospel ministers are to be solemnly set apart to the work of the
ministry with great deliberation and seriousness both in the ordainers
and in the ordained, as those that are to be employed in a great work
and entrusted with a great charge.

`II.` The consecration of the altar, which seems to have been coincident
with that of the priests, and the sin-offerings which were offered every
day for seven days together had reference to the altar as well as the
priests, v. 36, 37. An atonement was made for the altar. Though that was
not a subject capable of sin, nor, having never yet been used, could it
be said to be polluted with the sins of the people, yet, since the fall,
there can be no sanctification to God but there must first be an
atonement for sin, which renders us both unworthy and unfit to be
employed for God. The altar was also sanctified, not only set apart
itself to a sacred use, but made so holy as to sanctify the gifts that
were offered upon it, Mt. 23:19. Christ is our altar; for our sakes he
sanctified himself, that we and our performances might be sanctified and
recommended to God, Jn. 17:19.

### Verses 38-46

In this paragraph we have,

`I.` The daily service appointed. A lamb was to be offered upon the altar
every morning, and a lamb every evening, each with a meat-offering, both
made by fire, as a continual burnt-offering throughout their
generations, v. 38-41. Whether there were any other sacrifices to be
offered or not, these were sure to be offered, at the public charge, for
the benefit and comfort of all Israel, to make atonement for their daily
sins, and to be an acknowledgement to God of their daily mercies. This
was that which the duty of every day required. The taking away of this
daily sacrifice by Antiochus, for so many evenings and mornings, was
that great calamity of the church which was foretold, Dan. 8:11. Note,
`1.` This typified the continual intercession which Christ ever lives to
make, in virtue of his satisfaction, for the continual sanctification of
his church: though he offered himself once for all, yet that one
offering thus becomes a continual offering. 2. This teaches us to offer
up to God the spiritual sacrifices of prayer and praise every day,
morning and evening, in humble acknowledgement of our dependence upon
him and our obligations to him. Our daily devotions must be looked upon
as the most needful of our daily works and the most pleasant of our
daily comforts. Whatever business we have, this must never be omitted,
either morning or evening; prayer-time must be kept up as duly as
meat-time. The daily sacrifices were as the daily meals in God\'s house,
and therefore they were always attended with bread and wine. Those
starve their own souls that keep not up a constant attendance on the
throne of grace.

`II.` Great and precious promises made of God\'s favour to Israel, and
the tokens of his special presence with them, while they thus kept up
his institutions among them. He speaks as one well pleased with the
appointment of the daily sacrifice; for, before he proceeds to the other
appointments that follow, he interposes these promises. It is constancy
in religion that brings in the comfort of it. He promises, 1. That he
would keep up communion with them; that he would not only meet Moses,
and speak to him, but that he would meet the children of Israel, (v.
43), to accept the daily sacrifices offered up on their behalf. Note,
God will not fail to give those the meeting who diligently and
conscientiously attend upon him in the ordinances of his own
appointment. 2. That he would own his own institutions, the tabernacle,
the altar, the priesthood (v. 43, 44); he would take possession of that
which was consecrated to him. Note, What is sanctified to the glory of
God shall be sanctified by his glory. If we do our part, God will do
his, and will mark and fit that for himself which is in sincerity given
up to him. 3. That he would reside among them as God in covenant with
them, and would give them sure and comfortable tokens of his peculiar
favour to them, and his special presence with them (v. 45, 45): I will
dwell among the children of Israel. Note, Where God sets up the
tabernacle of his ordinances he will himself dwell. Lo, I am with you
always, Mt. 28:20. Those that abide in God\'s house shall have God to
abide with them. I will be their God, and they shall know that I am so.
Note, Those are truly happy that have a covenant-interest in God as
theirs and the comfortable evidence of that interest. If we have this,
we have enough, and need no more to make us happy.
