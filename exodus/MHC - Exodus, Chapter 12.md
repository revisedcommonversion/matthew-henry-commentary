Exodus, Chapter 12
==================

Commentary
----------

This chapter gives an account of one of the most memorable ordinances,
and one of the most memorable providences, of all that are recorded in
the Old Testament. `I.` Not one of all the ordinances of the Jewish church
was more eminent than that of the passover, nor is any one more
frequently mentioned in the New Testament; and we have here an account
of the institution to it. The ordinance consisted of three parts:-1. The
killing and eating of the paschal lamb (v. 1-6, 8-11). 2. The sprinkling
of the blood upon the door-posts, spoken of as a distinct thing (Heb.
11:28), and peculiar to this first passover (v. 7), with the reason for
it (v. 13). 3. The feast of unleavened bread for seven days following;
this points rather at what was to be done afterwards, in the observance
of this ordinance (v. 14-20). This institution is communicated to the
people, and they are instructed in the observance, `(1.)` Of this first
passover (v. 21-23). `(2.)` Of the after passovers (v. 24-27). And the
Israelites\' obedience to these orders (v. 28). `II.` Not one of all the
providences of God concerning the Jewish church was more illustrious, or
is more frequently mentioned, than the deliverance of the children of
Israel out of Egypt. 1. The firstborn of the Egyptians are slain (v. 29,
30). 2. Orders are given immediately for their discharge (v. 31-33). 3.
They begin their march. `(1.)` Loaded with their own effects (v. 34). `(2.)`
Enriched with the spoils of Egypt (v. 35, 36). `(3.)` Attended with a
mixed multitude (v. 37, 38). `(4.)` Put to their shifts for present supply
(v. 39). The event is dated (v. 40-42). Lastly, A recapitulation in the
close, `[1.]` Of this memorable ordinance, with some additions (v.
43-49). `[2.]` Of this memorable providence (v. 50, 51).

### Verses 1-20

Moses and Aaron here receive of the Lord what they were afterwards to
deliver to the people concerning the ordinance of the passover, to which
is prefixed an order for a new style to be observed in their months (v.
1, 2): This shall be to you the beginning of months. They had hitherto
begun their year from the middle of September, but henceforward they
were to begin it from the middle of March, at least in all their
ecclesiastical computations. Note, It is good to begin the day, and
begin the year, and especially to begin our lives, with God. This new
calculation began the year with the spring, which reneweth the face of
the earth, and was used as a figure of the coming of Christ, Cant. 2:11,
12. We may suppose that, while Moses was bringing the ten plagues upon
the Egyptians, he was directing the Israelites to prepare for their
departure at an hour\'s warning. Probably he had be degrees brought them
near together from their dispersions, for their are here called the
congregation of Israel (v. 3), and to them as a congregation orders are
here sent. Their amazement and hurry, it is easy to suppose, were great;
yet now they must apply themselves to the observance of a sacred rite,
to the honour of God. Note, When our heads are fullest of care, and our
hands of business, yet we must not forget our religion, nor suffer
ourselves to be indisposed for acts of devotion.

`I.` God appointed that on the night wherein they were to go out of Egypt
they should, in each of their families, kill a lamb, or that two or
three families, if they were small, should join for a lamb. The lamb was
to be got ready four days before and that afternoon they were to kill it
(v. 6) as a sacrifice; not strictly, for it was not offered upon the
altar, but as a religious ceremony, acknowledging God\'s goodness to
them, not only in preserving them from, but in delivering them by, the
plagues inflicted on the Egyptians. See the antiquity of
family-religion; and see the convenience of the joining of small
families together for religious worship, that it may be made the more
solemn.

`II.` The lamb so slain they were to eat, roasted (we may suppose, in its
several quarters), with unleavened bread and bitter herbs, because they
were to eat it in haste (v. 11), and to leave none of it until the
morning; for God would have them to depend upon him for their daily
bread, and not to take thought for the morrow. He that led them would
feed them.

`III.` Before they ate the flesh of the lamb, they were to sprinkle the
blood upon the doorposts, v. 7. By this their houses were to be
distinguished from the houses of the Egyptians, and so their first-born
secured from the sword of the destroying angel, v. 12, 13. Dreadful work
was to be made this night in Egypt; all the first-born both of man and
beast were to be slain, and judgment executed upon the gods of Egypt.
Moses does not mention the fulfillment, in this chapter, yet he speaks
of it Num. 33:4. It is very probable that the idols which the Egyptians
worshipped were destroyed, those of metal melted, those of wood
consumed, and those of stone broken to pieces, whence Jethro infers (ch.
18:11), The Lord is greater than all gods. The same angel that destroyed
their first-born demolished their idols, which were no less dear to
them. For the protection of Israel from this plague they were ordered to
sprinkle the blood of the lamb upon the door-posts, their doing which
would be accepted as an instance of their faith in the divine warnings
and their obedience to the divine precepts. Note, 1. If in times of
common calamity God will secure his own people, and set a mark upon
them; they shall be hidden either in heaven or under heaven, preserved
either from the stroke of judgments or at least from the sting of them.
2. The blood of sprinkling is the saint\'s security in times of common
calamity; it is this that marks them for God, pacifies conscience, and
gives them boldness of access to the throne of grace, and so becomes a
wall of protection round them and a wall of partition between them and
the children of this world.

`IV.` This was to be annually observed as a feast of the Lord in their
generations, to which the feast of unleavened bread was annexed, during
which, for seven days, they were to eat no bread but what was
unleavened, in remembrance of their being confined to such bread, of
necessity, for many days after they came out of Egypt, v. 14-20. The
appointment is inculcated for their better direction, and that they
might not mistake concerning it, and to awaken those who perhaps in
Egypt had grown generally very stupid and careless in the matters of
religion to a diligent observance of the institution. Now, without
doubt, there was much of the gospel in this ordinance; it is often
referred to in the New Testament, and, in it, to us is the gospel
preached, and not to them only, who could not stedfastly look to the end
of these things, Heb. 4:2; 2 Co. 3:13.

`1.` The paschal lamb was typical. Christ is our Passover, 1 Co. 5:7.
`(1.)` It was to be a lamb; and Christ is the Lamb of God (Jn. 1:29),
often in the Revelation called the Lamb, meek and innocent as a lamb,
dumb before the shearers, before the butchers. `(2.)` It was to be a male
of the first year (v. 5), in its prime; Christ offered up himself in the
midst of his days, not in infancy with the babes of Bethlehem. It
denotes the strength and sufficiency of the Lord Jesus, on whom our help
was laid. `(3.)` It was to be without blemish (v. 5), denoting the purity
of the Lord Jesus, a Lamb without spot, 1 Pt. 1:19. The judge that
condemned him (as if his trial were only like the scrutiny that was made
concerning the sacrifices, whether they were without blemish or no)
pronounced him innocent. `(4.)` It was to be set apart four days before
(v. 3, 6), denoting the designation of the Lord Jesus to be a Saviour,
both in the purpose and in the promise. It is very observable that as
Christ was crucified at the passover, so he solemnly entered into
Jerusalem four days before, the very day that the paschal lamb was set
apart. `(5.)` It was to be slain, and roasted with fire (v. 6-9), denoting
the exquisite sufferings of the Lord Jesus, even unto death, the death
of the cross. The wrath of God is as fire, and Christ was made a curse
for us. `(6.)` It was to be killed by the whole congregation between the
two evenings, that is, between three o\'clock and six. Christ suffered
in the end of the world (Heb. 9:26), by the hand of the Jews, the whole
multitude of them (Lu. 23:18), and for the good of all his spiritual
Israel. `(7.)` Not a bone of it must be broken (v. 46), which is expressly
said to be fulfilled in Christ (Jn. 19:33, 36), denoting the unbroken
strength of the Lord Jesus.

`2.` The sprinkling of the blood was typical. `(1.)` It was not enough that
the blood of the lamb was shed, but it must be sprinkled, denoting the
application of the merits of Christ\'s death to our souls; we must
receive the atonement, Rom. 5:11. `(2.)` It was to be sprinkled with a
bunch of hyssop (v. 22) dipped in the basin. The everlasting covenant,
like the basin, in the conservatory of this blood, the benefits and
privileges purchased by it are laid up for us there; faith is the bunch
of hyssop by which we apply the promises to ourselves and the benefits
of the blood of Christ laid up in them. `(3.)` It was to be sprinkled upon
the door-posts, denoting the open profession we are to make of faith in
Christ, and obedience to him, as those that are not ashamed to own our
dependence upon him. The mark of the beast may be received on the
forehead or in the right hand, but the seal of the Lamb is always in the
forehead, Rev. 7:3. There is a back-way to hell, but no back-way to
heaven; no, the only way to this is a high-way, Isa. 35:8. `(4.)` It was
to be sprinkled upon the lintel and the sideposts, but not upon the
threshold (v. 7), which cautions us to take heed of trampling under foot
the blood of the covenant, Heb. 10:29. It is precious blood, and must be
precious to us. `(5.)` The blood, thus sprinkled, was a means of the
preservation of the Israelites from the destroying angel, who had
nothing to do where the blood was. If the blood of Christ be sprinkled
upon our consciences, it will be our protection from the wrath of God,
the curse of the law, and the damnation of hell, Rom. 8:1.

`3.` The solemnly eating of the lamb was typical of our gospel-duty to
Christ. `(1.)` The paschal lamb was killed, not to be looked upon only,
but to be fed upon; so we must by faith make Christ ours, as we do that
which we eat, and we must receive spiritual strength and nourishment
from him, as from our food, and have delight and satisfaction in him, as
we have in eating and drinking when we are hungry or thirsty: see Jn.
6:53-55. `(2.)` It was to be all eaten; those that by faith feed upon
Christ must feed upon a whole Christ; they must take Christ and his
yoke, Christ and his cross, as well as Christ and his crown. Is Christ
divided? Those hat gather much of Christ will have nothing over. `(3.)` It
was to be eaten immediately, not deferred till morning, v. 10. To-day
Christ is offered, and is to be accepted while it is called to-day,
before we sleep the sleep of death. `(4.)` It was to be eaten with bitter
herbs (v. 8), in remembrance of the bitterness of their bondage in
Egypt. We must feed upon Christ with sorrow and brokenness of heart, in
remembrance of sin; this will give an admirable relish to the paschal
lamb. Christ will be sweet to us if sin be bitter. `(5.)` It was to be
eaten in a departing posture (v. 11); when we feed upon Christ by faith
we must absolutely forsake the rule and dominion of sin, shake off
Pharaoh\'s yoke; and we must sit loose to the world, and every thing in
it, forsake all for Christ, and reckon it no bad bargain, Heb. 13:13,
14.

`4.` The feast of unleavened bread was typical of the Christian life, 1
Co. 5:7, 8. Having received Christ Jesus the Lord, `(1.)` We must keep a
feast in holy joy, continually delighting ourselves in Christ Jesus; no
manner of work must be done (v. 16), no care admitted or indulged,
inconsistent with, or prejudicial to, this holy joy: if true believers
have not a continual feast, it is their own fault. `(2.)` It must be a
feast of unleavened bread, kept in charity, without the leaven of
malice, and in sincerity, without the leaven of hypocrisy. The law was
very strict as to the passover, and the Jews were so in their usages,
that no leaven should be found in their houses, v. 19. All the old
leaven of sin must be put far from us, with the utmost caution and
abhorrence, if we would keep the feast of a holy life to the honour of
Christ. `(3.)` It was by an ordinance for ever (v. 17); as long as we
live, we must continue feeding upon Christ and rejoicing in him, always
making thankful mention of the great things he has done for us.

### Verses 21-28

`I.` Moses is here, as a faithful steward in God\'s house, teaching the
children of Israel to observe all things which God had commanded him;
and no doubt he gave the instructions as largely as he received them,
though they are not so largely recorded. It is here added,

`1.` That this night, when the first-born were to be destroyed, no
Israelite must stir out of doors till morning, that is, till towards
morning, when they would be called to march out of Egypt, v. 22. Not but
that the destroying angel could have known an Israelite from an Egyptian
in the street; but God would intimate to them that their safety was
owing to the blood of sprinkling; if they put themselves from under the
protection of that, it was at their peril. Those whom God has marked for
himself must not mingle with evil doers: see Isa. 26:20, 21. They must
not go out of the doors, lest they should straggle and be out of the way
when they should be summoned to depart: they must stay within, to wait
for the salvation of the Lord, and it is good to do so.

`2.` That hereafter they should carefully teach their children the
meaning of this service, v. 26, 27. Observe,

`(1.)` The question which the children would ask concerning this solemnity
(which they would soon take notice of in the family): \"What mean you by
this service? What is he meaning of all this care and exactness about
eating this lamb, and this unleavened bread, more than about common
food? Why such a difference between this meal and other meals?\" Note,
`[1.]` It is a good thing to see children inquisitive about the things
of God; it is to be hoped that those who are careful to ask for the way
will find it. Christ himself, when a child, heard and asked questions,
Lu. 2:46. `[2.]` It concerns us all rightly to understand the meaning of
those holy ordinances wherein we worship God, what is the nature and
what the end of them, what is signified and what intended, what is the
duty expected from us in them and what are the advantages to be expected
by us. Every ordinance has a meaning; some ordinances, as sacraments,
have not their meaning so plain and obvious as others have; therefore we
are concerned to search, that we may not offer the blind for sacrifice,
but may do a reasonable service. If either we are ignorant of, or
mistake about, the meaning of holy ordinances, we can neither please God
nor profit ourselves.

`(2.)` The answer which the parents were to return to this question (v.
27): You shall say, It is the sacrifice of the Lord\'s passover, that
is, \"By the killing and sacrificing of this lamb, we keep in
remembrance the work of wonder and grace which God did for our fathers,
when,\" `[1.]` \"To make way for our deliverance out of bondage, he slew
the firstborn of the Egyptians, so compelling them to sign our
discharge;\" and, `[2.]` \"Though there were with us, even with us, sins
against the Lord our God, for which the destroying angel, when he was
abroad doing execution, might justly have destroyed our first-born too,
yet God graciously appointed and accepted the family-sacrifice of a
lamb, instead of the first-born, as, of old, the ram instead of Isaac,
and in every house where the lamb was slain the first-born were saved.\"
The repetition of this solemnity in the return of every year was
designed, First, To look backward as a memorial, that in it they might
remember what great things God had done for them and their fathers. The
word pesach signifies a leap, or transition; it is a passing over; for
the destroying angel passed over the houses of the Israelites, and did
not destroy their first-born. When God brings utter ruin upon his people
he says, I will not pass by them any more (Amos 7:8; 8:2), intimating
how often he had passed by them, as now when the destroying angel passed
over their houses. Note, 1. Distinguishing mercies lay under peculiar
obligations. When a thousand fall at our side, and ten thousand at our
right hand, and yet we are preserved, and have our lives given us for a
prey, this should greatly affect us, Ps. 91:7. In war or pestilence, if
the arrow of death have passed by us, passed over us, hit the next to us
and just missed us, we must not say it was by chance that we were
preserved but by the special providence of our God. 2. Old mercies to
ourselves, or to our fathers, must not be forgotten, but be had in
everlasting remembrance, that God may be praised, our faith in him
encouraged, and our hearts enlarged in his service. Secondly, It was
designed to look forward as an earnest of the great sacrifice of the
Lamb of God in the fulness of time, instead of us and our first-born. We
were obnoxious to the sword of the destroying angel, but Christ our
passover was sacrificed for us, his death was our life, and thus he was
the Lamb slain from the foundation of the world, from the foundation of
the Jewish church: Moses kept the passover by faith in Christ, for
Christ was the end of the law for righteousness.

`II.` The people received these instructions with reverence and ready
obedience. 1. They bowed the head and worshipped (v. 27): they hereby
signified their submission to this institution as a law, and their
thankfulness for it as a favour and privilege. Note, When God gives law
to us, we must give honour to him; when he speaks, we must bow our heads
and worship. 2. They went away and did as they were commanded, v. 23.
Here was none of that discontent and murmuring among them which we read
of, ch. 5:20, 21. The plagues of Egypt had done them good, and raised
their expectations of a glorious deliverance, which before they
despaired of; and now they went forth to meet it in the way appointed.
Note, The perfecting of God\'s mercies to us must be waited for in a
humble observance of his institutions.

### Verses 29-36

Here we have, `I.` The Egyptians\' sons, even their first-born, slain, v.
29, 30. If Pharaoh would have taken the warning which was given him of
this plague, and would thereupon have released Israel, what a great many
dear and valuable lives might have been preserved! But see what
obstinate infidelity brings upon men. Observe, 1. The time when this
blow was given: It was at midnight, which added to the terror of it. The
three preceding nights were made dreadful by the additional plague of
darkness, which might be felt, and doubtless disturbed their repose; and
now, when they hoped for one quiet night\'s rest, at midnight was the
alarm given. When the destroying angel drew his sword against Jerusalem,
it was in the day-time (2 Sa. 24:15), which made it the less frightful;
but the destruction of Egypt was by a pestilence walking in darkness,
Ps. 91:6. Shortly there will be an alarming cry at midnight, Behold, the
bridegroom cometh. 2. On whom the plague fastened-on their first-born,
the joy and hope of their respective families. They had slain the
Hebrews\' children, and now God slew theirs. Thus he visits the iniquity
of the fathers upon the children; and he is not unrighteous who taketh
vengeance. 3. How far it reached-from the throne to the dungeon. Prince
and peasant stand upon the same level before God\'s judgments, for there
is no respect of persons with him; see Job 34:29, 20. Now the slain of
the Lord were many; multitudes, multitudes, fall in this valley of
decision, when the controversy between God and Pharaoh was to be
determined. 4. What an outcry was made upon it: There was a great cry in
Egypt, universal lamentation for their only son (with many), and with
all for their first-born. If any be suddenly taken ill in the night, we
are wont to call up neighbours; but the Egyptians could have no help, no
comfort, from their neighbours, all being involved in the same calamity.
Let us learn hence, `(1.)` To tremble before God, and to be afraid of his
judgments, Ps. 119:120. Who is able to stand before him, or dares resist
him? `(2.)` To be thankful to God for the daily preservation of ourselves
and our families: lying so much exposed, we have reason to say, \"It is
of the Lord\'s mercies that we are not consumed.\"

`II.` God\'s sons, even his first-born, released; this judgment conquered
Pharaoh, and obliged him to surrender at discretion, without
capitulating. Men had better come up to God\'s terms at first, for he
will never come down to theirs, let them object as long as they will.
Now Pharaoh\'s pride is abased, and he yields to all that Moses had
insisted on: Serve the Lord as you have said (v. 31), and take your
flocks as you have said, v. 32. Note, God\'s word will stand, and we
shall get nothing by disputing it, or delaying to submit to it. Hitherto
the Israelites were not permitted to depart, but now things had come to
the last extremity, in consequence of which, 1. They are commanded to
depart: Rise up, and get you forth, v. 31. Pharaoh had told Moses he
should see his face no more; but now he sent for him. Those will seek
God early in their distress who before had set him at defiance. Such a
fright he was now in that he gave orders by night for their discharge,
fearing lest, if he delayed any longer, he himself should fall next; and
that he sent them out, not as men hated (as the pagan historians have
represented this matter), but as men feared, is plainly discovered by
his humble request to them (v. 32): \"Bless me also; let me have your
prayers, that I may not be plagued for what is past, when you are
gone.\" Note, Those that are enemies to God\'s church are enemies to
themselves, and, sooner or later, they will be made to see it. 2. They
are hired to depart by the Egyptians; they cried out (v. 33), We be all
dead men. Note, When death comes into our houses, it is seasonable for
us to think of our own mortality. Are our relations dead? It is easy to
infer thence that we are dying, and, in effect, already dead men. Upon
this consideration they were urgent with the Israelites to be gone,
which gave great advantage to the Israelites in borrowing their jewels,
v. 35, 36. When the Egyptians urged them to be gone, it was easy for the
to say that the Egyptians had kept them poor, that they could not
undertake such a journey with empty purses, but, that, if they would
give them wherewithal to bear their charges, they would be gone. And
this the divine Providence designed in suffering things to come to this
extremity, that they, becoming formidable to the Egyptians, might have
what they would, for asking; the Lord also, by the influence he has on
the minds of people, inclined the hearts of the Egyptians to furnish
them with what they desired, they probably intending thereby to make
atonement, that the plagues might be stayed, as the Philistines, when
they returned the ark, sent a present with it for a trespass-offering,
having an eye to this precedent, 1 Sa. 6:3, 6. The Israelites might
receive and keep what they thus borrowed, or rather required, of the
Egyptians, `(1.)` As justly as servants receive wages from their masters
for work done, and sue for it if it be detained. `(2.)` As justly as
conquerors take the spoils of their enemies whom they have subdued;
Pharaoh was in rebellion against the God of the Hebrews, by which all
that he had was forfeited. `(3.)` As justly as subjects receive the
estates granted to them by their prince. God is the sovereign proprietor
of the earth, and the fulness thereof; and, if he take from one and give
to another, who may say unto him, What doest thou? It was by God\'s
special order and appointment that the Israelites did what they did,
which was sufficient to justify them, and bear them out; but what they
did will by no means authorize others (who cannot pretend to any such
warrant) to do the same. Let us remember, `[1.]` That the King of kings
can do no wrong. `[2.]` That he will do right to those whom men injure,
Ps. 146:7. Hence it is that the wealth of the sinner often proves to be
laid up for the just, Prov. 13:22; Job 27:16, 17.

### Verses 37-42

Here is the departure of the children of Israel out of Egypt; having
obtained their dismission, they set forward without delay, and did not
defer to a more convenient season. Pharaoh was now in a good mind; but
they had reason to think he would not long continue so, and therefore it
was no time to linger. We have here an account, 1. Of their number,
about 600,000 men (v. 37), besides women and children, which I think, we
cannot suppose to make less than 1,200,000 more. What a vast increase
was this, to arise from seventy souls in little more than 200 years\'
time! See the power and efficacy of that blessing, when God commands it,
Be fruitful and multiply. This was typical of the multitudes that were
brought into the gospel church when it was first founded; so mightily
grew the word of God, and prevailed. 2. Of their retinue (v. 38): A
mixed multitude went up with them, hangers on to that great family, some
perhaps willing to leave their country, because it was laid waste by the
plagues, and to seek their fortune, as we say, with the Israelites;
others went out of curiosity, to see the solemnities of Israel\'s
sacrifice to their God, which had been so much talked of, and expecting
to see some glorious appearances of their God to them in the wilderness,
having seen such glorious appearances of their God for them in the field
of Zoan, Ps. 78:12. Probably the greatest part of this mixed multitude
were but a rude unthinking mob, that followed the crowd they knew not
why; we afterwards find that they proved a snare to them (Num. 11:4),
and it is probable that when, soon afterwards, they understood that the
children of Israel were to continue forty years in the wilderness, they
quitted them, and returned to Egypt. Note, There were always those among
the Israelites that were not Israelites, and there are still hypocrites
in the church, who make a deal of mischief, but will be shaken off at
last. 3. Of their effects. They had with them flocks and herds, even
very much cattle. This is taken notice of because it was long before
Pharaoh would give them leave to remove their effects, which were
chiefly cattle, Gen. 46:32. 4. Of the provision made for the camp, which
was very poor and slender. They brought some dough with them out of
Egypt in their knapsacks, v. 34. They had prepared to bake, the next
day, in order to their removal, understanding it was very near; but,
being hastened away sooner than they thought of, by some hours, they
took the dough as it was, unleavened; when they came to Succoth, their
first stage, they baked unleavened cakes, and, though these were of
course insipid, yet the liberty they were brought into made this the
most joyful meal they had ever eaten in their lives. Note, The servants
of God must not be slaves to their appetites, nor solicitous to wind up
all the delights of sense to their highest pitch. We should be willing
to take up with dry bread, nay, with unleavened bread, rather than
neglect or delay any service we have to do for God, as those whose meat
and drink it is to do his will. 5. Of the date of this great event: it
was just 430 years from the promise made to Abraham (as the apostle
explains it, Gal. 3:17) at his first coming into Canaan, during all
which time the children of Israel, that is, the Hebrews, the
distinguished chosen seed, were sojourners in a land that was not
theirs, either Canaan or Egypt. So long the promise God made to Abraham
of a settlement lay dormant and unfulfilled, but now, at length, it
revived, and things began to work towards the accomplishment of it. The
first day of the march of Abraham\'s seed towards Canaan was just 430
years (it should seem to a day) from the promise made to Abraham, Gen.
12:2, I will make of thee a great nation. See how punctual God is to his
time; though his promises be not performed quickly, they will be
accomplished in their season. 6. Of the memorableness of it: It is a
night to be much observed, v. 42. `(1.)` The providences of that first
night were very observable; memorable was the destruction of the
Egyptians, and the deliverance of the Israelites by it; God herein made
himself taken notice of. `(2.)` The ordinances of that night, in the
annual return of it, were to be carefully observed: This is that night
of the Lord, that remarkable night, to be celebrated in all generations.
Note, The great things God does for his people are not to be a nine
days\' wonder, as we say, but the remembrance of them is to be
perpetuated throughout all ages, especially the work of our redemption
by Christ. This first passover-night was a night of the Lord much to be
observed; but the last passover-night, in which Christ was betrayed (and
in which the passover, with the rest of the ceremonial institutions, was
superseded and abolished), was a night of the Lord much more to be
observed, when a yoke heavier than that of Egypt was broken from off our
necks, and a land better than that of Canaan set before us. That was a
temporal deliverance to be celebrated in their generation; this is an
eternal redemption to be celebrated in the praises of glorious saints,
world without end.

### Verses 43-51

Some further precepts are here given concerning the passover, as it
should be observed in times to come.

`I.` All the congregation of Israel must keep it, v. 47. All that share in
God\'s mercies should join in thankful praises for them. Though it was
observed in families apart, yet it is looked upon as the act of the
whole congregation; for the smaller communities constituted the greater.
The New-Testament passover, the Lord\'s supper, ought not to be
neglected by any who are capable of celebrating it. He is unworthy the
name of an Israelite that can contentedly neglect the commemoration of
so great a deliverance. 1. No stranger that was uncircumcised might be
admitted to eat of it, v. 43, 45, 48. None might sit at the table but
those that came in by the door; nor may any now approach to the
improving ordinance of the Lord\'s supper who have not first submitted
to the initiating ordinance of baptism. We must be born again by the
word ere we can be nourished by it. Nor shall any partake of the benefit
of Christ\'s sacrifice, or feast upon it, who are not first circumcised
in heart, Col. 2:11. 2. Any stranger that was circumcised might be
welcome to eat of the passover, even servants, v. 44. If, by
circumcision, they would make themselves debtors to the law in its
burdens, they were welcome to share in the joy of its solemn feasts, and
not otherwise. Only it is intimated (v. 48) that those who were masters
of families must not only be circumcised themselves, but have all their
males circumcised too. If in sincerity, and with that zeal which the
thing required and deserves, we give up ourselves to God, we shall, with
ourselves, give up all we have to him, and do our utmost that all ours
may be his too. Here is an early indication of favour to the poor
Gentiles, that the stranger, if circumcised, stands upon the same level
with the home-born Israelite. One law for both, v. 49. This was a
mortification to the Jews, and taught them that it was their dedication
to God, not their descent from Abraham, that entitled them to their
privileges. A sincere proselyte was as welcome to the passover as a
native Israelite, Isa. 56:6, 7.

`II.` In one house shall it be eaten (v. 46), for good-fellowship sake,
that they might rejoice together, and edify one another in the eating of
it. None of it must be carried to another place, nor left to another
time; for God would not have them so taken up with care about their
departure as to be indisposed to take the comfort of it, but to leave
Egypt, and enter upon a wilderness, with cheerfulness, and, in token of
that, to eat a good hearty meal. The papists\' carrying their
consecrated host from house to house is not only superstitious in
itself, but contrary to this typical law of the passover, which directed
that no part of the lamb should be carried abroad.

The chapter concludes with a repetition of the whole matter, that the
children of Israel did as they were bidden, and God did for them as he
promised (v. 50, 51); for he will certainly be the author of salvation
to those that obey him.
