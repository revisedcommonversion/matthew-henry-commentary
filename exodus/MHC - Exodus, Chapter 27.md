Exodus, Chapter 27
==================

Commentary
----------

In this chapter directions are given, `I.` Concerning the brazen altar for
burnt-offerings (v. 1-8). `II.` Concerning the court of the tabernacle,
with the hangings of it (v. 9-19). `III.` Concerning oil for the lamp (v.
20, 21).

### Verses 1-8

As God intended in the tabernacle to manifest his presence among his
people, so there they were to pay their devotions to him, not in the
tabernacle itself (into that only the priests entered as God\'s domestic
servants), but in the court before the tabernacle, where, as common
subjects, they attended. There an altar was ordered to be set up, to
which they must bring their sacrifices, and on which their priests must
offer them to God: and this altar was to sanctify their gifts. Here they
were to present their services to God, as from the mercy-seat he gave
his oracles to them; and thus a communion was settled between God and
Israel. Moses is here directed about, 1. The dimensions of it; it was
square, v. 1. 2. The horns of it (v. 2), which were for ornament and for
use; the sacrifices were bound with cords to the horns of the altar, and
to them malefactors fled for refuge. 3. The materials; it was of wood
overlaid with brass, v. 1, 2. 4. The appurtenances of it (v. 3), which
were all of brass. 5. The grate, which was let into the hollow of the
altar, about the middle of it, in which the fire was kept, and the
sacrifice burnt; it was made of network like a sieve, and hung hollow,
that the fire might burn the better, and that the ashes might fall
through into the hollow of the altar, v. 4, 5. 6. The staves with which
it must be carried, v. 6, 7. And, lastly, he is referred to the pattern
shown him, v. 8.

Now this brazen altar was a type of Christ dying to make atonement for
our sins: the wood would have been consumed by the fire from heaven if
it had not been secured by the brass; nor could the human nature of
Christ have borne the wrath of God if it had not been supported by a
divine power. Christ sanctified himself for his church, as their altar
(Jn. 17:19), and by his mediation sanctifies the daily services of his
people, who have also a right to eat of this altar (Heb. 13:10), for
they serve at it as spiritual priests. To the horns of this altar poor
sinners fly for refuge when justice pursues them, and they are safe in
virtue of the sacrifice there offered.

### Verses 9-19

Before the tabernacle there was to be a court or yard, enclosed with
hangings of the finest linen that was used for tents. This court,
according to the common computation of cubits, was fifty yards long, and
twenty-five broad. Pillars were set up at convenient distances, in
sockets of brass, the pillars filleted with silver, and silver
tenter-hooks in them, on which the linen hangings were fastened: the
hanging which served for the gate was finer than the rest, v. 16. This
court was a type of the church, enclosed and distinguished from the rest
of the world, the enclosure supported by pillars, denoting the stability
of the church, hung with the clean linen, which is said to be the
righteousness of saints, Rev. 19:8. These were the courts David longed
for and coveted to reside in (Ps. 84:2, 10), and into which the people
of God entered with praise and thanksgiving (Ps. 100:4); yet this court
would contain but a few worshippers. Thanks be to God, now, under the
gospel, the enclosure is taken down. God\'s will is that men pray
everywhere; and there is room for all that in every place call on the
name of Jesus Christ.

### Verses 20-21

We read of the candlestick in the twenty-fifth chapter; here is an order
given for the keeping of the lamps constantly burning in it, else it was
useless; in every candlestick there should be a burning and shining
light; candlesticks without candles are as wells without water or as
clouds without rain. Now, 1. The people were to provide the oil; from
them the Lord\'s ministers must have their maintenance. Or, rather, the
pure oil signified the gifts and graces of the Spirit, which are
communicated to all believers from Christ the good olive, of whose
fullness we receive (Zec. 4:11, 12), and without which our light cannot
shine before men. 2. The priests were to light the lamps, and to tend
them; it was part of their daily service to cause the lamp to burn
always, night and day; thus it is the work of ministers, by the
preaching and expounding of the scriptures (which are as a lamp), to
enlighten the church, God\'s tabernacle upon the earth, and to direct
the spiritual priests in his service. This is to be a statute for ever,
that the lamps of the word be lighted as duly as the incense of prayer
and praise is offered.
