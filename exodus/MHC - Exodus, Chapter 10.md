Exodus, Chapter 10
==================

Commentary
----------

The eighth and ninth of the plagues of Egypt, that of locusts and that
of darkness, are recorded in this chapter. `I.` Concerning the plague of
locusts, 1. God instructs Moses in the meaning of these amazing
dispensations of his providence (v. 1, 2). 2. He threatens the locusts
(v. 3-6). 3. Pharaoh, at the persuasion of his servants, is willing to
treat again with Moses (v. 7-9), but they cannot agree (v. 10, 11). 4.
The locusts come (v. 12-15). 5. Pharaoh cries Peccavi-I have offended
(v. 16, 17), whereupon Moses prays for the removal of the plague, and it
is done; but Pharaoh\'s heart is still hardened (v. 18-20). `II.`
Concerning the plague of darkness, 1. It is inflicted (v. 21-23). 2.
Pharaoh again treats with Moses about a surrender, but the treaty breaks
off in a heat (v. 26, etc.).

### Verses 1-11

Here, `I.` Moses is instructed. We may well suppose that he, for his part,
was much astonished both at Pharaoh\'s obstinacy and at God\'s severity,
and could not but be compassionately concerned for the desolations of
Egypt, and at a loss to conceive what this contest would come to at
last. Now here God tells him what he designed, not only Israel\'s
release, but the magnifying of his own name: That thou mayest tell in
thy writings, which shall continue to the world\'s end, what I have
wrought in Egypt, v. 1, 2. The ten plagues of Egypt must be inflicted,
that they may be recorded for the generations to come as undeniable
proofs, 1. Of God\'s overruling power in the kingdom of nature, his
dominion over all the creatures, and his authority to use them either as
servants to his justice or sufferers by it, according to the counsel of
his will. 2. Of God\'s victorious power over the kingdom of Satan, to
restrain the malice and chastise the insolence of his and his church\'s
enemies. These plagues are standing monuments of the greatness of God,
the happiness of the church, and the sinfulness of sin, and standing
monitors to the children of men in all ages not to provoke the Lord to
jealousy nor to strive with their Maker. The benefit of these
instructions to the world sufficiently balances the expense.

`II.` Pharaoh is reproved (v. 3): Thus saith the Lord God of the poor,
despised, persecuted, Hebrews, How long wilt thou refuse to humble
thyself before me? Note, It is justly expected from the greatest of men
that they humble themselves before the great God, and it is at their
peril if they refuse to do it. This has more than once been God\'s
quarrel with princes. Belshazzar did not humble his heart, Dan. 5:22.
Zedekiah humbled not himself before Jeremiah, 2 Chr. 36:12. Those that
will not humble themselves God will humble. Pharaoh had sometimes
pretended to humble himself, but no account was made of it, because he
was neither sincere nor constant in it.

`III.` The plague of locusts is threatened, v. 4-6. The hail had broken
down the fruits of the earth, but these locusts should come and devour
them: and not only so, but they should fill their houses, whereas the
former inroads of these insects had been confined to their lands. This
should be much worse than all the calamities of that king which had ever
been known. Moses, when he had delivered his message, not expecting any
better answer than he had formerly, turned himself and went out from
Pharaoh, v. 6. Thus Christ appointed his disciples to depart from those
who would not receive them, and to shake off the dust of their feet for
a testimony against them; and ruin is not far off from those who are
thus justly abandoned by the Lord\'s messengers, 1 Sa. 15:27, etc.

`IV.` Pharaoh\'s attendants, his ministers of state, or
privy-counsellors, interpose, to persuade him to come to some terms with
Moses, v. 7. They, as in duty bound, represent to him the deplorable
condition of the kingdom (Egypt is destroyed), and advise him by all
means to release his prisoners (Let the men go); for Moses, they found,
would be a snare to them till it was done, and it were better to consent
at first than to be compelled at last. The Israelites had become a
burdensome stone to the Egyptians, and now, at length, the princes of
Egypt were willing to be rid of them, Zec. 12:3. Note, It is a thing to
be regretted (and prevented, if possible) that a whole nation should be
ruined for the pride and obstinacy of its princes, Salus populi suprema
lex-To consult the welfare of the people is the first of laws.

`V.` A new treaty is, hereupon, set on foot between Pharaoh and Moses, in
which Pharaoh consents for the Israelites to go into the wilderness to
do sacrifice; but the matter in dispute was who should go, v. 8. 1.
Moses insists that they should take their whole families, and all their
effects, along with them, v. 9. note, Those that serve God must serve
him with all they have. Moses pleads, \"We must hold a feast, therefore
we must have our families to feast with, and our flocks and herds to
feast upon, to the honour of God.\" 2. Pharaoh will by no means grant
this: he will allow the men to go, pretending that this was all they
desired, though this matter was never yet mentioned in any of the former
treaties; but, for the little ones, he resolves to keep them as
hostages, to oblige them to return, v. 10, 11. In a great passion he
curses them, and threatens that, if they offer to remove their little
ones, they will do it at their peril. Note, Satan does all he can to
hinder those that serve God themselves from bringing their children in
to serve him. He is a sworn enemy to early piety, knowing how
destructive it is to the interests of his kingdom; whatever would hinder
us from engaging our children to the utmost in God\'s service, we have
reason to suspect the hand of Satan in it. 3. The treaty, hereupon,
breaks off abruptly; those that before went out from Pharaoh\'s presence
(v. 6) were now driven out. Those will quickly hear their doom that
cannot bear to hear their duty. See 2 Chr. 25:16. Quos Deus destruet eos
dementat-Whom God intends to destroy he delivers up to infatuation.
Never was man so infatuated to his own ruin as Pharaoh was.

### Verses 12-20

Here is, `I.` The invasion of the land by the locusts-God\'s great army,
Joel 2:11. God bids Moses stretch out his hand (v. 12), to beckon them,
as it wee (for they came at a call), and he stretched forth his rod, v.
13. Compare ch. 9:22 23. Moses ascribes it to the stretching out, not of
his own hand, but the rod of God, the instituted sign of God\'s presence
with him. The locusts obey the summons, and fly upon the wings of the
wind, the east wind, and caterpillars without number, as we are told,
Ps. 105:34, 35. A formidable army of horse and foot might more easily
have been resisted than this host of insects. Who then is able to stand
before the great God?

`II.` The desolations they made in it (v. 15): They covered the face of
the earth, and ate up the fruit of it. The earth God has given to the
children of men; yet, when God pleases, he can disturb their possession
and send locusts and caterpillars to force them out. Herbs grow for the
service of man; yet, when God pleases, those contemptible insects shall
not only be fellow-commoners with him, but shall plunder him, and eat
the bread out of his mouth. Let our labour be, not for the habitation
and meat which thus lie exposed, but for those which endure to eternal
life, which cannot be thus invaded, nor thus corrupted.

`III.` Pharaoh\'s admission, hereupon, v. 16, 17. He had driven Moses and
Aaron from him (v. 11), telling them (it is likely) he would have no
more to do with them. But now he calls for them again in all haste, and
makes court to them with as much respect as before he had dismissed them
with disdain. Note, The day will come when those who set at nought their
counsellors, and despise all their reproofs, will be glad to make an
interest in them and engage them to intercede on their behalf. The
foolish virgins court the wise to give them of their oil; and see Ps.
141:6. 1. Pharaoh confesses his fault: I have sinned against the Lord
your God, and against you. He now sees his own folly in the slights and
affronts he had put on God and his ambassadors, and seems at least, to
repent of it. When God convinces men of sin, and humbles them for it,
their contempt of God\'s ministers, and the word of the Lord in their
mouths, will certainly come into the account, and lie heavily upon their
consciences. Some think that when Pharaoh said, \"The LORD your God,\"
he did in effect say, \"The LORD shall not be my God.\" Many treat with
God as a potent enemy, whom they are willing not to be at war with, but
care not for treating with him as their rightful prince, to whom they
are willing to submit with loyal affection. True penitents lament sin as
committed against God, even their own God, to whom they stand obliged.
2. He begs pardon, not of God, as penitents ought, but of Moses, which
was more excusable in him, because, by a special commission, Moses was
made a god to Pharaoh, and whosesoever sins he remitted they were
forgiven; when he prays, Forgive this once, he, in effect, promises not
to offend in like manner any more, yet seems loth to express that
promise, nor does he say any thing particularly of letting the people
go. Note, Counterfeit repentance commonly cheats men with general
promises and is loth to covenant against particular sins. 3. He entreats
Moses and Aaron to pray for him. There are those who, in distress,
implore the help of other persons\' prayers, but have no mind to pray
for themselves, showing thereby that they have no true love to God, nor
any delight in communion with him. Pharaoh desires their prayers that
this death only might be taken away, not this sin: he deprecates the
plague of locusts, not the plague of a hard heart, which yet was much
the more dangerous.

`IV.` The removal of the judgment, upon the prayer of Moses, v. 18, 19.
This was, 1. As great an instance of the power of God as the judgment
itself. An east wind brought the locusts, and now a west wind carried
them off. Note, Whatever point of the compass the wind is in, it is
fulfilling God\'s word, and turns about by his counsel. The wind bloweth
where it listeth, as it respects any control of ours; not so as it
respects the control of God: he directeth it under the whole heaven. 2.
It was as great a proof of the authority of Moses, and as firm a
ratification of his commission and his interest in that God who both
makes peace and creates evil, Isa. 45:7. Nay, hereby he not only
commanded the respect, but recommended himself to the good affections of
the Egyptians, inasmuch as, while the judgment came in obedience to his
summons, the removal of it was in answer to his prayers. He never
desired the woeful day, though he threatened it. His commission indeed
ran against Egypt, but his intercession was for it, which was a good
reason why they should love him, though they feared him. 3. It was also
as strong an argument for their repentance as the judgment itself; for
by this it appeared that God is ready to forgive, and swift to show
mercy. If he turn away a particular judgment, as he did often from
Pharaoh, or defer it, as in Ahab\'s case, upon the profession of
repentance and the outward tokens of humiliation, what will he do if we
be sincere, and how welcome will true penitents be to him! O that this
goodness of God might lead us to repentance!

`V.` Pharaoh\'s return to his impious resolution again not to let the
people go (v. 20), through the righteous hand of God upon him, hardening
his heart, and confirming him in his obstinacy. Note, Those that have
often baffled their convictions, and stood it out against them, forfeit
the benefit of them, and are justly given up to those lusts of their own
hearts which (how strong soever their convictions) prove too strong for
them.

### Verses 21-29

Here is, `I.` The plague of darkness brought upon Egypt, and a most
dreadful plague it was, and therefore is put first of the ten in Ps.
105:28, though it was one of the last; and in the destruction of the
spiritual Egypt it is produced by the fifth vial, which is poured out
upon the seat of the beast, Rev. 16:10. His kingdom was full of
darkness. Observe particularly concerning this plague, 1. That it was a
total darkness. We have reason to think, not only that the lights of
heaven were clouded, but that all their fires and candles were put out
by the damps or clammy vapours which were the cause of this darkness;
for it is said (v. 23), They saw not one another. It is threatened to
the wicked (Job 18:5, 6) that the spark of his fire shall not shine
(even the sparks of his own kindling, as they are called, Isa. 50:11),
and that the light shall be dark in his tabernacle. Hell is utter
darkness. The light of a candle shall shine no more at all in thee, Rev.
18:23. 2. That it was darkness which might be felt (v. 21), felt in its
causes by their fingers\' ends (so thick were the fogs), felt in its
effects, some think, by their eyes, which were pricked with pain, and
made the more sore by their rubbing them. Great pain is spoken of as the
effect of that darkness, Rev. 16:10, which alludes to this. 3. No doubt
it astonished and terrified them. The cloud of locusts, which had
darkened the land (v. 15), was nothing to this. The tradition of the
Jews is that in this darkness they were terrified by the apparitions of
evil spirits, or rather by dreadful sounds and murmurs which they made,
or (which is no less frightful) by the horrors of their own consciences;
and this is the plague which some think is intended (for, otherwise, it
is not mentioned at all there) Ps. 78:49, He poured upon them the
fierceness of his anger, by sending evil angels among them; for to those
to whom the devil has been a deceiver he will, at length, be a terror.
4. It continued three days, six nights (says bishop Hall) in one; so
long they were imprisoned by those chains of darkness, and the most
lightsome palaces were perfect dungeons. No man rose from his place, v.
23. They were all confined to their houses; and such a terror seized
them that few of them had the courage to go from the chair to the bed,
or from the bed to the chair. Thus were they silent in darkness, 1 Sa.
2:9. Now Pharaoh had time to consider, if he would have improved it.
Spiritual darkness is spiritual bondage; while Satan blinds men\'s eyes
that they see not, he binds them hands and feet that they work not for
God, nor move towards heaven. They sit in darkness. 5. It was a
righteous thing with God thus to punish them. Pharaoh and his people had
rebelled against the light of God\'s word, which Moses spoke to them;
justly therefore are they punished with darkness, for they loved it and
chose it rather. The blindness of their minds brings upon them this
darkness of the air. Never was mind so blinded as Pharaoh\'s, never was
air so darkened as Egypt\'s. The Egyptians by their cruelty would have
extinguished the lamp of Israel, and quenched their coal; justly
therefore does God put out their lights. Compare it with the punishment
of the Sodomites, Gen. 19:11. Let us dread the consequences of sin; if
three days\' darkness was so dreadful, what will everlasting darkness
be? 6. The children of Israel, at the same time, had light in their
dwellings (v. 23), not only in the land of Goshen, where most of them
dwelt, but in the habitations of those who were dispersed among the
Egyptians: for that some of them were thus dispersed appears from the
distinction afterwards appointed to be put on their door-posts, ch.
12:7. This is an instance, `(1.)` Of the power of God above the ordinary
power of nature. We must not think that we share in common mercies as a
matter of course, and therefore that we owe no thanks to God for them;
he could distinguish, and withhold that from us which he grants to
other. He does indeed ordinarily make his sun to shine on the just and
unjust; but he could make a difference, and we must own ourselves
indebted to his mercy that he does not. `(2.)` Of the particular favour he
bears to his people: they walk in the light when others wander endlessly
in thick darkness; wherever there is an Israelite indeed, though in this
dark world, there is light, there is a child of light, one for whom
light is sown, and whom the day-spring from on high visits. When God
made this difference between the Israelites and the Egyptians, who would
not have preferred the poorest cottage of an Israelite to the finest
palace of an Egyptian? There is still a real difference, though not so
discernible a one, between the house of the wicked, which is under a
curse, and the habitation of the just, which is blessed, Prov. 3:33. We
should believe in that difference, and govern ourselves accordingly.
Upon Ps. 105:28, He sent darkness and made it dark, and they rebelled
not against his word, some ground a conjecture that, during these three
days of darkness, the Israelites were circumcised, in order to their
celebrating the passover which was now approaching, and that the command
which authorized this was the word against which they rebelled not; for
their circumcision, when they entered Canaan, is spoken of as a second
general circumcision, Jos. 5:2. During these three days of darkness to
the Egyptians, if God had so pleased, the Israelites, by the light which
they had, might have made their escape, and without asking leave of
Pharaoh; but God would bring them out with a high hand, and not by
stealth, nor in haste, Isa. 52:12.

`II.` Here is the impression made upon Pharaoh by this plague, much like
that of the foregoing plagues. 1. It awakened him so far that he renewed
the treaty with Moses and Aaron, and now, at length, consented that they
should take their little ones with them, only he would have their cattle
left in pawn, v. 24. It is common for sinners thus to bargain with God
Almighty. Some sins they will leave, but not all; they will leave their
sins for a time, but they will not bid them a final farewell; they will
allow him some share in their hearts, but the world and the flesh must
share with him: thus they mock God, but they deceive themselves. Moses
resolves not to abate in his terms: Our cattle shall go with us, v. 26.
Note, The terms of reconciliation are so fixed that though men dispute
them ever so long they cannot possibly alter them, nor bring them lower.
We must come up to the demands of God\'s will, for we cannot expect he
should condescend to the provisos of our lusts. God\'s messengers must
always be bound up by that rule (Jer. 15:19), Let them return unto thee,
but return not thou unto them. Moses gives a very good reason why they
must take their cattle with them; they must go to do sacrifice, and
therefore they must take wherewithal. What numbers and kinds of
sacrifices would be required they did not yet know, and therefore they
must take all they had. Note, With ourselves, and our children, we must
devote all our worldly possessions to the service of God, because we
know not what use God will make of what we have, nor in what way we may
be called upon to honour God with it. 2. Yet it exasperated him so far
that, when he might not make his own terms, he broke off the conference
abruptly, and took up a resolution to treat no more. Wrath now came upon
him to the utmost, and he became outrageous beyond all bounds, v. 28.
Moses is dismissed in anger, forbidden the court upon pain of death,
forbidden so much as to meet Pharaoh any more, as he had been used to
do, by the river\'s side: In that day thou seest my face, thou shalt
die. Prodigious madness! Had he not found that Moses could plague him
without seeing his face? Or had he forgotten how often he had sent for
Moses as his physician to heal him and ease him of his plagues? and must
he now be bidden to come near him no more? Impotent malice! To threaten
him with death who was armed with such a power, and at whose mercy he
had so often laid himself. What will not hardness of heart and contempt
of God\'s word and commandments bring men to? Moses takes him at his
word (v. 29): I will see thy face no more, that is, \"after this time;\"
for this conference did not break off till ch. 11:8, when Moses went out
in a great anger, and told Pharaoh how soon he would change his mind,
and his proud spirit would come down, which was fulfilled (ch. 12:31),
when Pharaoh became a humble supplicant to Moses to depart. So that,
after this interview, Moses came no more, till he was sent for. Note,
When men drive God\'s word from them he justly permits their delusions,
and answers them according to the multitude of their idols. When the
Gadarenes desired Christ to depart, he presently left them.
