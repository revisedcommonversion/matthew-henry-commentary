Exodus, Chapter 38
==================

Commentary
----------

Here is an account, `I.` Of the making of the brazen altar (v. 1-7), and
the laver (v. 8). `II.` The preparing of the hangings for the enclosing of
the court in which the tabernacle was to stand (v. 9-20). `III.` A summary
of the gold, silver, and brass, that was contributed to, and used in,
the preparing of the tabernacle (v. 21, etc.).

### Verses 1-8

Bezaleel having finished the gold-work, which, though the richest, yet
was ordered to lie most out of sight, in the tabernacle itself, here
goes on to prepare the court, which lay open to the view of all. Two
things the court was furnished with, and both made of brass:-

`I.` An altar of burnt-offering, v. 1-7. On this all their sacrifices were
offered, and it was this which, being sanctified itself for this purpose
by the divine appointment, sanctified the gift that was in faith offered
on it. Christ was himself the altar to his own sacrifice of atonement,
and so he is to all our sacrifices of acknowledgment. We must have an
eye to him in offering them, as God has in accepting them.

`II.` A laver, to hold water for the priests to wash in when they went in
to minister, v. 8. This signified the provision that is made in the
gospel of Christ for the cleansing of our souls from the moral pollution
of sin by the merit and grace of Christ, that we may be fit to serve the
holy God in holy duties. This is here said to be made of the
looking-glasses (or mirrors) of the women that assembled at the door of
the tabernacle.

`1.` It should seem these women were eminent and exemplary for devotion,
attending more frequently and seriously at the place of public worship
than others did; and notice is here taken of it to their honour. Anna
was such a one long afterwards, who departed not from the temple, but
served God with fastings and prayers night and day, Lu. 2:37. It seems
in every age of the church there have been some who have thus
distinguished themselves by their serious zealous piety, and they have
thereby distinguished themselves; for devout women are really honourable
women (Acts 13:50), and not the less so for their being called, by the
scoffers of the latter days, silly women. Probably these women were such
as showed their zeal upon this occasion, by assisting in the work that
was now going on for the service of the tabernacle. They assembled by
troops, so the word is; a blessed sight, to see so many, and those so
zealous and so unanimous, in this good work.

`2.` These women parted with their mirrors (which were of the finest
brass, burnished for that purpose) for the use of the tabernacle. Those
women that admire their own beauty, are in love with their own shadow,
and make the putting on of apparel their chief adorning by which they
value and recommend themselves, can but ill spare their looking-glasses;
yet these women offered them to God, either, `(1.)` In token of their
repentance for the former abuse of them, to the support of their pride
and vanity; now that they were convinced of their folly, and had devoted
themselves to the service of God at the door of the tabernacle, they
thus threw away that which, though lawful and useful in itself, yet had
been an occasion of sin to them. Thus Mary Magdalene, who had been a
sinner, when she became a penitent wiped Christ\'s feet with her hair.
Or, `(2.)` In token of their great zeal for the work of the tabernacle;
rather than the workmen should want brass, or not have of the best, they
would part with their mirrors, though they could not do well without
them. God\'s service and glory must always be preferred by us before any
satisfactions or accommodations of our own. Let us never complain of the
want of that which we may honour God by parting with.

`3.` These mirrors were used for the making of the laver. Either they
were artfully joined together, or else molten down and cast anew; but it
is probable that the laver was so brightly burnished that the sides of
it still served for mirrors, that the priests, when they came to wash,
might there see their faces, and so discover the spots, to wash them
clean. Note, In the washing of repentance, there is need of the
looking-glass of self-examination. The word of God is a glass, in which
we may see our own faces (see Jam. 1:23); and with it we must compare
our own hearts and lives, that, finding out our blemishes, we may wash
with particular sorrow, and application of the blood of Christ to our
souls. Usually the more particular we are in the confession of sin the
more comfort we have in the sense of the pardon.

### Verses 9-20

The walls of the court, or church-yard, were like the rest curtains or
hangings, made according to the appointment, ch. 27:9, etc. This
represented the state of the Old-Testament church: it was a garden
enclosed; the worshippers were then confined to a little compass. But
the enclosure being of curtains only intimated that the confinement of
the church in one particular nation was not to be perpetual. The
dispensation itself was a tabernacle-dispensation, movable and mutable,
and in due time to be taken down and folded up, when the place of the
tent should be enlarged and its cords lengthened, to make room for the
Gentile world, as is foretold, Isa. 54:2, 3. The church here on earth is
but the court of God\'s house, and happy they that tread these courts
and flourish in them; but through these courts we are passing to the
holy place above. Blessed are those that dwell in that house of God:
they well be still praising him. The enclosing of a court before the
tabernacle teaches us a gradual approach to God. The priests that
ministered must pass through the holy court, before they entered the
holy house. Thus before solemn ordinances there ought to be the
separated and enclosed court of a solemn preparation, in which we must
wash our hands, and so draw near with a true heart.

### Verses 21-31

Here we have a breviat of the account which, by Moses\'s appointment,
the Levites took and kept of the gold, silver, and brass, that was
brought in for the tabernacle\'s use, and how it was employed. Ithamar
the son of Aaron was appointed to draw up this account, and was thus by
less services trained up and fitted for greater, v. 21. Bezaleel and
Aholiab must bring in the account (v. 22, 23), and Ithamar must audit
it, and give it in to Moses. And it was thus:-1. All the gold was a
free-will offering; every man brought as he could and would, and it
amounted to twenty-nine talents, and 730 shekels over, which some
compute to be about 150,000l. worth of gold, according to the present
value of it. Of this were made all the golden furniture and vessels. 2.
The silver was levied by way of tax; every man was assessed half a
shekel, a kind of poll-money, which amounted in the whole to 100
talents, and 1775 shekels over, v. 25, 26. Of this they made the sockets
into which the boards of the tabernacle were let, and on which they
rested; so that they were as the foundation of the tabernacle, v. 27.
The silver amounted to about 34,000l. of our money. The raising of the
gold by voluntary contribution, and of the silver by way of tribute,
shows that either way may be taken for the defraying of public expenses,
provided that nothing be done with partiality. 3. The brass, though less
valuable, was of use not only for the brazen altar, but for the sockets
of the court, which probably in other tents were of wood: but it is
promised (Isa. 60:17), For wood I will bring brass. See how liberal the
people were and how faithful the workmen were, in both which respects
their good example ought to be followed.
