Exodus, Chapter 22
==================

Commentary
----------

The laws of this chapter relate, `I.` To the eighth commandment,
concerning theft (v. 1-4), trespass by cattle (v. 5), damage by fire (v.
6), trusts (v. 7-13), borrowing cattle (v. 14, 15), or money (v. 25-27).
`II.` To the seventh commandment. Against fornication (v. 16, 17),
bestiality (v. 19). `III.` To the first table, forbidding witchcraft (v.
18), idolatry (v. 20). Commanding to offer the firstfruits (v. 29, 30).
IV. To the poor (v. 21-24). `V.` To the civil government (v. 28). `VI.` To
the peculiarity of the Jewish nation (v. 31).

### Verses 1-6

Here are the laws,

`I.` Concerning theft, which are these:-1. If a man steal any cattle (in
which the wealth of those times chiefly consisted), and they be found in
his custody, he must restore double, v. 4. Thus he must both satisfy for
the wrong and suffer for the crime. But it was afterwards provided that
if the thief were touched in conscience, and voluntarily confessed it,
before it was discovered or enquired into by any other, then he should
only make restitution of what he had stolen, and add to it a fifth part,
Lev. 6:4, 5. 2. If he had killed or sold the sheep or ox he had stolen,
and thereby persisted in his crime, he must restore five oxen for an ox,
and four sheep for a sheep (v. 1), more for an ox than for a sheep
because the owner, besides all the other profit, lost the daily labour
of his ox. This law teaches us that fraud and injustice, so far from
enriching men, will impoverish them: if we unjustly get and keep that
which is another\'s, it will not only waste itself, but it will consume
that which is our own. 3. If he was not able to make restitution, he
must be sold for a slave, v. 3. The court of judgment was to do it, and
it is probable that the person robbed had the money. Thus with us, in
some cases, felons are transported into plantations where alone
Englishmen know what slavery is. 4. If a thief broke a house in the
night, and was killed in the doing of it, his blood was upon his own
head, and should not be required at the hand of him that shed it, v. 2.
As he that does an unlawful act bears the blame of the mischief that
follows to others, so likewise of that which follows to himself. A
man\'s house is his castle, and God\'s law, as well as man\'s, sets a
guard upon it; he that assaults it does so at his peril. Yet, if it was
in the day-time that the thief was killed, he that killed him must be
accountable for it (v. 3), unless it was in the necessary defence of his
own life. Note, We ought to be tender of the lives even of bad men; the
magistrate must afford us redress, and we must not avenge ourselves.

`II.` Concerning trespass, v. 5. He that wilfully put his cattle into his
neighbour\'s field must make restitution of the best of his own. Our law
makes a much greater difference between this and other thefts than the
law of Moses did. The Jews hence observed it as a general rule that
restitution must always be made of the best, and that no man should keep
any cattle that were likely to trespass upon his neighbours or do them
any damage. We should be more careful not to do wrong than not to suffer
wrong, because to suffer wrong is only an affliction, but to do wrong is
a sin, and sin is always worse than affliction.

`III.` Concerning damage done by fire, v. 6. He that designed only the
burning of thorns might become accessory to the burning of corn, and
should not be held guiltless. Men of hot and eager spirits should take
heed, lest, while they pretend only to pluck up the tares, they root out
the wheat also. If the fire did mischief, he that kindled it must answer
for it, though it could not be proved that he designed the mischief. Men
must suffer for their carelessness, as well as for their malice. We must
take heed of beginning strife; for, though it seem but little, we know
not how great a matter it may kindle, the blame of which we must bear,
if, with the madman, we cast fire-brands, arrows, and death, and pretend
we mean no harm. It will make us very careful of ourselves, if we
consider that we are accountable, not only for the hurt we do, but for
the hurt we occasion through inadvertency.

### Verses 7-15

These laws are,

`I.` Concerning trusts, v. 7-13. If a man deliver goods, suppose to a
carrier to be conveyed, or to a warehouse-keeper to be preserved, or
cattle to a farmer to be fed, upon a valuable consideration, and if a
special confidence be reposed in the person they are lodged with, in
case these goods be stolen or lost, perish or be damaged, if it appear
that it was not by any fault of the trustee, the owner must stand to the
loss, otherwise he that has been false to this trust must be compelled
to make satisfaction. The trustee must aver his innocence upon oath
before the judges, if the case was such as afforded no other proof, and
they were to determine the matter according as it appeared. This teaches
us, 1. That we ought to be very careful of every thing we are entrusted
with, as careful of it, though it be another\'s, as if it were our own.
It is unjust and base, and that which all the world cries shame on, to
betray a trust. 2. That there is such a general failing of truth and
justice upon earth as gives too much occasion to suspect men\'s honesty
whenever it is their interest to be dishonest. 3. That an oath for
confirmation is an end of strife, Heb. 6:16. It is called an oath for
the Lord (v. 11), because to him the appeal is made, not only as to a
witness of truth, but as to an avenger of wrong and falsehood. Those
that had offered injury to their neighbour by doing any unjust thing,
yet, it might be hoped, had not so far debauched their consciences as to
profane an oath of the Lord, and call the God of truth to be witness to
a lie: perjury is a sin which natural conscience startles at as much as
any other. The religion of an oath is very ancient, and a plain
indication of the universal belief of a God, and a providence, and a
judgment to come. 4. That magistracy is an ordinance of God, designed,
among other intentions, to assist men both in discovering rights
disputed and recovering rights denied; and great respect ought to be
paid to the determination of the judges. 5. That there is no reason why
a man should suffer for that which he could not help: masters should
consider this, in dealing with their servants, and not rebuke that as a
fault which was a mischance, and which they themselves, had they been in
their servants\' places, could not have prevented.

`II.` Concerning loans, v. 14, 15. If a man (suppose) lent his team to
his neighbour, if the owner was with it, or was to receive profit for
the loan of it, whatever harm befel the cattle the owner must stand to
the loss of: but if the owner was so kind to the borrower as to lend it
to him gratis, and put such a confidence in him as to trust it from
under his own eye, then, if any harm happened, the borrower must make it
good. Let us learn hence to be very careful not to abuse any thing that
is lent us; it is not only unjust, but base and disingenuous, inasmuch
as it is rendering evil for good; we should much rather choose to lose
ourselves than that any should sustain loss by their kindness to us.
Alas, master! for it was borrowed, 2 Ki. 6:5.

### Verses 16-24

Here is, `I.` A law that he who debauched a young woman should be obliged
to marry her, v. 16, 17. If she was betrothed to another, it was death
to debauch her (Deu. 22:23, 24); but the law here mentioned respects her
as single. But, if the father refused her to him, he was to give
satisfaction in money for the injury and disgrace he had done her. This
law puts an honour upon marriage and shows likewise how improper a thing
it is that children should marry without their parents\' consent: even
here, where the divine law appointed the marriage, both as a punishment
to him that had done wrong and a recompence to her that had suffered
wrong, yet there was an express reservation for the father\'s power; if
he denied his consent, it must be no marriage.

`II.` A law which makes witchcraft a capital crime, v. 18. Witchcraft not
only gives that honour to the devil which is due to God alone, but bids
defiance to the divine Providence, wages war with God\'s government, and
puts his work into the devil\'s hand, expecting him to do good and evil,
and so making him indeed the god of this world; justly therefore was it
punished with death, especially among a people that were blessed with a
divine revelation, and cared for by divine Providence above any people
under the sun. By our law, consulting, covenanting with, invocating, or
employing, any evil spirit, to any intent whatsoever, and exercising any
enchantment, charm, or sorcery, whereby hurt shall be done to any person
whatsoever, is made felony, without benefit of clergy; also pretending
to tell where goods lost or stolen may be found, or the like, is an
iniquity punishable by the judge, and the second offence with death. The
justice of our law herein is supported by the law of God recorded here.

`III.` Unnatural abominations are here made capital; such beasts in the
shape of men as are guilty of them are unfit to live (v. 19): Whosoever
lies with a beast shall die.

`IV.` Idolatry is also made capital, v. 20. God having declared himself
jealous in this matter, the civil powers must be jealous in it too, and
utterly destroy those persons, families, and places of Israel, that
worshipped any god, save the Lord: this law might have prevented the
woeful apostasies of the Jewish nation in after times, if those that
should have executed it had not been ringleaders in the breach of it.

`V.` A caution against oppression. Because those who were empowered to
punish other crimes were themselves most in danger of this, God takes
the punishing of it into his own hands.

`1.` Strangers must not be abused (v. 21), not wronged in judgment by the
magistrates, not imposed upon in contracts, nor must any advantage be
taken of their ignorance or necessity; no, nor must they be taunted,
trampled upon, treated with contempt, or upbraided with being strangers;
for all these were vexations, and would discourage strangers from coming
to live among them, or would strengthen their prejudices against their
religion, to which, by all kind and gentle methods, they should
endeavour to proselyte them. The reason given why they should be kind to
strangers is, \"You were strangers in Egypt, and knew what it was to be
vexed and oppressed there,\" Note, `(1.)` Humanity is one of the laws of
religion, and obliges us particularly to be tender of those that lie
most under disadvantages and discouragements, and to extend our
compassionate concern to strangers, and those to whom we are not under
the obligations of alliance or acquaintance. Those that are strangers to
us are known to God, and he preserves them, Ps. 146:9. `(2.)` Those that
profess religion should study to oblige strangers, that they may thereby
recommend religion to their good opinion, and take heed of doing any
thing that may tempt them to think ill of it or its professors, 1 Pt.
2:12. `(3.)` Those that have themselves been in poverty and distress, if
Providence enrich and enlarge them, ought to show a particular
tenderness towards those that are now in such circumstances as they were
in formerly, doing now by them as they then wished to be done by.

`2.` Widows and fatherless must not be abused (v. 22): You shall not
afflict them, that is, \"You shall comfort and assist them, and be ready
upon all occasions to show them kindness.\" In making just demands from
them, their condition must be considered, who have lost those that
should deal for them, and protect them; they are supposed to be unversed
in business, destitute of advice, timorous, and of a tender spirit, and
therefore must be treated with kindness and compassion; no advantage
must be taken against them, nor any hardship put upon them, from which a
husband or a father would have sheltered them. For, `(1.)` God takes
particular cognizance of their case, v. 23. Having no one else to
complain and appeal to, they will cry unto God, and he will be sure to
hear them; for his law and his providence are guardians to the widows
and fatherless, and if men do not pity them, and will not hear them, he
will. Note, It is a great comfort to those who are injured and oppressed
by men that they have a God to go to who will do more than give them the
hearing; and it ought to be a terror to those who are oppressive that
they have the cry of the poor against them, which God will hear. Nay,
`(2.)` He will severely reckon with those that do oppress them. Though
they escape punishments from men, God\'s righteous judgments will pursue
and overtake them, v. 24. Men that have a sense of justice and honour
will espouse the injured cause of the weak and helpless; and shall not
the righteous God do it? Observe the equity of the sentence here passed
upon those that oppress the widows and fatherless: their wives shall
become widows, and their children fatherless; and the Lord is known by
these judgments, which he sometimes executes still.

### Verses 25-31

Here is, `I.` A law against extortion in lending. 1. They must not receive
use for money from any that borrowed for necessity (v. 25), as in that
case, Neh. 5:5, 7. And such provision the law made for the preservation
of estates to their families by the year of jubilee that a people who
had little concern in trade could not be supposed to borrow money but
for necessity, and therefore it is generally forbidden among themselves;
but to a stranger, whom yet they might not oppress, they were allowed to
lend upon usury: this law, therefore, in the strictness of it, seems to
have been peculiar to the Jewish state; but, in the equity of it, it
obliges us to show mercy to those of whom we might take advantage, and
to be content to share, in loss as well as profit, with those we lend
to, if Providence cross them; and, upon this condition, it seems as
lawful to receive interest for my money, which another takes pains with
and improves, but runs the hazard of, in trade, as it is to receive rent
for my land, which another takes pains with and improves, but runs the
hazard of, in husbandry. 2. They must not take a poor man\'s bed-clothes
in pawn; but, if they did, must restore them by bed-time, v. 26, 27.
Those who lie soft and warm themselves should consider the hard and cold
lodgings of many poor people, and not do any thing to make bad worse, or
to add affliction to the afflicted.

`II.` A law against the contempt of authority (v. 28): Thou shalt not
revile the gods, that is, the judges and magistrates, for their
executing these laws; they must do their duty, whoever suffer by it.
Magistrates ought not to fear the reproach of men, nor their revilings,
but to despise them as long as they keep a good conscience; but those
that do revile them for their being a terror to evil works and workers
reflect upon God himself, and will have a great deal to answer for
another day. We find those under a black character, and a heavy doom,
that despise dominion, and speak evil of dignities, Jude 8. Princes and
magistrates are our fathers, whom the fifth commandment obliges us to
honour and forbids us to revile. St. Paul applies this law to himself,
and owns that he ought not to speak evil of the ruler of his people; no,
not though the ruler was then his most unrighteous persecutor, Acts
23:5; see Eccl. 10:20.

`III.` A law concerning the offering of their first-fruits to God, v. 29,
30. It was appointed before (ch. 13), and it is here repeated: The
firstborn of thy sons shalt thou give unto me; and much more reason have
we to give ourselves, and all we have, to God, who spared not his own
Son, but delivered him up for us all. The first ripe of their corn they
must not delay to offer. There is danger, if we delay our duty, lest we
wholly omit it; and by slipping the first opportunity, in expectation of
another, we suffer Satan to cheat us of all our time. Let not young
people delay to offer to God the first-fruits of their time and
strength, lest their delays come, at last, to be denials, through the
deceitfulness of sin, and the more convenient season they promise
themselves never arrive. Yet it is provided that the firstlings of their
cattle should not be dedicated to God till they were past seven days
old, for then they began to be good for something. Note, God is the
first and best, and therefore must have the first and best.

`IV.` A distinction put between the Jews and all other people: You shall
be holy men unto me; and one mark of that honourable distinction is
appointed in their diet, which was, that they should not eat any flesh
that was torn of beasts (v. 31), not only because it was unwholesome,
but because it was paltry, and base, and covetous, and a thing below
those who were holy men unto God, to eat the leavings of the beasts of
prey. We that are sanctified to God must not be curious in our diet; but
we must be conscientious, not feeding ourselves without fear, but eating
and drinking by rule, the rule of sobriety, to the glory of God.
