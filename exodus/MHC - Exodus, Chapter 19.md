Exodus, Chapter 19
==================

Commentary
----------

This chapter introduces the solemnity of the giving of the law upon
mount Sinai, which was one of the most striking appearances of the
divine glory that ever was in this lower world. We have here, `I.` The
circumstances of time and place (v. 1, 2). `II.` The covenant between God
and Israel settled in general. The gracious proposal God made to them
(v. 3-6), and their consent to the proposal (v. 7, 8). `III.` Notice given
three days before of God\'s design to give the law out of a thick cloud
(v. 9). Orders given to prepare the people to receive the law (v.
10-13), and care taken to execute those orders (v. 14, 15). `IV.` A
terrible appearance of God\'s glory upon mount Sinai (v. 16-20). `V.`
Silence proclaimed, and strict charges given to the people to observe
decorum while God spoke to them (v. 21, etc.).

### Verses 1-8

Here is, `I.` The date of that great charter by which Israel was
incorporated. 1. The time when it bears date (v. 1)-in the third month
after they came out of Egypt. It is computed that the law was given just
fifty days after their coming out of Egypt, in remembrance of which the
feast of Pentecost was observed the fiftieth day after the passover, and
in compliance with which the Spirit was poured out upon the apostles at
the feast of pentecost, fifty days after the death of Christ. In Egypt
they had spoken of a three days\' journey into the wilderness to the
place of their sacrifice (ch. 5:3), but it proved to be almost a two
months\' journey; so often are we out in the calculation of times, and
things prove longer in the doing than we expected. 2. The place whence
it bears date-from Mount Sinai, a place which nature, not art, had made
eminent and conspicuous, for it was the highest in all that range of
mountains. Thus God put contempt upon cities, and palaces, and
magnificent structures, setting up his pavilion on the top of a high
mountain, in a waste and barren desert, there to carry on this treaty.
It is called Sinai, from the multitude of thorny bushes that overspread
it.

`II.` The charter itself. Moses was called up the mountain (on the top of
which God had pitched his tent, and at the foot of which Israel had
pitched theirs), and was employed as the mediator, or rather no more
than the messenger of the covenant: Thus shalt thou say to the house of
Jacob, and tell the children of Israel, v. 3. Here the learned bishop
Patrick observes that the people are called by the names both of Jacob
and Israel, to remind them that those who had lately been as low as
Jacob when he went to Padan-aram had now grown as great as God made him
when he came thence (justly enriched with the spoils of him that had
oppressed him) and was called Israel. Now observe, 1. That the maker,
and first mover, of the covenant, is God himself. Nothing was said nor
done by this stupid unthinking people themselves towards this
settlement; no motion made, no petition put up for God\'s favour, but
this blessed charter was granted ex mero motu-purely out of God\'s own
good-will. Note, In all our dealings with God, free grace anticipates us
with the blessings of goodness, and all our comfort is owing, not to our
knowing God, but rather to our being known of him, Gal. 4:9. We love
him, visit him, and covenant with him, because he first loved us,
visited us, and covenanted with us. God is the Alpha, and therefore must
be the Omega. 2. That the matter of the covenant is not only just and
unexceptionable, and such as puts no hardship upon them, but kind and
gracious, and such as gives them the greatest privileges and advantages
imaginable. `(1.)` He reminds them of what he had done for them, v. 4. He
had righted them, and avenged them upon their persecutors and
oppressors: \"You have seen what I did unto the Egyptians, how many
lives were sacrificed to Israel\'s honour and interests:\" He had given
them unparalleled instances of his favour to them, and his care of them:
I bore you on eagles\' wings, a high expression of the wonderful
tenderness God had shown for them. It is explained, Deu. 32:11, 12. It
denotes great speed. God not only came upon the wing for their
deliverance (when the set time was come, he rode on a cherub, and did
fly), but he hastened them out, as it were, upon the wing. He did it
also with great ease, with the strength as well as with the swiftness of
an eagle: those that faint not, nor are weary, are said to mount up with
wings as eagles, Isa. 40:31. Especially, it denotes God\'s particular
care of them and affection to them. Even Egypt, that iron furnace, was
the nest in which these young ones were hatched, where they were first
formed as the embryo of a nation; when, by the increase of their
numbers, they grew to some maturity, they were carried out of that nest.
Other birds carry their young in their talons, but the eagle (they say)
upon her wings, so that even those archers who shoot flying cannot hurt
the young ones, unless they first shoot through the old one. Thus, in
the Red Sea, the pillar of cloud and fire, the token of God\'s presence,
interposed itself between the Israelites and their pursuers (lines of
defence which could not be forced, a wall which could not be
penetrated): yet this was not all; their way so paved, so guarded, was
glorious, but their end much more so: I brought you unto myself. They
were brought not only into a state of liberty and honour, but into
covenant and communion with God. This, this was the glory of their
deliverance, as it is of ours by Christ, that he died, the just for the
unjust, that he might bring us to God. This God aims at in all the
gracious methods of his providence and grace, to bring us back to
himself, from whom we have revolted, and to bring us home to himself, in
whom alone we can be happy. He appeals to themselves, and their own
observation and experience, for the truth of what is here insisted on:
You have seen what I did; so that they could not disbelieve God, unless
they would first disbelieve their own eyes. They saw how all that was
done was purely the Lord\'s doing. It was not they that reached towards
God, but it was he that brought them to himself. Some have well observed
that the Old-Testament church is said to be borne upon eagles\' wings,
denoting the power of that dispensation, which was carried on with a
high hand an out-stretched arm; but the New-Testament church is said to
be gathered by the Lord Jesus, as a hen gathers her chickens under her
wings (Mt. 23:37), denoting the grace and compassion of that
dispensation, and the admirable condescension and humiliation of the
Redeemer. `(2.)` He tells them plainly what he expected and required from
them in one word, obedience (v. 5), that they should obey his voice
indeed and keep his covenant. Being thus saved by him, that which he
insisted upon was that they should be ruled by him. The reasonableness
of this demand is, long after, pleaded with them, that in the day he
brought them out of the land of Egypt this was the condition of the
covenant, Obey my voice (Jer. 7:23); and this he is said to protest
earnestly to them, Jer. 11:4, 7. Only obey indeed, not in profession and
promise only, not in pretence, but in sincerity. God had shown them real
favours, and therefore required real obedience. `(3.)` He assures them of
the honour he would put upon them, and the kindness he would show them,
in case they did thus keep his covenant (v. 5, 6): Then you shall be a
peculiar treasure to me. He does not specify any one particular favour,
as giving them the land of Canaan, or the like, but expresses it in that
which was inclusive of all happiness, that he would be to them a God in
covenant, and they should be to him a people. `[1.]` God here asserts
his sovereignty over, and propriety in, the whole visible creation: All
the earth is mine. Therefore he needed them not; he that had so vast a
dominion was great enough, and happy enough, without concerning himself
for so small a demesne as Israel was. All nations on the earth being
his, he might choose which he pleased for his peculiar, and act in a way
of sovereignty. `[2.]` He appropriates Israel to himself, First, As a
people dear unto him. You shall be a peculiar treasure; not that God was
enriched by them, as a man is by his treasure, but he was pleased to
value and esteem them as a man does his treasure; they were precious in
his sight and honourable (Isa. 43:4); he set his love upon them (Deu.
7:7), took them under his special care and protection, as a treasure
that is kept under lock and key. He looked upon the rest of the world
but as trash and lumber in comparison with them. By giving them divine
revelation, instituted ordinances, and promises inclusive of eternal
life, by sending his prophets among them, and pouring out his Spirit
upon them, he distinguished them from, and dignified them above, all
people. And this honour have all the saints; they are unto God a
peculiar people (Tit. 2:14), his when he makes up his jewels. Secondly,
As a people devoted to him, to his honour and service (v. 6), a kingdom
of priests, a holy nation. All the Israelites, if compared with other
people, were priests unto God, so near were they to him (Ps. 148:14), so
much employed in his immediate service, and such intimate communion they
had with him. When they were first made a free people it was that they
might sacrifice to the Lord their God, as priests; they were under
God\'s immediate government, and the tendency of the laws given them was
to distinguish them from others, and engage them for God as a holy
nation. Thus all believers are, through Christ, made to our God kings
and priests (Rev. 1:6), a chosen generation, a royal priesthood, 1 Pt.
2:9.

`III.` Israel\'s acceptance of this charter, and consent to the
conditions of it. 1. Moses faithfully delivered God\'s message to them
(v. 7): He laid before their faces all those words; he not only
explained to them what God had given him in charge, but he put it to
their choice whether they would accept these promises upon these terms
or no. His laying it to their faces denotes his laying it to their
consciences. 2. They readily agreed to the covenant proposed. They would
oblige themselves to obey the voice of God, and take it as a great
favour to be made a kingdom of priests to him. They answered together as
one man, nemine contradicente-without a dissentient voice (v. 8): All
that the Lord hath spoken we will do. Thus they strike the bargain,
accepting the Lord to be to them a God, and giving up themselves to be
to him a people. O that there had been such a heart in them! 3. Moses,
as a mediator, returned the words of the people to God, v. 8. Thus
Christ, the Mediator between us and God, as a prophet reveals God\'s
will to us, his precepts and promises, and then as a priest offers up to
God our spiritual sacrifices, not only of prayer and praise, but of
devout affections and pious resolutions, the work of his own Spirit in
us. Thus he is that blessed days-man who lays his hand upon us both.

### Verses 9-15

Here, `I.` God intimates to Moses his purpose of coming down upon mount
Sinai, in some visible appearance of his glory, in a thick cloud (v. 9);
for he said that he would dwell in the thick darkness (2 Chr. 6:1), and
make this his pavilion (Ps. 18:11), holding back the face of his throne
when he set it upon mount Sinai, and spreading a cloud upon it, Job
26:9. This thick cloud was to prohibit curious enquiries into things
secret, and to command an awful adoration of that which was revealed.
God would come down in the sight of all the people (v. 11); though they
should see no manner of similitude, yet they should see so much as would
convince them that God was among them of a truth. And so high was the
top of mount Sinai that it is supposed that not only the camp of Israel,
but even the countries about, might discern some extraordinary
appearance of glory upon it, which would strike a terror upon them. It
seems also to have been particularly intended to put an honour upon
Moses: That they may hear when I speak with thee, and believe thee for
ever, v. 9. Thus the correspondence was to be first settled by a
sensible appearance of the divine glory, which was afterwards to be
carried on more silently by the ministry of Moses. In like manner, the
Holy Ghost descended visibly upon Christ at his baptism, and all that
were present heard God speak to him (Mt. 3:17), that afterwards, without
the repetition of such visible tokens, they might believe him. So
likewise the Spirit descended in cloven tongues upon the apostles (Acts
2:3), that they might be believed. Observe, When the people had declared
themselves willing to obey the voice of God, then God promised they
should hear his voice; for, if any man be resolved to do his will, he
shall know it, Jn. 7:17.

`II.` He orders Moses to make preparation for this great solemnity,
giving him two days\' time for it.

`1.` He must sanctify the people (v. 10), as Job, before this, sent and
sanctified his sons, Job 1:5. He must raise their expectation by giving
them notice what God would do, and assist their preparation by directing
them what they must do. \"Sanctify them,\" that is, \"Call them off from
their worldly business, and call them to religious exercises, meditation
and prayer, that they may receive the law from God\'s mouth with
reverence and devotion. Let them be ready,\" v. 11. Note, When we are to
attend upon God in solemn ordinances it concerns us to sanctify
ourselves, and to get ready beforehand. Wandering thoughts must be
gathered in, impure affections abandoned, disquieting passions
suppressed, nay, and all cares about secular business, for the present,
dismissed and laid by, that our hearts may be engaged to approach unto
God. Two things particularly prescribed as signs and instances of their
preparation:-`(1.)` In token of their cleansing themselves from all sinful
pollutions, that they might be holy to God, they must wash their clothes
(v. 10), and they did so (v. 14); not that God regards our clothes; but
while they were washing their clothes he would have them think of
washing their souls by repentance from the sins they had contracted in
Egypt and since their deliverance. It becomes us to appear in clean
clothes when we wait upon great men; so clean hearts are required in our
attendance on the great God, who sees them as plainly as men see our
clothes. This is absolutely necessary to our acceptably worshipping God.
See Ps. 26:6; Isa. 1:16-18; Heb. 10:22. `(2.)` In token of their devoting
themselves entirely to religious exercises, upon this occasion, they
must abstain even from lawful enjoyments during these three days, and
not come at their wives, v. 15. See 1 Co. 7:5.

`2.` He must set bounds about the mountain, v. 12, 13. Probably he drew a
line, or ditch, round at the foot of the hill, which none were to pass
upon pain of death. This was to intimate, `(1.)` That humble awful
reverence which ought to possess the minds of all those that worship
God. We are mean creatures before a great Creator, vile sinners before a
holy righteous Judge; and therefore a godly fear and shame well become
us, Heb. 12:28; Ps. 2:11. `(2.)` The distance at which worshippers were
kept, under that dispensation, which we ought to take notice of, that we
may the more value our privilege under the gospel, having boldness to
enter into the holiest by the blood of Jesus, Heb. 10:19.

`3.` He must order the people to attend upon the summons that should be
given (v. 13): \"When the trumpet soundeth long then let them take their
places at the foot of the mount, and so sit down at God\'s feet,\" as it
is explained, Deu. 33:3. Never was so great a congregation called
together, and preached to, at once, as this was here. No one man\'s
voice could have reached so many, but the voice of God did.

### Verses 16-25

Now, at length, comes that memorable day, that terrible day of the Lord,
that day of judgment, in which Israel heard the voice of the Lord God
speaking to them out of the midst of the fire, and lived, Deu. 4:33.
Never was there such a sermon preached, before nor since, as this which
was here preached to the church in the wilderness. For,

`I.` The preacher was God himself (v. 18): The Lord descended in fire, and
(v. 20), The Lord came down upon mount Sinai. The shechinah, or glory of
the Lord, appeared in the sight of all the people; he shone forth from
mount Paran with ten thousands of his saints (Deu. 33:2), that is,
attended, as the divine Majesty always is, by a multitude of the holy
angels, who were both to grace the solemnity and to assist at it. Hence
the law is said to be given by the disposition of angels, Acts 7:53.

`II.` The pulpit (or throne rather) was mount Sinai, hung with a thick
cloud (v. 16), covered with smoke (v. 18), and made to quake greatly.
Now it was that the earth trembled at the presence of the Lord, and the
mountains skipped like rams (Ps. 114:4, 7), that Sinai itself, though
rough and rocky, melted from before the Lord God of Israel, Jdg. 5:5.
Now it was that the mountains saw him, and trembled (Hab. 3:10), and
were witnesses against a hard-hearted unmoved people, whom nothing would
influence.

`III.` The congregation was called together by the sound of a trumpet,
exceedingly loud (v. 16), and waxing louder and louder, v. 19. This was
done by the ministry of the angels, and we read of trumpets sounded by
angels, Rev. 8:6. It was the sound of the trumpet that made all the
people tremble, as those who knew their own guilt, and who had reason to
expect that the sound of this trumpet was to them the alarm of war.

`IV.` Moses brought the hearers to the place of meeting, v. 17. He that
had led them out of the bondage of Egypt now led them to receive the law
from God\'s mouth. Public persons are indeed public blessings when they
lay out themselves in their places to promote the public worship of God.
Moses, at the head of an assembly worshipping God, was as truly great as
Moses at the head of an army in the field.

`V.` The introductions to the service were thunders and lightnings, v. 16.
These were designed to strike an awe upon the people, and to raise and
engage their attention. Were they asleep? The thunders would awaken
them. Were they looking another way? The lightnings would engage them to
turn their faces towards him that spoke to them. Thunder and lightning
have natural causes, but the scripture directs us in a particular manner
to take notice of the power of God, and his terror, in them. Thunder is
the voice of God, and lightning the fire of God, proper to engage the
senses of sight and hearing, those senses by which we receive so much of
our information.

`VI.` Moses is God\'s minister, who is spoken to, to command silence, and
keep the congregation in order: Moses spoke, v. 19. Some think it was
now that he said, I exceedingly fear and quake (Heb. 12:21); but God
stilled his fear by his distinguishing favour to him, in calling him up
to the top of the mount (v. 20), by which also he tried his faith and
courage. No sooner had Moses got up a little way towards the top of the
mount than he was sent down again to keep the people from breaking
through to gaze, v. 21. Even the priests or princes, the heads of the
houses of their fathers, who officiated for their respective families,
and therefore are said to come near to the Lord at other times, must now
keep their distance, and conduct themselves with a great deal of
caution. Moses pleads that they needed not to have any further orders
given them, effectual care being taken already to prevent any
intrusions, v. 23. But God, who knew their wilfulness and presumption,
and what was now in the hearts of some of them, hastens him down with
this in charge, that neither the priests nor the people should offer to
force the lines that were set, to come up unto the Lord, but Moses and
Aaron on, the men whom God delighted to honour. Observe, 1. What it was
that God forbade them-breaking through to gaze; enough was provided to
awaken their consciences, but they were not allowed to gratify their
vain curiosity. They might see, but not gaze. Some of them, probably,
were desirous to see some similitude, that they might know how to make
an image of God, which he took care to prevent, for they saw no manner
of similitude, Deu. 4:5. Note, In divine things we must not covet to
know more than God would have us know; and he has allowed us as much as
is good for us. A desire of forbidden knowledge was the ruin of our
first parents. Those that would be wise above what is written, and
intrude into those things which they have not seen, need this
admonition, that they break not through to gaze. 2. Under what penalty
it was forbidden: Lest the Lord break forth upon them (v. 22-24), and
many of them perish. Note, `(1.)` The restraints and warnings of the
divine law are all intended for our good, and to keep us out of that
danger into which we should otherwise, by our own folly, run ourselves.
`(2.)` It is at our peril if we break the bounds that God has set us, and
intrude upon that which he has not allowed us; the Bethshemites and
Uzzah paid dearly for their presumption. And, even when we are called to
approach God, we must remember that he is in heaven and we upon earth,
and therefore it behoves us to exercise reverence and godly fear.
