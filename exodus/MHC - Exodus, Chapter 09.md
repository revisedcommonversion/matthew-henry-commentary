Exodus, Chapter 9
=================

Commentary
----------

In this chapter we have an account of three more of the plagues of
Egypt. `I.` Murrain among the cattle, which was fatal to them (v. 1-7).
`II.` Boils upon man and beast (v. 8-12). `III.` Hail, with thunder and
lightning. 1. Warning is given of this plague (v. 13-21). 2. It is
inflicted, to their great terror (v. 22-26). 3. Pharaoh, in a fright,
renews his treaty with Moses, but instantly breaks his word (v. 27,
etc.).

### Verses 1-7

Here is, `I.` Warning given of another plague, namely, the murrain of
beasts. When Pharaoh\'s heart was hardened, after he had seemed to
relent under the former plague, then Moses is sent to tell him there is
another coming, to try what that would do towards reviving the
impressions of the former plagues. Thus is the wrath of God revealed
from heaven, both in his word and in his works, against all ungodliness
and unrighteousness of men. 1. Moses puts Pharaoh in a very fair way to
prevent it: Let my people go, v. 1. This was still the demand. God will
have Israel released; Pharaoh opposes it, and the trial is, whose word
shall stand. See how jealous God is for his people. When the year of his
redeemed has come, he will give Egypt for their ransom; that kingdom
shall be ruined, rather than Israel shall not be delivered. See how
reasonable God\'s demands are. Whatever he calls for, it is but his own:
They are my people, therefore let them go. 2. He describes the plague
that should come, if he refused, v. 2, 3. The hand of the Lord
immediately, without the stretching out of Aaron\'s hand, is upon the
cattle, many of which, some of all kinds, should die by a sort of
pestilence. This was greatly to the loss of the owners: they had made
Israel poor, and now God would make them poor. Note, The hand of God is
to be acknowledged even in the sickness and death of cattle, or other
damage sustained in them; for a sparrow falls not to the ground without
our Father. 3. As an evidence of the special hand of God in it, and of
his particular favour to his own people, he foretels that none of their
cattle should die, though they breathed in the same air and drank of the
same water with the Egyptians\' cattle: The Lord shall sever, v. 4.
Note, When God\'s judgments are abroad, though they may fall both on the
righteous and the wicked, yet God makes such a distinction that they are
not the same to the one that they are to the other. See Isa. 27:7. The
providence of God is to be acknowledged with thankfulness in the life of
the cattle, for he preserveth man and beast, Ps. 36:6. 4. To make the
warning the more remarkable, the time is fixed (v. 5): To-morrow it
shall be done. We know not what any day will bring forth, and therefore
we cannot say what we will do to-morrow, but it is not so with God.

`II.` The plague itself inflicted. The cattle died, v. 6. Note, The
creature is made subject to vanity by the sin of man, being liable,
according to its capacity, both to serve his wickedness and to share in
his punishment, as in the universal deluge. Rom. 8:20, 22. Pharaoh and
the Egyptians sinned; but the sheep, what had they done? Yet they are
plagued. See Jer. 12:4, For the wickedness of the land, the beasts are
consumed. The Egyptians afterwards, and (some think) now, worshipped
their cattle; it was among them that the Israelites learned to make a
god of a calf: in this therefore the plague here spoken of meets with
them. Note, What we make an idol of it is just with God to remove from
us, or embitter to us. See Isa. 19:1.

`III.` The distinction put between the cattle of the Egyptians and the
Israelites\' cattle, according to the word of God: Not one of the cattle
of the Israelites died, v. 6, 7. Does God take care of oxen? Yes, he
does; his providence extends itself to the meanest of his creatures. But
it is written also for our sakes, that, trusting in God, and making him
our refuge, we may not be afraid of the pestilence that walketh in
darkness, no, not though thousands fall at our side, Ps. 91:6, 7.
Pharaoh sent to see if the cattle of the Israelites were infected, not
to satisfy his conscience, but only to gratify his curiosity, or with
design, by way of reprisal, to repair his own losses out of their
stocks; and, having no good design in the enquiry, the report brought to
him made no impression upon him, but, on the contrary, his heart was
hardened. Note, To those that are wilfully blind, even those methods of
conviction which are ordained to life prove a savour of death unto
death.

### Verses 8-12

Observe here, concerning the plague of boils and blains,

`I.` When they were not wrought upon by the death of their cattle, God
sent a plague that seized their own bodies, and touched them to the
quick. If less judgments do not do their work, God will send greater.
Let us therefore humble ourselves under the mighty hand of God, and go
forth to meet him in the way of his judgments, that his anger may be
turned away from us.

`II.` The signal by which this plague was summoned was the sprinkling of
warm ashes from the furnace, towards heaven (v. 8, 10), which was to
signify the heating of the air with such an infection as should produce
in the bodies of the Egyptians sore boils, which would be both noisome
and painful. Immediately upon the scattering of the ashes, a scalding
dew came down out of the air, which blistered wherever it fell. Note,
Sometimes God shows men their sin in their punishment; they had
oppressed Israel in the furnaces, and now the ashes of the furnace are
made as much a terror to them as ever their task-masters had been to the
Israelites.

`III.` The plague itself was very grievous-a common eruption would be so,
especially to the nice and delicate, but these eruptions were
inflammations, like Job\'s. This is afterwards called the botch of Egypt
(Deu. 28:27), as if it were some new disease, never heard of before, and
known ever after by that name, Note, Sores in the body are to be looked
upon as the punishments of sin, and to be hearkened to as calls to
repentance.

`IV.` The magicians themselves were struck with these boils, v. 11. 1.
Thus they were punished, `(1.)` For helping to harden Pharaoh\'s heart, as
Elymas for seeking to ;pervert the right ways of the Lord; God will
severely reckon with those that strengthen the hands of the wicked in
their wickedness. `(2.)` For pretending to imitate the former plagues, and
making themselves and Pharaoh sport with them. Those that would produce
lice shall, against their wills, produce boils. Note, It is ill jesting
with God\'s judgments, and more dangerous than playing with fire. Be you
not mockers, lest your bands be made strong. 2. Thus they were shamed in
the presence of their admirers. How weak were their enchantments, which
could not so much as secure themselves! The devil can give no protection
to those that are in confederacy with him. 3. Thus they were driven from
the field. Their power was restrained before (ch. 8:18), but they
continued to confront Moses, and confirm Pharaoh in his unbelief, till
now, at length, they were forced to retreat, and could not stand before
Moses, to which the apostle refers (2 Tim. 3:9) when he says that their
folly was made manifest unto all men.

`V.` Pharaoh continued obstinate, for now the Lord hardened his heart, v.
12. Before, he had hardened his own heart, and resisted the grace of
God; and now God justly gave him up to his own heart\'s lusts, to a
reprobate mind, and strong delusions, permitting Satan to blind and
harden him, and ordering every thing, henceforward, so as to make him
more and more obstinate. Note, Wilful hardness is commonly punished with
judicial hardness. If men shut their eyes against the light, it is just
with God to close their eyes. Let us dread this as the sorest judgment a
man can be under on this side hell.

### Verses 13-21

Here is, `I.` A general declaration of the wrath of God against Pharaoh
for his obstinacy. Though God has hardened his heart (v. 12), yet Moses
must repeat his applications to him; God suspends his grace and yet
demands obedience, to punish him for requiring bricks of the children of
Israel when he denied them straw. God would likewise show forth a
pattern of long-suffering, and how he waits to be gracious to a
rebellious and gainsaying people Six times the demand had been made in
vain, yet Moses must make it the seventh time: Let my people go, v. 13.
A most dreadful message Moses is here ordered to deliver to him, whether
he will hear or whether he will forbear. 1. He must tell him that he is
marked for ruin, that he now stands as the butt at which God would shoot
all the arrows of his wrath, v. 14, 15. \"Now I will send all my
plagues.\" Now that no place is found for repentance in Pharaoh, nothing
can prevent his utter destruction, for that only would have prevented
it. Now that God begins to harden his heart, his case is desperate. \"I
will send my plagues upon thy heart, not only temporal plagues upon thy
body, but spiritual plagues upon thy soul.\" Note, God can send plagues
upon thy soul.\" Note, God can send plagues upon the heart, either by
making it senseless or by making it hopeless-and these are the worst
plagues. Pharaoh must now expect no respite, no cessation of arms, but
to be followed with plague upon plague, till he is utterly consumed.
Note, When God judges he will overcome; none ever hardened his heart
against him and prospered. 2. He must tell him that he is to remain in
history a standing monument of the justice and power of God\'s wrath (v.
16): \"For this cause have I raised thee up to the throne at this time,
and made thee to stand the shock of the plagues hitherto, to show in
thee my power.\" Providence ordered it so that Moses should have a man
of such a fierce and stubborn spirit as he was to deal with; and every
thing was so managed in this transaction as to make it a most signal and
memorable instance of the power God has to humble and bring down the
proudest of his enemies. Every thing concurred to signalize this, that
God\'s name (that is, his incontestable sovereignty, his irresistible
power, and his inflexible justice) might be declared throughout all the
earth, not only to all places, but through all ages while the earth
remains. Note, God sometimes raises up very bad men to honour and power,
spares them long, and suffers them to grow insufferably insolent, that
he may be so much the more glorified in their destruction at last. See
how the neighbouring nations, at that time, improved the ruin of Pharaoh
to the glory of God. Jethro said upon it, Now know I that the Lord is
greater than all gods, 18:11. The apostle illustrates the doctrine of
God\'s sovereignty with this instance, Rom. 9:17. To justify God in
these resolutions, Moses is directed to ask him (v. 17), As yet exaltest
thou thyself against my people? Pharaoh was a great king; God\'s people
were poor shepherds at the best, and now poor slaves; and yet Pharaoh
shall be ruined if he exalt himself against them, for it is considered
as exalting himself against God. This was not the first time that God
reproved kings for their sakes, and let them know that he would not
suffer his people to be trampled upon and insulted, no, not by the most
powerful of them.

`II.` A particular prediction of the plague of hail (v. 18), and a
gracious advice to Pharaoh and his people to send for their servants and
cattle out of the field, that they might be sheltered from the hail, v.
19. Note, When God\'s justice threatens ruin his mercy, at the same
time, shows us a way of escape from it, so unwilling is he that any
should perish. See here what care God took, not only to distinguish
between Egyptians and Israelites, but between some Egyptians and others.
If Pharaoh will not yield, and so prevent the judgment itself, yet an
opportunity is given to those that have any dread of God and his word to
save themselves from sharing in the judgment. Note, Those that will take
warning may take shelter; and those that will not may thank themselves
if they fall by the overflowing scourge, and the hail which will sweep
away the refuge of lies, Isa. 28:17. See the different effect of this
warning. 1. Some believed the things that were spoken, and they feared,
and housed their servants and cattle (v. 20), like Noah (Heb. 11:7), and
it was their wisdom. Even among the servants of Pharaoh there were some
that trembled at God\'s word; and shall not the sons of Israel dread it?
But, 2. Others believed not: though, whatever plague Moses had hitherto
foretold, the event exactly answered to the prediction; and though, if
they had had any reason to question this, it would have been no great
damage to them to have kept their cattle in the house for one day, and
so, supposing it a doubtful case, to have chosen the surer side; yet
they were so foolhardy as in defiance to the truth of Moses, and the
power of God (of both which they had already had experience enough, to
their cost), to leave their cattle in the field, Pharaoh himself, it is
probable, giving them an example of the presumption, v. 21. Note,
Obstinate infidelity, which is deaf to the fairest warnings and the
wisest counsels, leaves the blood of those that perish upon their own
heads.

### Verses 22-35

The threatened plague of hail is here summoned by the powerful hand and
rod of Moses (v. 22, 23), and it obeys the summons, or rather the divine
command; for fire and hail fulfil God\'s word, Ps. 148:8. And here we
are told,

`I.` What desolations it made upon the earth. The thunder, and fire from
heaven (or lightning), made it both the more dreadful and the more
destroying, v. 23, 24. Note, God makes the clouds, not only his
store-houses whence he drops fatness on his people, but his magazines
whence, when he pleases, he can draw out a most formidable train of
artillery, with which to destroy his enemies. He himself speaks of the
treasures of hail which he hath reserved against the day of battle and
war, Job 38:22, 23. Woeful havoc this hail made in the land of Egypt. It
killed both men and cattle, and battered down, not only the herbs, but
the trees, v. 25. The corn that was above ground was destroyed, and that
only preserved which as yet had not come up, v. 31, 32. Note, God has
many ways of taking away the corn in the season thereof (Hos. 2:9),
either by a secret blasting, or a noisy hail. In this plague the hot
thunderbolts, as well as the hail, are said to destroy their flocks, Ps.
78:47, 48; and see Ps. 105:32, 33. Perhaps David alludes to this when,
describing God\'s glorious appearances for the discomfiture of his
enemies, he speaks of the hailstones and coals of fire he threw among
them, Ps. 18:12, 13. And there is a plan reference to it on the pouring
out of the seventh vial, Rev. 16:21. Notice is here taken (v. 26) of the
land of Goshen\'s being preserved from receiving any damage by this
plague. God has the directing of the pregnant clouds, and causes it to
rain or hail on one city and not on another, either in mercy or in
judgment.

`II.` What a consternation it put Pharaoh in. See what effect it had upon
him, 1. He humbled himself to Moses in the language of a penitent, v.
27, 28. No man could have spoken better. He owns himself on the wrong
side in his contest with the God of the Hebrews: \"I have sinned in
standing it out so long.\" He owns the equity of God\'s proceedings
against him: The Lord is righteous, and must be justified when he
speaks, though he speak in thunder and lightning. He condemns himself
and his land: \"I and my people are wicked, and deserve what is brought
upon us.\" He begs the prayers of Moses: \"Entreat the Lord for me, that
this direful plague may be removed.\" And, lastly, he promises to yield
up his prisoners: I will let you go. What could one desire more? And yet
his heart was hardened all this while. Note, The terror of the rod often
extorts penitent acknowledgments from those who have no penitent
affections; under the surprise and smart of affliction, they start up,
and say that which is pertinent enough, not because they are deeply
affected, but because they know that they should be and that it is meet
to be said. 2. Moses, hereupon, becomes an intercessor for him with God.
Though he had all the reason in the world to think that he would
immediately repent of his repentance, and told him so (v. 30), yet he
promises to be this friend in the court of heaven. Note, Even those whom
we have little hopes of, yet we should continue to pray for, and to
admonish, 1 Sa. 12:23. Observe, `(1.)` The place Moses chose for his
intercession. He went out of the city (v. 33), not only for privacy in
his communion with God, but to show that he durst venture abroad into
the field, notwithstanding the hail and lightning which kept Pharaoh and
his servants withindoors, knowing that every hail-stone had its
direction from his God, who meant him no hurt. Note, Peace with God
makes men thunderproof, for thunder is the voice of their Father. `(2.)`
The gesture: He spread abroad his hands unto the Lord-an outward
expression of earnest desire and humble expectation. Those that come to
God for mercy must stand ready to receive it. `(3.)` The end Moses aimed
at in interceding for him: That thou mayest know, and be convinced, that
the earth is the Lord\'s (v. 29), that is, that God has a sovereign
dominion over all the creatures, that they all are ruled by him, and
therefore that thou oughtest to be so. See what various methods God uses
to bring men to their proper senses. Judgments are sent, judgments
removed, and all for the same end, to make men know that he Lord reigns.
`(4.)` The success of it. `[1.]` He prevailed with God, v. 33. But, `[2.]`
He could not prevail with Pharaoh: He sinned yet more, and hardened his
heart, v. 34, 35. The prayer of Moses opened and shut heaven, like
Elias\'s (Jam. 5:17, 18), and such is the power of God\'s two witnesses
(Rev. 11:6); yet neither Moses nor Elias, nor those two witnesses, could
subdue the hard hearts of men. Pharaoh was frightened into a compliance
by the judgment, but, when it was over, his convictions vanished, and
his fair promises were forgotten. Note, Little credit is to be given to
confessions upon the rack. Note also, Those that are not bettered by
judgments and mercies are commonly made worse.
