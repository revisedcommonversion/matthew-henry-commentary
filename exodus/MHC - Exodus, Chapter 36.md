Exodus, Chapter 36
==================

Commentary
----------

In this chapter, `I.` The work of the tabernacle is begun (v. 1-4). `II.` A
stop is put to the people\'s contributions (v. 5-7). `III.` A particular
account is given of the making of the tabernacle itself; the fine
curtains of it (v. 8-13). The coarse ones (v. 14-19). The boards (v.
20-30). The bars (v. 31-34). The partition veil (v. 35, 36). And the
hanging for the door (v. 37, etc.).

### Verses 1-7

`I.` The workmen set in without delay. Then they wrought, v. 1. When God
had qualified them for the work, then they applied themselves to it.
Note, The talents we are entrusted with must not be laid up, but laid
out; not hid in a napkin, but traded with. What have we all our gifts
for, but to do good with them? They began when Moses called them, v. 2.
Even those whom God has qualified for, and inclined to, the service of
the tabernacle, yet must wait for a regular call to it, either
extraordinary, as that of prophets and apostles, or ordinary, as that of
pastors and teachers. And observe who they were that Moses called: Those
in whose heart God had put wisdom for this purpose, beyond their natural
capacity, and whose heart stirred them up to come to the work in good
earnest. Note, Those are to be called to the building of the gospel
tabernacle whom God has by his grace made in some measure fit for the
work and free to engage in it. Ability and willingness (with resolution)
are the two things to be regarded in the call of ministers. Has God
given them not only knowledge, but wisdom? (for those that would win
souls must be wise, and have their hearts stirred up to come to the
work, and not to the honour only; to do it, and not to talk of it only),
let them come to it with full purpose of heart to go through with it.
The materials which the people had contributed were delivered by Moses
to the workmen, v. 3. They could not create a tabernacle, that is, make
it out of nothing, nor work, unless they had something to work upon; the
people therefore brought the materials and Moses put them into their
hands. Precious souls are the materials of the gospel tabernacle; they
are built up a spiritual house, 1 Pt. 2:5. To this end they are to offer
themselves a free-will offering to the Lord, for his service (Rom.
15:16), and they are then committed to the care of his ministers, as
builders, to be framed and wrought upon by their edification and
increase in holiness, till they all come, like the curtains of the
tabernacle, in the unity of the faith, to be a holy temple, Eph. 2:21,
22; 4:12, 13.

`II.` The contributions restrained. The people continued to bring free
offerings every morning, v. 3. Note, We should always make it our
morning\'s work to bring our offerings unto the Lord; even the spiritual
offerings of prayer and praise, and a broken heart surrendered entirely
to God. This is that which the duty of every day requires. God\'s
compassions are new every morning, and so must our duty to him be.
Probably there were some that were backward at first to bring their
offering, but their neighbours\' forwardness stirred them up and shamed
them. The zeal of some provoked many. There are those who will be
content to follow who yet do not care for leading in a good work. It is
best to be forward, but better late than never. Or perhaps some who had
offered at first, having pleasure in reflecting upon it, offered more;
so far were they from grudging what they had contributed, that they
doubled their contribution. Thus, in charity, give a portion to seven,
and also to eight; having given much, give more. Now observe, 1. The
honesty of the workmen. When they had cut out their work, and found how
their stuff held out, and that the people were still forward to bring in
more, they went in a body to Moses to tell him that there needed no more
contributions, v. 4, 5. Had they sought their own things, they had now a
fair opportunity of enriching themselves by the people\'s gifts; for
they might have made up their work, and converted the overplus to their
own use, as perquisites of their place. But they were men of integrity,
that scorned to do so mean a thing as to sponge upon the people, and
enrich themselves with that which was offered to the Lord. Those are the
greatest cheats that cheat the public. If to murder many is worse than
to murder one, by the same rule to defraud communities, and to rob the
church or state, is a much greater crime than to pick the pocket of a
single person. But these workmen were not only ready to account for all
they received, but were not willing to receive more than they had
occasion for, lest they should come either into the temptation or under
the suspicion of taking it to themselves. These were men that knew when
they had enough. 2. The liberality of the people. Though they saw what
an abundance was contributed, yet they continued to offer, till they
were forbidden by proclamation, v. 6, 7. A rare instance! Most need a
spur to quicken their charity; few need a bridle to check it, yet these
did. Had Moses aimed to enrich himself, he might have suffered them
still to bring in their offerings; and when the work was finished might
have taken the remainder to himself: but he also preferred the public
before his own private interest, and was therein a good example to all
in public trusts. It is said (v. 6), The people were restrained from
bringing; they looked upon it as a restraint upon them not to be allowed
to do more for the tabernacle; such was the zeal of those people, who
gave to their power, yea, and beyond their power, praying the collectors
with much entreaty to receive the gift, 2 Co. 8:3, 4. These were the
fruits of a first love; in these last-days charity has grown too cold
for us to expect such things from it.

### Verses 8-13

The first work they set about was the framing of the house, which must
be done before the furniture of it was prepared. This house was not made
of timber or stone, but of curtains curiously embroidered and coupled
together. This served to typify the state of the church in this world,
the palace of God\'s kingdom among men. 1. Though it is upon the earth,
yet its foundation is not in the earth, as that of a house is; no,
Christ\'s kingdom is not of this world, nor founded in it. 2. It is mean
and mutable, and in a militant state; shepherds dwelt in tents, and God
is the Shepherd of Israel; soldiers dwelt in tents, and the Lord is a
man of war, and his church marches through an enemy\'s country, and must
fight its way. The kings of the earth enclose themselves in cedar (Jer.
22:15), but the ark of God was lodged in curtains only. 3. Yet there is
a beauty in holiness; the curtains were embroidered, so is the church
adorned with the gifts and graces of the Spirit, that raiment of
needle-work, Ps. 45:14. 4. The several societies of believers are united
in one, and, as here, all become one tabernacle; for there is one Lord,
one faith, and one baptism.

### Verses 14-34

Here, 1. The shelter and special protection that the church is under are
signified by the curtains of hair-cloth, which were spread over the
tabernacle, and the covering of rams\' skins and badgers\' skins over
them, v. 14-19. God has provided for his people a shadow from the heat,
and a covert from storm and rain, Isa. 4:6. They are armed against all
weathers; the sun and the moon shall not smite them: and they are
protected from the storms of divine wrath, that hail which will sweep
away the refuge of lies, Isa. 28:17. Those that dwell in God\'s house
shall find, be the tempest ever so violent, or the dropping ever so
continual, it does not rain in. 2. The strength and stability of the
church, though it is but a tabernacle, are signified by the boards and
bars with which the curtains were borne up, v. 20-34. The boards were
coupled together and joined by the bars which shot through them; for the
union of the church, and the hearty agreement of those that are its
stays and supporters, contribute abundantly to its strength and
establishment.

### Verses 35-38

In the building of a house there is a great deal of work about the doors
and partitions. In the tabernacle these were answerable to the rest of
the fabric; there were curtains for doors, and veils for partitions. 1.
There was a veil made for a partition between the holy place, and the
most holy, v. 35, 36. This signified the darkness and distance of that
dispensation, compared with the New Testament, which shows us the glory
of God more clearly and invites us to draw near to it; and the darkness
and distance of our present state, in comparison with heaven, where we
shall be ever with the Lord and see him as he is. 2. There was a veil
made for the door of the tabernacle, v. 37, 38. At this door the people
assembled, though forbidden to enter; for, while we are in this present
state, we must get as near to God as we can.
