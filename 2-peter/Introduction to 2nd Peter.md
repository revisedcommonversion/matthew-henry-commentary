Introduction to 2nd Peter
=========================

The penman of this epistle appears plainly to be the same who wrote the
foregoing; and, whatever difference some learned men apprehend they
discern in the style of this epistle from that of the former, this
cannot be a sufficient argument to assert that it was written by Simon
who succeeded the apostle James in the church at Jerusalem, inasmuch as
he who wrote this epistle calls himself Simon Peter, and an apostle (v.
1), and says that he was one of the three apostles that were present at
Christ\'s transfiguration (v. 18), and says expressly that he had
written a former epistle to them, 3:1. The design of this second epistle
is the same with that of the former, as is evident from the first verse
of the third chapter, whence observe that, in the things of God, we have
need of precept upon precept, and line upon line, and all little enough
to keep them in remembrance; and yet these are the things which should
be most faithfully recorded and frequently remembered by us.
