1st Kings, Chapter 8
====================

Commentary
----------

The building and furniture of the temple were very glorious, but the
dedication of it exceeds in glory as much as prayer and praise, the work
of saints, exceed the casting of metal and the graving of stones, the
work of the craftsman. The temple was designed for the keeping up of the
correspondence between God and his people; and here we have an account
of the solemnity of their first meeting there. `I.` The representatives of
all Israel were called together (v. 1, 2), to keep a feast to the honour
of God, for fourteen days (v. 65). `II.` The priests brought the ark into
the most holy place, and fixed it there (v. 3-9). `III.` God took
possession of it by a cloud (v. 10, 11). `IV.` Solomon, with thankful
acknowledgments to God, informed the people touching the occasion of
their meeting (v. 12-21). `V.` In a long prayer he recommended to God\'s
gracious acceptance all the prayers that should be made in or towards
this place (v. 22-53). `VI.` He dismissed the assembly with a blessing and
an exhortation (v. 54-61). `VII.` He offered abundance of sacrifices, on
which he and his people feasted, and so parted, with great satisfaction
(v. 62-66). These were Israel\'s golden days, days of the Son of man in
type.

### Verses 1-11

The temple, though richly beautified, yet while it was without the ark
was like a body without a soul, or a candlestick without a candle, or
(to speak more properly) a house without an inhabitant. All the cost and
pains bestowed on this stately structure are lost if God do not accept
them; and, unless he please to own it as the place where he will record
his name, it is after all but a ruinous heap. When therefore all the
work is ended (ch. 7:51), the one thing needful is yet behind, and that
is the bringing in of the ark. This therefore is the end which must
crown the work, and which here we have an account of the doing of with
great solemnity.

`I.` Solomon presides in this service, as David did in the bringing up of
the ark to Jerusalem; and neither of them thought it below him to follow
the ark nor to lead the people in their attendance on it. Solomon
glories in the title of the preacher (Eccl. 1:1), and the master of
assemblies, Eccl. 12:11. This great assembly he summons (v. 1), and he
is the centre of it, for to him they all assembled (v. 2) at the feast
in the seventh month, namely, the feast of tabernacles, which was
appointed on the fifteenth day of that month, Lev. 23:34. David, like a
very good man, brings the ark to a convenient place, near him; Solomon,
like a very great man, brings it to a magnificent place. As every man
has received the gift, so let him minister; and let children proceed in
God\'s service where their parents left off.

`II.` All Israel attend the service, their judges and the chief of their
tribes and families, all their officers, civil and military, and (as
they speak in the north) the heads of their clans. A convention of these
might well be called an assembly of all Israel. These came together, on
this occasion, 1. To do honour to Solomon, and to return him the thanks
of the nation for all the good offices he had done in kindness to them.
2. To do honour to the ark, to pay respect to it, and testify their
universal joy and satisfaction in its settlement. The advancement of the
ark in external splendour, though it has often proved too strong a
temptation to its hypocritical followers, yet, because it may prove an
advantage to its true interests, is to be rejoiced in (with trembling)
by all that wish well to it. Public mercies call for public
acknowledgments. Those that appeared before the Lord did not appear
empty, for they all sacrificed sheep and oxen innumerable, v. 5. The
people in Solomon\'s time were very rich, very easy, and very cheerful,
and therefore it was fit that, on this occasion, they should consecrate
not only their cheerfulness, but a part of their wealth, to God and his
honour.

`III.` The priests do their part of the service. In the wilderness, the
Levites were to carry the ark, because then there were not priests
enough to do it; but here (it being the last time that the ark was to be
carried) the priests themselves did it, as they were ordered to do when
it surrounded Jericho. We are here told, 1. What was in the ark, nothing
but the two tables of stone (v. 9), a treasure far exceeding all the
dedicated things both of David and Solomon. The pot of manna and
Aaron\'s rod were by the ark, but not in it. 2. What was brought up with
the ark (v. 4): The tabernacle of the congregation. It is probable that
both that which Moses set up in the wilderness, which was in Gibeon, and
that which David pitched in Zion, were brought to the temple, to which
they did, as it were, surrender all their holiness, merging it in that
of the temple, which must henceforward be the place where God must be
sought unto. Thus will all the church\'s holy things on earth, that are
so much its joy and glory, be swallowed up in the perfection of holiness
above. 3. Where it was fixed in its place, the place appointed for its
rest after all its wanderings (v. 6): In the oracle of the house, whence
they expected God to speak to them, even in the most holy place, which
was made so by the presence of the ark, under the wings of the great
cherubim which Solomon set up (ch. 6:27), signifying the special
protection of angels, under which God\'s ordinances and the assemblies
of his people are taken. The staves of the ark were drawn out, so as to
be seen from under the wings of the cherubim, to direct the high priest
to the mercy-seat, over the ark, when he went in, once a year, to
sprinkle the blood there; so that still they continued of some use,
though there was no longer occasion for them to carry it by.

`IV.` God graciously owns what is done and testifies his acceptance of
it, v. 10, 11. The priests might come into the most holy place till God
manifested his glory there; but, thenceforward, none might, at their
peril, approach the ark, except the high priest, on the day of
atonement. Therefore it was not till the priests had come out of the
oracle that the Shechinah took possession of it, in a cloud, which
filled not only the most holy place, but the temple, so that the priests
who burnt incense at the golden altar could not bear it. By this visible
emanation of the divine glory, 1. God put an honour upon the ark, and
owned it as a token of his presence. The glory of it had been long
diminished and eclipsed by its frequent removes, the meanness of its
lodging, and its being exposed too much to common view; but God will now
show that it is as dear to him as ever, and he will have it looked upon
with as much veneration as it was when Moses first brought it into his
tabernacle. 2. He testified his acceptance of the building and
furnishing of the temple as good service done to his name and his
kingdom among men. 3. He struck an awe upon this great assembly; and, by
what they saw, confirmed their belief of what they read in the books of
Moses concerning the glory of God\'s appearance to their fathers, that
hereby they might be kept close to the service of the God of Israel and
fortified against temptations to idolatry. 4. He showed himself ready to
hear the prayer Solomon was now about to make; and not only so, but took
up his residence in this house, that all his praying people might there
be encouraged to make their applications to him. But the glory of God
appeared in a cloud, a dark cloud, to signify, `(1.)` The darkness of that
dispensation in comparison with the light of the gospel, by which, with
open face, we behold, as in a glass, the glory of the Lord. `(2.)` The
darkness of our present state in comparison with the vision of God,
which will be the happiness of heaven, where the divine glory is
unveiled. Now we can only say what he is not, but then we shall see him
as he is.

### Verses 12-21

Here, `I.` Solomon encourages the priests, who came out of the temple from
their ministration, much astonished at the dark cloud that overshadowed
them. The disciples of Christ feared when they entered into the cloud,
though it was a bright cloud (Lu. 9:34), so did the priests when they
found themselves wrapped in a thick cloud. To silence their fears, 1. He
reminds them of that which they could not but know, that this was a
token of God\'s presence (v. 12): The Lord said he would dwell in the
thick darkness. It is so far from being a token of his displeasure that
it is an indication of his favour; for he had said, I will appear in a
cloud, Lev. 16:2. Note, Nothing is more effectual to reconcile us to
dark dispensations than to consider what God hath said, and to compare
his word and works together; as Lev. 10:3, This is that which the Lord
hath said. God is light (1 Jn. 1:5), and he dwells in light (1 Tim.
6:16), but he dwells with men in the thick darkness, makes that his
pavilion, because they could not bear the dazzling brightness of his
glory. Verily thou art a God that hidest thyself. Thus our holy faith is
exercised and our holy fear is increased. Where God dwells in light
faith is swallowed up in vision and fear in love. 2. He himself bids it
welcome, as worthy of all acceptation; and since God, by this cloud,
came down to take possession, he does, in a few words, solemnly give him
possession (v. 13): \"Surely I come,\" says God. \"Amen,\" says Solomon,
\"Even so, come, Lord,. The house is thy own, entirely thy own, I have
surely built it for thee, and furnished it for thee; it is for ever thy
own, a settled place for thee to abide in for ever; it shall never be
alienated nor converted to any other use; the ark shall never be removed
from it, never unsettled again.\" It is Solomon\'s joy that God has
taken possession; and it is his desire that he would keep possession.
Let not the priests therefore dread that in which Solomon so much
triumphs.

`II.` He instructs the people, and gives them a plain account concerning
this house, which they now saw God take possession of. He spoke briefly
to the priests, to satisfy them (a word to the wise), but turned his
face about (v. 14) from them to the congregation that stood in the outer
court, and addressed himself to them largely.

`1.` He blessed them. When they saw the dark cloud enter the temple they
blessed themselves, being astonished at it and afraid lest the thick
darkness should be utter darkness to them. The amazing sight, such as
they had never seen in their days, we may suppose, drove every man to
his prayers, and the vainest minds were made serious by it. Solomon
therefore set in with their prayers, and blessed them all, as one having
authority (for the less is blessed of the better); in God\'s name, he
spoke peace to them, and a blessing, like that with which the angel
blessed Gideon when he was in a fright, upon a similar occasion. Jdg.
6:22, 23, Peace be unto thee. Fear not; thou shalt not die. Solomon
blessed them, that is, he pacified them, and freed them from the
consternation they were in. To receive this blessing, they all stood up,
in token of reverence and readiness to hear and accept it. It is a
proper posture to be in when the blessing is pronounced.

`2.` He informed them concerning this house which he had built and was
now dedicating.

`(1.)` He began his account with a thankful acknowledgment of the good
hand of his God upon him hitherto: Blessed be the Lord God of Israel, v.
15. What we have the pleasure of God must have the praise of. He thus
engaged the congregation to lift up their hearts in thanksgivings to
God, which would help to still the tumult of spirit which, probably,
they were in. \"Come,\" says he, \"let God\'s awful appearances not
drive us from him, but draw us to him; let us bless the Lord God of
Israel.\" Thus Job, under a dark scene, blessed the name of the Lord.
Solomon here blessed God, `[1.]` For his promise which he spoke with his
mouth to David. `[2.]` For the performance, that he had now fulfilled it
with his hand. We have then the best sense of God\'s mercies, and most
grateful both to ourselves and to our God, when we run up those streams
to the fountain of the covenant, and compare what God does with what he
has said.

`(2.)` Solomon is now making a solemn surrender or dedication of this
house unto God, delivering it to God by his own act and deed. Grants and
conveyances commonly begin with recitals of what has been before done,
leading to what is now done: accordingly, here is a recital of the
special causes and considerations moving Solomon to build this house.
`[1.]` He recites the want of such a place. It was necessary that this
should be premised; for, according to the dispensation they were under,
there must be but one place in which they must expect God to record his
name. If, therefore, there were any other chosen, this would be a
usurpation. But he shows, from what God himself had said, that there was
no other (v. 16): I chose no city to build a house in for my name;
therefore there is occasion for the building of this. `[2.]` He recites
David\'s purpose to build such a place. God chose the person first that
should rule his people (I chose David, v. 16) and then put it into his
heart to build a house for God\'s name, v. 17. It was not a project of
his own, for the magnifying of himself; but his good father, of blessed
memory, laid the first design of it, though he lived not to lay the
first stone. `[3.]` He recites God\'s promise concerning himself. God
approved his father\'s purpose (v. 18): Thou didst well, that it was in
thy heart. Note, Sincere intentions to do good shall be graciously
approved and accepted of God, though Providence prevent our putting them
in execution. The desire of a man is his kindness. See 2 Co. 8:12. God
accepted David\'s good will, yet would not permit him to do the good
work, but reserved the honour of it for his son (v. 19): He shall build
the house to my name; so that what he had done was not of his own head,
nor for his own glory, but the work itself was according to his
father\'s design and his doing it was according to God\'s designation.
`[4.]` He recites what he himself had done, and with what intention: I
have built a house, not for my own name, but for the name of the Lord
God of Israel (v. 20), and set there a place for the ark, v. 21. Thus
all the right, title, interest, claim, and demand, whatsoever, which he
or his had or might have in or to this house, or any of its
appurtenances, he resigns, surrenders, and gives up, to God for ever. It
is for his name, and his ark. In this, says he, the Lord hath performed
his word that he spoke. Note, Whatever good we do, we must look upon it
as the performance of God\'s promise to us, rather than the performance
of our promises to him. The more we do for God the more we are indebted
to him; for our sufficiency is of him, and not of ourselves.

### Verses 22-53

Solomon having made a general surrender of this house to God, which God
had signified his acceptance of by taking possession, next follows
Solomon\'s prayer, in which he makes a more particular declaration of
the uses of that surrender, with all humility and reverence, desiring
that God would agree thereto. In short, it is his request that this
temple may be deemed and taken, not only for a house of sacrifice (no
mention is made of that in all this prayer, that was taken for granted),
but a house of prayer for all people; and herein it was a type of the
gospel church; see Isa. 56:7, compared with Mt. 21:13. Therefore Solomon
opened this house, not only with an extraordinary sacrifice, but with an
extraordinary prayer.

`I.` The person that prayed this prayer was great. Solomon did not appoint
one of the priests to do it, nor one of the prophets, but did it
himself, in the presence of all the congregation of Israel, v. 22. 1. It
was well that he was able to do it, a sign that he had made a good
improvement of the pious education which his parents gave him. With all
his learning, it seems, he learnt to pray well, and knew how to express
himself to God in a suitable manner, pro re nata-on the spur of the
occasion, without a prescribed form. In the crowd of his philosophical
transactions, his proverbs, and songs, he did not forget his devotions.
He was a gainer by prayer (ch. 3:11, etc.), and, we may suppose, gave
himself much to it, so that he excelled, as we find here, in praying
gifts. 2. It was well that he was willing to do it, and not shy of
performing divine service before so great a congregation. He was far
from thinking it any disparagement to him to be his own chaplain and the
mouth of the assembly to God; and shall any think themselves too great
to do this office for their own families? Solomon, in all his other
glory, even on his ivory throne, looked not so great as he did now.
Great men should thus support the reputation of religious exercises and
so honour God with their greatness. Solomon was herein a type of Christ,
the great intercessor for all over whom he rules.

`II.` The posture in which he prayed was very reverent, and expressive of
humility, seriousness, and fervency in prayer. He stood before the altar
of the Lord, intimating that he expected the success of his prayer in
virtue of that sacrifice which should be offered up in the fulness of
time, typified by the sacrifices offered at that altar. But when he
addressed himself to prayer, 1. He kneeled down, as appears, v. 54,
where he is said to rise from his knees; compare 2 Chr. 6:13. Kneeling
is the most proper posture for prayer, Eph. 3:14. The greatest of men
must not think it below them to kneel before the Lord their Maker. Mr.
Herbert says, \"Kneeling never spoiled silk stocking.\" 2. He spread
forth his hands towards heaven, and (as it should seem by v. 54)
continued so to the end of the prayer, hereby expressing his desire
towards, and expectations from, God, as a Father in heaven. He spread
forth his hands, as it were to offer up the prayer from an open enlarged
heart and to present it to heaven, and also to receive thence, with both
arms, the mercy which he prayed for. Such outward expressions of the
fixedness and fervour of devotion ought not to be despised or ridiculed.

`III.` The prayer itself was very long, and perhaps much longer than is
here recorded. At the throne of grace we have liberty of speech, and
should use our liberty. It is not making long prayers, but making them
for a pretence, that Christ condemns. In this excellent prayer Solomon
does, as we should in every prayer,

`1.` Give glory to God. This he begins with, as the most proper act of
adoration. He addresses himself to God as the Lord God of Israel, a God
in covenant with them And, `(1.)` He gives him the praise of what he is,
in general, the best of beings in himself (\"There is no God like thee,
none of the powers in heaven or earth to be compared with thee\"), and
the best of masters to his people: \"Who keepest covenant and mercy with
thy servants; not only as good as thy word in keeping covenant, but
better than thy word in keeping mercy, doing that for them of which thou
hast not given them an express promise, provided they walk before thee
with all their heart, are zealous for thee, with an eye to thee.\" `(2.)`
He gives him thanks for what he had done, in particular, for his family
(v. 24): \"Thou hast kept with thy servant David, as with thy other
servants, that which thou promisedst him.\" The promise was a great
favour to him, his support and joy, and now performance is the crown of
it: Thou hast fulfilled it, as it is this day. Fresh experiences of the
truth of God\'s promises call for enlarged praises.

`2.` He sues for grace and favour from God.

`(1.)` That God would perform to him and his the mercy which he had
promised, v. 25, 26. Observe how this comes in. He thankfully
acknowledges the performance of the promise in part; hitherto God had
been faithful to his word: \"Thou hast kept with thy servant David that
which thou promisedst him, so far that his son fills his throne and has
built the intended temple; therefore now keep with thy servant David
that which thou hast further promised him, and which yet remains to be
fulfilled in its season.\" Note, The experiences we have had of God\'s
performing his promises should encourage us to depend upon them and
plead them with God: and those who expect further mercies must be
thankful for former mercies. Hitherto God has helped, 2 Co. 1:10.
Solomon repeats the promise (v. 25): There shall not fail thee a man to
sit on the throne, not omitting the condition, so that thy children take
heed to their way; for we cannot expect God\'s performance of the
promise but upon our performance of the condition. And then he humbly
begs this entail (v. 26): Now, O God of Israel! let thy word be
verified. God\'s promises (as we have often observed) must be both the
guide of our desires and the ground of our hopes and expectations in
prayer. David had prayed (2 Sa. 7:25): Lord, do as thou hast said. Note,
Children should learn of their godly parents how to pray, and plead in
prayer.

`(2.)` That God would have respect to this temple which he had now taken
possession of, and that his eyes might be continually open towards it
(v. 29), that he would graciously own it, and so put an honour upon it.
To this purpose,

`[1.]` He premises, First, A humble admiration of God\'s gracious
condescension (v. 27): \"But will God indeed dwell on the earth? Can we
imagine that a Being infinitely high, and holy, and happy, will stoop so
low as to let it be said of him that he dwells upon the earth and
blesses the worms of the earth with his presence-the earth, that is
corrupt, and overspread with sin-cursed, and reserved to fire? Lord, how
is it?\" Secondly, A humble acknowledgment of the incapacity of the
house he had built, though very capacious, to contain God: \"The heaven
of heavens cannot contain thee, for no place can include him who is
present in all places; even this house is too little, too mean to be the
residence of him that is infinite in being and glory.\" Note, When we
have done the most we can for God we must acknowledge the infinite
distance and disproportion between us and him, between our services and
his perfections.

`[2.]` This premised, he prays in general, First, That God would
graciously hear and answer the prayer he was now praying, v. 28. It was
a humble prayer (the prayer of thy servant), an earnest prayer (such a
prayer as is a cry), a prayer made in faith (before thee, as the Lord,
and my God): \"Lord, hearken to it, have respect to it, not as the
prayer of Israel\'s king (no man\'s dignity in the world, or titles of
honour, will recommend him to God), but as the prayer of thy servant.\"
Secondly, That God would in like manner hear and answer all the prayers
that should, at any time hereafter, be made in or towards this house
which he had now built, and of which God had said, My name shall be
there (v. 29), his own prayers (Hearken to the prayers which thy servant
shall make), and the prayers of all Israel, and of every particular
Israelite (v. 30): \"Hear it in heaven, that is indeed thy
dwelling-place, of which this is but a figure; and, when thou hearest,
forgive the sin that separates between them and God, even the iniquity
of their holy things.\" a. He supposes that God\'s people will ever be a
prayer people; he resolves to adhere to that duty himself. b. He directs
them to have an eye, in their prayers, to that place where God was
pleased to manifest his glory as he did not any where else on earth.
None but priests might come into that place; but, when they worshipped
in the courts of the temple, it must be with an eye towards it, not as
the object of their worship (that were idolatry), but as an instituted
medium of their worship, helping the weakness of their faith, and
typifying the mediation of Jesus Christ, who is the true temple, to whom
we must have an eye in every thing wherein we have to do with God. Those
that were at a distance looked towards Jerusalem, for the sake of the
temple, even when it was in ruins, Dan. 6:10. c. He begs that God will
hear the prayers, and forgive the sins, of all that look this way in
their prayers. Not as if he thought all the devout prayers offered up to
God by those who had no knowledge of this house, or regard to it, were
therefore rejected; but he desired that the sensible tokens of the
divine presence with which this house was blessed might always give
sensible encouragement and comfort to believing petitioners.

`[3.]` More particularly, he here puts divers cases in which he supposed
application would be made to God by prayer in or towards this house of
prayer.

First, If God were appealed to by an oath for the determining of any
controverted right between man and man, and the oath were taken before
this altar, he prayed that God would, in some way or other, discover the
truth, and judge between the contending parties, v. 31, 32. He prayed
that, in difficult matters, this throne of grace might be a throne of
judgment, from which God would right the injured that believingly
appealed to it, and punish the injurious that presumptuously appealed to
it. It was usual to swear by the temple and altar (Mt. 23:16, 18), which
corruption perhaps took its rise from this supposition of an oath taken,
not by the temple or altar, but at or near them, for the greater
solemnity.

Secondly, If the people of Israel were groaning under any national
calamity, or any particular Israelite under any personal calamity, he
desired that the prayers they should make in or towards this house might
be heard and answered.

`a.` In case of public judgments, war (v. 33), want of rain (v. 35),
famine, or pestilence (v. 37), and he ends with an et cetera-any plague
or sickness; for no calamity befals other people which may not befal
God\'s Israel. Now he supposes, `(a.)` That the cause of the judgment
would be sin, and nothing else. \"If they be smitten before the enemy,
if there be no rain, it is because they have sinned against thee.\" It
is sin that makes all the mischief. `(b.)` That the consequence of the
judgment would be that they would cry to God, and make supplication to
him in or towards that house. Those that slighted him before would
solicit him then. Lord, in trouble have they visited thee. In their
afflictions they will seek me early and earnestly. `(c.)` That the
condition of the removal of the judgment was something more than barely
praying for it. He could not, he would not, ask that their prayer might
be answered unless they did also turn from their sin (v. 35) and turn
again to God (v. 33), that is, unless they did truly repent and reform.
On no other terms may we look for salvation in this world or the other.
But, if they did thus qualify themselves for mercy, he prays, `[a.]`
That God would hear from heaven, his holy temple above, to which they
must look, through this temple. `[b.]` That he would forgive their sin;
for then only are judgments removed in mercy when sin is pardoned.
`[c.]` That he would teach them the good way wherein they should walk,
by his Spirit, with his word and prophets; and thus they might be both
profited by their trouble (for blessed is the man whom God chastens and
teaches), and prepared for deliverance, which then comes in love when it
finds us brought back to the good way of God and duty. `[d.]` That he
would then remove the judgment, and redress the grievance, whatever it
might be-not only accept the prayer, but give in the mercy prayed for.

`b.` In case of personal afflictions, v. 38-40. \"If any man of Israel
has an errand to thee, here let him find thee, here let him find favour
with thee.\" He does not mention particulars, so numerous, so various,
are the grievances of the children of men. `(a.)` He supposes that the
complainants themselves would very sensibly feel their own burden, and
would open that case to God which otherwise they kept to themselves and
did not make any man acquainted with: They shall know every man the
plague of his own heart, what it is that pains him, and (as we say)
where the shoe pinches, and shall spread their hands, that is, spread
their case, as Hezekiah spread the letter, in prayer, towards this
house; whether the trouble be of body or mind, they shall represent it
before God. Inward burdens seem especially meant. Sin is the plague of
our own heart; our indwelling corruptions are our spiritual diseases.
Every Israelite indeed endeavours to know these, that he may mortify
them and watch against the risings of them. These he complains of. This
is the burden he groans under: O wretched man that I am! These drive him
to his knees, drive him to the sanctuary. Lamenting these, he spreads
forth his hands in prayer. `(b.)` He refers all cases of this kind, that
should be brought hither, to God. `[a.]` To his omniscience: \"Thou,
even thou only, knowest the hearts of all the children of men, not only
the plagues of their hearts, their several wants and burdens\" (these he
knows, but he will know them from us), \"but the desire and intent of
the heart, the sincerity or hypocrisy of it. Thou knowest which prayer
comes from the heart, and which from the lips only.\" The hearts of
kings are not unsearchable to God. `[b.]` To his justice: Give to every
man according to his ways; and he will not fail to do so, by the rules
of grace, not the law, for then we should all be undone. `[c.]` To his
mercy: Hear, and forgive, and do (v. 39), that they may fear thee all
their days, v. 40. This use we should make of the mercy of God to us in
hearing our prayers and forgiving our sins, we should thereby he engaged
to fear him while we live. Fear the Lord and his goodness. There is
forgiveness with him, that he may be feared.

`c.` The case of the stranger that is not an Israelite is next mentioned,
a proselyte that comes to the temple to pray to the God of Israel, being
convinced of the folly and wickedness of worshipping the gods of his
country. `(a.)` He supposed that there would be many such (v. 41, 42),
that the fame of God\'s great works which he had wrought for Israel, by
which he proved himself to be above all gods, nay, to be God alone,
would reach to distant countries: \"Those that live remote shall hear of
thy strong hand, and thy stretched-out arm; and this will bring all
thinking considerate people to pray towards this house, that they may
obtain the favour of a God that is able to do them a real kindness.\"
`(b.)` He begged that God would accept and answer the proselyte\'s prayer
(v. 43): Do according to all that the stranger calleth to thee for. Thus
early, thus ancient, were the indications of favour towards the sinners
of the Gentiles: as there was then one law for the native and for the
stranger (Ex. 12:49), so there was one gospel for both. `(c.)` Herein he
aimed at the glory of God and the propagating of the knowledge of him:
\"O let the stranger, in a special manner, speed well in his addresses,
that he may carry away with him to his own country a good report of the
God of Israel, that all people may know thee and fear thee (and, if they
know thee aright, they will fear thee) as do thy people Israel.\" So far
was Solomon from monopolizing the knowledge and service of God, and
wishing to have them confined to Israel only (which was the envious
desire of the Jews in the days of Christ and his apostles), that he
prayed that all people might fear God as Israel did. Would to God that
all the children of men might receive the adoption, and be made God\'s
children! Father, thus glorify thy name.

`d.` The case of an army going forth to battle is next recommended by
Solomon to the divine favour. It is supposed that the army is encamped
at a distance, somewhere a great way off, sent by divine order against
the enemy, v. 44. \"When they are ready to engage, and consider the
perils and doubtful issues of battle, and put up a prayer to God for
protection and success, with their eye towards this city and temple,
then hear their prayer, encourage their hearts, strengthen their hands,
cover their heads, and so maintain their cause and give them victory.\"
Soldiers in the field must not think it enough that those who tarry at
home pray for them, but must pray for themselves, and they are here
encouraged to hope fore a gracious answer. Praying should always go
along with fighting.

`e.` The case of poor captives is the last that is here mentioned as a
proper object of divine compassion. `(a.)` He supposes that Israel will
sin. He knew them, and himself, and the nature of man, too well to think
this a foreign supposition; for there is no man that sinneth not, that
does not enough to justify God in the severest rebukes of his
providence, no man but what is in danger of falling into gross sin, and
will if God leave him to himself. `(b.)` He supposes, what may well be
expected, that, if Israel revolt from God, God will be angry with them,
and deliver them into the hand of their enemies, to be carried captive
into a strange country, v. 46. `(c.)` He then supposes that they will
bethink themselves, will consider their ways (for afflictions put men
upon consideration), and, when once they are brought to consider, they
will repent and pray, will confess their sins, and humble themselves,
saying, We have sinned and have done perversely (v. 47), and in the land
of their enemies will return to God, whom they had forsaken in their own
land. `(d.)` He supposes that in their prayers they will look towards
their own land, the holy land, Jerusalem, the holy city, and the temple,
the holy house, and directs them so to do (v. 48), for his sake who gave
them that land, chose that city, and to whose honour that house was
built. `(e.)` He prays that then God would hear their prayers, forgive
their sins, plead their cause, and incline their enemies to have
compassion on them, v. 49. 50. God has all hearts in his hand, and can,
when he pleases, turn the strongest stream the contrary way, and make
those to pity his people who have been their most cruel persecutors. See
this prayer answered, Ps. 106:46. He made them to be pitied of those
that carried them captive, which, if it did not release them, yet eased
their captivity. `(f.)` He pleads their relation to God, and his interest
in them: \"They are thy people, whom thou hast taken into thy covenant
and under thy care and conduct, thy inheritance, from which, more than
from any other nation, thy rent and tribute of glory issue and arise (v.
51), separated from among all people to be so and by distinguishing
favours appropriated to thee,\" v. 53.

Lastly, After all these particulars, he concludes with this general
request, that God would hearken to all his praying people in all that
they call unto him for, v. 52. No place now, under the gospel, can be
imagined to add any acceptableness to the prayers made in or towards it,
as the temple then did. That was a shadow: the substance is Christ;
whatever we ask in his name, it shall be given us.

### Verses 54-61

Solomon, after his sermon in Ecclesiastes, gives us the conclusion of
the whole matter; so he does here, after this long prayer; it is called
his blessing the people, v. 55. He pronounced it standing, that he might
be the better heard, and because he blessed as one having authority.
Never were words more fitly spoken, nor more pertinently. Never was
congregation dismissed with that which was more likely to affect them
and abide with them.

`I.` He gives God the glory of the great and kind things he had done for
Israel, v. 56. He stood up to bless the congregation (v. 55), but began
with blessing God; for we must in every thing give thanks. Do we expect
God should do well for us and ours? let us take all occasion to speak
well of him and his. He blesses God who has given, he does not say
wealth, and honour, and power, and victory, to Israel, but rest, as if
that were a blessing more valuable than any of those. Let not those who
have rest under-value that blessing, though they want some others. He
compares the blessings God had bestowed upon them with the promises he
had given them, that God might have the honour of his faithfulness and
the truth of that word of his which he has magnified above all his name.
`1.` He refers to the promises given by the hand of Moses, as he did (v.
15, 24) to those which were made to David. There were promises given by
Moses, as well as precepts. It was long ere God gave Israel the promised
rest, but they had it at last, after many trials. The day will come when
God\'s spiritual Israel will rest from all their labours. 2. He does, as
it were, write a receipt in full on the back of these bonds: There has
not failed one word of all his good promises. This discharge he gives in
the name of all Israel, to the everlasting honour of the divine
faithfulness, and the everlasting encouragement of all those that build
upon the divine promises.

`II.` He blesses himself and the congregation, expressing his earnest
desire and hope of these four things:-1. The presence of God with them,
which is all in all to the happiness of a church and nation and of every
particular person. This great congregation was now shortly to be
scattered, and it was not likely that they would ever be all together
again in this world. Solomon therefore dismisses them with this
blessing: \"The Lord be present with us, and that will be comfort enough
when we are absent from each other. The Lord our God be with us, as he
was with our fathers (v. 57); let him not leave us, let him be to us to
day, and to ours for ever, what he was to those that went before us.\"
2. The power of his grace upon them: \"Let him be with us, and continue
with us, not that he may enlarge our coasts and increase our wealth, but
that he may incline our hearts to himself, to walk in all his ways and
to keep his commandments,\" v. 58. Spiritual blessings are the best
blessings, with which we should covet earnestly to be blessed. Our
hearts are naturally averse to our duty, and apt to decline from God; it
is his grace that inclines them, grace that must be obtained by prayer.
3. An answer to the prayer he had now made: \"Let these my words be nigh
unto the Lord our God day and night, v. 59. Let a gracious return be
made to every prayer that shall be made here, and that will be a
continual answer to this prayer.\" What Solomon asks here for his prayer
is still granted in the intercession of Christ, of which his
supplication was a type; that powerful prevailing intercession is before
the Lord our God day and night, for our great Advocate attends
continually to this very thing, and we may depend upon him to maintain
our cause (against the adversary that accuses us day and night, Rev.
12:10) and the common cause of his people Israel, at all times, upon all
occasions, as the matter shall require, so as to speak for us the word
of the day in its day, as the original here reads it, from which we
shall receive grace sufficient, suitable, and seasonable, in every time
of need. 4. The glorifying of God in the enlargement of his kingdom
among men. Let Israel be thus blessed, thus favoured; not that all
people may become tributaries to us (Solomon sees his kingdom as great
as he desires), but that all people may know that the Lord is God, and
he only, and may come and worship him, v. 60. With this Solomon\'s
prayers, like the prayers of his father David, the son of Jesse, are
ended (Ps. 72:19, 20): Let the whole earth be filled with his glory. We
cannot close our prayers with a better summary than this, Father,
glorify thy name.

`III.` He solemnly charges his people to continue and persevere in their
duty to God. Having spoken to God for them, he here speaks from God to
them, and those only would fare the better for his prayers that were
made better by his preaching. His admonition, at parting, is, \"Let your
heart be perfect with the Lord our God, v. 61. Let your obedience be
universal, without dividing-upright, without dissembling-constant,
without declining;\" this is evangelical perfection.

### Verses 62-66

We read before that Judah and Israel were eating and drinking, and very
cheerful under their own vines and fig-trees; here we have them so in
God\'s courts. Now they found Solomon\'s words true concerning Wisdom\'s
ways, that they are ways of pleasantness.

`I.` They had abundant joy and satisfaction while they attended at God\'s
house, for there, 1. Solomon offered a great sacrifice, 22,000 oxen and
120,000 sheep, enough to have drained the country of cattle if it had
not been a very fruitful land. The heathen thought themselves very
generous when they offered sacrifices by hundreds (hecatombs they called
them), but Solomon out-did them: he offered them by thousands. When
Moses dedicated his altar, the peace-offerings were twenty-four
bullocks, and of rams, goats, and lambs, 180 (Num. 7:88); then the
people were poor, but now that they had increased in wealth more was
expected from them. Where God sows plentifully he must reap accordingly.
All these sacrifices could not be offered in one day, but in the several
days of the feast. Thirty oxen a day served Solomon\'s table, but
thousands shall go to God\'s altar. Few are thus minded, to spend more
on their souls than on their bodies. The flesh of the peace-offerings,
which belonged to the offerer, it is likely, Solomon treated the people
with. Christ fed those who attended him. The brazen altar was not large
enough to receive all these sacrifices, so that, to serve the present
occasion, they were forced to offer many of them in the middle of the
court, (v. 64), some think on altars, altars of earth or stone, erected
for the purpose and taken down when the solemnity was over, others think
on the bare ground. Those that will be generous in serving God need not
stint themselves for want of room and occasion to be so. 2. He kept a
feast, the feast of tabernacles, as it should seem, after the feast of
dedication, and both together lasted fourteen days (v. 65), yet they
said not, Behold, what a weariness is this!

`II.` They carried this joy and satisfaction with them to their own
houses. When they were dismissed they blessed the king (v. 66),
applauded him, admired him, and returned him the thanks of the
congregation, and then went to their tents joyful and glad of heart, all
easy and pleased. God\'s goodness was the matter of their joy, so it
should be of ours at all times. They rejoiced in God\'s blessing both on
the royal family and on the kingdom; thus should we go home rejoicing
from holy ordinances, and go on our way rejoicing for God\'s goodness to
our Lord Jesus (of whom David his servant was a type, in the advancement
and establishment of his throne, pursuant to the covenant of
redemption), and to all believers, his spiritual Israel, in their
sanctification and consolation, pursuant to the covenant of grace. If we
rejoice not herein always it is our own fault.
