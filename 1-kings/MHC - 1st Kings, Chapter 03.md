1st Kings, Chapter 3
====================

Commentary
----------

Solomon\'s reign looked bloody in the foregoing chapter, but the
necessary acts of justice must not be called cruelty; in this chapter it
appears with another face. We must not think the worse of God\'s mercy
to his subjects for his judgments on rebels. We have here, `I.` Solomon\'s
marriage to Pharaoh\'s daughter (v. 1). `II.` A general view of his
religion (v. 2-4). `III.` A particular account of his prayer to God for
wisdom, and the answer to that prayer (v. 5-15). `IV.` A particular
instance of his wisdom in deciding the controversy between the two
harlots (v. 16-28). And very great he looks here, both at the altar and
on the bench, and therefore on the bench because at the altar.

### Verses 1-4

We are here told concerning Solomon,

`I.` Something that was unquestionably good, for which he is to be praised
and in which he is to be imitated. 1. He loved the Lord, v. 3.
Particular notice was taken of God\'s love to him, 2 Sa. 12:24. He had
his name from it: Jedidiah-beloved of the Lord. And here we find he
returned that love, as John, the beloved disciple, was most full of
love. Solomon was a wise man, a rich man; yet the brightest encomium of
him is that which is the character of all the saints, even the poorest,
He loved the Lord, so the Chaldee; all that love God love his worship,
love to hear from him and speak to him, and so to have communion with
him. 2. He walked in the statutes of David his father, that is, in the
statutes that David gave him, ch. 2:2, 3; 1 Chr. 28:9, 10 (his dying
father\'s charge was sacred, and as a law to him), or in God\'s
statutes, which David his father walked in before him; he kept close to
God\'s ordinances, carefully observed them and diligently attended them.
Those that truly love God will make conscience of walking in his
statutes. 3. He was very free and generous in what he did for the honour
of God. When he offered sacrifice he offered like a king, in some
proportion to his great wealth, a thousand burnt-offerings, v. 4. Where
God sows plentifully he expects to reap accordingly; and those that
truly love God and his worship will not grudge the expenses of their
religion. We may be tempted to say, To what purpose is this waste? Might
not these cattle have been given to the poor? But we must never think
that wasted which is laid out in the service of God. It seems strange
how so many beasts should be burnt upon one altar in one feast, though
it continued seven days; but the fire on the altar is supposed to be
more quick and devouring than common fire, for it represented that
fierce and mighty wrath of God which fell upon the sacrifices, that the
offerers might escape. Our God is a consuming fire. Bishop Patrick
quotes it as a tradition of the Jews that the smoke of the sacrifices
ascended directly in a straight pillar, and was not scattered, otherwise
it would have choked those that attended, when so many sacrifices were
offered as were here.

`II.` Here is something concerning which it may be doubted whether it was
good or no. 1. His marrying Pharaoh\'s daughter, v. 1. We will suppose
she was proselyted, otherwise the marriage would not have been lawful;
yet, if so, surely it was not advisable. He that loved the Lord should,
for his sake, have fixed his love upon one of the Lord\'s people.
Unequal matches of the sons of God with the daughters of men have often
been of pernicious consequence; yet some think that he did this with the
advice of his friends, that she was a sincere convert (for the gods of
the Egyptians are not reckoned among the strange gods which his strange
wives drew him in to the worship of, ch. 11:5, 6), and that the book of
Canticles and the 45th Psalm were penned on this occasion, by which
these nuptials were made typical of the mystical espousals of the church
to Christ, especially the Gentile church. 2. His worshipping in the high
places, and thereby tempting the people to do so too, v. 2, 3. Abraham
built his altars on mountains (Gen. 12:8; 22:2), and worshipped in a
grove, Gen. 21:33. Thence the custom was derived, and was proper, till
the divine law confined them to one place, Deu. 12:5, 6. David kept to
the ark, and did not care for the high places, but Solomon, though in
other things he walked in the statutes of his father, in this came short
of him. He showed thereby a great zeal for sacrificing, but to obey
would have been better. This was an irregularity. Though there was as
yet no house built, there was a tent pitched, to the name of the Lord,
and the ark ought to have been the centre of their unity. It was so by
divine institution; from it the high places separated; yet while they
worshipped God only, and in other things according to the rule, he
graciously overlooked their weakness, and accepted their services; and
it is owned that Solomon loved the Lord, though he burnt incense in the
high places, and let not men be more severe than God is.

### Verses 5-15

We have here an account of a gracious visit which God paid to Solomon,
and the communion he had with God in it, which put a greater honour upon
Solomon than all the wealth and power of his kingdom did.

`I.` The circumstances of this visit, v. 5. 1. The place. It was in
Gibeon; that was the great high place, and should have been the only
one, because there the tabernacle and the brazen altar were, 2 Chr. 1:3.
There Solomon offered his great sacrifices, and there God owned him more
than in any other of the high places. The nearer we come to the rule in
our worship the more reason we have to expect the tokens of God\'s
presence. Where God records his name, there he will meet us and bless
us. 2. The time. It was by night, the night after he had offered that
generous sacrifice, v. 4. The more we abound in God\'s work the more
comfort we may expect in him; if the day has been busy for him, the
night will be easy in him. Silence and retirement befriend our communion
with God. His kindest visits are often in the night, Ps. 17:3. 3. The
manner. It was in a dream, when he was asleep, his senses locked up,
that God\'s access to his mind might be the more free and immediate. In
this way God used to speak to the prophets (Num. 12:6) and to private
persons, for their own benefit, Job 33:15, 16. These divine dreams, no
doubt, were plainly distinguishable from those in which there are divers
vanities, Eccl. 5:7.

`II.` The gracious offer God made him of the favour he should choose,
whatever it might be, v. 5. He saw the glory of God shine about him, and
heard a voice saying, Ask what I shall give thee. Not that God was
indebted to him for his sacrifices, but thus he would testify his
acceptance of them, and signify to him what great mercy he had in store
for him, if he were not wanting to himself. Thus he would try his
inclinations and put an honour upon the prayer of faith. God, in like
manner, condescends to us, and puts us in the ready way to be happy by
assuring us that we shall have what we will for the asking, Jn. 16:23; 1
Jn. 5:14. What would we more? Ask, and it shall be given you.

`III.` The pious request Solomon hereupon made to God. He readily laid
hold of this offer. Why do we neglect the like offer made to us, like
Ahaz, who said, I will not ask? Isa. 7:12. Solomon prayed in his sleep,
God\'s grace assisting him; yet it was a lively prayer. What we are most
in care about, and which makes the greatest impression upon us when we
are awake, commonly affects us when we are asleep; and by our dreams,
sometimes, we may know what our hearts are upon and how our pulse beats.
Plutarch makes virtuous dreams one evidence of increase in virtue. Yet
this must be attributed to a higher source. Solomon\'s making such an
intelligent choice as this when he was asleep, and the powers of reason
were least active, showed that it came purely from the grace of God,
which wrought in him these gracious desires. If his reins thus instruct
him in the night season, he must bless the Lord who gave him counsel,
Ps. 16:7. Now, in this prayer,

`1.` He acknowledges God\'s great goodness to his father David, v. 6. He
speaks honourably of his father\'s piety, that he had walked before God
in uprightness of heart, drawing a veil over his faults. It is to be
hoped that those who praise their godly parents will imitate them. But
he speaks more honourably of God\'s goodness to his father, the mercy he
had shown to him while he lived, in giving him to be sincerely religious
and then recompensing his sincerity and the great kindness he had kept
for him, to be bestowed on the family when he was gone, in giving him a
son to sit on his throne. Children should give God thanks for his
mercies to their parents, for the sure mercies of David. God\'s favours
are doubly sweet when we observe them transmitted to us through the
hands of those that have gone before us. The way to get the entail
perpetuated is to bless God that it has hitherto been preserved.

`2.` He owns his own insufficiency for the discharge of that great trust
to which he is called, v. 7, 8. And here is a double plea to enforce his
petition for wisdom:-`(1.)` That his place required it, as he was
successor to David (\"Thou hast made me king instead of David, who was a
very wise and good man: Lord, give me wisdom, that I may keep up what he
wrought, and carry on what he began\") and as he was ruler over Israel:
\"Lord, give me wisdom to rule well; for they are a numerous people,
that will not be managed without much care, and they are thy people,
whom thou hast chosen, and therefore to be ruled for thee, and the more
wisely they are ruled the more glory thou wilt have from them.\" `(2.)`
That he wanted it. As one that had a humble sense of his own deficiency,
he pleads, \"Lord, I am but a little child (so he calls himself, a child
in understanding, though his father called him a wise man, ch. 2:9); I
know not how to go out or come in as I should, nor to do so much as the
common daily business of the government, much less what to do in a
critical juncture.\" Note, Those who are employed in public stations
ought to be very sensible of the weight and importance of their work and
their own insufficiency for it, and then they are qualified for
receiving divine instruction. Paul\'s question (Who is sufficient for
these things?) is much like Solomon\'s here, Who is able to judge this
thy so great a people? v. 9. Absalom, who was a wise man, trembles at
the undertaking and suspects his own fitness for it. The more knowing
and considerate men are the better acquainted they are with their own
weakness and the more jealous of themselves.

`3.` He begs of God to give him wisdom (v. 9); Give therefore thy servant
an understanding heart. He calls himself God\'s servant, pleased with
that relation to God (Ps. 116:16) and pleading it with him: \"I am
devoted to thee, and employed for thee; give me that which is requisite
to the services in which I am employed.\" Thus his good father prayed,
and thus he pleaded. Ps. 119:125, I am thy servant, give me
understanding. An understanding heart is God\'s gift, Prov. 2:6. We must
pray for it (James 1:5), and pray for it with application to our
particular calling and the various occasions we have for it; as Solomon,
Give me an understanding, not to please my own curiosity with, or puzzle
my neighbours, but to judge thy people. That is the best knowledge which
will be serviceable to us in doing our duty; and such that knowledge is
which enables us to discern between good and bad, right and wrong, sin
and duty, truth and falsehood, so as not to be imposed upon by false
colours in judging either of others\' actions or of our own.

`4.` The favourable answer God gave to his request. It was a pleasing
prayer (v. 10): The speech pleased the Lord. God is well pleased with
his own work in his people, the desires of his own kindling, the prayers
of his Spirit\'s inditing. By this choice Solomon made it appear that he
desired to be good more than great, and to serve God\'s honour more than
to advance his own. Those are accepted of God who prefer spiritual
blessings to temporal, and are more solicitous to be found in the way of
their duty than in the way to preferment. But that was not all; it was a
prevailing prayer, and prevailed for more than he asked. `(1.)` God gave
him wisdom, v. 12. He fitted him for all that great work to which he had
called him, gave him such a right understanding of the law which he was
to judge by, and the cases he was to judge of, that he was unequalled
for a clear head, a solid judgment, and a piercing eye. Such an insight,
and such a foresight, never was prince so blessed with. `(2.)` He gave him
riches and honour over and above into the bargain (v. 13), and it was
promised that in these he should as much exceed his predecessors, his
successors, and all his neighbours, as in wisdom. These also are God\'s
gift, and, as far as is good for them, are promised to all that seek
first the kingdom of God and the righteousness thereof, Mt. 6:33. Let
young people learn to prefer grace to gold in all that they choose,
because godliness has the promise of the life that now is, but the life
that now is has not the promise of godliness. How completely blessed was
Solomon, that had both wisdom and wealth! He that has wealth and power
without wisdom and grace is in danger of doing hurt with them; he that
has wisdom and grace without wealth and power is not capable of doing so
much good with them as he that has both. Wisdom is good, is so much the
better, with an inheritance, Eccles. 7:11. But, if we make sure of
wisdom and grace, these will either bring outward prosperity with them
or sweeten the want of it. God promised Solomon riches and honour
absolutely, but long life upon condition (v. 14). If thou wilt walk in
my ways, as David did, then I will lengthen thy days. He failed in the
condition; and therefore, though he had riches and honour, he did not
live so long to enjoy them as in the course of nature he might have
done. Length of days is wisdom\'s right-hand blessing, typical of
eternal life; but it is in her left hand that riches and honour are,
Prov. 3:16. Let us see here, `[1.]` That the way to obtain spiritual
blessings is to be importunate for them, to wrestle with God in prayer
for them, as Solomon did for wisdom, asking that only, as the one thing
needful. `[2.]` That the way to obtain temporal blessings is to be
indifferent to them and to refer ourselves to God concerning them.
Solomon had wisdom given him because he did ask it and wealth because he
did not ask it.

`5.` The grateful return Solomon made for the visit God was pleased to
pay him, v. 15. He awoke, we may suppose in a transport of joy, awoke,
and his sleep was sweet to him, as the prophet speaks (Jer. 31:26);
being satisfied of God\'s favour, he was satisfied with it, and he began
to think what he should render to the Lord. He had made his prayer at
the high place at Gibeon, and there God had graciously met him; but he
comes to Jerusalem to give thanks before the ark of the covenant,
blaming himself, as it were, that he had not prayed there, the ark being
the token of God\'s presence, and wondering that God had met him any
where else. God\'s passing by our mistakes should persuade us to amend
them. There he, `(1.)` Offered a great sacrifice to God. We must give God
praise for his gifts in the promise, though not yet fully performed.
David used to praise God\'s word, as well as his works (Ps. 56:10, and
particularly, 2 Sa. 7:18), and Solomon trod in his steps. `(2.)` He made a
great feast upon the sacrifice, that those about him might rejoice with
him in the grace of God.

### Verses 16-28

An instance is here given of Solomon\'s wisdom, to show that the grant
lately made him had a real effect upon him. The proof is fetched, not
from the mysteries of state and the policies of the council-board,
though there no doubt he excelled, but from the trial and determination
of a cause between party and party, which princes, though they devolve
them upon their judges, must not think it below them to take cognizance
of. Observe,

`I.` The case opened, not by lawyers, but by the parties themselves,
though they were women, which made it the easier to such a piercing eye
as Solomon had to discern between right and wrong by their own showing.
These two women were harlots, kept a public house, and their children,
some think, were born of fornication, because here is no mention of
their husbands. It is probable the cause had been heard in the inferior
courts, before it was brought before Solomon, and had been found
special, the judges being unable to determine it, that Solomon\'s wisdom
in deciding it at last might be the more taken notice of. These two
women, who lived in a house together, were each of them delivered of a
son within three days of one another, v. 17, 18. They were so poor that
they had no servant or nurse to be with them, so slighted, because
harlots, that they had no friend or relation to accompany them. One of
them overlaid her child, and, in the night, exchanged it with the other
(v. 19, 20), who was soon aware of the cheat put upon her, and appealed
to public justice to be righted, v. 21. See, 1. What anxiety is caused
by little children, how uncertain their lives are, and to how many
dangers they are continually exposed. The age of infancy is the valley
of the shadow of death; and the lamp of life, when first lighted, is
easily blown out. It is a wonder of mercy that so few perish in the
perils of nursing. 2. How much better it was in those times with
children born in fornication than commonly it is now. harlots then loved
their children, nursed them, and were loth to part with them; whereas
now they are often sent to a distance, abandoned, or killed. But thus is
was foretold that in the last days perilous times should come, when
people should be without natural affection, 2 Tim. 3:1, 3.

`II.` The difficulty of the case. The question was, Who was the mother of
this living child, which was brought into court, to be finally adjudged
either to the one or to the other? Both mothers were vehement in their
claim, and showed a deep concern about it. Both were peremptory in their
asseverations: \"It is mine,\" says one. \"Nay, it is mine,\" says the
other. Neither will own the dead child, though it would be cheaper to
bury that than to maintain the other: but it is the living one they
strive for. The living child is therefore the parent\'s joy because it
is their hope; and may not the dead children be so? See Jer. 31:17. Now
the difficulty of the case was that there was no evidence on either
side. The neighbours, though it is probable that some of them were
present at the birth and circumcision of the children, yet had not taken
so much notice of them as to be able to distinguish them. To put the
parties to the rack would have been barbarous; not she who had justice
on her side, but she who was most hardy, would have had the judgment in
her favour. Little stress is to be laid on extorted evidence. Judges and
juries have need of wisdom to find out truth when it thus lies hid.

`III.` The determination of it. Solomon, having patiently heard what both
sides had to say, sums up the evidence, v. 23. And now the whole court
is in expectation what course Solomon\'s wisdom will take to find out
the truth. One knows not what to say to it; another, perhaps, would
determine it by lot. Solomon calls for a sword, and gives orders to
divide the living child between the two contenders. Now, 1. This seemed
a ridiculous decision of the case, and a brutal cutting of the knot
which he could not untie. \"Is this,\" think the sages of the law, \"the
wisdom of Solomon?\" little dreaming what he aimed at in it. The hearts
of kings, such kings, are unsearchable, Prov. 25:3. There was a law
concerning the dividing of a living ox and a dead one. (Ex. 21:35), but
that did not reach this case. But, 2. It proved an effectual discovery
of the truth. Some think that Solomon did himself discern it, before he
made this experiment, by the countenances of the women and their way of
speaking: but by this he gave satisfaction to all the company, and
silenced the pretender. To find out the true mother, he could not try
which the child loved best, and must therefore try which loved the child
best; both pretended to a motherly affection, but their sincerity will
be tried when the child is in danger. `(1.)` She that knew the child was
not her own, but in contending for it stood upon a point of honour, was
well content to have it divided. She that had overlaid her own child
cared not what became of this, so that the true mother might not have
it: Let it be neither mine nor thine, but divide it. By this it appeared
that she knew her own title to be bad, and feared Solomon would find it
so, though she little suspected she was betraying herself, but thought
Solomon in good earnest. If she had been the true mother she would not
have forfeited her interest in the child by agreeing so readily to this
bloody decision. But, `(2.)` She that knew the child was her own, rather
than the child should be butchered, gives it up to her adversary. How
feelingly does she cry out, O, my lord! give her the living child, v.
26. \"Let me see it hers, rather than not see it at all.\" By this
tenderness towards the child it appeared that she was not the careless
mother that had overlaid the dead child, but was the true mother of the
living one, that could not endure to see its death, having compassion on
the son of her womb. \"The case is plain,\" says Solomon; \"what need of
witnesses? Give her the living child; for you all see, by this
undissembled compassion, she is the mother of it.\" Let parents show
their love to their children by taking care of them, especially by
taking care of their souls, and, with a holy violence, snatching them as
brands out of the burning. Those are most likely to have the comfort of
children that do their duty to them. Satan pretends to the heart of man,
but by this it appears that he is only a pretender, that he would be
content to divide with God, whereas the rightful sovereign of the heart
will have all or none.

`IV.` We are told what a great reputation Solomon got among his people by
this and other instances of his wisdom, which would have a great
influence upon the ease of his government: They feared the king (v. 28),
highly reverenced him, durst not in any thing oppose him, and were
afraid of doing an unjust thing; for they knew, if ever it came before
him, he would certainly discover it, for they saw that the wisdom of God
was in him, that is, that wisdom with which God had promised to endue
him. This made his face to shine, Eccl. 8:1. This strengthened him,
Eccl. 7:19. This was better to him than weapons of war, Eccl. 9:18. For
this he was both feared and loved.
