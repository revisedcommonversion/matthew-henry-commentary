1st Kings, Chapter 6
====================

Commentary
----------

Great and long preparation had been making for the building of the
temple, and here, at length, comes an account of the building of it; a
noble piece of work it was, one of the wonders of the world, and taking
in its spiritual significancy, one of the glories of the church. Here
is, `I.` The time when it was built (v. 1), and how long it was in the
building (v. 37, 38). `II.` The silence with which it was build (v. 7).
III. The dimensions of it (v. 2, 3). `IV.` The message God sent to
Solomon, when it was in the building (v. 11-13). `V.` The particulars:
windows (v. 4), chambers (v. 5, 6, 8-10), the walls and flooring (v.
15-18), the oracle (v. 19-22), the cherubim (v. 23-30), the doors (v.
31-35), and the inner court (v. 36). Many learned men have well bestowed
their pains in expounding the description here given of the temple
according to the rules of architecture, and solving the difficulties
which, upon search, they find in it; but in that matter, having nothing
new to offer, we will not be particular or curious; it was then well
understood, and every man\'s eyes that saw this glorious structure
furnished him with the best critical exposition of this chapter.

### Verses 1-10

Here, `I.` The temple is called the house of the Lord (v. 1), because it
was, 1. Directed and modelled by him. Infinite Wisdom was the architect,
and gave David the plan or pattern by the Spirit, not by word of mouth
only, but, for the greater certainty and exactness, in writing (1 Chr.
28:11, 12), as he had given to Moses in the mouth a draught of the
tabernacle. 2. Dedicated and devoted to him and to his honour, to be
employed in his service, so his as never any other house was, for he
manifested his glory in it (so as never in any other) in a way agreeable
to that dispensation; for, when there were carnal ordinances, there was
a worldly sanctuary, Heb. 9:1, 10. This gave it its beauty of holiness,
that it was the house of the Lord, which far transcended all its other
beauties.

`II.` The time when it began to be built is exactly set down. 1. It was
just 480 years after the bringing of the children of Israel out of
Egypt. Allowing forty years to Moses, seventeen to Joshua, 299 to the
Judges, forty to Eli, forty to Samuel and Saul, forty to David, and four
to Solomon before he began the work, we have just the sum of 480. So
long it was after that holy state was founded before that holy house was
built, which, in less than 430 years, was burnt by Nebuchadnezzar. It
was thus deferred because Israel had, by their sins, rendered themselves
unworthy of this honour, and because God would show how little he values
external pomp and splendour in his service: he was in no haste for a
temple. David\'s tent, which was clean and convenient, though it was
neither stately nor rich, nor, for aught that appears, ever consecrated,
is called the house of the Lord (2 Sa. 12:20), and served as well as
Solomon\'s temple; yet, when God gave Solomon great wealth, he put it
into his heart thus to employ it, and graciously accepted him, chiefly
because it was to be a shadow of good things to come, Heb. 9:9. 2. It
was in the fourth year of Solomon\'s reign, the first three years being
taken up in settling the affairs of his kingdom, that he might not find
any embarrassment from them in this work. It is not time lost which is
spent in composing ourselves for the work of God, and disentangling
ourselves from every thing which might distract or divert us. During
this time he was adding to the preparations which his father had made (1
Chr. 22:14), hewing the stone, squaring the timber, and getting every
thing ready, so that he is not to be blamed for slackness in deferring
it so long. We are truly serving God when we are preparing for his
service and furnishing ourselves for it.

`III.` The materials are brought in, ready for their place (v. 7), so
ready that there was neither hammer nor ax heard in the house while it
was in building. In all building Solomon prescribes it as a rule of
prudence to prepare the work in the field, and afterwards build, Prov.
24:27. But here, it seems, the preparation was more than ordinarily full
and exact, to such a degree that, when the several parts came to be put
together, there was nothing defective to be added, nothing amiss to be
amended. It was to be the temple of God of peace, and therefore no iron
tool must be heard in it. Quietness and silence both become and befriend
religious exercises: God\'s work should be done with as much care and as
little noise as may be. The temple was thrown down with axes and
hammers, and those that threw it down roared in the midst of the
congregation (Ps. 74:4, 6); but it was built up in silence. Clamour and
violence often hinder the work of God, but never further it.

`IV.` The dimensions are laid down (v. 2, 3) according to the rules of
proportion. Some observe that the length and breadth were just double to
that of the tabernacle. Now that Israel had grown more numerous the
place of their meeting needed to be enlarged (Isa. 54:1, 2), and now
that they had grown richer they were the better able to enlarge it.
Where God sows plentifully he expects to reap so.

`V.` An account of the windows (v. 4): They were broad within, and narrow
without, Marg. Such should the eyes of our mind be, reflecting nearer on
ourselves than on other people, looking much within, to judge ourselves,
but little without, to censure our brethren. The narrowness of the
lights intimated the darkness of that dispensation, in comparison with
the gospel day.

`VI.` The chambers are described (v. 5, 6), which served as vestries, in
which the utensils of the tabernacle were carefully laid up, and where
the priests dressed and undressed themselves and left the clothes in
which they ministered: probably in some of these chambers they feasted
upon the holy things. Solomon was not so intent upon the magnificence of
the house as to neglect the conveniences that were requisite for the
offices thereof, that every thing might be done decently and in order.
Care was taken that the beams should not be fastened in the walls to
weaken them, v. 6. Let not the church\'s strength be impaired under
pretence of adding to its beauty or convenience.

### Verses 11-14

Here is, `I.` The word God sent to Solomon, when he was engaged in
building the temple. God let him know that he took notice of what he was
doing, the house he was now building, v. 12. None employ themselves for
God without having his eye upon them. \"I know thy works, thy good
works.\" He assured him that if he would proceed and persevere in
obedience to the divine law, and keep in the way of duty and the true
worship of God, the divine loving-kindness should be drawn out both to
himself (I will perform my word with thee) and to his kingdom: \"Israel
shall be ever owned as my people; I will dwell among them, and not
forsake them.\" This word God sent him probably by a prophet, 1. That by
the promise he might be encouraged and comforted in his work. Perhaps
sometimes the great care, expense, and fatigue of it, made him ready to
wish he had never begun it; but this would help him through the
difficulties of it, that the promised establishment of his family and
kingdom would abundantly recompense all his pains. An eye to the promise
will carry us cheerfully through our work; and those who wish well to
the public will think nothing too much that they can do to secure and
perpetuate to it the tokens of God\'s presence. 2. That, by the
condition annexed, he might be awakened to consider that though he built
the temple ever so strong the glory of it would soon depart, unless he
and his people continued to walk in God\'s statutes. God plainly let him
know that all this charge which he and his people were at, in erecting
this temple, would neither excuse them from obedience to the law of God
nor shelter them from his judgments in case of disobedience. Keeping
God\'s commandments is better, and more pleasing to him, than building
churches.

`II.` The work Solomon did for God: So he built the house (v. 14), so
animated by the message God had sent him, so admonished not to expect
that God should own his building unless he were obedient to his laws:
\"Lord, I proceed upon these terms, being firmly resolved to walk in thy
statutes.\" The strictness of God\'s government will never drive a good
man from his service, but quicken him in it. Solomon built and finished,
he went on with the work, and God went along with him till it was
completed. It is spoken both to God\'s praise and his: he grew not weary
of the work, met not with any obstructions (as Ezra 4:24), did not
out-build his property, nor do it by halves, but, having begun to build,
was both able and willing to finish; for he was a wise builder.

### Verses 15-38

Here, `I.` We have a particular account of the details of the building.

`1.` The wainscot of the temple. It was of cedar (v. 15), which was
strong and durable, and of a very sweet smell. The wainscot was
curiously carved with knops (like eggs or apples) and flowers, no doubt
as the fashion then was, v. 18.

`2.` The gilding. It was not like ours, washed over, but the whole house,
all the inside of the temple (v. 22), even the floor (v. 30), he
overlaid with gold, and the most holy place with pure gold, v. 21.
Solomon would spare no expense necessary to make it every way sumptuous.
Gold was under foot there, as it should be in all the living temples:
the abundance of it lessened its worth.

`3.` The oracle, or speaking-place (for so the word signifies), the holy
of holies, so called because thence God spoke to Moses, and perhaps to
the high priest, when he consulted with the breast-plate of judgment. In
this place the ark of the covenant was to be set, v. 19. Solomon made
every thing new, and more magnificent than it had been, except the ark,
which was still the same that Moses made, with its mercy-seat and
cherubim; that was the token of God\'s presence, which is always the
same with his people whether they meet in tent or temple, and changes
not with their condition.

`4.` The cherubim. Besides those at the ends of the mercy-seat, which
covered the ark, `(1.)` Solomon set up two more, very large ones, images
of young men (as some think), with wings made of olive-wood, and all
overlaid with gold, v. 23, etc. This most holy place was much larger
than that in the tabernacle, and therefore the ark would have seemed
lost in it, and the dead wall would have been unsightly, if it had not
been thus adorned. `(2.)` He carved cherubim upon all the walls of the
house, v. 29. The heathen set up images of their gods and worshipped
them; but these were designed to represent the servants and attendants
of the God of Israel, the holy angels, not to be themselves worshipped
(see thou do it not), but to show how great he is whom we are to
worship.

`5.` The doors. The folding doors that led into the oracle were but a
fifth part of the wall (v. 31), those into the temple were a fourth part
(v. 33); but both were beautified with cherubim engraven on them, v. 32,
35.

`6.` The inner court, in which the brazen altar was at which the priests
ministered. This was separated from the court where the people were by a
low wall, three rows of hewn stone tipped with a cornice of cedar (v.
36), that over it the people might see what was done and hear what the
priests said to them; for, even under that dispensation, they were not
kept wholly either in the dark or at a distance.

`7.` The time spent in this building. It was but seven years and a half
from the founding to the finishing of it, v. 38. Considering the
vastness and elegance of the building, and the many appurtenances to it
which were necessary to fit it for use, it was soon done. Solomon was in
earnest in it, had money enough, had nothing to divert him from it, and
many hands made quick work. He finished it (as the margin reads it) with
all the appurtenances thereof, and with all the ordinances thereof, not
only built the place, but set forward the work for which it was built.

`II.` Let us now see what was typified by this temple. 1. Christ is the
true temple; he himself spoke of the temple of his body, Jn. 2:21. God
himself prepared him his body, Heb. 10:5. In him dwelt the fulness of
the Godhead, as the Shechinah in the temple. In him meet all God\'s
spiritual Israel. Through him we have access with confidence to God. All
the angels of God, those blessed cherubim, have a charge to worship him.
2. Every believer is a living temple, in whom the Spirit of God dwells,
1 Co. 3:16. Even the body is such by virtue of its union with the soul,
1 Co. 6:19. We are not only wonderfully made by the divine providence,
but more wonderfully made anew by the divine grace. This living temple
is built upon Christ as its foundation and will be perfected in due
time. 3. The gospel church is the mystical temple; it grows to a holy
temple in the Lord (Eph. 2:21), enriched and beautified with the gifts
and graces of the Spirit, as Solomon\'s temple with gold and precious
stones. Only Jews built the tabernacle, but Gentiles joined with them in
building the temple. Even strangers and foreigners are built up a
habitation of God, Eph. 2:19, 22. The temple was divided into the holy
place and the most holy, the courts of it into the outer and inner; so
there are the visible and the invisible church. The door into the temple
was wider than that into the oracle. Many enter into profession that
come short of salvation. This temple is built firm, upon a rock, not to
be taken down as the tabernacle of the Old Testament was. The temple was
long in preparing, but was built at last. The top-stone of the gospel
church will, at length, be brought forth with shoutings, and it is a
pity that there should be the clashing of axes and hammers in the
building of it. Angels are ministering spirits, attending the church on
all sides and all the members of it. 4. Heaven is the everlasting
temple. There the church will be fixed, and no longer movable. The
streets of the new Jerusalem, in allusion to the flooring of the temple,
are said to be of pure gold, Rev. 21:21. The cherubim there always
attend the throne of glory. The temple was uniform, and in heaven there
is the perfection of beauty and harmony. In Solomon\'s temple there was
no noise of axes and hammers. Every thing is quiet and serene in heaven;
all that shall be stones in that building must in the present sate of
probation and preparation be fitted and made ready for it, must be hewn
and squared by divine grace, and so made meet for a place there.
