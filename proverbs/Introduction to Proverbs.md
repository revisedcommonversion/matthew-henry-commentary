Introduction to Proverbs
========================

We have now before us, `I.` A new author, or penman rather, or pen (if you
will) made use of by the Holy Ghost for making known the mind of God to
us, writing as moved by the finger of God (so the Spirit of God is
called), and that is Solomon; through his hand came this book of
Scripture and the two that follow it, Ecclesiastes and Canticles, a
sermon and a song. Some think he wrote Canticles when he was very young,
Proverbs in the midst of his days, and Ecclesiastes when he was old. In
the title of his song he only writes himself Solomon, perhaps because he
wrote it before his accession to the throne, being filled with the Holy
Ghost when he was young. In the title of his Proverbs he writes himself
the son of David, king of Israel, for then he ruled over all Israel. In
the title of his Ecclesiastes he writes himself the son of David, king
of Jerusalem, because then perhaps his influence had grown less upon the
distant tribes, and he confined himself very much in Jerusalem.
Concerning this author we may observe, 1. That he was a king, and a
king\'s son. The penmen of scripture, hitherto, were most of them men of
the first rank in the world, as Moses and Joshua, Samuel and David, and
now Solomon; but, after him, the inspired writers were generally poor
prophets, men of no figure in the world, because that dispensation was
approaching in the which God would choose the weak and foolish things of
the world to confound the wise and mighty and the poor should be
employed to evangelize. Solomon was a very rich king, and his dominions
were very large, a king of the first magnitude, and yet he addicted
himself to the study of divine things, and was a prophet and a
prophet\'s son. It is no disparagement to the greatest princes and
potentates in the world to instruct those about them in religion and the
laws of it. 2. That he was one whom God endued with extraordinary
measures of wisdom and knowledge, in answer to his prayers at his
accession to the throne. His prayer was exemplary: Give me a wise and an
understanding heart; the answer to it was encouraging: he had what he
desired and all other things were added to him. Now here we find what
good use he made of the wisdom God gave him; he not only governed
himself and his kingdom with it, but he gave rules of wisdom to others
also, and transmitted them to posterity. Thus must we trade with the
talents with which we are entrusted, according as they are. 3. That he
was one who had his faults, and in his latter end turned aside from
those good ways of God which in this book he had directed others in. We
have the story of it 1 Ki. 11, and a sad story it is, that the penman of
such a book as this should apostatize as he did. Tell it not in Gath.
But let those who are most eminently useful take warning by this not to
be proud or secure; and let us all learn not to think the worse of good
instructions though we have them from those who do not themselves
altogether live up to them.

`II.` A new way of writing, in which divine wisdom is taught us by
Proverbs, or short sentences, which contain their whole design within
themselves and are not connected with one another. We have had divine
laws, histories, and songs, and how divine proverbs; such various
methods has Infinite Wisdom used for our instruction, that, no stone
being left unturned to do us good, we may be inexcusable if we perish in
our folly. Teaching by proverbs was, 1. An ancient way of teaching. It
was the most ancient way among the Greeks; each of the seven wise men of
Greece had some one saying that he valued himself upon, and that made
him famous. These sentences were inscribed on pillars, and had in great
veneration as that which was said to come down from heaven. A coelo
descendit, Gnoµthi seauton-Know thyself is a precept which came down
from heaven. 2. It was a plain and easy way of teaching, which cost
neither the teachers nor the learners much pains, nor put their
understandings nor their memories to the stretch. Long periods, and
arguments far-fetched, must be laboured both by him that frames them and
by him that would understand them, while a proverb, which carries both
its sense and its evidence in a little compass, is quickly apprehended
and subscribed to, and is easily retained. Both David\'s devotions and
Solomon\'s instructions are sententious, which may recommend that way of
expression to those who minister about holy things, both in praying and
preaching. 3. It was a very profitable way of teaching, and served
admirably well to answer the end. The word Mashal, here used for a
proverb, comes from a word that signifies to rule or have dominion,
because of the commanding power and influence which wise and weighty
sayings have upon the children of men; he that teaches by them dominatur
in concionibus-rules his auditory. It is easy to observe how the world
is governed by proverbs. As saith the proverb of the ancients (1 Sa.
24:13), or (as we commonly express it) As the old saying is, goes very
far with most men in forming their notions and fixing their resolves.
Much of the wisdom of the ancients has been handed down to posterity by
proverbs; and some think we may judge of the temper and character of a
nation by the complexion of its vulgar proverbs. Proverbs in
conversation are like axious in philosophy, maxims in law, and postulata
in the mathematics, which nobody disputes, but every one endeavours to
expound so as to have them on his side. Yet there are many corrupt
proverbs, which tend to debauch men\'s minds and harden them in sin. The
devil has his proverbs, and the world and the flesh have their proverbs,
which reflect reproach on God and religion (as Eze. 12:22; 18:2), to
guard us against the corrupt influences of which God has his proverbs,
which are all wise and good, and tend to make us so. These proverbs of
Solomon were not merely a collection of the wise sayings that had been
formerly delivered, as some have imagined, but were the dictates of the
Spirit of God in Solomon. The very first of them (ch. 1:7) agrees with
what God said to man in the beginning (Job 28:28, Behold, the fear of
the Lord, that is wisdom); so that though Solomon was great, and his
name may serve as much as any man\'s to recommend his writings, yet,
behold, a greater than Solomon is here. It is God, by Solomon, that here
speaks to us: I say, to us; for these proverbs were written for our
learning, and, when Solomon speaks to his son, the exhortation is said
to speak to us as unto children, Heb. 12:5. And, as we have no book so
useful to us in our devotions as David\'s psalms, so have we none so
serviceable to us, for the right ordering of our conversations, as
Solomon\'s proverbs, which as David says of the commandments, are
exceedingly broad, containing, in a little compass, a complete body of
divine ethics, politics, and economics, exposing every vice,
recommending every virtue, and suggesting rules for the government of
ourselves in every relation and condition, and every turn of the
conversation. The learned bishop Hall has drawn up a system of moral
philosophy out of Solomon\'s Proverbs and Ecclesiastes. The first nine
chapters of this book are reckoned as a preface, by way of exhortation
to the study and practice of wisdom\'s rules, and caution against those
things that would hinder therein. We have then the first volume of
Solomon\'s proverbs (ch. 10-24); after that a second volume (ch. 25-29);
and then Agur\'s prophecy (ch. 30), and Lemuel\'s (ch. 31). The scope of
all is one and the same, to direct us so to order our conversation
aright as that in the end we may see the salvation of the Lord. The best
comment on these rules is to be ruled by them.
