Proverbs, Chapter 4
===================

Commentary
----------

When the things of God are to be taught precept must be upon precept,
and line upon line, not only because the things themselves are of great
worth and weight, but because men\'s minds, at the best, are unapt to
admit them and commonly prejudiced against them; and therefore Solomon,
in this chapter, with a great variety of expression and a pleasant
powerful flood of divine eloquence, inculcates the same things that he
had pressed upon us in the foregoing chapters. Here is, `I.` An earnest
exhortation to the study of wisdom, that is, of true religion and
godliness, borrowed from the good instructions which his father gave
him, and enforced with many considerable arguments (v. 1-13). `II.` A
necessary caution against bad company and all fellowship with the
unfruitful works of darkness (v. 14-19). `III.` Particular directions for
the attaining and preserving of wisdom, and bringing forth the fruits of
it (v. 20-27). So plainly, so pressingly, is the case laid before us,
that we shall be for ever inexcusable if we perish in our folly.

### Verses 1-13

Here we have,

`I.` The invitation which Solomon gives to his children to come and
receive instruction from him (v. 1, 2): Hear, you children, the
instruction of a father. That is, 1. \"Let my own children, in the first
place, receive and give good heed to those instructions which I set down
for the use of others also.\" Note, Magistrates and ministers, who are
entrusted with the direction of larger societies, are concerned to take
a more than ordinary care for the good instruction of their own
families; from this duty their public work will by no means excuse them.
This charity must begin at home, though it must not end there; for he
that has not his children in subjection with all gravity, and does not
take pains in their good education, how shall he do his duty as he ought
to the church of God? 1 Tim. 3:4, 5. The children of those that are
eminent for wisdom and public usefulness ought to improve in knowledge
and grace in proportion to the advantages they derive from their
relation to such parents. Yet it may be observed, to save both the
credit and the comfort of those parents whose children do not answer the
hopes that arose from their education, that Rehoboam, the son of
Solomon, was far from being either one of the wisest or one of the best.
We have reason to think that thousands have got more good by Solomon\'s
proverbs than his own son did, to whom they seem to have been dedicated.
2. Let all young people, in the days of their childhood and youth, take
pains to get knowledge and grace, for that is their learning age, and
then their minds are formed and seasoned. He does not say, My children,
but You children. We read but of one son that Solomon had of his own;
but (would you think it?) he is willing to set up for a schoolmaster,
and to teach other people\'s children! for at that age there is most
hope of success; the branch is easily bent when it is young and tender.
3. Let all that would receive instruction come with the disposition of
children, though they be grown persons. Let all prejudices be laid
aside, and the mind be as white paper. let them be dutiful, tractable,
and self-diffident, and take the word as the word of a father, which
comes both with authority and with affection. We must see it coming from
God as our Father in heaven, to whom we pray, from whom we expect
blessings, the Father of our spirits, to whom we ought to be in
subjection, that we may live. We must look upon our teachers as our
fathers, who love us and seek our welfare; and therefore though the
instruction carry in it reproof and correction, for so the word
signifies, yet we must bid it welcome. Now, `(1.)` To recommend it to us,
we are told, not only that it is the instruction of a father, but that
it is understanding, and therefore should be welcome to intelligent
creatures. Religion has reason on its side, and we are taught it by fair
reasoning. It is a law indeed (v. 2), but that law is founded upon
doctrine, upon unquestionable principles of truth, upon good doctrine,
which is not only faithful, but worthy of all acceptation. If we admit
the doctrine, we cannot but submit to the law. `(2.)` To rivet it in us,
we are directed to receive it as a gift, to attend to it with all
diligence, to attend so as to know it, for otherwise we cannot do it,
and not to forsake it by disowning the doctrine or disobeying the law.

`II.` The instructions he gives them. Observe,

`1.` How he came by these instructions; he had them from his parents, and
teaches his children the same that they taught him, v. 3, 4. Observe,
`(1.)` His parents loved him, and therefore taught him: I was my father\'s
son. David had many sons, but Solomon was his son indeed, as Isaac is
called (Gen. 17:19) and for the same reason, because on him the covenant
was entailed. He was his father\'s darling, above any of his children.
God had a special kindness for Solomon (the prophet called him Jedidiah,
because the Lord loved him, 2 Sa. 12:25), and for that reason David had
a special kindness for him, for he was a man after God\'s own heart. If
parents may ever love one child better than another, it must not be till
it plainly appears that God does so. He was tender, and only beloved, in
the sight of his mother. Surely there was a manifest reason for making
such a distinction when both the parents made it. Now we see how they
showed their love; they catechised him, kept him to his book, and held
him to a strict discipline. Though he was a prince, and heir-apparent to
the crown, yet they did not let him live at large; nay, therefore they
tutored him thus. And perhaps David was the more strict with Solomon in
his education because he had seen the ill effects of an undue indulgence
in Adonijah, whom he had not crossed in any thing (1 Ki. 1:6), as also
in Absalom. `(2.)` What his parents taught him he teaches others. Observe,
`[1.]` When Solomon was grown up he not only remembered, but took a
pleasure in repeating, the good lessons his parents taught him when he
was a child. He did not forget them, so deep were the impressions they
made upon him. He was not ashamed of them, such a high value had he for
them, nor did he look upon them as the childish things, the mean things,
which, when he became a man, a king, he should put away, as a
disparagement to him; much less did he repeat them: as some wicked
children have done, to ridicule them, and make his companions merry with
them, priding himself that he had got clear from grave lessons and
restraints. `[2.]` Though Solomon was a wise man himself, and divinely
inspired, yet, when he was to teach wisdom, he did not think it below
him to quote his father and to make use of his words. Those that would
learn well, and teach well, in religion, must not affect new-found
notions and new-coined phrases, so as to look with contempt upon the
knowledge and language of their predecessors; if we must keep to the
good old way, why should we scorn the good old words? Jer. 6:16. `[3.]`
Solomon, having been well educated by his parents, thought himself
thereby obliged to give his children a good education, the same that his
parents had given him; and this is one way in which we must requite our
parents for the pains they took with us, even by showing piety at home,
1 Tim. 5:4. They taught us, not only that we might learn ourselves, but
that we might teach our children, the good knowledge of God, Ps. 78:6.
And we are false to a trust if we do not; for the sacred deposit of
religious doctrine and law was lodged in our hands with a charge to
transmit it pure and entire to those that shall come after us, 2 Tim.
2:2. `[4.]` Solomon enforces his exhortations with the authority of his
father David, a man famous in his generation upon all accounts. Be it
taken notice of, to the honour of religion, that the wisest and best men
in every age have been most zealous, not only for the practice of it
themselves, but for the propagating of it to others; and we should
therefore continue in the things which we have learned, knowing of whom
we have learned them, 2 Tim. 3:14.

`2.` What these instructions were, v. 4-13.

`(1.)` By way of precept and exhortation. David, in teaching his son,
though he was a child of great capacity and quick apprehension, yet to
show that he was in good earnest, and to affect his child the more with
what he said, expressed himself with great warmth and importunity, and
inculcated the same thing again and again. So children must be taught.
Deu. 6:7, Thou shalt whet them diligently upon thy children. David,
though he was a man of public business, and had tutors for his son, took
all this pains with him himself.

`[1.]` He recommends to him his Bible and his catechism, as the means,
his father\'s words (v. 4), the words of his mouth (v. 5), his sayings
(v. 10), all the good lessons he had taught him; and perhaps he means
particularly the book of Psalms, many of which were Maschils-psalms of
instruction, and two of them are expressly said to be for Solomon.
These, and all his other words, Solomon must have an eye to. First, He
must hear and receive them (v. 10), diligently attend to them, and
imbibe them, as the earth drinks in the rain that comes often upon it,
Heb. 6:7. God thus bespeaks our attention to his word: Hear, O my son!
and receive my sayings. Secondly, He must hold fast the form of sound
words which his father gave him (v. 4): Let thy heart retain my words;
and except the word be hid in the heart, lodged in the will and
affections, it will not be retained. Thirdly, He must govern himself by
them: Keep my commandments, obey them, and that is the way to increase
in the knowledge of them, Jn. 7:17. Fourthly, He must stick to them and
abide by them: \"Decline not from the words of my mouth (v. 5), as
fearing they will be too great a check upon thee, but take fast hold of
instruction (v. 13), as being resolved to keep thy hold and never let it
go.\" Those that have a good education, though they strive to shake it
off, will find it hang about them a great while, and, if it do not,
their case is very sad.

`[2.]` He recommends to him wisdom and understanding as the end to be
aimed at in the use of these means; that wisdom which is the principal
wisdom, get that. Quod caput est sapientia eam acquire sapientiam-Be
sure to mind that branch of wisdom which is the top branch of it, and
that is the fear of God, ch. 1:7. Junius and Tremellius. A principle of
religion in the heart is the one thing needful; therefore, First, Get
this wisdom, get this understanding, v. 5. And again, \"Get wisdom, and
with all thy getting, get understanding, v. 7. Pray for it, take pains
for it, give diligence in the use of all appointed means to attain it.
Wait at wisdom\'s gate, Prov. 8:34. Get dominion over thy corruptions,
which are thy follies: get possession of wise principles and the habits
of wisdom. Get wisdom by experience, get it above all thy getting; be
more in care and take more pains to get this than to get the wealth of
this world; whatever thou forgettest, get this, reckon it a great
achievement, and pursue it accordingly.\" True wisdom is God\'s gift,
and yet we are here commanded to get it, because God gives it to those
that labour for it; yet, after all, we must not say, Our might and the
power of our hand have gotten us this wealth. Secondly, Forget her not
(v. 5), forsake her not (v. 6), let her not go (v. 13), but keep her.
Those that have got this wisdom must take heed of losing it again by
returning to folly: it is indeed a good part, that shall not be taken
from us; but then we must take heed lest we throw it from us, as those
do that forget it first, and let it slip out of their minds, and then
forsake it and turn out of its good ways. That good thing which is
committed to us we must keep, and not let it drop, through carelessness,
nor suffer it to be forced from us, nor suffer ourselves to be wheedled
out of it; never let go such a jewel. Thirdly, Love her (v. 6), and
embrace her (v. 8), as worldly men love their wealth and set their
hearts upon it. Religion should be very dear to us, dearer than any
thing in this world; and, if we cannot reach to be great masters of
wisdom, yet let us be true lovers of it; and what grace we have let us
embrace it with a sincere affection, as those that admire its beauty.
Fourthly, \"Exalt her, v. 8. Always keep up high thoughts of religion,
and do all thou canst to bring it into reputation, and maintain the
credit of it among men. Concur with God in his purpose, which is to
magnify the law and make it honourable, and do what thou canst to serve
that purpose.\" Let Wisdom\'s children not only justify her, but magnify
her, and prefer her before that which is dearest to them in this world.
In honouring those that fear the Lord, though they are low in the world,
and in regarding a poor wise man, we exalt wisdom.

`(2.)` By way of motive and inducement thus to labour for wisdom, and
submit to the guidance of it, consider, `[1.]` It is the main matter,
and that which ought to be the chief and continual care of every man in
this life (v. 7): Wisdom is the principal thing; other things which we
are solicitous to get and keep are nothing to it. It is the whole of
man, Eccl. 12:13. It is that which recommends us to God, which
beautifies the soul, which enables us to answer the end of our creation,
to live to some good purpose in the world, and to get to heaven at last;
and therefore it is the principal thing. `[2.]` It has reason and equity
on its side (v. 11): \"I have taught thee in the way of wisdom, and so
it will be found to be at last. I have led thee, not in the crooked ways
of carnal policy, which does wrong under colour of wisdom, but in right
paths, agreeable to the eternal rules and reasons of good and evil.\"
The rectitude of the divine nature appears in the rectitude of all the
divine laws. Observe, David not only taught his son by good
instructions, but led him both by a good example and by applying general
instructions to particular cases; so that nothing was wanting on his
part to make him wise. `[3.]` It would be much for his own advantage:
\"If thou be wise and good, thou shalt be so for thyself.\" First, \"It
will be thy life, thy comfort, thy happiness; it is what thou canst not
live without:\" Keep my commandments and live, v. 4. That of our Saviour
agrees with this, If thou wilt enter into life, keep the commandments,
Mt. 19:17. It is upon pain of death, eternal death, and in prospect of
life, eternal life, that we are required to be religious. \"Receive
wisdom\'s sayings, and the years of thy life shall be many (v. 10), as
many in this world as Infinite Wisdom sees fit, and in the other world
thou shalt live that life the years of which shall never be numbered.
Keep her therefore, whatever it cost thee, for she is thy life, v. 13.
All thy satisfaction will be found in this;\" and a soul without true
wisdom and grace is really a dead soul. Secondly, \"It will be thy guard
and guide, thy convoy and conductor, through all the dangers and
difficulties of thy journey through this wilderness. Love wisdom, and
cleave to her, and she shall preserve thee, she shall keep thee (v. 6)
from sin, the worst of evils, the worst of enemies; she shall keep thee
from hurting thyself, and then none else can hurt thee.\" As we say,
\"Keep thy shop, and thy shop will keep thee;\" so, \"Keep thy wisdom,
and thy wisdom will keep thee.\" It will keep us from straits and
stumbling-blocks in the management of ourselves and our affairs, v. 12.
`1.` That our steps be not straitened when we go, that we bring not
ourselves into such straits as David was in, 2 Sa. 24:14. Those that
make God\'s word their rule shall walk at liberty, and be at ease in
themselves. 2. That our feet do not stumble when we run. If wise and
good men be put upon sudden resolves, the certain rule of God\'s word
which they go by will keep them even then from stumbling upon any thing
that may be pernicious. Integrity and uprightness will preserve us.
Thirdly, \"It will be thy honour and reputation (v. 8): Exalt wisdom (do
thou but show thy good-will to her advancement) and though she needs not
thy service she will abundantly recompense it, she shall promote thee,
she shall bring thee to honour.\" Solomon was to be a king, but his
wisdom and virtue would be more his honour than his crown or purple; it
was that for which all his neighbours had him so much in veneration; and
no doubt, in his reign and David\'s, wise and good men stood fairest for
preferment. However, religion will, first or last, bring all those to
honour that cordially embrace her; they shall be accepted of God,
respected by all wise men, owned in the great day, and shall inherit
everlasting glory. This he insists on (v. 9): \"She shall give to thy
head an ornament of grace in this world, shall recommend thee both to
God and man, and in the other world a crown of glory shall she deliver
to thee, a crown that shall never totter, a crown of glory that shall
never wither.\" That is the true honour which attends religion.
Nobilitas sola est atique unica virtus-Virtue is the only nobility!
David having thus recommended wisdom to his son, no marvel that when God
bade him ask what he would he prayed, Lord, give me a wise and an
understanding heart. We should make it appear by our prayers how well we
are taught.

### Verses 14-19

Some make David\'s instructions to Solomon, which began v. 4, to
continue to the end of the chapter; nay, some continue them to the end
of the ninth chapter; but it is more probable that Solomon begins here
again, if not sooner. In these verses, having exhorted us to walk in the
paths of wisdom, he cautions us against the path of the wicked. 1. We
must take heed of the ways of sin and avoid them, every thing that looks
like sin and leads to it. 2. In order to this we must keep out of the
ways of sinners, and have no fellowship with them. For fear of falling
into wicked courses, we must shun wicked company. Here is,

`I.` The caution itself, v. 14, 15. 1. We must take heed of falling in
with sin and sinners: Enter not into the paths of the wicked. Our
teacher, having like a faithful guide shown us the right paths (v. 11),
here warns us of the by-paths into which we are in danger of being drawn
aside. Those that have been well educated, and trained up in the way
they should go, let them never turn aside into the way they should not
go; let them not so much as enter into it, no, not to make trial of it,
lest it prove a dangerous experiment and difficult to retreat with
safety. \"Venture not into the company of those that are infected with
the plague, no, not though thou think thyself guarded with an
antidote.\" 2. If at any time we are inveigled into an evil way, we must
hasten out of it. \"If, ere thou wast aware, thou didst enter in at the
gate, because it was wide, go not on in the way of evil men. As soon as
thou art made sensible of thy mistake, retire immediately, take not a
step more, stay not a minute longer, in the way that certainly leads to
destruction.\" 3. We must dread and detest the ways of sin and sinners,
and decline them with the utmost care imaginable. \"The way of evil men
may seem a pleasant way and sociable, and the nearest way to the
compassing of some secular end we may have in view; but it is an evil
way, and will end ill, and therefore if thou love thy God and thy soul
avoid it, pass not by it, that thou mayest not be tempted to enter into
it; and, if thou find thyself near it, turn from it and pass away, and
get as far off it as thou canst.\" The manner of expression intimates
the imminent danger we are in, the need we have of this caution, and the
great importance of it, and that our watchmen are, or should be, in good
earnest, in giving us warning. It intimates likewise at what a distance
we should keep from sin and sinners; he does not say, Keep at a due
distance, but at a great distance, the further the better; never think
you can get far enough from it. Escape for thy life: look not behind
thee.

`II.` The reasons to enforce this caution.

`1.` \"Consider the character of the men whose way thou art warned to
shun.\" They are mischievous men (v. 16, 17); they not only care not
what hurt they do to those that stand in their way, but it is their
business to do mischief, and their delight, purely for mischief-sake.
They are continually designing and endeavouring to cause some to fall,
to ruin them body and soul. Wickedness and malice are in their nature,
and violence is in all their actions. They are spiteful in the highest
degree; for, `(1.)` Mischief is rest and sleep to them. As much
satisfaction as a covetous man has when he has got money, an ambitious
man when he has got preferment, and a good man when he has done good, so
much have they when they have said or done that which is injurious and
ill-natured; and they are extremely uneasy if they cannot get their envy
and revenge gratified, as Haman, to whom every thing was unpleasant as
long as Mordecai was unhanged. It intimates likewise how restless and
unwearied they are in their mischievous pursuits; they will rather be
deprived of sleep than of the pleasure of being vexatious. `(2.)` Mischief
is meat and drink to them; they feed and feast upon it. They eat the
bread of the wickedness (they eat up my people as they eat bread, Ps.
14:4) and drink the wine of violence (v. 17), drink iniquity like water,
Job 15:16. All they eat and drink is got by rapine and oppression. Do
wicked men think the time lost in which they are not doing hurt? Let
good men make it as much their business and delight to do good. Amici,
diem perdidi-Friends, I have lost a day. And let all that are wise, and
wish well to themselves, avoid the society of the wicked; for, `[1.]` It
is very scandalous; for there is no disposition of mind that is a
greater reproach to human nature, a greater enemy to human society, a
bolder defiance to God and conscience, that has more of the devil\'s
image in it, or is more serviceable to his interests, than a delight to
do mischief and to vex, and hurt, and ruin every body. `[2.]` It is very
dangerous. \"Shun those that delight to do mischief as thou tenderest
thy own safety; for, whatever friendship they may pretend, one time or
other they will do thee mischief; thou wilt ruin thyself if thou dost
concur with them (ch. 1:18) and they will ruin thee if thou dost not.\"

`2.` \"Consider the character of the way itself which thou art warned to
shun, compared with the right way which thou art invited to walk in.\"

`(1.)` The way of righteousness is light (v. 18): The path of the just,
which they have chosen, and in which they walk, is as light; the light
shines on their ways (Job 22:28) and makes them both safe and pleasant.
Christ is their way and he is the light. They are guided by the word of
God and that is a light to their feet; they themselves are light in the
Lord and they walk in the light as he is in the light. `[1.]` It is a
shining light. Their way shines to themselves in the joy and comfort of
it; it shines before others in the lustre and honour of it; it shines
before men, who see their good works, Mt. 5:16. They go on in their way
with a holy security and serenity of mind, as those that walk in the
light. It is as the morning-light, which shines out of obscurity (Isa.
58:8, 10) and puts an end to the works of darkness. `[2.]` It is a
growing light; it shines more and more, not like the light of a meteor,
which soon disappears, or that of a candle, which burns dim and burns
down, but like that of the rising sun, which goes forward shining,
mounts upward shining. Grace, the guide of this way, is growing; he that
has clean hands shall be stronger and stronger. That joy which is the
pleasure of this way, that honour which is the brightness of it, and all
that happiness which is indeed its light, shall be still increasing.
`[3.]` It will arrive, in the end, at the perfect day. The light of the
dayspring will at length be noon-day light, and it is this that the
enlightened soul is pressing towards. The saints will not be perfect
till they come to heaven, but there they shall themselves shine as the
sun when he goes forth in his strength, Mt. 13:43. Their graces and joys
shall be all consummate. Therefore it is our wisdom to keep close to the
path of the just.

`(2.)` The way of sin is as darkness, v. 19. The works he had cautioned us
not to have fellowship with are works of darkness. What true pleasure
and satisfaction can those have who know no pleasure and satisfaction
but what they have in doing mischief? What sure guide have those that
cast God\'s word behind them? The way of the wicked is dark, and
therefore dangerous; for they stumble and yet know not at what they
stumble. They fall into sin, but are not aware which way the temptation
came by which they were overthrown, and therefore know not how to avoid
it the next time. They fall into trouble, but never enquire wherefore
God contends with them; they consider not that they do evil, nor what
will be in the end of it, Ps. 82:5; Job 18:5, 6. This is the way we are
directed to shun.

### Verses 20-27

Solomon, having warned us not to do evil, here teaches us how to do
well. It is not enough for us to shun the occasions of sin, but we must
study the methods of duty.

`I.` We must have a continual regard to the word of God and endeavour that
it may be always ready to us.

`1.` The sayings of wisdom must be our principles by which we must govern
ourselves, our monitors to warn us of duty and danger; and therefore,
`(1.)` We must receive them readily: \"Incline thy ear to them (v. 20);
humbly bow to them; diligently listen to them.\" The attentive hearing
of the word of God is a good sign of a work of grace begun in the heart
and a good means of carrying it on. It is to be hoped that those are
resolved to do their duty who are inclined to know it. `(2.)` We must
retain them carefully (v. 21); we must lay them before us as our rule:
\"Let them not depart from thy eyes; view them, review them, and in
every thing aim to conform to them.\" We must lodge them within us, as a
commanding principle, the influences of which are diffused throughout
the whole man: \"Keep them in the midst of thy heart, as things dear to
thee, and which thou art afraid of losing.\" Let the word of God be
written in the heart, and that which is written there will remain.

`2.` The reason why we must thus make much of the words of wisdom is
because they will be both food and physic to us, like the tree of life,
Rev. 22:2; Eze. 47:12. Those that seek and find them, find and keep
them, shall find in them, `(1.)` Food: For they are life unto those that
find them, v. 22. As the spiritual life was begun by the word as the
instrument of it, so by the same word it is still nourished and
maintained. We could not live without it; we may by faith live upon it.
`(2.)` Physic. They are health to all their flesh, to the whole man, both
body and soul; they help to keep both in good plight. They are health to
all flesh, so the Septuagint. There is enough to cure all the diseases
of this distempered world. They are a medicine to all their flesh (so
the word is), to all their corruptions, for they are called flesh, to
all their grievances, which are as thorns in the flesh. There is in the
word of God a proper remedy for all our spiritual maladies.

`II.` We must keep a watchful eye and a strict hand upon all the motions
of our inward man, v. 23. Here is, 1. A great duty required by the laws
of wisdom, and in order to our getting and preserving wisdom: Keep thy
heart with all diligence. God, who gave us these souls, gave us a strict
charge with them: Man, woman, keep thy heart; take heed to thy spirit,
Deu. 4:9. We must maintain a holy jealousy of ourselves, and set a
strict guard, accordingly, upon all the avenues of the soul; keep our
hearts from doing hurt and getting hurt, from being defiled by sin and
disturbed by trouble; keep them as our jewel, as our vineyard; keep a
conscience void of offence; keep out bad thoughts; keep up good
thoughts; keep the affections upon right objects and in due bounds. Keep
them with all keepings (so the word is); there are many ways of keeping
things-by care, by strength, by calling in help, and we must use them
all in keeping our hearts; and all little enough, so deceitful are they,
Jer. 17:9. Or above all keepings; we must keep our hearts with more care
and diligence than we keep any thing else. We must keep our eyes (Job
31:1), keep our tongues (Ps. 34:13), keep our feet (Eccl. 5:1), but,
above all, keep our hearts. 2. A good reason given for this care,
because out of it are the issues of life. Out of a heart well kept will
flow living issues, good products, to the glory of God and the
edification of others. Or, in general, all the actions of the life flow
from the heart, and therefore keeping that is making the tree good and
healing the springs. Our lives will be regular or irregular, comfortable
or uncomfortable, according as our hearts are kept or neglected.

`III.` We must set a watch before the door of our lips, that we offend
not with out tongue (v. 24): Put away from thee a froward mouth and
perverse lips. Our hearts being naturally corrupt, out of them a great
deal of corrupt communication is apt to come, and therefore we must
conceive a great dread and detestation of all manner of evil words,
cursing, swearing, lying, slandering, brawling, filthiness, and foolish
talking, all which come from a froward mouth and perverse lips, that
will not be governed either by reason or religion, but contradict both,
and which are as unsightly and ill-favoured before God as a crooked
distorted mouth drawn awry is before men. All manner of tongue sins, we
must, by constant watchfulness and stedfast resolution, put from us, put
far from us, abstaining from all words that have an appearance of evil
and fearing to learn any such words.

`IV.` We must make a covenant with our eyes: \"Let them look right on and
straight before thee, v. 25. Let the eye be fixed and not wandering; let
it not rove after every thing that presents itself, for then it will be
diverted form good and ensnared in evil. Turn it from beholding vanity;
let thy eye be single and not divided; let thy intentions be sincere and
uniform, and look not asquint at any by-end.\" We must keep our eye upon
our Master, and be careful to approve ourselves to him; keep our eye
upon our rule, and conform to that; keep our eye upon our mark, the
prize of the high calling, and direct all towards that. Oculum in
metam-The eye upon the goal.

`V.` We must act considerately in all we do (v. 26): Ponder the path of
thy feet, weigh it (so the word is); \"put the word of God in one scale,
and what thou hast done, or art about to do, in the other, and see how
they agree; be nice and critical in examining whether thy way be good
before the Lord and whether it will end well.\" We must consider our
past ways and examine what we have done, and our present ways, what we
are doing, whither we are going, and see that we walk circumspectly. It
concerns us to consider what are the duties and what the difficulties,
what are the advantages and what the dangers, of our way, that we may
act accordingly. \"Do nothing rashly.\"

`VI.` We must act with steadiness, caution, and consistency: \"Let all
thy ways be established (v. 26) and be not unstable in them, as the
double-minded man is; halt not between two, but go on in an even uniform
course of obedience; turn not to the right hand not to the left, for
there are errors on both hands, and Satan gains his point if he prevails
to draw us aside either way. Be very careful to remove thy foot from
evil; take heed of extremes, for in them there is evil, and let thy eyes
look right on, that thou mayest keep the golden mean.\" Those that would
approve themselves wise must always be watchful.
