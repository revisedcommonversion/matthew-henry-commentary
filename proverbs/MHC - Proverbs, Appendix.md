Appendix
========

Twenty chapters of the book of Proverbs (beginning with ch. 10 and
ending with ch. 29), consisting mostly of entire sentences in each
verse, could not well be reduced to proper heads, and the contents of
them gathered; I have therefore here put the contents of all these
chapters together, which perhaps may be of some use to those who desire
to see at once all that is said of any one head in these chapters. Some
of the verses, perhaps, I have not put under the same heads that another
would have put them under, but the most of them fall (I hope) naturally
enough to the places I have assigned them.

`1.` Of the comfort, or grief, parents have in their children, according
as they are wise or foolish, godly or ungodly, ch. 10:1; 15:20; 17:21,
25; 19:13, 26; 23:15, 16, 24, 25; 27:11; 29:3

`2.` Of the world\'s insufficiency, and religion\'s sufficiency, to make
us happy (ch. 10:2, 3; 11:4) and the preference to be therefore given to
the gains of virtue above those of this world, ch. 15:16, 17; 16:8, 16;
17:1; 19:1; 28:6, 11

`3.` Of slothfulness and diligence, ch. 10:4, 26; 12:11, 24, 27; 13:4,
23; 15:19; 16:26; 18:9; 19:15, 24; 20:4, 13; 21:5, 25, 26; 22:13, 29;
24:30-34; 26:13-16; 27:18, 23, 27; 28:19. Particularly the improving or
neglecting opportunities, ch. 6:6; 10:5

`4.` The happiness of the righteous, and the misery of the wicked, ch.
10:6, 9, 16, 24, 25, 27-30; 11:3, 5-8, 18-21, 31; 12:2, 3, 7, 13, 14,
21, 26, 28; 13:6, 9, 14 15, 21, 22, 25; 14:11, 14, 19, 32; 15:6, 8, 9,
24, 26, 29; 20:7; 21:12, 15, 16, 18, 21; 22:12; 28:10, 18; 29:6

`5.` Of honour and dishonour, ch. 10:7; 12:8, 9; 18:3; 26:1; 27:21. And
of vain-glory, ch. 25:14, 27; 27:2

`6.` The wisdom of obedience, and folly of disobedience, ch. 10:8, 17;
12:1, 15; 13:1, 13, 18; 15:5, 10, 12, 31, 32; 19:16; 28:4, 7, 9

`7.` Of mischievousness and usefulness, ch. 10:10, 23; 11:9-11, 23, 27;
12:5, 6, 12, 18, 20; 13:2; 14:22; 16:29, 30; 17:11; 21:10; 24:8; 26:23,
27

`8.` The praise of wise and good discourse, and the hurt and shame of an
ungoverned tongue, ch. 10:11, 13, 14, 20, 21, 31, 32; 11:30; 14:3; 15:2,
4, 7, 23, 28; 16:20, 23, 24; 17:7; 18:4, 7, 20, 21; 20:15; 21:23; 23:9;
24:26; 25:11

`9.` Of love and hatred, peaceableness and contention, ch. 10:12; 15:17;
17:1, 9, 14, 19; 18:6, 17-19; 20:3; 25:8; 26:17, 21; 29:9

`10.` Of the rich and poor, ch. 10:5, 22; 11:28; 13:7, 8; 14:20, 24;
18:11, 23; 19:1, 4, 7, 22; 22:2, 7; 28:6, 11; 29:13

`11.` Of lying, fraud, and dissimulation, and of truth and sincerity, ch.
10:18; 12:17, 19, 22; 13:5; 17:4; 20:14, 17; 26:18, 19, 24-26, 28

`12.` Of slandering, ch. 10:18; 16:27; 25:23

`13.` Of talkativeness and silence, ch. 10:19; 11:12; 12:23; 13:3; 17:27,
28; 29:11, 20

`14.` Of justice and injustice, ch. 11:1; 13:16; 16:8, 11; 17:15, 26;
18:5; 20:10, 23; 22:28; 23:10, 11; 29:24

`15.` Of pride and humility, ch. 11:2; 13:10; 15:25, 33; 16:5, 18, 19;
18:12; 21:4; 25:6, 7; 28:25; 29:23

`16.` Of despising and respecting others, ch. 11:12; 14:21

`17.` Of tale-bearing, ch. 11:13; 16:28; 18:8; 20:19; 26:20, 22

`18.` Of rashness and deliberation, ch. 11:14; 15:22; 18:13; 19:2; 20:5,
18; 21:29; 22:3; 25:8-10

`19.` Of suretiship, ch. 11:15; 17:18; 20:16; 22:26, 27; 27:13

`20.` Of good and bad women, or wives, ch. 11:16, 22; 12:4; 14:1; 18:22;
19:13, 14; 21:9, 19; 25:24; 27:15, 16

`21.` Of mercifulness and unmercifulness, ch. 11:17; 12:10; 14:21; 19:17;
21:13

`22.` Of charity to the poor, and uncharitableness, ch. 11:24-26; 14:31;
17:5; 22:9, 16, 22, 23; 28:27; 29:7

`23.` Of covetousness and contentment, ch. 11:29; 15:16, 17, 27; 23:4, 5

`24.` Of anger and meekness, ch. 12:16; 14:17, 29; 15:1, 18; 16:32;
17:12, 26; 19:11, 19; 22:24, 25; 25:15, 28; 26:21; 29:22

`25.` Of melancholy and cheerfulness, ch. 12:25; 14:10, 13; 15:13, 15;
17:22; 18:14; 25:20, 25

`26.` Of hope and expectation, ch. 13:12, 19

`27.` Of prudence and foolishness, ch. 13:16; 14:8, 18, 33; 15:14, 21;
16:21, 22; 17:24; 18:2, 15; 24:3-7; 7:27; 26:6-11; 28:5

`28.` Of treachery and fidelity, ch. 13:17; 25:13, 19

`29.` Of good and bad company, ch. 13:20; 14:7; 28:7; 29:3

`30.` Of the education of children, ch. 13:24; 19:18; 20:11; 22:6, 15;
23:12; 14:14; 29:15, 17

`31.` Of the fear of the Lord, ch. 14:2, 26, 27; 15:16, 33; 16:6; 19:23;
22:4; 23:17, 18

`32.` Of true and false witness-bearing, ch. 14:5, 25; 19:5, 9, 28;
21:28; 24:28; 25:18

`33.` Of scorners, ch. 14:6, 9; 21:24; 22:10; 24:9; 29:9

`34.` Of credulity and caution, ch. 14:15, 16; 27:12

`35.` Of kings and their subjects, ch. 14:28, 34, 35; 16:10, 12-15; 19:6,
12; 20:2, 8, 26, 28; 22:11; 24:23-25; 30:2-5; 28:2, 3, 15, 16; 29:5, 12,
14, 26

`36.` Of envy, especially envying sinners, ch. 14:30; 23:17, 18; 24:1, 2,
19, 20; 27:4

`37.` Of God\'s omniscience, and his universal providence, ch. 15:3, 11;
16:1, 4, 9, 33; 17:3; 19:21; 20:12, 24; 21:1, 30, 31; 29:26

`38.` Of a good and ill name, ch. 15:30; 22:1

`39.` Of men\'s good opinion of themselves, ch. 14:12; 16:2, 25; 20:6;
21:2; 26:12; 28:26

`40.` Of devotion towards God, and dependence on him, ch. 16:3; 18:10;
23:26; 27:1; 28:25; 29:25

`41.` Of the happiness of God\'s favour, ch. 16:7; 29:26

`42.` Excitements to get wisdom, ch. 16:16; 18:1; 19:8, 20; 22:17-21;
23:15, 16, 22-25; 24:13, 14; 27:11

`43.` Cautions against temptations, ch. 16:17; 29:27

`44.` Of old age and youth, ch. 16:31; 17:6; 20:29

`45.` Of servants, 17:2; 19:10; 29:19, 21

`46.` Of bribery, ch. 17:8, 23; 18:16; 21:14; 28:21

`47.` Of reproof and correction, ch. 17:10; 19:25, 29; 20:30; 21:11;
25:12; 26:3; 27:5, 6, 22; 28:23; 29:1

`48.` Of ingratitude, ch. 17:13

`49.` Of friendship, ch. 17:17; 18:24; 27:9, 10, 14, 17

`50.` Of sensual pleasures, ch. 21:17; 23:1-3, 6-8, 19-21; 27:7

`51.` Of drunkenness, ch. 20:1; 23:23, 29-35

`52.` Of the universal corruption of nature, ch. 20:9

`53.` Of flattery, ch. 20:19; 26:28; 28:23; 29:5

`54.` Of undutiful children, ch. 20:20; 28:24

`55.` Of the short continuance of what is ill-gotten, ch. 20:21; 21:6, 7;
22:8; 28:8

`56.` Of revenge, ch. 20:22; 24:17, 18, 29

`57.` Of sacrilege, ch. 20:25

`58.` Of conscience, ch. 20:27; 27:19

`59.` Of the preference of moral duties before ceremonial, ch. 15:8;
21:3, 27

`60.` Of prodigality and wastefulness, ch. 21:20

`61.` The triumphs of wisdom and godliness, ch. 21:22; 24:15, 16

`62.` Of frowardness and tractableness, ch. 22:5

`63.` Of uncleanness, ch. 22:14; 23:27, 28

`64.` Of fainting in affliction, ch. 24:10

`65.` Of helping the distressed, ch. 14:11, 12

`66.` Of loyalty to the government, ch. 24:21, 22

`67.` Of forgiving enemies, 25:21, 22

`68.` Of causeless curse, ch. 26:2

`69.` Of answering fools, ch. 26:4, 5

`70.` Of unsettledness and unsatisfiedness, ch. 27:8, 20

`71.` Of cowardliness and courage, ch. 28:1

`72.` The people\'s interest in the character of their rulers, ch. 28:12,
28; 29:2, 16; 11:10, 11

`73.` The benefit of repentance and holy fear, ch. 28:13, 14

`74.` The punishment of murder, ch. 28:17

`75.` Of hastening to be rich, ch. 28:20, 22

`76.` The enmity of the wicked against the godly, ch. 29:10, 27

`77.` The necessity of the means of grace, ch. 29:18
