Proverbs, Chapter 8
===================

Commentary
----------

The word of God is two-fold, and, in both senses, is wisdom; for a word
without wisdom is of little value, and wisdom without a word is of
little use. Now, `I.` Divine revelation is the word and wisdom of God, and
that pure religion and undefiled which is built upon it; and of that
Solomon here speaks, recommending it to us as faithful, and well worthy
of all acceptation (v. 1-2). God, by it, instructs, and governs, and
blesses, the children of men. `II.` The redeemer is the eternal Word and
wisdom, the Logos. He is the Wisdom that speaks to the children of men
in the former part of the chapter. All divine revelation passes through
his hand, and centres in him; but of him as the personal Wisdom, the
second person in the Godhead, in the judgment of many of the ancients,
Solomon here speaks (v. 22-31). He concludes with a repeated charge to
the children of men diligently to attend to the voice of God in his word
(v. 32-36).

### Verses 1-11

The will of God revealed to us for our salvation is here largely
represented to us as easy to be known and understood, that none may have
an excuse for their ignorance or error, and as worthy to be embraced,
that none may have an excuse for their carelessness and unbelief.

`I.` The things revealed are easy to be known, for they belong to us and
to our children (Deu. 29:29), and we need not soar up to heaven, or dive
into the depths, to get the knowledge of them (Deu. 30:11), for they are
published and proclaimed in some measure by the works of the creation
(Ps. 19:1), more fully by the consciences of men and the eternal reasons
and rules of good and evil, but most clearly by Moses and the prophets;
let them hear them. The precepts of wisdom may easily be known; for, 1.
They are proclaimed aloud (v. 1): Does not Wisdom cry? Yes, she cries
aloud, and does not spare (Isa. 58:1); she puts forth her voice, as one
in earnest and desirous to be heard. Jesus stood and cried, Jn. 7:37.
The curses and blessings were read with a loud voice by the Levites,
Deu. 27:14. And men\'s own hearts sometimes speak aloud to them; there
are clamours of conscience, as well as whispers. 2. They are proclaimed
from on high (v. 2): She stands in the top of high places; it was from
the top of Mount Sinai that the law was given, and Christ expounded it
in a sermon upon the mount. Nay, if we slight divine revelation, we turn
away from him that speaks from heaven, a high place indeed, Heb. 12:25.
The adulterous woman spoke in secret, the oracles of the heathen
muttered, but Wisdom speaks openly; truth seeks no corners, but gladly
appeals to the light. 3. They are proclaimed in the places of concourse,
where multitudes are gathered together, the more the better. Jesus spoke
in the synagogues and in the temple, whither the Jews always resorted,
Jn. 18:20. Every man that passes by on the road, of what rank or
condition soever, may know what is good, and what the Lord requires of
him, if it be not his own fault. There is no speech nor language where
Wisdom\'s voice is not heard; her discoveries and directions are given
to all promiscuously. He that has ears to hear, let him hear. 4. They
are proclaimed where they are most needed. They are intended for the
guide of our way, and therefore are published in the places of the
paths, where many ways meet, that travellers may be shown, if they will
but ask, which is the right way, just then when they are at a loss; thou
shalt then hear the word behind thee, saying, This is the way, Isa.
30:21. The foolish man known not how to go to the city (Eccl. 10:15),
and therefore Wisdom stands ready to direct him, stands at the gates, at
the entry of the city, ready to tell him where the seer\'s house is, 1
Sa. 9:18. Nay, she follows men to their own houses, and cries to them at
the coming in at the doors, saying, Peace be to this house; and, if the
son of peace be there, it shall certainly abide upon it. God\'s
ministers are appointed to testify to people both publicly and from
house to house. Their own consciences follow them with admonitions
wherever they go, which they cannot be out of the hearing of while they
carry their own heads and hearts about with them, which are a law unto
themselves. 5. They are directed to the children of men. We attend to
that discourse in which we hear ourselves named, though otherwise we
should have neglected it; therefore Wisdom speaks to us: \"Unto you, O
men! I call (v. 4), not to angels (they need not these instructions),
not to devils (they are past them), not to the brute-creatures (they are
not capable of them), but to you, O men! who are taught more than the
beasts of the earth and made wiser than the fowls of heaven. To you is
this law given, to you is the word of this invitation, this exhortation
sent. My voice is to the sons of men, who are concerned to receive
instruction, and to whom, one would think, it should be very welcome. It
is not, to you, O Jews! only, that Wisdom cries, nor to you, O
gentlemen! not to you, O scholars! but to you, O men! O sons of men!
even the meanest.\" 6. They are designed to make them wise (v. 5); they
are calculated not only for men that are capable of wisdom, but for
sinful men, fallen men, foolish men, that need it, and are undone
without it: \"O you simple ones! understand wisdom. Though you are ever
so simple, Wisdom will take you for her scholars, and not only so, but,
if you will be ruled by her, will undertake to give you an understanding
heart.\" When sinners leave their sins, and become truly religious, then
the simple understand wisdom.

`II.` The things revealed are worthy to be known, well worthy of all
acceptation. We are concerned to hear; for, 1. They are of inestimable
value. They are excellent things (v. 6), princely things, so the word
is. Though they are level to the capacity of the meanest, yet there is
that in them which will be entertainment for the greatest. They are
divine and heavenly things, so excellent that, in comparison with them,
all other learning is but children\'s play. Things which relate to an
eternal God, an immortal soul, and an everlasting state, must needs be
excellent things. 2. They are of incontestable equity, and carry along
with them the evidence of their own goodness. They are right things (v.
6), all in righteousness (v. 8), and nothing froward or perverse in
them. All the dictates and directions of revealed religion are consonant
to, and perfective of, the light and law of nature, and there is nothing
in them that puts any hardship upon us, that lays us under any undue
restraints, unbecoming the dignity and liberty of the human nature,
nothing that we have reason to complain of. All God\'s precepts
concerning all things are right. 3. They are of unquestionable truth.
Wisdom\'s doctrines, upon which her laws are founded, are such as we may
venture our immortal souls upon: My mouth shall speak truth (v. 7), the
whole truth, and nothing but the truth, for it is a testimony to the
world. Every word of God is true; there are not so much as pious frauds
in it, nor are we imposed upon in that which is told us for our good.
Christ is a faithful witness, is the truth itself; wickedness (that is,
lying) is an abomination to his lips. Note, Lying is wickedness, and we
should not only refrain from it, but it should be an abomination to us,
and as far from what we say as from what God says to us. His word to us
is yea, and amen; never then let ours be yea and nay. 4. They are
wonderfully acceptable and agreeable to those who take them aright, who
understand themselves aright, who have not their judgments blinded and
biassed by the world and the flesh, are not under the power of
prejudice, are taught of God, and whose understanding he has opened, who
impartially seek knowledge, take pains for it, and have found it in the
enquiries they have hitherto made. To them, `(1.)` They are all plain, and
not hard to be understood. If the book is sealed, it is to those who are
willingly ignorant. If our gospel is hidden, it is hidden to those who
are lost; but to those who depart from evil, which is understanding, who
have that good understanding which those have who do the commandments,
to them they are all plain and there is nothing difficult in them. The
way of religion is a highway, and the way-faring men, though fools,
shall not err therein, Isa. 35:8. Those therefore do a great wrong to
the common people who deny them the use of the scripture under pretence
that they cannot understand it, whereas it is plain for plain people.
`(2.)` They are all right, and not hard to be submitted to. Those who
discern things that differ, who know good and evil, readily subscribe to
the rectitude of all Wisdom\'s dictates, and therefore, without
murmuring or disputing, govern themselves by them.

`III.` From all this he infers that the right knowledge of those things,
such as transforms us into the image of them, is to be preferred before
all the wealth of this world (v. 10, 11): Receive my instruction, and
not silver. Instruction must not only be heard, but received. We must
bid it welcome, receive the impressions of it, and submit to the command
of it; and this rather than choice gold, that is, 1. We must prefer
religion before riches, and look upon it that, if we have the knowledge
and fear of God in our hearts, we are really more happy and better
provided for every condition of life than if we had ever so much silver
and gold. Wisdom is in itself, and therefore must be in our account,
better than rubies. It will bring us in a better price, be to us a
better portion; show it forth, and it will be a better ornament than
jewels and precious stones of the greatest value. Whatever we can sit
down and wish for of the wealth of this world would, if we had it, be
unworthy to be compared with the advantages that attend serious
godliness. 2. We must be dead to the wealth of this world, that we may
the more closely and earnestly apply ourselves to the business of
religion. We must receive instruction as the main matter, and then be
indifferent whether we receive silver or no; nay, we must not receive it
as our portion and reward, as the rich man in his life-time received his
good things.

### Verses 12-21

Wisdom here is Christ, in whom are hidden all the treasures of wisdom
and knowledge; it is Christ in the word and Christ in the heart, not
only Christ revealed to us, but Christ revealed in us. It is the word of
God, the whole compass of divine revelation; it is God the Word, in whom
all divine revelation centres; it is the soul formed by the word; it is
Christ formed in the soul; it is religion in the purity and power of it.
Glorious things are here spoken of this excellent person, this excellent
thing.

`I.` Divine wisdom gives men good heads (v. 12): I Wisdom dwell with
prudence, not with carnal policy (the wisdom that is from above is
contrary to that, 2 Co. 1:12), but with true discretion, which serves
for the right ordering of the conversation, that wisdom of the prudent
which is to understand his way and is in all cases profitable to direct,
the wisdom of the serpent, not only to guard from harm, but to guide in
doing food. Wisdom dwells with prudence; for prudence is the product of
religion and an ornament to religion; and there are more witty
inventions found out with the help of the scripture, both for the right
understanding of God\'s providences and for the effectual countermining
of Satan\'s devices and the doing of good in our generation, than were
ever discovered by the learning of the philosophers or the politics of
statesmen. We may apply it to Christ himself; he dwells with prudence,
for his whole undertaking is the wisdom of God in a mystery, and in it
God abounds towards us in all wisdom and prudence. Christ found out the
knowledge of that great invention, and a costly one it was to him,
man\'s salvation, by his satisfaction, an admirable expedient. We had
found out many inventions for our ruin; he found out one for our
recovery. The covenant of grace is so well ordered in all things that we
must conclude that he who ordered it dwelt with prudence.

`II.` It gives men good hearts, v. 13. True religion, consisting in the
fear of the Lord, which is the wisdom before recommended, teaches men,
`1.` To hate all sin, as displeasing to God and destructive to the soul:
The fear of the Lord is to hate evil, the evil way, to hate sin as sin,
and therefore to hate every false way. Wherever there is an awe of God
there is a dread of sin, as an evil, as only evil. 2. Particularly to
hate pride and passion, those two common and dangerous sins.
Conceitedness of ourselves, pride and arrogancy, are sins which Christ
hates, and so do all those who have the Spirit of Christ; every one
hates them in others, but we must hate them in ourselves. The froward
mouth, peevishness towards others, God hates, because it is such an
enemy to the peace of mankind, and therefore we should hate it. Be it
spoken to the honour of religion that, however it is unjustly accused,
it is so far from making men conceited and sour that there is nothing
more directly contrary to it than pride and passion, nor which it
teaches us more to detest.

`III.` It has a great influence upon public affairs and the
well-governing of all societies, v. 14. Christ, as God, has strength and
wisdom; wisdom and might are his; as Redeemer, he is the wisdom of God
and the power of God. To all that are his he is made of God both
strength and wisdom; in him they are laid up for us, that we may both
know and do our duty. He is the wonderful counsellor and gives that
grace which alone is sound wisdom. He is understanding itself, and has
strength for all those that strengthen themselves in him. True religion
gives men the best counsel in all difficult cases, and helps to make
their way plain. Wherever it is, it is understanding, it has strength;
it will be all that to us that we need, both for services and
sufferings. Where the word of God dwells richly it makes a man perfect
and furnishes him thoroughly for every good word and work. Kings,
princes, and judges, have of all men most need of wisdom and strength,
of counsel and courage, for the faithful discharge of the trusts reposed
in them, and that they may be blessings to the people over whom they are
set. And therefore Wisdom says, By me kings reign (v. 15, 16), that is,
`1.` Civil government is a divine institution, and those that are
entrusted with the administration of it have their commission from
Christ; it is a branch of his kingly office that by him kings reign;
from him to whom all judgment is committed their power is derived. They
reign by him, and therefore ought to reign for him. 2. Whatever
qualifications for government any kings or princes have they are
indebted to the grace of Christ for them; he gives them the spirit of
government, and they have nothing, no skill, no principles of justice,
but what he endues them with. A divine sentence is in the lips of the
king; and kings are to their subjects what he makes them. 3. Religion is
very much the strength and support of the civil government; it teaches
subjects their duty, and so by it kings reign over them the more easily;
it teaches kings their duty, and so by it kings reign as they ought;
they decree justice, while they rule in the fear of God. Those rule well
whom religion rules.

`IV.` It will make all those happy, truly happy, that receive and embrace
it.

`1.` They shall be happy in the love of Christ; for he it is that says, I
love those that love me, v. 17. Those that love the Lord Jesus Christ in
sincerity shall be beloved of him with a peculiar distinguishing love:
he will love them and manifest himself to them.

`2.` They shall be happy in the success of their enquiries after him:
\"Those that seek me early, seek an acquaintance with me and an interest
in me, seek me early, that is, seek me earnestly, seek me first before
any thing else, that begin betimes in the days of their youth to seek
me, they shall find what they seek.\" Christ shall be theirs, and they
shall be his. He never said, Seek in vain.

`3.` They shall be happy in the wealth of the world, or in that which is
infinitely better. `(1.)` They shall have as much riches and honour as
Infinite Wisdom sees good for them (v. 18); they are with Christ, that
is, he has them to give, and whether he will see fit to give them to us
must be referred to him. Religion sometimes helps to make people rich
and great in this world, gains them a reputation, and so increases their
estates; and the riches which Wisdom gives to her favourites have these
two advantages:-`[1.]` That they are riches and righteousness, riches
honestly got, not by fraud and oppression, but in regular ways, and
riches charitably used, for alms are called righteousness. Those that
have their wealth from God\'s blessing on their industry, and that have
a heart to do good with it, have riches and righteousness. `[2.]` That
therefore they are durable riches. Wealth gotten by vanity will soon be
diminished, but that which is well got will wear well and will be left
to the children\'s children, and that which is well spent in works of
piety and charity is put out to the best interest and so will be
durable; for the friends made by the mammon of unrighteousness when we
fail will receive us into everlasting habitations, Lu. 16:9. It will be
found after many days, for the days of eternity. `(2.)` They shall have
that which is infinitely better, if they have not riches and honour in
this world (v. 19): \"My fruit is better than gold, and will turn to a
better account, will be of more value in less compass, and my revenue
better than the choicest silver, will serve a better trade.\" We may
assure ourselves that not only Wisdom\'s products at last, but her
income in the mean time, not only her fruit, but her revenue, is more
valuable than the best either of the possessions or of the reversions of
this world.

`4.` They shall be happy in the grace of God now; that shall be their
guide in the good way, v. 20. This is that fruit of wisdom which is
better than gold, than fine gold, it leads us in the way of
righteousness, shows us that way and goes before us in it, the way that
God would have us walk in and which will certainly bring us to our
desired end. It leads in the midst of the paths of judgment, and saves
us from deviating on either hand. In medio virtus-Virtue lies in the
midst. Christ by his Spirit guides believers into all truth, and so
leads them in the way of righteousness, and they walk after the Spirit.

`5.` They shall be happy in the glory of God hereafter, v. 21. Therefore
Wisdom leads in the paths of righteousness, not only that she may keep
her friends in the way of duty and obedience, but that she may cause
them to inherit substance and may fill their treasures, which cannot be
done with the things of this world, nor with any thing less than God and
heaven. The happiness of those that love God, and devote themselves to
his service, is substantial and satisfactory. `(1.)` It is substantial; it
is substance itself. It is a happiness which will subsist of itself, and
stand alone, without the accidental supports of outward conveniences.
Spiritual and eternal things are the only real and substantial things.
Joy in God is substantial joy, solid and well-grounded. The promises are
their bonds, Christ is their surety, and both substantial. They inherit
substance; that is, their inheritance hereafter is substantial; it is a
weight of glory; it is substance, Heb. 10:34. All their happiness they
have as heirs; it is grounded upon their sonship. `(2.)` It is satisfying;
it will not only fill their hands, but fill their treasures, not only
maintain them, but make them rich. The things of this world may fill
men\'s bellies (Ps. 17:14), but not their treasures, for they cannot in
them secure to themselves goods for many years; perhaps they may be
deprived of them this night. But let the treasures of the soul be ever
so capacious there is enough in God, and Christ, and heaven, to fill
them. In Wisdom\'s promises believers have goods laid up, not for days
and years, but for eternity; her fruit therefore is better than gold.

### Verses 22-31

That it is an intelligent and divine person that here speaks seems very
plain, and that it is not meant of a mere essential property of the
divine nature, for Wisdom here has personal properties and actions; and
that intelligent divine person can be no other than the Son of God
himself, to whom the principal things here spoken of wisdom are
attributed in other scriptures, and we must explain scripture by itself.
If Solomon himself designed only the praise of wisdom as it is an
attribute of God, by which he made the world and governs it, so to
recommend to men the study of that wisdom which belongs to them, yet the
Spirit of God, who indited what he wrote, carried him, as David often,
to such expressions as could agree to no other than the Son of God, and
would lead us into the knowledge of great things concerning him. All
divine revelation is the revelation of Jesus Christ, which God gave unto
him, and here we are told who and what he is, as God, designed in the
eternal counsels to be the Mediator between God and man. The best
exposition of these verses we have in the first four verses of St.
John\'s gospel. In the beginning was the Word, etc. Concerning the Son
of God observe here,

`I.` His personality and distinct subsistence, one with the Father and of
the same essence, and yet a person of himself, whom the Lord possessed
(v. 22), who was set up (v. 23), was brought forth (v. 24, 25), was by
him (v. 30), for he was the express image of his person, Heb. 1:3.

`II.` His eternity; he was begotten of the Father, for the Lord possessed
him, as his own Son, his beloved Son, laid him in his bosom; he was
brought forth as the only-begotten of the Father, and this before all
worlds, which is most largely insisted upon here. The Word was eternal,
and had a being before the world, before the beginning of time; and
therefore it must follow that it was from eternity. The Lord possessed
him in the beginning of his way, of his eternal counsels, for those were
before his works. This way indeed had no beginning, for God\'s purposes
in himself are eternal like himself, but God speaks to us in our own
language. Wisdom explains herself (v. 23): I was set up from
everlasting. The Son of God was, in the eternal counsels of God,
designed and advanced to be the wisdom and power of the Father, light
and life, and all in all both in the creation and in the redemption of
the world. That he was brought forth as to his being, and set up as to
the divine counsels concerning his office, before the world was made, is
here set forth in a great variety of expressions, much the same with
those by which the eternity of God himself is expressed. Ps. 90:2,
Before the mountains were brought forth. 1. Before the earth was, and
that was made in the beginning, before man was made; therefore the
second Adam had a being before the first, for the first Adam was made of
the earth, the second had a being before the earth, and therefore is not
of the earth, Jn. 3:31. 2. Before the sea was (v. 24), when there were
no depths in which the waters were gathered together, no fountains from
which those waters might arise, none of that deep on which the Spirit of
God moved for the production of the visible creation, Gen. 1:2. 3.
Before the mountains were, the everlasting mountains, v. 25. Eliphaz, to
convince Job of his inability to judge of the divine counsels, asks him
(Job 15:7), Wast thou made before the hills? No, thou wast not. But
before the hills was the eternal Word brought forth. 4. Before the
habitable parts of the world, which men cultivate, and reap the profits
of (v. 26), the fields in the valleys and plains, to which the mountains
are as a wall, which are the highest part of the dust of the world; the
first part of the dust (so some), the atoms which compose the several
parts of the world; the chief or principal part of the dust, so it may
be read, and understood of man, who was made of the dust of the ground
and is dust, but is the principal part of the dust, dust enlivened, dust
refined. The eternal Word had a being before man was made, for in him
was the life of men.

`III.` His agency in making the world. He not only had a being before the
world, but he was present, not as a spectator, but as the architect,
when the world was made. God silenced and humbled Job by asking him,
\"Where wast thou when I laid the foundations of the earth? Who hath
laid the measures thereof? (Job 38:4, etc.). Wast thou that eternal Word
and wisdom, who was the prime manager of that great affair? No; thou art
of yesterday.\" But here the Son of God, referring, as it should seem,
to the discourse God had with Job, declares himself to have been engaged
in that which Job could not pretend to be a witness of and a worker in,
the creation of the world. By him God made the worlds, Eph. 3:9; Heb.
1:2; Col. 1:16. 1. When, on the first day of the creation, in the very
beginning of time, God said, Let there be light, and with a word
produced it, this eternal Wisdom was that almighty Word: Then I was
there, when he prepared the heavens, the fountain of that light, which,
whatever it is here, is there substantial. 2. He was no less active
when, on the second day, he stretched out the firmament, the vast
expanse, and set that as a compass upon the face of the depth (v. 27),
surrounded it on all sides with that canopy, that curtain. Or it may
refer to the exact order and method with which God framed all the parts
of the universe, as the workman marks out his work with his line and
compasses. The work in nothing varied from the plan of it formed in the
eternal mind. 3. He was also employed in the third day\'s work, when the
waters above the heavens, were gathered together by establishing the
clouds above, and those under the heavens by strengthening the fountains
of the deep, which send forth those waters (v. 28), and by preserving
the bounds of the sea, which is the receptacle of those waters, v. 29.
This speaks much the honour of this eternal Wisdom, for by this instance
God proves himself a God greatly to be feared (Jer. 5:22) that he has
placed the sand for the bound of the sea, that the dry land might
continue to appear above water, fit to be a habitation for man; and thus
he has appointed the foundation of the earth. How able, how fit, is the
Son of God to be the Saviour of the world, who was the Creator of it!

`IV.` The infinite complacency which the Father had in him, and he in the
Father (v. 30): I was by him, as one brought up with him. As by an
eternal generation he was brought forth of the Father, so by an eternal
counsel he was brought up with him, which intimates, not only the
infinite love of the Father to the Son, who is therefore called the Son
of his love (Col. 1:13), but the mutual consciousness and good
understanding that were between them concerning the work of man\'s
redemption, which the Son was to undertake, and about which the counsel
of peace was between them both, Zec. 6:13. He was alumnus patris-the
Father\'s pupil, as I may say, trained up from eternity for that service
which in time, in the fulness of time, he was to go through with, and is
therein taken under the special tuition and protection of the Father; he
is my servant whom I uphold, Isa. 42:1. He did what he saw the Father do
(Jn. 5:19), pleased his Father, sought his glory, did according to the
commandment he received from his Father, and all this as one brought up
with him. He was daily his Father\'s delight (my elect, in whom my soul
delighteth, says God, Isa. 43:1), and he also rejoiced always before
him. This may be understood either, 1. Of the infinite delight which the
persons of the blessed Trinity have in each other, wherein consists much
of the happiness of the divine nature. Or, 2. Of the pleasure which the
Father took in the operations of the Son, when he made the world; God
saw every thing that the Son made, and, behold, it was very good, it
pleased him, and therefore his Son was daily, day by day, during the six
days of the creation, upon that account, his delight, Ex. 39:43. And the
Son also did himself rejoice before him in the beauty and harmony of the
whole creation, Ps. 104:31. Or, 3. Of the satisfaction they had in each
other, with reference to the great work of man\'s redemption. The Father
delighted in the Son, as Mediator between him and man, was well-pleased
with what he proposed (Mt. 3:17), and therefore loved him because he
undertook to lay down his life for the sheep; he put a confidence in him
that he would go through his work, and not fail nor fly off. The Son
also rejoiced always before him, delighted to do his will (Ps. 40:8),
adhered closely to his undertaking, as one that was well-satisfied in
it, and, when it came to the setting to, expressed as much satisfaction
in it as ever, saying, Lo, I come, to do as in the volume of the book it
is written of me.

`V.` The gracious concern he had for mankind, v. 31. Wisdom rejoiced, not
so much in the rich products of the earth, or the treasures hid in the
bowels of it, as in the habitable parts os it, for her delights were
with the sons of men; not only in the creation of man is it spoken with
a particular air of pleasure (Gen. 1:26), Let us make man, but in the
redemption and salvation of man. The Son of God was ordained, before the
world, to that great work, 1 Pt. 1:20. A remnant of the sons of men were
given him to be brought, through his grace, to his glory, and these were
those in whom his delights were. His church was the habitable part of
his earth, made habitable for him, that the Lord God might dwell even
among those that had been rebellious; and this he rejoiced in, in the
prospect of seeing his seed. Though he foresaw all the difficulties he
was to meet with in his work, the services and sufferings he was to go
through, yet, because it would issue in the glory of his Father and the
salvation of those sons of men that were given him, he looked forward
upon it with the greatest satisfaction imaginable, in which we have all
the encouragement we can desire to come to him and rely upon him for all
the benefits designed us by his glorious undertaking.

### Verses 32-36

We have here the application of Wisdom\'s discourse; the design and
tendency of it is to bring us all into an entire subjection to the laws
of religion, to make us wise and good, not to fill our heads with
speculations, or our tongues with disputes, but to rectify what is amiss
in our hearts and lives. In order to this, here is,

`I.` An exhortation to hear and obey the voice of Wisdom, to attend and
comply with the good instructions that the word of God gives us, and in
them to discern the voice of Christ, as the sheep know the shepherd\'s
voice.

`1.` We must be diligent hearers of the word; for how can we believe in
him of whom we have not heart? \"Hearken unto me, O you children!\" v.
32. \"Read the word written, sit under the word preached, bless God for
both, and hear him in both speaking to you.\" Let children age, and what
they hearken to then, it is likely, they will be so seasoned by as to be
governed by all their days. Let Wisdom\'s children justify Wisdom by
hearkening to her and show themselves to be indeed her children. We must
hear Wisdom\'s words, `(1.)` Submissively, and with a willing heart (v.
33): \"Hear instruction, and refuse it not, either as that which you
need not or as that which you like not; it is offered you as a kindness,
and it is at your peril if you refuse it.\" Those that reject the
counsel of God reject it against themselves, Lu. 7:30. \"Refuse it not
now, lest you should not have another offer.\" `(2.)` Constantly, and with
an attentive ear. We must hear Wisdom so as to watch daily at her gates,
as beggars to receive an alms, as clients and patients to receive
advice, and to wait as servants, with humility, and patience, and ready
observance, at the posts of her doors. See here what a good house Wisdom
keeps, for every day is dole-day; what a good school, for every day is
lecture-day. While we have God\'s works before our eyes, and his word in
our hand, we may be every day hearing Wisdom, and learning instruction
from her. See here what a dutiful and diligent attendance is required of
all Christ\'s disciples; they must watch at the gates. `[1.]` We must
lay hold on all opportunities of getting knowledge and grace, and must
get into, and keep in, a constant settled course of communion with God.
`[2.]` We must be very humble in our attendance on divine instructions,
and be glad of any place, even the meanest, so we may but be within
hearing of them, as David, who would gladly be a door-keeper in the
house of God. `[3.]` We must raise our expectations of these
instructions, and hearken to them with care, and patience, and
perseverance, must watch and wait, as Christ\'s hearers, that hanged on
him to hear him, as the word in the original is (Lu. 19:48) and (ch.
21:38) came early in the morning to hear him.

`2.` We must be conscientious doers of the work, for we are blessed only
in our deed. It is not enough to hearken unto Wisdom\'s words, but we
must keep her ways (v. 32), do every thing that she prescribes, keep
within the hedges of her ways, and not transgress them, keep in the
tracks of her ways, proceed and persevere in them. \"Hear instruction
and be wise; let it be a means to make you wise in ordering your
conversation.\" What we know is known in vain if it do not make us wise,
v. 33.

`II.` An assurance of happiness to all those that do hearken to Wisdom.
They are blessed, v. 32, and again v. 34. Those are blessed that watch
and wait at Wisdom\'s gates; even their attendance there is their
happiness; it is the best place they can be in. Those are blessed that
wait there, for they shall not be put to wait long; let them continue to
knock awhile and it shall be opened to them. They are seeking Wisdom,
and they shall find what they seek. But will it make them amends if they
do find it? Yes (v. 35): Whoso finds me finds life, that is, all
happiness, all that good which he needs or can desire. He finds life in
that grace which is the principle of spiritual life and the pledge of
eternal life. He finds life, for he shall obtain favour of the Lord, and
in his favour is life. If the king\'s favour is towards a wise son, much
more the favour of the King of kings. Christ is Wisdom, and he that
finds Christ, that obtains an interest in him, he finds life; for Christ
is life to all believers. He that has the Son of God has life, eternal
life, and he shall obtain favour of the Lord, who is well-pleased with
all those that are in Christ; nor can we obtain God\'s favour, unless we
find Christ and be found in him.

`III.` The doom passed upon all those that reject Wisdom and her
proposals, v. 36. They are left to ruin themselves, and Wisdom will not
hinder them, because they have set at nought all her counsel. 1. Their
crime is very great; they sin against Wisdom, rebel against its light
and laws, thwart its designs, and by their folly offend it. They sin
against Christ; they act in contempt of his authority, and in
contradiction to all the purposes of his life and death. This is
construed into hating Wisdom, hating Christ; they are his enemies, who
will not have him to reign over them. What can appear worse than hating
him who is the centre of all beauty and fountain of all goodness, love
itself? 2. Their punishment will be very just, for they wilfully bring
it upon themselves. `(1.)` Those that offend Christ do the greatest wrong
to themselves; they wrong their own souls; they wound their own
consciences, bring a blot and stain upon their souls, which renders them
odious in the eyes of God, and unfit for communion with him; they
deceive themselves, disturb themselves, destroy themselves. Sin is a
wrong to the soul. `(2.)` Those that are at variance with Christ are in
love with their own ruin: Those that hate me love death; they love that
which will be their death, and put that from them which would be their
life. Sinners die because they will die, which leaves them inexcusable,
makes their condemnation the more intolerable, and will for ever justify
God when he judges. O Israel! thou hast destroyed thyself.
