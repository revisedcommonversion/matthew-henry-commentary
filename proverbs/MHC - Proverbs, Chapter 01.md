Proverbs, Chapter 1
===================

Commentary
----------

Those who read David\'s psalms, especially those towards the latter end,
would be tempted to think that religion is all rapture and consists in
nothing but the ecstasies and transports of devotion; and doubtless
there is a time for them, and if there be a heaven upon earth it is in
them: but, while we are on earth, we cannot be wholly taken up with
them; we have a life to live in the flesh, must have a conversation in
the world, and into that we must now be taught to carry our religion,
which is a rational thing, and very serviceable to the government of
human life, and tends as much to make us discreet as to make us devout,
to make the face shine before men, in a prudent, honest, useful
conversation, as to make the heart burn towards God in holy and pious
affections. In this chapter we have, `I.` The title of the book, showing
the general scope and design of it (v. 1-6). `II.` The first principle of
it recommended to our serious consideration (v. 7-9). `III.` A necessary
caution against bad company (v. 10-19). `IV.` A faithful and lively
representation of wisdom\'s reasonings with the children of men, and the
certain ruin of those who turn a deaf ear to those reasonings (v.
20-33).

### Verses 1-6

We have here an introduction to this book, which some think was prefixed
by the collector and publisher, as Ezra; but it is rather supposed to
have been penned by Solomon himself, who, in the beginning of his book,
proposes his end in writing it, that he might keep to his business, and
closely pursue that end. We are here told,

`I.` Who wrote these wise sayings, v. 1. They are the proverbs of Solomon.
`1.` His name signifies peaceable, and the character both of his spirit
and of his reign answered to it; both were peaceable. David, whose life
was full of troubles, wrote a book of devotion; for is any afflicted?
let him pray. Solomon, who lived quietly, wrote a book of instruction;
for when the churches had rest they were edified. In times of peace we
should learn ourselves, and teach others, that which in troublous times
both they and we must practise. 2. He was the son of David; it was his
honour to stand related to that good man, and he reckoned it so with
good reason, for he fared the better for it, 1 Ki. 11:12. He had been
blessed with a good education, and many a good prayer had been put up
for him (Ps. 72:1), the effect of both which appeared in his wisdom and
usefulness. The generation of the upright are sometimes thus blessed,
that they are made blessings, eminent blessings, in their day. Christ is
often called the Son of David, and Solomon was a type of him in this, as
in other things, that he opened his mouth in parables or proverbs. 3. He
was king of Israel-a king, and yet it was no disparagement to him to be
an instructor of the ignorant, and a teacher of babes-king of Israel,
that people among whom God was known and his name was great; among them
he learned wisdom, and to them he communicated it. All the earth sought
to Solomon to hear his wisdom, which excelled all men\'s (1 Ki. 4:30;
10:24); it was an honour to Israel that their king was such a dictator,
such an oracle. Solomon was famous for apophthegms; every word he said
had weight in it, and something that was surprising and edifying. His
servants who attended him, and heard his wisdom, had, among them,
collected 3000 proverbs of his which they wrote in their day-books; but
these were of his own writing, and do not amount to nearly a thousand.
In these he was divinely inspired. Some think that out of those other
proverbs of his, which were not so inspired, the apocryphal books of
Ecclesiasticus and the Wisdom of Solomon were compiled, in which are
many excellent sayings, and of great use; but, take altogether, they are
far short of this book. The Roman emperors had each of them his symbol
or motto, as many now have with their coat of arms. But Solomon had many
weighty sayings, not as theirs, borrowed from others, but all the
product of that extraordinary wisdom which God had endued him with.

`II.` For what end they were written (v. 2-4), not to gain a reputation
to the author, or strengthen his interest among his subjects, but for
the use and benefit of all that in every age and place will govern
themselves by these dictates and study them closely. This book will help
us, 1. To form right notions of things, and to possess our minds with
clear and distinct ideas of them, that we may know wisdom and
instruction, that wisdom which is got by instruction, by divine
revelation, may know both how to speak and act wisely ourselves and to
give instruction to others. 2. To distinguish between truth and
falsehood, good and evil-to perceive the words of understanding, to
apprehend them, to judge of them, to guard against mistakes, and to
accommodate what we are taught to ourselves and our own use, that we may
discern things that differ and not be imposed upon, and may approve
things that are excellent and not lose the benefit of them, as the
apostle prays, Phil. 1:10. 3. To order our conversation aright in every
things, v. 3. This book will give, that we may receive, the instruction
of wisdom, that knowledge which will guide our practice in justice,
judgment, and equity (v. 3), which will dispose us to render to all
their due, to God the things that are God\'s, in all the exercises of
religion, and to all men what is due to them, according to the
obligations which by relation, office, contract, or upon any other
account, we lie under to them. Note, Those are truly wise, and none but
those, who are universally conscientious; and the design of the
scripture is to teach us that wisdom, justice in the duties of the first
table, judgment in those of the second table, and equity (that is
sincerity) in both; so some distinguish them.

`III.` For whose use they were written, v. 4. They are of use to all, but
are designed especially, 1. For the simple, to give subtlety to them.
The instructions here given are plain and easy, and level to the meanest
capacity, the wayfaring men, though fools, shall not err therein; and
those are likely to receive benefit by them who are sensible of their
own ignorance and their need to be taught, and are therefore desirous to
receive instruction; and those who receive these instructions in their
light and power, though they be simple, will hereby be made subtle,
graciously crafty to know the sin they should avoid and the duty they
should do, and to escape the tempter\'s wiles. He that is harmless as
the dove by observing Solomon\'s rules may become wise as the serpent;
and he that has been sinfully foolish when he begins to govern himself
by the word of God becomes graciously wise. 2. For young people, to give
them knowledge and discretion. Youth is the learning age, catches at
instructions, receives impressions, and retains what is then received;
it is therefore of great consequence that the mind be then seasoned
well, nor can it receive a better tincture than from Solomon\'s
proverbs. Youth is rash, and heady, and inconsiderate; man is born like
the wild ass\'s colt, and therefore needs to be broken by the restraints
and managed by the rules we find here. And, if young people will but
take heed to their ways according to Solomon\'s proverbs, they will soon
gain the knowledge and discretion of the ancients. Solomon had an eye to
posterity in writing this book, hoping by it to season the minds of the
rising generation with the generous principles of wisdom and virtue.

`IV.` What good use may be made of them, v. 5, 6. Those who are young and
simple may by them be made wise, and are not excluded from Solomon\'s
school, as they were from Plato\'s. But is it only for such? No; here is
not only milk for babes, but strong meat for strong men. This book will
not only make the foolish and bad wise and good, but the wise and good
wiser and better; and though the simple and the young man may perhaps
slight those instructions, and not be the better for them, yet the wise
man will hear. Wisdom will be justified by her own children, though not
by the children sitting in the market-place. Note, Even wise men must
hear, and not think themselves too wise to learn. A wise man is sensible
of his own defects (Plurima ignoro, sed ignorantiam meam non ignoro-I am
ignorant of many things, but not of my own ignorance), and therefore is
still pressing forward, that he may increase in learning, may know more
and know it better, more clearly and distinctly, and may know better how
to make use of it. As long as we live we should strive to increase in
all useful learning. It was a saying of one of the greatest of the
rabbim, Qui non auget scientiam, amittit de ea-If our stock of knowledge
by not increasing, it is wasting; and those that would increase in
learning must study the scriptures; these perfect the man of God. A wise
man, by increasing in learning, is not only profitable to himself, but
to others also, 1. As a counsellor. A man of understanding in these
precepts of wisdom, by comparing them with one another and with his own
observations, shall by degrees attain unto wise counsels; he stands fair
for preferment, and will be consulted as an oracle, and entrusted with
the management of public affairs; he shall come to sit at the helm, so
the word signifies. Note, Industry is the way to honour; and those whom
God has blessed with wisdom must study to do good with it, according as
their sphere is. It is more dignity indeed to be counsellor to the
prince, but it is more charity to be counsellor to the poor, as Job was
with his wisdom. Job 29:15, I was eyes to the blind. 2. As an
interpreter (v. 6)-to understand a proverb. Solomon was himself famous
for expounding riddles and resolving hard questions, which was of old
the celebrated entertainment of the eastern princes, witness the
solutions he gave to the enquiries with which the queen of Sheba thought
to puzzle him. Now here he undertakes to furnish his readers with that
talent, as far as would be serviceable to the best purposes. \"They
shall understand a proverb, even the interpretation, without which the
proverb is a nut uncracked; when they hear a wise saying, though it be
figurative, they shall take the sense of it, and know how to make use of
it.\" The words of the wise are sometimes dark sayings. In St. Paul\'s
epistles there is that which is hard to be understood; but to those who,
being well-versed in the scriptures, know how to compare spiritual
things with spiritual, they will be easy and safe; so that, if you ask
them, Have you understood all these things? they may answer, Yea, Lord.
Note, It is a credit to religion when men of honesty are men of sense;
all good people therefore should aim to be intelligent, and run to and
fro, take pains in the use of means, that their knowledge may be
increased.

### Verses 7-9

Solomon, having undertaken to teach a young man knowledge and
discretion, here lays down two general rules to be observed in order
thereunto, and those are, to fear God and honour his parents, which two
fundamental laws of morality Pythagoras begins his golden verses with,
but the former of them in a wretchedly corrupted state. Primum, deos
immortales cole, parentesque honora-First worship the immortal gods, and
honour your parents. To make young people such as they should be,

`I.` Let them have regard to God as their supreme.

`1.` He lays down this truth, that the fear of the Lord is the beginning
of knowledge (v. 7); it is the principal part of knowledge (so the
margin); it is the head of knowledge; that is, `(1.)` Of all things that
are to be known this is most evident, that God is to be feared, to be
reverenced, served, and worshipped; this is so the beginning of
knowledge that those know nothing who do not know this. `(2.)` In order to
the attaining of all useful knowledge this is most necessary, that we
fear God; we are not qualified to profit by the instructions that are
given us unless our minds be possessed with a holy reverence of God, and
every thought within us be brought into obedience to him. If any man
will do his will, he shall know of his doctrine, Jn. 7:17. `(3.)` As all
our knowledge must take rise from the fear of God, so it must tend to it
as its perfection and centre. Those know enough who know how to fear
God, who are careful in every thing to please him and fearful of
offending him in any thing; this is the Alpha and Omega of knowledge.

`2.` To confirm this truth, that an eye to God must both direct and
quicken all our pursuits of knowledge, he observes, Fools (atheists, who
have no regard to God) despise wisdom and instruction; having no dread
at all of God\'s wrath, nor any desire of his favour, they will not give
you thanks for telling them what they may do to escape his wrath and
obtain his favour. Those who say to the Almighty, Depart from us, who
are so far from fearing him that they set him at defiance, can excite no
surprise if they desire not the knowledge of his ways, but despise that
instruction. Note, Those are fools who do not fear God and value the
scriptures; and though they may pretend to be admirers of wit they are
really strangers and enemies to wisdom.

`II.` Let them have regard to their parents as their superiors (v. 8, 9):
My son, hear the instruction of thy father. He means, not only that he
would have his own children to be observant of him, and of what he said
to them, nor only that he would have his pupils, and those who came to
him to be taught, to look upon him as their father and attend to his
precepts with the disposition of children, but that he would have all
children to be dutiful and respectful to their parents, and to conform
to the virtuous and religious education which they give them, according
to the law of the fifth commandment.

`1.` He takes it for granted that parents will, with all the wisdom they
have, instruct their children, and, with all the authority they have,
give law to them for their good. They are reasonable creatures, and
therefore we must not give them law without instruction; we must draw
them with the cords of a man, and when we tell them what they must do we
must tell them why. But they are corrupt and wilful, and therefore with
the instruction there is need of a law. Abraham will not only catechize,
but command, his household. Both the father and the mother must do all
they can for the good education of their children, and all little
enough.

`2.` He charges children both to receive and to retain the good lessons
and laws their parents give them. `(1.)` To receive them with readiness:
\"Hear the instruction of thy father; hear it and heed it; hear it and
bid it welcome, and be thankful for it, and subscribe to it.\" `(2.)` To
retain them with resolution: \"Forsake not their law; think not that
when thou art grown up, and no longer under tutors and governors, thou
mayest live at large; no, the law of thy mother was according to the law
of thy God, and therefore it must never be forsaken; thou wast trained
up in the way in which thou shouldst go, and therefore, when thou art
old, thou must not depart from it.\" Some observe that whereas the
Gentile ethics, and the laws of the Persians and Romans, provided only
that children should pay respect to their father, the divine law secures
the honour of the mother also.

`3.` He recommends this as that which is very graceful and will put an
honour upon us: \"The instructions and laws of thy parents, carefully
observed and lived up to, shall be an ornament of grace unto thy head
(v. 9), such an ornament as is, in the sight of God, of great price, and
shall make thee look as great as those that wear gold chains about their
necks.\" Let divine truths and commands be to us a coronet, or a collar
of SS, which are badges of first-rate honours; let us value them, and be
ambitious of them, and then they shall be so to us. Those are truly
valuable, and shall be valued, who value themselves more by their virtue
and piety than by their worldly wealth and dignity.

### Verses 10-19

Here Solomon gives another general rule to young people, in order to
their finding out, and keeping in, the paths of wisdom, and that is to
take heed of the snare of bad company. David\'s psalms begin with this
caution, and so do Solomon\'s proverbs; for nothing is more destructive,
both to a lively devotion and to a regular conversation (v. 10): \"My
son, whom I love, and have a tender concern for, if sinners entice thee,
consent thou not.\" This is good advice for parents to give their
children when they send them abroad into the world; it is the same that
St. Peter gave to his new converts, (Acts 2:40), Save yourselves from
this untoward generation. Observe, 1. How industrious wicked people are
to seduce others into the paths of the destroyer: they will entice.
Sinners love company in sin; the angels that fell were tempters almost
as soon as they were sinners. They do not threaten or argue, but entice
with flattery and fair speech; with a bait they draw the unwary young
man to the hook. But they mistake if they think that by bringing others
to partake with them in their guilt, and to be bound, as it were, in the
bond with them, they shall have the less to pay themselves; for they
will have so much the more to answer for. 2. How cautious young people
should be that they be not seduced by them: \"Consent thou not; and
then, though they entice thee, they cannot force thee. Do not say as
they say, nor do as they do or would have thee to do; have no fellowship
with them.\" To enforce this caution,

`I.` He represents the fallacious reasonings which sinners use in their
enticements, and the arts of wheedling which they have for the beguiling
of unstable souls. He specifies highwaymen, who do what they can to draw
others into their gang, v. 11-14. See here what they would have the
young man to do: \"Come with us (v. 11); let us have thy company.\" At
first they pretend to ask no more; but the courtship rises higher (v.
14): \"Cast in thy lot among us; come in partner with us, join thy force
to ours, and let us resolve to live and die together: thou shalt fare as
we fare; and let us all have one purse, that what we get together we may
spend merrily together,\" for that is it they aim it \[at?\]. Two
unreasonable insatiable lusts they propose to themselves the
gratification of, and therewith entice their pray into the snare:-1.
Their cruelty. They thirst after blood, and hate those that are innocent
and never gave them any provocation, because by their honesty and
industry they shame and condemn them: \"Let us therefore lay wait for
their blood, and lurk privily for them; they are conscious to themselves
of no crime and consequently apprehensive of no danger, but travel
unarmed; therefore we shall make the more easy prey of them. And, O how
sweet it will be to swallow them up alive!\" v. 12. These bloody men
would do this as greedily as the hungry lion devours the lamb. If it be
objected, \"The remains of the murdered will betray the murderers;\"
they answer, \"No danger of that; we will swallow them whole as those
that are buried.\" Who could imagine that human nature should degenerate
so far that it should ever be a pleasure to one man to destroy another!
2. Their covetousness. They hope to get a good booty by it (v. 13): \"We
shall find all precious substance by following this trade. What though
we venture our necks by it? we shall fill our houses with spoil.\" See
here, `(1.)` The idea they have of worldly wealth. They call it precious
substance; whereas it is neither substance nor precious; it is a shadow;
it is vanity, especially that which is got by robbery, Ps. 62:10. It is
as that which is not, which will give a man no solid satisfaction. It is
cheap, it is common, yet, in their account, it is precious, and
therefore they will hazard their lives, and perhaps their souls, in
pursuit of it. It is the ruining mistake of thousands that they
over-value the wealth of this world and look on it as precious
substance. `(2.)` The abundance of it which they promise themselves: We
shall fill our houses with it. Those who trade with sin promise
themselves mighty bargains, and that it will turn to a vast account (All
this will I give thee, says the tempter); but they only dream that they
eat; the housefuls dwindle into scarcely a handful, like the grass on
the house-tops.

`II.` He shows the perniciousness of these ways, as a reason why we
should dread them (v. 15): \"My son, walk not thou in the way with them;
do not associate with them; get, and keep, as far off from them as thou
canst; refrain thy foot from their path; do not take example by them,
not do as they do.\" Such is the corruption of our nature that our foot
is very prone to step into the path of sin, so that we must use
necessary violence upon ourselves to refrain our foot from it, and check
ourselves if at any time we take the least step towards it. Consider, 1.
How pernicious their way is in its own nature (v. 16): Their feet run to
evil, to that which is displeasing to God and hurtful to mankind, for
they make haste to shed blood. Note, The way of sin is down-hill; men
not only cannot stop themselves, but, the longer they continue in it,
the faster they run, and make haste in it, as if they were afraid they
should not do mischief enough and were resolved to lose no time. They
said they would proceed leisurely (Let us lay wait for blood, v. 11),
but thou wilt find they are all in haste, so much has Satan filled their
hearts. 2. How pernicious the consequences of it will be. They are
plainly told that this wicked way will certainly end in their own
destruction, and yet they persist in it. Herein, `(1.)` They are like the
silly bird, that sees the net spread to take her, and yet it is in vain;
she is decoyed into it by the bait, and will not take the warning which
her own eyes gave her, v. 17. But we think ourselves of more value than
many sparrows, and therefore should have more wit, and act with more
caution. God has made us wiser than the fowls of heaven (Job 35:11), and
shall we then be as stupid as they? `(2.)` They are worse than the birds,
and have not the sense which we sometimes perceive them to have; for the
fowler knows it is in vain to lay his snare in the sight of the bird,
and therefore he has arts to conceal it. But the sinner sees ruin at the
end of his way; the murderer, the thief, see the jail and the gallows
before them, nay, they may see hell before them; their watchmen tell
them they shall surely die, but it is to no purpose; they rush into sin,
and rush on in it, like the horse into the battle. For really the stone
they roll will turn upon themselves, v. 18, 19. They lay wait, and lurk
privily, for the blood and lives of others, but it will prove, contrary
to their intention, to be for their own blood, their own lives; they
will come, at length, to a shameful end; and, if they escape the sword
of the magistrate, yet there is a divine Nemesis that pursues them.
Vengeance suffers them not to live. Their greediness of gain hurries
them upon those practices which will not suffer them to live out half
their days, but will cut off the number of their months in the midst.
They have little reason to be proud of their property in that which
takes away the life of the owners and then passes to other masters; and
what is a man profited, though he gain the world, if he lose his life?
For then he can enjoy the world no longer; much less if he lose his
soul, and that be drowned in destruction and perdition, as multitudes
are by the love of money.

Now, though Solomon specifies only the temptation to rob on the highway,
yet he intends hereby to warn us against all other evils which sinners
entice men to. Such are the ways of the drunkards and unclean; they are
indulging themselves in those pleasures which tend to their ruin both
here and for ever; and therefore consent not to them.

### Verses 20-33

Solomon, having shown how dangerous it is to hearken to the temptations
of Satan, here shows how dangerous it is not to hearken to the calls of
God, which we shall for ever rue the neglect of. Observe,

`I.` By whom God calls to us-by wisdom. It is wisdom that crieth without.
The word is plural-wisdoms, for, as there is infinite wisdom in God, so
there is the manifold wisdom of God, Eph. 3:10. God speaks to the
children of men by all the kinds of wisdom, and, as in every will, so in
every word, of God there is a counsel. 1. Human understanding is wisdom,
the light and law of nature, the powers and faculties of reason, and the
office of conscience, Job 38:36. By these God speaks to the children of
men, and reasons with them. The spirit of a man is the candle of the
Lord; and, wherever men go, they may hear a voice behind them, saying,
This is the way; and the voice of conscience is the voice of God, and
not always a still small voice, but sometimes it cries. 2. Civil
government is wisdom; it is God\'s ordinance; magistrates are his
vicegerents \[viceregents?\]. God by David had said to the fools, Deal
not foolishly, Ps. 75:4. In the opening of the gates, and in the places
of concourse, where courts were kept, the judges, the wisdom of the
nation, called to wicked people, in God\'s name, to repent and reform.
3. Divine revelation is wisdom; all its dictates, all its laws, are wise
as wisdom itself. God does, by the written word, by the law of Moses,
which sets before us the blessing and the curse, by the priests\' lips
which keep knowledge, by his servants the prophets, and all the
ministers of this word, declare his mind to sinners, and give them
warning as plainly as that which is proclaimed in the streets or courts
of judicature by the criers. God, in his word, not only opens the case,
but argues it with the children of men. Come, now, and let us reason
together, Isa. 1:18. 4. Christ himself is Wisdom, is Wisdoms, for in him
are hidden all the treasures of wisdom and knowledge, and he is the
centre of all divine revelation, not only the essential Wisdom, but the
eternal Word, by whom God speaks to us and to whom he has committed all
judgment; he it is therefore who here both pleads with sinners and
passes sentence on them. He calls himself Wisdom, Lu. 7:35.

`II.` How he calls to us, and in what manner. 1. Very publicly, that
whosoever hath ears to hear may hear, since all are welcome to take the
benefit of what is said and all are concerned to heed it. The rules of
wisdom are published without in the streets, not in the schools only, or
in the palaces of princes, but in the chief places of concourse, among
the common people that pass and repass in the opening of the gates and
in the city. It is comfortable casting the net of the gospel where there
is a multitude of fish, in hopes that then some will be enclosed. This
was fulfilled in our Lord Jesus, who taught openly in the temple, in
crowds of people, and in secret said nothing (Jn. 18:20), and charged
his ministers to proclaim his gospel on the housetop, Mt. 10:27. God
says (Isa. 45:19), I have not spoken in secret. There is no speech or
language where Wisdom\'s voice is not heard. Truth seeks not corners,
nor is virtue ashamed of itself. 2. Very pathetically; she cries, and
again she cries, as one in earnest. Jesus stood and cried. She utters
her voice, she utters her words with all possible clearness and
affection. God is desirous to be heard and heeded.

`III.` What the call of God and Christ is.

`1.` He reproves sinners for their folly and their obstinately persisting
in it, v. 22. Observe, `(1.)` Who they are that Wisdom here reproves and
expostulates with. In general, they are such as are simple, and
therefore might justly be despised, such as love simplicity, and
therefore might justly be despaired of; but we must use the means even
with those that we have but little hopes of, because we know not what
divine grace may do. Three sorts of persons are here called to:-`[1.]`
Simple ones that love simplicity. Sin is simplicity, and sinners are
simple ones; they do foolishly, very foolishly; and the condition of
those is very bad who love simplicity, are fond of their simple notions
of good and evil, their simple prejudices against the ways of God, and
are in their element when they are doing a simple thing, sporting
themselves in their own deceivings and flattering themselves in their
wickedness. `[2.]` Scorners that delight in scorning-proud people that
take a pleasure in hectoring all about them, jovial people that banter
all mankind, and make a jest of every thing that comes in their way. But
scoffers at religion are especially meant, the worst of sinners, that
scorn to submit to the truths and laws of Christ, and to the reproofs
and admonitions of his word, and take a pride in running down every
thing that is sacred and serious. `[3.]` Fools that hate knowledge. None
but fools hate knowledge. Those only are enemies to religion that do not
understand it aright. And those are the worst of fools that hate to be
instructed and reformed, and have a rooted antipathy to serious
godliness. `(2.)` How the reproof is expressed: \"How long will you do
so?\" This implies that the God of heaven desires the conversion and
reformation of sinners and not their ruin, that he is much displeased
with their obstinacy and dilatoriness, that he waits to be gracious, and
is willing to reason the case with them.

`2.` He invites them to repent and become wise, v. 23. And here, `(1.)` The
precept is plain: Turn you at my reproof. We do not make a right use of
the reproofs that are given us for that which is evil if we do not turn
from it to that which is good; for for this end the reproof was given.
Turn, that is, return to your right mind, turn to God, turn to your
duty, turn and live. `(2.)` The promises are very encouraging. Those that
love simplicity find themselves under a moral impotency to change their
own mind and way; they cannot turn by any power of their own. To this
God answers, \"Behold, I will pour out my Spirit unto you; set
yourselves to do what you can, and the grace of God shall set in with
you, and work in you both to will and to do that good which, without
that grace, you could not do.\" Help thyself, and God will help thee;
stretch forth thy withered hand, and Christ will strengthen and heal it.
`[1.]` The author of this grace is the Spirit, and that is promised: I
will pour out my Spirit unto you, as oil, as water; you shall have the
Spirit in abundance, rivers of living water, Jn. 7:38. Our heavenly
Father will give the Holy Spirit to those that ask him. `[2.]` The means
of this grace is the word, which, if we take it aright, will turn us; it
is therefore promised, \"I will make known my words unto you, not only
speak them to you, but make them known, give you to understand them.\"
Note, Special grace is necessary to a sincere conversion. But that grace
shall never be denied to any that honestly seek it and submit to it.

`3.` He reads the doom of those that continue obstinate against all these
means and methods of grace. It is large and very terrible, v. 24-32.
Wisdom, having called sinners to return, pauses awhile, to see what
effect the call has, hearkens and hears; but they speak not aright (Jer.
8:6), and therefore she goes on to tell them what will be in the end
hereof.

`(1.)` The crime is recited and it is highly provoking. See what it is for
which judgment will be given against impenitent sinners in the great
day, and you will say they deserve it, and the Lord is righteous in it.
It is, in short, rejecting Christ and the offers of his grace, and
refusing to submit to the terms of his gospel, which would have saved
them both from the curse of the law of God and from the dominion of the
law of sin. `[1.]` Christ called to them, to warn them of their danger;
he stretched out his hand to offer them mercy, nay, to help them out of
their miserable condition, stretched out his hand for them to take hold
of, but they refused and no man regarded; some were careless and never
heeded it, nor took notice of what was said to them; others were wilful,
and, though they could not avoid hearing the will of Christ, yet they
gave him a flat denial, they refused, v. 24. They were in love with
their folly, and would not be made wise. They were obstinate to all the
methods that were taken to reclaim them. God stretched out his hand in
mercies bestowed upon them, and, when those would not work upon them, in
corrections, but all were in vain; they regarded the operations of his
hand no more than the declarations of his mouth. `[2.]` Christ reproved
and counselled them, not only reproved them for what they did amiss, but
counselled them to do better (those are reproofs of instruction and
evidences of love and good-will), but they set at nought all his counsel
as not worth heeding, and would none of his reproof, as if it were below
them to be reproved by him and as if they had never done any thing that
deserved reproof, v. 25. This is repeated (v. 30): \"They would none of
my counsel, but rejected it with disdain; they called reproofs
reproaches, and took them as an insult (Jer. 6:10); nay, they despised
all my reproof, as if it were all a jest, and not worth taking notice
of.\" Note, Those are marked for ruin that are deaf to reproof and good
counsel. `[3.]` They were exhorted to submit to the government of right
reason and religion, but they rebelled against both. First, Reason
should not rule them, for they hated knowledge (v. 29), hated the light
of divine truth because it discovered to them the evil of their deeds,
Jn. 3:20. They hated to be told that which they could not bear to know.
Secondly, Religion could not rule them, for they did not choose the fear
of the Lord, but chose to walk in the way of their heart and in the
sight of their eyes. They were pressed to set God always before them,
but they chose rather to cast him and his fear behind their backs. Note,
Those who do not choose the fear of the Lord show that they have no
knowledge.

`(2.)` The sentence is pronounced, and it is certainly ruining. Those that
will not submit to God\'s government will certainly perish under his
wrath and curse, and the gospel itself will not relieve them. They would
not take the benefit of God\'s mercy when it was offered them, and
therefore justly fall as victims to his justice, ch. 29:1. The
threatenings here will have their full accomplishment in the judgment of
the great day and the eternal misery of the impenitent, of which yet
there are some earnests in present judgments. `[1.]` Now sinners are in
prosperity and secure; they live at ease, and set sorrow at defiance.
But, First, Their calamity will come (v. 26); sickness will come, and
those diseases which they shall apprehend to be the very arrests and
harbingers of death; other troubles will come, in mind, in estate, which
will convince them of their folly in setting God at a distance.
Secondly, Their calamity will put them into a great fright. Fear seizes
them, and they apprehend that bad will be worse. When public judgments
are abroad the sinners in Zion are afraid, fearfulness surprises the
hypocrites. Death is the king of terrors to them (Job 15:21, etc.;
18:11, etc.); this fear will be their continual torment. Thirdly,
According to their fright will it be to them. Their fear shall come (the
thing they were afraid of shall befal them); it shall come as
desolation, as a mighty deluge bearing down all before it; it shall be
their destruction, their total and final destruction; and it shall come
as a whirlwind, which suddenly and forcibly drives away all the chaff.
Note, Those that will not admit the fear of God lay themselves open to
all other fears, and their fears will not prove causeless. Fourthly,
Their fright will then be turned into despair: Distress and anguish
shall come upon them, for, having fallen into the pit they were afraid
of, they shall see no way to escape, v. 27. Saul cries out (2 Sa. 1:9),
Anguish has come upon me; and in hell there is weeping, and wailing, and
gnashing of teeth for anguish, tribulation and anguish to the soul of
the sinner, the fruit of the indignation and wrath of the righteous God,
Rom. 2:8, 9. `[2.]` Now God pities their folly, but he will then laugh
at their calamity (v. 26): \"I also will laugh at your distress, even as
you laughed at my counsel.\" Those that ridicule religion will thereby
but make themselves ridiculous before all the world. The righteous will
laugh at them (Ps. 52:6), for God himself will. It intimates that they
shall be for ever shut out of God\'s compassions; they have so long
sinned against mercy that they have now quite sinned it away. His eye
shall not spare, neither will he have pity. Nay, his justice being
glorified in their ruin, he will be pleased with it, though now he would
rather they should turn and live. Ah! I will ease me of my adversaries.
`[3.]` Now God is ready to hear their prayers and to meet them with
mercy, if they would but seek to him for it; but then the door will be
shut, and they shall cry in vain (v. 28): \"Then shall they call upon me
when it is too late, Lord, Lord, open to us. They would then gladly be
beholden to that mercy which now they reject and make light of; but I
will not answer, because, when I called, they would not answer;\" all
the answer then will be, Depart from me, I know you not. This has been
the case of some even in this life, as of Saul, whom God answered not by
Urim or prophets; but, ordinarily, while there is life there is room for
prayer and hope of speeding, and therefore this must refer to the
inexorable justice of the last judgment. Then those that slighted God
will seek him early (that is, earnestly), but in vain; they shall not
find him, because they sought him not when he might be found, Isa. 55:6.
The rich man in hell begged, but was denied. `[4.]` Now they are eager
upon their own way, and fond of their own devices; but then they will
have enough of them (v. 31), according to the proverb, Let men drink as
they brew; they shall eat the fruit of their own way; their wages shall
be according to their work, and, as was their choice, so shall their
doom be, Gal. 6:7, 8. Note, First, There is a natural tendency in sin to
destruction, Jam. 1:15. Sinners are certainly miserable if they do but
eat the fruit of their own way. Secondly, Those that perish must thank
themselves, and can lay no blame upon any other. It is their own device;
let them make their boast of it. God chooses their delusions, Isa. 66:4.
`[5.]` Now they value themselves upon their worldly prosperity; but then
that shall help to aggravate their ruin, v. 32. First, They are now
proud that they can turn away from God and get clear of the restraints
of religion; but that very thing shall slay them, the remembrance of it
shall cut them to the heart. Secondly, They are now proud of their own
security and sensuality; but the ease of the simple (so the margin reads
it) shall slay them; the more secure they are the more certain and the
more dreadful will their destruction be, and the prosperity of fools
shall help to destroy them, by puffing them up with pride, gluing their
hearts to the world, furnishing them with fuel for their lusts, and
hardening their hearts in their evil ways.

`4.` He concludes with an assurance of safety and happiness to all those
that submit to the instructions of wisdom (v. 33): \"Whoso hearkeneth
unto me, and will be ruled by me, he shall,\" `(1.)` \"Be safe; he shall
dwell under the special protection of Heaven, so that nothing shall do
him any real hurt.\" `(2.)` \"He shall be easy, and have no disquieting
apprehensions of danger; he shall not only be safe from evil, but quiet
from the fear of it.\" Though the earth be removed, yet shall not they
fear. Would we be safe from evil, and quiet from the fear of it? Let
religion always rule us and the word of God be our counsellor. That is
the way to dwell safely in this world, and to be quiet from the fear of
evil in the other world.
