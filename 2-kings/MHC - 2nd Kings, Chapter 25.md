2nd Kings, Chapter 25
=====================

Commentary
----------

Ever since David\'s time Jerusalem had been a celebrated place,
beautiful for situation and the joy of the whole earth: while the book
of psalms lasts that name will sound great. In the New Testament we read
much of it, when it was, as here, ripening again for its ruin. In the
close of the Bible we read of a new Jerusalem. Every thing therefore
that concerns Jerusalem is worthy our regard. In this chapter we have,
`I.` The utter destruction of Jerusalem by the Chaldeans, the city
besieged and taken (v. 1-4), the houses burnt (v. 8, 9), and wall broken
down (v. 10), and the inhabitants carried away into captivity (v. 11,
12). The glory of Jerusalem was, 1. That it was the royal city, where
were set \"the thrones of the house of David;\" but that glory has now
departed, for the prince is made a most miserable prisoner, the seed
royal is destroyed (v. 5-7), and the principal officers are put to death
(v. 18-21). 2. That it was the holy city, where was the testimony of
Israel; but that glory has departed, for Solomon\'s temple is burnt to
the ground (v. 9) and the sacred vessels that remained are carried away
to Babylon (v. 13-17). Thus has Jerusalem become as a widow, Lam. 1:1.
Ichabod-Where is the glory? `II.` The distraction and dispersion of the
remnant that was left in Judah under Gedaliah (v. 22-26). `III.` The
countenance which, after thirty-seven years\' imprisonment, was given to
Jehoiachin the captive king of Judah (v. 27-30).

### Verses 1-7

We left king Zedekiah in rebellion against the king of Babylon (ch.
24:20), contriving and endeavouring to shake off his yoke, when he was
no way able to do it, nor took the right method by making God his friend
first. Now here we have an account of the fatal consequences of that
attempt.

`I.` The king of Babylon\'s army laid siege to Jerusalem, v. 1. What
should hinder them when the country was already in their possession? ch.
24:2. They built forts against the city round about, whence, by such
arts of war as they then had, they battered it, sent into it instruments
of death, and kept out of it the necessary supports of life. Formerly
Jerusalem had been compassed with the favour of God as with a shield,
but now their defence had departed from them and their enemies
surrounded them on every side. Those that by sin have provoked God to
leave them will find that innumerable evils will compass them about. Two
years this siege lasted; at first the army retired, for fear of the king
of Egypt (Jer. 37:11), but, finding him not so powerful as they thought,
they soon returned, with a resolution not to quit the city till they had
made themselves masters of it.

`II.` During this siege the famine prevailed (v. 3), so that for a long
time they ate their bread by weight and with care, Eze. 4:16. Thus they
were punished for their gluttony and excess, their fulness of bread and
feeding themselves without fear. At length there was no bread for the
people of the land, that is, the common people, the soldiers, whereby
they were weakened and rendered unfit for service. Now they ate their
own children for want of food. See this foretold by one prophet (Eze.
5:10) and bewailed by another, Lam. 4:3, etc. Jeremiah earnestly
persuaded the king to surrender (Jer. 38:17), but his heart was hardened
to his destruction.

`III.` At length the city was taken by storm: it was broken up, v. 4. The
besiegers made a breach in the wall, at which they forced their way into
it. The besieged, unable any longer to defend it, endeavoured to quit
it, and make the best of their way; and many, no doubt, were put to the
sword, the victorious army being much exasperated by their obstinacy.

`IV.` The king, his family, and all his great men, made their escape in
the night, by some secret passages which the besiegers either had not
discovered or did not keep their eye upon, v. 4. But those as much
deceive themselves who think to escape God\'s judgments as those who
think to brave them; the feet of him that flees from them will as surely
fail as the hands of him that fights against them. When God judges he
will overcome. Intelligence was given to the Chaldeans of the king\'s
flight, and which way he had gone, so that they soon overtook him, v. 5.
His guards were scattered from him, every man shifting for his own
safety. Had he put himself under God\'s protection, that would not have
failed him now. He presently fell into the enemies\' hands, and here we
are told what they did with him. 1. He was brought to the king of
Babylon, and tried by a council of war for rebelling against him who set
him up, and to whom he had sworn fidelity. God and man had a quarrel
with him for this; see Eze. 17:16, etc. The king of Babylon now lay at
Riblah (which lay between Judea and Babylon), that he might be ready to
give orders both to his court at home and his army abroad. 2. His sons
were slain before his eyes, though children, that this doleful
spectacle, the last his eyes were to behold, might leave an impression
of grief and horror upon his spirit as long as he lived. In slaying his
sons, they showed their indignation at his falsehood, and in effect
declared that neither he nor any of his were fit to be trusted, and
therefore that they were not fit to live. 3. His eyes were put out, by
which he was deprived of that common comfort of human life which is
given even to those that are in misery, and to the bitter in soul, the
light of the sun, by which he was also disabled for any service. He
dreaded being mocked, and therefore would not be persuaded to yield
(Jer. 38:19), but that which he feared came upon him with a witness, and
no doubt added much to his misery; for, as those that are deaf suspect
that every body talks of them, so those that are blind suspect that
every body laughs at them. By this two prophecies that seemed to
contradict one another were both fulfilled. Jeremiah prophesied that
Zedekiah should be brought to Babylon, Jer. 32:5; 34:3. Ezekiel
prophesied that he should not see Babylon, Eze. 12:13. He was brought
thither, but, his eyes being put out, he did not see it. Thus he ended
his days, before he ended his life. 4. He was bound in fetters of brass
and so carried to Babylon. He that was blind needed not be bound (his
blindness fettered him), but, for his greater disgrace, they led him
bound; only, whereas common malefactors are laid in irons (Ps. 105:18;
107:10), he, being a prince, was bound with fetters of brass; but that
the metal was somewhat nobler and lighter was little comfort, while
still he was in fetters. Let it not seem strange if those that have been
held in the cords of iniquity come to be thus held in the cords of
affliction, Job 36:8.

### Verses 8-21

Though we have reason to think that the army of the Chaldeans were much
enraged against the city for holding out with so much stubbornness, yet
they did not therefore put all to fire and sword as soon as they had
taken the city (which is too commonly done in such cases), but about a
month after (compare v. 8 with v. 3) Nebuzar-adan was sent with orders
to complete the destruction of Jerusalem. This space God gave them to
repent, after all the foregoing days of his patience, but in vain; their
hearts (for aught that appears) were still hardened, and therefore
execution is awarded to the utmost. 1. The city and temple are burnt, v.
9. It does not appear that the king of Babylon designed to send any
colonies to people Jerusalem and therefore he ordered it to be laid in
ashes, as a nest of rebels. At the burning of the king\'s house and the
houses of the great men one cannot so much wonder (the inhabitants had,
by their sins, made them combustible), but that the house of the Lord
should perish in these flames, that that holy and beautiful house should
be burnt with fire (Isa. 64:11), is very strange. That house which David
prepared for, and which Solomon built at such a vast expense-that house
which had the eye and heart of God perpetually upon it (1 Ki. 9:3)-might
not that have been snatched as a brand out of this burning? No, it must
not be fire-proof against God\'s judgments. This stately structure must
be turned into ashes, and it is probable the ark in it, for the enemies,
having heard how dearly the Philistines paid for the abusing of it,
durst not seize that, nor did any of its friends take care to preserve
it, for then we should have heard of it again in the second temple. One
of the apocryphal writers does indeed tell us that the prophet Jeremiah
got it out of the temple, and conveyed it to a cave in Mount Nebo on the
other side Jordan, and hid it there (2 Macc. 2:4, 5), but that could not
be, for Jeremiah was a close prisoner at that time. By the burning of
the temple God would show how little cares for the external pomp of his
worship when the life and power of religion are neglected. The people
trusted to the temple, as if that would protect them in their sins (Jer.
7:4), but God, by this, let them know that when they had profaned it
they would find it but a refuge of lies. This temple had stood about
420, some say 430 years. The people having forfeited the promises made
concerning it, those promises must be understood of the gospel-temple,
which is God\'s rest for ever. It is observable that the second temple
was burnt by the Romans the same month, and the same day of the month,
that the first temple was burnt by the Chaldeans, which, Josephus says,
was the tenth of August. 2. The walls of Jerusalem are demolished (v.
10), as if the victorious army would be revenged on them for having kept
them out so long, or at least prevent the like opposition another time.
Sin unwalls a people and takes away their defence. These walls were
never repaired till Nehemiah\'s time. 3. The residue of the people are
carried away captive to Babylon, v. 11. Most of the inhabitants had
perished by sword or famine, or had made their escape when the king did
(for it is said, v. 5, His army was scattered from him), so that there
were very few left, who with the deserters, making in all but 832
persons (as appears, Jer. 52:29), were carried away into captivity; only
the poor of the land were left behind (v. 12), to till the ground and
dress the vineyards for the Chaldeans. Sometimes poverty is a
protection; for those that have nothing have nothing to lose. When the
rich Jews, who had been oppressive to the poor, were made strangers,
nay, prisoners, in an enemy\'s country, the poor whom they had despised
and oppressed had liberty and peace in their own country. Thus
Providence sometimes remarkably humbles the proud and favours those of
low degree. 4. The brazen vessels, and other appurtenances of the
temple, are carried away, those of silver and gold being most of them
gone before. Those two famous columns of brass, Jachin and Boaz, which
signified the strength and stability of the house of God, were broken to
pieces and the brass of them was carried to Babylon, v. 13. When the
things signified were sinned away what should the signs stand there for?
Ahaz had profanely cut off the borders of the bases, and put the brazen
sea upon a pavement of stones (2 Ki. 16:17); justly therefore are the
brass themselves, and the brazen sea, delivered into the enemy\'s hand.
It is just with God to take away his ordinances from those that profane
and abuse them, that curtail and depress them. Some things remained of
gold and silver (v. 15) which were now carried off; but most of this
plunder was brass, such a vast quantity of it that it is said to be
without weight, v. 16. The carrying away of the vessels wherewith they
ministered (v. 14) put an end to the ministration. It was a righteous
thing with God to deprive those of the benefit of his worship who had
slighted it so long and preferred false worships before it. Those that
would have many altars shall now have none. 5. Several of the great men
are slain in cold blood-Seraiah the chief priest (who was the father of
Ezra as appears, Ezra 7:1), the second priest (who, when there was
occasion, officiated for him), and three door-keepers of the temple (v.
18), the general of the army, five privy-counsellors (afterwards they
made them up seven, Jer. 52:25), the secretary of war, or pay-master of
the army, and sixty country gentlemen who had concealed themselves in
the city. These, being persons of some rank, were brought to the king of
Babylon (v. 19, 20), who ordered them to be all put to death (v. 21),
when, in reason, they might have hoped that surely the bitterness of
death was past. These the king of Babylon\'s revenge looked upon as most
active in opposing him; but divine justice, we may suppose, looked upon
them as ringleaders in that idolatry and impiety which were punished by
these desolations. This completed the calamity: So Judah was carried
away out of their land, about 860 years after they were put in
possession of it by Joshua. Now the scripture was fulfilled, The Lord
shall bring thee, and the king which thou shalt set over thee, into a
nation which thou hast not known, Deu. 28:36. Sin kept their fathers
forty years out of Canaan, and now turned them out. The Lord is known by
those judgments which he executes, and makes good that word which he has
spoken, Amos 3:2. You only have I known of all the families of the
earth, therefore I will punish you for all your iniquities.

### Verses 22-30

In these verses we have,

`I.` The dispersion of the remaining people. The city of Jerusalem was
quite laid waste. Some people there were in the land of Judah (v. 22)
that had weathered the storm, and (which was no small favour at this
time, Jer. 45:5) had their lives given them for a prey. Now see, 1. What
a good posture they were put into. The king of Babylon appointed
Gedaliah, one of themselves, to be their governor and protector under
him, a very good man, and one that would make the best of the bad, v.
22. His father Ahikam was one that countenanced and protected Jeremiah
when the princes had vowed his death, Jer. 26:24. It is probable that
this Gedaliah, by the advice of Jeremiah, had gone over the Chaldeans,
and had conducted himself so well that the king of Babylon entrusted him
with the government. He resided not at Jerusalem, but at Mizpah, in the
land of Benjamin, a place famous in Samuel\'s time. Thither those came
who had fled from Zedekiah (v. 4) and put themselves under his
protection (v. 23), which he assured them of if they would be patient
and peaceable under the government of the king of Babylon, v. 24.
Gedaliah, though he had not the pomp and power of a sovereign prince,
yet might have been a greater blessing to them than many of their kings
had been, especially having such a privy-council as Jeremiah, who was
now with them, and interested himself in their affairs, Jer. 40:5, 6. 2.
What a fatal breach was made upon them, soon afterwards, by the death of
Gedaliah, within two months after he entered upon his government. The
utter extirpation of the Jews, for the present, was determined, and
therefore it was in vain for them to think of taking root again: the
whole land must be plucked up, Jer. 45:4. Yet this hopeful settlement is
dashed to pieces, not by the Chaldeans, but by some of themselves. The
things of their peace were so hidden from their eyes that they knew not
when they were well off, nor would believe when they were told. `(1.)`
They had a good governor of their own, and him they slew, out of spite
to the Chaldeans, because he was appointed by Nebuchadnezzar, v. 25.
Ishmael, who was of the royal family, envying Gedaliah\'s advancement
and the happy settlement of the people under him, though he could not
propose to set up himself, resolved to ruin him, and basely slew him and
all his friends, both Jews and Chaldeans. Nebuchadnezzar would not,
could not, have been a more mischievous enemy to their peace than this
degenerate branch of the house of David was. `(2.)` They were as yet in
their own good land, but they forsook it, and went to Egypt, for fear of
the Chaldeans, v. 26. The Chaldeans had reason enough to be offended at
the murder of Gedaliah; but if those that remained had humbly
remonstrated, alleging that it was only the act of Ishmael and his
party, we may suppose that those who were innocent of it, nay, who
suffered greatly by it, would not have been punished for it: but, under
pretence of this apprehension, contrary to the counsel of Jeremiah, they
all went to Egypt, where, it is probable, they mixed with the Egyptians
by degrees, and were never heard of more as Israelites. Thus was there a
full end made of them by their own folly and disobedience, and Egypt had
the last of them, that the last verse of that chapter of threatenings
might be fulfilled, after all the rest, Deu. 28:68, The Lord shall bring
thee into Egypt again. These events are more largely related by the
prophet Jeremiah, ch. 40 to ch. 45. Quaeque ipse miserrima vidit, et
quorum pars magna fuit-Which scenes he was doomed to behold, and in
which he bore a melancholy part.

`II.` The reviving of the captive prince. Of Zedekiah we hear no more
after he was carried blind to Babylon; it is probable that he did not
live long, but that when he died he was buried with some marks of
honour, Jer. 34:5. Of Jehoiachin, or Jeconiah, who surrendered himself
(ch. 24:12), we are here told that as soon as Evil-merodach came to the
crown, upon the death of his father Nebuchadnezzar, he released him out
of prison (where he had lain thirty-seven years, and was now fifty-five
years old), spoke kindly to him, paid more respect to him than to any
other of the kings his father had left in captivity (v. 28), gave him
princely clothing instead of his prison-garments, maintained him in his
own palace (v. 29), and allowed him a pension for himself and his family
in some measure corresponding to his rank, a daily rate for every day as
long as he lived. Consider this, 1. As a very happy change of
Jehoiachin\'s condition. To have honour and liberty after he had been so
long in confinement and disgrace, the plenty and pleasure of a court
after he had been so long accustomed to the straits and miseries of a
prison, was like the return of the morning after a very dark and tedious
night. Let none say that they shall never see good again because they
have long seen little but evil; the most miserable know not what blessed
turn Providence may yet give to their affairs, nor what comforts they
are reserved for, according to the days wherein they have been
afflicted, Ps. 110:15. However the death of afflicted saints is to them
such a change as this was to Jehoiachin: it will release them out of
their prison, shake off the body, that prison-garment, and open the way
to their advancement; it will send them to the throne, to the table, of
the King of kings, the glorious liberty of God\'s children. 2. As a very
generous act of Evil-merodach\'s. He thought his father made the yoke of
his captives too heavy, and therefore, with the tenderness of a man and
the honour of a prince, made it lighter. It should seem all the kings he
had in his power were favoured, but Jehoiachin above them all, some
think for the sake of the antiquity of his family and the honour of his
renowned ancestors, David and Solomon. None of the kings of the nations,
it is likely, had descended from so long a race of kings in a direct
lineal succession, and by a male line, as the king of Judah. The Jews
say that this Evil-merodach had been himself imprisoned by his own
father, when he returned from his madness, for some mismanagement at
that time, and that in prison he contracted a friendship with
Jehoiachin, in consequence of which, as soon as he had it in his power,
he showed him this kindness as a sufferer, as a fellow-sufferer. Some
suggest that Evil-merodach had learned from Daniel and his fellows the
principles of the true religion, and was well affected to them, and upon
that account favoured Jehoiachin. 3. As a kind dispensation of
Providence, for the encouragement of the Jews in captivity, and the
support of their faith and hope concerning their enlargement in due
time. This happened just about the midnight of their captivity.
Thirty-six of the seventy years were now past, and almost as many were
yet behind, and now to see their king thus advanced would be a
comfortable earnest to them of their own release in due time, in the set
time. Unto the upright there thus ariseth light in the darkness, to
encourage them to hope, even in the cloudy and dark day, that at evening
time it shall be light; when therefore we are perplexed, let us not be
in despair.
