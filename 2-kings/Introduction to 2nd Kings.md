Introduction to 2nd Kings
=========================

This second book of the Kings (which the Septuagint, numbering from
Samuel, called the fourth) is a continuation of the former book; and,
some think, might better have been made to begin with the fifty-first
verse of the foregoing chapter, where the reign of Ahaziah begins. The
former book had an illustrious beginning, in the glories of the kingdom
of Israel, when it was entire; this has a melancholy conclusion, in the
desolations of the kingdoms of Israel first, and then of Judah, after
they had been long broken into two: for a kingdom divided against itself
cometh to destruction. But, as Elijah\'s mighty works were very much the
glory of the former book, towards the latter end of it, so were
Elisha\'s the glory of this, towards the beginning of it. These prophets
out-shone their princes; and therefore, as far as they go, the history
shall be accounted for in them. Here is, `I.` Elijah fetching fire from
heaven and ascending in fire to heaven, ch. 1 and 2. `II.` Elisha working
many miracles, both for prince and people, Israelites and foreigners,
ch. 3-7. `III.` Hazael and Jehu anointed, the former for the correction of
Israel, the latter for the destruction of the house of Ahab and the
worship of Baal, ch. 8-10. `IV.` The reign of several of the kings, both
of Judah and Israel, ch. 11-16. `V.` The captivity of the ten tribes, ch.
17. `VI.` The good and glorious reign of Hezekiah, ch. 18-20. `VII.`
Manassah\'s wicked reign, and Josiah\'s good one, ch. 21-23. `VIII.` The
destruction of Jerusalem by the king of Babylon, ch. 24 and 25. This
history, in the several passages of it, confirms that observation of
Solomon, That righteousness exalts a nation, but sin is the reproach of
any people.
