2nd Kings, Chapter 7
====================

Commentary
----------

Relief is here brought to Samaria and her king, when the case is, in a
manner, desperate, and the king despairing. `I.` It is foretold by Elisha,
and an unbelieving lord shut out from the benefit of it (v. 1, 2). `II.`
It is brought about, 1. By an unaccountable fright into which God put
the Syrians (v. 6), which caused them to retire precipitately (v. 7). 2.
By the seasonable discovery which four lepers made of this (v. 3-5), and
the account which they gave of it to the court (v. 8-11). 3. By the
cautious trial which the king made of the truth of it (v. 12-15). `III.`
The event answered the prediction both in the sudden plenty (v. 16), and
the death of the unbelieving lord (v. 17-20); for no word of God shall
fall to the ground.

### Verses 1-2

Here, `I.` Elisha foretels that, notwithstanding the great straits to
which the city of Samaria is reduced, yet within twenty-four hours they
shall have plenty, v. 1. The king of Israel despaired of it and grew
weary of waiting: then Elisha foretold it, when things were at the
worst. Man\'s extremity is God\'s opportunity of magnifying his own
power; his time to appear for his people is when their strength is gone,
Deu. 32:36. When they had given over expecting help it came. When the
son of man comes shall he find faith on the earth? Lu. 18:8. The king
said, What shall I wait for the Lord any longer? And perhaps some of the
elders were ready to say the same: \"Well,\" said Elisha, \"you hear
what these say; now hear you the word of the Lord, hear what he says,
hear it and heed it and believe it: to-morrow corn shall be sold at the
usual rate in the gate of Samaria;\" that is, the siege shall be raised,
for the gate of the city shall be opened, and the market shall be held
there as formerly. The return of peace is thus expressed (Jdg. 5:11),
Then shall the people of the Lord go down to the gates, to buy and sell
there. 2. The consequence of that shall be great plenty. This would, in
time, follow of course, but that corn should be thus cheap in so short a
time was quite beyond what could be thought of. Though the king of
Israel had just now threatened Elisha\'s life, God promises to save his
life and the life of his people; for where sin abounded grace doth much
more abound.

`II.` A peer of Israel that happened to be present openly declared his
disbelief of this prediction, v. 2. He was a courtier whom the king had
an affection for, as the man of his right hand, on whom he leaned, that
is, on whose prudence he much relied, and in whom he reposed much
confidence. He thought it impossible, unless God should rain corn out of
the clouds, as once he did manna; no less than the repetition of
Moses\'s miracle will serve him, though that of Elijah might have served
to answer this intention, the increasing of the meal in the barrel.

`III.` The just doom passed upon him for his infidelity, that he should
see this great plenty for this conviction, and yet not eat of it to his
comfort. Note, Unbelief is a sin by which men greatly dishonour and
displease God, and deprive themselves of the favours he designed for
them. The murmuring Israelites saw Canaan, but could not enter in
because of unbelief. Such (says bishop Patrick) will be the portion of
those that believe not the promise of eternal life; they shall see it at
a distance-Abraham afar off, but shall never taste of it; for they
forfeit the benefit of the promise if they cannot find in their heart to
take God\'s word.

### Verses 3-11

We are here told,

`I.` How the siege of Samaria was raised in the evening, at the edge of
night (v. 6, 7), not by might or power, but by the Spirit of the Lord of
hosts, striking terror upon the spirits of the besiegers. Here was not a
sword drawn against them, not a drop of blood shed, it was not by
thunder or hailstones that they were discomfited, nor were they slain,
as Sennacherib\'s army before Jerusalem, by a destroying angel; but, 1.
The Lord made them to hear a noise of chariots and horses. The Syrians
that besieged Dothan had their sight imposed upon, ch. 6:18. These had
their hearing imposed upon. For God knows how to work upon every sense,
pursuant to his own counsels as he makes the hearing ear and the seeing
eye, so he makes the deaf and the blind, Ex. 4:11. Whether the noise was
really made in the air by the ministry of angels, or whether it was only
a sound in their ears, is not certain; which soever it was, it was from
God, who both brings the wind out of his treasures, and forms the spirit
of man within him. The sight of horses and chariots had encouraged the
prophet\'s servant, ch. 6:17. The noise of horses and chariots terrified
the hosts of Syria. For notices from the invisible world are either very
comfortable or very dreadful, according as men are at peace with God or
at war with him. 2. Hearing this noise, they concluded the king of
Israel had certainly procured assistance from some foreign power: He has
hired against us the kings of the Hittites and the kings of the
Egyptians. There was, for aught we know but one king of Egypt, and what
kings there were of the Hittites nobody can imagine; but, as they were
imposed upon by that dreadful sound in their ears, so they imposed upon
themselves by the interpretation they made of it. Had they supposed the
king of Judah to have come with his forces, there would have been more
of probability in their apprehensions than to dream of the kings of the
Hittites and the Egyptians. If the fancies of any of them raised this
spectre, yet their reasons might soon have laid it: how could the king
of Israel, who was closely besieged, hold intelligence with those
distant princes? What had he to hire them with? It was impossible but
some notice would come, before, of the motions of so great a host; but
there were they in great fear where no fear was. 3. Hereupon they all
fled with incredible precipitation, as for their lives, left their camp
as it was: even their horses, that might have hastened their flight,
they could not stay to take with them, v. 7. None of them had so much
sense as to send out scouts to discover the supposed enemy, much less
courage enough to face the enemy, though fatigued with a long march. The
wicked flee when none pursues. God can, when he pleases, dispirit the
boldest and most brave, and make the stoutest heart to tremble. Those
that will not fear God he can make to fear at the shaking of a leaf.

`II.` How the Syrians\' flight was discovered by four leprous men.
Samaria was delivered, and did not know it. The watchmen on the walls
were not aware of the retreat of the enemy, so silently did they steal
away. But Providence employed four lepers to be the intelligencers, who
had their lodging without the gate, being excluded from the city, as
ceremonially unclean: the Jews say they were Gehazi and his three sons;
perhaps Gehazi might be one of them, which might cause him to be taken
notice of afterwards by the king, ch. 8:4. See here, 1. How these lepers
reasoned themselves into a resolution to make a visit in the night to
the camp of the Syrians, v. 3, 4. They were ready to perish for hunger;
none passed through the gate to relieve them. Should they go into the
city, there was nothing to be had there, they mist die in the streets;
should they sit still, they must pine to death in their cottage. They
therefore determine to go over to the enemy, and throw themselves upon
their mercy: if they killed them, better die by the sword than by
famine, one death than a thousand; but perhaps they would save them
alive, as objects of compassion. Common prudence will put us upon that
method which may better our condition, but cannot make it worse. The
prodigal son resolves to return to his father, whose displeasure he had
reason to fear, rather than perish with hunger in the far country. These
lepers conclude, \"If they kill us, we shall but die;\" and happy they
who, in another sense, can thus speak of dying. \"We shall but die, that
is the worst of it, not die and be damned, not be hurt of the second
death.\" According to this resolution, they went, in the beginning of
the night, to the camp of the Syrians, and, to their great surprise,
found it wholly deserted, not a man to be seen or heard in it, v. 5.
Providence ordered it, that these lepers came as soon as ever the
Syrians had fled, for they fled in the twilight, the evening twilight
(v. 7), and in the twilight the lepers came (v. 5), and so no time was
lost. 2. How they reasoned themselves into a resolution to bring tidings
of this to the city. They feasted in the first tent they came to (v. 8)
and then began to think of enriching themselves with the plunder; but
they corrected themselves (v. 9): \"We do not well to conceal these good
tidings from the community we are members of, under colour of being
avenged upon them for excluding us from their society; it was the law
that did it, not they, and therefore let us bring them the news. Though
it awake them from sleep, it will be life from the dead to them.\" Their
own consciences told them that some mischief would befal them if they
acted separately, and sought themselves only. Selfish narrow-spirited
people cannot expect to prosper; the most comfortable advantage is that
which our brethren share with us in. According to this resolution, they
returned to the gate, and acquainted the sentinel with what they had
discovered (v. 10), who straightway brought the intelligence to court
(v. 11), and it was not the less acceptable for being first brought by
lepers.

### Verses 12-20

Here we have,

`I.` The king\'s jealousy of a stratagem in the Syrian\'s retreat, v. 12.
He feared that they had withdrawn into an ambush, to draw out the
besieged, that they might fall on them with more advantage. he knew he
had no reason to expect that God should appear thus wonderfully for him,
having forfeited his favour by his unbelief and impatience. He knew no
reason the Syrians had to fly, for it does not appear that he or any of
this attendants heard the noise of the chariots which the Syrians were
frightened at. Let not those who, like him, are unstable in all their
ways, think to receive any thing from God; nay, a guilty conscience
fears the worst and makes men suspicious.

`II.` The course they took for their satisfaction, and to prevent their
falling into a snare. They sent out spies to see what had become of the
Syrians, and found they had all fled indeed, commanders as well a common
soldiers. They could track them by the garments which they threw off,
and left by the way, for their greater expedition, v. 15. He that gave
this advice seems to have been very sensible of the deplorable condition
the people were in (v. 13); for speaking of the horses, many of which
were dead and the rest ready to perish for hunger, he says, and repeats
it, \"They are as all the multitude of Israel. Israel used to glory in
their multitude, but now they are diminished and brought low.\" He
advised to send five horsemen, but, it should seem, there were only two
horses fit to be sent, and those chariot-horses, v. 14. Now the Lord
repented himself concerning his servants, when he saw that their
strength was gone, Deu. 32:36.

`III.` The plenty that was in Samaria, from the plunder of the camp of
the Syrians, v. 16. Had the Syrians been governed by the modern policies
of war, when they could not take their baggage and their tents with them
they would rather have burnt them (as it is common to do with the forage
of a country) than let them fall into their enemies\' hands; but God
determined that the besieging of Samaria, which was intended for its
ruin, should turn to its advantage, and that Israel should now be
enriched with the spoil of the Syrians as of old with that of the
Egyptians. here see, 1. The wealth of the sinner laid up for the just
(Job 27:16, 17) and the spoilers spoiled, Isa. 33:1. 2. The wants of
Israel supplied in a way that they little thought of, which should
encourage us to depend upon the power and goodness of God in our
greatest straits. 3. The word of Elisha fulfilled to a tittle: A measure
of fine flour was sold for a shekel; those that spoiled the camp had not
only enough to supply themselves with, but an overplus to sell at an
easy rate for the benefit of others, and so even those that tarried at
home did divide the spoil, Ps. 68:12; Isa. 33:23. God\'s promise may be
safely relied on, for no word of his shall fall to the ground.

`IV.` The death of the unbelieving courtier, that questioned the truth of
Elisha\'s word. Divine threatenings will as surely be accomplished as
divine promises. He that believeth not shall be damned stands as firm as
He that believeth shall be saved. This lord, 1. Was preferred by the
king to the charge of the gate (v. 17), to keep the peace, and to see
that there was no tumult or disorder in dividing and disposing of the
spoil. So much trust did the king repose in him, in his prudence and
gravity, and so much did he delight to honour him. He that will be
great, let him serve the public. 2. Was trodden to death by the people
in the gate, either by accident, the crowd being exceedingly great, and
he in the thickest of it, or perhaps designedly, because he abused his
power, and was imperious in restraining the people from satisfying their
hunger. However it was, God\'s justice was glorified, and the word of
Elisha was fulfilled. He saw the plenty, for the silencing and shaming
of his unbelief, corn cheap without opening windows in heaven, and
therein saw his own folly in prescribing to God; but he did not eat of
the plenty he saw. When he was about to fill his belly God cast the fury
of his wrath upon him (Job 20:23) and it came between the cup and the
lip. Justly are those thus tantalized with the world\'s promises that
think themselves tantalized with the promises of God. If believing shall
not be seeing, seeing shall not be enjoying. This matter is repeated,
and the event very particularly compared with the prediction (v. 18-20),
that we might take special notice of it, and might learn, `(1.)` How
deeply God resents out distrust of him, of his power, providence, and
promise. When Israel said, Can God furnish a table? the Lord heard it
and was wroth. Infinite wisdom will not be limited by our folly. God
never promises the end without knowing where to provide the means. `(2.)`
How uncertain life and the enjoyments of it are. Honour and power cannot
secure men from sudden and inglorious deaths. He whom the king leaned
upon the people trod upon; he who fancied himself the stay and support
of the government was trampled under foot as the mire in the streets.
Thus hath the pride of men\'s glory been often stained. `(3.)` How certain
God\'s threatenings are, and how sure to alight on the guilty and
obnoxious heads. Let all men fear before the great God, who treads upon
princes as mortar and is terrible to the kings of the earth.
