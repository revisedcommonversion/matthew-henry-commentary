Daniel, Chapter 10
==================

Commentary
----------

This chapter and the two next (which conclude this book) make up one
entire vision and prophecy, which was communicated to Daniel for the use
of the church, not by signs and figures, as before (ch. 7 and 8), but by
express words; and this was about two years after the vision in the
foregoing chapter. Daniel prayed daily, but had a vision only now and
then. In this chapter we have some things introductory to the prophecy,
in the eleventh chapter the particular predictions, and ch. 12 the
conclusion of it. This chapter shows us, `I.` Daniel\'s solemn fasting and
humiliation, before he had this vision (v. 1-3). `II.` A glorious
appearance of the Son of God to him, and the deep impression it made
upon him (v. 4-9). `III.` The encouragement that was given him to expect
such a discovery of future events as should be satisfactory and useful
both to others and to himself, and that he should be enabled both to
understand the meaning of this discovery, though difficult, and to bear
up under the lustre of it, though dazzling and dreadful (v. 10-21).

### Verses 1-9

This vision is dated in the third year of Cyrus, that is, of his reign
after the conquest of Babylon, his third year since Daniel became
acquainted with him and a subject to him. Here is,

`I.` A general idea of this prophecy (v. 1): The thing was true; every
word of God is so; it was true that Daniel had such a vision, and that
such and such things were said. This he solemnly attests upon the word
of a prophet. Et hoc paratus est verificare-He was prepared to verify
it; and, if it was a word spoken from heaven, no doubt it is stedfast
and may be depended upon. But the time appointed was long, as long as to
the end of the reign of Antiochus, which was 300 years, a long time
indeed when it is looked upon as to come. Nay, and because it is usual
with the prophets to glance at things spiritual and eternal, there is
that in this prophecy which looks in type as far forward as to the end
of the world and the resurrection of the dead; and then he might well
say, The time appointed was long. It was, however, made as plain to him
as if it had been a history rather than a prophecy; he understood the
thing; so distinctly was it delivered to him, and received by him, that
he could say he had understanding of the vision. It did not so much
operate upon his fancy as upon his understanding.

`II.` An account of Daniel\'s mortification of himself before he had this
vision, not in expectation of it, nor, when he prayed that solemn prayer
ch. 9, does it appear that he had any expectation of the vision in
answer to it, but purely from a principle of devotion and pious sympathy
with the afflicted people of God. He was mourning full three weeks (v.
2), for his own sins and the sins of his people, and their sorrows. Some
think that the particular occasion of his mourning was slothfulness and
indifference of many of the Jews, who, though they had liberty to return
to their own land, continued still in the land of their captivity, not
knowing how to value the privileges offered them; and perhaps it
troubled him the more because those that did so justified themselves by
the example of Daniel, though they had not that reason to stay behind
which he had. Others think that it was because he heard of the
obstruction given to the building of the temple by the enemies of the
Jews, who hired counsellors against them, to frustrate their purpose
(Ezra 4:4, 5), all the days of Cyrus, and gained their point from his
son Cambyses, or Artaxerxes, who governed while Cyrus was absent in the
Scythian war. Note, Good men cannot but mourn to see how slowly the work
of God goes on in the world and what opposition it meets with, how weak
its friends are and how active its enemies. During the days of Daniel\'s
mourning he ate no pleasant bread; he could not live without meat, but
he ate little, and very sparingly, and mortified himself in the quality
as well as the quantity of what he ate, which may truly be reckoned
fasting, and a token of humiliation and sorrow. He did not eat the
pleasant bread he used to eat, but that which was course and
unpalatable, which he would not be tempted to eat any more of than was
just necessary to support nature. As ornaments, so delicacies, are very
disagreeable to a day of humiliation. Daniel ate no flesh, drank no
wine, nor anointed himself, for those three week\'s time, v. 3. Though
he was now a very old man, and might plead that the decay of his nature
required what was nourishing, though he was a very great man, and might
plead that, being used to dainty meats, he could not do without them, it
would prejudice his health if he were, yet, when it was both to testify
and to assist his devotion, he could thus deny himself; let this be
noted to the shame of many young people in the common ranks of life who
cannot persuade themselves thus to deny themselves.

`III.` A description of that glorious person whom Daniel saw in vision,
which, it is generally agreed, could be no other that Christ himself,
the eternal Word. He was by the side of the river Hiddekel (v. 4),
probably walking there, not for diversion, but devotion and
contemplation, as Isaac walked in the field, to meditate; and, being a
person of distinction, he had his servants attending him at some
distance. There he looked up, and saw one man Christ Jesus. It must be
he, for he appears in the same resemblance wherein he appeared to St.
John in the isle of Patmos, Rev. 1:13-15. His dress was priestly, for he
is the high priest of our profession, clothed in linen, as the high
priest himself was on the day of atonement, that great day; his loins
were girded (in St. John\'s vision his paps were girded) with a golden
girdle of the finest gold, that of Uphaz, for every thing about Christ
is the best in its kind. The girding of the loins denotes his ready and
diligent application to his work, as his Father\'s servant, in the
business of our redemption. His shape was amiable, his body like the
beryl, a precious stone of a sky-colour. His countenance was awful, and
enough to strike a terror on the beholders, for his face was as the
appearance of lightning, which dazzles the eyes, both brightens and
threatens. His eyes were bright and sparkling, as lamps of fire. His
arms and feet shone like polished brass, v. 6. His voice was loud, and
strong, and very piercing, like the voice of a multitude. The vox
Dei-voice of God can overpower the vox populi-voice of the people. Thus
glorious did Christ appear, and it should engage us, 1. To think highly
and honourably of him. Now consider how great this man is, and in all
things let him have the pre-eminence. 2. To admire his condescension for
us and our salvation. Over all this splendour he drew a veil when he
took upon him the form of a servant, and emptied himself.

`IV.` The wonderful influence that this appearance had upon Daniel and
his attendants, and the terror that it struck upon him and them.

`1.` His attendants saw not the vision; it was not fit that they should
be honoured with the sight of it. There is a divine revelation
vouchsafed to all, from converse with which none are excluded who do not
exclude themselves; but such a vision must be peculiar to Daniel, who
was a favourite. Paul\'s companions were aware of the light, but saw no
man, Acts 9:7; 22:9. Note, It is the honour of those who are beloved of
God that, what is hidden from others, is known to them. Christ manifests
himself to them, but not to the world, Jn. 14:22. But, though they saw
not the vision, they were seized with an unaccountable trembling; either
from the voice they heard, or from some strange concussion or vibration
of the air they felt, so it was that a great quaking fell upon them, so
that they fled to hide themselves, probably among the willows that grew
by the river\'s side. Note, Many have a spirit of bondage to fear who
never receive a spirit of adoption, to whom Christ has been, and will
be, never otherwise than a terror. Now the fright that Daniel\'s
attendants were in is a confirmation of the truth of the vision; it
could not be Daniel\'s fancy, or the product of a heated imagination of
his own, or it had a real, powerful, and strange effect upon those about
him.

`2.` He himself saw it, and saw it alone, but he was not able to bear the
sight of it. It not only dazzled his eyes, but overwhelmed his spirit,
so that there remained no strength in him, v. 8. He said, as Moses
himself, I exceedingly fear and quake. His spirits were all so employed,
either in an intense speculation of the glory of this vision or in the
fortifying of his heart against the terror of it, that his body was left
in a manner lifeless and spiritless. He had no vigour in him, and was
but one remove from a dead carcase; he looked as pale as death, his
colour was gone, his comeliness in him was turned into corruption, and
he retained no strength. Note, the greatest and best of men cannot bear
the immediate discoveries of the divine glory; no man can see it and
live; it is next to death to see a glimpse of it, as Daniel here; but
glorified saints see Christ as he is and can bear the sight. But, though
Daniel was thus dispirited with the vision of Christ, yet he heard the
voice of his words and knew what he said. Note, We must take heed lest
our reverence of God\'s glory, by which we should be awakened to hear
his voice both in his word and in his providence, should degenerate into
such a dread of him as will disable or indispose us to hear it. It
should seem that when the vision of Christ terrified Daniel the voice of
his words soon pacified and composed him, silenced his fear, and laid
him to sleep in a holy security and serenity of mind: When I heard the
voice of his words I fell into a slumber, a sweet slumber, on my face,
and my face towards the ground. When he saw the vision he threw himself
prostrate, into a posture of the most humble adoration, and dropped
asleep, not as careless of what he heard and saw, but charmed with it.
Note, How dreadful soever Christ may appear to those who are under
convictions of sin, and in terror by reason of it, there is enough in
his word to quiet their spirits and make them easy, if they will but
attend to it and apply it.

### Verses 10-21

Much ado here is to bring Daniel to be able to bear what Christ has to
say to him. Still we have him in a fright, hardly and very slowly
recovering himself; but he is still answered and supported with good
words and comfortable words. Let us see how Daniel is by degrees brought
to himself, and gather up the several passages that are to the same
purport.

`I.` Daniel is in a great consternation and finds it very difficult to get
clear of it. The hand that touched him set him at first upon his knees
and the palms of his hands, v. 10. Note, Strength and comfort commonly
come by degrees to those that have been long cast down and disquieted;
they are first helped up a little, and then more. After two days he will
revive us, and then the third day he will raise us up. And we must not
despise the day of small things, but be thankful for the beginnings of
mercy. Afterwards he is helped up, but he stands trembling (v. 11), for
fear lest he fall again. Note, Before God gives strength and power unto
his people he makes them sensible of their own weakness. I trembled in
myself, that I might rest in the day of trouble, Hab. 3:16. But when,
afterwards, Daniel recovered so much strength in his limbs that he could
stand steadily, yet he tells us (v. 15) that he set his face towards the
ground and became dumb; he was as a man astonished, who knew not what to
say, struck dumb with admiration and fear, and was loth to enter into
discourse with one so far above him; he kept silence, yea, even from
good, till he had recollected himself a little. Well, at length he
recovered, not only the use of his feet, but the use of his tongue; and,
when he opened his mouth (v. 16), that which he had to say was to excuse
his having been so long silent, for really he durst not speak, he could
not speak: \"O my lord\" (so, in great humility, this prophet calls the
angel, though the angels, in great humility, called themselves
fellow-servants to the prophets, Rev. 22:9), \"by the vision my sorrows
are turned upon me; they break in upon me with violence; the sense of my
sinful sorrowful state turns upon me when I see thy purity and
brightness.\" Note, Man, who has lost his integrity, has reason to
blush, and be ashamed of himself, when he sees or considers the glory of
the blessed angels that keep their integrity. \"My sorrows are turned
upon me, and I have retained no strength to resist them or bear up a
head against them.\" And again (v. 17), like one half dead with the
fright, he complains, \"As for me, straightway there remained no
strength in me to receive these displays of the divine glory and these
discoveries of the divine will; nay, there is no breath left in me.\"
Such a deliquium did he suffer that he could not draw one breath after
another, but panted and languished, and was in a manner breathless. See
how well it is for us that the treasure of divine revelation is put into
earthen vessels, that God speaks to us by men like ourselves and not by
angels. Whatever we may wish, in a peevish dislike of the method God
takes in dealing with us, it is certain that if we were tried we should
all be of Israel\'s mind at Mt. Sinai, when they said to Moses, Speak
thou to us, and we will hear, but let not God speak to us lest we die,
Ex. 20:19. If Daniel could not bear it, how could we? Now this he
insists upon as an excuse for his irreverent silence, which otherwise
would have been blame-worthy: How can the servant of this my lord talk
with this my lord? v. 17. Note, Whenever we enter into communion with
God it becomes us to have a due sense of the vast distance and
disproportion that there are between us and the holy angels, and of the
infinite distance, and no proportion at all, between us and the holy
God, and to acknowledge that we cannot order our speech by reason of
darkness. How shall we that are dust and ashes speak to the Lord of
glory?

`II.` The blessed angel that was employed by Christ to converse with him
gave him all the encouragement and comfort that could be. It should
seem, it was not he whose glory he saw in vision (v. 5, 6) that here
touched him, and talked with him; that was Christ, but this seems to
have been the angel Gabriel, whom Christ had once before ordered to
instruct Daniel, ch. 8:16. That glorious appearance (as that of the God
of glory to Abraham, Acts 7:2) was to give authority and to gain
attention to what the angel should say. Christ himself comforted John
when he in a like case fell at his feet as dead (Rev. 1:17); but here he
did it by the angel, whom Daniel saw in a glory much inferior to that of
the vision in the verses before; for he was like the similitude of the
sons of men (v. 16), one like the appearance of a man, v. 18. When he
only appeared, as he had done before (ch. 9:21), we do not find that
Daniel was put into any disorder by it, as he was by this vision; and
therefore he is here employed a third time with Daniel.

`1.` He lent him his hand to help him, touched him, and set him upon his
hands and knees (v. 10), else he would still have lain grovelling,
touched his lips (v. 16), else he would have been still dumb; again he
touched him (v. 18), and put strength into him, else he would still have
been staggering and trembling. Note, The hand of God\'s power going
along with the word of his grace is alone effectual to redress all our
grievances, and to rectify whatever is amiss in us. One touch from
heaven brings us to our knees, sets us on our feet, opens our lips, and
strengthens us; for it is God that works on us, and works in us, both to
will and to do that which is good.

`2.` He assured him of the great favour that God had for him: Thou art a
man greatly beloved (v. 11); and again (v. 19), O man greatly beloved!
Note, Nothing is more likely, nothing more effectual, to revive the
drooping spirits of the saints than to be assured of God\'s love to
them. Those are greatly beloved indeed whom God loves; and it is comfort
enough to know it.

`3.` He silenced his fears, and encouraged his hopes, with good words and
comfortable words. He said unto him, Fear not, Daniel (v. 12); and again
(v. 19), O man greatly beloved! fear not; peace be unto thee; be strong,
yea, be strong. Never did any tender mother quiet her child, when any
thing had grieved or frightened it, with more compassion and affection
than the angel here quieted Daniel. Those that are beloved of God have
no reason to be afraid of any evil; peace is to them; God himself speaks
peace to them; and they ought, upon the warrant of that, to speak peace
to themselves; and that peace, that joy of the Lord, will be their
strength. Will God plead against us with his great power? will he take
advantage against us of our being overcome by his terror? No, but he
will put strength into us, Job 23:6. So he did into Daniel here, when,
by reason of the lustre of the vision, no strength of his own remained
in him; and he acknowledges it (v. 19): When he had spoken to me I was
strengthened. Note, God by his word puts life, and strength, and spirit
into his people; for if he says, Be strong, power goes along with the
word. And, now that Daniel has experienced the efficacy of God\'s
strengthening word and grace, he is ready for any thing: \"Now, Let my
lord speak, and I can hear it, I can bear it, and am ready to do
according to it, for thou hast strengthened me.\" Note, To those that
(like Daniel here) have no might God increases strength, Isa. 40:29. And
we cannot keep up our communion with God but by strength derived from
him; but, when he is pleased to put strength into us, we must make a
good use of it, and say, Speak, Lord, for thy servant hears. Let God
enable us to comply with his will, and them, whatever it is, we will
stand complete in it. Da quod jubes, et jube quod vis-Give what thou
commandest, and then command what thou wilt.

`4.` He assured him that his fastings and prayers had come up for a
memorial before God, as the angel told Cornelius (Acts 10:4): Fear not,
Daniel, v. 12. It is natural to fallen man to be afraid of an
extraordinary messenger from heaven, as dreading to hear evil tidings
thence; but Daniel need not fear, for he has by his three weeks\'
humiliation and supplication sent extraordinary messengers to heaven,
which he may expect to return with an olive-branch of peace: \"From the
first day that thou didst set thy heart to understand the word of God,
which is to be the rule of thy prayers, and to chasten thyself before
thy God, that thou mightest put an edge upon thy prayers, thy words were
heard,\" as, before, at the beginning of thy supplication, ch. 9:23.
Note, As the entrance of God\'s word is enlightening to the upright, so
the entrance of their prayers is pleasing to God, Ps. 119:130. From the
first day that we begin to look towards God in a way of duty he is ready
to meet us in a way of mercy. Thus ready is God to hear prayer. I said,
I will confess, and thou forgavest. 5. He informed him that he was sent
to him on purpose to bring him a prediction of the future state of the
church, as a token of God\'s accepting his prayers for the church:
\"Knowest thou wherefore I come unto thee? If thou knewest on what
errand I come, thou wouldst not be put into such a consternation by
it.\" Note, If we rightly understood the meaning of God\'s dealings with
us, and the methods of his providence and grace concerning us, we should
be better reconciled to them. \"I have come for thy words (v. 12), to
bring thee a gracious answer to thy prayers.\" Thus, when God\'s praying
people call to him, he says, Here I am (Isa. 58:9); what would you have
with me? See the power of prayer, what glorious things it has, in its
time, fetched from heaven, what strange discoveries! On what errand did
this angel come to Daniel? He tells him (v. 14): I have come to make
thee understand what shall befal thy people in the latter days. Daniel
was a curious inquisitive man, that had all his days been searching into
secret things, and it would be a great gratification to him to be let
into the knowledge of things to come. Daniel had always been concerned
for the church; its interests lay much upon his heart, and it would be a
particular satisfaction to him to know what its state should be, and he
would know the better what to pray for as long as he lived. He was now
lamenting the difficulties which his people met with in the present day;
but, that he might not be offended in those, the angel must tell him
what greater difficulties are yet before them; and, if they be wearied
now that they only run with the footmen, how will they contend with
horses? Note, It would abate our resentment of present troubles to
consider that we know not but much greater are before us, which we are
concerned to provide for. Daniel must be made to know what shall befal
his people in the latter days of the church, after the cessation of
prophecy, and when the time drew nigh for the Messiah to appear, for yet
the vision is for many days; the principal things that this vision was
intended to give the church the foresight of would come to pass in the
days of Antiochus, nearly 300 years after this. Now that which the angel
is entrusted to communicate to Daniel, and which Daniel is encouraged to
expect from him, is not any curious speculations, moral
prognostications, nor rational prospects of his own, though he is an
angel, but what he has received from the Lord. It was the revelation of
Jesus Christ that the angel gave to St. John to be delivered to the
churches, Rev. 1:1. So here (v. 21): I will show thee what is written in
the scriptures of truth, that is, what is fixed in the determinate
counsel and foreknowledge of God. The decree of God is a thing written,
it is a scripture which remains and cannot be altered. What I have
written I have written. As there are scriptures for the revealed will of
God, the letters-patent, which are published to the world, so there are
scriptures for the secret will of God, the close rolls, which are sealed
among his treasures, the book of his decrees. Both are scriptures of
truth; nothing shall be added to nor taken from either of them. The
secret things belong not to us, only now and then some few paragraphs
have been copied out from the book of God\'s counsels, and delivered to
the prophets for the use of the church, as here to Daniel; but they are
the things revealed, even the words of this law, which belong to us and
to our children; and we are concerned to study what is written in these
scriptures of truth, for they are things which belong to our everlasting
peace.

`6.` He gave him a general account of the adversaries of the church\'s
cause, from whom it might be expected that troubles would arise, and of
its patrons, under whose protection it might be assured of safety and
victory at last. `(1.)` The kings of the earth are and will be its
adversaries; for they set themselves against the Lord, and against his
Anointed, Ps. 2:2. The angel told Daniel that he was to have come to him
with a gracious answer to his prayers, but that the prince of the
kingdom of Persia withstood him one and twenty days, just the three
weeks that Daniel had been fasting and praying. Cambyses king of Persia
had been very busy to embarrass the affairs of the Jews, and to do them
all the mischief he could, and the angel had been all that time employed
to counter-work him; so that he had been constrained to defer his visit
to Daniel till now, for angels can be but in one place at a time. Or, as
Dr. Lightfoot says, This new king of Persia, by hindering the temple,
had hindered those good tidings which otherwise he should have brought
him. The kings and kingdoms of the world were indeed sometimes helpful
to the church, but more often they were injurious to it. \"When I have
gone forth from the kings of Persia, when their monarchy is brought down
for their unkindness to the Jews, then the prince of Grecia shall
come,\" v. 20. The Grecian monarchy, though favourable to the Jews at
first, as the Persian was, will yet come to be vexatious to them. Such
is the state of the church-militant; when it has got clear of one enemy
it has another to encounter: and such a hydra\'s head is that of the old
serpent; when one storm has blown over it is not long before another
rises. `(2.)` The God of heaven is, and will be, its protector, and, under
him, the angels of heaven are its patrons and guardians. `[1.]` Here is
the angel Gabriel busy in the service of the church, making his part
good in defence of it twenty-one days, against the prince of Persia, and
remaining there with the kings of Persia, as consul, or
liege-ambassador, to take care of the affairs of the Jews in that court,
and to do them service, v. 13. And, though much was done against them by
the kings of Persia (God permitting it), it is probably that much more
mischief would have been done them, and they would have been quite
ruined (witness Haman\'s plot) if God had not prevented it by the
ministration of angels. Gabriel resolves, when he has despatched this
errand to Daniel, that he will return to fight with the prince of
Persia, will continue to oppose him, and will at length humble and bring
down that proud monarchy (v. 20), though he knows that another as
mischievous, even that of Grecia, will rise instead of it. `[2.]` Here
is Michael our prince, the great protector of the church, and the patron
of its just but injured cause: The first of the chief princes, v. 13.
Some understand it of a created angel, but an archangel of the highest
order, 1 Th. 4:16; Jude 9. Others think that Michael the archangel is no
other than Christ himself, the angel of the covenant, and the Lord of
the angels, he whom Daniel saw in vision, v. 5. He came to help me (v.
13); and there is none but he that holds with me in these things, v. 21.
Christ is the church\'s prince; angels are not, Heb. 2:5. He presides in
the affairs of the church and effectually provides for its good. He is
said to hold with the angels, for it is he that makes them serviceable
to the heirs of salvation; and, if he were not on the church\'s side,
its case were bad. But, says David, and so says the church, The Lord
takes my part with those that help me, Ps. 118:7. The Lord is with those
that uphold my soul, Ps. 54:4.
