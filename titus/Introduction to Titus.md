Introduction to Titus
=====================

This Epistle of Paul to Titus is much of the same nature with those to
Timothy; both were converts of Paul, and his companions in labours and
sufferings; both were in the office of evangelists, whose work was to
water the churches planted by the apostles, and to set in order the
things that were wanting in them: they were vice-apostles, as it were,
working the work of the Lord, as they did, and mostly under their
direction, though not despotic and arbitrary, but with the concurring
exercise of their own prudence and judgment, 1 Co. 16:10, 12. We read
much of this Titus, his titles, character, and active usefulness, in
many places-he was a Greek, Gal. 2:3. Paul called him his son (Tit.
1:4), his brother (2 Co. 2:13), his partner and fellow-helper (2 Co.
8:23), one that walked in the same spirit and in the same steps with
himself. He went up with the apostles to the church at Jerusalem (Gal.
2:1), was much conversant at Corinth, for which church he had an earnest
care, 2 Co. 8:16. Paul\'s second epistle to them, and probably his first
also, was sent by his hand, 2 Co. 8:16-18, 23; 9:2-4; 12:18. He was with
the apostle at Rome, and thence went into Dalmatia (2 Tim. 4:10), after
which no more occurs of him in the scriptures. So that by them he
appears not to have been a fixed bishop; if such he were, and in those
times, the church of Corinth, where he most laboured, had the best title
to him. In Crete (now called Candia, formerly Hecatompolis, from the
hundred cities that were in it), a large island at the mouth of the
Aegean Sea, the gospel had got some footing; and here were Paul and
Titus in one of their travels, cultivating this plantation; but the
apostle of the Gentiles, having on him the care of all the churches,
could not himself tarry long at this place. He therefore left Titus some
time there, to carry on the work which had been begun, wherein,
probably, meeting with more difficulty than ordinary, Paul wrote this
epistle to him; and yet perhaps not so much for his own sake as for the
people\'s, that the endeavours of Titus, strengthened with apostolic
advice and authority, might be more significant and effectual among
them. He was to see all the cities furnished with good pastors, to
reject and keep out the unmeet and unworthy, to teach sound doctrine,
and instruct all sorts in their duties, to set forth the free grace of
God in man\'s salvation by Christ, and withal to show the necessity of
maintaining good works by those who have believed in God and hope for
eternal life from him.
